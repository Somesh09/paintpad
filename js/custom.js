$(document).ready(function(){

$(".fa-bars").click(function() {
  $(".menu").removeClass('menuClose');
  $(".menu").addClass('menuOpen');

  $(".mainClose").addClass('mainOpen');
  $(".mainOpen").removeClass('mainClose');

  $(".fa-bars").hide(500);
  $(".fa-times").show(500);
});

$(".fa-times").click(function() {
  $(".menu").addClass('menuClose');
  $(".menu").removeClass('menuOpen');

  $(".mainOpen").addClass('mainClose');
  $(".mainClose").removeClass('mainOpen');

  $(".fa-times").hide(500);
  $(".fa-bars").show(500);

});
});