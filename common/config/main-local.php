<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            // 'dsn' => 'mysql:host=localhost;dbname=pp_updated',
            'dsn' => 'mysql:host=localhost;dbname=paintsql',
            'username' => 'root',
            // 'password' => 'Paint$123',
            'password' => '', //modified by ankit on 30-10-2020
            'charset' => 'utf8',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
    ],
];