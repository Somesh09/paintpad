<!DOCTYPE html>
<html>
<head>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>
<body>


<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include_once('db_connection.php');
include_once('simple_html_dom.php');

$curl = curl_init();

curl_setopt_array($curl, [
  CURLOPT_URL => "http://paintpad.mysys.com.au/index.php",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => "search=&brand_id=&option=com_cweb&view=painting_product&layout=painting_product_list&format=painting_product&limitstart=0&limit=500&order_by=product.name&direction=asc&stamp=1603260273&=",
  CURLOPT_HTTPHEADER => [
    "accept: text/html, application/xml, text/xml, */*",
    "accept-language: en-US,en;q=0.9",
    "connection: keep-alive",
    "content-type: application/x-www-form-urlencoded; charset=UTF-8",
    "cookie: _ga=GA1.3.1552556711.1596804089; _gid=GA1.3.150157920.1603082507; a449327b7ee44e35d763db262fc4b65d=d02520qu7hn3181p633vr1pol5",
    "origin: http://paintpad.mysys.com.au",
    "referer: http://paintpad.mysys.com.au/index.php?option=com_cweb&view=management&tmpl=component",
    "user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.80 Safari/537.36",
    "x-requested-with: XMLHttpRequest"
  ],
]);

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  // echo $response;die;

  $html = str_get_html($response);
  ini_set('memory_limit', '-1');
  $sql1 = '';
  foreach($html->find('.open_product_btn') as $key => $element){
    $htm = str_get_html($element->outertext);
    echo 'Product Id:'. $element->product_id.'<br>';
    echo 'Product name:'. $htm->find('tr td')[0].'<br>';
    echo 'Brand name:'. $htm->find('tr td')[1].'<br>';
    $brand_name = $htm->find('tr td')[1]->plaintext;

    $sql = "SELECT * FROM `tbl_brands` where name = '$brand_name'";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
      while($row = $result->fetch_assoc()) {
        $brand_id = $row["brand_id"];
      }
    } else {
      $brand_id = 0;
    }

    echo 'Brand Id:'. $brand_id.'<br>';
    echo 'Type:'. $htm->find('tr td')[2].'<br>';

    $pro_name = $htm->find('tr td')[0]->plaintext;
    if($htm->find('tr td')[2]->plaintext == 'Interior'){
      $type = 1;
    }elseif($htm->find('tr td')[2]->plaintext == 'Exterior'){
      $type = 2;
    }else{
      $type = 3;
    }


    $curl = curl_init();

    curl_setopt_array($curl, [
    CURLOPT_URL => "http://paintpad.mysys.com.au/index.php?option=com_cweb&view=painting_product&layout=add_update_painting_product&format=painting_product&product_id=".$element->product_id."&stamp=1603262745469&nocache=1603262746978",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_POSTFIELDS => "",
    CURLOPT_HTTPHEADER => [
      "accept: text/html, application/xml, text/xml, */*",
      "accept-language: en-US,en;q=0.9",
      "connection: keep-alive",
      "cookie: _ga=GA1.3.1552556711.1596804089; _gid=GA1.3.150157920.1603082507; a449327b7ee44e35d763db262fc4b65d=d02520qu7hn3181p633vr1pol5",
      "referer: http://paintpad.mysys.com.au/index.php?option=com_cweb&view=management&tmpl=component",
      "user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.80 Safari/537.36",
      "x-requested-with: XMLHttpRequest"
    ],
  ]);

    $response123 = curl_exec($curl);
    $err123 = curl_error($curl);

    curl_close($curl);

    if ($err123) {
      echo "cURL Error #:" . $err123;
    } else {
      // print_r($response123);die;
      $responsehtml = $response123;
      $findName = "/^.*\bspread_rate_per_l\b.*$/m";

      $matchesInput = array();
      preg_match($findName, $responsehtml, $matchesInput);
      $wordTorm = '<input type="text" name="spread_rate_per_l" value="';
      $matchesInput =  str_replace($wordTorm,"",$matchesInput[0]);
      $spread_rate = substr($matchesInput, 0, strpos($matchesInput, '"'));
      // print_r($matchesInput);die;

      $sql = "INSERT INTO `tbl_products`(`product_id`, `name`, `spread_rate`, `brand_id`, `type_id`) VALUES ($element->product_id,'$pro_name','$spread_rate',$brand_id,$type);";
      if ($conn->multi_query($sql) === TRUE) {
        echo "New records created successfully";
      } else {
        echo "Error in product: " . $sql . "<br>" . $conn->error;
      }


      //product_add_update_function
      $pattern = '/product_add_update_function/i';
      $response123 =   preg_replace($pattern, 'W3Schools', $response123,1);
      // echo $response123;die;
      $patterntofind = "/^.*\bproduct_add_update_function\b.*$/m";

      $matches = array();
      preg_match($patterntofind, $response123, $matches);
      $keyword = "product_add_update_function('".$element->product_id."' , ";
      $matches =  str_replace($keyword,"",$matches[0]);
      $matches =  str_replace(");","",$matches);
      // $matches = substr($matches, 1, strpos($matches, "] , {"));
      $products = json_decode($matches);
      ini_set('memory_limit', '-1');
      
      foreach($products as $product){
        // echo "<pre>";print_r($product);echo "<br>";
        echo 'Stock Id:'. $product->id.'<br>';
        echo 'Sheen Id:'. $product->sheen_id.'<br>';
        echo 'Litre:'. $product->litre.'<br>';
        echo 'Code:'. $product->code.'<br>';
        echo 'Stock Name:'. $product->name.'<br>';
        echo 'Price:'. $product->price.'<br>';
        $sql1 .= "INSERT INTO `tbl_product_stock`(`product_stock_id`, `product_id`, `sheen_id`, `litres`, `code`, `name`, `price`) VALUES ($product->id,$element->product_id,$product->sheen_id,$product->litre,'$product->code','$product->name',$product->price);";
           
        echo "<br>";
      } 
       

    }
     

    echo "<br>";
  }
  if ($conn->multi_query($sql1) === TRUE) {
    echo "New stock records created successfully";
  } else {
    echo "Error in product: " . $sql1 . "<br>" . $conn->error;
  }
}

?>


</body>
</html>