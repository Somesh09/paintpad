<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Dashboard';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container">

  <div class="row">
    <a data-toggle="modal" data-target="#smallShoes" class="col-4 img-sel" style="background: #149de9;"><img src="<?= Yii::$app->request->baseUrl ?>/image/newquote.png"> <span class="cap-l"> New Quote </span></a>
    <a href="#" class="col img-sel" style="background: #f6f6f6;"><img src="<?= Yii::$app->request->baseUrl ?>/image/quotes.png"><span class="cap-l" style="color: #000"> Quotes </span></a>
    <a href="#" class="col img-sel" style="background: #0f4988;"><img src="<?= Yii::$app->request->baseUrl ?>/image/contact.png"><span class="cap-l"> Contacts </span></a>
    <a href="#" class="col img-sel" style="background: #72d8ff;"><img src="<?= Yii::$app->request->baseUrl ?>/image/calendar.png"><span class="cap-l"> Calendar </span></a>
  </div>
  <div class="row">
    <a href="#" class="col img-sel" style="background: #0f4988;"><img src="<?= Yii::$app->request->baseUrl ?>/image/Invoices.png"><span class="cap-l"> Invoices </span></a>
    <a href="#" class="col img-sel" style="background: #72d8ff;"><img src="<?= Yii::$app->request->baseUrl ?>/image/communication.png"><span class="cap-l"> Communication </span></a>
    <a href="#" class="col img-sel" style="background: #149de9;"><img src="<?= Yii::$app->request->baseUrl ?>/image/settings.png"><span class="cap-l"> Settings </span></a>
    <a href="#" data-toggle="modal" data-target="#smallShoes2" class="col img-sel" style="background: #2e5d8c;"><img src="<?= Yii::$app->request->baseUrl ?>/image/help.png"><span class="cap-l"> Help </span></a>
  </div>

  <!-- <div class="col-2 push-md-5 text-center logout">
    <a href="<?= Yii::$app->request->baseUrl ?>/site/logout" class=""><img src="<?= Yii::$app->request->baseUrl ?>/image/logout.png"><span>Logout</span></a>
  </div> -->



</div>



<!-- The modal -->
<div class="modal fade" id="smallShoes" tabindex="-1" role="dialog" aria-labelledby="modalLabelSmall" aria-hidden="true">
<div class="modal-dialog modal-sm">
<div class="modal-content">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button>
  </div>

  <div class="modal-body">
    <div class="container">
      <div class="row">
      <a href="#" class="col img-sel interior" data-toggle="modal" data-target="#InteriorQuote" data-dismiss="modal"><img src="<?= Yii::$app->request->baseUrl ?>/image/Interior.png"><span class="cap-l"> Interior </span></a>
      <a href="#" class="col img-sel exterior"><img src="<?= Yii::$app->request->baseUrl ?>/image/Exterior.png"><span class="cap-l"> Exterior </span></a>
      </div>
    </div>
  </div>
</div>

</div>
</div>

<div class="modal fade" id="smallShoes2" tabindex="-1" role="dialog" aria-labelledby="modalLabelSmall" aria-hidden="true">
<div class="modal-dialog modal-sm">
<div class="modal-content">

<div class="modal-header">

<button type="button" class="close" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
</div>

<div class="modal-body">
<div class="container">
  <div class="row">
    <a href="#" class="col img-sel" style="background: #149de9; margin: 5px;"><img src="<?= Yii::$app->request->baseUrl ?>/image/tutorials.png"><span class="cap-l"> Tutorials </span></a>
    <a href="#" class="col img-sel" style="background: #0f4988; margin: 5px;"><img src="<?= Yii::$app->request->baseUrl ?>/image/glossary.png"><span class="cap-l"> Glossary </span></a>
    <a href="#" class="col img-sel" style="background: #2e5d8c; margin: 5px;" ><img src="<?= Yii::$app->request->baseUrl ?>/image/support.png"><span class="cap-l"> Support</span></a>
</div>
</div></div>
</div>

</div>
</div>

</div>

<!-- The Interior modal -->
<div class="modal fade" id="InteriorQuote" tabindex="-1" role="dialog" aria-labelledby="modalLabelSmall" aria-hidden="true">
<div class="modal-dialog modal-sm">
<div class="modal-content">

  <div class="modal-header">
    <h3><img src="<?= Yii::$app->request->baseUrl ?>/image/newinteriorquote.png" alt=""> New Interior Quote</h3>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>

  <div class="modal-body">
    <div class="container">
      <div class="row">
        <div class="Detail_prt_div text-center">
          <div class="Detail_inner">
            <ul class="nav nav-tabs" role="tablist">
              <li class="nav-item"><a class="nav-link active" role="tab" data-toggle="tab" href="#home"><h3><span><img src="<?= Yii::$app->request->baseUrl ?>/image/client_blk.png" class="clientIcon"><img src="<?= Yii::$app->request->baseUrl ?>/image/client_detail.png" class="clientIcon icon1"></span> <span class="headingTxt">Client Detail</span></h3></a></li>
              <li class="nav-item"><a class="nav-link" role="tab" data-toggle="tab" href="#menu1"><h3><span><img src="<?= Yii::$app->request->baseUrl ?>/image/siteDetail_blk.png" class="clientIcon"><img src="<?= Yii::$app->request->baseUrl ?>/image/siteDetail.png" class="clientIcon icon1"></span> <span class="headingTxt">Site Detail </span></h3></a></li>
            </ul>
          </div><!-- detail_inner -->
        </div><!-- Detail_prt_div -->
        <div  class="tab-content">
          <div role="tabpanel" id="home" class="tab-pane  in active">
            <div class="client_detail_form">
              <div class="form-group">
                <label>Quote Description</label>
                <textarea class="description_input" placeholder="Please give your quote a brief description"></textarea>
              </div><!-- form-group -->
            </div><!-- client_detail_form -->
            <div class="new_exist_client">
              <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item"><a class="nav-link active" role="tab" data-toggle="tab" href="#menu2"><h4>New Client</h4></a></li>
                <li class="nav-item"><a class="nav-link" role="tab" data-toggle="tab" href="#menu3"><h4>Existing Client</h4></a></li>
              </ul>
              <div class="tab-content">
                <div role="tabpanel" id="menu2" class="tab-pane  in active">
                  <div class="container">
                    <div class="row">
                      <div class="col">
                        <div class="form-group">
                          <input type="text" name="Name" class="form-control name" Placeholder="Contact name">
                        </div><!-- form-group -->
                      </div><!-- col -->
                      <div class="col">
                        <div class="form-group">
                          <input type="text" name="Email" class="form-control email" Placeholder="Contact email">
                        </div><!-- form-group -->
                      </div><!-- col -->
                      <div class="col">
                        <div class="form-group">
                          <input type="text" name="Name" class="form-control phone" Placeholder="Contact phone">
                        </div><!-- form-group -->
                      </div><!-- col -->
                    </div><!-- row -->
                    <div class="row">
                      <div class="col-8">
                        <div class="form-group">
                          <input type="text" name="Address" class="form-control Address" Placeholder="Address">
                        </div><!-- form-group -->
                        <div class="row">
                          <div class="col">
                            <div class="form-group">
                              <input type="text" name="Suburb" class="form-control Suburb" Placeholder="Suburb">
                            </div><!-- form-group -->
                          </div><!-- col -->
                          <div class="col">
                            <div class="form-group">
                              <select name="ad_account_selected" onchange="this.form.submit()" class="selectpicker" data-style="btn-new">
                                <option>Mustard</option>
                                <option>Ketchup</option>
                                <option>Relish</option>        
                              </select>
                            </div><!-- form-group -->
                          </div><!-- col -->
                        </div><!-- row -->
                        <div class="row">
                          <div class="col-6">
                            <div class="form-group">
                              <input type="text" name="Suburb" class="form-control Suburb" Placeholder="Suburb">
                            </div><!-- form-group -->
                          </div><!-- col -->
                        </div><!-- row -->
                      </div><!-- col -->
                      <div class="col-4">
                        <div class="map_div">
                          <img src="<?= Yii::$app->request->baseUrl ?>/image/map.png" alt="">
                          <span class="map-pin"><img src="<?= Yii::$app->request->baseUrl ?>/image/location.png" alt=""></span>
                        </div><!-- map_div -->
                      </div><!-- col -->
                    </div><!-- row -->
                  </div><!-- container -->
                </div><!-- menu2 -->
                <div role="tabpanel" id="menu3" class="tab-pane">
                  <div class="container">
                    <div class="row">
                      <div class="col">
                        <div class="form-group">
                          <input type="text" name="Search" class="form-control search" placeholder="Search">
                          <button type="submit" class="searchbtn btn-primary"><img src="<?= Yii::$app->request->baseUrl ?>/image/search.png" alt=""></button>
                        </div><!-- form-group -->
                        <ul class="existing_client_list">
                          <li><h3>Berts Bloggs</h3><small>98563214701</small></li>
                          <li><h3>Charles Jones</h3><small>98563214701</small></li>
                          <li><h3>David Esson</h3><small>98563214701</small></li>
                        </ul><!-- existing_client_list -->
                      </div><!-- col -->
                      <div class="col border-leftright">
                        <div class="client_Detail text-center">
                          <div class="profile_img"><img src="<?= Yii::$app->request->baseUrl ?>/image/profile.png" alt=""></div>
                          <div class="client_name">Adam ericesson</div>
                          <div class="client_email">adamericesson@gmail.com<br/>0419565575</div>
                        </div><!-- client_detail -->
                      </div><!-- col -->
                      <div class="col address">
                        <h4>Address</h4>
                        <p>1000 kaley Orlando FL 32804 united States</p>
                        <div class="map_div">
                          <img src="<?= Yii::$app->request->baseUrl ?>/image/map.png" alt="">
                          <span class="map-pin"><img src="<?= Yii::$app->request->baseUrl ?>/image/location.png" alt=""></span>
                        </div><!-- map_div -->
                      </div><!-- col -->
                    </div><!-- row -->
                  </div><!-- container -->
                </div><!-- menu2 -->
              </div><!-- tab-content -->
            </div><!-- new_exis_client -->
          </div><!-- tab-pane -->
          <div role="tabpanel" id="menu1" class="tab-pane fade">
            <div class="container">
              <div class="row existing_menu">
                <div class="col-8">
                  <div class="form-group">
                    <input type="text" name="Address" class="form-control Address" Placeholder="Address">
                  </div><!-- form-group -->
                  <div class="row">
                    <div class="col">
                      <div class="form-group">
                        <input type="text" name="Suburb" class="form-control Suburb" Placeholder="Suburb">
                      </div><!-- form-group -->
                    </div><!-- col -->
                    <div class="col">
                      <div class="form-group">
                        <select name="ad_account_selected" onchange="this.form.submit()" class="selectpicker" data-style="btn-new">
                          <option>Mustard</option>
                          <option>Ketchup</option>
                          <option>Relish</option>        
                        </select>
                      </div><!-- form-group -->
                    </div><!-- col -->
                  </div><!-- row -->
                  <div class="row">
                    <div class="col-6">
                      <div class="form-group">
                        <input type="text" name="Suburb" class="form-control Suburb" Placeholder="Suburb">
                      </div><!-- form-group -->
                    </div><!-- col -->
                  </div><!-- row -->
                </div><!-- col -->
                <div class="col-4">
                  <div class="map_div">
                    <img src="<?= Yii::$app->request->baseUrl ?>/image/map.png" alt="">
                    <span class="map-pin"><img src="<?= Yii::$app->request->baseUrl ?>/image/location.png" alt=""></span>
                  </div><!-- map_div -->
                </div><!-- col -->
              </div><!-- row -->
              <div class="row"><div class="col text-right"><button type="submit" class="clear btn-primary">Clear</button></div></div>
              <div class="row existing_client_detail">
                <div class="col">
                  <div class="form-group">
                    <input type="text" name="Name" class="form-control name" Placeholder="Contact name">
                  </div><!-- form-group -->
                </div><!-- col -->
                <div class="col">
                  <div class="form-group">
                    <input type="text" name="Email" class="form-control email" Placeholder="Contact email">
                  </div><!-- form-group -->
                </div><!-- col -->
                <div class="col">
                  <div class="form-group">
                    <input type="text" name="Name" class="form-control phone" Placeholder="Contact phone">
                  </div><!-- form-group -->
                </div><!-- col -->
              </div><!-- row -->
            </div><!-- container -->
          </div><!-- tab-pane -->
        </div><!-- tab-content -->
        <div class="goto_btn_div">
          <div class="col text-right">
            <button type="submit" class="gotoBtn btn btn-primary"><img src="<?= Yii::$app->request->baseUrl ?>/image/goto_icon.png" alt=""></button>
          </div><!-- col -->
        </div><!-- row -->
      </div>
    </div>
  </div>
</div>

</div>
</div>

