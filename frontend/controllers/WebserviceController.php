<?php

namespace frontend\controllers;

use Yii;
use app\models\UserDetails;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\filters\auth\QueryParamAuth;
use common\models\LoginForm;

use common\models\SignupForm;

use yii\base\Model;
use common\models\User;
use yii\web\UploadedFile;

use app\models\TblUsers;
use app\models\TblUsersDetails;
use app\models\TblContacts;
use app\models\TblAddress;
use app\models\TblQuotes;
use app\models\TblCommunications;
use app\models\TblAppointments;
use app\models\TblAppointmentType;
use app\models\TblMembers;
use app\models\TblInvoices;
use app\models\TblBrands;
use app\models\TblTiers;
use app\models\TblStrengths;
use app\models\TblSheen;
use app\models\TblColorTags;
use app\models\TblColors;

use app\models\TblComponents;
use app\models\TblComponentGroups;
use app\models\TblComponentType;

use app\models\TblSpecialItems;
use app\models\TblRoomTypes;


class WebserviceController extends Controller
{

    /**
     * @inheritdoc
     */

    public function behaviors(){

		return [
		    'verbs' => [
		        'class' => VerbFilter::className(),
		        'actions' => [
		            'login'                => ['POST'],
		            'create-contact'       => ['POST'],
		            'contacts'             => ['GET'],
		            'contact-details'      => ['GET'],	
		            'create-quote'         => ['POST'],
		            'communication'        => ['GET'],
		            'appointments'         => ['GET'],		 
		            'create-appointment'   => ['POST'], 
		            'get-appointment-init' => ['GET'],
		            'update-contact'       => ['POST'],
		            'invoices'             => ['GET'],
		            'update-appointment'   => ['POST'],
		            'quotes'               => ['GET'],
		            'brands'               => ['GET'],
		            'tiers'                => ['GET'],
		            'tiers-component'      => ['GET'],
		            'product-colors'       => ['GET'],
		            'components'           => ['GET'],
		            'special-items'        => ['GET'],
		            'rooms'                => ['GET'],
		            'tags'                 => ['GET'],

		        ],
		    ],
		    /*'authenticator' =>[
		    	'class'=> QueryParamAuth::className(),
		    ],*/
		];

    }

    public function actionLogin()
    {
	    //echo "<pre>"; print_r($_POST); exit;

	    $email = !empty(Yii::$app->request->post('email'))?Yii::$app->request->post('email'):'';
	    $password = !empty(Yii::$app->request->post('pwd'))?Yii::$app->request->post('pwd'):'';
	    $response = [];

	    // validate
	    if(empty($email) || empty($password)){
	      $response = [
	        'success' => '0',
	        'message' => 'Email & Password cannot be blank!',
	        'data' => '',
	      ];
	    }
	    else{
	        // search in the database, there is no email referred
	        $user = \common\models\User::findByEmail($email);
	        // if the email exists then
	        if(!empty($user)){
	          // check, valid not password, if valid then make response success
	          if($user->validatePassword($password)){

				$model = TblUsersDetails::find()
				        ->joinWith('user')
				        ->joinWith('address')		        		       
				        ->where([ `TblUsers`.'email' => $email])
				        ->all();

				//echo "<pre>"; print_r($model); exit;       

				$uArr = array(); 

				$arr = array('setting'=>(object) array());

				foreach ($model as $user) {

					$uArr['subscriber_id'] = $user['user_id'];
					$uArr['name']          = $user['name'];
					$uArr['image']         = $user['logo'];

				    // get data from User relation model
				    $email = $user->user->email;

				    // get data from Address relation model
				    $street1 = $user->address->street1;
				    $street2 = $user->address->street2;
				    $suburb  = $user->address->suburb;


				    $uArr['contact']['email']   = $email;
					$uArr['contact']['link']    = $user['website_link'];
					$uArr['contact']['phone']   = $user['phone'];
					$uArr['contact']['address'] = $street1 . ' ' . $street2 . ' ' . $suburb;

					$uArr['counts']['jss']      = 10;
					$uArr['counts']['contacts'] = 15;
					$uArr['counts']['quotes']   = 8;
					$uArr['counts']['docs']     = 12;

					$uArr['settings']           = (object) array();
					$uArr['prep']               = (object) array();
					$uArr['spl_items']          = (object) array();


				}

				$response = [
					'success' => '1',
					'message' => 'Login successful!',
					'data' => $uArr,
				];


	          }
	          // If the password is wrong then make a response like this
	          else{
	            $response = [
	              'success' => '0',
	              'message' => 'Password incorrect!',
	              'data' => '',
	            ];
	          }
	        }
	        // If the email is not found make a response like this
	        else{
	          $response = [
	            'success' => '0',
	            'message' => 'Email doesnot exist in database!',
	            'data' => '',
	          ];
	        }
	    }
	    //return $response;
	    ob_start();
	    echo json_encode($response);
	}


	public function actionCreateContact()
    {
		$host_name = isset($_SERVER['HTTP_X_FORWARDED_HOST']) ? $_SERVER['HTTP_X_FORWARDED_HOST'] : $_SERVER['HTTP_HOST'];  				
    	$siteURL = 'http://' . $host_name . '/' . Yii::$app->request->baseUrl . '/uploads';	  

    	/* mandaory fields */
	    $sub_id  = !empty(Yii::$app->request->post('subscriber_id'))?Yii::$app->request->post('subscriber_id'):'';
	    $name    = !empty(Yii::$app->request->post('name'))?Yii::$app->request->post('name'):'';
	    $email   = !empty(Yii::$app->request->post('email'))?Yii::$app->request->post('email'):'';
	    $phone   = !empty(Yii::$app->request->post('phone'))?Yii::$app->request->post('phone'):'';

	    /* optional fields */
	    $street1 = !empty(Yii::$app->request->post('street1'))?Yii::$app->request->post('street1'):'';
	    $street2 = !empty(Yii::$app->request->post('street2'))?Yii::$app->request->post('street2'):'';
	    $suburb  = !empty(Yii::$app->request->post('suburb'))?Yii::$app->request->post('suburb'):'';
	    $state   = !empty(Yii::$app->request->post('state'))?Yii::$app->request->post('state'):'';
		$postal  = !empty(Yii::$app->request->post('postal'))?Yii::$app->request->post('postal'):'';
		$country = !empty(Yii::$app->request->post('country'))?Yii::$app->request->post('country'):'';
		$frm_add = !empty(Yii::$app->request->post('formatted_addr'))?Yii::$app->request->post('formatted_addr'):'';
		$lat     = !empty(Yii::$app->request->post('lat'))?Yii::$app->request->post('lat'):'';
		$long    = !empty(Yii::$app->request->post('long'))?Yii::$app->request->post('long'):'';

		$uploads = UploadedFile::getInstancesByName("image");

	    $response = [];

	    $arr = array();

	    // validate
	    if(empty($sub_id) || empty($name) || empty($email) || empty($phone)){
	      $response = [
	        'success' => '0',
	        'message' => 'Fields cannot be blank!',
	        'data' => '',
	      ];
	    }
	    else{

	        // search in the database, there is no email referred
	        $user = \common\models\User::findIdentity($sub_id);

	        // if the email exists then
	        if(!empty($user)){

			date_default_timezone_set("UTC");
			$milliseconds = strtotime(gmdate("Y-m-d H:i:s"));

		    	$addressModel = new TblAddress();
				$contactModel = new TblContacts();


				$contact = $contactModel::findByEmail($email);


				if($contact){
					$response = [
						'success' => '0',
						'message' => 'Email already exist',
						'data' => '',
					];
				}else{

				/* Insert data in Address table */

			        $addressModel->street1           = $street1;
			        $addressModel->street2           = $street2;
			        $addressModel->suburb            = $suburb;
			        $addressModel->state_id          = $state;
			        $addressModel->country_id        = $country;
			        $addressModel->postal_code       = $postal;
			        $addressModel->formatted_address = $frm_add;
			        $addressModel->lat               = $lat;
			        $addressModel->lng               = $long;
			        $addressModel->created_at        = $milliseconds;
			        $addressModel->updated_at        = $milliseconds;


			        if($addressModel->save(false)){

			        	$addId = $addressModel->address_id;

			        	$image = '';

			        	if(!empty($uploads)){

							foreach ($uploads as $file){					    		

						        if(isset($file->size)){		           
						            $varr = time().rand(1,100);
						            $file->saveAs('uploads/' . $file->baseName . '_' . $varr .'.' . $file->extension);
						        }

						        $image = $file->baseName . '_' . $varr .'.' . $file->extension;

							} //foreach
			        	}



						/* Insert data in Contact table */	

			        		$contactModel->subscriber_id = $sub_id;
			        		$contactModel->name          = $name;
			        		$contactModel->email         = $email;
			        		$contactModel->phone         = $phone;
			        		$contactModel->image         = $image;
			        		$contactModel->address_id    = $addId;
			        		$contactModel->created_at    = $milliseconds;
			        		$contactModel->updated_at    = $milliseconds;

			        		if($contactModel->save(false)){

								$contactId = $contactModel->contact_id;

							    	$arr['contact_id']     = $contactId;
							    	$arr['name']           = $name;
							    	$arr['email']          = $email;
							    	$arr['phone']          = $phone;
							    	$arr['street1']        = $street1;
							    	$arr['street2']        = $street2;
							    	$arr['suburb']         = $suburb;
							    	$arr['state']          = $state;
							    	$arr['postal']         = $postal;
							    	$arr['country']        = $country;
							    	$arr['formatted_addr'] = $frm_add;
							    	$arr['lat']            = $lat;
							    	$arr['lng']            = $long;
							    	$arr['image']          = $siteURL.'/'.$image;

								      $response = [
								        'success' => '1',
								        'message' => 'Contact data saved successfully!',
								        'data' => $arr,
								      ];

							}
			        }			

				}

	        }
	        // If the email is not found make a response like this
	        else{
	          $response = [
	            'success' => '0',
	            'message' => 'Subscriber doesnot exist in database!',
	            'data' => '',
	          ];
	        }

	    }

	    ob_start();
	    echo json_encode($response);    	

    }//actionCreateContact


	public function actionContacts()
    {

    	/* mandaory fields */
	    $sub_id  = !empty($_GET['subscriber_id'])?$_GET['subscriber_id']:'';

	    //echo 'here-'.$sub_id; exit;    

	    $response = [];

	    $arr = array();

	    // validate
	    if(empty($sub_id)){
	      $response = [
	        'success' => '0',
	        'message' => 'Fields cannot be blank!',
	        'data' => '',
	      ];
	    }
	    else{

	        // search in the database, there is no email referred
	        $subscriber = \common\models\User::findIdentity($sub_id);

	        if(!empty($subscriber)){

				$host_name = isset($_SERVER['HTTP_X_FORWARDED_HOST']) ? $_SERVER['HTTP_X_FORWARDED_HOST'] : $_SERVER['HTTP_HOST'];  				
	        	$siteURL = 'http://' . $host_name . '/' . Yii::$app->request->baseUrl . '/uploads';	        	

				$model = TblContacts::find()
				        ->joinWith('address')       		       
				        ->where([ 'subscriber_id' => $sub_id])
				        ->all();

				//echo "<pre>"; print_r($model); exit;        

				$newarr = array();

				foreach($model as $result){

					$newarr['contact_id'] = $result->contact_id;
					$newarr['name']       = $result->name;
					$newarr['phone']      = $result->phone;
					$newarr['image']      = $siteURL . '/' . $result->image;
					$newarr['email']      = $result->email;

						if(isset($result->address)){

						    // get data from Address relation model
						    $street1        = $result->address->street1;
						    $street2        = $result->address->street2;
						    $suburb         = $result->address->suburb;
						    $state          = $result->address->state_id;
						    $postal         = $result->address->postal_code;
						    $country        = $result->address->country_id;
						    $formatted_addr = $result->address->formatted_address;
						    $lat            = $result->address->lat;
						    $lng            = $result->address->lng;

						 $newarr['address'] = $street1 . ' ' . $street2 . ' ' . $suburb . ' ' . $state . ' ' . $postal . '' . $country;   

						}else{

						 $newarr['address'] = "";   

						}

					 $arr['contact'][] = $newarr;
				}			
				 
				
		          $response = [
		            'success' => '1',
		            'message' => 'Contacts Data!',
		            'data' => $arr,
		          ];

	        }
	        // If the email is not found make a response like this
	        else{
	          $response = [
	            'success' => '0',
	            'message' => 'Subscriber doesnot exist in database!',
	            'data' => '',
	          ];
	        }	

	    }

	    ob_start();
	    echo json_encode($response);    	

    }//actionContacts


	public function actionContactDetails()
    {

    	/* mandaory fields */
	    $sub_id  = !empty($_GET['subscriber_id'])?$_GET['subscriber_id']:'';
	    $con_id  = !empty($_GET['contact_id'])?$_GET['contact_id']:'';

	    //echo 'here-'.$sub_id; exit;    

	    $response = [];

	    $arr = array();

	    // validate
	    if(empty($sub_id) || empty($con_id)){
	      $response = [
	        'success' => '0',
	        'message' => 'Fields cannot be blank!',
	        'data' => '',
	      ];
	    }
	    else{

	        // search in the database, there is no email referred
	        $subscriber = \common\models\User::findIdentity($sub_id);

	        if(!empty($subscriber)){

				$host_name = isset($_SERVER['HTTP_X_FORWARDED_HOST']) ? $_SERVER['HTTP_X_FORWARDED_HOST'] : $_SERVER['HTTP_HOST'];  				
	        	$siteURL = 'http://' . $host_name . '/' . Yii::$app->request->baseUrl . '/uploads';

				$model = TblContacts::find()						
				        ->joinWith('address')
				        ->joinWith(['tblQuotes' => function($q){ $q->joinWith('siteAddress')->orderBy('quote_id DESC')->limit(2)->offset(0);}])	      		       
				        ->where([ 'tbl_contacts.subscriber_id' => $sub_id])
				        ->andWhere(['tbl_contacts.contact_id'=>$con_id])
				        ->orderBy('quote_id DESC')
				        ->all();

					//echo "<pre>"; print_r($model); exit;        

					$uArr = array(); 

					foreach($model as $contact){

					    $name  = $contact['name'];
					    $cId   = $contact['contact_id'];
						$email = $contact['email'];
						$phone = $contact['phone'];
						$image = $siteURL . '/' . $contact['image'];


					    // get data from Address relation model
					    $street1        = $contact->address->street1;
					    $street2        = $contact->address->street2;
					    $suburb         = $contact->address->suburb;
					    $state          = $contact->address->state_id;
					    $postal         = $contact->address->postal_code;
					    $country        = $contact->address->country_id;
					    $formatted_addr = $contact->address->formatted_address;
					    $lat            = $contact->address->lat;
					    $lng            = $contact->address->lng;						



						if(isset($contact->tblQuotes) && !empty($contact->tblQuotes) ){

								$qArr = array();

								foreach($contact->tblQuotes as $quotes){
									
									$qArr['contact']['name']    = $name;
									$qArr['contact']['phone']   = $phone;	
									$qArr['contact']['email']   = $email;
									$qArr['contact']['image']   = $image;
									$qArr['contact']['formatted_addr'] = $formatted_addr;	
									$qArr['contact']['lat']     = $lat;
									$qArr['contact']['lng']     = $lng;
									$qArr['contact']['country'] = $country;	
									$qArr['contact']['postal']  = $postal;
									$qArr['contact']['state']   = $state;
									$qArr['contact']['street1'] = $street1;	
									$qArr['contact']['street2'] = $street2;
									$qArr['contact']['suburb']  = $suburb;	


										$qArr['date']        = $quotes->created_at;
										$qArr['description'] = $quotes->description;
										$qArr['note']        = $quotes->note;
										$qArr['price']       = $quotes->price;
										$qArr['quote_id']    = $quotes->quote_id;
										$qArr['status']      = $quotes->status;			
										$qArr['type']        = $quotes->type;


										if(isset($quotes->siteAddress) && !empty($quotes->siteAddress)){

											$qArr['site']['name']    = $name;
											$qArr['site']['phone']   = $phone;	
											$qArr['site']['email']   = $email;	

											$qArr['site']['formatted_addr'] = $quotes->siteAddress->formatted_address;	
											$qArr['site']['lat']     = $quotes->siteAddress->lat;
											$qArr['site']['lng']     = $quotes->siteAddress->lng;
											$qArr['site']['country'] = $quotes->siteAddress->country_id;
											$qArr['site']['postal']  = $quotes->siteAddress->postal_code;
											$qArr['site']['state']   = $quotes->siteAddress->state_id;
											$qArr['site']['street1'] = $quotes->siteAddress->street1;
											$qArr['site']['street2'] = $quotes->siteAddress->street2;
											$qArr['site']['suburb']  = $quotes->siteAddress->suburb;

										}else{

											$qArr['site']['name']    = $name;
											$qArr['site']['phone']   = $phone;	
											$qArr['site']['email']   = $email;											
											$qArr['site']['formatted_addr'] = $formatted_addr;	
											$qArr['site']['lat']     = $lat;
											$qArr['site']['lng']     = $lng;
											$qArr['site']['country'] = $country;	
											$qArr['site']['postal']  = $postal;
											$qArr['site']['state']   = $state;
											$qArr['site']['street1'] = $street1;	
											$qArr['site']['street2'] = $street2;
											$qArr['site']['suburb']  = $suburb;	

										}

											$qArr['counts']['jss'] = 10;
											$qArr['counts']['contacts'] = 15;
											$qArr['counts']['quotes'] = 8;
											$qArr['counts']['docs'] = 12;

									$finalArr[] = $qArr;
							}

						}else{
								$finalArr = [];
						}


					} //foreach

							$uArr['contact']['name'] = $name;
							$uArr['contact']['email'] = $email;
							$uArr['contact']['phone'] = $phone;
							$uArr['contact']['street1'] = $street1;
							$uArr['contact']['street2'] = $street2;
							$uArr['contact']['suburb'] = $suburb;
							$uArr['contact']['state'] = $state;
							$uArr['contact']['postal'] = $postal;
							$uArr['contact']['country'] = $country;
							$uArr['contact']['formatted_addr'] = $formatted_addr;
							$uArr['contact']['lat'] = $lat;
							$uArr['contact']['lng'] = $lng;
							$uArr['contact']['image'] = $image;
							$uArr['contact']['contact_id'] = $con_id;

							$uArr['counts']['jss'] = 10;
							$uArr['counts']['contacts'] = 15;
							$uArr['counts']['quotes'] = 8;
							$uArr['counts']['docs'] = 12;

							$uArr['quotes'] = $finalArr;				 
								
						          $response = [
						            'success' => '1',
						            'message' => 'Contacts Details!',
						            'data' => $uArr,
						          ];

	        }
	        // If the email is not found make a response like this
	        else{
	          $response = [
	            'success' => '0',
	            'message' => 'Subscriber doesnot exist in database!',
	            'data' => '',
	          ];
	        }	

	    }

	    ob_start();
	    echo json_encode($response);    	

    }//actionContactDetails


	public function actionCreateQuote()
    {

		$host_name = isset($_SERVER['HTTP_X_FORWARDED_HOST']) ? $_SERVER['HTTP_X_FORWARDED_HOST'] : $_SERVER['HTTP_HOST'];  				
    	$siteURL = 'http://' . $host_name . '/' . Yii::$app->request->baseUrl . '/uploads';	 

    	/* mandaory fields */
	    $cnt_id  = !empty(Yii::$app->request->post('contact_id'))?Yii::$app->request->post('contact_id'):'';
	    $sub_id  = !empty(Yii::$app->request->post('subscriber_id'))?Yii::$app->request->post('subscriber_id'):'';

	    /* optional fields */
	    $description      = !empty(Yii::$app->request->post('description'))?Yii::$app->request->post('description'):'';
	    $s_name           = !empty(Yii::$app->request->post('s_name'))?Yii::$app->request->post('s_name'):'';
	    $s_email          = !empty(Yii::$app->request->post('s_email'))?Yii::$app->request->post('s_email'):'';
	    $s_phone          = !empty(Yii::$app->request->post('s_phone'))?Yii::$app->request->post('s_phone'):'';
	    $s_street1        = !empty(Yii::$app->request->post('s_street1'))?Yii::$app->request->post('s_street1'):'';
	    $s_street2        = !empty(Yii::$app->request->post('s_street2'))?Yii::$app->request->post('s_street2'):'';
	    $s_suburb         = !empty(Yii::$app->request->post('s_suburb'))?Yii::$app->request->post('s_suburb'):'';
	    $s_state          = !empty(Yii::$app->request->post('s_state'))?Yii::$app->request->post('s_state'):'';
	    $s_postal         = !empty(Yii::$app->request->post('s_postal'))?Yii::$app->request->post('s_postal'):'';
	    $s_country        = !empty(Yii::$app->request->post('s_country'))?Yii::$app->request->post('s_country'):'';
	    $s_formatted_addr = !empty(Yii::$app->request->post('s_formatted_addr'))?Yii::$app->request->post('s_formatted_addr'):'';
	    $s_lat            = !empty(Yii::$app->request->post('s_lat'))?Yii::$app->request->post('s_lat'):'';
		$s_long           = !empty(Yii::$app->request->post('s_long'))?Yii::$app->request->post('s_long'):'';

		$type             = !empty(Yii::$app->request->post('type'))?Yii::$app->request->post('type'):'';

	    $response = [];

        $uArr = array();

	    // validate
	    if(empty($cnt_id) || empty($sub_id)){
	      $response = [
	        'success' => '0',
	        'message' => 'Fields cannot be blank!',
	        'data' => '',
	      ];
	    }
	    else{

	        // search in the database, there is no email referred
	        $subscriber = \common\models\User::findIdentity($sub_id);

	        if(!empty($subscriber)){

				date_default_timezone_set("UTC");
				$milliseconds = strtotime(gmdate("Y-m-d H:i:s"));
	        	/* insert in DB */

	        	$quote_id = '';

	        	if( (!empty($s_street1) && $s_street1 != '') || (!empty($s_street2) && $s_street2 != '') || (!empty($s_suburb) && $s_suburb != '')  || (!empty($s_state) && $s_state != '') || (!empty($s_postal) &&    $s_postal != '') || (!empty($s_country) && $s_country != '') || (!empty($s_formatted_addr) && $s_formatted_addr != '') || (!empty($s_lat) && $s_lat != '') || (!empty($s_long) && $s_long != '') ){

	        		$addressModel = new TblAddress();

			        $addressModel->street1           = $s_street1;
			        $addressModel->street2           = $s_street2;
			        $addressModel->suburb            = $s_suburb;
			        $addressModel->state_id          = $s_state;
			        $addressModel->country_id        = $s_country;
			        $addressModel->postal_code       = $s_postal;
			        $addressModel->formatted_address = $s_formatted_addr;
			        $addressModel->lat               = $s_lat;
			        $addressModel->lng               = $s_long;
			        $addressModel->created_at        = $milliseconds;
			        $addressModel->updated_at        = $milliseconds;

				        if($addressModel->save(false)){

				        	$addId = $addressModel->address_id;

					        	$quotesModel = new TblQuotes();

					        	$quotesModel->contact_id       = $cnt_id;
					        	$quotesModel->subscriber_id    = $sub_id;
					        	$quotesModel->description      = $description;
					        	$quotesModel->type             = $type;
					        	$quotesModel->site_address_id  = $addId;	
					        	$quotesModel->contact_email    = $s_email;
					        	$quotesModel->contact_name     = $s_name;
					        	$quotesModel->contact_number   = $s_phone;

						        $quotesModel->created_at       = $milliseconds;
						        $quotesModel->updated_at       = $milliseconds;

						        if($quotesModel->save(false)){

						        	$quote_id = $quotesModel->quote_id;

					        		$model = TblContacts::find()	        				   						
						        				->joinWith('address') 		        							
					    						->where(['contact_id' => $cnt_id])		    						
					    						->all();

										//echo "<pre>"; print_r($model); exit;

					    				foreach($model as $contact){
					    					
										    $name  = $contact['name'];
											$email = $contact['email'];
											$phone = $contact['phone'];
											$image = $siteURL . '/' . $contact['image'];

										    // get data from Address relation model
										    $street1        = $contact->address->street1;
										    $street2        = $contact->address->street2;
										    $suburb         = $contact->address->suburb;
										    $state          = $contact->address->state_id;
										    $postal         = $contact->address->postal_code;
										    $country        = $contact->address->country_id;
										    $formatted_addr = $contact->address->formatted_address;
										    $lat            = $contact->address->lat;
										    $lng            = $contact->address->lng;

					    					$uArr['quote_id']    = $quote_id;
					    					$uArr['description'] = $description;
					    					$uArr['status']      = 0;

					    					/* user details */
											$uArr['contact']['name']           = $name;
											$uArr['contact']['email']          = $email;
											$uArr['contact']['phone']          = $phone;
											$uArr['contact']['street1']        = $street1;
											$uArr['contact']['street2']        = $street2;
											$uArr['contact']['suburb']         = $suburb;
											$uArr['contact']['state']          = $state;
											$uArr['contact']['postal']         = $postal;
											$uArr['contact']['country']        = $country;
											$uArr['contact']['formatted_addr'] = $formatted_addr;
											$uArr['contact']['lat']            = $lat;
											$uArr['contact']['lng']           = $lng;
											$uArr['contact']['image']          = $image;

											/* site details */
											$uArr['site']['name']           = $s_name;
											$uArr['site']['email']          = $s_email;
											$uArr['site']['phone']          = $s_phone;
											$uArr['site']['street1']        = $s_street1;
											$uArr['site']['street2']        = $s_street2;
											$uArr['site']['suburb']         = $s_suburb;
											$uArr['site']['state']          = $s_state;
											$uArr['site']['postal']         = $s_postal;
											$uArr['site']['country']        = $s_country;
											$uArr['site']['formatted_addr'] = $s_formatted_addr;
											$uArr['site']['lat']            = $s_lat;
											$uArr['site']['lng']           = $s_long;
											
											/* counts */
											$uArr['counts']['jss']      = 10;
											$uArr['counts']['contacts'] = 15;
											$uArr['counts']['quotes']   = 8;
											$uArr['counts']['docs']     = 12;

					    				}//foreach

						        }
						}

		        	}else{

					        	$quotesModel = new TblQuotes();

					        	$quotesModel->contact_id          = $cnt_id;
					        	$quotesModel->subscriber_id       = $sub_id;
					        	$quotesModel->description         = $description;
					        	$quotesModel->type                = $type;
					        	$quotesModel->site_address_id     = "";	
					        	$quotesModel->contact_email       = $s_email;
					        	$quotesModel->contact_name        = $s_name;
					        	$quotesModel->contact_number      = $s_phone;

						        $quotesModel->created_at          = $milliseconds;
						        $quotesModel->updated_at          = $milliseconds;

						        if($quotesModel->save(false)){

					        		$quote_id = $quotesModel->quote_id;

						        		$model = TblContacts::find()	        				   						
							        				->joinWith('address') 		        							
						    						->where(['contact_id' => $cnt_id])
						    						->all();

											//echo "<pre>"; print_r($model); exit;

						    				foreach($model as $contact){
						    					
											    $name  = $contact['name'];
												$email = $contact['email'];
												$phone = $contact['phone'];
												$image = $siteURL . '/' . $contact['image'];

											    // get data from Address relation model
											    $street1        = $contact->address->street1;
											    $street2        = $contact->address->street2;
											    $suburb         = $contact->address->suburb;
											    $state          = $contact->address->state_id;
											    $postal         = $contact->address->postal_code;
											    $country        = $contact->address->country_id;
											    $formatted_addr = $contact->address->formatted_address;
											    $lat            = $contact->address->lat;
											    $lng            = $contact->address->lng;

						    					$uArr['quote_id']    = $quote_id;
						    					$uArr['description'] = $description;
						    					$uArr['status']      = 0;

						    					/* user contact */
												$uArr['contact']['name'] = $name;
												$uArr['contact']['email'] = $email;
												$uArr['contact']['phone'] = $phone;
												$uArr['contact']['street1'] = $street1;
												$uArr['contact']['street2'] = $street2;
												$uArr['contact']['suburb'] = $suburb;
												$uArr['contact']['state'] = $state;
												$uArr['contact']['postal'] = $postal;
												$uArr['contact']['country'] = $country;
												$uArr['contact']['formatted_addr'] = $formatted_addr;
												$uArr['contact']['lat'] = $lat;
												$uArr['contact']['lng'] = $lng;
												$uArr['contact']['image'] = $image;

												/* site contact */
												/*$uArr['site']['name'] = $s_name;
												$uArr['site']['email'] = $s_email;
												$uArr['site']['phone'] = $s_phone;
												$uArr['site']['street1'] = $street1;
												$uArr['site']['street2'] = $street2;
												$uArr['site']['suburb'] = $suburb;
												$uArr['site']['state'] = $state;
												$uArr['site']['postal'] = $postal;
												$uArr['site']['country'] = $country;
												$uArr['site']['formatted_addr'] = $formatted_addr;
												$uArr['site']['lat'] = $lat;
												$uArr['site']['lng'] = $lng;	*/		

												$uArr['site']['name'] = $name;
												$uArr['site']['email'] = $email;
												$uArr['site']['phone'] = $phone;
												$uArr['site']['street1'] = $street1;
												$uArr['site']['street2'] = $street2;
												$uArr['site']['suburb'] = $suburb;
												$uArr['site']['state'] = $state;
												$uArr['site']['postal'] = $postal;
												$uArr['site']['country'] = $country;
												$uArr['site']['formatted_addr'] = $formatted_addr;
												$uArr['site']['lat'] = $lat;
												$uArr['site']['lng'] = $lng;																						


												$uArr['counts']['jss']      = 10;
												$uArr['counts']['contacts'] = 15;
												$uArr['counts']['quotes']   = 8;
												$uArr['counts']['docs']     = 12;


						    				}//foreach
						        }

		        	}

				          $response = [
				            'success' => '1',
				            'message' => 'Quote successfully saved!',
				            'data' => $uArr,
				          ];

	        }
	        // If the email is not found make a response like this
	        else{
	          $response = [
	            'success' => '0',
	            'message' => 'Subscriber doesnot exist in database!',
	            'data' => '',
	          ];
	        }	

	  	}

	    ob_start();
	    echo json_encode($response);    

	} //actionCreateQuote

	public function actionCommunication(){

    	/* mandaory fields */
	    $sub_id  = !empty($_GET['subscriber_id'])?$_GET['subscriber_id']:'';
	    $cnt_id  = !empty($_GET['contact_id'])?$_GET['contact_id']:'';

	    //echo 'here-'.$sub_id; exit;    

	    $response = [];

	    $arr = array();

	    // validate
	    if(empty($sub_id)){
	      $response = [
	        'success' => '0',
	        'message' => 'Fields cannot be blank!',
	        'data' => '',
	      ];
	    }
	    else{

	        // search in the database, there is no email referred
	        $subscriber = \common\models\User::findIdentity($sub_id);

	        if(!empty($subscriber)){

	        	/* fetch communication data here */      	
				if(!empty($cnt_id) && $cnt_id != ''){

					$commModel = TblCommunications::find()			      		       
								->where(['subscriber_id' => $subscriber])
								->where(['contact_id' => $cnt_id])
								->all();				
				}else{
					$commModel = TblCommunications::find()			      		       
								->where(['subscriber_id' => $subscriber])
								->all();
				}


				if(count($commModel) > 0){
					//echo "<pre>"; print_r($commModel); exit;

					foreach($commModel as $communication){

						$arr['From']    = $communication->comm_from;
						$arr['date']    = (string)$communication->date;
						$arr['to']      = $communication->comm_to;
						$arr['bcc']     = $communication->bcc;
						$arr['subject'] = $communication->subject;
						$arr['body']    = $communication->body;

						$finalArr[] = $arr;

					} //foreach

					$commArr['emails'] = $finalArr;

				}else{
					$commArr['emails'] = [];					
				}

			      $response = [
			        'success' => '1',
			        'message' => 'Communication Listing!',
			        'data' => $commArr,
			      ];

			}
	        // If the email is not found make a response like this
	        else{
	          $response = [
	            'success' => '0',
	            'message' => 'Subscriber doesnot exist in database!',
	            'data' => '',
	          ];
	        }			

	    }

	    ob_start();
	    echo json_encode($response);    

	} //actionCommunication


	public function actionCreateAppointment(){ 

    	/* mandaory fields */
	    $cnt_id  = !empty(Yii::$app->request->post('contact_id'))?Yii::$app->request->post('contact_id'):'';
	    $sub_id  = !empty(Yii::$app->request->post('subscriber_id'))?Yii::$app->request->post('subscriber_id'):'';
	    $date    = !empty(Yii::$app->request->post('date'))?Yii::$app->request->post('date'):'';
	    $note    = !empty(Yii::$app->request->post('note'))?Yii::$app->request->post('note'):'';
	    $type    = !empty(Yii::$app->request->post('type'))?Yii::$app->request->post('type'):'';
	    $mem_id  = !empty(Yii::$app->request->post('member_id'))?Yii::$app->request->post('member_id'):'';
	    $all_day = !empty(Yii::$app->request->post('allDay'))?Yii::$app->request->post('allDay'):'';

	    /* optional fields */
	    $duration = !empty(Yii::$app->request->post('duration'))?Yii::$app->request->post('duration'):'';
	    $scope    = !empty(Yii::$app->request->post('scope'))?Yii::$app->request->post('scope'):'';
	    $quote_id = !empty(Yii::$app->request->post('quote_id'))?Yii::$app->request->post('quote_id'):'';
	    $street1  = !empty(Yii::$app->request->post('street1'))?Yii::$app->request->post('street1'):'';
	    $street2  = !empty(Yii::$app->request->post('street2'))?Yii::$app->request->post('street2'):'';
	    $suburb   = !empty(Yii::$app->request->post('suburb'))?Yii::$app->request->post('suburb'):'';
	    $state    = !empty(Yii::$app->request->post('state'))?Yii::$app->request->post('state'):'';
	    $postal   = !empty(Yii::$app->request->post('postal'))?Yii::$app->request->post('postal'):'';
	    $country  = !empty(Yii::$app->request->post('country'))?Yii::$app->request->post('country'):'';
	    $frm_addr = !empty(Yii::$app->request->post('formatted_addr'))?Yii::$app->request->post('formatted_addr'):'';
	    $lat      = !empty(Yii::$app->request->post('lat'))?Yii::$app->request->post('lat'):'';
	    $long     = !empty(Yii::$app->request->post('long'))?Yii::$app->request->post('long'):'';

	    $response = [];

        $appArr = array();

	    // validate
	    if(empty($cnt_id)){
	      $response = [
	        'success' => '0',
	        'message' => 'Contact id cannot be blank!',
	        'data' => '',
	      ];
	    }elseif(empty($sub_id)){
	      $response = [
	        'success' => '0',
	        'message' => 'Subscriber id cannot be blank!',
	        'data' => '',
	      ];
	    }elseif(empty($note)){
	      $response = [
	        'success' => '0',
	        'message' => 'Note cannot be blank!',
	        'data' => '',
	      ];	    	
	    }elseif(empty($type)){
	      $response = [
	        'success' => '0',
	        'message' => 'Type cannot be blank!',
	        'data' => '',
	      ];	    	
	    }elseif(empty($mem_id)){
	      $response = [
	        'success' => '0',
	        'message' => 'Member id cannot be blank!',
	        'data' => '',
	      ];	    	
	    }elseif(empty($date)){
	      $response = [
	        'success' => '0',
	        'message' => 'Date cannot be blank!',
	        'data' => '',
	      ];	    	
	    }
	    else{
	        // search in the database, there is no email referred
	        $subscriber = \common\models\User::findIdentity($sub_id);

	        if(!empty($subscriber)){


			date_default_timezone_set("UTC");
			$milliseconds = strtotime(gmdate("Y-m-d H:i:s"));


				if($all_day == 1){					
					$duration = 0;
				}else{

					$all_day = 0;
				}

	        	$appModel = new TblAppointments();

		        	$appModel->contact_id     = $cnt_id;
		        	$appModel->subscriber_id  = $sub_id;
		        	$appModel->quote_id       = $quote_id;
		        	$appModel->note           = $note;
		        	$appModel->street1        = $street1;	
		        	$appModel->street2        = $street2;
		        	$appModel->suburb         = $suburb;
		        	$appModel->state          = $state;
		        	$appModel->postal         = $postal;
		        	$appModel->country        = $country;
		        	$appModel->formatted_addr = $frm_addr;
		        	$appModel->lat            = $lat;
		        	$appModel->lng            = $long;			        	
		        	$appModel->date           = $date;
		        	$appModel->duration       = $duration;
		        	$appModel->type           = $type;
		        	$appModel->member_id      = $mem_id;  
		        	$appModel->scope          = $scope;      	
		        	$appModel->allDay         = $all_day;      
			        $appModel->created_at     = $milliseconds;
			        $appModel->updated_at     = $milliseconds;

			        if($appModel->save(false)){

			        	$app_id = $appModel->id;

				        	$appArr['allDay']     = $all_day;
				        	$appArr['id']         = $app_id;
				        	$appArr['date']       = $date;
				        	$appArr['duration']   = $duration;
				        	$appArr['quote_id']   = $quote_id;
				        	$appArr['note']       = $note;
				        	$appArr['location']   = $frm_addr;
				        	$appArr['type']       = $type;
				        	$appArr['member_id']  = $mem_id;
				        	$appArr['scope']      = $scope;
				        	$appArr['lat']        = $lat;
				        	$appArr['lng']        = $long;

								$response = [
									'success' => '1',
									'message' => 'Appointment saved successfully!',
									'data' => $appArr,
								];		        	
			        }

	        }else{

	          $response = [
	            'success' => '0',
	            'message' => 'Subscriber doesnot exist in database!',
	            'data' => '',
	          ];

	        }
	    }

	    ob_start();
	    echo json_encode($response);    

	} //actionCreateAppointment

	public function actionGetAppointmentInit(){

    	/* mandaory fields */
	    $sub_id  = !empty($_GET['subscriber_id'])?$_GET['subscriber_id']:'';

	    //echo 'here-'.$sub_id; exit;    

	    $response = [];

	    $memFinalArr = array();
	    $appFinalArr = array();

	    // validate
	    if(empty($sub_id)){
	      $response = [
	        'success' => '0',
	        'message' => 'Fields cannot be blank!',
	        'data' => '',
	      ];
	    }
	    else{

	        // search in the database, there is no email referred
	        $subscriber = \common\models\User::findIdentity($sub_id);

	        if(!empty($subscriber)){

				$memModel = TblMembers::find()
				        ->joinWith('subscriber')		
				        ->where(['tbl_members.subscriber_id' => '1'])		        	       
				        ->all();

				//echo "<pre>"; print_r($memModel); exit;

				$memArr = array();

				if(count($memModel) > 0){

					foreach($memModel as $model){

						$memArr['id']     = $model->id;
						$memArr['email']  = $model->email;
						$memArr['f_name'] = $model->f_name;
						$memArr['l_name'] = $model->l_name;
						$memArr['phone']  = $model->phone;

						$memFinalArr[] = $memArr;

					} //foreach

				} //memArr


				$appTypeModel = TblAppointmentType::find()->all();

				//echo "<pre>"; print_r($appTypeModel); exit;

				$apArr = array();			
				if(count($appTypeModel) > 0){
					
					foreach($appTypeModel as $appointment){
						$apArr['id']   = $appointment->id;
						$apArr['name'] = $appointment->name;

						$appFinalArr[] = $apArr;
					} //foreach
				}

					$finalArr['types']   = $appFinalArr; 
					$finalArr['members'] = $memFinalArr; 

					$response = [
						'success' => '1',
						'message' => 'Init Data',
						'data'    => $finalArr,
					];

	        }else{

	          $response = [
	            'success' => '0',
	            'message' => 'Subscriber doesnot exist in database!',
	            'data' => '',
	          ];

	        }
	    }

	    ob_start();
	    echo json_encode($response);    	    

	} //GetAppointmentInit

	public function actionAppointments(){

    	/* mandaory fields */
	    $sub_id  = !empty($_GET['subscriber_id'])?$_GET['subscriber_id']:'';

	    $cnt_id = !empty($_GET['conatct_id'])?$_GET['conatct_id']:'';
	    $month  = !empty($_GET['month'])?$_GET['month']:'';
	    $year   = !empty($_GET['year'])?$_GET['year']:'';

	    //echo 'here-'.$sub_id; exit;    

	    $response = [];
	    
	    $appFinalArr = array();

	    // validate
	    if(empty($sub_id)){
	      $response = [
	        'success' => '0',
	        'message' => 'Fields cannot be blank!',
	        'data' => '',
	      ];
	    }
	    else{

	        // search in the database, there is no email referred
	        $subscriber = \common\models\User::findIdentity($sub_id);

	        if(!empty($subscriber)){

        	$connection = \Yii::$app->db;

	        	if($month !='' && $year != ''){
	        	 $model = $connection->createCommand('SELECT t1.*, t2.`name` AS customerName FROM tbl_appointments as t1 LEFT JOIN `tbl_contacts` as t2 ON t1.`contact_id` = t2.`contact_id` WHERE MONTH(from_unixtime(t1.`date`)) = '.$month.' AND YEAR(from_unixtime(t1.`date`)) = '.$year.' AND 
	        	 	t1.`subscriber_id` ='.$sub_id.' ORDER by t1.`id` DESC');
	        	}else{
	        	 $model = $connection->createCommand('SELECT t1.*, t2.`name` AS customerName FROM tbl_appointments as t1 LEFT JOIN `tbl_contacts` as t2 ON t1.`contact_id` = t2.`contact_id` WHERE t1.`subscriber_id` ='.$sub_id.' ORDER by id DESC');
	        	}
				
				$users = $model->queryAll();

				//echo "<pre>"; print_r($users); exit;

					$apArr = array();			

					if(count($users) > 0){
						
						foreach($users as $_users){

								$apArr['allDay']       = $_users['allDay'];
								$apArr['id']           = $_users['id'];
								$apArr['date']         = $_users['date'];
								$apArr['duration']     = $_users['duration'];
								$apArr['quote_id']     = $_users['quote_id'];							
								$apArr['location']     = $_users['formatted_addr'];
								$apArr['note']         = $_users['note'];
								$apArr['lat']          = $_users['lat'];
								$apArr['lng']          = $_users['lng'];								
								$apArr['customerName'] = $_users['customerName'];

									$appType = TblAppointmentType::findOne($_users['type']);

										if($appType){
											$type = $appType->name;						
										}else{
											$type = '';
										}

										$apArr['type_id'] = $_users['type'];
										$apArr['type']    = $type;

									$member = TblMembers::findOne($_users['member_id']);

										if($member){
											$memberName = $member->f_name . ' ' . $member->l_name;						
										}else{
											$memberName = '';
										}

										$apArr['member_id']  = $_users['member_id'];
										$apArr['memberName'] = $memberName;
										$apArr['contact_id'] = $_users['contact_id'];

								$appFinalArr[] = $apArr;

						} //foreach
					}
						
						$finalArr['appointments'] = $appFinalArr; 

						$response = [
							'success' => '1',
							'message' => 'Appointment Data',
							'data'    => $finalArr,
						];

	        }else{

	          $response = [
	            'success' => '0',
	            'message' => 'Subscriber doesnot exist in database!',
	            'data' => '',
	          ];

	        }
	    }

	    ob_start();
	    echo json_encode($response);    	    

	} //actionAppointments


	public function actionUpdateContact(){

    	//echo "<pre>"; print_r($_FILES); exit;

		date_default_timezone_set("UTC");
		$milliseconds = strtotime(gmdate("Y-m-d H:i:s"));

		$host_name = isset($_SERVER['HTTP_X_FORWARDED_HOST']) ? $_SERVER['HTTP_X_FORWARDED_HOST'] : $_SERVER['HTTP_HOST'];  				
    	$siteURL = 'http://' . $host_name . '/' . Yii::$app->request->baseUrl . '/uploads';

    	/* mandaory fields */
    	$cnt_id  = !empty(Yii::$app->request->post('contact_id'))?Yii::$app->request->post('contact_id'):'';
	    $sub_id  = !empty(Yii::$app->request->post('subscriber_id'))?Yii::$app->request->post('subscriber_id'):'';
	    $name    = !empty(Yii::$app->request->post('name'))?Yii::$app->request->post('name'):'';
	    $email   = !empty(Yii::$app->request->post('email'))?Yii::$app->request->post('email'):'';
	    $phone   = !empty(Yii::$app->request->post('phone'))?Yii::$app->request->post('phone'):'';

	    /* optional fields */
	    $street1 = !empty(Yii::$app->request->post('street1'))?Yii::$app->request->post('street1'):'';
	    $street2 = !empty(Yii::$app->request->post('street2'))?Yii::$app->request->post('street2'):'';
	    $suburb  = !empty(Yii::$app->request->post('suburb'))?Yii::$app->request->post('suburb'):'';
	    $state   = !empty(Yii::$app->request->post('state'))?Yii::$app->request->post('state'):'';
		$postal  = !empty(Yii::$app->request->post('postal'))?Yii::$app->request->post('postal'):'';
		$country = !empty(Yii::$app->request->post('country'))?Yii::$app->request->post('country'):'';
		$frm_add = !empty(Yii::$app->request->post('formatted_addr'))?Yii::$app->request->post('formatted_addr'):'';
		$lat     = !empty(Yii::$app->request->post('lat'))?Yii::$app->request->post('lat'):'';
		$long    = !empty(Yii::$app->request->post('long'))?Yii::$app->request->post('long'):'';

		$uploads = UploadedFile::getInstancesByName("image");

	    $response = [];

	    $arr = array();

	    // validate
	    if(empty($cnt_id) || empty($sub_id) || empty($name) || empty($email) || empty($phone)){
	      $response = [
	        'success' => '0',
	        'message' => 'Fields cannot be blank!',
	        'data' => '',
	      ];
	    }
	    else{

	        // search in the database, there is no email referred
	        $user = \common\models\User::findIdentity($sub_id);

	        // if the email exists then
	        if(!empty($user)){

				date_default_timezone_set("UTC");
				$milliseconds = strtotime(gmdate("Y-m-d H:i:s"));

					$contact = TblContacts::findOne($cnt_id);

					if($contact){

						$addrId = $contact->address_id;

						$address = TblAddress::findOne($addrId);

							if($address){

								$address->street1           = $street1;
								$address->street2           = $street2;
								$address->suburb            = $suburb;
								$address->state_id          = $state;
								$address->postal_code       = $postal;
								$address->country_id        = $country;
								$address->formatted_address = $frm_add;
								$address->lat               = $lat;
								$address->lng               = $long;
								$address->updated_at        = $milliseconds;

								$address->save(false);

							}

								$contact->name       = $name;
								$contact->email      = $email;
								$contact->phone      = $phone;
								$contact->updated_at = $milliseconds;									

					        	$image = '';

					        	if(!empty($uploads)){

									foreach ($uploads as $file){					    		

								        if(isset($file->size)){		           
								            $varr = time().rand(1,100);
								            $file->saveAs('uploads/' . $file->baseName . '_' . $varr .'.' . $file->extension);
								        }

								        $image = $file->baseName . '_' . $varr .'.' . $file->extension;

									} //foreach

									$contact->image = $image;
					        	}

								$contact->save(false);		

								//echo "<pre>"; print_r($contact); exit;
								$contact->name = $name;
								$contact->save(false);

								if($image!=''){
									$image = $siteURL.'/'.$image;
								}

						    	$arr['contact_id']     = $cnt_id;
						    	$arr['name']           = $name;
						    	$arr['email']          = $email;
						    	$arr['phone']          = $phone;
						    	$arr['street1']        = $street1;
						    	$arr['street2']        = $street2;
						    	$arr['suburb']         = $suburb;
						    	$arr['state']          = $state;
						    	$arr['postal']         = $postal;
						    	$arr['country']        = $country;
						    	$arr['formatted_addr'] = $frm_add;
						    	$arr['lat']            = $lat;
						    	$arr['lng']            = $long;
						    	$arr['image']          = $image;

							      $response = [
							        'success' => '1',
							        'message' => 'Contact successfully updated!',
							        'data' => $arr,
							      ];

					}else{

			          $response = [
			            'success' => '0',
			            'message' => 'Contact doesnot exist in database!',
			            'data' => '',
			          ];	

					}

			}else{

	          $response = [
	            'success' => '0',
	            'message' => 'Subscriber doesnot exist in database!',
	            'data' => '',
	          ];				

			}

	    }

	    ob_start();
	    echo json_encode($response);    	    

	} //actionUpdateContact


	public function actionInvoices()
    {

    	/* mandaory fields */
	    $sub_id  = !empty($_GET['subscriber_id'])?$_GET['subscriber_id']:'';
	    $cnt_id  = !empty($_GET['contact_id'])?$_GET['contact_id']:'';

	    //echo 'here-'.$sub_id; exit;    

	    $response = [];

	    $arr = array();

	    // validate
	    if(empty($sub_id)){
	      $response = [
	        'success' => '0',
	        'message' => 'Fields cannot be blank!',
	        'data' => '',
	      ];
	    }
	    else{

	        // search in the database, there is no email referred
	        $subscriber = \common\models\User::findIdentity($sub_id);

	        if(!empty($subscriber)){

				/*$host_name = isset($_SERVER['HTTP_X_FORWARDED_HOST']) ? $_SERVER['HTTP_X_FORWARDED_HOST'] : $_SERVER['HTTP_HOST'];  				
	        	$siteURL = 'http://' . $host_name . '/' . Yii::$app->request->baseUrl . '/uploads';*/

	        	if($cnt_id != ''){

					$model = TblInvoices::find()	
					        ->where([ 'subscriber_id' => $sub_id])
					        ->andWhere(['contact_id'=>$cnt_id])
					        ->orderBy('invoice_id DESC')
					        ->all();

	        	}else{

					$model = TblInvoices::find()	
					        ->where([ 'subscriber_id' => $sub_id])					       
					        ->orderBy('invoice_id DESC')
					        ->all();
	        	}

					//echo "<pre>"; print_r($model); exit;        

					$uArr = array();

					$finalArr = array(); 

					if(count($model)>0){

						foreach($model as $invoice){
						    $inv_id      = $invoice->invoice_id;
							$quote_id    = $invoice->quote_id;
							$quote_note  = $invoice->quote_note;
							$description = $invoice ->description;
							$amount      = $invoice ->amount;
						    $sent        = $invoice->sent;
							$paid        = $invoice->paid;
							$date        = $invoice->date;
							$created_at  = $invoice->date;
							$updated_at  = $invoice->date;		

								$uArr['id']          = $inv_id;
								$uArr['quote_id']    = $quote_id;
								$uArr['quote_note']  = $quote_note;
								$uArr['description'] = $description;
								$uArr['amount']      = $amount;
								$uArr['sent']        = $sent;
								$uArr['paid']        = $paid;
								$uArr['date']        = $date;
								$uArr['created_at']  = $created_at;
								$uArr['updated_at']  = $updated_at;

									$finalArr['invoices'][] = $uArr;

						} //foreach

					}else{

						$finalArr['invoices'] = array();
					}

					$response = [
						'success' => '1',
						'message' => 'Invoice Details!',
						'data' => $finalArr,
					];

	        }
	        // If the email is not found make a response like this
	        else{
	          $response = [
	            'success' => '0',
	            'message' => 'Subscriber doesnot exist in database!',
	            'data' => '',
	          ];
	        }	

	    }

	    ob_start();
	    echo json_encode($response);    	

    }//actionInvoices

	public function actionUpdateAppointment(){

    	/* mandaory fields */
	    $app_id  = !empty(Yii::$app->request->post('appointment_id'))?Yii::$app->request->post('appointment_id'):'';
	    $cnt_id  = !empty(Yii::$app->request->post('contact_id'))?Yii::$app->request->post('contact_id'):'';
	    $sub_id  = !empty(Yii::$app->request->post('subscriber_id'))?Yii::$app->request->post('subscriber_id'):'';	    
	    $note    = !empty(Yii::$app->request->post('note'))?Yii::$app->request->post('note'):'';
	    $type    = !empty(Yii::$app->request->post('type'))?Yii::$app->request->post('type'):'';
	    $mem_id  = !empty(Yii::$app->request->post('member_id'))?Yii::$app->request->post('member_id'):'';
	    $all_day = !empty(Yii::$app->request->post('allDay'))?Yii::$app->request->post('allDay'):0;

	    /* optional fields */
	    $date     = !empty(Yii::$app->request->post('date'))?Yii::$app->request->post('date'):'';
	    $duration = !empty(Yii::$app->request->post('duration'))?Yii::$app->request->post('duration'):'';
	    $scope    = !empty(Yii::$app->request->post('scope'))?Yii::$app->request->post('scope'):'';
	    $quote_id = !empty(Yii::$app->request->post('quote_id'))?Yii::$app->request->post('quote_id'):'';
	    $street1  = !empty(Yii::$app->request->post('street1'))?Yii::$app->request->post('street1'):'';
	    $street2  = !empty(Yii::$app->request->post('street2'))?Yii::$app->request->post('street2'):'';
	    $suburb   = !empty(Yii::$app->request->post('suburb'))?Yii::$app->request->post('suburb'):'';
	    $state    = !empty(Yii::$app->request->post('state'))?Yii::$app->request->post('state'):'';
	    $postal   = !empty(Yii::$app->request->post('postal'))?Yii::$app->request->post('postal'):'';
	    $country  = !empty(Yii::$app->request->post('country'))?Yii::$app->request->post('country'):'';
	    $frm_addr = !empty(Yii::$app->request->post('formatted_addr'))?Yii::$app->request->post('formatted_addr'):'';
	    $lat      = !empty(Yii::$app->request->post('lat'))?Yii::$app->request->post('lat'):'';
	    $long     = !empty(Yii::$app->request->post('long'))?Yii::$app->request->post('long'):'';

	    $response = [];

        $appArr = array();

	    // validate
	    if(empty($app_id)){
			$response = [
				'success' => '0',
				'message' => 'Appointment id cannot be blank!',
				'data' => '',
			];
	    }elseif(empty($cnt_id)){
	      $response = [
	        'success' => '0',
	        'message' => 'Contact id cannot be blank!',
	        'data' => '',
	      ];
	    }elseif(empty($sub_id)){
	      $response = [
	        'success' => '0',
	        'message' => 'Subscriber id cannot be blank!',
	        'data' => '',
	      ];
	    }elseif(empty($note)){
	      $response = [
	        'success' => '0',
	        'message' => 'Note cannot be blank!',
	        'data' => '',
	      ];	    	
	    }elseif(empty($type)){
	      $response = [
	        'success' => '0',
	        'message' => 'Type cannot be blank!',
	        'data' => '',
	      ];	    	
	    }elseif(empty($mem_id)){
	      $response = [
	        'success' => '0',
	        'message' => 'Member id cannot be blank!',
	        'data' => '',
	      ];	    	
	    }elseif(empty($date)){
	      $response = [
	        'success' => '0',
	        'message' => 'Date cannot be blank!',
	        'data' => '',
	      ];	    	
	    }
	    else{

	        // search in the database, there is no email referred
	        $subscriber = \common\models\User::findIdentity($sub_id);

	        if(!empty($subscriber)){

				date_default_timezone_set("UTC");
				$milliseconds = strtotime(gmdate("Y-m-d H:i:s"));

				if($all_day == 1){					
					$duration = 0;
				}else{
					$all_day = 0;
				}

					$appointment = TblAppointments::findOne($app_id);

					if($appointment){

						//echo '<pre>'; print_r($appointment); exit;

			        	$appointment->contact_id     = $cnt_id;
			        	$appointment->subscriber_id  = $sub_id;
			        	$appointment->quote_id       = $quote_id;
			        	$appointment->note           = $note;
			        	$appointment->street1        = $street1;	
			        	$appointment->street2        = $street2;
			        	$appointment->suburb         = $suburb;
			        	$appointment->state          = $state;
			        	$appointment->postal         = $postal;
			        	$appointment->country        = $country;
			        	$appointment->formatted_addr = $frm_addr;
			        	$appointment->lat            = $lat;
			        	$appointment->lng            = $long;			        	
			        	$appointment->date           = $date;
			        	$appointment->duration       = $duration;
			        	$appointment->type           = $type;
			        	$appointment->member_id      = $mem_id;  
			        	$appointment->scope          = $scope; 
			        	$appointment->allDay         = $all_day;      					        
				        $appointment->updated_at     = $milliseconds;

				        	if($appointment->save(false)){

				        		$appArr['allDay']     = $all_day;
					        	$appArr['id']         = $app_id;
					        	$appArr['date']       = $date;
					        	$appArr['duration']   = $duration;
					        	$appArr['quote_id']   = $quote_id;
					        	$appArr['note']       = $note;
					        	$appArr['location']   = $frm_addr;
					        	$appArr['type']       = $type;
					        	$appArr['member_id']  = $mem_id;
					        	$appArr['scope']      = $scope;
					        	$appArr['lat']        = $lat;
					        	$appArr['lng']        = $long;

									$response = [
										'success' => '1',
										'message' => 'Appointment saved successfully!',
										'data' => $appArr,
									];		  
				        	}
					}

	        }else{

	          $response = [
	            'success' => '0',
	            'message' => 'Subscriber doesnot exist in database!',
	            'data' => '',
	          ];

	        }
	    }

	    ob_start();
	    echo json_encode($response);    

	} //actionUpdateAppointment

	public function actionQuotes()
    {

    	/* mandaory fields */
	    $sub_id  = !empty($_GET['subscriber_id'])?$_GET['subscriber_id']:'';
	    $con_id  = !empty($_GET['contact_id'])?$_GET['contact_id']:'';

	    $type   = !empty($_GET['type'])?$_GET['type']:'';
	    $status = !empty($_GET['status'])?$_GET['status']:'';

	    $from   = !empty($_GET['from'])?$_GET['from']:'';
	    $to     = !empty($_GET['to'])?$_GET['to']:'';

		$quoteId = !empty($_GET['quote_id'])?$_GET['quote_id']:'';

		$page   = isset($_GET['page'])?$_GET['page']:'';

	    //echo 'here-'.$sub_id; exit;    

	    $response = [];

	    $arr = array();

	    // validate
	    if(empty($sub_id)){
	      $response = [
	        'success' => '0',
	        'message' => 'Fields cannot be blank!',
	        'data' => '',
	      ];
	    }
	    else{

	        // search in the database, there is no email referred
	        $subscriber = \common\models\User::findIdentity($sub_id);

	        if(!empty($subscriber)){

				$host_name = isset($_SERVER['HTTP_X_FORWARDED_HOST']) ? $_SERVER['HTTP_X_FORWARDED_HOST'] : $_SERVER['HTTP_HOST'];  				
	        	$siteURL = 'http://' . $host_name . '/' . Yii::$app->request->baseUrl . '/uploads';


	        	/*if($con_id != ''){
					$model = TblQuotes::find()				
					        ->joinWith('contact')	      		       
					        ->where([ 'tbl_quotes.subscriber_id' => $sub_id])
					        ->andWhere(['tbl_quotes.contact_id'=>$con_id])				        
					        ->orderBy('quote_id DESC')
					        ->all();
	        	}else{
					$model = TblQuotes::find()				
					        ->joinWith('contact')	      		       
					        ->where([ 'tbl_quotes.subscriber_id' => $sub_id])					        
					        ->orderBy('quote_id DESC')
					        ->all();
	        	}*/

	        		$modelQuery = TblQuotes::find()->joinWith('contact')->joinWith('siteAddress')->where([ 'tbl_quotes.subscriber_id' => $sub_id]);

			        	if($con_id != '')	    				
						{
							$modelQuery->andWhere(['tbl_quotes.contact_id'=>$con_id]);
						}
			        	if($status)	    				
						{
							$statusIDs = array_map('intval', explode(',', $status));
								
								$modelQuery->andWhere(['in','tbl_quotes.status',$statusIDs]);
						}
			        	if($type)	    				
						{			
							$typeIDs = array_map('intval', explode(',', $type));		
								
								$modelQuery->andWhere(['in', 'tbl_quotes.type', $typeIDs]);
						}

			        	if($quoteId)
			        	{
							$modelQuery->andWhere(['tbl_quotes.quote_id'=>$quoteId]);
						}

						if( $from !='' && $to != ''){

							$modelQuery->andWhere('tbl_quotes.created_at>='.$from)->andWhere('tbl_quotes.created_at<='.$to);
						}

						/*---------- execute query below ---------*/

						$offset = ($page==0) ? 0 : (50*$page);

    						$model = $modelQuery->orderBy('quote_id DESC')->limit(50)->offset($offset)->all();


					//echo "<pre>"; print_r($model); exit;        


	        	if(count($model) > 0){

					$uArr = array(); 
					$cArr = array();

					foreach($model as $quotes){

					    $quoteId = $quotes->quote_id;
						$date    = $quotes->created_at;
						$type    = $quotes->type;
						$note    = $quotes->note;
						$price   = $quotes->price;
						$status  = $quotes->status;
						$description  = $quotes->description;


    					$finalArr['quote_id']    = $quoteId;
    					$finalArr['description'] = $description;
    					$finalArr['status']      = $status;
    					$finalArr['date']        = $date;
    					$finalArr['note']        = $note;
    					$finalArr['type']        = $type;
    					$finalArr['price']       = $price;



						if(isset($quotes->contact) && !empty($quotes->contact) ){								

							$addr = TblAddress::findOne($quotes->contact->address_id);

								$cArr['email'] = $quotes->contact->email;
								$cArr['name']  = $quotes->contact->name;
								$cArr['phone'] = $quotes->contact->phone;	
								$cArr['image'] = $siteURL.'/'.$quotes->contact->image;	
								$cArr['formatted_addr'] = $addr->formatted_address;	
								$cArr['lat']     = $addr->lat;
								$cArr['lng']     = $addr->lng;								
								$cArr['country'] = $addr->country_id;
								$cArr['postal']  = $addr->postal_code;
								$cArr['state']   = $addr->state_id;	
								$cArr['street1'] = $addr->street1;
								$cArr['street2'] = $addr->street2;
								$cArr['suburb']  = $addr->suburb;		
						}

						if(isset($quotes->siteAddress) && !empty($quotes->siteAddress) ){								
							$finalArr['site']['country'] = $quotes->siteAddress->country_id;
							$finalArr['site']['email']   = $quotes->contact->email;
							$finalArr['site']['formatted_addr'] = $quotes->siteAddress->formatted_address;	
							$finalArr['site']['lat']     = $quotes->siteAddress->lat;
							$finalArr['site']['lng']     = $quotes->siteAddress->lng;
							$finalArr['site']['name']    = $quotes->contact->name;	
							$finalArr['site']['phone']   = $quotes->contact->phone;
							$finalArr['site']['postal']  = $quotes->siteAddress->postal_code;
							$finalArr['site']['state']   = $quotes->siteAddress->state_id;	
							$finalArr['site']['street1'] = $quotes->siteAddress->street1;
							$finalArr['site']['street2'] = $quotes->siteAddress->street2;
							$finalArr['site']['suburb']  = $quotes->siteAddress->suburb;	

						}

						/*$finalArr['quote_id']    = $quoteId;
						$finalArr['date']        = $date;
						$finalArr['type']        = $type;
						$finalArr['note']        = $note;
						$finalArr['price']       = $price;
						$finalArr['status']      = $status;
						$finalArr['description'] = $description;*/

						$finalArr['contact']  = $cArr;

						$finalArr['counts']['jss']      = 10;
						$finalArr['counts']['contacts'] = 15;
						$finalArr['counts']['quotes']   = 8;
						$finalArr['counts']['docs']     = 12;


							$arr['quotes'][] = $finalArr;

					} //foreach						 

						$response = [
							'success' => '1',
							'message' => 'Contacts Details!',
							'data' => $arr,
						];
	        	}else{

	        		    $arr['quotes'] = array();

	        			$response = [
							'success' => '1',
							'message' => 'Contacts Details!',
							'data' => $arr,
						];
	        	}


	        }
	        // If the email is not found make a response like this
	        else{
	          $response = [
	            'success' => '0',
	            'message' => 'Subscriber doesnot exist in database!',
	            'data' => '',
	          ];
	        }	

	    }

	    ob_start();
	    echo json_encode($response);    	

    }//actionQuotes


	public function actionBrands()
    {
    	$data = array();
    		

		$brandModel = TblBrands::find()
						->joinWith('tblTiers')
		                ->all();

		if(count($brandModel)>0){

			//echo "<pre>"; print_r($brandModel); exit;

			$tArr = array();

			foreach($brandModel as $model){

				$arr['brand_id'] =  $model->brand_id;
				$arr['name']     =  $model->name;
				$arr['image']    =  $model->logo;						

					//echo "<pre>"; print_r($model->tblTiers[1]); exit;

					$tFinArr = array();

					if(count($model->tblTiers) > 0){			

						foreach($model->tblTiers as $tiermodel){

							$tArr['tier_id'] = $tiermodel->tier_id;
							$tArr['name']    = $tiermodel->name;
							$tArr['image']   = $model->logo;

							$tFinArr[] = $tArr;
						} //foreach

					}

					$arr['tiers']    =  $tFinArr;

					$fArr[] = $arr;		
					//$fArr[] = $tFinArr;		

			}//foreach

			$strModel = TblStrengths::find()
							->select(['strength_id', 'name'])
			              	->all();

				if(count($strModel)>0){

					//echo "<pre>"; print_r($brandModel); exit;

					foreach($strModel as $smodel){

						$sArr['id']   = $smodel->strength_id;
						$sArr['name'] = $smodel->name;

						$finalArr[] = $sArr;

					}
				}else{
					$finalArr[] = array();

				}

				//coats
				$coatsArr[] = 'one';
				$coatsArr[] = 'two';
				$coatsArr[] = 'three';

				//prep
				$prepArr[] = 'test';
				$prepArr[] = 'premium';

					$data['brands']        = $fArr;				
					$data['tint_strength'] = $finalArr;
					$data['coats']         = $coatsArr;
					$data['prep']          = $prepArr;

					
					$response = [
						'success' => '1',
						'message' => 'Brands and Strength data!',
						'data' => $data,
					];

		}else{
	          $response = [
	            'success' => '0',
	            'message' => 'There are no brands exist in database!',
	            'data' => '',
	          ];

		}

	    ob_start();
	    echo json_encode($response);    	

    }//actionBrands


	public function actionTiers()
    {
    	/* mandaory fields */
	    $brand_id = !empty($_GET['brand_id'])?$_GET['brand_id']:'';
	    $type_id  = !empty($_GET['type_id'])?$_GET['type_id']:'';

	    $response = [];
	    $arr      = array();
	    $data     = array();

	    // validate
	    if(empty($brand_id) && empty($type_id)){
	      $response = [
	        'success' => '0',
	        'message' => 'Fields cannot be blank!',
	        'data' => '',
	      ];
	    }
	    else{

			$tierModel = TblTiers::find()
							->joinWith('brand')
					        ->where(['tbl_tiers.brand_id' => $brand_id])
					        ->andWhere(['tbl_tiers.type_id'=>$type_id])				        
					        ->orderBy('tier_id DESC')
					        ->all();

			if(count($tierModel)>0){

				//echo "<pre>"; print_r($tierModel); exit;

				foreach($tierModel as $model){

					$arr['tier_id'] =  $model->brand_id;
					$arr['name']    =  $model->name;
					$arr['image']   =  $model->brand->logo;

					$fArr[] = $arr;

				}
					$data['tiers'] = $fArr;

				$response = [
					'success' => '1',
					'message' => 'Tiers data!',
					'data' => $data,
				];

			}else{
		          $response = [
		            'success' => '0',
		            'message' => 'There are no tiers exist in database!',
		            'data' => '',
		          ];

			}
		}

	    ob_start();
	    echo json_encode($response);    	

    }//actionTiers



	public function actionTiersComponent()
    {

	    $response = [];
	    $arr      = array();
	    $data     = array();

    	/* mandaory fields */
	    $tier_id = !empty($_GET['tier_id'])?$_GET['tier_id']:'';
	    $type_id = isset($_GET['type_id'])?$_GET['type_id']:'';
	   
	    // validate
	    if(empty($tier_id) || $type_id==''){
	      $response = [
	        'success' => '0',
	        'message' => 'Fields cannot be blank!',
	        'data' => '',
	      ];
	    }else{    	
			
			$grpModels = TblComponentGroups::find()
						->joinWith(
								['tblTierCoats' => 
									function($q) use ($tier_id){
										$q->where(['tbl_tier_coats.tier_id' => $tier_id]);
									}
								])
						->joinWith(['tblTierCoats.sheen','tblTierCoats.product'])
						->where(['in', ['tbl_component_groups.type_id'], [$type_id,3]])
						->andWhere(['tbl_component_groups.enabled' => 1])
						->all();

			//echo '<pre>'; print_r($grpModels); exit;

			$finalArr = array();
			if(count($grpModels) > 0){

				foreach($grpModels as $model){

					$dummy = array();
					
					//echo '<pre>'; print_r($top_coat);

					$arr['id']                  = $model->group_id;
					$arr['name']                = $model->name;

					if(isset($model->tblTierCoats) && count($model->tblTierCoats)>0){

						$i = 0;
						$top_coat =  array();
						$under_coat =  array();
						$sFinalArr = array();
						$sArr = array();
						$myArr = array();
						foreach($model->tblTierCoats as $sheendata){

							if($sheendata->top_coats == 1){
								$top_coat['name'] = $sheendata->product->name;
								$top_coat['product_id'] = $sheendata->product->product_id;
								$top_coat['default'] = $sheendata->enabled;
								$myArr[$sheendata->sheen_id]['sheen_name'] = $sheendata->sheen->name;
								$myArr[$sheendata->sheen_id]['top_coats'][] = $top_coat;
							}
							else{
								$unders['name'] = $sheendata->product->name;
								$unders['product_id'] = $sheendata->product->product_id;
								$unders['default'] = $sheendata->enabled;
								$under_coat[] = $unders;
							}

						} //foreach

						foreach($myArr as $sheen_id => $mySheen){
							$sArr['sheen_id'] = $sheen_id;
							$sArr['name'] = $mySheen['sheen_name'];
							$sArr['top_coats'] = $mySheen['top_coats'];
							$sFinalArr[] = $sArr;
						}


					}else{

						$sFinalArr = array();
					}
					$arr['sheens'] = $sFinalArr;
					$arr['under_coat'] = $under_coat;
					//echo '<pre>'; print_r($myArr); exit;

					$finalArr[] = $arr;

				} //foreach
				//echo '<pre>'; print_r($finalArr); exit;

			} //if

				$data['tier_types'] = $finalArr;

				$response = [
					'success' => '1',
					'message' => 'Component array!',
					'data' => $data,
				];

	    }

	    ob_start();
	    echo json_encode($response);    	

	} //actionTiersComponent


	public function actionProductColors()
    {

	    $response = [];
	    $arr      = array();
	    
    	/* mandaory fields */
	    $page      = isset($_GET['page'])?$_GET['page']:'';
	    $tag_id    = !empty($_GET['tag_id'])?$_GET['tag_id']:'';
	    $searchVal = !empty($_GET['search'])?$_GET['search']:'';


	    // validate
	    if($page==''){
	      $response = [
	        'success' => '0',
	        'message' => 'Page cannot be blank!',
	        'data' => '',
	      ];
	    }else{    	

	    	if($tag_id !='' && $searchVal!=''){
	    		
	    		$offset = ($page==0) ? 0 : (50*$page);

				$colorsModel = TblColorTags::find()
								->joinWith(['tblColors'=>function($q) use($offset,$searchVal){ $q->andFilterWhere(['like', 'tbl_colors.name', $searchVal])->orderBy('tag_id ASC')->limit(50)->offset($offset);}]) 
								->where(['not in', ['tbl_colors.tag_id'], [1]])
								->andWhere(['tbl_colors.tag_id'=>$tag_id])														
								->all();	

	    	}elseif($tag_id !='' ){
	    		
	    		$offset = ($page==0) ? 0 : (50*$page);

				$colorsModel = TblColorTags::find()								
								->joinWith(['tblColors' => function($q) use ($offset){ $q->orderBy('tag_id ASC')->limit(50)->offset($offset);}])
								->where(['not in', ['tbl_colors.tag_id'], [1]])
								->andWhere(['tbl_colors.tag_id'=>$tag_id])														
								->all();	

	    	}else{

	    		$offset = ($page==0) ? 0 : (50*$page);

				$colorsModel = TblColorTags::find()
						->joinWith(['tblColors' => function($q) use ($page,$searchVal,$offset){ $q->andFilterWhere(['like', 'tbl_colors.name', $searchVal])->orderBy('tag_id ASC')->limit(50)->offset($offset);}])        		       
						->where(['not in', ['tbl_colors.tag_id'], [1]])							
						->all();	
	    	}
		

			if(count($colorsModel)>0){

				//echo "<pre>"; print_r($colorsModel); exit;	

				$finalArr['colors'] = array();				

				foreach($colorsModel as $model){

					$tagId       = $model->tag_id;
					$tagName     = $model->name;

					$colorId     = '';
					$colorName   = '';
					$colorHex    = '';
					$colorNumber = '';

					if(isset($model->tblColors) && count($model->tblColors)>0){

						foreach($model->tblColors as $colors){

							$colorId     = $colors->color_id;
							$colorName   = $colors->name;
							$colorHex    = $colors->hex;
							$colorNumber = $colors->number;

								$arr['id']       = $colorId;
								$arr['name']     = $colorName;
								$arr['hex']      = $colorHex;
								$arr['number']   = $colorNumber;
								$arr['tag_id']   = $tagId;
								$arr['tag_name'] = $tagName;	

							$finalArr['colors'][] = $arr;	

						}//foreach

					}

				} //foreach

				//echo "<pre>"; print_r($data); exit;

					$response = [
						'success' => '1',
						'message' => 'Colors data!',
						'data' => $finalArr,
					];

			}else{

				$finalArr['colors'] = array();
				$response = [
					'success' => '0',
					'message' => 'No data found!',
					'data' => $finalArr,
				];
			}//else

	    }

	    ob_start();
	    echo json_encode($response);    	

	} //actionProductColors

	public function actionComponents(){

    	/* mandaory fields */
	    $type_id = !empty($_GET['type_id'])?$_GET['type_id']:'';

	    $response = [];	    
	    $arr      = array();
	    $myArr = array();

	    // validate
	    if(empty($type_id)){
	      $response = [
	        'success' => '0',
	        'message' => 'Type id cannot be blank!',
	        'data' => '',
	      ];
	    }
	    else{

        	$connection = \Yii::$app->db;

	        	$model = $connection->createCommand('Select tct.comp_type_id, tct.comp_id, tct.name AS typename, tct.work_rate, tct.spread_ratio, tct.is_default, tct.thickness,tct.excl_area, tc.name,tc.price_method_id, tc.calc_method_id, tcg.enabled, tcg.type_id from tbl_components as tc, tbl_component_groups as tcg, tbl_component_type as tct where tcg.type_id='.$type_id.' AND tcg.group_id=tc.group_id AND tct.comp_id=tc.comp_id AND tcg.enabled=1 ORDER BY tct.comp_id');
				
				$components = $model->queryAll();

				if(count($components)>0){
					//echo "<pre>"; print_r($components); exit;

					$cArr      = array();
					$compArr   = array();
					$cFinalArr = array();

						foreach($components as $_comp){
							//echo $_comp['name'].'</br>';n

							$cArr['id']           = $_comp['comp_type_id'];
							$cArr['name']         = $_comp['typename'];
							$cArr['work_rate']    = $_comp['work_rate'];
							$cArr['spread_ratio'] = $_comp['spread_ratio'];
							$cArr['default']      = $_comp['is_default'];

							$myArr[$_comp['comp_id']]['id']              = $_comp['comp_id'];
							$myArr[$_comp['comp_id']]['comp_name']       = $_comp['name'];
							$myArr[$_comp['comp_id']]['price_method_id'] = $_comp['price_method_id'];
							$myArr[$_comp['comp_id']]['calc_method_id']  = $_comp['calc_method_id'];
							$myArr[$_comp['comp_id']]['options'][]       = $cArr;

						}//foreach

						foreach($myArr as $comp_id => $myComp){
							$compArr['id']              = $comp_id;
							$compArr['comp_name']       = $myComp['comp_name'];
							$compArr['price_method_id'] = $myComp['price_method_id'];
							$compArr['calc_method_id']  = $myComp['calc_method_id'];
							$compArr['options'] = $myComp['options'];

							$cFinalArr['components'][] = $compArr;
						} //foreach


						//echo "<pre>"; print_r($myArr); exit;

						$response = [
							'success' => '1',
							'message' => 'Component data!',
							'data' => $cFinalArr,
						];

				}else{

					$cFinalArr['components'] = array();;
					$response = [
						'success' => '0',
						'message' => 'No Component found!',
						'data'    => $cFinalArr,
					];

				}
		}

	    ob_start();
	    echo json_encode($response);  

	}//actionComponents


	public function actionSpecialItems(){

		$response = [];
		$arr      = array();

		$specialModel = TblSpecialItems::find()
						 	    ->all();

		if(count($specialModel)>0){
			//echo "<pre>"; print_r($specialModel); exit;

			foreach($specialModel as $_model){

				$arr['id']    = $_model->item_id;
				$arr['name']  = $_model->name;
				$arr['price'] = $_model->price;

					$fArr['items'][] = $arr;
			}

			$response = [
				'success' => '1',
				'message' => 'Items data!',
				'data'    => $fArr,
			];

		}else{

			$fArr['items'] = array();
			$response = [
				'success' => '0',
				'message' => 'No Items found!',
				'data'    => $fArr,
			];
		}		 	            
		
	    ob_start();
	    echo json_encode($response);  

	}//actionSpecialItems

	public function actionRooms(){

    	/* mandaory fields */
	    $type_id = !empty($_GET['type_id'])?$_GET['type_id']:'';

	    $response = [];	    
	    $arr      = array();
	    
	    // validate
	    if(empty($type_id)){
	      $response = [
	        'success' => '0',
	        'message' => 'Type id cannot be blank!',
	        'data' => '',
	      ];
	    }
	    else{

			$roomsModel = TblRoomTypes::find()
									->where(['type_id' => $type_id])
							 	    ->all();

			if(count($roomsModel)>0){
				//echo "<pre>"; print_r($roomsModel); exit;

				foreach($roomsModel as $_model){

					$arr['id']       = $_model->room_id;
					$arr['name']     = $_model->name;
					$arr['multiple'] = $_model->multiples;

						$fArr['rooms'][] = $arr;
				}

				$response = [
					'success' => '1',
					'message' => 'Rooms data!',
					'data'    => $fArr,
				];

			}else{

				$fArr['rooms'] = array();
				$response = [
					'success' => '0',
					'message' => 'No Rooms found!',
					'data'    => $fArr,
				];
			}		 	      
		}      
		
	    ob_start();
	    echo json_encode($response);  

	}//actionRooms



	public function actionTags(){

		$response = [];

		$arr = array();

		$tagsModel = TblColorTags::find()
						->where(['not in', ['tbl_color_tags.tag_id'], [1]])															
						->all();	

		if(count($tagsModel)>0){

			//echo "<pre>"; print_r($tagsModel);

			foreach($tagsModel as $model){

				$arr['tag_id'] = $model->tag_id;
				$arr['name']   = $model->name;

				$tArr['tags'][] = $arr;
			}

			$response = [
				'success' => '1',
				'message' => 'Tags found!',
				'data'    => $tArr,
			];


		}else{

			$tArr['tags'] = array();
			$response = [
				'success' => '0',
				'message' => 'No tags found!',
				'data'    => $tArr,
			];

		}

	    ob_start();
	    echo json_encode($response);  

	}//actionTags

}
