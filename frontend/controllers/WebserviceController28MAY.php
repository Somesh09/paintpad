<?php

namespace frontend\controllers;

use Yii;
use app\models\UserDetails;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\filters\auth\QueryParamAuth;
use common\models\LoginForm;

use common\models\SignupForm;

use yii\base\Model;
use common\models\User;
use yii\web\UploadedFile;

use app\models\TblUsers;
use app\models\TblUsersDetails;
use app\models\TblContacts;
use app\models\TblAddress;
use app\models\TblQuotes;
use app\models\TblCommunications;

class WebserviceController extends Controller
{

    /**
     * @inheritdoc
     */

    public function behaviors(){

		return [
		    'verbs' => [
		        'class' => VerbFilter::className(),
		        'actions' => [
		            'login'  => ['POST'],
		            'create-contact'  => ['POST'],
		            'contacts'  => ['GET'],
		            'contact-details'  => ['GET'],	
		            'create-quote'  => ['POST'],
		            'communication'  => ['GET'],		                       

		        ],
		    ],
		    /*'authenticator' =>[
		    	'class'=> QueryParamAuth::className(),
		    ],*/
		];

    }

    public function actionLogin()
    {
	    //echo "<pre>"; print_r($_POST); exit;

	    $email = !empty(Yii::$app->request->post('email'))?Yii::$app->request->post('email'):'';
	    $password = !empty(Yii::$app->request->post('pwd'))?Yii::$app->request->post('pwd'):'';
	    $response = [];

	    // validate
	    if(empty($email) || empty($password)){
	      $response = [
	        'success' => '0',
	        'message' => 'Email & Password cannot be blank!',
	        'data' => '',
	      ];
	    }
	    else{
	        // search in the database, there is no email referred
	        $user = \common\models\User::findByEmail($email);
	        // if the email exists then
	        if(!empty($user)){
	          // check, valid not password, if valid then make response success
	          if($user->validatePassword($password)){

				$model = TblUsersDetails::find()
				        ->joinWith('user')
				        ->joinWith('address')		        		       
				        ->where([ `TblUsers`.'email' => $email])
				        ->all();

				//echo "<pre>"; print_r($model); exit;       

				$uArr = array(); 

				$arr = array('setting'=>(object) array());

				foreach ($model as $user) {

					$uArr['subscriber_id'] = $user['user_id'];
					$uArr['name']          = $user['name'];
					$uArr['image']         = $user['logo'];

				    // get data from User relation model
				    $email = $user->user->email;

				    // get data from Address relation model
				    $street1 = $user->address->street1;
				    $street2 = $user->address->street2;
				    $suburb  = $user->address->suburb;


				    $uArr['contact']['email']   = $email;
					$uArr['contact']['link']    = $user['website_link'];
					$uArr['contact']['phone']   = $user['phone'];
					$uArr['contact']['address'] = $street1 . ' ' . $street2 . ' ' . $suburb;

					$uArr['counts']['jss']      = 10;
					$uArr['counts']['contacts'] = 15;
					$uArr['counts']['quotes']   = 8;
					$uArr['counts']['docs']     = 12;

					$uArr['settings']           = (object) array();
					$uArr['prep']               = (object) array();
					$uArr['spl_items']          = (object) array();


				}

				$response = [
					'success' => '1',
					'message' => 'Login successful!',
					'data' => $uArr,
				];


	          }
	          // If the password is wrong then make a response like this
	          else{
	            $response = [
	              'success' => '0',
	              'message' => 'Password incorrect!',
	              'data' => '',
	            ];
	          }
	        }
	        // If the email is not found make a response like this
	        else{
	          $response = [
	            'success' => '0',
	            'message' => 'Email doesnot exist in database!',
	            'data' => '',
	          ];
	        }
	    }
	    //return $response;
	    ob_start();
	    echo json_encode($response);
	}


	public function actionCreateContact()
    {
    	/* mandaory fields */
	    $sub_id  = !empty(Yii::$app->request->post('subscriber_id'))?Yii::$app->request->post('subscriber_id'):'';
	    $name    = !empty(Yii::$app->request->post('name'))?Yii::$app->request->post('name'):'';
	    $email   = !empty(Yii::$app->request->post('email'))?Yii::$app->request->post('email'):'';
	    $phone   = !empty(Yii::$app->request->post('phone'))?Yii::$app->request->post('phone'):'';

	    /* optional fields */
	    $street1 = !empty(Yii::$app->request->post('street1'))?Yii::$app->request->post('street1'):'';
	    $street2 = !empty(Yii::$app->request->post('street2'))?Yii::$app->request->post('street2'):'';
	    $suburb  = !empty(Yii::$app->request->post('suburb'))?Yii::$app->request->post('suburb'):'';
	    $state   = !empty(Yii::$app->request->post('state'))?Yii::$app->request->post('state'):'';
		$postal  = !empty(Yii::$app->request->post('postal'))?Yii::$app->request->post('postal'):'';
		$country = !empty(Yii::$app->request->post('country'))?Yii::$app->request->post('country'):'';
		$frm_add = !empty(Yii::$app->request->post('frm_add'))?Yii::$app->request->post('frm_add'):'';
		$lat     = !empty(Yii::$app->request->post('lat'))?Yii::$app->request->post('lat'):'';
		$long    = !empty(Yii::$app->request->post('long'))?Yii::$app->request->post('long'):'';

	    $response = [];

	    $arr = array();

	    // validate
	    if(empty($sub_id) || empty($name) || empty($email) || empty($phone)){
	      $response = [
	        'success' => '0',
	        'message' => 'Fields cannot be blank!',
	        'data' => '',
	      ];
	    }
	    else{

	        // search in the database, there is no email referred
	        $user = \common\models\User::findIdentity($sub_id);

	        // if the email exists then
	        if(!empty($user)){

			date_default_timezone_set("UTC");
			$milliseconds = strtotime(gmdate("Y-m-d H:i:s"));

		    	$addressModel = new TblAddress();
				$contactModel = new TblContacts();


				$contact = $contactModel::findByEmail($email);


				if($contact){
					$response = [
						'success' => '0',
						'message' => 'Email already exist',
						'data' => '',
					];
				}else{

				/* Insert data in Address table */

			        $addressModel->street1           = $street1;
			        $addressModel->street2           = $street2;
			        $addressModel->suburb            = $suburb;
			        $addressModel->state_id          = $state;
			        $addressModel->country_id        = $country;
			        $addressModel->postal_code       = $postal;
			        $addressModel->formatted_address = $frm_add;
			        $addressModel->lat               = $lat;
			        $addressModel->lng               = $long;
			        $addressModel->created_at        = $milliseconds;
			        $addressModel->updated_at        = $milliseconds;


			        if($addressModel->save(false)){

			        	$addId = $addressModel->address_id;

						/* Insert data in Contact table */	

			        		$contactModel->subscriber_id = $sub_id;
			        		$contactModel->name          = $name;
			        		$contactModel->email         = $email;
			        		$contactModel->phone         = $phone;
			        		$contactModel->image         = '';
			        		$contactModel->address_id    = $addId;
			        		$contactModel->created_at    = $milliseconds;
			        		$contactModel->updated_at    = $milliseconds;

			        		if($contactModel->save(false)){

								$contactId = $contactModel->contact_id;

							    	$arr['contact_id']     = $contactId;
							    	$arr['name']           = $name;
							    	$arr['email']          = $email;
							    	$arr['phone']          = $phone;
							    	$arr['street1']        = $street1;
							    	$arr['street2']        = $street2;
							    	$arr['suburb']         = $suburb;
							    	$arr['state']          = $state;
							    	$arr['postal']         = $postal;
							    	$arr['country']        = $country;
							    	$arr['formatted_addr'] = $frm_add;
							    	$arr['lat']            = $lat;
							    	$arr['lng']           = $long;
							    	$arr['image']          = "";

								      $response = [
								        'success' => '1',
								        'message' => 'Contact data saved successfully!',
								        'data' => $arr,
								      ];

							}
			        }			

				}

	        }
	        // If the email is not found make a response like this
	        else{
	          $response = [
	            'success' => '0',
	            'message' => 'Subscriber doesnot exist in database!',
	            'data' => '',
	          ];
	        }

	    }

	    ob_start();
	    echo json_encode($response);    	

    }//actionCreateContact


	public function actionContacts()
    {

    	/* mandaory fields */
	    $sub_id  = !empty($_GET['subscriber_id'])?$_GET['subscriber_id']:'';

	    //echo 'here-'.$sub_id; exit;    

	    $response = [];

	    $arr = array();

	    // validate
	    if(empty($sub_id)){
	      $response = [
	        'success' => '0',
	        'message' => 'Fields cannot be blank!',
	        'data' => '',
	      ];
	    }
	    else{

	        // search in the database, there is no email referred
	        $subscriber = \common\models\User::findIdentity($sub_id);

	        if(!empty($subscriber)){


				$host_name = isset($_SERVER['HTTP_X_FORWARDED_HOST']) ? $_SERVER['HTTP_X_FORWARDED_HOST'] : $_SERVER['HTTP_HOST'];  				
	        	$siteURL = 'http://' . $host_name . '/' . Yii::$app->request->baseUrl . '/uploads';	        	

				$model = TblContacts::find()
				        ->joinWith('address')       		       
				        ->where([ 'subscriber_id' => $sub_id])
				        ->all();

				//echo "<pre>"; print_r($model); exit;        

				$newarr = array();

				foreach($model as $result){

					$newarr['contact_id'] = $result->contact_id;
					$newarr['name']       = $result->name;
					$newarr['phone']      = $result->phone;
					$newarr['image']      = $siteURL . '/' . $result->image;
					$newarr['email']      = $result->email;

						if(isset($result->address)){

						    // get data from Address relation model
						    $street1        = $result->address->street1;
						    $street2        = $result->address->street2;
						    $suburb         = $result->address->suburb;
						    $state          = $result->address->state_id;
						    $postal         = $result->address->postal_code;
						    $country        = $result->address->country_id;
						    $formatted_addr = $result->address->formatted_address;
						    $lat            = $result->address->lat;
						    $lng            = $result->address->lng;

						 $newarr['address'] = $street1 . ' ' . $street2 . ' ' . $suburb . ' ' . $state . ' ' . $postal . '' . $country;   

						}else{

						 $newarr['address'] = "";   

						}



					 $arr['contact'][] = $newarr;
				}	
				 
				
		          $response = [
		            'success' => '1',
		            'message' => 'Contacts Data!',
		            'data' => $arr,
		          ];

	        }
	        // If the email is not found make a response like this
	        else{
	          $response = [
	            'success' => '0',
	            'message' => 'Subscriber doesnot exist in database!',
	            'data' => '',
	          ];
	        }	

	    }

	    ob_start();
	    echo json_encode($response);    	

    }//actionContacts


	public function actionContactDetails()
    {

    	/* mandaory fields */
	    $sub_id  = !empty($_GET['subscriber_id'])?$_GET['subscriber_id']:'';
	    $con_id  = !empty($_GET['contact_id'])?$_GET['contact_id']:'';

	    //echo 'here-'.$sub_id; exit;    

	    $response = [];

	    $arr = array();

	    // validate
	    if(empty($sub_id) || empty($con_id)){
	      $response = [
	        'success' => '0',
	        'message' => 'Fields cannot be blank!',
	        'data' => '',
	      ];
	    }
	    else{

	        // search in the database, there is no email referred
	        $subscriber = \common\models\User::findIdentity($sub_id);

	        if(!empty($subscriber)){

				$host_name = isset($_SERVER['HTTP_X_FORWARDED_HOST']) ? $_SERVER['HTTP_X_FORWARDED_HOST'] : $_SERVER['HTTP_HOST'];  				
	        	$siteURL = 'http://' . $host_name . '/' . Yii::$app->request->baseUrl . '/uploads';

				$model = TblContacts::find()						
				        ->joinWith('address')
				        ->joinWith(['tblQuotes' => function($q){ $q->orderBy('quote_id DESC')->limit(2)->offset(0);}])	      		       
				        ->where([ 'subscriber_id' => $sub_id])
				        ->andWhere(['contact_id'=>$con_id])
				        ->orderBy('quote_id DESC')
				        ->all();

					//echo "<pre>"; print_r($model); exit;        

					$uArr = array(); 

					foreach($model as $contact){

						if(isset($contact->tblQuotes) && !empty($contact->tblQuotes) ){

							$qArr = array();

							foreach($contact->tblQuotes as $quotes){

								$qArr['quote_id']      = $quotes->quote_id;
								$qArr['contact_email'] = $quotes->contact_email;
								$qArr['contact_name']  = $quotes->contact_name;
								$qArr['contact_phone'] = $quotes->contact_number;
								$qArr['description']   = $quotes->description;
								$qArr['type']          = $quotes->type;			
								$qArr['created_at']    = $quotes->created_at;				

								$finalArr[] = $qArr;
							}

						}else{
								$finalArr = [];
						}

					    $name  = $contact['name'];
						$email = $contact['email'];
						$phone = $contact['phone'];
						$image = $siteURL . '/' . $contact['image'];

					    // get data from Address relation model
					    $street1        = $contact->address->street1;
					    $street2        = $contact->address->street2;
					    $suburb         = $contact->address->suburb;
					    $state          = $contact->address->state_id;
					    $postal         = $contact->address->postal_code;
					    $country        = $contact->address->country_id;
					    $formatted_addr = $contact->address->formatted_address;
					    $lat            = $contact->address->lat;
					    $lng            = $contact->address->lng;

					} //foreach

							$uArr['contact']['name'] = $name;
							$uArr['contact']['email'] = $email;
							$uArr['contact']['phone'] = $phone;
							$uArr['contact']['street1'] = $street1;
							$uArr['contact']['street2'] = $street2;
							$uArr['contact']['suburb'] = $suburb;
							$uArr['contact']['state'] = $state;
							$uArr['contact']['postal'] = $postal;
							$uArr['contact']['country'] = $country;
							$uArr['contact']['formatted_addr'] = $formatted_addr;
							$uArr['contact']['lat'] = $lat;
							$uArr['contact']['lng'] = $lng;
							$uArr['contact']['image'] = $image;

							$uArr['counts']['jss'] = 10;
							$uArr['counts']['contacts'] = 15;
							$uArr['counts']['quotes'] = 8;
							$uArr['counts']['docs'] = 12;

							$uArr['quotes'] = $finalArr;				 
								
						          $response = [
						            'success' => '1',
						            'message' => 'Contacts Details!',
						            'data' => $uArr,
						          ];

	        }
	        // If the email is not found make a response like this
	        else{
	          $response = [
	            'success' => '0',
	            'message' => 'Subscriber doesnot exist in database!',
	            'data' => '',
	          ];
	        }	

	    }

	    ob_start();
	    echo json_encode($response);    	

    }//actionContactDetails


	public function actionCreateQuote()
    {

		$host_name = isset($_SERVER['HTTP_X_FORWARDED_HOST']) ? $_SERVER['HTTP_X_FORWARDED_HOST'] : $_SERVER['HTTP_HOST'];  				
    	$siteURL = 'http://' . $host_name . '/' . Yii::$app->request->baseUrl . '/uploads';	 

    	/* mandaory fields */
	    $cnt_id  = !empty(Yii::$app->request->post('contact_id'))?Yii::$app->request->post('contact_id'):'';
	    $sub_id  = !empty(Yii::$app->request->post('subscriber_id'))?Yii::$app->request->post('subscriber_id'):'';

	    /* optional fields */
	    $description      = !empty(Yii::$app->request->post('description'))?Yii::$app->request->post('description'):'';
	    $s_name           = !empty(Yii::$app->request->post('s_name'))?Yii::$app->request->post('s_name'):'';
	    $s_email          = !empty(Yii::$app->request->post('s_email'))?Yii::$app->request->post('s_email'):'';
	    $s_phone          = !empty(Yii::$app->request->post('s_phone'))?Yii::$app->request->post('s_phone'):'';
	    $s_street1        = !empty(Yii::$app->request->post('s_street1'))?Yii::$app->request->post('s_street1'):'';
	    $s_street2        = !empty(Yii::$app->request->post('s_street2'))?Yii::$app->request->post('s_street2'):'';
	    $s_suburb         = !empty(Yii::$app->request->post('s_suburb'))?Yii::$app->request->post('s_suburb'):'';
	    $s_state          = !empty(Yii::$app->request->post('s_state'))?Yii::$app->request->post('s_state'):'';
	    $s_postal         = !empty(Yii::$app->request->post('s_postal'))?Yii::$app->request->post('s_postal'):'';
	    $s_country        = !empty(Yii::$app->request->post('s_country'))?Yii::$app->request->post('s_country'):'';
	    $s_formatted_addr = !empty(Yii::$app->request->post('s_formatted_addr'))?Yii::$app->request->post('s_formatted_addr'):'';
	    $s_lat            = !empty(Yii::$app->request->post('s_lat'))?Yii::$app->request->post('s_lat'):'';
		$s_long           = !empty(Yii::$app->request->post('s_long'))?Yii::$app->request->post('s_long'):'';

		$type             = !empty(Yii::$app->request->post('type'))?Yii::$app->request->post('type'):'';

	    $response = [];

        $uArr = array();

	    // validate
	    if(empty($cnt_id) || empty($sub_id)){
	      $response = [
	        'success' => '0',
	        'message' => 'Fields cannot be blank!',
	        'data' => '',
	      ];
	    }
	    else{

	        // search in the database, there is no email referred
	        $subscriber = \common\models\User::findIdentity($sub_id);

	        if(!empty($subscriber)){

				date_default_timezone_set("UTC");
				$milliseconds = strtotime(gmdate("Y-m-d H:i:s"));
	        	/* insert in DB */

	        	$quote_id = '';

	        	if( (!empty($s_street1) && $s_street1 != '') || (!empty($s_street2) && $s_street2 != '') || (!empty($s_suburb) && $s_suburb != '')  || (!empty($s_state) && $s_state != '') || (!empty($s_postal) &&    $s_postal != '') || (!empty($s_country) && $s_country != '') || (!empty($s_formatted_addr) && $s_formatted_addr != '') || (!empty($s_lat) && $s_lat != '') || (!empty($s_long) && $s_long != '') ){

	        		$addressModel = new TblAddress();

			        $addressModel->street1           = $s_street1;
			        $addressModel->street2           = $s_street2;
			        $addressModel->suburb            = $s_suburb;
			        $addressModel->state_id          = $s_state;
			        $addressModel->country_id        = $s_country;
			        $addressModel->postal_code       = $s_postal;
			        $addressModel->formatted_address = $s_formatted_addr;
			        $addressModel->lat               = $s_lat;
			        $addressModel->lng               = $s_long;
			        $addressModel->created_at        = $milliseconds;
			        $addressModel->updated_at        = $milliseconds;

				        if($addressModel->save(false)){

				        	$addId = $addressModel->address_id;

					        	$quotesModel = new TblQuotes();

					        	$quotesModel->quote_contact_id    = $cnt_id;
					        	$quotesModel->quote_subscriber_id = $sub_id;
					        	$quotesModel->description         = $description;
					        	$quotesModel->type                = $type;
					        	$quotesModel->site_address_id     = $addId;	
					        	$quotesModel->contact_email       = $s_email;
					        	$quotesModel->contact_name        = $s_name;
					        	$quotesModel->contact_number      = $s_phone;

						        $quotesModel->created_at          = $milliseconds;
						        $quotesModel->updated_at          = $milliseconds;

						        if($quotesModel->save(false)){

						        	$quote_id = $quotesModel->quote_id;

					        		$model = TblContacts::find()	        				   						
						        				->joinWith('address') 		        							
					    						->where(['contact_id' => $cnt_id])		    						
					    						->all();

										//echo "<pre>"; print_r($model); exit;

					    				foreach($model as $contact){
					    					
										    $name  = $contact['name'];
											$email = $contact['email'];
											$phone = $contact['phone'];
											$image = $siteURL . '/' . $contact['image'];

										    // get data from Address relation model
										    $street1        = $contact->address->street1;
										    $street2        = $contact->address->street2;
										    $suburb         = $contact->address->suburb;
										    $state          = $contact->address->state_id;
										    $postal         = $contact->address->postal_code;
										    $country        = $contact->address->country_id;
										    $formatted_addr = $contact->address->formatted_address;
										    $lat            = $contact->address->lat;
										    $lng            = $contact->address->lng;

					    					$uArr['quote_id']    = $quote_id;
					    					$uArr['description'] = $description;
					    					$uArr['status']      = 0;

					    					/* user details */
											$uArr['contact']['name']           = $name;
											$uArr['contact']['email']          = $email;
											$uArr['contact']['phone']          = $phone;
											$uArr['contact']['street1']        = $street1;
											$uArr['contact']['street2']        = $street2;
											$uArr['contact']['suburb']         = $suburb;
											$uArr['contact']['state']          = $state;
											$uArr['contact']['postal']         = $postal;
											$uArr['contact']['country']        = $country;
											$uArr['contact']['formatted_addr'] = $formatted_addr;
											$uArr['contact']['lat']            = $lat;
											$uArr['contact']['lng']           = $lng;
											$uArr['contact']['image']          = $image;

											/* site details */
											$uArr['site']['name']           = $s_name;
											$uArr['site']['email']          = $s_email;
											$uArr['site']['phone']          = $s_phone;
											$uArr['site']['street1']        = $s_street1;
											$uArr['site']['street2']        = $s_street2;
											$uArr['site']['suburb']         = $s_suburb;
											$uArr['site']['state']          = $s_state;
											$uArr['site']['postal']         = $s_postal;
											$uArr['site']['country']        = $s_country;
											$uArr['site']['formatted_addr'] = $s_formatted_addr;
											$uArr['site']['lat']            = $s_lat;
											$uArr['site']['lng']           = $s_long;
											
											/* counts */
											$uArr['counts']['jss']      = 10;
											$uArr['counts']['contacts'] = 15;
											$uArr['counts']['quotes']   = 8;
											$uArr['counts']['docs']     = 12;

					    				}//foreach

						        }
						}

		        	}else{

					        	$quotesModel = new TblQuotes();

					        	$quotesModel->quote_contact_id    = $cnt_id;
					        	$quotesModel->quote_subscriber_id = $sub_id;
					        	$quotesModel->description         = $description;
					        	$quotesModel->type                = $type;
					        	$quotesModel->site_address_id     = "";	
					        	$quotesModel->contact_email       = $s_email;
					        	$quotesModel->contact_name        = $s_name;
					        	$quotesModel->contact_number      = $s_phone;

						        $quotesModel->created_at          = $milliseconds;
						        $quotesModel->updated_at          = $milliseconds;

						        if($quotesModel->save(false)){

					        		$quote_id = $quotesModel->quote_id;

						        		$model = TblContacts::find()	        				   						
							        				->joinWith('address') 		        							
						    						->where(['contact_id' => $cnt_id])
						    						->all();

											//echo "<pre>"; print_r($model); exit;

						    				foreach($model as $contact){
						    					
											    $name  = $contact['name'];
												$email = $contact['email'];
												$phone = $contact['phone'];
												$image = $siteURL . '/' . $contact['image'];

											    // get data from Address relation model
											    $street1        = $contact->address->street1;
											    $street2        = $contact->address->street2;
											    $suburb         = $contact->address->suburb;
											    $state          = $contact->address->state_id;
											    $postal         = $contact->address->postal_code;
											    $country        = $contact->address->country_id;
											    $formatted_addr = $contact->address->formatted_address;
											    $lat            = $contact->address->lat;
											    $lng            = $contact->address->lng;

						    					$uArr['quote_id']    = $quote_id;
						    					$uArr['description'] = $description;
						    					$uArr['status']      = 0;

						    					/* user contact */
												$uArr['contact']['name'] = $name;
												$uArr['contact']['email'] = $email;
												$uArr['contact']['phone'] = $phone;
												$uArr['contact']['street1'] = $street1;
												$uArr['contact']['street2'] = $street2;
												$uArr['contact']['suburb'] = $suburb;
												$uArr['contact']['state'] = $state;
												$uArr['contact']['postal'] = $postal;
												$uArr['contact']['country'] = $country;
												$uArr['contact']['formatted_addr'] = $formatted_addr;
												$uArr['contact']['lat'] = $lat;
												$uArr['contact']['lng'] = $lng;
												$uArr['contact']['image'] = $image;

												/* site contact */
												$uArr['site']['name'] = $s_name;
												$uArr['site']['email'] = $s_email;
												$uArr['site']['phone'] = $s_phone;
												$uArr['site']['street1'] = $street1;
												$uArr['site']['street2'] = $street2;
												$uArr['site']['suburb'] = $suburb;
												$uArr['site']['state'] = $state;
												$uArr['site']['postal'] = $postal;
												$uArr['site']['country'] = $country;
												$uArr['site']['formatted_addr'] = $formatted_addr;
												$uArr['site']['lat'] = $lat;
												$uArr['site']['lng'] = $lng;												


												$uArr['counts']['jss']      = 10;
												$uArr['counts']['contacts'] = 15;
												$uArr['counts']['quotes']   = 8;
												$uArr['counts']['docs']     = 12;


						    				}//foreach
						        }

		        	}

				          $response = [
				            'success' => '1',
				            'message' => 'Quote successfully saved!',
				            'data' => $uArr,
				          ];

	        }
	        // If the email is not found make a response like this
	        else{
	          $response = [
	            'success' => '0',
	            'message' => 'Subscriber doesnot exist in database!',
	            'data' => '',
	          ];
	        }	

	  	}

	    ob_start();
	    echo json_encode($response);    

	} //actionCreateQuote

	public function actionCommunication(){

    	/* mandaory fields */
	    $sub_id  = !empty($_GET['subscriber_id'])?$_GET['subscriber_id']:'';
	    $cnt_id  = !empty($_GET['contact_id'])?$_GET['contact_id']:'';

	    //echo 'here-'.$sub_id; exit;    

	    $response = [];

	    $arr = array();

	    // validate
	    if(empty($sub_id)){
	      $response = [
	        'success' => '0',
	        'message' => 'Fields cannot be blank!',
	        'data' => '',
	      ];
	    }
	    else{

	        // search in the database, there is no email referred
	        $subscriber = \common\models\User::findIdentity($sub_id);

	        if(!empty($subscriber)){

	        	/* fetch communication data here */      	
				if(!empty($cnt_id) && $cnt_id != ''){

					$commModel = TblCommunications::find()			      		       
								->where(['comm_subscriber_id' => $subscriber])
								->where(['comm_contact_id' => $cnt_id])
								->all();				
				}else{
					$commModel = TblCommunications::find()			      		       
								->where(['comm_subscriber_id' => $subscriber])
								->all();
				}



				if(count($commModel) > 0){
					//echo "<pre>"; print_r($commModel); exit;

					foreach($commModel as $communication){

						$arr['From']    = $communication->comm_from;
						$arr['date']    = (string)$communication->comm_date;
						$arr['to']      = $communication->comm_to;
						$arr['bcc']     = $communication->comm_bcc;
						$arr['subject'] = $communication->comm_subject;
						$arr['body']    = $communication->comm_body;

						$finalArr[] = $arr;

					} //foreach

					$commArr['emails'] = $finalArr;

				}else{
					$commArr['emails'] = [];					
				}

			      $response = [
			        'success' => '1',
			        'message' => 'Communication Listing!',
			        'data' => $commArr,
			      ];

			}
	        // If the email is not found make a response like this
	        else{
	          $response = [
	            'success' => '0',
	            'message' => 'Subscriber doesnot exist in database!',
	            'data' => '',
	          ];
	        }			

	    }

	    ob_start();
	    echo json_encode($response);    

	} //actionCommunication


}
