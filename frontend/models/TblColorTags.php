<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_color_tags".
 *
 * @property int $tag_id
 * @property string $name
 * @property int $created_at
 * @property int $updated_at
 *
 * @property TblColors[] $tblColors
 */
class TblColorTags extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_color_tags';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'created_at', 'updated_at'], 'required'],
            [['created_at', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'tag_id' => 'Tag ID',
            'name' => 'Name',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblColors()
    {
        return $this->hasMany(TblColors::className(), ['tag_id' => 'tag_id']);
    }
}
