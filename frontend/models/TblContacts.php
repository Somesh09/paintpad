<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_contacts".
 *
 * @property int $contact_id
 * @property int $subscriber_id
 * @property string $email
 * @property string $phone
 * @property string $image
 * @property int $address_id
 * @property int $created_at
 * @property int $updated_at
 *
 * @property TblAddress $address
 * @property TblUsers $subscriber
 */
class TblContacts extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_contacts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['subscriber_id', 'email', 'phone', 'image', 'address_id', 'created_at', 'updated_at'], 'required'],
            [['subscriber_id', 'address_id', 'created_at', 'updated_at'], 'integer'],
            [['email'], 'string', 'max' => 50],
            [['phone'], 'string', 'max' => 100],
            [['image'], 'string', 'max' => 255],
            [['address_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblAddress::className(), 'targetAttribute' => ['address_id' => 'address_id']],
            [['subscriber_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblUsers::className(), 'targetAttribute' => ['subscriber_id' => 'user_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'contact_id' => 'Contact ID',
            'subscriber_id' => 'Subscriber ID',
            'email' => 'Email',
            'phone' => 'Phone',
            'image' => 'Image',
            'address_id' => 'Address ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email]);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddress()
    {
        return $this->hasOne(TblAddress::className(), ['address_id' => 'address_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscriber()
    {
        return $this->hasOne(TblUsers::className(), ['user_id' => 'subscriber_id']);
    }

    public function getTblQuotes()
    {
        return $this->hasMany(TblQuotes::className(), ['contact_id' => 'contact_id']);
    }

    public function getTblCommunications()
    {
        return $this->hasMany(TblCommunications::className(), ['contact_id' => 'contact_id']);
    }

    public function getTblAppointments()
    {
        return $this->hasMany(TblAppointments::className(), ['contact_id' => 'contact_id']);
    }
    



}
