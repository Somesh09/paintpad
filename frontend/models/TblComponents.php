<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_components".
 *
 * @property int $comp_id
 * @property string $name
 * @property int $method_id
 * @property int $group_id
 * @property int $calc_method_id
 * @property int $created_at
 * @property int $updated_at
 *
 * @property TblComponentType[] $tblComponentTypes
 * @property TblComponentGroups $group
 * @property TblComponentMethods $method
 */
class TblComponents extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_components';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'method_id', 'group_id', 'calc_method_id', 'created_at', 'updated_at'], 'required'],
            [['method_id', 'group_id', 'calc_method_id', 'created_at', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 50],
            [['group_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblComponentGroups::className(), 'targetAttribute' => ['group_id' => 'group_id']],
            [['method_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblComponentMethods::className(), 'targetAttribute' => ['method_id' => 'method_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'comp_id' => 'Comp ID',
            'name' => 'Name',
            'method_id' => 'Method ID',
            'group_id' => 'Group ID',
            'calc_method_id' => 'Calc Method ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblComponentTypes()
    {
        return $this->hasMany(TblComponentType::className(), ['comp_id' => 'comp_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(TblComponentGroups::className(), ['group_id' => 'group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMethod()
    {
        return $this->hasOne(TblComponentMethods::className(), ['price_method_id' => 'price_method_id']);
    }
}
