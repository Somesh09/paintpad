<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_appointment_type".
 *
 * @property int $id
 * @property string $name
 * @property int $created_at
 * @property int $updated_at
 */
class TblAppointmentType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_appointment_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'orderid', 'created_at', 'updated_at'], 'required'],
            [['created_at', 'updated_at', 'orderid'], 'integer'],
            [['name'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'orderid' => 'Order Id',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
