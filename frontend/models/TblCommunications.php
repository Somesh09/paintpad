<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_communications".
 *
 * @property int $comm_id
 * @property int $contact_id
 * @property int $subscriber_id
 * @property int $quote_id
 * @property string $comm_from
 * @property int $date
 * @property string $to
 * @property string $bcc
 * @property string $subject
 * @property string $body
 * @property int $created_at
 * @property int $updated_at
 *
 * @property TblQuotes $commQuote
 * @property TblContacts $commContact
 * @property TblUsers $commSubscriber
 */
class TblCommunications extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_communications';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['contact_id', 'subscriber_id', 'quote_id', 'comm_from', 'date', 'comm_to', 'bcc', 'subject', 'body', 'created_at', 'updated_at'], 'required'],
            [['contact_id', 'subscriber_id', 'quote_id', 'date', 'created_at', 'updated_at'], 'integer'],
            [['body'], 'string'],
            [['from', 'comm_to', 'comm_bcc', 'comm_subject'], 'string', 'max' => 50],
            [['bcc'], 'string', 'max' => 255],
            [['quote_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblQuotes::className(), 'targetAttribute' => ['quote_id' => 'quote_id']],
            [['contact_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblContacts::className(), 'targetAttribute' => ['contact_id' => 'contact_id']],
            [['subscriber_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblUsers::className(), 'targetAttribute' => ['subscriber_id' => 'user_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'comm_id' => 'Comm ID',
            'contact_id' => 'Comm Contact ID',
            'subscriber_id' => 'Comm Subscriber ID',
            'quote_id' => 'Comm Quote ID',
            'comm_from' => 'Comm From',
            'date' => 'Comm Date',
            'comm_to' => 'Comm To',
            'bcc' => 'Comm Bcc',
            'subject' => 'Comm Subject',
            'body' => 'Comm Body',
            'created_at' => 'Comm Created At',
            'updated_at' => 'Comm Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCommQuote()
    {
        return $this->hasOne(TblQuotes::className(), ['quote_id' => 'quote_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCommContact()
    {
        return $this->hasOne(TblContacts::className(), ['contact_id' => 'contact_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCommSubscriber()
    {
        return $this->hasOne(TblUsers::className(), ['user_id' => 'subscriber_id']);
    }
}
