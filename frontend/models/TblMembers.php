<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_members".
 *
 * @property int $id
 * @property int $subscriber_id
 * @property string $email
 * @property string $f_name
 * @property string $l_name
 * @property string $phone
 * @property int $created_at
 * @property int $updated_at
 *
 * @property TblUsers $subscriber
 */
class TblMembers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_members';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['subscriber_id', 'email', 'f_name', 'l_name', 'phone', 'created_at', 'updated_at'], 'required'],
            [['subscriber_id', 'created_at', 'updated_at'], 'integer'],
            [['email'], 'string', 'max' => 255],
            [['f_name', 'l_name', 'phone'], 'string', 'max' => 50],
            [['subscriber_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblUsers::className(), 'targetAttribute' => ['subscriber_id' => 'user_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'subscriber_id' => 'Subscriber ID',
            'email' => 'Email',
            'f_name' => 'F Name',
            'l_name' => 'L Name',
            'phone' => 'Phone',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscriber()
    {
        return $this->hasOne(TblUsers::className(), ['user_id' => 'subscriber_id']);
    }
}
