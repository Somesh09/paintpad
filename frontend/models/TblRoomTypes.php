<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_room_types".
 *
 * @property int $room_id
 * @property string $name
 * @property int $type_id
 * @property int $multiples
 * @property int $created_at
 * @property int $updated_at
 *
 * @property TblProductType $type
 */
class TblRoomTypes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_room_types';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'type_id', 'multiples', 'created_at', 'updated_at'], 'required'],
            [['type_id', 'multiples', 'created_at', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 50],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblProductType::className(), 'targetAttribute' => ['type_id' => 'type_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'room_id' => 'Room ID',
            'name' => 'Name',
            'type_id' => 'Type ID',
            'multiples' => 'Multiples',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(TblProductType::className(), ['type_id' => 'type_id']);
    }
}
