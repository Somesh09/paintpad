<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_quotes".
 *
 * @property int $quote_id
 * @property int $contact_id
 * @property int $subscriber_id
 * @property string $description
 * @property int $type 0=Interior,1=Exterior
 * @property int $site_address_id
 * @property string $contact_email
 * @property string $contact_name
 * @property string $contact_number
 * @property int $created_at
 * @property int $updated_at
 *
 * @property TblUsers $subscriber
 * @property TblAddress $siteAddress
 * @property TblContacts $contact
 */
class TblQuotes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_quotes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['contact_id', 'subscriber_id', 'type', 'site_address_id', 'contact_email', 'contact_name', 'contact_number', 'created_at', 'updated_at'], 'required'],
            [['contact_id', 'subscriber_id', 'type', 'site_address_id', 'created_at', 'updated_at'], 'integer'],
            [['description'], 'string'],
            [['contact_email'], 'string', 'max' => 100],
            [['contact_name', 'contact_number'], 'string', 'max' => 50],
            [['subscriber_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblUsers::className(), 'targetAttribute' => ['subscriber_id' => 'user_id']],
            [['site_address_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblAddress::className(), 'targetAttribute' => ['site_address_id' => 'address_id']],
            [['contact_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblContacts::className(), 'targetAttribute' => ['contact_id' => 'contact_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'quote_id' => 'Quote ID',
            'contact_id' => 'Contact ID',
            'subscriber_id' => 'Subscriber ID',
            'description' => 'Description',
            'type' => 'Type',
            'site_address_id' => 'Site Address ID',
            'contact_email' => 'Contact Email',
            'contact_name' => 'Contact Name',
            'contact_number' => 'Contact Number',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscriber()
    {
        return $this->hasOne(TblUsers::className(), ['user_id' => 'subscriber_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSiteAddress()
    {
        return $this->hasOne(TblAddress::className(), ['address_id' => 'site_address_id'])->from(['u2' => TblAddress::tableName()]);;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContact()
    {
        return $this->hasOne(TblContacts::className(), ['contact_id' => 'contact_id']);
    }
    public function getTblCommunications()
    {
        return $this->hasMany(TblCommunications::className(), ['quote_id' => 'quote_id']);
    }

}
