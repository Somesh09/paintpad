<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_brands".
 *
 * @property int $brand_id
 * @property string $name
 * @property string $logo
 * @property int $enabled 0=NO, 1=YES
 * @property int $created_at
 * @property int $updated_at
 *
 * @property TblTiers[] $tblTiers
 */
class TblBrands extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_brands';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'logo', 'created_at', 'updated_at'], 'required'],
            [['enabled', 'created_at', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 50],
            [['logo'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'brand_id' => 'Brand ID',
            'name' => 'Name',
            'logo' => 'Logo',
            'enabled' => 'Enabled',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblTiers()
    {
        return $this->hasMany(TblTiers::className(), ['brand_id' => 'brand_id']);
    }
}
