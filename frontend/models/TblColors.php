<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_colors".
 *
 * @property int $color_id
 * @property string $name
 * @property string $hex
 * @property int $number
 * @property int $tag_id
 * @property int $created_at
 * @property int $updated_at
 *
 * @property TblColorTags $tag
 */
class TblColors extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_colors';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'hex', 'number', 'tag_id', 'created_at', 'updated_at'], 'required'],
            [['number', 'tag_id', 'created_at', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 50],
            [['hex'], 'string', 'max' => 20],
            [['tag_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblColorTags::className(), 'targetAttribute' => ['tag_id' => 'tag_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'color_id' => 'Color ID',
            'name' => 'Name',
            'hex' => 'Hex',
            'number' => 'Number',
            'tag_id' => 'Tag ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTag()
    {
        return $this->hasOne(TblColorTags::className(), ['tag_id' => 'tag_id']);
    }
}
