<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_appointments".
 *
 * @property int $id
 * @property int $contact_id
 * @property int $subscriber_id
 * @property int $quote_id
 * @property string $quote_note
 * @property string $street1
 * @property string $street2
 * @property string $suburb
 * @property string $state
 * @property string $postal
 * @property string $country
 * @property string $formatted_addr
 * @property string $lat
 * @property string $lng
 * @property int $date
 * @property int $created_at
 * @property int $updated_at
 *
 * @property TblQuotes $quote
 * @property TblContacts $contact
 * @property TblUsers $subscriber
 */
class TblAppointments extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_appointments';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['contact_id', 'subscriber_id', 'quote_id', 'quote_note', 'street1', 'street2', 'suburb', 'state', 'postal', 'country', 'formatted_addr', 'lat', 'lng', 'date', 'created_at', 'updated_at'], 'required'],
            [['contact_id', 'subscriber_id', 'quote_id', 'type', 'member_id', 'date', 'duration', 'allDay', 'created_at', 'updated_at'], 'integer'],            
            [['scope','note'], 'string'],
            [['street1', 'street2', 'suburb', 'state', 'postal', 'country', 'lat', 'lng'], 'string', 'max' => 50],
            [['formatted_addr'], 'string', 'max' => 100],
            [['quote_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblQuotes::className(), 'targetAttribute' => ['quote_id' => 'quote_id']],
            [['contact_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblContacts::className(), 'targetAttribute' => ['contact_id' => 'contact_id']],
            [['subscriber_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblUsers::className(), 'targetAttribute' => ['subscriber_id' => 'user_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'contact_id' => 'Contact ID',
            'subscriber_id' => 'Subscriber ID',
            'quote_id' => 'Quote ID',
            'quote_note' => 'Quote Note',
            'street1' => 'Street1',
            'street2' => 'Street2',
            'suburb' => 'Suburb',
            'state' => 'State',
            'postal' => 'Postal',
            'country' => 'Country',
            'formatted_addr' => 'Formatted Addr',
            'lat' => 'Lat',
            'lng' => 'Lng',
            'date' => 'Date',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContact()
    {
        return $this->hasOne(TblContacts::className(), ['contact_id' => 'contact_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscriber()
    {
        return $this->hasOne(TblUsers::className(), ['user_id' => 'subscriber_id']);
    }
}
