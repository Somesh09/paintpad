<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_address".
 *
 * @property int $address_id
 * @property string $street1
 * @property string $street2
 * @property string $suburb
 * @property int $state_id
 * @property int $country_id
 * @property string $postal_code
 * @property int $formatted_address
 * @property int $created_at
 * @property int $updated_at
 *
 * @property TblCountries $country
 * @property TblStates $state
 * @property TblContacts[] $tblContacts
 * @property TblUsersDetails[] $tblUsersDetails
 */
class TblAddress extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_address';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            //[['street1', 'street2', 'suburb', 'state_id', 'country_id', 'postal_code', 'formatted_address', 'created_at', 'updated_at'], 'required'],
            [['state_id', 'country_id', 'formatted_address', 'created_at', 'updated_at'], 'integer'],
            [['lat', 'long'], 'float'],
            [['street1', 'street2'], 'string', 'max' => 50],
            [['suburb', 'postal_code'], 'string', 'max' => 100],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblCountries::className(), 'targetAttribute' => ['country_id' => 'country_id']],
            [['state_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblStates::className(), 'targetAttribute' => ['state_id' => 'state_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'address_id' => 'Address ID',
            'street1' => 'Street1',
            'street2' => 'Street2',
            'suburb' => 'Suburb',
            'state_id' => 'State ID',
            'country_id' => 'Country ID',
            'postal_code' => 'Postal Code',
            'formatted_address' => 'Formatted Address',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(TblCountries::className(), ['country_id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getState()
    {
        return $this->hasOne(TblStates::className(), ['state_id' => 'state_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblContacts()
    {
        return $this->hasMany(TblContacts::className(), ['address_id' => 'address_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblUsersDetails()
    {
        return $this->hasMany(TblUsersDetails::className(), ['address_id' => 'address_id']);
    }
    public function getTblQuotes()
    {
        return $this->hasMany(TblQuotes::className(), ['address_id' => 'site_address_id']);
    }


}
