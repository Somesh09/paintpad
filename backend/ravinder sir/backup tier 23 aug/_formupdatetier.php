<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\models\TblBrands;

/* @var $this yii\web\View */
/* @var $model app\models\TblColorTags */
/* @var $form yii\widgets\ActiveForm */

?>
<style>

.right{
	float: right;
	width:620px;
}
.left{
	float: left;
	border-style: solid;
	border-width: 0.1px;
	width:200px;
	max-height: 250px;
	overflow-y: scroll;
	height: 100%;
}

</style>

<div class="right" id="tier">
		<div class="tbl-color-tags-form">


		    <div id="maintier" style="width: 100%;
   height: 500px;">
				<?php $form = ActiveForm::begin(['validationUrl' => Url::toRoute(['validatecolorupdate?id='.$id]),'enableAjaxValidation' => true, 'id' => 'mytierid']); ?>
				<?= $form->field($model, 'name')->textInput(['maxlength' => true,'label'=>false]) ?>


				<?= $form->field($model,'brand_id')->dropDownList(ArrayHelper::map(TblBrands::find()->all(),'brand_id','name'),['prompt'=>'Select brand']) ?>


				<?= $form->field($model, 'type_id')->radioList(array('1'=>'interior','2'=>'exterior','3'=>'both')); ?>
				<?= $form->field($model, 'enabled')->radioList(array('1'=>'YES','0'=>'NO')); ?>



				<div class="form-group">
					<?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
				</div>
					<?php ActiveForm::end(); ?>
				</div>
		</div>
</div>





<div >

	

	<div class="left">
		<h5  style="text-align:center;width:190px;background-color:#e0e4e5;height:35px;"><b>Components Groups</b></h5>
	<table class="table">
		

	<?php
$i=1;
	foreach($model1 as $model1)
	{
		?>
		<tr>
		<td>
		<?php
		echo "<a href='JavaScript:void(0);' color='blue' class='gettiercoat' tid='".$model->tier_id."' cid='".$model1->group_id."' id='compname".$model1->group_id."'>".$model1->name."</a></td></tr>";
	}


	?>
</tr>
</table>




  <!-- div style="display: none;">
	<div id="tiercoat" class="container">
		
		
		<div>
			<table class="table">
				<tr>
					<th>DEFAULT</th>
					<th>TOP COAT PRODUCT</th>
				</tr>
				<tr>
					<td></td>
				</tr>
			</table>

		</div>
	</div>
</div>

 -->


<script>
$('body').on('beforeSubmit', '#mytierid', function (e) {
  var pathname = window.location.href;  
     var form = $(this);
     
     $.ajax({
          url: form.attr('action'),
          type: 'post',
          data: form.serialize(),
          success: function (response) {
              

               console.log(response);              

               if(response == 1){
                 
               
                 $("#modaltier").modal('hide');
                 $.pjax.reload('#tierpjax' , {timeout : false});
              

               }
          },
         
        }
     );
      e.stopImmediatePropagation();
     return false;
});
</script>
<script>
$(document).on('click', '.gettiercoat', function(){  
	var pathname = window.location.href;    
     console.log(pathname);
        var base_url = pathname + 'data';
        console.log(base_url);

      	var cid = $(this).attr('cid');
  		var tid = $(this).attr('tid');
     	console.log(cid);
     	console.log(tid);

        var datastring = {cid:cid,tid:tid};
        console.log(datastring);

     $.ajax({
          url: "data",
          type: 'post',
          data: datastring,
          success: function (response) {
          	// var data = $.parseJSON(response)     
               // do something with response
               //console.log(response);       
               
                $('#maintier').empty();
                $('#maintier').html(response); 
                //$('#maintier').html(data.profiledata);

               
          },
         
     });

    });

</script>
















