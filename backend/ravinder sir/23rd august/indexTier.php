<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use dosamigos\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TblTiers_Search */
/* @var $dataProvider yii\data\ActiveDataProvider */



?>
<style>
#modalContenttier{
    width: 850px;
    height:700px;
    margin: 0px auto;
     overflow: scroll;
}


.tbl-tiers-index .modal-dialog.modal-lg {width: 1100px;}
.tbl-tiers-index .modal-dialog.modal-lg #modalContenttier {width: auto;height: 700px;margin: 0px auto;overflow: inherit;}
.tbl-tiers-index .modal-dialog.modal-lg #modalContenttier #tier.right {float: right;width: 858px;height: 700px;overflow-y: scroll;overflow-x: hidden;}

.tbl-tiers-index .modal-dialog.modal-lg #modalContenttier #tier .tiercoat.pull-left input {width: 95% !important;float: left;background: transparent;box-shadow: none;font-weight: bold;}
.tbl-tiers-index .modal-dialog.modal-lg #modalContenttier #tier .tiercoat.pull-left ul li input {
	width: auto !important;
	float: none;
	margin-right: 5px;
	font-weight: normal;
}

.tbl-tiers-index .modal-dialog.modal-lg #modalContenttier #tier .tiercoat.pull-left span {position: relative;top: 3px;font-size: 25px;font-weight: bolder;}
.tbl-tiers-index .modal-dialog.modal-lg #modalContenttier #tier .tiercoat.pull-left ul li span {position: relative;top: 3px;font-size: 23px;font-weight: normal;right: -2px;}
.tbl-tiers-index .modal-dialog.modal-lg #modalContenttier #tier .tiercoat.pull-left ul li:hover {background: #e8e8e8;}
.tbl-tiers-index .modal-dialog.modal-lg #modalContenttier #tier .tiercoat.pull-left ul li {border-bottom:1px solid #C8C8C8;}

.tbl-tiers-index .modal-dialog.modal-lg #modalContenttier #tier .tiercoat.pull-left {width: 50% !important; margin-right: 14px;}
.tbl-tiers-index .modal-dialog.modal-lg #modalContenttier #tier .tiercoat.pull-left ul {width: 100%;padding: 0;text-align: right;}
.tbl-tiers-index .modal-dialog.modal-lg #modalContenttier #tier .undercoats {width: 48% !important;}

.tbl-tiers-index .modal-dialog.modal-lg #modalContenttier #tier .tiercoat.pull-left input {}
.tbl-tiers-index .modal-dialog.modal-lg #modalContenttier #tier .undercoats .undercoat li {}

</style>
<div class="tbl-tiers-index">
<?php Pjax::begin(['timeout' => 5000,'id'=>'tierpjax']);?>

<?php
 Modal::begin([
          'header'=>'',
          'id'=>'modaltier',
          'size'=>'modal-lg',
       ]);
       echo "<div id='modalContenttier'><div style='text-align:center'><img src='uploads/avatar2.gif' height='100' width='100'></div></div>";
      Modal::end();
      ?>

    <?php echo $this->render('_searchtier', ['model' => $searchModelTier]); ?>

    <p>
     <?php echo  Html::button('<img src="uploads/add.png" width="20" height="20"/>', ['value'=>Url::to( Url::base().'/createtier'),'class'=>'btn btn-primary pull-right' ,'id'=>'modalButtontier']); ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProviderTier,
        //'filterModel' => $searchModelTier,
        'rowOptions'   => function ($model, $key, $index, $grid) {
              return  [
                           "class"=>"modalButtontier2",'value'=>Url::to(Url::base().'/updatetier?id='.$model->tier_id)
                          ];
                },
         'behaviors' => [
            [
                'class' => '\dosamigos\grid\behaviors\LoadingBehavior',
                'type' => 'spinner'
            ]
        ],  
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

        
            'name',

            'brand.name',
            'type.name',
             ['attribute'=>'enabled or disabled',
             'value' =>
                       function($model){
                         if($model->enabled == '1'){
                            return 'enabled';
                         }
                         else
                         {
                            return 'disabled';
                         }
                       },



             'filter' =>false,],
            //'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn',
            'header' => 'UPDATE',
               'headerOptions' => ['style' => 'color:#337ab7'],
               'template' => '{update}',
               'buttons' => [
            

               'update' => function ($url, $model,$id) {
                            return Html::a('<span class=""><img src="uploads/update.png" width="13" height="13"/></span>', $url, [
                           "class"=>"modalButtontier3",'value'=>Url::to(Url::base().'/updatetier?id='.$model->tier_id)
                ]);
            },
            

          ],
           'urlCreator' => function ($action, $model, $key, $index) {
            

            Url::to(['update?id='.$model->tier_id],['id'=>'modal'])   ;         
                //$url ='update?id='.$model->brand_id;
              //return $url;
                 
                 //return Html::a($url, ['title' => 'view']);

            }
          ],
        ],
    ]); ?>
</div>
<script>

  $(function()
{
  //
  $('.modalButtontier3').click(function(){
    $('#modaltier').modal('show')
      .find('#modalContenttier')
      .load ($(this).attr('value'));
  });
});


  $(function()
{
  //
  $('#modalButtontier').click(function(){
    $('#modaltier').modal('show')
      .find('#modalContenttier')
      .load ($(this).attr('value'));
  });
});
  $(function()

{
     $('.modalButtontier2').dblclick(function(){

        $('#modaltier').modal('show')
          .find('#modalContenttier')
          .load ($(this).attr('value'));
      });

});
   
$(function() { //equivalent to document.ready
  $("#tbltiers_search-name").focus();
});


$('#tbltiers_search-name').focus(function(){
  var that = this;
  setTimeout(function(){ that.selectionStart = that.selectionEnd = 10000; }, 0);
});

  

$('#tbltiers_search-name').bind('keyup', function() { 
    $('#tbltiers_search-name').delay(200).submit();
});

$('#tbltiers_search-brand_id').bind('change', function() { 
    $('#tbltiers_search-brand_id').delay(200).submit();
});


    </script>
<?php Pjax::end();?>