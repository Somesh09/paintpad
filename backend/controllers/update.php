<?php
use yii\helpers\Url;
?>
<!DOCTYPE html>
<html>
	<head>
		<style type="text/css">
		.modal-body {
			position: relative;
			padding: 15px;
			float: left;
			/*width: 93%;*/
		}		
		.modal-dialog{
			width: 80%;
			height:90%;
		}
		#blah21
		{
		    position: relative;
		    top: -53px;
		    left: -20px;
		}
		</style>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment-with-locales.min.js">
</script>
	</head>
	
	<body>
		<?php 
			$session = Yii::$app->session->getId();
			echo Yii::$app->user->id;
			die;
		?>

		<div class="showLoader text-center" style="display:none;"><p>loading....</p></div>
		<div style="display:none;" class="bodyMain col-sm-12 row">
		<div class="col-sm-6">
			<div>
				<label>Subscriber Details</label>
			</div>
			<input type="file" class="changeImage pull-right" style="display:none;" onchange="document.getElementById('blah').src = window.URL.createObjectURL(this.files[0])">
			<input type="text" style="display:none;" value="<?php echo $_GET['id']?>" class="idClicked">
			
			<div class="subscriberLogo pull-right"></div>
			<button class="upload pull-right btn btn-primary" style="margin-top: 117px;"></button>
			<button class="imageChange pull-right btn btn-primary" style="margin-top: 117px;"></button>
			<div class="subscriberName"></div>
			<div class="subscriberAddress"></div>
			<div class="subscriberSuburb"></div>
			<div class="subscriberState"></div>
			<div class="subscriberCountry"></div>
			<div class="subscriberPhone"></div>
			<div class="subscriberEmail"></div>
			<div class="subscriberWebsite"></div>
		</div>	
		<div class="col-sm-6">
			<button class="btn btn-primary col-sm-6 addDoc">Add New Library Document</button>
			<span class="col-sm-6">
				<input type="text" placeholder="Search/Filter" class="form-control"></span>
			<table class="table">
				<tr>
					<th>File ID</th>
					<th>File Name</th>
					<th>Description</th>
					<th>Uploaded By</th>
					<th>Uploaded Date</th>
				</tr>
				<tbody class="documentListing">
				</tbody>
			</table>
		</div>
	</div>

<!--- bootstrap Modal for adding doc starts here!-->
  <!-- Modal -->
<div id="myModalDocAdd"  class="modal modal-child" data-backdrop-limit="1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-modal-parent="#myModal">
    <div class="modal-dialog">
    
      <!-- Modal content-->
    	<form action="" method="post" enctype="multipart/form-data" id="formDoc">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="closeModalDocAdd pull-right btn btn-primary"  >&times;</button>


           <input type="button" class="SaveDocAdd pull-right btn btn-primary" value="Save" name="saveDoc">
  
          <h4 class="modal-title">Document Detail</h4>
        </div>
        <div class="modal-bodyDocAdd">
          
          		<table class="table">
          			<tr>
          				<td>
          					<label>File Name</label>
          				</td>
          				<td>
          					<input type="text" name="file_name" class="form-control">
          				</td>
          			</tr>
          			<tr>	
      						<td>
          					<label>File Description</label>
          				</td>
          				<td>
          					<textarea rows="4" cols="50" name="file_desc" class="form-control" style="margin: 0px 313px 0px 0px; width: 451px; height: 85px;"></textarea>
          				</td>
          			</tr>
          			<tr>	
      						<td>
          					<label>Always Link</label>
          				</td>
          				<td>
          					<label>Yes</label>
          					<input type="radio" name="alwaysLink" value="1" >
          					<label>No</label>
          					<input type="radio" name="alwaysLink"  value="0">
          				</td>
          			</tr>
          			<tr>	
      						<td>
          					<label>Upload New file</label>
          				</td>
          				<td>
          					<input type="file" name="image_0" class="imageDoc" onchange="document.getElementById('viewimageDoc').src = window.URL.createObjectURL(this.files[0])" style="display:none; ">

          					<img style="border: 3px solid #ddd;" src="<?php  echo Url::base(true); ?>/uploads/no_image.png"  class="viewimageDoc" id="viewimageDoc" width="120px" height="120px" >
          					<span class="opnImg"><img id="blah21" src="<?php  echo Url::base(true); ?>/uploads/edit.png" alt="your image" width="15px" height="16px"/ onclick="$('.imageDoc').click();"></span>
          				</td>          				
          			</tr>
          		</table>
          	</form>
        </div>
        <div class="modal-footer">
          
        </div>
      </div>
      </form>
    </div>
</div>
<!--- bootstrap Modal for adding doc ends here!-->


<!--- bootstrap Modal for Updating doc starts here!-->
  <!-- Modal -->
<div id="myModalDocUpdate"  class="modal modal-child" data-backdrop-limit="1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-modal-parent="#myModal">
    <div class="modal-dialog">
    
      <!-- Modal content-->
    	<form action="" method="post" enctype="multipart/form-data" id="formDocUpdate">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="closeModalDocAdd pull-right btn btn-primary"  >&times;</button>


           <input type="button" class="SaveDocUpdate pull-right btn btn-primary" value="Save" >
  
          <h4 class="modal-title">Document Detail</h4>
        </div>
        <div class="modal-bodyDocAdd">
          
          		<table class="table">
          			<tr>
          				<td>
          					<label>File Name</label>
          				</td>
          				<td>
          					<input type="text" name="file_name" class="form-control file_doc_name">
          				</td>
          			</tr>
          			<tr>	
      						<td>
          					<label>File Description</label>
          				</td>
          				<td>
          					<textarea rows="4" cols="50" name="file_desc" class="form-control file_doc_desc" style="margin: 0px 313px 0px 0px; width: 451px; height: 85px;"></textarea>
          				</td>
          			</tr>
          			<tr>	
      						<td>
          					<label>Always Link</label>
          				</td>
          				<td>
          					<label>Yes</label>
          					<input type="radio" name="alwaysLink" value="1" class="file_doc_alwayslinkon">
          					<label>No</label>
          					<input type="radio" name="alwaysLink"  value="0" class="file_doc_alwayslinkoff">
          				</td>
          			</tr>
          			<tr>	
      						<td>
          					<label>Upload New file</label>
          				</td>
          				<td>
          					<input type="file" name="image_0" class="imageDocupdate" onchange="document.getElementById('viewimageDocupdate').src = window.URL.createObjectURL(this.files[0])" style="display:none; ">

          					<img style="border: 3px solid #ddd;" src="<?php  echo Url::base(true); ?>/uploads/no_image.png"  class="viewimageDoc imagedoc_list" id="viewimageDocupdate" width="120px" height="120px" >
          					<span class="opnImg"><img id="blah21" src="<?php  echo Url::base(true); ?>/uploads/edit.png" alt="your image" width="15px" height="16px"/ onclick="$('.imageDocupdate').click();"></span>
          				</td>          				
          			</tr>
          		</table>
          	</form>
        </div>
        <div class="modal-footer">
          
        </div>
      </div>
      </form>
    </div>
</div>
<!--- bootstrap Modal for adding doc ends here!-->




</body>
</html>
<script>
	$('.showLoader').show();
	$('.bodyMain').hide();
	var data ={"json":'{"company_id":"<?php echo $_GET['id'];?>"}'};
	//var da = '{"company_id":"<?php //echo $_GET['id'];?>"}';
	// console.log({"json":da})
	// console.log(data);
	var siteBaseUrl = "<?php echo Url::base(true);?>";
	//console.log(siteBaseUrl);

	$.ajax({
	url:  siteBaseUrl+"/subscriber/all-company-data",
	type: "POST",
	data: data, // Send the object.
	  success: function(response) {
	  	var parsed = JSON.parse(response);
      	console.log(parsed);
      	$('.upload').text('Upload');
      	$('.imageChange').text('Change Image')
      	$('.subscriberName').html("<label>"+parsed.data.compDetail.name+"</label>");
      	$('.subscriberAddress').html("<label>"+parsed.data.compDetail.address+"</label>");
      	$('.subscriberSuburb').html("<label>"+parsed.data.compDetail.suburb+"</label>");
      	$('.subscriberState').html("<label>"+parsed.data.compDetail.state+" "+parsed.data.compDetail.postcode+ "</label>");
      	$('.subscriberCountry').html("<label>"+parsed.data.compDetail.country+"</label>");
      	$('.subscriberPhone').html("<label>"+parsed.data.compDetail.phone+"</label>");
      	$('.subscriberEmail').html("<label>"+parsed.data.compDetail.email+"</label>");
      	$('.subscriberWebsite').html("<label><a>"+parsed.data.compDetail.website+"</a></label>");
        $('.subscriberLogo').html("<img src='//"+parsed.data.compDetail.logo+"' width='100px' height='100px' id='blah'>");
        var doclist = parsed.data.docList;
        console.log(doclist.length);
        for(var i=0; i<doclist.length; i++)
        {
							var d = new Date(0); // The 0 there is the key, which sets the date to the epoch
							d.setUTCSeconds(doclist[i].uploaded_date);
							//alert(d);
							var da = moment(d).format("DD/MM/YYYY");
							da = da.toUpperCase();
        	$('.documentListing').append("<tr id="+doclist[i].file_id+" class='docUpdateListing' name='"+doclist[i].file_name+"' desc='"+doclist[i].desc+"' actual_name='"+doclist[i].actual_name+"' always_link ='"+doclist[i].always_link+"' image_url ='"+doclist[i].url+"'><td>"+doclist[i].file_id+"</td><td>"+doclist[i].file_name+"</td><td>"+doclist[i].desc+"</td><td>"+doclist[i].uploaded_by+"</td><td>"+da+"</td></tr>")
        }
        $('.bodyMain').show();  
  		  $('.showLoader').hide();

      }
	})
</script>
<script type="text/javascript">
	$(document).on('click', '.imageChange', function (e) {
		e.preventDefault();
		$('.changeImage').click();
	})
</script>
<script>
	$(document).on('click', '.upload', function (e) {
		e.preventDefault();
        var file_data = $('.changeImage').prop('files')[0];  
        var id_data = $('.idClicked').val(); 
        var form_data = new FormData();  
       // console.log(form_data);

         form_data.append('file', file_data);
         form_data.append('text', id_data);
         console.log(form_data);
           
        $.ajax({
            url: "change-image",
            type: "POST",
            data: form_data,
            contentType: false,
            cache: false,
            processData:false,
            success: function(data){
                console.log(data);
            }
        });		
		
	})
</script>
<script>
$('.addDoc').click(function(){
	$('#myModalDocAdd').modal('show')
	if($('.modal-backdrop').length>1)
	{
		 $('.modal-backdrop:gt(0)').remove();
		//$('.modal-backdrop').remove();
	}

})
</script>
<script>
$('.closeModalDocAdd').click(function(){
	$('#myModalDocAdd').modal('hide');
	$('#myModalDocUpdate').modal('hide');
})
</script>
<script>
	$('.viewimageDoc').click(function(){
		$('.imageDoc').click();
	})
	$('.viewimageDocupdate').click(function(){
		$('.imageDocupdate').click();
	})
</script>
<script type="text/javascript">
	$(document).on('click','.docUpdateListing',function(){
		//alert($(this).attr('id'));
		var name = $(this).attr('name');
		var desc = $(this).attr('desc');
		var actual_name = $(this).attr('actual_name');
		var always_link = $(this).attr('always_link');
		var image_url = $(this).attr('image_url');
		console.log("name--->"+name+"------desc---->"+desc+"-----actual_name----->"+actual_name+"-----always_link---->"+always_link+"---image_url---->"+image_url);
		$('.file_doc_name').val(name);
		$('.file_doc_desc').text(desc);
		if(always_link==1)
		{
			$('.file_doc_alwayslinkon').attr('checked','checked');
		}
		else
		{
			$('.file_doc_alwayslinkoff').attr('checked','checked');
		}
		$('.imagedoc_list').attr('src','//'+image_url);
		$('#myModalDocUpdate').modal('show');
		if($('.modal-backdrop').length>1)
		{
		 $('.modal-backdrop:gt(0)').remove();
		}
	})
</script>
<script>

	$('.SaveDocUpdate').click(function(e){
	    $.ajax({
            url: "save-doc",
            type: "POST",
            data: new FormData($('#formDocUpdate')[0]),
            contentType: false,
            //cache: false,
            processData:false,
            success: function(data){
                console.log(data);
            }
        });	
	    e.preventDefault();
	})
</script>	
<script>
	$('.SaveDocAdd').click(function(e){
	    var file_data = $('.imageDoc').prop('files')[0];  
        var form_value = $("#formDoc").serialize();
        var form_data = new FormData(); 
		form_data.append('file', file_data);
		console.log(form_data);
	    $.ajax({
            url: "save-doc",
            type: "POST",
            data: new FormData($('#formDoc')[0]),
            contentType: false,
            //cache: false,
            processData:false,
            success: function(data){
                console.log(data);
            }
        });	
	    e.preventDefault();
	})
</script>	

