<?php
namespace backend\controllers;

use Yii;
use yii\web\Response;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
//use common\models\LoginForm;
// use app\models\TblCompany;
use yii\helpers\Url;
use app\models\TblCompanySearch;

use app\models\TbluserActivity;
use app\models\TbluserActivitySeach;
use app\models\TblQuotessearch;

use app\models\TblemailTempSarch;
use app\models\TblemailTemp;

//use common\models\LoginForm;
use common\models\SignupForm;
use common\models\User;
use app\models\TblUsers;
use app\models\TblUsersDetails;
use app\models\TblContacts;
use app\models\TblAddress;
use app\models\TblQuotes;
use app\models\TblCommunications;
use app\models\TblAppointments;
use app\models\TblAppointmentType;
use app\models\TblMembers;
use app\models\TblInvoices;
use app\models\TblBrands;
use app\models\TblTiers;
use app\models\TblStrengths;
use app\models\TblSheen;
use app\models\TblColorTags;
use app\models\TblColors;
use app\models\TblComponents;
use app\models\TblComponentGroups;
use app\models\TblComponentType;
use app\models\TblSpecialItems;
use app\models\TblRoomTypes;
use app\models\TblNotes;
use app\models\TblBrandPref;
use app\models\TblPaintDefaults;
use app\models\TblCompanyStock;

use app\models\TblRooms;
use app\models\TblRoomPref;
use app\models\TblRoomCompPref;
use app\models\TblRoomDimensions;
use app\models\TblRoomSpecialItems;
use app\models\TblRoomNotes;


use app\models\TblProductStock;

use app\models\TblPaymentSchedule;
use app\models\TblSummary;
use app\models\TblQuoteSpecialItems;

use app\models\TblUsersSettings;
use app\models\TblPrepLevel;

use app\models\TblPaymentMethod;
use app\models\TblPaymentOptions;

use app\models\TblRoomBrandPref;
use app\models\TblRoomPaintDefaults;

use app\models\TblQuoteStatus;
use app\models\TblExtQuoteComponent;

use app\models\TblAreaCompPref;

use app\models\TblCompany;

use app\models\TblUserRoles;

use app\models\TblCompanyEmailSettings;
use app\models\TblEmailSettingsDoc;
use app\models\TblCompanyDocuments;

use app\models\TblCompanyPrepLevel;

class SubscriberController extends Controller
{
	public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['index', 'list', 'uploadcsv','exportdatacsv','exportdata','appointmenttype','create','update','all-company-data','change-image','save-doc','save-subscriber','addsubscriptions','quotes'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }



    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }



    public function beforeAction($action) 
    { 
        $this->enableCsrfValidation = false; 
        return parent::beforeAction($action); 
    }




    public function actionLogin()
    {
           if (!\Yii::$app->user->isGuest) {
              return $this->goHome();
           }

               $model = new LoginForm();
               if ($model->load(Yii::$app->request->post()) && $model->loginAdmin()) {
                        return $this->goBack();
           } else {
                   return $this->render('login', [
                        'model' => $model,
               ]);
           }
    }


    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionQuotes()
    {
        $company= $_POST['json'];
        $company = json_decode($company,true);
        $company_id = $company['company_id'];
        //$quotes = TblQuotes::find()->where(['subscriber_id' => $company_id])->orderBy('quote_id DESC')->all(); old one
        $quotes = TblQuotes::find()
                     ->joinWith(
                         ['subscriber' => function($q) use($company_id){
                             $q->where('tbl_users.company_id='.$company_id);}
                         ]
                     )
                     ->all();


        $res = [];
        foreach ($quotes as $key => $value) {
            $res[$value['quote_id']]['id'] = $value['quote_id'];
            $res[$value['quote_id']]['contact_id'] = $value['contact_id'];
            $res[$value['quote_id']]['type'] = $value['type'];
            $res[$value['quote_id']]['client_name'] = $value['client_name'];

            $contact = TblContacts::find()->where(['contact_id' => $value['contact_id']])->one();
// echo "<pre>";print_r($contact);die;
            $res[$value['quote_id']]['contact_email'] = $contact['email'];
            $res[$value['quote_id']]['contact_name'] = $contact['name'];
            $res[$value['quote_id']]['contact_number'] = $contact['phone'];
            $res[$value['quote_id']]['contact_mobile'] = $contact['mobile'];

            $res[$value['quote_id']]['price'] = $value['price'];
            $res[$value['quote_id']]['status'] = $value['status'];
            $res[$value['quote_id']]['updated_at'] = $value['updated_at'];
        }
        $res = array_values($res);
        // print_r($res);
        $response = [
            'success' => 1,
            'message' => 'success',
            'data'=>$res
        ];
        echo json_encode($response);exit();
        // return json_encode($res);
    }


    public function actionIndex()
    {
	    $searchModel = new TblCompanySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->orderBy(['company_id'=>SORT_DESC])->all();
        $model = new TblCompany();

        $searchModelActivity = new TbluserActivitySeach();
        $dataProviderActivity = $searchModelActivity->search(Yii::$app->request->queryParams);
        $dataProviderActivity->query->orderBy(['timestamp'=>SORT_DESC])->all();
        $modelActivity = new TbluserActivity();

        $searchModelQuotes = new TblQuotessearch();
        $dataProviderQuotes = $searchModelQuotes->search(Yii::$app->request->queryParams);
        $dataProviderQuotes->query->orderBy(['quote_id'=>SORT_DESC])->all();
        $modelQuotes = new TblQuotes();
        
        $searchModelEmailTemp = new TblemailTempSarch();
        $dataProviderEmailTemp = $searchModelEmailTemp->search(Yii::$app->request->queryParams);
        $dataProviderEmailTemp->query->orderBy(['temp_id'=>SORT_DESC])->all();
        $modelEmailTemp = new TblemailTemp();

        return $this->render('index',[
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model'=>$model,
            'modelActivity'=>$modelActivity,
            'searchModelActivity' => $searchModelActivity,
            'dataProviderActivity' => $dataProviderActivity,
            'modelQuotes'=>$modelQuotes,
            'searchModelQuotes' => $searchModelQuotes,
            'dataProviderQuotes' => $dataProviderQuotes,
            'modelEmailTemp'=>$modelEmailTemp,
            'searchModelEmailTemp' => $searchModelEmailTemp,
            'dataProviderEmail' => $dataProviderEmailTemp,
        ]);

    }

    public function actionList()
    {
        $searchModel = new TblCompanySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->orderBy(['company_id'=>SORT_DESC])->all();
        $model = new TblCompany();

        return $this->render('sublist',[
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model'=>$model,
        ]);

    }

    public function actionAppointmenttype()
    {
        return $this->render('appointment_types');
    }


    public function actionExportdata()
    {
        $companyies = TblCompany::find('company_id,name')->all();
        return $this->render('exportdata',['data'=>$companyies]);
    }
    public function actionAddsubscriptions()
    {
        // $companyies = TblCompany::find('company_id,name')->all();
        return $this->render('subscriptionsAdd');
    }

    public function actionUploadcsv()
    {

        $company_id = $_POST['company'];

        $csvMimes = array('application/x-csv', 'text/x-csv', 'text/csv', 'application/csv');
    
    // Validate whether selected file is a CSV file
    // if(!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'], $csvMimes)){
        
        // If the file is uploaded
        if(is_uploaded_file($_FILES['file']['tmp_name'])){
            
            // Open uploaded CSV file with read-only mode
            $csvFile = fopen($_FILES['file']['tmp_name'], 'r');
            
            // Skip the first line
            fgetcsv($csvFile);
            while(($line = fgetcsv($csvFile)) !== FALSE){
            
                // Get row data
                if($company_id != '0'){
                    

                    $priceDataCount = TblCompanyStock::find()->where(['company_id' => $company_id, 'stock_id'=>$line[4]])->count();
                    // echo "<pre>";print_r($priceDataCount);die;
                    if($priceDataCount > 0){
                        // print_r($priceData);die;
                        $priceData = TblCompanyStock::find()->where(['company_id' => $company_id, 'stock_id'=>$line[4]])->one();
                        $priceData->company_stock_id = $priceData->company_stock_id;
                        $priceData->price = $line[9];
                        $priceData->save();
                    }else{
                        $priceData = new TblCompanyStock();
                        $priceData->stock_id = $line[4];
                        $priceData->price = $line[9];
                        $priceData->company_id = $company_id;
                        $priceData->save();
                    }
                    // echo "<pre>";print_r($priceData);
                    
                    
                }else{
                    if(!empty($line[4]) || $line[4] != '0'){
                        TblProductStock::updateAll(['price' => $line[9]], 'product_stock_id ="'. $line[4] .'"' );
                    }

                }
                
                
            }
            // Close opened CSV file
            fclose($csvFile);
            // die;
            return true;
            
        }else{

            return json_encode('Invalid File');
        }
    // }else{

    //     return json_encode('No file');
    // }
}
public function uploadCSVProductpage()
    {

            $company_id = $_POST['company'];

            $csvMimes = array('application/x-csv', 'text/x-csv', 'text/csv', 'application/csv');
        
        // Validate whether selected file is a CSV file
        // if(!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'], $csvMimes)){
            
            // If the file is uploaded
            if(is_uploaded_file($_FILES['file']['tmp_name'])){
                
                // Open uploaded CSV file with read-only mode
                $csvFile = fopen($_FILES['file']['tmp_name'], 'r');
                
                // Skip the first line
                print_r(fgetcsv($csvFile));die;
                while(($line = fgetcsv($csvFile)) !== FALSE){
                
                    // Get row data
                    if($company_id != '0'){
                        

                        $priceData = TblCompanyStock::find()->where(['company_id' => $company_id, 'stock_id'=>$line[4]])->one();
                        if(count($priceData) > 0){
                            // print_r($priceData);die;
                            $priceData->company_stock_id = $priceData->company_stock_id;
                            $priceData->price = $line[9];
                        }else{
                            $priceData = new TblCompanyStock();
                            $priceData->stock_id = $line[4];
                            $priceData->price = $line[9];
                            $priceData->company_id = $company_id;
                        }
                        // echo "<pre>";print_r($priceData);
                        $priceData->save();
                        
                    }else{
                        if(!empty($line[4]) || $line[4] != '0'){
                            TblProductStock::updateAll(['price' => $line[9]], 'product_stock_id ="'. $line[4] .'"' );
                        }

                    }
                    
                    
                }
                // Close opened CSV file
                fclose($csvFile);
                // die;
                return true;
                
            }else{

                return json_encode('Invalid File');
            }
        // }else{

        //     return json_encode('No file');
        // }
    }
    
    public function actionExportdatacsv($id)
    {
        $company_id = $id;
        $connection = \Yii::$app->db;
        $productStockQry = "SELECT tbl_product_stock.product_stock_id,tbl_product_stock.product_id,tbl_product_stock.sheen_id,tbl_product_stock.litres,tbl_product_stock.code,tbl_product_stock.name,tbl_product_stock.price, tbl_products.name as product_name, tbl_products.spread_rate, (CASE WHEN tbl_products.type_id = '1' THEN 'I' WHEN tbl_products.type_id = '2' THEN 'E' WHEN tbl_products.type_id = '3' THEN 'B' END) as product_type, tbl_brands.name as brand_name, tbl_sheen.name as sheen_name from tbl_product_stock, tbl_products, tbl_brands, tbl_sheen where tbl_product_stock.product_id=tbl_products.product_id and tbl_brands.brand_id = tbl_products.brand_id and tbl_sheen.sheen_id = tbl_product_stock.sheen_id ORDER BY brand_name ASC";

        $model = $connection->createCommand($productStockQry);
        $productStock = $model->queryAll();
        // print_r($productStock);die;
        if($company_id == 0){
            $company_id = 'Master';
        }else{
            foreach ($productStock as $key => $value) {
                $priceData = TblCompanyStock::find()->where(['company_id' => $company_id, 'stock_id'=>$value['product_stock_id']])->one();

                if($priceData !=''  && $priceData->price != ''){
                    $productStock[$key]['price'] = $priceData->price;
                }
                // if($priceData !='' && sizeof($priceData) > 0 && $priceData->price != ''){
                //     $productStock[$key]['price'] = $priceData->price;
                // }
            }
        }

        if(count($productStock) > 0){
            $delimiter = ",";
            $filename = "pp_" . date('Y-m-d') . ".csv";
            
            //create a file pointer
            $f = fopen('php://memory', 'w');
            
            //set column headers
            $fields = array(
                'BRAND',
                 'Name',
                 'Spread Rate (m/L)',
                 'I/E SORT   I=INTERIOR E= EXTERIOR B = Both',
                 'StockId',
                 'Sheen',
                 'Litres',
                 'Stock Code',
                 'Paint Pad Stock Name',
                 'Price',
                 'Image (URL or Name)',
                 'PRODUCT NAME SORT',
                 'CompanyId');

            fputcsv($f, $fields, $delimiter);
            
                foreach ($productStock as $row) {
                $lineData = array($row['brand_name'],
                    $row['product_name'],
                    $row['spread_rate'],
                    $row['product_type'],
                    $row['product_stock_id'],
                    $row['sheen_name'],
                    $row['litres'],
                    $row['code'],
                    $row['name'],
                    $row['price'],
                    '',
                    '',
                    $company_id);
                fputcsv($f,$lineData,$delimiter);
            }
            
            //move back to beginning of file
            fseek($f, 0);
            //set headers to download file rather than displayed
            header('Content-Type: text/csv');
            header('Content-Disposition: attachment; filename="' . $filename . '";');
            fpassthru($f);exit();
            // header_remove();
            //output all remaining data on a file pointer
            // return fpassthru($f);
        }
        return;
    }


    public function actionCreate()
    {
        if(isset($_POST) && count($_POST)>0)
        {
            $connection = \Yii::$app->db;
            $features = (new \yii\db\Query())
                ->select(['feature_id'])
                ->from('TBL_PACKAGE_FEATURES')
                ->where(['pkg_id' => '27'])
                ->all();
            // $features_ids = [];
           
           // print_r($features_ids);die();
            $model = new TblCompany();
            $model->name = $_POST['name'];
            $model->address = $_POST['address'];
            $model->suburb = $_POST['suburb'];
            $model->state = $_POST['state'];
            $model->postcode = $_POST['postcode'];
            $model->country = $_POST['contact_country'];
            // $model->postal_address= $_POST['pAddress'];
            // $model->postal_suburb= $_POST['pSuburb'];
            // $model->postal_state= $_POST['pState'];
            // $model->postal_postcode= $_POST['pPostcode'];
            // $model->postal_country= $_POST['pContact_country'];
            $time = time();
            $hoursToAdd = 7*24;
            //Convert the hours into seconds.
            $secondsToAdd = $hoursToAdd * (60 * 60);
            //Add the seconds onto the current Unix timestamp.
            $expiry_time = $time + $secondsToAdd;

            $model->phone= $_POST['phone'];
            $model->trial= 1;
            $model->subscription_status = 'ACTIVE';
            // $model->stripe_subscription_id = 'TRIAL';
            $model->trial_expiration = $expiry_time;
            $model->email = $_POST['email'];
            $model->website = $_POST['website'];
            $model->abn = $_POST['abn'];
            $model->insurance_comp_detail = $_POST['insurance_comp_detail'];
            $model->worker_insurance_detail = $_POST['worker_insurance_detail'];
            $model->license_number = $_POST['license_number'];
            $model->paint_brand = $_POST['paint_brand'];
            $model->timezone = $_POST['timezone'];
            $model->save();

            for ($i=0; $i < sizeof($features) ; $i++) { 
                $connection->createCommand()->insert('tbl_user_subscription', [
                        'company_id' => $model->company_id,
                        'pkg_feature_id' => $features[$i]['feature_id'],
                    ])->execute();
            }
            
            if (!empty($_POST['gst_number'])) { //added by Abhijit Sood 18/6/2020
				$gst = $_POST['gst_number'];
				$userSettings = TblUsersSettings::find()->where(['company_id' => $model->company_id])->one();
				$userSettings->gst = $gst;
				$userSettings->save();
			}
            $userSettings = TblUsersSettings::find()->where(['company_id' => $model->company_id])->one();
            $userSettings->gross_margin_percent = 37.11;
            $userSettings->save();
            return 1;
        }
        else
        {
            $brandModel = TblBrands::find()
                        ->orderBy('brand_order_id ASC')
                        ->where(['tbl_brands.enabled' => 1])
                        ->all();
                        // print_r($brandModel);die;
            return $this->renderAjax('create',array('paintBrands'=>$brandModel));
        }
    }

    public function actionUpdate()
    {
      return $this->renderAjax('update');
    }


    public function actionAllCompanyData(){
        // print_r($_POST);
        // die;
        $domain = $_SERVER['SERVER_NAME'];
        $response = [];
        if(empty($_POST['json'])){
            $response = [
                'success' => '0',
                'message' => 'Fields cannot be blank!',
                'data'=>[]
            ];
            echo json_encode($response);exit();
        }

        $mydata = ($_POST['json'] != '')?$_POST['json']:json_encode(array());
        $mydata = json_decode($mydata,true);
        $siteURL = Url::base(true);

       // echo("<pre>");print_r($mydata);exit();

        if( !isset( $mydata['company_id'] ) || $mydata['company_id']=="" ){
            $response = [
                'success' => 0,
                'message' => 'Fields cannot be blank!',
                'data'=>[]
            ];
            echo json_encode($response);exit();
        }
        else{
            
            $company_id = $mydata['company_id'];
          

            $compPeopleCount = TblUsers::find()->joinWith('tblUsersDetails')->count();
             // print_r($compPeopleCount);
             // die;
            $data['compPeopleCount'] = $compPeopleCount;

            $quoteCount = TblQuotes::find()
                                        ->joinWith(
                                            ['subscriber' => function($q) use($company_id){
                                                $q->where('tbl_users.company_id='.$company_id);}
                                            ]
                                        )
                                        ->count();

            $data['quoteCount'] = $quoteCount;

            $appointmentCount = TblAppointments::find()
                            ->joinWith(
                                ['subscriber' => function($q) use($company_id){
                                    $q->where('tbl_users.company_id='.$company_id);}
                                ]
                            )
                            ->count();

            $data['appointmentCount'] = $appointmentCount;

            $communicationCount = TblCommunications::find()
                            ->joinWith(
                                ['commSubscriber' => function($q) use($company_id){
                                    $q->where('tbl_users.company_id='.$company_id);}
                                ]
                            )
                            ->count();

            $data['communicationCount'] = $communicationCount;


            $company = TblCompany::find()->where( ["tbl_company.company_id"=>$mydata['company_id']] )->joinWith('tblUsersSettings')->one();
            //echo "<pre>";print_r($company);exit;
            //$company = $user->tblCompany;
            $compDetail['name'] = $company->name;
            $compDetail['logo'] = ($company->logo == "" || is_null($company->logo) )?"":$siteURL."/uploads/".$company->logo;
            $compDetail['address'] = $company->address;
            $compDetail['suburb'] = $company->suburb;
            $compDetail['state'] = $company->state;
            $compDetail['postcode'] = $company->postcode;
            $compDetail['country'] = $company->country;
            $compDetail['phone'] = $company->phone;
            $compDetail['email'] = $company->email;
            $compDetail['website'] = $company->website;

            $data['compDetail'] = $compDetail;

            $companySettings = $company->tblUsersSettings;
            $companySetting['users_settings_id']                     = $companySettings->users_settings_id;
            $companySetting['application_cost']                      = ($companySettings->application_cost)?$companySettings->application_cost:"";
            $companySetting['profit_markup']                         = ($companySettings->profit_markup)?$companySettings->profit_markup:"";
            $companySetting['gross_margin_percent']                  = ($companySettings->gross_margin_percent)?$companySettings->gross_margin_percent:"";
            $companySetting['default_deposit_percent']               = ($companySettings->default_deposit_percent)?$companySettings->default_deposit_percent:"";
            $companySetting['xero_consumer_key']                     = ($companySettings->xero_consumer_key)?$companySettings->xero_consumer_key:"";
            $companySetting['xero_share_secret']                     = ($companySettings->xero_share_secret)?$companySettings->xero_share_secret:"";
            $companySetting['xero_invoice_line_account_code']        = ($companySettings->xero_invoice_line_account_code)?$companySettings->xero_invoice_line_account_code:"";
            $companySetting['account_name']                          = ($companySettings->account_name)?$companySettings->account_name:"";
            $companySetting['account_BSB']                           = ($companySettings->account_BSB)?$companySettings->account_BSB:"";
            $companySetting['account_number']                        = ($companySettings->account_number)?$companySettings->account_number:"";
            $companySetting['payment_phone_number']                  = ($companySettings->payment_phone_number)?$companySettings->payment_phone_number:"";
            $companySetting['colour_consultant_item']                = ($companySettings->colour_consultant_item)?$companySettings->colour_consultant_item:"";
            $companySetting['price_increase']                = ($companySettings->price_increase)?$companySettings->price_increase:"";
            $companySetting['gst']                                   = ($companySettings->gst)?$companySettings->gst:"";
            $companySetting['terms_condition']                       = ($companySettings->terms_condition)?$companySettings->terms_condition:"";


            $data['companySetting'] = $companySetting;

            
           // $user = TblCompany::find(["company_id"=>$mydata['company_id']])->one();
            $company = TblCompany::find()->joinWith('tblUsers')->where(["tbl_company.company_id"=>$company_id])->one();
            //echo "<pre>";print_r($user);exit;

            //$company = $user->tblCompany;
            $compDetail['name'] = $company->name;
            $compDetail['logo'] = ($company->logo == "" || is_null($company->logo) )?"":$domain."/paintpad/uploads/".$company->logo;
            $compDetail['address'] = $company->address;
            $compDetail['suburb'] = $company->suburb;
            $compDetail['state'] = $company->state;
            $compDetail['postcode'] = $company->postcode;
            $compDetail['country'] = $company->country;
            $compDetail['phone'] = $company->phone;
            $compDetail['email'] = $company->email;
            $compDetail['website'] = $company->website;

            $data['compDetail'] = $compDetail;

            $quoteEmailSettings = TblCompanyEmailSettings::find()->where(["tbl_company_email_settings.company_id"=>$mydata['company_id']])->joinWith('tblEmailSettingsDocs.companyDocuments')->all();

            $quoteEmailSettingsDetail = new \stdClass();
            $jssEmailSettingsDetail = new \stdClass();
            if($quoteEmailSettings){
                foreach($quoteEmailSettings as $k => $emailSetting){
                    if($emailSetting->type_id == 1){    //Quote Email Setting
                        $quoteEmailSettingsDetail->server_email_address = 'info@paintpad.mysys.com.au';
                        $quoteEmailSettingsDetail->bcc_to = $emailSetting->bcc_to;
                        $quoteEmailSettingsDetail->email_subject = $emailSetting->quote_email_subject;
                        $quoteEmailSettingsDetail->doc=[];
                        if($emailSetting->tblEmailSettingsDocs){
                            $doc=[];
                            foreach($emailSetting->tblEmailSettingsDocs as $sk => $emailSettingsDoc){
                                if($emailSettingsDoc->companyDocuments){
                                    $compDoc = $emailSettingsDoc->companyDocuments;
                                    $doc[] = $compDoc->file_name;
                                }
                            }
                            $quoteEmailSettingsDetail->doc=$doc;
                        }
                    }
                    else{
                        $jssEmailSettingsDetail->server_email_address = 'info@paintpad.mysys.com.au';
                        $jssEmailSettingsDetail->bcc_to = $emailSetting->bcc_to;
                        $jssEmailSettingsDetail->email_subject = $emailSetting->quote_email_subject;
                        $jssEmailSettingsDetail->doc=[];
                        //echo "<pre>";print_r($emailSetting->tblEmailSettingsDocs);exit;
                        if($emailSetting->tblEmailSettingsDocs){
                            $doc=[];
                            foreach($emailSetting->tblEmailSettingsDocs as $sk => $emailSettingsDoc){
                                if($emailSettingsDoc->companyDocuments){
                                    $compDoc = $emailSettingsDoc->companyDocuments;
                                    $doc[] = $compDoc->file_name;
                                }
                            }
                            $jssEmailSettingsDetail->doc=$doc;
                        }
                    }
                }
            }
            
            //echo "<pre>";print_r($quoteEmailSettingsDetail);exit;

            $data['quoteEmailSettingsDetail'] = $quoteEmailSettingsDetail;
            $data['jssEmailSettingsDetail'] = $jssEmailSettingsDetail;

            $uploadUrl = $domain. '/paintpad/uploads/';
            $docs = [];
            $companyDocuments = TblCompanyDocuments::find()->where(['tbl_company_documents.company_id' => $company_id])->joinWith('subscriber.tblUsersDetails')->all();
            if(empty($companyDocuments)){
                $data['docList'] = [];
            }
            else{
                foreach ($companyDocuments as $key => $doc) {
                    $first_name = $doc->subscriber->tblUsersDetails[0]->first_name;
                    $last_name = $doc->subscriber->tblUsersDetails[0]->last_name;
                    $arr['file_id']=$doc->doc_id;
                    $arr['file_name']=$doc->file_name;
                    $arr['actual_name']=$doc->actual_name;
                    $arr['desc']=$doc->description;
                    $arr['always_link']=$doc->always_link;
                    $arr['uploaded_by']=$first_name." ".$last_name;
                    $arr['uploaded_date']=$doc->created_at;
                    $arr['url']=$uploadUrl.$doc->file_path_name;
                    $docs[]=$arr;
                }
                $data['docList'] = $docs;
            }
            
            $companyPrepLevel = TblPrepLevel::find()->joinWith('company')->where(['tbl_prep_level.company_id' => $company_id])->all();
            //echo "<pre>";print_r($companyPrepLevel);exit;
            if(empty($companyPrepLevel))
            {
                $data['prepLevel']= [];
            }
            else
            {
                foreach ($companyPrepLevel as $key => $value) {
                    $arr = [];
                    $arr['id']=$value->prep_id;
                    $arr['name']=$value->prep_level;
                    $arr['uplift_cost']=$value->uplift_cost;
                    $arr['uplift_time']=$value->uplift_time;
                    $arr['is_default']=$value->is_default;
                    $arr['deletable']=$value->deletable;
                    $prep[]=$arr;
                }
                $data['prepLevel']=$prep;
            }

            $specialModel = TblSpecialItems::find()->where(['company_id' => $company_id])->all();
            if(count($specialModel)>0){
                foreach($specialModel as $_model){
                    $arr = [];
                    $arr['id']    = $_model->item_id;
                    $arr['name']  = $_model->name;
                    $arr['price'] = $_model->price;
                    $data['specialItems'][] = $arr;
                }
            }else{
                $data['specialItems'] = [];
            }

            $response = [
                'success' => 1,
                'message' => 'Company All Data!',
                'data'=>$data
            ];
            echo json_encode($response);exit;
        }
    }

    public function actionChangeImage()
    {
        print_r($_FILES);
        print_r($_POST);
        return 1;
        die;
    }    



    //////save subscriber through admin
    public function actionSaveSubscriber()
    {
       $response = [];
        if(empty($_POST['json'])){
            $response = [
                'success' => 0,
                'message' => 'Fields cannot be blank!',
                'data'=>[]
            ];
            echo json_encode($response);exit();
        }

        $mydata = ($_POST['json'] != '')?$_POST['json']:json_encode(array());
        $mydata = json_decode($mydata,true);
        $siteURL = Url::base(true);

        //echo("<pre>");print_r($mydata);exit();

        if( (!isset( $mydata['company_id'] ) || $mydata['company_id']=="") || (!isset( $mydata['first_name'] ) || $mydata['first_name']=="") || (!isset( $mydata['last_name'] ) || $mydata['last_name']=="") || (!isset( $mydata['username'] ) || $mydata['username']=="") || (!isset( $mydata['email'] ) || $mydata['email']=="") || (!isset( $mydata['user_role'] ) || $mydata['user_role']=="") || (!isset( $mydata['password'] ) || $mydata['password']=="")){
            $response = [
                'success' => 0,
                'message' => 'Fields cannot be blank!',
                'data'=>[]
            ];
            echo json_encode($response);exit();
        }
        else{
            // $user_id = $mydata['user_id'];

            $first_name = $mydata['first_name'];
            $last_name = $mydata['last_name'];
            $phone = $mydata['phone'];
            $mobile = $mydata['mobile'];
            $role = $mydata['user_role'];
            $status = $mydata['status'];
            $company_id = $mydata['company_id'];
            $password = $mydata['password'];
            $username = $mydata['username'];
            $user_email = $mydata['email'];
            $password = $mydata['password'];

            // $user = User::findOne([
            //   'username' => $username,
            // ]);
            // $email = User::findOne([
            //   'email' => $email,
            // ]);
            // if ($user) {
            //     $response = [
            //         'success' => 0,
            //         'message' => 'This User Name Already Exists',
            //         'data' => [],
            //     ];
            //     echo json_encode($response);exit;
            // }
            // if ($email) {
            //     $response = [
            //         'success' => 0,
            //         'message' => 'This Email Already Exists',
            //         'data' => [],
            //     ];
            //     echo json_encode($response);exit;
            // }

            $tblUsers = new User();
            //$tblUsersDetails = new TblUsersDetails();

            $tblUsers->username = $username;
            $tblUsers->email = $user_email;
            $tblUsers->setPassword($password);
           // $tblUsers->pwd=password_hash($password,PASSWORD_DEFAULT);  
            $tblUsers->generateAuthKey();
            $tblUsers->role = $role;
            $tblUsers->status = $status;
            $tblUsers->company_id = $company_id;
            $tblUsers->created_at = strtotime( gmdate('Y-m-d H:i:s') );
            $tblUsers->updated_at = strtotime( gmdate('Y-m-d H:i:s') );
            // print_r($tblUsers);die;
            $tblUsers->validate();
            if($tblUsers->validate())
            {
                if($tblUsers->save())
                {
                  $user_id = $tblUsers->user_id;
                  $tblDetails = new TblUsersDetails();
                  $tblDetails->user_id = $user_id;
                  $tblDetails->first_name = $first_name;
                  $tblDetails->last_name = $last_name;
                  $tblDetails->phone = $phone;
                  $tblDetails->mobile = $mobile;
                  $tblDetails->created_at = strtotime( gmdate('Y-m-d H:i:s') );
                  $tblDetails->updated_at = strtotime( gmdate('Y-m-d H:i:s') );
                 // print_r($tblDetails);
                 
                  // if($tblDetails->save(false) == 1)
                  if($tblDetails->save())
                  {
                    $data['user_id'] = $user_id;
                    $data['role'] = $role;
                    $data['state'] = 10;
                    $data['company_id'] = $company_id;
                    $data['first_name'] = $first_name;
                    $data['last_name'] = $last_name;
                    $data['phone'] = $phone;
                    $data['mobile'] = $mobile;
                    $data['password'] = $password;
                    $data['username'] = $username;
                    $data['email'] = $user_email;
                    $res['userdata'] =$data;
                    $response = [
                      'success' => 1,
                      'message' => 'Data Have been Saved',
                      'data' => $res,
                    ];
                  }else{ // by ankit
                    TblUsers::deleteAll('user_id = '.$user_id);
                    // print_r($tblDetails);die;
                    $response = [
                      'success' => 0,
                      'message' => 'Data Have Not been Saved',
                      'data' => [],
                    ];
                  } // by ankit
                    echo json_encode($response);exit;
                }                    
            }
            else
            {
              $message = $this->validatorError($tblUsers);
              $response = [
                'success' => 0,
                'message' => $message,
                'data' => [],
              ];
              echo json_encode($response);exit;
            }
        }


        //}

            //echo "<pre>";print_r($user);exit;
        //}
            // $emailVerified = 1;
            // if($email != $user->email){
            //     $user->email = $email;
            //     if(!$user->validate()){
            //         $message = $this->validatorError($user);
            //         $response = [
            //             'success' => 0,
            //             'message' => $message,
            //             'data' => [],
            //         ];
            //         echo json_encode($response);exit;
            //         $emailVerified = 0;
            //     }
            // }

            // $usernameVerified = 1;
            // if($username != $user->username){
            //     $user->username = $username;
            //     if(!$user->validate()){
            //         $message = $this->validatorError($user);
            //         $response = [
            //             'success' => 0,
            //             'message' => $message,
            //             'data' => [],
            //         ];
            //         echo json_encode($response);exit;
            //         $usernameVerified = 0;
            //     }
            // }

            // //echo $emailVerified." ".$usernameVerified;exit;

            // if($password){
            //     $user->setPassword($password);
            //     $user->generateAuthKey();
            //     $user->role = $role;
            //     if($user->validate()){
            //         if ($user->save()) {
            //             TblUsersDetails::updateAll([
            //                 'first_name' => $first_name,
            //                 'last_name' => $last_name,
            //                 'phone' => $phone,
            //                 'mobile' => $mobile,
            //             ], 'user_id = "'.$user_id.'"' );
            //         }
            //         $response = [
            //             'success' => 1,
            //             'message' => "Subscriber People Updated Successfully",
            //             'data' => [],
            //         ];
            //         echo json_encode($response);exit;
            //     }
            //     else{
            //         $message = $this->validatorError($user);
            //         $response = [
            //             'success' => 0,
            //             'message' => $message,
            //             'data' => [],
            //         ];
            //         echo json_encode($response);exit;
            //     }
            // }
            // else{
            //     $user->role = $role;
            //     if($user->validate()){
            //         $user->save();
            //         TblUsersDetails::updateAll([
            //             'first_name' => $first_name,
            //             'last_name' => $last_name,
            //             'phone' => $phone,
            //             'mobile' => $mobile,
            //         ], 'user_id = "'.$user_id.'"' );
            //         $response = [
            //             'success' => 1,
            //             'message' => 'Subscriber People Updated Successfully',
            //             'data' => [],
            //         ];
            //         echo json_encode($response);exit;
            //     }
            //     else{
            //         $message = $this->validatorError($user);
            //         $response = [
            //             'success' => 0,
            //             'message' => $message,
            //             'data' => [],
            //         ];
            //         echo json_encode($response);exit;
            //     }
            //}
        //}        
    }
    /////save subscriber through admin ends here

    public function validatorError($model)
    {
    $errors = [];
    //echo "<pre>";print_r($model);exit;
    foreach($model->errors as $mek => $columnError){
      foreach($columnError as $cek => $error){
        $errors[]=$error;
      }
    }
    //echo "<pre>";print_r($errors);exit;
    return $message = implode(" AND ",$errors);
   }

}