<?php
namespace backend\controllers;

use Yii;
use yii\web\Response;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use app\models\TblBrands;
use app\models\TblBrandsSearch;
use app\models\TblProducts;
use app\models\TblProductsSearch;
use app\models\TblProductType;
use yii\widgets\ActiveForm;
use app\models\TblSheen;
use app\models\TblSheenSearch;
use app\models\TblPrepLevel;
use app\models\tblPrepLevelSearch;
use app\models\TblComponentGroups;
use app\models\TblComponentGroupsSearch;
use app\models\TblRoomTypes;
use app\models\Tbl_rooms_typesSearch;
use app\models\TblColorTags;
use app\models\TblColorTagsSearch;
use app\models\TblSpecialItems;
use app\models\TblSpecialItemsSearch;
use app\models\TblStrengths;
use app\models\TblStrengthsSearch;
use app\models\TblComponents;
use app\models\TblComponentsSearch;
use app\models\TblComponentType;
use app\models\Model;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;
use app\models\TblColors;
use app\models\TblColorsSearch;
use app\models\TblTiers;
use app\models\TblTiers_Search;
use app\models\TblTierCoats;
use yii\helpers\Html;
use yii\filters\auth\QueryParamAuth;
use app\models\TblProductInventory;
use app\models\TblProductStock;
use yii\db\Command;
use yii\data\Pagination;
/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index','brand','create','validatebrand','update','product','updateproduct','createproduct','uploadcsv','downloadcsv','sheen','createsheen','updatesheen','validateproduct','validatesheen','prep','createprep','validateprep','updateprep','createcomponent','updatecomponent','createroom','updateroom','createcolor','updatecolor','createspecial','updatespecial','createstrength','updatestrength','validateprepupdate','validatebrandupdate','validateproductupdate','validatesheenupdate','validatecomponentupdate','validatecomponent','validateroomupdate','validateroom','validatecolorupdate','validatecolor','validatespecialupdate','validatespecial','validatestrengthupdate','validatestrength','createcomponentmain','validatecomponentmain','updatecomponentmain','validateupdatecomponentmain','sortbrand','sortsheen','sortcomponent','sortroom','createcolorsmain','updatecolorsmain','validatecolorsupdatemain','validatecolorsmain','createtier','validatetier','updatetier','exportbrand','data','showproduct','deletetiercoat','addsheen','savetier','undercoatproduct','deleteundercoat','updatetierimage','deleteproductstock','deletecomponenttype','selectproduct','selectproductundercoat','deleteproducttopcoat','deletesheentopcoat','deleteproductundercoat','buypackage'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }



      public function beforeAction($action) 
    { 
        $this->enableCsrfValidation = false; 
        return parent::beforeAction($action); 
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
     public function actionBuypackage(){
      $items = [];
        return $this->render('buyPackage', [
            // 'items' => $this->items,
        ]);

    }
    public function actionIndex()

    {
            $searchModel = new TblBrandsSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider->query->orderBy(['enabled'=>SORT_DESC,'brand_order_id'=>SORT_ASC])->all();
            $model = new TblBrands();


            $searchModelproduct = new TblProductsSearch();
            $dataproviderproduct = $searchModelproduct->search(Yii::$app->request->queryParams);
            $model2 =new TblProducts();


            $searchModelSheen = new TblSheenSearch();
            $dataProviderSheen = $searchModelSheen->search(Yii::$app->request->queryParams);
            $dataProviderSheen->query->orderBy(['sheen_order_id'=>SORT_ASC])->all();
            $model3 =new  TblSheen();


            $searchModelprep = new TblPrepLevelSearch();
            $dataProviderprep = $searchModelprep->search(Yii::$app->request->queryParams);
            $dataProviderprep->query->all();
            $model4 =new TblPrepLevel();


            $searchModelcomponent = new TblComponentGroupsSearch();
            $dataProvidercomponent = $searchModelcomponent->search(Yii::$app->request->queryParams);
            $dataProvidercomponent->query->orderBy(['enabled'=>SORT_DESC])->all();
            $model5=new TblComponentGroups();


            $searchModelrooms = new Tbl_rooms_typesSearch();
            $dataProviderrooms = $searchModelrooms->search(Yii::$app->request->queryParams);
            $dataProviderrooms->query->orderBy(['room_order_id'=>SORT_ASC])->all();
            $model6=new Tblroomtypes();


            $searchModelcolor = new TblColorTagsSearch();
            $dataProvidercolor = $searchModelcolor->search(Yii::$app->request->queryParams);
            $model7=new TblColorTags();



            $searchModelspecial = new TblSpecialItemsSearch();
            $dataProviderspecial = $searchModelspecial->search(Yii::$app->request->queryParams);
            $dataProviderspecial->query->all();
            $model8= new TblSpecialItems();


            $searchModelstrength = new TblStrengthsSearch();
            $dataProviderstrength = $searchModelstrength->search(Yii::$app->request->queryParams);
            $model9=new TblStrengths();


            $searchModelcomponents = new TblComponentsSearch();
            $dataProvidercomponents = $searchModelcomponents->search(Yii::$app->request->queryParams);
            $dataProvidercomponents->query->orderBy(['component_order_id'=>SORT_ASC])->all();
            $model10=new TblComponents();


            $searchModelcolorsmain = new TblColorsSearch();
            $dataProvidercolorsmain = $searchModelcolorsmain->search(Yii::$app->request->queryParams);
            $model11=new TblColors();


            $searchModelTier = new TblTiers_Search();
            $dataProviderTier = $searchModelTier->search(Yii::$app->request->queryParams);
            $dataProviderTier->query->orderBy(['enabled'=>SORT_DESC])->all();
            $model12=new TblTiers();
            

        return $this->render('index',[
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model'=>$model,

            'searchModelproduct'=>$searchModelproduct,
            'dataproviderproduct'=>$dataproviderproduct,
            'model2'=>$model2,

            'searchModelSheen'=>$searchModelSheen,
            'dataProviderSheen'=>$dataProviderSheen,
            'model3' =>$model3,

            'searchModelprep'=>$searchModelprep,
            'dataProviderprep'=>$dataProviderprep,
            'model4' =>$model4,

            'searchModelcomponent' =>$searchModelcomponent,
            'dataProvidercomponent'=>$dataProvidercomponent,
            'model5'=>$model5,

            'searchModelrooms'=>$searchModelrooms,
            'dataProviderrooms'=>$dataProviderrooms,
            'model6'=> $model6,

            'searchModelcolor'=>$searchModelcolor,
            'dataProvidercolor'=>$dataProvidercolor,
            'model7'=>$model7,

            'searchModelspecial'=>$searchModelspecial,
            'dataProviderspecial'=>$dataProviderspecial,
            'model8'=>$model8,

            'searchModelstrength'=>$searchModelstrength,
            'dataProviderstrength'=>$dataProviderstrength,
            'model9'=>$model9,

            'searchModelcomponents'=>$searchModelcomponents,
            'dataProvidercomponents'=>$dataProvidercomponents,
            'model10'=>$model10,

            'searchModelcolorsmain'=>$searchModelcolorsmain,
            'dataProvidercolorsmain'=>$dataProvidercolorsmain,
            'model11'=>$model11,

            'searchModelTier' => $searchModelTier,
            'dataProviderTier' => $dataProviderTier,
            'model12'=>$model12

        ]);
    }

    /**
     * Login action.
     *
     * @return string
     */
    /*public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }*/

    public function actionLogin()
        {
               if (!\Yii::$app->user->isGuest) {
                  return $this->goHome();
               }

                   $model = new LoginForm();
                   if ($model->load(Yii::$app->request->post()) && $model->loginAdmin()) {
                            //return $this->goBack();
                            return $this->redirect(['subscriber/index']);
               } else {
                       return $this->render('login', [
                            'model' => $model,
                   ]);
               }
        }


    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }




    
    public function actionCreate()
    {
        $model = new TblBrands();
//echo "<pre>";
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
           $model->created_at=strtotime(gmdate('y-m-d H:i:s'));
           // print_r(Yii::$app->request->post());
           // exit();
            if($model->save())
            {
            // echo $model->brand_id;
                $data = 1;
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return $data;
            }
            else
            {
                $data = 0;
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return $data;
            }
            
        }
        else
        {   
           if (Yii::$app->request->isAjax) {
        return $this->renderAjax('create', [
            'model' => $model,
        ]);
      }
      else
      {
        return $this->render('create', [
            'model' => $model,]);
      }
     }
  }



    
    public function actionUpdate($id)
    {
         $model = TblBrands::find()->where(['brand_id' => $id])->one();
           
          if ($model->load(Yii::$app->request->post()) && $model->validate()) {
           $model->updated_at=strtotime(gmdate('y-m-d H:i:s'));
            if($model->save())
            {
                $data = 1;
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return $data;
            }
            else
            {
                $data = 0;
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return $data;
            }
            
        }
        else
        {
        
      

            return $this->renderAjax('update', [
                'model' => $model,
                'id'=>$id,
            ]);
        }
    
   }

   public function actionSortbrand()
   {
     $model = TblBrands::find() ->orderBy([  'brand_order_id'=>SORT_ASC])->all();

      return $this->renderAjax('sortbrand',['model'=>$model]);
   }

   
   




   public function actionValidatebrand()
   {
         Yii::$app->response->format = Response::FORMAT_JSON;
                $model = new TblBrands();
            $model->load(Yii::$app->request->post());
            //print_r(Yii::$app->request->post()); exit();
            return ActiveForm::validate($model);

   }

   public function actionValidatebrandupdate($id)
   {
             Yii::$app->response->format = Response::FORMAT_JSON;
            $model = TblBrands::find()->where(['brand_id' => $id])->one();

           
            $model->load(Yii::$app->request->post());
            //print_r(Yii::$app->request->post()); exit();
            return ActiveForm::validate($model);

   }

  public function actionUploadcsv(){
    ini_set("auto_detect_line_endings", true);
    $company_id = $_POST['company_id'];

    $csvMimes = array('application/x-csv', 'text/x-csv', 'text/csv', 'application/csv');

    // Validate whether selected file is a CSV file
    // if(!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'], $csvMimes)){

    // If the file is uploaded
    if(is_uploaded_file($_FILES['file']['tmp_name'])){
        
        // Open uploaded CSV file with read-only mode
        $csvFile = fopen($_FILES['file']['tmp_name'], 'r');
        
        // Skip the first line
        // fgetcsv($csvFile, 1000, ",");
        $the_big_array = []; 
        
        while (($data = fgetcsv($csvFile, 1000, ",")) !== FALSE) {
            //$line is an array of the csv elements
            $the_big_array[] = $data;	
        }
        // Close opened CSV file
        fclose($csvFile);
        
        // print_r($modelstockData->sheen_id);die;
        for ($kk=1; $kk < sizeof($the_big_array) ; $kk++) { 
            $model = new TblProducts();   
            $model3 =new TblProductStock();
            // $modelstock = new TblProductStock();
            $model->name = $the_big_array[$kk]['0'];
            $model->spread_rate = $the_big_array[$kk]['1'];
            if ($the_big_array[$kk]['9'] != '') {
              $model->image = $the_big_array[$kk]['9'];
            }
            else{
              $model->image = null;
            }
            if ($the_big_array[$kk]['2'] == 'I') {
                $type = 1;
            }
            else if ($the_big_array[$kk]['2'] == 'B') {
                $type = 3;
            }
            else if ($the_big_array[$kk]['2'] == 'E') {
                $type = 2;
            }
            $model->type_id = $type ;
            $model->brand_id = $company_id;
            $model->created_at = strtotime(gmdate('y-m-d H:i:s'));
            $model->updated_at =strtotime(gmdate('y-m-d H:i:s'));
            $model->save();
            $modelstockData = TblSheen::find()->where('BINARY [[name]]=:name', ['name'=>$the_big_array[$kk]['4']])->one();
            if (!empty($modelstockData->sheen_id)) {
                $model3->sheen_id=$modelstockData->sheen_id ;
                $model3->litres=$the_big_array[$kk]['5'];
                $model3->code=$the_big_array[$kk]['6'];
                $model3->name=$the_big_array[$kk]['7'];
                $model3->price=$the_big_array[$kk]['8'];
                $model3->product_id=$model->product_id;
                $model3->created_at=strtotime(gmdate('y-m-d H:i:s'));
                $model3->updated_at=strtotime(gmdate('y-m-d H:i:s'));
                $model3->save();
            }
            
            // print_r($model3);
            // print_r($modelstockData);

        }
        if($model->validate())
        {
          echo '1';
        }
        else {
          echo json_encode($model->getErrors());
        }
        // echo "<pre>";
        // print_r($the_big_array);
        // echo '1';
    }else{

        echo json_encode('Invalid File');
    }


  }

  public function actionDownloadcsv(){
    $filename = 'sample.csv';
    header("Cache-Control: public");
    header("Content-Description: File Transfer");
    header("Content-Disposition: attachment; filename= sample.txcsvt");
    header("Content-Transfer-Encoding: binary");    
    $f = fopen('sample.csv', 'r');
    fpassthru($f);
    return;
    

  }
  
   public function actionUpdateproduct($id)
    {
        $model = TblProducts::find()->where(['product_id' => $id])->one();
        //$modelsecond = TblProductInventory::find()->where(['product_id' => $id])->all();
        $modelsecond = TblProductStock::find()->where(['product_id' => $id])->all();
        $modelstock =new TblProductStock();    
        //echo '<pre>'; print_r($modelsecond); exit;
        $modelstockData = TblSheen::find()->all();

        //$updatestock= Yii::$app->request->post('TblProductInventory');
        $updatestock= Yii::$app->request->post('stock_sheen_update');
        $createStock= Yii::$app->request->post('stock_sheen');
        //print_r( $updatestock);
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

        //      echo "<pre>";
        // print_r($_POST);
        // exit("---449line---");
                     $request = Yii::$app->request->post();
                                           // $name=$request['TblProducts']['name'];
                                           // $spread_rate=$request['TblProducts']['spread_rate'];
                                           // $brand_id=$request['TblProducts']['brand_id'];
                                           // $type_id=$request['TblProducts']['type_id'];
                     $model->save();
                     if(isset($updatestock))
                     {
                        //$modelthird = TblProductInventory::find()->where(['product_id' => $id])->all();
                       
                        $modelthird = TblProductStock::find()->where(['product_id' => $id])->all();  
                        // echo "<pre>";
                        // print_r($modelthird[0]);
                        // exit("--464--");
                        // $c=count($modelthird)-1;
                        // $i=0;
                        foreach($updatestock as $key=>$value)
                        {
                            if($value['code']=='')
                            {
                              $value['code'] = 'PP-DUMMY-1234';
                            }
                             
                            $modelthird[$key]->sheen_id=$value['stock'];
                            //$modelthird[$i]->litre=$value['litre'];
                            $modelthird[$key]->litres=$value['litre'];
                            $modelthird[$key]->code=$value['code'];
                            $modelthird[$key]->name=$value['name'];
                            $modelthird[$key]->price=$value['price']; 
                            $modelthird[$key]->updated_at=strtotime(gmdate('y-m-d H:i:s'));
                            $modelthird[$key]->save();
                            
                            
                        }
                        

                    }

                    if(isset($createStock))
                    {
                          foreach($createStock as $valueStock)
                          {
                               $model3 =new TblProductStock(); 
                              //print_r($value);      # code...
                              $model3->sheen_id=$valueStock['stock'];
                              if($valueStock['code']=="")
                              {
                                $valueStock['code'] = 'PP-DUMMY-1234';
                              }
                              $model3->litres=$valueStock['litre'];
                              $model3->code=$valueStock['code'];
                              $model3->name=$valueStock['name'];
                              $model3->price=$valueStock['price'];
                              $model3->product_id=$id;
                              $model3->created_at=strtotime(gmdate('y-m-d H:i:s'));
                              $model3->updated_at=strtotime(gmdate('y-m-d H:i:s'));
                              $model3->save(false);
                          }
                         
                    }


                      $model2 = TblProducts::find()->where(['product_id' => $id])->one();
                      $oldfile=$model2->image;
                      $image=$_FILES['image']['name'];

                      if($image!="")
                      {

                          $model2->updated_at=strtotime(gmdate('y-m-d H:i:s'));
                          $filename=strtotime(gmdate('y-m-d H:i:s'))."_".$image;
                          move_uploaded_file($_FILES['image']['tmp_name'], 'uploads/' . $filename);
                          $model2->image=$filename;
                          //
                          /*if($oldfile!="")
                          {
                              unlink("uploads/".$oldfile);
                          }*/
                          if($model2->save())
                          {
                              $data = 1;
                              Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                              return $data;
                          }

                      }
                      else
                      {
                          $model2->image=$oldfile;
                        
                          $model2->updated_at=strtotime(gmdate('y-m-d H:i:s'));
                           
                         if($model2->save())
                          {
                              $data = 1;
                              Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                              return $data;
                          }

                      }
        }

        else
        {
            return $this->renderAjax('updateproduct', [
            'model' => $model,
            'id'=>$id,
            'modelsecond'=>$modelsecond,
            'modelstock'=>$modelstock,
            'modelstockData'=>$modelstockData,
        ]);
        }
    }

    public function actionUpdatetierimage(){
    	$id = $_GET["tier_id"];
		$model2 = TblTiers::find()->where(['tier_id' => $id])->one();
		$oldfile=$model2->image;
		$image=$_FILES['image']['name'];

		if($image!="")
		{

			$model2->updated_at=strtotime(gmdate('y-m-d H:i:s'));
			$filename=strtotime(gmdate('y-m-d H:i:s'))."_".$image;
			move_uploaded_file($_FILES['image']['tmp_name'], 'uploads/' . $filename);
			$model2->image=$filename;

			if($model2->save())
			{
				$data = 1;
				Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
				return $data;
			}

		}
		else
		{
			$model2->image=$oldfile;

			$model2->updated_at=strtotime(gmdate('y-m-d H:i:s'));

			if($model2->save())
			{
				$data = 1;
				Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
				return $data;
			}

		}
    }



   public function actionCreateproduct()
   {
            $model = new TblProducts();   
            //$modelstock =new TblProductInventory(); 
            $modelstock =new TblProductStock();    
            $modelstockData = TblSheen::find()->all();

          //       echo "<pre>";
          // print_r($_POST);
          // exit();
           if ($model->load(Yii::$app->request->post()) && $model->validate()) {
       
                    $file=$_FILES['image']['name'];
                    $filetype=$_FILES['image']['type'];
                    $product=$_POST['stock_sheen'];

                    // echo '<pre>'; //print_r($product); 

                                  
                    // exit("---line 540----");

                    $filename=strtotime(gmdate('y-m-d H:i:s'))."_".$file;
                    $model->created_at=strtotime(gmdate('y-m-d H:i:s'));

            if($model->save())
           {

                  if(isset($product))
                                {
                                    foreach ($product as $value) {
                                    //  print_r($value);
                                         $model3 =new TblProductStock(); 
                                         // if($value['code']=="")
                                         // {
                                         //   $value['code'] = 'PP-DUMMY-1234';
                                         // }
                                          $model3->sheen_id=$value['stock'];
                                         //$model3->litre=$pid;
                                          $model3->litres=$value['litre'];
                                          $model3->code=$value['code'];
                                          $model3->name=$value['name'];
                                          $model3->price=$value['price'];
                                          $model3->product_id=$model->product_id;
                                          $model3->created_at=strtotime(gmdate('y-m-d H:i:s'));
                                          $model3->updated_at=strtotime(gmdate('y-m-d H:i:s'));
                                          $model3->save();
                                   }
                                }
                     

                    if($file!="")
                    {

                          $id=$model->product_id;
                          $model2 = TblProducts::find()->where(['product_id' => $id])->one();


                          move_uploaded_file($_FILES['image']['tmp_name'], 'uploads/' . $filename);
                          $model2->image=$filename;
                          if($model2->save())
                          {    
                            $data = 1;
                            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                           
                          }
                    }
                    else
                    {
                            $data = 1;
                            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                           
                    }


                      return $data;

            }
            else
            {
                $data = 0;
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return $data;
            }
            
        }
        else
        {   

            return $this->renderAjax('createproduct', [
                'model' => $model,
                //'model2'=>$model2,
                'modelstock'=>$modelstock,
                'modelstockData'=>$modelstockData,
            ]);

        }

        
   }


   public function actionValidateproduct()

   {

            Yii::$app->response->format = Response::FORMAT_JSON;
            $model = new TblProducts();

           
            $model->load(Yii::$app->request->post());
            //print_r(Yii::$app->request->post()); exit();
            return ActiveForm::validate($model);
   }




   public function actionValidateproductupdate($id)

   {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $model = TblProducts::find()->where(['product_id' => $id])->one();

           
            $model->load(Yii::$app->request->post());
            // echo "<pre>";print_r(Yii::$app->request->post()); exit();
            return ActiveForm::validate($model);


   }


    
    public function actionCreatesheen()
    {

            $model = new TblSheen();

            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
               $model->created_at=strtotime(gmdate('y-m-d H:i:s'));
            if($model->save())
            {
                $data = 1;
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return $data;
            }
            else
            {
                $data = 0;
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return $data;
            }
            
        }
        else
        {   

            return $this->renderAjax('createsheen', [
            'model' => $model,
                ]);

       }
    }




    public function actionUpdatesheen($id)
    {
        $model = TblSheen::find()->where(['sheen_id' => $id])->one();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
           $model->updated_at=strtotime(gmdate('y-m-d H:i:s'));
            if($model->save())
            {
                $data = 1;
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return $data;
            }
            else
            {
                $data = 0;
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return $data;
            }
        }
        else
        {

               return $this->renderAjax('updatesheen', [
               'model' => $model,
               'id'=>$id,
        ]);
      }
    }




    public function actionValidatesheen()

   {

            Yii::$app->response->format = Response::FORMAT_JSON;
            $model = new TblSheen();

           
            $model->load(Yii::$app->request->post());
            //print_r(Yii::$app->request->post()); exit();
            return ActiveForm::validate($model);
   }



   public function actionValidatesheenupdate($id)

   {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $model = TblSheen::find()->where(['sheen_id' => $id])->one();

           
            $model->load(Yii::$app->request->post());
            //print_r(Yii::$app->request->post()); exit();
            return ActiveForm::validate($model);


   }


   public function actionSortsheen()
   {
     $model = TblSheen::find() ->orderBy([  'sheen_order_id'=>SORT_ASC])->all();

      return $this->renderAjax('sortsheen',['model'=>$model]);
   }



   
    

    public function actionCreateprep()
    {
          $model = new TblPrepLevel();

            //echo '<pre>'; print_r(Yii::$app->request->post()); exit();
          if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            $model->created_at=strtotime(gmdate('y-m-d H:i:s'));

            $data = Yii::$app->request->post();
            $default = $data['TblPrepLevel']['is_default'];

            if($default == 1){
              TblPrepLevel::updateAll(['is_default' => 0]);
            }

            if($model->save())
            {
                $data = 1;
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return $data;
            }
             else
            {
                 $data = 0;
                 Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                 return $data;
            }
                        
    }
           else
           {   

               return $this->renderAjax('createprep', [
               'model' => $model,
                      ]);

           }
    }
    public function actionUpdateprep($id)
    {
        $model = TblPrepLevel::find()->where(['prep_id' => $id])->one();

         if ($model->load(Yii::$app->request->post()) && $model->validate()) {
           $model->updated_at=strtotime(gmdate('y-m-d H:i:s'));

              $data = Yii::$app->request->post();
              $default = $data['TblPrepLevel']['is_default'];

              if($default == 1){
                TblPrepLevel::updateAll(['is_default' => 0]);
              }
           
            if($model->save())
            {
                    //yii::$app->session->setFlash('ravi','YOU HAVE REGISTERED SUCCESSFULLY');
                    $data = 1;
                    Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                    return $data;
            }
           
                        
        }
        else
          {   

             return $this->renderAjax('updateprep', [
             'model' => $model,
             'id' =>$id,
                    ]);

          }
      
    }



    public function actionValidateprep() 
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = new TblPrepLevel();
        $model->load(Yii::$app->request->post());
        //print_r(Yii::$app->request->post()); exit();
        return ActiveForm::validate($model);
    }


    public function actionValidateprepupdate($id) 
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

       $model = TblPrepLevel::find()->where(['prep_id' => $id])->one();
        $model->load(Yii::$app->request->post());
        //print_r(Yii::$app->request->post()); exit();
        return ActiveForm::validate($model);
    }



     public function actionCreatecomponent()
     {
        $model = new TblComponentGroups();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) 
        {
            
           $file=$_FILES['image']['name'];
           $filetype=$_FILES['image']['type'];
           $filename=strtotime(gmdate('y-m-d H:i:s'))."_".$file;     
           $model->created_at=strtotime(gmdate('y-m-d H:i:s'));
            if($model->save())
            {
                if($file!="")
                    {
                          $id=$model->group_id;
                          $model2 = TblComponentGroups::find()->where(['group_id' => $id])->one();
                          move_uploaded_file($_FILES['image']['tmp_name'], 'uploads/' . $filename);
                          $model2->tt_image=$filename;
                          if($model2->save())
                          {    
                            $data = 1;
                            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                           
                          }
                    }
                    else
                    {
                            $data = 1;
                            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                           
                    }
                      return $data;
            }
             else
           {
                 $data = 0;
                 Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                 return $data;
           }
                        
        }
         else
          {   

             return $this->renderAjax('createcomponent', [
             'model' => $model,
                    ]);

          }
     } 

     public function actionUpdatecomponent($id)
     {
   
        $model = TblComponentGroups::find()->where(['group_id' => $id])->one();

         if ($model->load(Yii::$app->request->post()) && $model->validate()) {
           $model->updated_at=strtotime(gmdate('y-m-d H:i:s'));
            if($model->save())
            {
                $model2 = TblComponentGroups::find()->where(['group_id' => $id])->one();
                      $oldfile=$model2->tt_image;
                      $tt_image=$_FILES['image']['name'];
                      $ext = pathinfo($tt_image, PATHINFO_EXTENSION);

                      if($tt_image!="")
                      {

                          $model2->updated_at=strtotime(gmdate('y-m-d H:i:s'));
                          // $filename=strtotime(gmdate('y-m-d H:i:s'))."_".$tt_image;
                          $filename=strtotime(gmdate('y-m-d H:i:s')).'.'.$ext;
                          move_uploaded_file($_FILES['image']['tmp_name'], 'uploads/' . $filename);
                          $model2->tt_image=$filename;
                          
                          if($model2->save())
                          {
                              $data = 1;
                              Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                              return $data;
                          }

                      }
                      else
                      {
                          $model2->tt_image = $oldfile;
                        
                          $model2->updated_at=strtotime(gmdate('y-m-d H:i:s'));
                           
                         if($model2->save())
                          {
                              $data = 1;
                              Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                              return $data;
                          }

                      }
            }
             else
            {
                 $data = 0;
                 Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                 return $data;
            }
                        
        }
         else
          {   

             return $this->renderAjax('updatecomponent', [
             'model' => $model,
             'id'=>$id,
                    ]);

          }

     }


      public function actionValidatecomponent()

   {

            Yii::$app->response->format = Response::FORMAT_JSON;
            $model = new TblComponentGroups();

           
            $model->load(Yii::$app->request->post());
            //print_r(Yii::$app->request->post()); exit();
            return ActiveForm::validate($model);
   }



   public function actionValidatecomponentupdate($id)

   {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $model = TblComponentGroups::find()->where(['group_id' => $id])->one();

           
            $model->load(Yii::$app->request->post());
            //print_r(Yii::$app->request->post()); exit();
            return ActiveForm::validate($model);


   }
   

     public function actionCreateroom()
     {
        $model = new TblRoomTypes();
          if ($model->load(Yii::$app->request->post()) && $model->validate()) {
           $model->created_at=strtotime(gmdate('y-m-d H:i:s'));
            if($model->save())
            {
                $data = 1;
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return $data;
            }
             else
           {
                 $data = 0;
                 Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                 return $data;
           }
                        
        }
         else
          {   

             return $this->renderAjax('createroom', [
             'model' => $model,
                    ]);

          }

      }
       public function actionUpdateroom($id)
     {
          $model = TblRoomTypes::find()->where(['room_id' => $id])->one();

         if ($model->load(Yii::$app->request->post()) && $model->validate()) {
           $model->updated_at=strtotime(gmdate('y-m-d H:i:s'));
            if($model->save())
            {
                $data = 1;
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return $data;
            }
             else
           {
                 $data = 0;
                 Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                 return $data;
           }
                        
        }
         else
          {   

             return $this->renderAjax('updateroom', [
             'model' => $model,
             'id'=>$id,
                    ]);

          }

     }




     public function actionValidateroom()

   {

            Yii::$app->response->format = Response::FORMAT_JSON;
            $model = new Tblroomtypes();

           
            $model->load(Yii::$app->request->post());
            //print_r(Yii::$app->request->post()); exit();
            return ActiveForm::validate($model);
   }



   public function actionValidateroomupdate($id)

   {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $model = TblRoomTypes::find()->where(['room_id' => $id])->one();

           
            $model->load(Yii::$app->request->post());
            //print_r(Yii::$app->request->post()); exit();
            return ActiveForm::validate($model);


   }


   public function actionSortroom()
   {
         $model = TblRoomTypes::find() ->orderBy([  'room_order_id'=>SORT_ASC])->all();

        return $this->renderAjax('sortroom',['model'=>$model]);
   }
     
     public function actionCreatecolor()
     {
         $model = new TblColorTags();
          if ($model->load(Yii::$app->request->post()) && $model->validate(false)) {
           $model->created_at=strtotime(gmdate('y-m-d H:i:s'));
            if($model->save(false))
            {
                $data = 1;
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return $data;
            }
             else
           {
                 $data = 0;
                 Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                 return $data;
           }
                        
        }
         else
          {   

             return $this->renderAjax('createcolor', [
             'model' => $model,
                    ]);

          }
     }    
     public function actionUpdatecolor($id)
     {
         $model = TblColorTags::find()->where(['tag_id' => $id])->one();
         if ($model->load(Yii::$app->request->post()) && $model->validate()) {
           $model->updated_at=strtotime(gmdate('y-m-d H:i:s'));
            if($model->save())
            {
                $data = 1;
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return $data;
            }
             else
           {
                 $data = 0;
                 Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                 return $data;
           }
                        
        }
         else
          {   

             return $this->renderAjax('updatecolor', [
             'model' => $model,
             'id'=>$id,
                    ]);

          }


     }




     public function actionValidatecolor()

   {

            Yii::$app->response->format = Response::FORMAT_JSON;
            $model = new TblColorTags();

           
            $model->load(Yii::$app->request->post());
            //print_r(Yii::$app->request->post()); exit();
            return ActiveForm::validate($model);
   }



   public function actionValidatecolorupdate($id)

   {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $model = TblColorTags::find()->where(['tag_id' => $id])->one();

           
            $model->load(Yii::$app->request->post());
            //print_r(Yii::$app->request->post()); exit();
            return ActiveForm::validate($model);


   }

     public function actionCreatespecial()
     {
             $model = new TblSpecialItems();
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->created_at=strtotime(gmdate('y-m-d H:i:s'));
            if($model->save())
            {
                $data = 1;
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return $data;
            }
             else
           {
                 $data = 0;
                 Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                 return $data;
           }
                        
        }
         else
          {   

             return $this->renderAjax('createspecial', [
             'model' => $model,
                    ]);

          }
     }
      public function actionUpdatespecial($id)
      {
         $model = TblSpecialItems::find()->where(['item_id' => $id])->one();
         if ($model->load(Yii::$app->request->post()) && $model->validate()) {
           $model->updated_at=strtotime(gmdate('y-m-d H:i:s'));
            if($model->save())
            {
                $data = 1;
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return $data;
            }
             else
           {
                 $data = 0;
                 Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                 return $data;
            }
                         
          }
          else
          {   

             return $this->renderAjax('updatespecial', [
             'model' => $model,
             'id' =>$id,
                    ]);

          }
      } 



      public function actionValidatespecial()

   {

            Yii::$app->response->format = Response::FORMAT_JSON;
            $model = new TblSpecialItems();

           
            $model->load(Yii::$app->request->post());
            //print_r(Yii::$app->request->post()); exit();
            return ActiveForm::validate($model);
   }



   public function actionValidatespecialupdate($id)

   {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $model = TblSpecialItems::find()->where(['item_id' => $id])->one();

           
            $model->load(Yii::$app->request->post());
            //print_r(Yii::$app->request->post()); exit();
            return ActiveForm::validate($model);


   }




      public function actionCreatestrength()
      {
             $model = new TblStrengths();
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->created_at=strtotime(gmdate('y-m-d H:i:s'));
            if($model->save())
            {
                $data = 1;
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return $data;
            }
             else
           {
                 $data = 0;
                 Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                 return $data;
           }
                        
        }
         else
          {   

             return $this->renderAjax('createstrength', [
             'model' => $model,
                    ]);

          }
             

      }
      public function actionUpdatestrength($id)
      {
           $model = TblStrengths::find()->where(['strength_id' => $id])->one();
         if ($model->load(Yii::$app->request->post()) && $model->validate()) {
           $model->updated_at=strtotime(gmdate('y-m-d H:i:s'));
            if($model->save())
            {
                $data = 1;
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return $data;
            }
             else
           {
                 $data = 0;
                 Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                 return $data;
            }
                         
          }
          else
          {   

             return $this->renderAjax('updatestrength', [
             'model' => $model,
             'id'=>$id,
                    ]);

          }


      }

       public function actionValidatestrength()

   {

            Yii::$app->response->format = Response::FORMAT_JSON;
            $model = new TblStrengths();

           
            $model->load(Yii::$app->request->post());
            //print_r(Yii::$app->request->post()); exit();
            return ActiveForm::validate($model);
   }



   public function actionValidatestrengthupdate($id)

   {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $model = TblStrengths::find()->where(['strength_id' => $id])->one();

           
            $model->load(Yii::$app->request->post());
            //print_r(Yii::$app->request->post()); exit();
            return ActiveForm::validate($model);


   }



   public function actionCreatecomponentmain()
   {


      $model = new TblComponents();
      $model2 =[new TblComponentType];
      $model3 =[new TblComponentType];
      $model4 =[new TblComponentType];

      
      if ($model->load(Yii::$app->request->post())) {
             
           //   echo "<pre>";
           //  print_r($_POST['TblComponents']);
           // // print_r($_FILES);
           //  exit("--line 1473--");
                //$componentData = $_POST['']
             $file=$_FILES['image']['name'];
             $filetype=$_FILES['image']['type'];
             // echo "<pre>";
             // print_r($_FILES);
             // exit();
             $filename=strtotime(gmdate('y-m-d H:i:s'))."_".$file;   
             $model->created_at=strtotime(gmdate('y-m-d H:i:s'));  
             $model->updated_at=strtotime(gmdate('y-m-d H:i:s'));  
             $data = 1;
             //$priceId = $_POST['price_method_id'];
             //$componentTypeModel = $_POST['TblComponentType'];
             $model->tt_url = $_POST['TblComponents']['tt_url'];
             $model->tt_text = $_POST['TblComponents']['tt_text'];
              if($model->save())
              {
                     $id = $model->comp_id;
                     if($file!="")
                    {
                          $model2 = TblComponents::find()->where(['comp_id' => $id])->one();
                          move_uploaded_file($_FILES['image']['tmp_name'], 'uploads/' . $filename);
                          $model2->tt_image=$filename;
                          if($model2->save())
                          {    
                            $data = 1;
                            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                            return $data;
                           
                          }
                    }
                    else
                    {
                            $data = 1;
                            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                            return $data;
                           
                    }
                      //return $data;
                    // foreach ($componentTypeModel as  $value) 
                    // {
                    //     if(!isset($value['spread_ratio']))
                    //     {
                    //       $value['spread_ratio'] = 0;
                    //     }
                    //     if(!isset($value['excl_area']))
                    //     {
                    //       $value['excl_area'] = 0;
                    //     }
                    //     if(!isset($value['thickness']))
                    //     {
                    //       $value['thickness'] = 0;
                    //     }
                    //     $componentType = new TblComponentType(); 
                    //     $componentType->comp_id = $id;
                    //     $componentType->name = $value['name'];
                    //     $componentType->work_rate = $value['work_rate'];
                    //     $componentType->spread_ratio = $value['spread_ratio'];
                    //     $componentType->is_default = $value['is_default'];
                    //     $componentType->excl_area = $value['excl_area'];
                    //     $componentType->thickness = $value['thickness'];
                    //     $componentType->created_at = strtotime(gmdate('y-m-d H:i:s'));  
                    //     $componentType->updated_at = strtotime(gmdate('y-m-d H:i:s'));  
                    //     $componentType->save();                       
                    // }
                   
             }
            //      $model2 = Model::createMultiple(TblComponentType::classname());
            //      Model::loadMultiple($model2, Yii::$app->request->post());
            //  // print_r(Yii::$app->request->post());
            // // exit();

            // // validate all models
            // $valid = $model->validate();
            // $valid = Model::validateMultiple($model2) && $valid;
            
            // if ($valid) {
                
               
            //         if ($flag = $model->save()) {
            //             foreach ($model2 as $model2) {
            //                                 $model2->created_at=strtotime(gmdate('y-m-d H:i:s'));  
            //                 $model2->comp_id = $model->comp_id;
            //                  $model2->save();
            //                    /// $transaction->rollBack();
            //                     }
            //                 }
            //             }

                    
            //         if ($flag = $model->save()) {
            //             //$transaction->commit();
            //             Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            //             return $data;
            //         }
                
            } 
              return $this->renderAjax('createcomponentmain', [
                    'model' => $model,
             
                    'model2' => (empty($model2)) ? [new TblComponentType] : $model2,
                    'model3' => (empty($model3)) ? [new TblComponentType] : $model3,
                    'model4' => (empty($model4)) ? [new TblComponentType] : $model4,

                        ]);

          }

   

   public function actionValidatecomponentmain()
   {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $model = new TblComponents();


           
            $model->load(Yii::$app->request->post());
            //print_r(Yii::$app->request->post()); exit();
            return ActiveForm::validate($model);
   }


   public function actionUpdatecomponentmain($id)
   {
             
                  
               
         $model = $this->findModel($id);
         $model2 = $model->tblComponentTypes;
         $data=1;
         $model3 = $model->tblComponentTypes;
         $model4 = $model->tblComponentTypes;
         $model5 = $model->tblComponentTypes;
         $model3 = [new TblComponentType];
         $methodIDs=$model->price_method_id;
         if($model->load(Yii::$app->request->post())) {
          // echo "<pre>";
          // print_r($_POST);
          // exit("1609");
            $model->calc_method_id = $_POST['TblComponents']['calc_method_id'];
            $model->tt_url = $_POST['TblComponents']['tt_url'];
            $model->tt_text = $_POST['TblComponents']['tt_text'];
            $model->updated_at=strtotime(gmdate('y-m-d H:i:s'));
            $model->save();
            $modelImageSave = TblComponents::find()->where(['comp_id' => $id])->one();
            $oldfile=$modelImageSave->tt_image;
            $tt_image=$_FILES['image']['name'];
            $ext = pathinfo($tt_image, PATHINFO_EXTENSION);
            if($tt_image!="")
            {

                $modelImageSave->updated_at=strtotime(gmdate('y-m-d H:i:s'));
                // $filename=strtotime(gmdate('y-m-d H:i:s'))."_".$tt_image;
                $filename=strtotime(gmdate('y-m-d H:i:s')).'.'.$ext;
                move_uploaded_file($_FILES['image']['tmp_name'], 'uploads/' . $filename);
                $modelImageSave->tt_image=$filename;

                if($modelImageSave->save())
                {
                    //$data = 1;
                    Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                    
                }

            }
            else
            {
                $modelImageSave->tt_image = $oldfile;

                $modelImageSave->updated_at=strtotime(gmdate('y-m-d H:i:s'));

                if($modelImageSave->save())
                {
                   // $data = 1;
                    Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                    
                }

            }


          if(isset($_POST['TblComponentType']))
          {
             $modelComponentUpdate = $_POST['TblComponentType'];
             $modelthird = TblComponentType::find()->where(['comp_id' => $id])->all(); 
             foreach ($modelComponentUpdate as $key => $value) {
               if($methodIDs==1)
               {
                  $modelthird[$key]->name=$value['name_update'];
                  $modelthird[$key]->work_rate=$value['work_rate_update'];
                  $modelthird[$key]->spread_ratio=$value['spread_ratio_update'];
                  $modelthird[$key]->is_default=$value['is_default_update'];
                  $modelthird[$key]->excl_area=0;
                  $modelthird[$key]->thickness=0;
                  $modelthird[$key]->updated_at=strtotime(gmdate('y-m-d H:i:s'));
                  $modelthird[$key]->save(false);
               }
               if($methodIDs==2)
               {
                  $modelthird[$key]->name=$value['name_update'];               
                  $modelthird[$key]->work_rate=$value['work_rate_update'];
                  $modelthird[$key]->excl_area=$value['excl_area_update'];
                  $modelthird[$key]->spread_ratio=$value['spread_ratio_update'];
                  $modelthird[$key]->fixed_area=$value['fixed_area'];
                  $modelthird[$key]->is_default=$value['is_default_update'];
                  $modelthird[$key]->thickness=0;
                  $modelthird[$key]->updated_at=strtotime(gmdate('y-m-d H:i:s'));
                  $modelthird[$key]->save(false);
               }
               if($methodIDs==3)
               {
                  $modelthird[$key]->name=$value['name_update'];                 
                  $modelthird[$key]->work_rate=$value['work_rate_update'];
                  $modelthird[$key]->spread_ratio=$value['spread_ratio_update'];
                  $modelthird[$key]->thickness=$value['thickness_update'];
                  $modelthird[$key]->is_default=$value['is_default_update'];
                  $modelthird[$key]->excl_area=0;
                  $modelthird[$key]->updated_at=strtotime(gmdate('y-m-d H:i:s'));
                  $modelthird[$key]->save(false);
               }
             }
          }
          $newMethod = $_POST['TblComponents']['price_method_id'];


          if($methodIDs!=$newMethod)
          {
            Yii::$app
                ->db
                ->createCommand()
                ->delete('tbl_component_type', ['comp_id'=>$id])
                ->execute();
            //print_r($_POST);
            $newType=$_POST['newType'];
           // print_r($newType);
            foreach($newType as $key=>$newType)
            {
                $componentType = new TblComponentType(); 
                if($newType['is_default']=='on')
                {
                  $newType['is_default']=1;
                }
                else if(!isset($newType['is_default']))
                {
                  $newType['is_default']=0;
                }
                if($newMethod==1)
                {
                  $componentType->comp_id=$id;
                  $componentType->name=$newType['name'];
                  $componentType->work_rate=$newType['work_rate'];
                  $componentType->spread_ratio=$newType['spread_ratio'];
                  if (!empty($newType['is_default'])) {
                      $componentType->is_default=$newType['is_default'];
                  }
                  
                  $componentType->thickness=0;
                  $componentType->excl_area=0;
                  $componentType->save();
                }
                if($newMethod==2)
                {
                  $componentType->comp_id=$id;
                  $componentType->name=$newType['name'];
                  $componentType->work_rate=$newType['work_rate'];
                  $componentType->thickness=0;
                   if (!empty($newType['is_default'])) {
                      $componentType->is_default=$newType['is_default'];
                  }
                  // $componentType->is_default=$newType['is_default'];
                  $componentType->spread_ratio=$newType['spread_ratio'];
                  $componentType->fixed_area=$newType['fixed_area'];
                  $componentType->excl_area=$newType['excl_area'];
                  $componentType->save();
                }
                if($newMethod==3)
                {
                  $componentType->comp_id=$id;
                  $componentType->name=$newType['name'];
                  $componentType->work_rate=$newType['work_rate'];
                  $componentType->spread_ratio=$newType['spread_ratio'];
                  // $componentType->is_default=$newType['is_default'];
                   if (!empty($newType['is_default'])) {
                      $componentType->is_default=$newType['is_default'];
                  }
                  $componentType->thickness=$newType['thickness'];
                  $componentType->excl_area=0;
                  $componentType->save();
                }

            }
         }
// echo "<pre>";
// print_r($_POST['new']);
// exit();
         if(isset($_POST['new']))
         {
            $newTypeData=$_POST['new'];
            
            foreach ($newTypeData as $key => $new) {
              $componentTypes = new TblComponentType(); 
                if(isset($new['checkbox']) && $new['checkbox']=='on')
                {
                  $new['checkbox']=1;
                }
                else if(!isset($new['checkbox']))
                {
                  $new['checkbox']=0;
                }
                if($methodIDs==1)
                    {
                      $componentTypes->comp_id=$id;
                      $componentTypes->name=$new['name'];
                      $componentTypes->work_rate=$new['work_rate'];
                      $componentTypes->spread_ratio=$new['spread_ratio'];
                      $componentTypes->is_default=$new['checkbox'];
                      $componentTypes->thickness=0;
                      $componentTypes->excl_area=0;
                      $componentTypes->save();
                    }
                if($methodIDs==2)
                {
                  $componentTypes->comp_id=$id;
                  $componentTypes->name=$new['name'];
                  $componentTypes->work_rate=$new['work_rate'];
                  $componentTypes->thickness=0;
                  $componentTypes->is_default=$new['checkbox'];
                  $componentTypes->spread_ratio=$new['spread_ratio'];
                  $componentTypes->fixed_area=$new['fixed_area'];
                  $componentTypes->excl_area=$new['excl_area'];
                  $componentTypes->save();
                }
                if($methodIDs==3)
                {
                  $componentTypes->comp_id=$id;
                  $componentTypes->name=$new['name'];
                  $componentTypes->work_rate=$new['work_rate'];
                  $componentTypes->spread_ratio=$new['spread_ratio'];
                  $componentTypes->is_default=$new['checkbox'];
                  $componentTypes->thickness=$new['thickness'];
                  $componentTypes->excl_area=0;
                  $componentTypes->save();
                }
                }
         
             }
             return $data;
           }
         else
             {
               return $this->renderAjax('updatecomponentmain', [
                    'model' => $model,
                    'model2' => (empty($model2)) ? [new TblComponentType] : $model2,
                    //'model3' => (empty($model3)) ? [new TblComponentType] : $model3,
                    'id'=>$id,
                    'model3'=>(empty($model3)) ? [new TblComponentType] : $model3,
                    'model4'=>(empty($model4)) ? [new TblComponentType] : $model4,
                    'model5'=>(empty($model5)) ? [new TblComponentType] : $model5,
                ]);            
             }
     }



     public function actionSortcomponent()
     {
        $model = TblComponents::find() ->orderBy([  'component_order_id'=>SORT_ASC])->all();

        return $this->renderAjax('sortcomponent',['model'=>$model]);
     }


   public function actionCreatecolorsmain()
   {
        $model= new TblColors();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
           $model->created_at=strtotime(gmdate('y-m-d H:i:s'));
            if($model->save())
            {
                $data = 1;
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return $data;
            }
            else
            {
                $data = 0;
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return $data;
            }
            
        }
        else
        {   

        return $this->renderAjax('createcolorsmain',['model'=>$model]);
        }

   }


   public function actionUpdatecolorsmain($id)
   {
        $model = TblColors::find()->where(['color_id' => $id])->one();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
           $model->updated_at=strtotime(gmdate('y-m-d H:i:s'));
            if($model->save())
            {
                $data = 1;
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return $data;
            }
             else
           {
                 $data = 0;
                 Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                 return $data;
            }
                         
          }
          else
          {   

             return $this->renderAjax('updateindexcolorsmain', [
             'model' => $model,
             'id'=>$id,
                    ]);

          }
   }


   public function actionValidatecolorsmain()
   {
         Yii::$app->response->format = Response::FORMAT_JSON;
            $model = new TblColors();

           
            $model->load(Yii::$app->request->post());
            //print_r(Yii::$app->request->post()); exit();
            return ActiveForm::validate($model);
   }


    public function actionValidatecolorsupdatemain($id)
   {
         Yii::$app->response->format = Response::FORMAT_JSON;
         $model = TblColors::find()->where(['color_id' => $id])->one();

           
            $model->load(Yii::$app->request->post());
            //print_r(Yii::$app->request->post()); exit();
            return ActiveForm::validate($model);
   }



   public function actionCreatetier()
   {
       $model= new TblTiers();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
           $model->created_at=strtotime(gmdate('y-m-d H:i:s'));
            if($model->save())
            {
                $data = 1;
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return $data;
            }
            else
            {
                $data = 0;
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return $data;
            }
            
        }
        else
        {   

        return $this->renderAjax('createTier',['model'=>$model]);
        }
   }
   public function actionValidatetier()
   {
          Yii::$app->response->format = Response::FORMAT_JSON;
            $model = new TblTiers();

           
            $model->load(Yii::$app->request->post());
            //print_r(Yii::$app->request->post()); exit();
            return ActiveForm::validate($model);
   }



   public function actionUpdatetier($id)
   {
  		$model = TblTiers::find()->where(['tier_id' => $id])->one();
  		//$model1=TblComponentGroups::find()->where(['type_id'=>1 or'type_id'=>3 ])->all();
      if($model->type_id != 3){
        $model1 = TblComponentGroups::find()->where(['type_id' => [$model->type_id,3]])->all();
      }else{
        $model1 = TblComponentGroups::find()->where(['type_id' => [1,2,3]])->all();
      }
      //print_r($_POST);

  		if ($model->load(Yii::$app->request->post()) && $model->validate()) {
  			$model->updated_at=strtotime(gmdate('y-m-d H:i:s'));
  			if($model->save())
  			{
  				$data = 1;
  				Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
  				return $data;
  			}
  			else
  			{
  				$data = 0;
  				Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
  				return $data;
  			}
  		}
  		else
  		{
  			return $this->renderAjax('updatetier',['model'=>$model,'id'=>$id,'model1'=>$model1]);
  		} 
   }
 

  public function actionData()
  {
    
     if(isset($_POST) && $_POST['cid'] !='' && $_POST['tid']!=''){
             $cid=$_POST['cid'];
             $tid=$_POST['tid'];
             $modelSheen = TblSheen::find()->all();
          
             $model=TblTierCoats::find()->where(['tier_id'=>$tid , 'comp_group_id'=>$cid])->all();
             $modelthird=TblTierCoats::find()                         
                          ->joinWith('product') 
                          ->where(['tier_id'=>$tid , 'comp_group_id'=>$cid,'sheen_id'=>0])->all();

                          // print_r($modelthird);die;
             $model2 = TblTierCoats::find()
                          ->joinWith('sheen')
                          ->joinWith('product') 
                          ->where(['tier_id'=>$tid , 'comp_group_id'=>$cid])->all();
         
             $model1= [new TblTierCoats()];
             
                      return $this->renderAjax('showdiv',['cid'=>$cid,'tid'=>$tid, 
                      'model'=>(empty($model)) ? [new TblTierCoats] : $model,
                      'model2'=>$model2,
                      'model3'=>$modelthird,
                      'modelSheen'=>$modelSheen,
                     
                      


                ]);

      }
  }


  

  public function actionShowproduct($id)
  {   
      if(isset($_GET['alreadyProduct']) && $_GET['alreadyProduct'] != "") {
        $exitProduct = explode('-', $_GET['alreadyProduct']);
        $model = TblProducts::find()->where(['not in','product_id',$exitProduct])->all();
      } else {
        $model = TblProducts::find()->all();
      }
      return $this->renderAjax("showproduct",['model'=>$model,'id'=>$id]);
  }


  public function actionDeletetiercoat()
  {
       if($_POST['cid'] !='' && $_POST['tid']!='' && $_POST['id']!=''){
        $data =1;
       
       $cid=$_POST['cid'];
       $tid=$_POST['tid'];
      
       $id =$_POST['id'] ;
      if($_POST['pid']!="")
      {
         $pid=$_POST['pid'];
         $model =Yii::$app
                ->db
                ->createCommand()
                ->delete('tbl_tier_coats', ['comp_group_id'=>$cid,'tier_id'=>$tid,'sheen_id'=>$pid,'product_id'=>$id])
                ->execute();
      }
      else
      {
         //$pid=NULL;
         $model =Yii::$app
                ->db
                ->createCommand()
                ->delete('tbl_tier_coats', ['comp_group_id'=>$cid,'tier_id'=>$tid,'product_id'=>$id])
                ->execute();
      }
        if($model==1)
        {
            return $data;
        }

      }
  }



  public function actionAddsheen()
  {
    
     if(isset($_GET['sheen']) && $_GET['sheen'] != "") {
        $exitProduct = explode('-', $_GET['sheen']);
      
        $model = TblSheen::find()->where(['not in','sheen_id',$exitProduct])->all();

     }
        else
        {
           $model = TblSheen::find()->all();
        }
           

      return $this->renderAjax("sheenshow",['model'=>$model]);
  }




  public function actionSavetier()
  {
        // echo "<pre>";
        // print_r($_POST);
        // exit("--2083--");
             $data=1;
             $cid=Yii::$app->request->post('cid');
             $tid=Yii::$app->request->post('tid');
             $model2 = TblTierCoats::find()
                          ->joinWith('sheen')
                          ->joinWith('product') 
                          ->where(['tier_id'=>$tid , 'comp_group_id'=>$cid])->all();     
            $grouped=[];
            $undercoat = (isset($_POST['under_coat']))?$_POST['under_coat']:[];
            $topcoatEnabled = (isset($_POST['topcoatEnabled']))?$_POST['topcoatEnabled']:[];
            $field_name = (isset($_POST['field_name']))?$_POST['field_name']:[];
            $under_coatEnabled = (isset($_POST['under_coatEnabled']))?$_POST['under_coatEnabled']:[];
            $under_coat = (isset($_POST['under_coat']))?$_POST['under_coat']:[];
            $sheenEnabled = (isset($_POST['sheenEnabled']))?$_POST['sheenEnabled']:[];
            /*print_r($undercoat);
            print_r($under_coatEnabled);*/
            if(!empty($undercoat))
            {                  
                  foreach($undercoat as $key => $value)
                  {
                  	// echo"<br>";
                   //  echo $key;
                          $modelfour= new TblTierCoats();
                          $modelfour->sheen_id=0;
                          $modelfour->product_id=$key;
                          $modelfour->comp_group_id= $cid;
                          $modelfour->tier_id=$tid;
                          $modelfour->top_coats=0;
                          $modelfour->created_at=strtotime(gmdate('y-m-d H:i:s'));
                          $modelfour->updated_at=strtotime(gmdate('y-m-d H:i:s'));
                          $modelfour->save(false);  
                          
                  }
            }
            if(!empty($under_coatEnabled))
            {
                $modelsixth = TblTierCoats::find()->where(['tier_id' => $tid ,'comp_group_id'=>$cid,'sheen_id'=>0])->all();
                // echo "<pre>";
                // print_r($under_coatEnabled);
                // exit();
                foreach($modelsixth as $key=>$model)
                {                  
	                 $model->product_default=0;
	                 $model->save(false);                     
                }

                foreach ($under_coatEnabled as $key => $value) 
                {
                	$modelUnderCoatEnabled=TblTierCoats::find()
                	                       ->where(['tier_id' => $tid ,
                	                       					'comp_group_id'=>$cid,
                	                       					'sheen_id'=>0,
                	                       					'product_id'=>$key])
                	                     						 ->one();
     						 // echo "<pre>";
     						  $modelUnderCoatEnabled->product_default=1;
     						  $modelUnderCoatEnabled->save(false);
     						  //echo "<pre>";
                }
            }
           if(!empty($field_name))
           {
           		foreach ($field_name as $key => $value) 
           		{
           			foreach ($value as $k => $val) 
           			{
           				$modelFieldName = new TblTierCoats();

           				$modelFieldName->sheen_id = $key;
           				$modelFieldName->product_id = $k;
           				$modelFieldName->comp_group_id = $cid;
           				$modelFieldName->tier_id = $tid;
           				$modelFieldName->top_coats = 1;
									$modelFieldName->created_at=strtotime(gmdate('y-m-d H:i:s'));
									$modelFieldName->updated_at=strtotime(gmdate('y-m-d H:i:s'));
									$modelFieldName->save(false);
           			}

           		}
           }
           if(!empty($topcoatEnabled))
           {
           	// echo "<pre>";
           	// print_r($topcoatEnabled);
           		foreach ($topcoatEnabled as $keyTop => $valueTop) 
           		{
								 // echo "<pre>";
         //   			 print_r($valueTop);
         //   			 echo "<br>";
         			   $modelseventh = TblTierCoats::find()->where(['tier_id' => $tid ,'comp_group_id'=>$cid,'sheen_id'=>$keyTop,'top_coats'=>1])->all();
         			  // print_r($modelseventh);
         			   foreach ($modelseventh as $k => $v) 
         			   {
         			   		// echo "<pre>";
         			   		// print_r($v);	
         			   		// echo "<br>";
         			   		$v->product_default=0;
         			   		$v->save(false);
         			   }
         			   foreach ($valueTop as $k => $val) 
         			   {
         			   		$modelTopCoatEnabled=TblTierCoats::find()
                	                       ->where(['tier_id' => $tid ,
                	                       					'comp_group_id'=>$cid,
                	                       					'sheen_id'=>$keyTop,
                	                       					'product_id'=>$k])
                	                     						 ->all();
       						  // echo "<pre>";
               //  	  print_r($modelTopCoatEnabled);      
                	  foreach ($modelTopCoatEnabled as $key => $value) 
                	  {
                	  	$value->product_default=1;
                	  	$value->save(false);
         						}      						 

         			   }
         			   // foreach ($variable as $key => $value) 
         			   // {
         			   		
         			   // }
           			
           			//print_r($modelseventh);
           			//exit();
           		}
           }
           if(!empty($sheenEnabled))
           {
              $modeleighth = TblTierCoats::find()->where(['tier_id' => $tid ,'comp_group_id'=>$cid,'top_coats'=>1])->all();
              //echo "<pre>";
              //print_r($modeleighth);
              foreach ($modeleighth as $key => $value) 
              {
                // print_r($value['sheen_default']);
                // echo "<br>";
                $value->sheen_default = 0;
                $value->save(false);   
              }
             foreach ($sheenEnabled as $key => $value) 
             {
              //echo "<pre>";
              //print_r($key);
                
                
              $modelninth = TblTierCoats::find()->where(['tier_id' => $tid ,'comp_group_id'=>$cid,'top_coats'=>1,'sheen_id'=>$key])->all();
               // echo "<pre>";
                // print_r($modelninth[]);

                foreach ($modelninth as $key => $value) 
                {
                  //print_r($value['sheen_default']);

                   $value->sheen_default = 1;
                   $value->save(false);   
                }
                
             }
           }
           return $data;


             
  }



      public function actionDeleteundercoat()
      {
          if($_POST['cid'] !='' && $_POST['tid']!='' && $_POST['id']!=''){
                    $data =1;

                    $cid=$_POST['cid'];
                    $tid=$_POST['tid'];

                    $id =$_POST['id'] ;
               
                  
                    $model =Yii::$app
                    ->db
                    ->createCommand()
                    ->delete('tbl_tier_coats', ['comp_group_id'=>$cid,'tier_id'=>$tid,'sheen_id'=>0,'product_id'=>$id])
                    ->execute();
                
                      //$pid=NULL;
                     
                      if($model==1)
                      {
                        return $data;
                      }
                }
                
          }


   protected function findModel($id)
    {
        if (($model = TblComponents::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionUndercoatproduct()
    {
         

          if(isset($_GET['alreadyProduct']) && $_GET['alreadyProduct'] != "") {
            $exitProduct = explode('-', $_GET['alreadyProduct']);
            $model = TblProducts::find()->where(['not in','product_id',$exitProduct])->all();
          } else {
            $model = TblProducts::find()->all();
          }
           return $this->renderAjax("showproductundercoat",['model'=>$model,'id'=>$id]);
    }

    public function actionDeleteproductstock()
    {
      if(isset($_POST['id']) && $_POST['id']!='')
      {
        $id=$_POST['id'];
        $model =Yii::$app
                ->db
                ->createCommand()
                ->delete('tbl_product_stock', ['product_stock_id'=>$id])
                ->execute();
        if($model)
        {
          return 1;
        }
      }
    }

    public function actionDeletecomponenttype()
    {
      if(isset($_POST['id']) && $_POST['id']!='')
      {
        $id=$_POST['id'];
        $model =Yii::$app
                ->db
                ->createCommand()
                ->delete('tbl_component_type', ['comp_type_id'=>$id])
                ->execute();
        if($model)
        {
          return 1;
        }
      }
    }


    public function actionSelectproduct($id)
    {
		    
         
    	if(isset($_GET['array']) && $_GET['array'] != "") {
    	 		$exitProduct = explode('-', $_GET['array']);
          //$fproduct = count($exitProduct);
          $indexon = 10;
          $perpage = 50;
          if (!empty($_POST['page'])) {
              $page = $_POST['page'];
          }
          if (empty($page) or $page < 1) {
              $page = 1;
          }
          if(!empty($_POST['sp'])){
             $searchPage = $_POST['sp'];
          }
          if (empty($searchPage) or $searchPage < '') {
              $searchPage ="";
          }
          $start = ($page - 1) * $perpage;
    	 		$model = TblProducts::find()->where(['not in','product_id',$exitProduct])->andWhere(['like','name',$searchPage])->all();
          $count = count($model); 
          $total = ($count/$perpage);
          $total = ceil($total); 
         // $totalProduct[] = $total;
          
          $model2 = TblProducts::find()->where(['not in','product_id',$exitProduct])->andWhere(['like','name',$searchPage])->limit($perpage)->offset($start)->all();
          if (!empty($_POST['page'])) {
          
              $main = [];
              foreach($model2 as $model)
              {
                $main['product_id'] = $model['product_id'];
                $main['name'] = $model['name'];
                $model3[]=$main;
              }

               json_encode($model3);


          }
  	    	return $this->renderAjax('selectproduct', [
  	            'model2' =>$model2,
  	            'id'=>$id,
                'total' => $total,
                'array' => $_GET['array'],
  	         ]);
    		}
		
    }


    public function actionSelectproductundercoat()
    {
    		if(isset($_GET['array']) && $_GET['array'] != "") {
              $exitProduct = explode('-', $_GET['array']);
              //$fproduct = count($exitProduct);
              $indexon = 10;
              $perpage = 50;
              if (!empty($_POST['page'])) {
                  $page = $_POST['page'];
              }
              if (empty($page) or $page < 1) {
                  $page = 1;
              }
              if(!empty($_POST['sp'])){
                 $searchPage = $_POST['sp'];
              }
              if (empty($searchPage) or $searchPage < '') {
                  $searchPage ="";
              }
              $start = ($page - 1) * $perpage;
              $model = TblProducts::find()->where(['not in','product_id',$exitProduct])->andWhere(['like','name',$searchPage])->all();
              $count = count($model); 
              $total = ($count/$perpage);
              $total = ceil($total); 
             // $totalProduct[] = $total;
              
              $model2 = TblProducts::find()->where(['not in','product_id',$exitProduct])->andWhere(['like','name',$searchPage])->limit($perpage)->offset($start)->all();
              if (!empty($_POST['page'])) {
              
                  $main = [];
                  foreach($model2 as $model)
                  {
                    $main['product_id'] = $model['product_id'];
                    $main['name'] = $model['name'];
                    $model3[]=$main;
                  }

                   json_encode($model3);


              }
              return $this->renderAjax('selectproductundercoat', [
                    'model2' =>$model2,
                    // 'id'=>$id,
                    'total' => $total,
                    'array' => $_GET['array'],
                 ]);
            }
		
    }

    public function actionDeleteproducttopcoat()
    {
      if(isset($_POST['id']) && $_POST['id']!='')
      {
        $id=$_POST['id'];
        $model =Yii::$app
                ->db
                ->createCommand()
                ->delete('tbl_tier_coats', ['id'=>$id])
                ->execute();
        if($model)
        {
          return 1;
        }
      }
    }

    public function actionDeletesheentopcoat()
    {
      //print_r($_POST);
      if(isset($_POST['id']) && $_POST['id']!='' && isset($_POST['cid']) && $_POST['cid']!='' && isset($_POST['tid']) && $_POST['tid']!='')
      {
        $sid=$_POST['id'];
        $cid=$_POST['cid'];
        $tid=$_POST['tid'];
        $model =Yii::$app
                ->db
                ->createCommand()
                ->delete('tbl_tier_coats', ['sheen_id'=>$sid,'comp_group_id'=>$cid,'tier_id'=>$tid])
                ->execute();
        if($model)
        {
          return 1;
        }
      }
    }

    public function actionDeleteproductundercoat()
    {
      if(isset($_POST['id']) && $_POST['id']!='')
      {
        $id=$_POST['id'];
        $model =Yii::$app
                ->db
                ->createCommand()
                ->delete('tbl_tier_coats', ['id'=>$id])
                ->execute();
        if($model)
        {
          return 1;
        }
      }
    }

                        

}