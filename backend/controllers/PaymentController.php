<?php
namespace backend\controllers;

use Yii;
use yii\web\Response;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use app\models\TblPaymentMethod;
use app\models\TblPaymentMethodSearch;
use app\models\TblPaymentOptions;
use app\models\TblPaymentOptionsSearch;

class PaymentController extends Controller
{
	public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['index','createpaymentmethod','updatepaymentmethod','createpaymentterm','updatepaymentterm'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }



    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }



    public function beforeAction($action) 
    { 
        $this->enableCsrfValidation = false; 
        return parent::beforeAction($action); 
    }




    public function actionLogin()
    {
           if (!\Yii::$app->user->isGuest) {
              return $this->goHome();
           }

               $model = new LoginForm();
               if ($model->load(Yii::$app->request->post()) && $model->loginAdmin()) {
                        return $this->goBack();
           } else {
                   return $this->render('login', [
                        'model' => $model,
               ]);
           }
    }


    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }


    public function actionIndex()
    {
    	$searchModel = new TblPaymentMethodSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model = new TblPaymentMethod();


        $searchModelTerm = new TblPaymentOptionsSearch();
        $dataProviderTerm = $searchModelTerm->search(Yii::$app->request->queryParams);
        $model2 = new TblPaymentOptions();

        return $this->render('/site/payment/index',[
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model'=>$model,

            'searchModelTerm' => $searchModelTerm,
            'dataProviderTerm' => $dataProviderTerm,
            'model2'=>$model2,
        ]);

    }


    public function actionUpdatepaymentmethod($id)
    {
         $model = TblPaymentMethod::find()->where(['id' => $id])->one();
         $default = $model->is_default;
          if ($model->load(Yii::$app->request->post()) && $model->validate()) 
          {
            // echo "<pre>";
            // print_r($_POST);
            // exit();
            if($default==$_POST['TblPaymentMethod']['is_default'])
            {
               $model->updated_at=strtotime(gmdate('y-m-d H:i:s'));
               $model->save();
               $data = 1;
               return $data;
            }
            else
            {
                $model->updated_at=strtotime(gmdate('y-m-d H:i:s'));
                if($_POST['TblPaymentMethod']['is_default']==1)
                {
                    $model2 =  TblPaymentMethod::find()->all();
                    // echo "<pre>";
                    // print_r($model2);
                    // exit();
                    foreach($model2 as $key=>$value)
                    {
                      //echo "<pre>";
                      // print_r($value->is_default);
                      // echo "<br>";
                       $value->is_default = 0;
                       $value->save(false);
                    }
                    $model->is_default = 1;
                    if($model->save())
                    {
                        $data = 1;
                        //Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                        return $data;
                    }
                    else
                    {
                        $data = 0;
                        //Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                        return $data;
                    }
                }
                else
                {
                  if($model->save())
                  {
                    $data = 1;
                    //Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                    return $data;
                  }
                  else
                  {
                      $data = 0;
                      //Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                      return $data;
                  }
                }
            }
              
                        
         }
        else
        {
            return $this->renderAjax('/site/payment/updatePaymentMethod', [
                'model' => $model,
            ]);
        }
    
    }

   public function actionCreatepaymentmethod()
   {
         $model = new TblPaymentMethod();;
           //

          if ($model->load(Yii::$app->request->post()) && $model->validate()) 
         {
            // echo "<pre>";
            // print_r($_POST['TblPaymentMethod']['is_default']);
          // print_r($_POST);die;
            // exit();
            $model->created_at=strtotime(gmdate('y-m-d H:i:s'));
            if (Yii::$app->user->identity->role != 20) {
              $model->company_id = $_POST['TblPaymentMethod']['company_id'];
            }
             else{
              $model->delete = 0;
            }
            if($_POST['TblPaymentMethod']['is_default']==1)
            {
                $model2 =  TblPaymentMethod::find()->all();
                // echo "<pre>";
                // print_r($model2);
                // exit();
                foreach($model2 as $key=>$value)
                {
                    $value->is_default = 0;
                    // $value->company_id = $_POST['TblPaymentMethod']['company_id'];
                    $value->save(false);
                }
                $model->is_default = 1;
                // $model->company_id = $_POST['TblPaymentMethod']['company_id'];
                if($model->save())
                {
                    $data = 1;
                    //Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                    return $data;
                }
                else
                {
                    $data = 0;
                    //Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                    return $data;
                }
            }
            else
            {
                if($model->save())
                {
                    $data = 1;
                    //Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                    return $data;
                }
                else
                {
                    $data = 0;
                    //Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                    return $data;
                }
            }
            
            
        }
        else
        {
            return $this->renderAjax('/site/payment/createPaymentMethod', [
                'model' => $model,
            ]);
        }
    
    }

    public function actionCreatepaymentterm()
    {
        $model = new TblPaymentOptions();;
           
        if ($model->load(Yii::$app->request->post()) && $model->validate()) 
         {
           $model->created_at=strtotime(gmdate('y-m-d H:i:s'));
           if (Yii::$app->user->identity->role != 20) {
              $model->company_id = $_POST['TblPaymentOptions']['company_id'];
            }
            else{
              $model->delete = 0;
            }
           if($_POST['TblPaymentOptions']['is_default']==1)
            {
                $model2 =  TblPaymentOptions::find()->all();
                // echo "<pre>";
                // print_r($model2);
                // exit();
                foreach($model2 as $key=>$value)
                {
                    $value->is_default = 0;
                    $value->save(false);
                }
                $model->is_default = 1;
                if($model->save())
                {
                    $data = 1;
                    //Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                    return $data;
                }
                else
                {
                    $data = 0;
                    //Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                    return $data;
                }
            }
            else
            {
              if($model->save())
                {
                    $data = 1;
                    //Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                    return $data;
                }
                else
                {
                    $data = 0;
                    //Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                    return $data;
                }
            }

         }
        else
        {
            return $this->renderAjax('/site/payment/createPaymentOption', [
                'model' => $model,
            ]);
        }

    }

    public function actionUpdatepaymentterm($id)
    {
        $model = TblPaymentOptions::find()->where(['id' => $id])->one();   
        $default = $model->is_default;
        if ($model->load(Yii::$app->request->post()) && $model->validate()) 
         {
            if($default==$_POST['TblPaymentOptions']['is_default'])
            {
               $model->updated_at=strtotime(gmdate('y-m-d H:i:s'));
               $model->save();
               $data = 1;
               return $data;
            }
            else
            {
              // echo "<pre>";
              // print_r($_POST);
              // echo "<pre>";
                $model->updated_at=strtotime(gmdate('y-m-d H:i:s'));
                if($_POST['TblPaymentOptions']['is_default']==1)
                {
                    $model2 =  TblPaymentOptions::find()->all();
                    // echo "<pre>";
                    // print_r($model2);
                    // exit();
                    foreach($model2 as $key=>$value)
                    {
                      //echo "<pre>";
                      // print_r($value->is_default);
                      // echo "<br>";
                       $value->is_default = 0;
                       $value->save(false);
                    }
                    $model->is_default = 1;
                    if($model->save())
                    {
                        $data = 1;
                        //Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                        return $data;
                    }
                    else
                    {
                        $data = 0;
                        //Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                        return $data;
                    }
                }
                else
                {
                  if($model->save())
                  {
                    $data = 1;
                    //Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                    return $data;
                  }
                  else
                  {
                      $data = 0;
                      //Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                      return $data;
                  }
                }
            }           
         }
        else
        {
            return $this->renderAjax('/site/payment/updatePaymentOption', [
                'model' => $model,
            ]);
        }

    }

   









}