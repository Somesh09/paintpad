<?php

namespace backend\controllers;

use Yii;
use app\models\Tbldate_range;
use app\models\Tblfrom;
use app\models\Tbl_orientation;
use app\models\size;
use app\models\Tblgroups;
use app\models\TblProjects;
use app\models\Child;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\TblBrands;
use app\models\TblSheen;
use yii\helpers\Url;
use yii\jui\DatePicker;
use wbraganca\dynamicform\DynamicFormWidget;


class ReportController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    
     /**
     * 
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
     /**
     * 
     * @return mixed
     */
    // public function actionProjects()
    // {
    //     // $date_range = Tbldate_range::find('id,date')->all();
    //     // $date_from = Tblfrom::find('id,from_date')->all();
    //     // $page_size = size::find('id,page_size')->all();
    //     // $orie = Tbl_orientation::find('id,orientation')->all();
    //     // $groups = Tbl_groups::find('id,groups')->all();
    //     // return $this->render('projects',['data'=>$date_range , 'dad'=>$date_from , 'siz'=>$page_size , 'orii'=>$orie ,'grps'=>$groups]);


       
    // }

    /**  
     * function actionProjects() to insert data 
     * by Somesh Vashist
     */   

    public function actionProjects()   
    {   
       //$groups = Tblgroups::find('id,groups')->all();
        if(!empty(Yii::$app->request->post())){
            // print_r(Yii::$app->request->post('report_name'));
            // die();
           
            $report_name  = !empty(Yii::$app->request->post('report_name'))?Yii::$app->request->post('report_name'):''; 
            $date_from  = !empty(Yii::$app->request->post('date_from'))?Yii::$app->request->post('date_from'):''; 
            $date_to  = !empty(Yii::$app->request->post('date_to'))?Yii::$app->request->post('date_to'):''; 
            $page_size  = !empty(Yii::$app->request->post('page_size'))?Yii::$app->request->post('page_size'):''; 
            $page_orien  = !empty(Yii::$app->request->post('page_orien'))?Yii::$app->request->post('page_orien'):'';
            $page_break  = !empty(Yii::$app->request->post('page_break'))?Yii::$app->request->post('page_break'):''; 
            $hide_group  = !empty(Yii::$app->request->post('hide_group'))?Yii::$app->request->post('hide_group'):'';

           // print_r(Yii::$app->request->post('page_size'));
           
        $TblProjects = new TblProjects(); 
         
//    $date_to = date('y-m-d h:i:s', time());
//    $md = date('d', time());
//    if($_POST['date_from'] == '' and $_POST['date_to'] == '')
//    {
//        if($name == "plus_1")
//        {
//            $nd = $md + 1;
//            $date_from = date('y-m-$nd h:i:s', time());
//        }
//    }

        $TblProjects->report_name          = $report_name;
        $TblProjects->date_from            = $date_from;
        $TblProjects->date_to              = $date_to;
        $TblProjects->page_size            = $page_size;
        $TblProjects->page_orien           = $page_orien;
        $TblProjects->page_break           = $page_break;
        $TblProjects->hide_group           = $hide_group;
        $TblProjects->date_created = date('y-m-d h:i:s', time());// current date
        
//         $datetime = new DateTime('2013-01-22');
// $datetime->modify('+1 day');
// echo $datetime->format('Y-m-d H:i:s');

//        if ($TblProjects->validate()) {
//            $TblProjects->save();
//        }
//        else{
////print_r($TblProjects->errors);
////die();
//    
//        }
                
        if($TblProjects->save()){  //save data to db

            $Id = $TblProjects->id;

                $arr['id']                  = $Id;
                $arr['report_name']         = $report_name;
                $arr['date_from']           = $date_from;
                $arr['date_to']             = $date_to;
                $arr['page_size']           = $page_size;
                $arr['page_orien']          = $page_orien;
                $arr['page_break']          = $page_break;
                $arr['hide_group']          = $hide_group;
        }
      
        

    // else {
    //     echo "Please enter valid data";
    //     $arr = $TblProjects->errors;
    // }
        return $this->render('projects', [
          
            'value' => $arr // 'grps'=>$groups
            
        ]);


        }
        else{
            return $this->render('projects');

        }
  
       
    }  
    


    
    // public function action_Projects()
    // {
    //    // $date_range = ReportModel::find('id,date')->all();
    //     $date_from = ReportModel::find('id,from_date')->all();
    //     return $this->render('projects',['data'=>$date_from]);
       
    // }
   

   

}

?>