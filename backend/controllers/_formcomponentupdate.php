<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\TblComponentGroups */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-component-groups-form">

    <?php $form = ActiveForm::begin(['validationUrl' => Url::toRoute(['validatecomponentupdate?id='.$id]),'enableAjaxValidation' => true, 'id' => 'myid6']); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

   
    <?= $form->field($model, 'type_id')->radioList(array('1'=>'interior','2'=>'exterior','3'=>'both')); ?>

    <?= $form->field($model, 'enabled')->radioList(array('1'=>'YES','0'=>'NO')); ?>


  

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success','id'=>'closecomponent']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
<script>
$('body').on('beforeSubmit', '#myid6', function () {
   
     var form = $(this);
     $.ajax({
          url: form.attr('action'),
          type: 'post',
          data: form.serialize(),
          success: function (response) {

               console.log(response);              

               if(response == 1){
                  //$('#myid')[0].reset();
                 //console.log('here11');
                  $("#modalcomponent").modal('hide');

               }
          },
          error: function () {
            alert("Something went wrong");
        }
     });
     return false;
});


</script>
