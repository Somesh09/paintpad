<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_components".
 *
 * @property int $comp_id
 * @property string $name
 * @property int $group_id
 * @property int $price_method_id
 * @property int $calc_method_id
 * @property int $created_at
 * @property int $updated_at
 *
 * @property TblComponentType[] $tblComponentTypes
 * @property TblComponentGroups $group
 * @property TblComponentPricingMethods $priceMethod
 * @property TblComponentCalculateMethod $calcMethod
 */
class TblComponents extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_components';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'price_method_id','tt_text'], 'required'],
            [['name'],'unique'],
            [['price_method_id','created_at', 'updated_at'], 'integer'],
            [['name','tt_url'], 'string', 'max' => 50],
            [['group_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblComponentGroups::className(), 'targetAttribute' => ['group_id' => 'group_id']],
            [['price_method_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblComponentPricingMethods::className(), 'targetAttribute' => ['price_method_id' => 'price_method_id']],
            //[['calc_method_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblComponentCalculateMethod::className(), 'targetAttribute' => ['calc_method_id' => 'calc_method_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'comp_id' => 'Comp ID',
            /*'name' => 'Name',
            'group_id' => 'Group ID',
            'price_method_id' => 'Price Method ID',
            'calc_method_id' => 'Calc Method ID',*/
            // 'enabled' => 'Disabled',
            'name' => 'Component',
            'group_id' => 'Grouping',
            'price_method_id' => 'Pricing Method',
            'calc_method_id' => 'Calculate Method (Interior Only)',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'tt_image' =>'Image',
            'tt_text' => 'Text',
            'tt_url' => 'Url',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblComponentTypes()
    {
        return $this->hasMany(TblComponentType::className(), ['comp_id' => 'comp_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(TblComponentGroups::className(), ['group_id' => 'group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPriceMethod()
    {
        return $this->hasOne(TblComponentPricingMethods::className(), ['price_method_id' => 'price_method_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCalcMethod()
    {
        return $this->hasOne(TblComponentCalculateMethod::className(), ['calc_method_id' => 'calc_method_id']);
    }
}
