<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TblPaymentOptions;

/**
 * TblColorsSearch represents the model behind the search form of `app\models\TblColors`.
 */
class TblPaymentOptionsSearch extends TblPaymentOptions
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['enabled', 'created_at', 'updated_at','is_default','no_of_days','discount','is_default','discount_days'], 'integer'],
            [['name','description'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        // $query = TblPaymentOptions::find();
        if (Yii::$app->user->identity->role != 20) {
              $query = TblPaymentOptions::find()             
                ->andWhere(['or',
                    ['company_id'=>0],
                    ['company_id'=>Yii::$app->user->identity->company_id]
                ]);
          }
          else{
            $query = TblPaymentOptions::find();
          }
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'name' => $this->name,
            'discount' => $this->discount,
            'no_of_days' => $this->no_of_days,
            'discount_days' => $this->discount_days,
            'description' => $this->description,
            'enabled' => $this->enabled,
            'is_default' => $this->is_default,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,


           
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}