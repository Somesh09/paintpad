<?php

namespace app\models;

use Yii;
use yii\helpers\Url;
/**
 * This is the model class for table "tbl_contacts".
 *
 * @property int $contact_id
 * @property int $subscriber_id
 * @property string $email
 * @property string $phone
 * @property string $image
 * @property int $address_id
 * @property int $created_at
 * @property int $updated_at
 *
 * @property TblAddress $address
 * @property TblUsers $subscriber
 */
class TblContacts extends \yii\db\ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_contacts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            //[['subscriber_id', 'name', 'email', 'phone', 'image', 'address_id', 'created_at', 'updated_at'], 'required'],
            [['name', 'email', 'phone'], 'required'],
            [['subscriber_id', 'address_id', 'created_at', 'updated_at'], 'integer'],
            [['email'], 'email'],
            [['image'], 'file'],
            ['email', 'unique', 'targetAttribute' => ['email'], 'message' => 'Email must be unique.'],            
            [['phone'], 'string', 'max' => 100],            
            [['address_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblAddress::className(), 'targetAttribute' => ['address_id' => 'address_id']],
            [['subscriber_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblUsers::className(), 'targetAttribute' => ['subscriber_id' => 'user_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'contact_id' => 'Contact ID',
            'subscriber_id' => 'Subscriber ID',
            'email' => 'Email',
            'phone' => 'Phone',
            'image' => 'Image',
            'address_id' => 'Address ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email]);
    }


    public static function listSubscriberContacts(){
        /* mandaory fields */
        //$sub_id  = !empty($_GET['subscriber_id'])?$_GET['subscriber_id']:'';

        $sub_id  = Yii::$app->user->id;

        $response = [];

        $arr = array();

        // validate
        if(empty($sub_id)){
          $response = [
            'success' => '0',
            'message' => 'Fields cannot be blank!',
            'data' => [],
          ];
        }
        else{

            // search in the database, there is no email referred
            $subscriber = \common\models\User::findIdentity($sub_id);

            if(!empty($subscriber)){

                $siteURL = Url::base(true);

                $model = TblContacts::find()
                        ->joinWith('address')                      
                        ->where([ 'subscriber_id' => $sub_id])
                        ->orderBy('name','ASC')
                        ->all();

                //echo "<pre>"; print_r($model); exit;        

                $newarr = array();

                foreach($model as $result){

                    $newarr['contact_id'] = $result->contact_id;
                    $newarr['name']       = $result->name;
                    $newarr['phone']      = $result->phone;
                    $newarr['image']      = ($result->image != "")?$siteURL . '/uploads/' . $result->image:$siteURL ."/image/profileIcon.png";
                    $newarr['email']      = $result->email;

                        if(isset($result->address)){
                            // get data from Address relation model
                            $newarr['address']['street1']        = $result->address->street1;
                            $newarr['address']['street2']        = $result->address->street2;
                            $newarr['address']['suburb']         = $result->address->suburb;
                            //$newarr['address']['state']          = $result->address->state_id;
                            $newarr['address']['state']          = $result->address->client_state;
                            $newarr['address']['postal']         = $result->address->postal_code;
                            $newarr['address']['country']        = $result->address->country_id;
                            $newarr['address']['formatted_addr'] = $result->address->formatted_address;
                            $newarr['address']['lat']            = $result->address->lat;
                            $newarr['address']['lng']            = $result->address->lng;

                        }else{

                         $newarr['address'] = "";   

                        }

                     $arr['contact'][] = $newarr;
                }           

                $response = [
                    'success' => '1',
                    'message' => 'Contacts Data!',
                    'data' => $arr,
                ];

            }
            else{
              $response = [
                'success' => '0',
                'message' => 'Subscriber doesnot exist in database!',
                'data' => [],
              ];
            }   

        }

        return $response;
    }

    public static function listSubscriberContactsJson(){

        $sub_id  = Yii::$app->user->id;

        $response = [];

        $arr = array();

        

        // search in the database, there is no email referred
        $subscriber = \common\models\User::findIdentity($sub_id);

        

        $siteURL = Url::base(true);

        $model = TblContacts::find()
                //->joinWith('address')                      
                ->where([ 'subscriber_id' => $sub_id])
                ->orderBy('name','ASC')
                ->all();

        $newarr = array();

        foreach($model as $result){
            $newarr['id'] = $result->contact_id;
            $newarr['value']    = $result->name;
            $newarr['label']    = $result->name;
            $newarr['phone']    = $result->phone;
            $newarr['img']      = ($result->image != "")?$siteURL . '/uploads/' . $result->image:$siteURL ."/image/profileIcon.png";
            $arr['contact'][] = $newarr;
        }
        return $response;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddress()
    {
        return $this->hasOne(TblAddress::className(), ['address_id' => 'address_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscriber()
    {
        return $this->hasOne(TblUsers::className(), ['user_id' => 'subscriber_id']);
    }

    public function getTblQuotes()
    {
        return $this->hasMany(TblQuotes::className(), ['contact_id' => 'contact_id']);
    }

    public function getTblCommunications()
    {
        return $this->hasMany(TblCommunications::className(), ['contact_id' => 'contact_id']);
    }

    public function getTblAppointments()
    {
        return $this->hasMany(TblAppointments::className(), ['contact_id' => 'contact_id']);
    }
    
    public function getTblInvoices()
    {
        return $this->hasMany(TblInvoices::className(), ['contact_id' => 'contact_id']);
    }


}
