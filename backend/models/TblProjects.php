<?php

namespace app\models;

use Yii;


class TblProjects extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */


     public static function tableName()   
    {   
        return 'tbl_projects';   
    }

     /**  
     * @inheritdoc  
     */   
   

    public function rules()
{
    return [
        
        [['report_name', 'date_from', 'date_to','page_size','page_orien','page_break','hide_group'], 'required'],

    ];
}

     /**
     * {@inheritdoc}
     */
//    public function attributeLabels()
//    {
//        return [
//            'id' => 'ID',
//            'report_name' => 'report_name',
//            'date_from' => 'date_from',
//            'date_to' => 'date_to',
//        ];
//    }

}