<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TblPrepLevel;

/**
 * tblPrepLevelSearch represents the model behind the search form of `app\models\tblPrepLevel`.
 */
class tblPrepLevelSearch extends TblPrepLevel
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['prep_id', 'is_default', 'created_at', 'updated_at'], 'integer'],
            [['prep_level'], 'safe'],
            [['uplift_cost', 'uplift_time'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TblPrepLevel::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'prep_id' => $this->prep_id,
            'uplift_cost' => $this->uplift_cost,
            'uplift_time' => $this->uplift_time,
            'is_default' => $this->is_default,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'prep_level', $this->prep_level]);

        return $dataProvider;
    }
}
