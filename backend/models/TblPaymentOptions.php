<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_payment_options".
 *
 * @property int $id
 * @property string $name
 * @property int $enabled
 * @property int $created_at
 * @property int $updated_at
 */
class TblPaymentOptions extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_payment_options';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['no_of_days','name'], 'required'],
            [['enabled', 'created_at', 'updated_at','is_default','no_of_days','discount','is_default','discount_days'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['description'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'is_default' =>'Is Default',
            'name' => 'Name',
            'description' => 'Description',
            'no_of_days' => 'No Of Days',
            'discount_days' => 'Discount Days',
            'discount' => 'Discount %',
            'enabled' => 'Enabled',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
