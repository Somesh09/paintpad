<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TblemailTemp;

/**
 * TblContactsSearch represents the model behind the search form of `app\models\TblContacts`.
 */
class TblemailTempSarch extends TblemailTemp
{
    /**
     * {@inheritdoc}
     */
    // public function rules()
    // {
    //     return [
    //         [['company_id','status', 'phone'], 'integer'],
    //         [['name', 'last_name', 'address', 'suburb','state','postcode','country','postal_address','postal_suburb','postal_state','postal_postcode','postal_country','email','website','abn'], 'safe'],
    //     ];
    // }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    { 
      if (Yii::$app->user->identity->role != 20) {
          $query = TblemailTemp::find()->andWhere(['or',
             ['company_id'=>0],
             ['company_id'=>Yii::$app->user->identity->company_id]
         ]);
      }
      else{
        $query = TblemailTemp::find();
      }
        
        // ->andFilterWhere(['company_id' => Yii::$app->user->identity->company_id]);

        // add conditions that should always apply here

        $dataProviderEmail = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        // print_r($params['TblemailTempSarch']);
        if (!empty($params['TblemailTempSarch'])) {
            $pp = $params['TblemailTempSarch'];
            // print_r($pp);die;
            // if (!$this->validate()) {
            //     // uncomment the following line if you do not want to return any records when validation fails
            //     // $query->where('0=1');
            //     echo '23';die;
            //     return $dataProviderEmail;
            // }

            // grid filtering conditions
             // echo '1';die;
            $query->andFilterWhere([
                // 'company_id' => $this->company_id,
                // 'phone' => $this->phone,
                // // 'name' => $this->name,
                // 'last_name' => $this->last_name,
                // 'address' => $this->address,
                // 'suburb' => $this->suburb,
                // 'state' => $this->state,
                // 'postcode' => $this->postcode,
                // 'country' => $this->country,
                // 'postal_address' => $this->postal_address,
                // 'postal_suburb' => $this->postal_suburb,
                // 'postal_state' => $this->postal_state,
                // 'postal_postcode' => $this->postal_postcode,
                // 'postal_country' => $this->postal_country,
                // 'email' => $this->email,
                // 'website' => $this->website,
                // 'abn' => $this->abn,
               // 'status' => $pp['status'],

            ]);
            // print_r( $pp['name']);die;
            // $query->andFilterWhere(['company_id' => Yii::$app->user->identity->company_id]);
             $query->andFilterWhere(['like', 'temp_name',  $pp['temp_name']]);
              return $dataProviderEmail;
        }
        else
        {
             return $dataProviderEmail;
        }

       
    }
}
