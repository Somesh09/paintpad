<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_company_documents".
 *
 * @property int $doc_id
 * @property int $company_id
 * @property int $subscriber_id
 * @property string $file_name
 * @property string $file_path_name
 * @property string $description
 * @property int $always_link
 * @property int $created_at
 */
class TblCompanyDocuments extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_company_documents';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'subscriber_id', 'file_name', 'file_path_name', 'description', 'always_link', 'created_at'], 'required'],
            [['company_id', 'subscriber_id', 'always_link', 'created_at'], 'integer'],
            [['file_name', 'file_path_name', 'description'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'doc_id' => 'Doc ID',
            'company_id' => 'Company ID',
            'subscriber_id' => 'Subscriber ID',
            'file_name' => 'File Name',
            'file_path_name' => 'File Path Name',
            'description' => 'Description',
            'always_link' => 'Always Link',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscriber()
    {
        return $this->hasOne(TblUsers::className(), ['user_id' => 'subscriber_id']);
    }
}
