<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_colors".
 *
 * @property int $color_id
 * @property string $name
 * @property string $hex
 * @property int $tag_id
 * @property int $number
 * @property int $created_at
 * @property int $updated_at
 *
 * @property TblColorTags $tag
 */
class TblUsers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username','email','pwd'], 'required'],
            [['role', 'status','created_at', 'updated_at'], 'integer'],
            [['username'], 'string', 'max' => 128],
            [['user_id'], 'unique'],
            //[['tag_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblColorTags::className(), 'targetAttribute' => ['tag_id' => 'tag_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'username' => 'User Name',
            'pwd' => 'Password',
            'auth_key' => 'Auth key',
            'role' => 'Role',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
        public function getTblContacts()
    {
        return $this->hasMany(TblContacts::className(), ['subscriber_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblUsersDetails()
    {
        return $this->hasMany(TblUsersDetails::className(), ['user_id' => 'user_id']);
    }
    public function getTblQuotes()
    {
        return $this->hasMany(TblQuotes::className(), ['user_id' => 'subscriber_id']);
    }    
    public function getTblCommunications()
    {
        return $this->hasMany(TblCommunications::className(), ['subscriber_id' => 'user_id']);
    }
    public function getTblMembers()
    {
        return $this->hasMany(TblMembers::className(), ['subscriber_id' => 'user_id']);
    }
    public function getTblCompany()
    {
        return $this->hasOne(TblCompany::className(), ['company_id' => 'company_id']);
    }
    public function getTblCompanyDocuments()
    {
        return $this->hasMany(TblCompanyDocuments::className(), ['user_id' => 'subscriber_id']);
    }

}
