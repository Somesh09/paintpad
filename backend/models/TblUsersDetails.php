<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_colors".
 *
 * @property int $color_id
 * @property string $name
 * @property string $hex
 * @property int $tag_id
 * @property int $number
 * @property int $created_at
 * @property int $updated_at
 *
 * @property TblColorTags $tag
 */
class TblUsersDetails extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_users_details';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            // [['name','phone'], 'required'],
            [['phone'], 'required'], // by ankit
            [['address_id','user_id','created_at', 'updated_at','phone'], 'integer'],
            [['name'], 'string', 'max' => 128],
            [['details_id'], 'unique'],
            //[['tag_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblColorTags::className(), 'targetAttribute' => ['tag_id' => 'tag_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'details_id' => 'Detail Id',
            'name' => 'Name',
            'logo' => 'logo',
            'phone' => 'Phone',
            'user_id' => 'User Id',
            'address_id' =>'Address',
            'website_link' => 'Website Link',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTag()
    {
        return $this->hasOne(TblUsers::className(), ['user_id' => 'user_id']);

    }
}
