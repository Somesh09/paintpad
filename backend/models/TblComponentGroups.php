<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_component_groups".
 *
 * @property int $group_id
 * @property string $name
 * @property int $type_id
 * @property int $enabled
 * @property int $created_at
 * @property int $updated_at
 *
 * @property TblProductType $type
 * @property TblComponents[] $tblComponents
 */
class TblComponentGroups extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_component_groups';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'type_id', 'enabled','tt_text'], 'required'],
            [['name'],'unique'],
            [['type_id', 'enabled', 'created_at', 'updated_at'], 'integer'],
            [['name','tt_url'], 'string', 'max' => 50],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblProductType::className(), 'targetAttribute' => ['type_id' => 'type_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'group_id' => 'Group ID',
            //'name' => 'Component Group',
            'name' => 'Group',
            'type_id' => 'Type ',
            'enabled' => 'Enabled',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'tt_image' =>'Image',
            'tt_text' => 'Text',
            'tt_url' => 'Url',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(TblProductType::className(), ['type_id' => 'type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblComponents()
    {
        return $this->hasMany(TblComponents::className(), ['group_id' => 'group_id']);
    }
}
