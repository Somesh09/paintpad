<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_component_calculate_method".
 *
 * @property int $calc_method_id
 * @property string $name
 * @property int $created_at
 * @property int $updated_at
 */
class TblComponentCalculateMethod extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_component_calculate_method';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'created_at', 'updated_at'], 'required'],
            [['created_at', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'calc_method_id' => 'Calc Method ID',
            'name' => 'Name',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
