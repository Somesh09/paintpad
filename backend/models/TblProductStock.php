<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_product_stock".
 *
 * @property int $product_stock_id
 * @property int $product_id
 * @property int $sheen_id
 * @property double $litres
 * @property string $code
 * @property string $name
 * @property double $price
 * @property int $created_at
 * @property int $updated_at
 */
class TblProductStock extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_product_stock';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id', 'litres', 'code', 'name', 'price', 'created_at', 'updated_at'], 'required'],
            [['product_id', 'sheen_id', 'created_at', 'updated_at'], 'integer'],
            [['litres', 'price'], 'number'],
            [['code', 'name'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'product_stock_id' => 'Product Stock ID',
            'product_id' => 'Product ID',
            'sheen_id' => 'Sheen ID',
            'litres' => 'Litres',
            'code' => 'Code',
            'name' => 'Name',
            'price' => 'Price',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
