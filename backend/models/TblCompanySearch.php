<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TblCompany;

/**
 * TblContactsSearch represents the model behind the search form of `app\models\TblContacts`.
 */
class TblCompanySearch extends TblCompany
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id','status', 'phone'], 'integer'],
            [['name', 'last_name', 'address', 'suburb','state','postcode','country','postal_address','postal_suburb','postal_state','postal_postcode','postal_country','email','website','abn'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TblCompany::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'company_id' => $this->company_id,
            'phone' => $this->phone,
            // 'name' => $this->name,
            'last_name' => $this->last_name,
            'address' => $this->address,
            'suburb' => $this->suburb,
            'state' => $this->state,
            'postcode' => $this->postcode,
            'country' => $this->country,
            'postal_address' => $this->postal_address,
            'postal_suburb' => $this->postal_suburb,
            'postal_state' => $this->postal_state,
            'postal_postcode' => $this->postal_postcode,
            'postal_country' => $this->postal_country,
            // 'email' => $this->email,
            // 'website' => $this->website,
            'abn' => $this->abn,
            'status' => $this->status,

        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);
        $query->orFilterWhere(['like', 'email', $this->name]);
        $query->orFilterWhere(['like', 'website', $this->name]);

        return $dataProvider;
    }
}
