<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_company".
 *
 * @property int $company_id
 * @property string $name
 * @property string $last_name
 * @property string $address
 * @property string $suburb
 * @property string $state
 * @property string $postcode
 * @property string $country
 * @property string $postal_address
 * @property string $postal_suburb
 * @property string $postal_state
 * @property string $postal_postcode
 * @property string $postal_country
 * @property string $phone
 * @property string $email
 * @property string $website
 * @property string $abn
 */
class TblCompany extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_company';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['address', 'postal_address', 'website', 'abn'], 'string'],
            [['name', 'last_name', 'suburb', 'postal_suburb'], 'string', 'max' => 250],
            [['state', 'postcode', 'country', 'postal_state', 'postal_postcode', 'postal_country', 'phone'], 'string', 'max' => 50],
            [['email'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'company_id' => 'Company ID',
            'name' => 'Name',
            'last_name' => 'Last Name',
            'address' => 'Address',
            'suburb' => 'Suburb',
            'state' => 'State',
            'postcode' => 'Postcode',
            'country' => 'Country',
            'postal_address' => 'Postal Address',
            'postal_suburb' => 'Postal Suburb',
            'postal_state' => 'Postal State',
            'postal_postcode' => 'Postal Postcode',
            'postal_country' => 'Postal Country',
            'phone' => 'Phone',
            'email' => 'Email',
            'website' => 'Website',
            'abn' => 'Abn',
        ];
    }

    public function getTblUsers()
    {
        return $this->hasMany(TblUsers::className(), ['company_id' => 'company_id']);
    }

    public function getTblUsersSettings()
    {
        return $this->hasOne(TblUsersSettings::className(), ['company_id' => 'company_id']);
    }
}
