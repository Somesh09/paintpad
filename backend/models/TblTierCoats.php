<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_tier_coats".
 *
 * @property int $id
 * @property int $tier_id
 * @property int $comp_group_id
 * @property int $sheen_id
 * @property int $product_id
 * @property int $top_coats "0 -> Under Coat, 1 -> Top Coat"
 * @property int $enabled "0 -> No, 1 -> Yes"
 * @property int $created_at
 * @property int $updated_at
 *
 * @property TblTiers $tier
 * @property TblComponentGroups $compGroup
 * @property TblSheen $sheen
 * @property TblProducts $product
 */
class TblTierCoats extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_tier_coats';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tier_id', 'comp_group_id', 'product_id'], 'required'],
            [['tier_id', 'comp_group_id', 'sheen_id', 'product_id', 'top_coats', 'enabled', 'created_at', 'updated_at'], 'integer'],
            [['tier_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblTiers::className(), 'targetAttribute' => ['tier_id' => 'tier_id']],
            [['comp_group_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblComponentGroups::className(), 'targetAttribute' => ['comp_group_id' => 'group_id']],
            [['sheen_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblSheen::className(), 'targetAttribute' => ['sheen_id' => 'sheen_id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblProducts::className(), 'targetAttribute' => ['product_id' => 'product_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tier_id' => 'Tier ID',
            'comp_group_id' => 'Comp Group ID',
            'sheen_id' => 'Sheen ID',
            'product_id' => 'Product ID',
            'top_coats' => 'Top Coats',
            'enabled' => 'Enabled',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTier()
    {
        return $this->hasOne(TblTiers::className(), ['tier_id' => 'tier_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompGroup()
    {
        return $this->hasOne(TblComponentGroups::className(), ['group_id' => 'comp_group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSheen()
    {
        return $this->hasOne(TblSheen::className(), ['sheen_id' => 'sheen_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(TblProducts::className(), ['product_id' => 'product_id']);
    }
}
