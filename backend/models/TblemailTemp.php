<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_company".
 *
 * @property int $company_id
 * @property string $name
 * @property string $last_name
 * @property string $address
 * @property string $suburb
 * @property string $state
 * @property string $postcode
 * @property string $country
 * @property string $postal_address
 * @property string $postal_suburb
 * @property string $postal_state
 * @property string $postal_postcode
 * @property string $postal_country
 * @property string $phone
 * @property string $email
 * @property string $website
 * @property string $abn
 */
class TblemailTemp extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_newsletter_templates';
    }
    
    /**
     * {@inheritdoc}
     */
}
