<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TblComponents;

/**
 * TblComponentsSearch represents the model behind the search form of `app\models\TblComponents`.
 */
class TblComponentsSearch extends TblComponents
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['comp_id', 'group_id', 'price_method_id', 'calc_method_id', 'created_at', 'updated_at'], 'integer'],
            [['name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TblComponents::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'comp_id' => $this->comp_id,
            'group_id' => $this->group_id,
            'price_method_id' => $this->price_method_id,
            'calc_method_id' => $this->calc_method_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'tt_image' => $this->tt_image,
            'tt_url' => $this->tt_url,
            'tt_text' => $this->tt_text,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
