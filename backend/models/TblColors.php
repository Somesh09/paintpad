<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_colors".
 *
 * @property int $color_id
 * @property string $name
 * @property string $hex
 * @property int $tag_id
 * @property int $number
 * @property int $created_at
 * @property int $updated_at
 *
 * @property TblColorTags $tag
 */
class TblColors extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_colors';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hex','name','tag_id'], 'required'],
            [['color_id', 'tag_id', 'number', 'created_at', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 128],
            [['hex'], 'string', 'max' => 12],
            [['color_id'], 'unique'],
            [['tag_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblColorTags::className(), 'targetAttribute' => ['tag_id' => 'tag_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'color_id' => 'Color ID',
            'name' => 'Name',
            'hex' => 'Hex',
            'tag_id' => 'Tag ID',
            'number' => 'Number',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTag()
    {
        return $this->hasOne(TblColorTags::className(), ['tag_id' => 'tag_id']);
    }
}
