<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TbluserActivity;

/**
 * TblContactsSearch represents the model behind the search form of `app\models\TblContacts`.
 */
class TbluserActivitySeach extends TbluserActivity
{
    /**
     * {@inheritdoc}
     */
    // public function rules()
    // {
    //     return [
    //         [['company_id','status', 'phone'], 'integer'],
    //         [['name', 'last_name', 'address', 'suburb','state','postcode','country','postal_address','postal_suburb','postal_state','postal_postcode','postal_country','email','website','abn'], 'safe'],
    //     ];
    // }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        // $query = TbluserActivity::find();
         if (Yii::$app->user->identity->role != 20) {
              $query = TbluserActivity::find()
                ->select('TBL_USER_ACTIVITY.*')
                ->leftJoin('tbl_users', '`tbl_users`.`user_id` = `TBL_USER_ACTIVITY`.`user_id`')
              
                ->andWhere(['or',
                 // ['company_id'=>0],
                    ['tbl_users.company_id'=>Yii::$app->user->identity->company_id]
                ]);
          }
          else{
            $query = TbluserActivity::find();
          }

        // add conditions that should always apply here

        $dataProviderActivity = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        if (!empty($params['TbluserActivitySeach'])) {
            $pp = $params['TbluserActivitySeach'];
            if (!$this->validate()) {
                // uncomment the following line if you do not want to return any records when validation fails
                // $query->where('0=1');
                return $dataProviderActivity;
            }

            // grid filtering conditions
            $query->andFilterWhere([
                // 'company_id' => $this->company_id,
                // 'phone' => $this->phone,
                // // 'name' => $this->name,
                // 'last_name' => $this->last_name,
                // 'address' => $this->address,
                // 'suburb' => $this->suburb,
                // 'state' => $this->state,
                // 'postcode' => $this->postcode,
                // 'country' => $this->country,
                // 'postal_address' => $this->postal_address,
                // 'postal_suburb' => $this->postal_suburb,
                // 'postal_state' => $this->postal_state,
                // 'postal_postcode' => $this->postal_postcode,
                // 'postal_country' => $this->postal_country,
                // 'email' => $this->email,
                // 'website' => $this->website,
                // 'abn' => $this->abn,
                'TBL_USER_ACTIVITY.group' => $pp['group'],

            ]);
            // print_r( $pp['name']);die;
            $query->andFilterWhere(['like', 'TBL_USER_ACTIVITY.name', $pp['name']]);
            // $query->orFilterWhere(['like', 'email', $this->name]);
            // $query->orFilterWhere(['like', 'website', $this->name]);
        }
        else{
             if (!$this->validate()) {
                // uncomment the following line if you do not want to return any records when validation fails
                // $query->where('0=1');
                return $dataProviderActivity;
            }

            // grid filtering conditions
            $query->andFilterWhere([
                // 'company_id' => $this->company_id,
                // 'phone' => $this->phone,
                // // // 'name' => $this->name,
                // 'last_name' => $this->last_name,
                // 'address' => $this->address,
                // 'suburb' => $this->suburb,
                // 'state' => $this->state,
                // 'postcode' => $this->postcode,
                // 'country' => $this->country,
                // 'postal_address' => $this->postal_address,
                // 'postal_suburb' => $this->postal_suburb,
                // 'postal_state' => $this->postal_state,
                // 'postal_postcode' => $this->postal_postcode,
                // 'postal_country' => $this->postal_country,
                // 'email' => $this->email,
                // 'website' => $this->website,
                // 'abn' => $this->abn,
                'group' => $this->group,

            ]);
            // print_r( $pp['name']);die;
            $query->andFilterWhere(['like', 'name', $this->name]);
            // $query->orFilterWhere(['like', 'email', $this->name]);
            // $query->orFilterWhere(['like', 'website', $this->name]);
        }

        return $dataProviderActivity;
    }
}
