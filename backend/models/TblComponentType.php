<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_component_type".
 *
 * @property int $comp_type_id
 * @property int $comp_id
 * @property string $name
 * @property int $work_rate
 * @property int $spread_ratio
 * @property int $is_default
 * @property double $thickness
 * @property double $excl_area
 * @property int $created_at
 * @property int $updated_at
 *
 * @property TblComponents $comp
 */
class TblComponentType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_component_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[ 'name', 'work_rate'], 'required'],
            //[['name'],'unique'],
            [['comp_id', 'is_default', 'created_at', 'updated_at'], 'integer'],
            [['thickness', 'excl_area', 'spread_ratio', 'work_rate'], 'number'],
            [['name'], 'string', 'max' => 50],
            [['comp_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblComponents::className(), 'targetAttribute' => ['comp_id' => 'comp_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'comp_type_id' => 'Comp Type ID',
            'comp_id' => 'Comp ID',
            'name' => 'Name',
            'fixed_area' => 'Fixed Area',
            'work_rate' => 'Work Rate',
            'spread_ratio' => 'Spread Ratio',
            'is_default' => 'Is Default',
            'thickness' => 'Thickness',
            'excl_area' => 'Excl Area',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'fixed_area' =>'Fixed Area'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComp()
    {
        return $this->hasOne(TblComponents::className(), ['comp_id' => 'comp_id']);
    }
}
