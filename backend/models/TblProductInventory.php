<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_product_inventory".
 *
 * @property int $id
 * @property int $product_id
 * @property int $sheen_id
 * @property string $litre
 * @property string $code
 * @property string $name
 * @property int $price
 */
class TblProductInventory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_product_inventory';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            //[['sheen_id', 'litre', 'code', 'name', 'price'], 'required'],
            //[['product_id', 'sheen_id', 'price'], 'integer'],
            [['litre', 'code', 'name'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'sheen_id' => 'Sheen ID',
            'litre' => 'Litre',
            'code' => 'Code',
            'name' => 'Name',
            'price' => 'Price',
        ];
    }
}
