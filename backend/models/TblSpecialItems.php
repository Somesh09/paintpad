<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_special_items".
 *
 * @property int $item_id
 * @property string $name
 * @property double $price
 * @property int $created_at
 * @property int $updated_at
 */
class TblSpecialItems extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_special_items';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'price',], 'required'],
            // [['name'],'unique'],
            [['price'], 'number'],
            [['created_at', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'item_id' => 'Item ID',
            'name' => 'Name',
            //'price' => 'Price',
            'price' => 'Price (Exc. GST)',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
