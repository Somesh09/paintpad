<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_sheen".
 *
 * @property int $sheen_id
 * @property string $name
 * @property int $created_at
 * @property int $updated_at
 */
class TblSheen extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_sheen';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name',], 'required'],
            [['name'],'unique'],
            [['created_at', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'sheen_id' => 'Sheen ID',
            'name' => 'Name',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
