<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use yii\widgets\Pjax;
use yii\helpers\Url;

AppAsset::register($this);

 $this->registerJs('
      jQuery(document).on("pjax:success", "#main",  function(event){
          var activeMenu = localStorage.getItem("activeMenu");
          if(typeof activeMenu != "undefined" && activeMenu != undefined && activeMenu != "null" && activeMenu != "") {
            console.log("activeMenu on load-> PJAX-------", activeMenu);
            jQuery(\'a[href="\'+activeMenu+\'"]\').trigger("click");
          }
        }
      );
      jQuery(document).on("click", ".nav.nav-tabs li a", function() {
        var active_tab = jQuery(this).attr("href");
        console.log("active_tab--------> updation", active_tab);   
        localStorage.setItem("activeMenu", active_tab);
      });
      jQuery(document).ready(function() {
          var activeMenu = localStorage.getItem("activeMenu");
          if(typeof activeMenu != "undefined" && activeMenu != undefined && activeMenu != "null" && activeMenu != "") {
            console.log("activeMenu on load-> normal---", activeMenu);
            jQuery(\'a[href="\'+activeMenu+\'"]\').trigger("click");
          }
           jQuery(".order_radio").click(function(e) {
jQuery(".order_radio").prop("checked", false);
jQuery(this).prop("checked", true);
jQuery(".hidden_order").val(jQuery(this).val());

console.log(jQuery(this).val());
});
      })
  ');


?>

<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600&display=swap" rel="stylesheet">
    <link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/themes/base/jquery-ui.css" rel="stylesheet">
    <link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel = "stylesheet">
    <?= Html::csrfMetaTags() ?>
    
    <?php $this->head() ?>
<title>Admin Panel PaintPad</title>
<style>
.active{
  
   
  
    
   
  
    font-size: 13px;
    
    cursor: pointer;
}
</style>
</head>
<script src="<?php echo Url::base(); ?>/assets/jquery.js"></script>

<body>
<?php $this->beginBody() ?>
<div id="refresh">
<div class="wrap admin-wrap">

    <?php $gg =Yii::$app->user->getIsGuest(); 
    if ($gg !=1 && \Yii::$app->user->identity->role == 20) {
       NavBar::begin(
        [
            'brandLabel' => 'PaintPad DashBoard',
            'brandUrl' => ['subscriber/index'],
            'options' => [
                'class' => 'navbar navbar-expand-lg  top-navigation',
            ],
        ]   
     );
        $menuItems = [
                ['label' => 'Home', 'url' => ['subscriber/index']],
            [
                'label' => 'Admin',
                //'url' => ['product/index'],
                'options'=>['class'=>'dropdown'],
                'template' => '<a href="{url}" class="href_class">{label}</a>',
                'items' => [
                    ['label' => 'Subscribers', 'url' => ['subscriber/list']],
                    ['label' => 'Subscription Packages', 'url' => ['subscriber/addsubscriptions']],
                    ['label' => 'CRM', 'href' => ['javascript:void(0)']],
                    ['label' => 'Accounting', 'url' => ['/payment/index']],
                    ['label' => 'Appointment Type', 'url' => ['/subscriber/appointmenttype']],
                    ['label' => 'Export Sheets', 'url' => ['/subscriber/exportdata']],
                    ['label' => 'Documents', 'href' => ['javascript:void(0)']],
                    ['label' => 'Plugins', 'href' => ['javascript:void(0)']],
                ]
            ],
            
            ['label' => 'Product', 'url' => ['/site/index']],
            //['label' => 'Reports', 'url' => ['/report/index']],
            [
                'label' => 'Reports',
                //'url' => ['product/index'],
                'options'=>['class'=>'dropdown'],
                'template' => '<a href="{url}" class="href_class">{label}</a>',
                'items' => [
                    ['label' => 'Predefined Reports', 'url' => ['report/index']],
                    ['label' => 'Projects', 'url' => ['report/projects']],

                ]
                ],
            
            
        ];
        //'submenuTemplate' => "\n<ul class='dropdown-menu' role='menu'>\n{items}\n</ul>\n",
        //];
        if (Yii::$app->user->isGuest) {
            $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
        } else {
            $menuItems[] = '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>';
        }
        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => $menuItems,
        ]);
        NavBar::end();
    }
    else{
        NavBar::begin(
            [
                'brandLabel' => 'PaintPad DashBoard',
                'brandUrl' => ['subscriber/index'],
                'options' => [
                    'class' => 'navbar navbar-expand-lg  top-navigation',
                ],
            ]   
         );
            $menuItems = [
                    ['label' => 'Home', 'url' => ['subscriber/index']],
                [
                    'label' => 'Admin',
                    //'url' => ['product/index'],
                    'options'=>['class'=>'dropdown'],
                    'template' => '<a href="{url}" class="href_class">{label}</a>',
                    'items' => [
                        // ['label' => 'Subscribers', 'url' => ['subscriber/index']],
                        // ['label' => 'Subscription Packages', 'url' => ['subscriber/addsubscriptions']],
                        ['label' => 'CRM', 'href' => ['javascript:void(0)']],
                        ['label' => 'Accounting', 'url' => ['/payment/index']],
                        ['label' => 'Appointment Type', 'url' => ['/subscriber/appointmenttype']],
                        ['label' => 'Export Sheets', 'url' => ['/subscriber/exportdata']],
                        ['label' => 'Documents', 'href' => ['javascript:void(0)']],
                        ['label' => 'Plugins', 'href' => ['javascript:void(0)']],
                    ]
                ],
                
                // ['label' => 'Product', 'url' => ['/site/index']],
                
            ];
            //'submenuTemplate' => "\n<ul class='dropdown-menu' role='menu'>\n{items}\n</ul>\n",
            //];
            if (Yii::$app->user->isGuest) {
                $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
            } else {
                $menuItems[] = '<li>'
                    . Html::beginForm(['/site/logout'], 'post')
                    . Html::submitButton(
                        'Logout (' . Yii::$app->user->identity->username . ')',
                        ['class' => 'btn btn-link logout']
                    )
                    . Html::endForm()
                    . '</li>';
            }
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => $menuItems,
            ]);
            NavBar::end();
    }
    
    ?>

    <div class="container-fluid">
        <div class="main-table-div">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>

  
        
          <?= $content ?>
      </div>
          </div>
        </div>
        </div>
        


     
     
       
        
    


</div>
<?php $this->endBody() ?>
    

</body>
</html>
<?php $this->endPage() ?>

