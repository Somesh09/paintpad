<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use yii\widgets\Pjax;
use yii\helpers\Url;

AppAsset::register($this);

 $this->registerJs('
      jQuery(document).on("pjax:success", "#main",  function(event){
          var activeMenu = localStorage.getItem("activeMenu");
          if(typeof activeMenu != "undefined" && activeMenu != undefined && activeMenu != "null" && activeMenu != "") {
            console.log("activeMenu on load-> PJAX-------", activeMenu);
            jQuery(\'a[href="\'+activeMenu+\'"]\').trigger("click");
          }
        }
      );
      jQuery(document).on("click", ".nav.nav-tabs li a", function() {
        var active_tab = jQuery(this).attr("href");
        console.log("active_tab--------> updation", active_tab);   
        localStorage.setItem("activeMenu", active_tab);
      });
      jQuery(document).ready(function() {
          var activeMenu = localStorage.getItem("activeMenu");
          if(typeof activeMenu != "undefined" && activeMenu != undefined && activeMenu != "null" && activeMenu != "") {
            console.log("activeMenu on load-> normal---", activeMenu);
            jQuery(\'a[href="\'+activeMenu+\'"]\').trigger("click");
          }
      })
  ');


?>

<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    
    <?php $this->head() ?>
<title>Admin Panel PaintPad</title>
<style>
.active{
  
   
  
    
   
  
    font-size: 13px;
    
    cursor: pointer;
}
</style>
</head>
<script src="<?php echo Url::base(); ?>/assets/jquery.js"></script>

<body>
<?php $this->beginBody() ?>
<div id="refresh">
<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'PaintPad DashBoard',
        'brandUrl' => '#',
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $menuItems = [
        [
            'label' => 'Admin',
            //'url' => ['product/index'],
            'options'=>['class'=>'dropdown'],
            'template' => '<a href="{url}" class="href_class">{label}</a>',
            'items' => [
                ['label' => 'Subscriber', 'url' => ['subscriber/index']],
                ['label' => 'CRM', 'href' => ['javascript:void(0)']],
                ['label' => 'Accounting', 'url' => ['/payment/index']],
                ['label' => 'Documents', 'href' => ['javascript:void(0)']],
                ['label' => 'Plugins', 'href' => ['javascript:void(0)']],
            ]
        ],
        //['label' => 'Home', 'url' => ['/site/index']],
        ['label' => 'Product', 'url' => ['/site/index']],
        
    ];
    //'submenuTemplate' => "\n<ul class='dropdown-menu' role='menu'>\n{items}\n</ul>\n",
    //];
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
    } else {
        $menuItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Logout (' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>';
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        
         

  
        
          <?= $content ?>
          </div>
        </div>
        </div>
        


     
     
       
        
    


</div>
<?php $this->endBody() ?>

</body>
</html>
<?php $this->endPage() ?>

