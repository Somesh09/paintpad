<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\TblBrands;
use app\models\TblSheen;
use yii\helpers\Url;
use wbraganca\dynamicform\DynamicFormWidget;
$features = (new \yii\db\Query())
    ->select(['id', 'name'])
    ->from('TBL_FEATURES')
    ->where(['enabled' => 1])
    ->all();
$packages = (new \yii\db\Query())
    ->from('TBL_PACKAGES')->select(['TBL_PACKAGES.*', 'TBL_PACKAGE_FEATURES.feature_id'])->join('LEFT JOIN', 'TBL_PACKAGE_FEATURES', 'TBL_PACKAGE_FEATURES.pkg_id = TBL_PACKAGES.id')->orderBy(['TBL_PACKAGES.price' => SORT_ASC])
    ->all();
    // print_r($packages);die;
    $packages1=[];
    foreach ($packages as $ke => $valu) {
      $packages1[$valu['id']]['id'] = $valu['id'];
      $packages1[$valu['id']]['interval_time'] = $valu['interval_time'];
      $packages1[$valu['id']]['description'] = $valu['description'];
      $packages1[$valu['id']]['price'] = $valu['price'];
      $packages1[$valu['id']]['name'] = $valu['name'];
      $packages1[$valu['id']]['disabled'] = $valu['disabled'];
      $packages1[$valu['id']]['plan_id'] = $valu['plan_id'];
      $packages1[$valu['id']]['features'][$ke] = $valu['feature_id'];

      $packages1[$valu['id']]['features'] = array_values($packages1[$valu['id']]['features']);
    }
    $packages1 = array_values($packages1);
    // echo "<pre>";
    // print_r($packages1);die;
    // print_r($packages1);die;
?>
<div class="container">
<select id="sortBy">
  <option value="" disabled="" selected="true" >Sort By</option>
   <option value="0">Name (A-Z)</option>
  <option value="2">Price</option>
 
  <option value="5">Features</option>
</select>
<button type="button" class="btn btn-primary add-subscr" data-toggle="modal" data-target="#exampleModal">
  <img src="/app/admin/uploads/add.png" width="17" height="17"> Add
</button>
  <table class="table" id="table_idPackage">
  <thead>
    <tr>
      <th scope="col">Name</th>
      <th scope="col">Description</th>
      <th scope="col">price</th>
      <th scope="col">Interval</th>
      <th scope="col">Update</th>
      <th scope="col">Features</th>
      
    </tr>
  </thead>
  <tbody>
    <?php foreach($packages1 as $key => $value ){ 
      if($value['interval_time'] == 0){
        $int = "monthly";
        $intC = "1";
      }
      else if($value['interval_time'] == 1){
        $int = "Half yearly";
        $intC = "6";
      }
      else if($value['interval_time'] == 3){
        $int = "Weekly";
      }
      else if($value['interval_time'] == 2){
      $int = "yearly";
    }
    $sF = $value['features'];
    $arr = $features;
    for ($e=0; $e < sizeof($sF); $e++) { 
      for ($ka=0; $ka < sizeof($features) ; $ka++) { 
        if($sF[$e] == $features[$ka]['id']){
          $arr[$ka]['sel'] =  1;
        }
      }
    }
    $str = json_encode($arr);
    if($value['disabled'] == 0){?>
    <!-- <span class="openmodel" data-toggle="modal" data-target="#viewPaa"></span>  -->

    <tr class="openTr"  data-name="<?php  echo $value['name']; ?>" data-interval="<?php  echo $value['interval_time']; ?>" data-id="<?php  echo $value['id']; ?>" data-disabled="<?php  echo $value['disabled']; ?>" data-desc="<?php  echo $value['description']; ?>" data-price="<?php  echo $value['price']; ?>" data-features='<?php echo $str ;?>' data-plans='<?php  echo $value['plan_id']; ?>'>
      <td><?php  echo $value['name']; ?></td>
      <td><?php  echo $value['description']; ?></td>
      <td>$<?php  echo $value['price']; ?></td>
       <td><?php  echo $int; ?></td>
       <td><span class=""><img src="/app/admin/uploads/update.png" width="13" height="13"></span></td>
       <td><?php  echo sizeof($value['features']); ?></td>
       
    </tr>

<?php }
 else{?>
  <tr class="openTr disabled"  data-name="<?php  echo $value['name']; ?>" data-interval="<?php  echo $value['interval_time']; ?>" data-id="<?php  echo $value['id']; ?>" data-disabled="<?php  echo $value['disabled']; ?>" data-desc="<?php  echo $value['description']; ?>" data-price="<?php  echo $value['price']; ?>" data-features='<?php echo $str ;?>' data-plans='<?php  echo $value['plan_id']; ?>'>
      <td><?php  echo $value['name']; ?></td>
      <td><?php  echo $value['description']; ?></td>
      <td>$<?php  echo $value['price']; ?></td>
       <td><?php  echo $int; ?></td>
       <td><span class=""><img src="/app/admin/uploads/update.png" width="13" height="13"></span></td>
       <td><?php  echo sizeof($value['features']); ?></td>
    </tr>
<?php }}?>
  </tbody>
</table>
</div>
<!-- Button trigger modal -->


<!-- Modal -->
<div class="modal fade add-package" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <form id="form-addP">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Package</h5>
		<button type="submit" class="btn btn-primary save">Save</button>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    
        <div class="modal-body">
		 <div class="form-group">
		  <label>Name</label>
          <input id="name"type="text" name="name" class="form-control half" placeholder="Name">
		  </div>
		  
		   <div class="form-group">
		   <label>Description</label>
          <input id="description" type="text" name="description" class="form-control half" placeholder="Description">
		  </div>
		  
		  <div class="form-group">
		  <label>Price</label>
          <input id="price" type="text" name="price" class="form-control half" placeholder="Price">
		  </div>
		  
		  <div class="form-group">
		  <label>Interval</label>
          <select id="interval" name="interval" class="form-control half">
            <option value='3'>Weekly</option>
            <option value='0'>Monthly</option>
            <option value='1'>Half Yearly</option>
            <option value='2'>Yearly</option>
          </select>
		  </div>
     <div class="form-group status">
        <label>Status</label>
         <label   class="radio-container">Enabled <input type="radio" name="disabled" value="0" id="" checked><span class="checkmark"></span></label>
            <label  class="radio-container">Disabled <input type="radio" name="disabled" value="1" id=""><span class="checkmark"></span></label>
          
        </div>
		  
          <div class="features">
            <?php for ($i=0; $i <sizeof($features) ; $i++) { 
            ?>
            <div class="checkbox">
              <label class="sign-container"><?php echo $features[$i]['name']?><input type="checkbox" name="features[]" class="fea"value="<?php echo $features[$i]['id']?>"><span class="checkmark"></span></label>
            </div>
          <?php }?>
          </div>
        </div>
        
      </form>
    </div>
  </div>
  
</div>
<!-- Modal -->
<div class="Loaderoverlay" style="display: none">
  <div class="progress-bar-overlay loader" id="uniqueLoader">
  
  </div><!-- progress-bar-overlay -->
</div>

<div class="modal fade" id="viewPaa" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form id="upform">
        
      
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Update Package</h5>
         <button type="submit" class="btn btn-primary">Save</button>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>

      </div>
      <div class="modal-body">
        <div class="form-group">
      <label>Name</label>
          <input id="nameu"type="text" name="name" class="form-control half" placeholder="Name">
      </div>
      
       <div class="form-group">
       <label>Description</label>
          <input id="descriptionu" type="text" name="description" class="form-control half" placeholder="Description">
          <input id="updateee" type="hidden" name="update" >
          <input id="Prid" type="hidden" name="id" >
      </div>
      
      <div class="form-group">
      <label>Price</label>
          <input id="priceu" type="text" name="price" class="form-control half" placeholder="Price">
          <input id="priceold" type="hidden" name="priceold"  value="" class="form-control half" placeholder="Price">
          <input id="intervalold" type="hidden" name="intervalold"  value="" class="form-control half" placeholder="Price">
      </div>
      
      <div class="form-group">
      <label>Interval</label>
          <select id="intervalu" name="interval" class="form-control half">
            <option value='3'>Weekly</option>
            <option value='0'>Monthly</option>
            <option value='1'>Half Yearly</option>
            <option value='2'>Yearly</option>
          </select>
      </div>
       <div class="form-group status">
        <label>Status</label>
         <label class="radio-container">Enabled <input type="radio" name="disabled" value="0" id="enabled"><span class="checkmark"></span></label>
            <label class="radio-container">Disabled <input type="radio" name="disabled" value="1" id="disabled"><span class="checkmark"></span></label>
          
        </div>
          <div class="featuresu">
            
          </div>
      </div>
      <!-- <div class="modal-footer"> -->
       
      <!-- </div> -->
    </form>
    </div>
  </div>
</div>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.css">
  
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.js"></script>
<script type="text/javascript">
  $(document).on('submit','#form-addP',function(event){
     $('#updateee').val('0')
       event.preventDefault();
       $('.Loaderoverlay').show()
       $.ajax({
           url:'<?php echo Url::base(); ?>/addSubscriptionAjax.php',
           method:"POST",
           data:new FormData(this),
           dataType:'JSON',
           contentType: false,
           cache: false,
           processData: false,
           success:function(data)
           {
               console.log(data)
               $('.Loaderoverlay').hide()
               location.reload()
               if (data.success == '1') {
                // $('#myPayment').modal('hide')
               }
               
           },
           complete:function(data){
            $('.Loaderoverlay').hide()
            location.reload()
           }
       })
   });
  $('.openTr').on('click',function(){
    $('#nameu').val($(this).data('name'))
    $('#descriptionu').val($(this).data('desc'))
    $('#priceu').val($(this).data('price'))
    $('#priceold').val($(this).data('price'))
    // $('#priceu').prop('disabled',true)
    if ($(this).data('disabled') == 0) {
       $('#enabled').prop("checked", true);
       $('#disabled').prop("checked", false);

    }
    else{
      $('#enabled').prop("checked", false);
      $('#disabled').prop("checked", true);
    }
    $('#intervalu').val($(this).data('interval'))
    $('#intervalold').val($(this).data('interval'))
    // $('#intervalu').prop('disabled',true)
    $('#updateee').val('1')
    $('#Prid').val($(this).data('id'))

    // console.log($(this).data('features'))
    var parsed = $(this).data('features')
    var featu = ''
    for (var i = 0; i < parsed.length; i++) {
      // Things[i]
    // }
    // for (var i = parsed.length - 1; i >= 0; i--) {
      if (parsed[i]['sel'] == 1) {
        featu += '<div class="checkboxu"><label class="sign-container">'+parsed[i]['name']+'<input class="fea" type="checkbox" name="features[]"value='+parsed[i]['id']+' checked>   <span class="checkmark"></span></label> </div> '
      }
      else{
        featu += '<div class="checkboxu"><label class="sign-container">'+parsed[i]['name']+'<input class="fea" type="checkbox" name="features[]"value='+parsed[i]['id']+' > <span class="checkmark"></span></label> </div> '
      }
       
    }
    $('.featuresu').empty().append(featu)
    jQuery('#viewPaa').modal('show')
  })
  $(document).on('submit','#upform',function(event){
       event.preventDefault();
       $('.Loaderoverlay').show()
       $.ajax({
           url:'<?php echo Url::base(); ?>/addSubscriptionAjax.php',
           method:"POST",
           data:new FormData(this),
           dataType:'JSON',
           contentType: false,
           cache: false,
           processData: false,
           success:function(data)
           {
               console.log(data)
               location.reload()
               $('.Loaderoverlay').hide()
               if (data.success == '1') {
                // $('#myPayment').modal('hide')
               }
               
           },
           complete:function(data){
            $('.Loaderoverlay').hide()
            // location.reload()
           }
       })
   });
  $(document).on('click','[value=14]',function(){
    if ($(this).prop('checked') == true) {
      $(".fea").prop("checked", true);
    }else{
      $(".fea").prop("checked", false);
    }
    
  })
</script>
<script type="text/javascript">
  $(document).ready(function(){
    $.noConflict();
    var oTable = $('#table_idPackage').DataTable({
       paging: false,
       // "pageLength": 15
       "order": [],
       "bLengthChange": false,
       "bInfo": false,
       searching: false,
       "columnDefs": [
            {
                "targets": [ 5 ],
                "visible": false,
            },
        ]
       // ordering:false
    });

    jQuery("#sortBy").change(function () {
      // console.log('1')
      var s = $(this).val()
        oTable.order([s, 'asc']).draw();
    })
     $('.sorting').off('click');

  })
  // $('#disable').on('click',function(){
       
    // });
</script>