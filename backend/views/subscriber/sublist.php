<?php
use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\bootstrap\ButtonDropdown;
use dosamigos\grid\GridView;
use dosamigos\exportable\ExportableButton; 
use kartik\export\ExportMenu;

// global 
// global $cRole = "";
?>
<style type="text/css">
  /*#modalSubscriber .modal-body {
    max-height: calc(100vh - 120px);
    overflow-y: auto;
  }*/
  .subscriberBoxclass {
    width: 100% !important;
  }
</style>

<?php if (Yii::$app->user->identity->role == 20) { ?>
  <div class="tbl-brand-index subscriberBoxclass">
  <?php //print_r(Yii::$app->user->identity->role);die; ?>
  <?php Pjax::begin(['timeout' => 5000,'id'=>'my-subscriber-pjax']);?>   

    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php  echo Html::button('<img src="'.Url::base(true).'/uploads/add.png"width="17" height="17"/>', ['value'=>Url::to( Url::base().'/subscriber/create'), 'class' => 'btn btn-primary pull-right','id'=>'modalButton']);?>
  <?php
    Modal::begin([
      'header'=>'',
      'id'=>'modalSubscriber',
      //'class'=>'main-popup-all',
      'size'=>'modal-lg',
      'clientOptions' => ['backdrop' => 'static'],
    ]);
    echo "<div id='modalContentSubscriber' class='clearfix'>
    <div style='text-align:center'>
     <img src='".Url::base()."/uploads/avatar2.gif' height='100' width='100'>
    </div></div>";
    Modal::end();
  ?>

  <?php
    Modal::begin([
      'header'=>'',
      'id'=>'modalSubscriberCreate',
      //'class'=>'main-popup-all',
      'size'=>'modal-lg',
      'clientOptions' => ['backdrop' => 'static'],
    ]);
    echo "<div id='modalContentSubscriberCreate' class='clearfix'>
    <div style='text-align:center'>
     <img src='".Url::base()."/uploads/avatar2.gif' height='100' width='100'>
    </div></div>";
    Modal::end();
  ?>

      
  <div id="SubscriberSearch" class="main-table-index-page list-page-main">
  <?=  GridView::widget([
      'dataProvider' => $dataProvider,
      'rowOptions'   => function ($model, $key, $index, $grid) {
                        return  [
                        // "class"=>"modalButton44",
                          "class"=>($model->status != 1)?"modalButton44 disabledTr":"modalButton44",
                        'value'=>Url::base().'/subscriber/update?id='.$model->company_id
                        ];
      },
      'behaviors' => [
          [
          'class' => '\dosamigos\grid\behaviors\LoadingBehavior',
          'type' => 'spinner'
          ]
      ],
      'columns' => [                              
        
        ['attribute'=>'name',
         'label' => 'Name',
         'format' => 'raw',
         'value' =>
           function($model){
             if($model->status != 1){
                return '<strike>'.$model->name.'</strike>';
              }else{
                return $model->name;
              }
           },
         ],
         ['attribute'=>'address',
         'label' => 'Address',
         'format' => 'raw',
         'value' =>
           function($model){
             if($model->status != 1){
                return '<strike>'.$model->address.'</strike>';
              }else{
                return $model->address;
              }
           },
         ],
         ['attribute'=>'phone',
         'label' => 'Phone',
         'format' => 'raw',
         'value' =>
           function($model){
             if($model->status != 1){
                return '<strike>'.$model->phone.'</strike>';
              }else{
                return $model->phone;
              }
           },
         ],
         ['attribute'=>'email',
         'label' => 'Email',
         'format' => 'raw',
         'value' =>
           function($model){
             if($model->status != 1){
                return '<strike>'.$model->email.'</strike>';
              }else{
                return $model->email;
              }
           },
         ],
         ['attribute'=>'website',
         'label' => 'Website',
         'format' => 'raw',
         'value' =>
           function($model){
             if($model->status != 1){
                return '<strike>'.$model->website.'</strike>';
              }else{
                return $model->website;
              }
           },
         ],
         ['attribute'=>'trial',
         'label' => 'Subscription',
         'format' => 'raw',
         'value' =>
           function($model){
             if($model->trial != 1){
                return 'Paid';
              }else{
                return 'Trial';
              }
           },
         ],
        ['class' => 'yii\grid\ActionColumn',
          'header' => 'UPDATE',
          'headerOptions' => ['style' => 'color:#337ab7'],
          'template' => '{update}',
          'buttons' => [
              'update' => function ($url, $model,$id) {
                                  return Html::a('<span class=""><img src="'.Url::base().'/uploads/update.png" width="13" height="13"/></span>', $url, [
                                      "class"=>"modalButton4",'value'=>Url::base().'/subscriber/update?id='.$model->company_id
                                  ]);
                            },
          ],
          'urlCreator' => function ($action, $model, $key, $index) {
               Url::to(['update?id='.$model->company_id],['id'=>'modal'])   ;         
          }
        ],
      ],
  ]); 
  ?>
  </div>

  </div>

  <script type="text/javascript">
    
  $('#modalButton').click(function(){
    $('#modalContentSubscriberCreate')
    .load ($(this).attr('value'));
    console.log('vv')
    console.log($(this).attr('value'))
    $('#modalSubscriberCreate').modal('show')

  })


  $('.modalButton4').click(function(){
     $("#modalContentSubscriber").html(" <div style='text-align:center'><img src='<?php echo Url::base();?>/uploads/avatar2.gif' height='100' width='100'></div>");
    $('#modalContentSubscriber')
    .load ($(this).attr('value'));
    $('#modalSubscriber').modal('show')
  })


  $('.modalButton44').click(function(){
     $("#modalContentSubscriber").html(" <div style='text-align:center'><img src='<?php echo Url::base();?>/uploads/avatar2.gif' height='100' width='100'></div>");
    $('#modalContentSubscriber')
    .load ($(this).attr('value'));
    $('#modalSubscriber').modal('show')

  })

  $(".modal").on("hidden.bs.modal", function(){
      $("#modalContentSubscriber").html("");
      $('.modal-backdrop').remove();
      // $('#modalSubscriberCreate').html("")
  });


  $('#tblcompanysearch-status').bind('change', function() { 
    $('.modal-backdrop').remove();
      $('#tblcompanysearch-status').delay(200).submit();
  });

  $(document).ready(function(){
    $(".modal").modal({
      show:false,
      backdrop:'static'
    });
  });
  </script>
  <?php Pjax::end();?>
<?php } ?>

<script >
$(document).on('keyup','#tblcompanysearch-name',function(){
  $('#my-subscriber-pjax').find('form').submit();
});

</script>


