<?php
use yii\helpers\Url;
use app\models\TblUserRoles;
use app\models\TblCompanyDocuments;
?>
<!DOCTYPE html>
<html>
	<head>
		<style type="text/css">
		.modal-body {
			position: relative;
			padding: 15px;
			float: left;
			/*width: 93%;*/
		}		
		.modal-dialog{
			width: 80%;
			height:90%;
		}
		#blah21
		{
		    position: relative;
		    top: -53px;
		    left: -20px;
		}
    .deletequotedoc
      {cursor: pointer;}

/*    #popup {
      width: 320px;
      height: 300px;
      margin: 0 auto;
      box-shadow: 1px 1px 1px 1px black;
      display: none;
    }
    #popup iframe {
      width: 100%;
      height: 100%
    }*/ 
    .col-sm-6
    {
      overflow-y: scroll;
      height:363px;
    } 
/*    #mydocModal
    {
       max-height: calc(100vh - 210px);
       overflow-y: auto;
    }*/
		</style>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment-with-locales.min.js">
</script>
	</head>
	
	<body>
    <?php 
      $model = TblUserRoles::find()->all(); 
      $modelDoc = TblCompanyDocuments::find()->all();
    ?>
		<!-- <div class="showLoader text-center" style="display:none;"><p>loading....</p></div> -->
    <div class="showLoader text-center" style="display:none;" style='text-align:center;display:none;'><img src='<?php echo Url::base();?>/uploads/avatar2.gif' height='100' width='100'></div>
		<div style="display:none;" class="bodyMain col-sm-12 row">
		<div class="col-sm-6">
			<div>
				<label>Subscriber Details</label>
			</div>
			<input type="file" class="changeImage pull-right" style="display:none;" onchange="document.getElementById('blah').src = window.URL.createObjectURL(this.files[0])">
			<input type="text" style="display:none;" value="<?php echo $_GET['id']?>" class="idClicked">
			
			<div class="subscriberLogo pull-right"></div>
			<button class="upload pull-right btn btn-primary" style="margin-top: 117px;"></button>
			<button class="imageChange pull-right btn btn-primary" style="margin-top: 117px;"></button>
			<div class="subscriberName"></div>
			<div class="subscriberAddress"></div>
			<div class="subscriberSuburb"></div>
			<div class="subscriberState"></div>
			<div class="subscriberCountry"></div>
			<div class="subscriberPhone"></div>
			<div class="subscriberEmail"></div>
			<div class="subscriberWebsite"></div>
		</div>	
		<div class="col-sm-6">
			<button class="btn btn-primary addDoc">Add New Library Document</button>
			<span>
				<input type="text" placeholder="Search/Filter" class="form-control"></span>
			<table class="table">
				<tr>
					<th>File ID</th>
					<th>File Name</th>
					<th>Description</th>
					<th>Uploaded By</th>
					<th>Uploaded Date</th>
				</tr>
				<tbody class="documentListing">
				</tbody>
			</table>
		</div>
    <div class="col-sm-6">
      <span style="font-size: 22px;">Settings</span><span class="pull-right updateSettings btn btn-large btn-primary"  style="font-size: 20px;">Edit</span>
      <div>
        <table class="table">
            <tr>
              <td>Application Cost</td>
              <td class="application_cost empty"></td>
            </tr>
            <tr>
              <td>Profit Mark Up %</td>
              <td class="profit_markup empty"></td>
            </tr>
            <tr>
              <td>Gross Margin %</td>
              <td class="gross_margin_percent empty"></td>
            </tr>            
            <tr>
              <td>Default Deposit Percent</td>
              <td class="default_deposit_percent empty"></td>
            </tr>  
            <tr>
              <td>Terms & Conditions Doc</td>
              <td class="terms_condition empty"></td>
            </tr>
            <tr>
              <td>Xero Consumer Key</td>
              <td class="xero_consumer_key empty"></td>
            </tr> 
            <tr>
              <td>Xero Share Secret</td>
              <td class="xero_share_secret empty"></td>
            </tr>
            <tr>
              <td>Xero Invoice Line Account Code</td>
              <td class="xero_invoice_line_account_code empty"></td>
            </tr>
            <tr>
              <td>Account Name</td>
              <td class="account_name empty"></td>
            </tr> 
            <tr>
              <td>Account BSB</td>
              <td class="account_BSB empty"></td>
            </tr>
            <tr>
              <td>Account Number</td>
              <td class="account_number empty"></td>
            </tr>
            <tr>
              <td>Payment Phone Number</td>
              <td class="payment_phone_number empty"></td>
            </tr> 
            <tr>
              <td>Colour Consultant Item</td>
              <td class="colour_consultant_item empty"></td>
            </tr>   
        </table>
      </div>
    </div>
    <div class="col-sm-6">
      <span style="font-size: 22px;">Subscriber's People</span><span style="font-size:20px" class="pull-right addsubscriberPeople btn btn-large btn-primary">Add People</span>
      <table class="table">
        <tr>
          <th>
            Person
          </th>
          <th>
            Active
          </th>
          <th>
            Logged On
          </th>
          <th>
            Last Activity
          </th>
        </tr>
        <tbody class="subscriberPeople">
        </tbody>
      </table>
    </div>
    <div class="col-sm-6">
      <span style="font-size: 22px;">Quote Email Settings</span><span style="font-size: :20px" class="pull-right btn btn-large btn-primary editEmailSetting">Edit</span>
      <table class="table">
        <tr>
          <td>Server Email Address</td>
          <td class='serverEmailAddress'></td>
        </tr>
        <tr>
          <td>BCC to</td>
          <td class='serverBccAddress'></td>
        </tr> 
        <tr>
          <td>Quote Email Subject</td>
          <td class='quoteEmailSubject'></td>
        </tr>  
        <tr>
          <td>Include Documents</td>
          <td></td>
        </tr>                        
      </table>
       <table class="table quoteIncludeDocuments pull-right"></table>
    </div>
    <div class="col-sm-6">
      <span style="font-size: 22px;">JSS Email Settings</span><span style="font-size: :20px" class="pull-right btn btn-large btn-primary editEmailSettingJss">Edit</span>
      <table class="table">
        <tr>
          <td>Server Email Address</td>
          <td class='serverEmailAddressjss'></td>
        </tr>
        <tr>
          <td>BCC to</td>
          <td class='serverBccAddressjss'></td>
        </tr> 
        <tr>
          <td>Quote Email Subject</td>
          <td class='quoteEmailSubjectjss'></td>
        </tr>  
        <tr>
          <td>Include Documents</td>
          <td></td>
        </tr>                        
      </table>
     <table class="table quoteIncludeDocumentsjss pull-right"></table>
    </div>
    <div class="col-sm-6">
       <span style="font-size: 22px;">Prep</span><span style="font-size: :20px" class="pull-right btn btn-large btn-primary prepbutton">Create Prep</span>
      <table class="table">
        <tr>
          <th>Name</th>
          <th>% uplift to paint cost</th>
          <th>% uplift to paint time</th>
        </tr>
        <tbody class="prepBody"></tbody>
      </table>
    </div>    
	</div>

<!--- bootstrap Modal for adding doc starts here!-->
  <!-- Modal -->
<div id="myModalDocAdd"  class="modal modal-child" data-backdrop-limit="1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-modal-parent="#myModal">
    <div class="modal-dialog">
    
      <!-- Modal content-->
    	<form action="" method="post" enctype="multipart/form-data" id="formDoc">
    		<input type="hidden" name="subscriber_id" class="subscriber_id_create" value="<?php echo Yii::$app->user->id;?>"> 
    		<input type="hidden" name="company_id" value="<?php echo $_GET['id']?>" class="company_id_create">
    		<input type="hidden" name="doc_id" value="0" class="doc_id_create">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="closeModalDocAdd pull-right btn btn-primary"  >&times;</button>


           <input type="button" class="SaveDocAdd pull-right btn btn-primary" value="Save" name="saveDoc">
  
          <h4 class="modal-title">Document Detail</h4>
        </div>
        <div class="modal-bodyDocAdd">
          
          		<table class="table">
          			<tr>
          				<td>
          					<label>File Name</label>
          				</td>
          				<td>
          					<input type="text" name="file_name" class="form-control file_name_doc_create " required="required">
          				</td>
          			</tr>
          			<tr>	
      						<td>
          					<label>File Description</label>
          				</td>
          				<td>
          					<textarea rows="4" cols="50" name="desc" class="form-control file_desc_doc_create" style="margin: 0px 313px 0px 0px; width: 451px; height: 85px;"></textarea>
          				</td>
          			</tr>
          			<tr>	
      						<td>
          					<label>Always Link</label>
          				</td>
          				<td>
          					<label>Yes</label>
          					<input type="radio" name="always_link" value="1" >
          					<label>No</label>
          					<input type="radio" name="always_link"  value="0" checked>
          				</td>
          			</tr>
          			<tr>	
      						<td>
          					<label>Upload New file</label>
          				</td>
          				<td>
          					<input type="file" name="image_0" class="imageDoc_create" onchange="document.getElementById('viewimageDoc').src = window.URL.createObjectURL(this.files[0])" style="display:none; ">

          					<img style="border: 3px solid #ddd;" src="<?php  echo Url::base(true); ?>/uploads/no_image.png"  class="viewimageDoc" id="viewimageDoc" width="120px" height="120px" >
          					<span class="opnImg"><img id="blah21" src="<?php  echo Url::base(true); ?>/uploads/edit.png" alt="your image" width="15px" height="16px"/ onclick="$('.imageDoc_create').click();"></span>
          				</td>          				
          			</tr>
          		</table>
          	</form>
        </div>
        <div class="modal-footer">
          
        </div>
      </div>
      </form>
    </div>
</div>
<!--- bootstrap Modal for adding doc ends here!-->


<!--- bootstrap Modal for Updating doc starts here!-->
  <!-- Modal -->
<div id="myModalDocUpdate"  class="modal modal-child" data-backdrop-limit="1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-modal-parent="#myModal">
    <div class="modal-dialog">
    
      <!-- Modal content-->
    	<form action="" method="post" enctype="multipart/form-data" id="formDocUpdate">
    			<input type="hidden" name="subscriber_id" class="subscriber_id" value="<?php echo Yii::$app->user->id;?>">
				<input type="hidden" name="company_id" class="company_id" value="<?php echo $_GET['id']?>">
				<input type="hidden" name="doc_id" class="doc_id" >
        <input type="hidden" class="uploaded_byUpdate">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="closeModalDocAdd pull-right btn btn-primary"  >&times;</button>


           <input type="button" class="SaveDocUpdate pull-right btn btn-primary" value="Save" >
  
          <h4 class="modal-title">Document Detail</h4>
        </div>
        <div class="modal-bodyDocAdd">
          
          		<table class="table">
          			<tr>
          				<td>
          					<label>File Name</label>
          				</td>
          				<td>
          					<input required type="text" name="file_name" class="form-control file_doc_name">
          				</td>
          			</tr>
          			<tr>	
      						<td>
          					<label>File Description</label>
          				</td>
          				<td>
          					<textarea rows="4" cols="50" name="desc" class="form-control file_doc_desc" style="margin: 0px 313px 0px 0px; width: 451px; height: 85px;"></textarea>
          				</td>
          			</tr>
          			<tr>	
      						<td>
          					<label>Always Link</label>
          				</td>
          				<td>
          					<label>Yes</label>
          					<input type="radio" name="always_link" value="1" class="file_doc_alwayslinkon">
          					<label>No</label>
          					<input type="radio" name="always_link"  value="0" class="file_doc_alwayslinkoff">
          				</td>
          			</tr>
                <tr>
                  <td class="imageName">
                  </td>
                  <td class="previewImage" id="previewImage">
                    <span>Preview</span>
                  </td>
                </tr>
          			<tr>	
      						<td>
          					<label>Upload New file</label>
          				</td>
          				<td>
          					<input type="file" name="image_0" class="imageDocupdate" >

          					<!-- <img style="border: 3px solid #ddd;" src="<?php  //echo Url::base(true); ?>/uploads/no_image.png"  class="viewimageDoc imagedoc_list" id="viewimageDocupdate" width="120px" height="120px" >
          					<span class="opnImg"><img id="blah21" src="<?php  //echo Url::base(true); ?>/uploads/edit.png" alt="your image" width="15px" height="16px"/ onclick="$('.imageDocupdate').click();"></span> -->
          				</td>          				
          			</tr>
          		</table>
          	</form>
        </div>
        <div class="modal-footer">
          
        </div>
      </div>
      </form>
    </div>
</div>
<!--- bootstrap Modal for adding doc ends here!-->


<!--bootstrap modal for adding people starts here-->
     <!-- Modal -->
  <div class="modal modal-child" data-backdrop-limit="1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-modal-parent="#myModal" id="myModalPeopleAdd">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
         <!--  <button type="button"  class="pull-right">&times;</button>
          <button type="button" onclick="$('#myModalPeopleAdd').modal('hide')" class="pull-right">&times;</button> -->
           <button type="button" onclick="$('#myModalPeopleAdd').modal('hide')" class="pull-right btn btn-primary">&times;</button>
           <input type="button" class="SavePeopleSubscriber pull-right btn btn-primary" value="Save" >
          <h4 class="modal-title">Add New System User</h4>
        </div>
        <div class="modal-body">
          <table class="table">
            <tr>
              <td>
                <label>First Name</label>
              </td>
              <td>
                <input type="text" class="form-control first_name empty" name="first_name">
              </td>
            </tr>
            <tr>
              <td>
                <label>Last Name</label>
              </td>
              <td>
                <input type="text" class="form-control last_name empty" name="last_name">
              </td>
            </tr> 
            <tr>
              <td>
                <label>User Name</label>
              </td>
              <td>
                <input type="text" class="form-control user_name empty" name="user_name">
              </td>
            </tr>
            <tr>
              <td>
                <label>Email</label>
              </td>
              <td>
                <input type="text" class="form-control email empty" name="email">
              </td>
            </tr>
            <tr>
              <td>
                <label>Phone</label>
              </td>
              <td>
                <input type="text" class="form-control phone empty" name="phone">
              </td>
            </tr>
            <tr>
              <td>
                <label>Mobile</label>
              </td>
              <td>
                <input type="text" class="form-control mobile empty" name="mobile">
              </td>
            </tr>  
            <tr>
              <td>
                <label>User Role</label>
              </td>
              <!-- <td>
                <input type="text" class="form-control user_role" name="user_role">
              </td> -->
              <td>
                  <select  class="form-control user_role empty" name="user_role"> 
                  <option value="">Select User Role</option>
                  <?php
                      foreach($model as $models)
                      {
                        echo "<option value='".$models->role_id."''>".$models->role_name."</option>";
                      }
                  ?>
                </select>
              </td>  
            </tr> 
            <tr>
              <td>
                <label>New Password</label>
              </td>
              <td>
                <input type="password" class="form-control newPassword empty" name="newPassword">
              </td>
            </tr>
            <tr>
              <td>
                <label>Confirm Password</label>
              </td>
              <td>
                <input type="password" class="form-control confirm_password empty" name="confirm_password">
              </td>
            </tr>                                                                                               
          </table>
        </div>
      </div>
      
    </div>
  </div>

<!--bootstrap modal for adding people end here-->


<!--bootstrap modal for Updating people starts here-->
     <!-- Modal -->
  <div class="modal modal-child" data-backdrop-limit="1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-modal-parent="#myModal" id="myModalPeopleUpdate">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
         <!--  <button type="button"  class="pull-right">&times;</button>
          <button type="button" onclick="$('#myModalPeopleAdd').modal('hide')" class="pull-right">&times;</button> -->
           <button type="button" onclick="$('#myModalPeopleUpdate').modal('hide')" class="pull-right btn btn-primary">&times;</button>
           <input type="button" class="SavePeopleSubscriberUpdate pull-right btn btn-primary" value="Save" >
          <h4 class="modal-title">Update System User</h4>
          <input type="hidden" id="user_idUpdateSystem">
        </div>
        <div class="modal-body">
          <table class="table">
            <tr>
              <td>
                <label>First Name</label>
              </td>
              <td>
                <input type="text" class="form-control first_name fnameSystem" name="first_name" >
              </td>
            </tr>
            <tr>
              <td>
                <label>Last Name</label>
              </td>
              <td>
                <input type="text" class="form-control last_name lnameSystem" name="last_name" >
              </td>
            </tr> 
            <tr>
              <td>
                <label>User Name</label>
              </td>
              <td>
                <input type="text" class="form-control user_name usernameSystem" name="user_name">
              </td>
            </tr>
            <tr>
              <td>
                <label>Email</label>
              </td>
              <td>
                <input type="text" class="form-control email emailSystem" name="email">
              </td>
            </tr>
            <tr>
              <td>
                <label>Phone</label>
              </td>
              <td>
                <input type="text" class="form-control phone phoneSystem" name="phone">
              </td>
            </tr>
            <tr>
              <td>
                <label>Mobile</label>
              </td>
              <td>
                <input type="text" class="form-control mobile mobileSystem" name="mobile">
              </td>
            </tr>  
            <tr>
              <td>
                <label>User Role</label>
              </td>
              <!-- <td>
                <input type="text" class="form-control user_role" name="user_role">
              </td> -->
              <td>
                  <select  class="form-control user_role" name="user_role"> 
                  <option value="">Select User Role</option>
                  <?php
                      foreach($model as $models)
                      {
                        echo "<option value='".$models->role_id."''>".$models->role_name."</option>";
                      }
                  ?>
                </select>
              </td>  
            </tr> 
            <tr>
              <td>
                <label>New Password</label>
              </td>
              <td>
                <input type="password" class="form-control newPassword" name="newPassword" id="newPassword">
              </td>
            </tr>
            <tr>
              <td>
                <label>Confirm Password</label>
              </td>
              <td>
                <input type="password" class="form-control confirm_password" name="confirm_password" id="confirm_password">
              </td>
            </tr>                                                                                               
          </table>
        </div>
      </div>
      
    </div>
  </div>

<!--bootstrap modal for Updating people end here-->


<!--bootstrap modal for Editing Setting starts here-->
     <!-- Modal -->
  <div class="modal modal-child" data-backdrop-limit="1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-modal-parent="#myModal" id="myModalsettingsUpdate">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
         <!--  <button type="button"  class="pull-right">&times;</button>
          <button type="button" onclick="$('#myModalPeopleAdd').modal('hide')" class="pull-right">&times;</button> -->
           <button type="button" onclick="$('#myModalsettingsUpdate').modal('hide')" class="pull-right btn btn-primary">&times;</button>
           <input type="button" class="SaveSettingCompany pull-right btn btn-primary" value="Save" >
          <h4 class="modal-title">Settings</h4>
          <input type="hidden" id="user_idUpdateSystem">
        </div>
        <div class="modal-body">
          <table class="table">
            <tr>
              <td>
                <label>Application Cost</label>
              </td>
              <td>
                <input type="text" class="form-control application_cost_setting" name="application_cost">
              </td>
            </tr>
            <tr>
              <td>
                <label>Profit Mark Up %</label>
              </td>
              <td>
                <input type="text" class="form-control profit_markup_setting" name="profit_markup" >
              </td>
            </tr> 
            <tr>
              <td>
                <label>Gross Margin %</label>
              </td>
              <td>
                <input type="text" class="form-control gross_margin_percent_setting" name="gross_margin_percent">
              </td>
            </tr>
            <tr>
              <td>
                <label>Default Deposit Percent</label>
              </td>
              <td>
                <input type="text" class="form-control default_deposit_percent_setting" name="default_deposit_percent">
              </td>
            </tr>
            <tr>
              <td>
                <label>Terms & Conditions</label>
              </td>
              <td>
                <input type="text" class="form-control terms_condition_setting" name="terms_condition">
              </td>
            </tr>
            <tr>
              <td>
                <label>Xero Consumer Key</label>
              </td>
              <td>
                <input type="text" class="form-control xero_consumer_key_setting" name="xero_consumer_key">
              </td>
            </tr>  
            <tr>
              <td>
                <label>Xero Share Secret</label>
              </td>
              <td>
                <input type="text" class="form-control xero_share_secret_setting" name="xero_share_secret">
              </td>
            </tr>
            <tr>
              <td>
                <label>Xero Invoice Line Account Code</label>
              </td>
              <td>
                <input type="text" class="form-control xero_invoice_line_account_code_setting" name="xero_invoice_line_account_code">
              </td>
            </tr> 
            <tr>
              <td>
                <label>Account Name</label>
              </td>
              <td>
                <input type="text" class="form-control account_name_setting" name="account_name">
              </td>
            </tr>  
            <tr>
              <td>
                <label>Account BSB</label>
              </td>
              <td>
                <input type="text" class="form-control account_BSB_setting" name="account_BSB">
              </td>
            </tr> 
            <tr>
              <td>
                <label>Account Number</label>
              </td>
              <td>
                <input type="text" class="form-control account_number_setting" name="account_number">
              </td>
            </tr> 
            <tr>
              <td>
                <label>Payment Phone Number</label>
              </td>
              <td>
                <input type="text" class="form-control payment_phone_number_setting" name="payment_phone_number">
              </td>
            </tr>   
            <tr>
              <td>
                <label>Colour Consultant Item</label>
              </td>
              <td>
                <input type="text" class="form-control colour_consultant_item_setting" name="colour_consultant_item">
              </td>
            </tr>                                                                                                                                        
          </table>
        </div>
      </div>
      
    </div>
  </div>

<!--bootstrap modal for editing setting end here-->



<!--bootstrap modal for Editing Setting starts here-->
     <!-- Modal -->
  <div class="modal modal-child" data-backdrop-limit="1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-modal-parent="#myModal" id="editEmailSettingquote">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
         <!--  <button type="button"  class="pull-right">&times;</button>
          <button type="button" onclick="$('#myModalPeopleAdd').modal('hide')" class="pull-right">&times;</button> -->
           <button type="button" onclick="$('#editEmailSettingquote').modal('hide')" class="pull-right btn btn-primary">&times;</button>
           <input type="button" class="saveEmailSettingquote pull-right btn btn-primary" value="Save" >
          <h4 class="modal-title">Quote Email Settings</h4>
          <input type="hidden" id="user_idUpdateSystem">
           <input type="hidden" class="email_setting_id">
        </div>
        <div class="modal-body">
          <table class="table quoteEmailSettings">
            <tr>
              <td>
                <label>BCC to</label>
              </td>
              <td>
                <input type="text" class="form-control bcc_to_quote" name="bcc_to">
              </td>
            </tr>
            <tr>
              <td>
                <label>Email Subject</label>
              </td>
              <td>
                <input type="text" class="form-control email_subject_quote" name="email_subject" >
              </td>
            </tr> 
            <tr>
              <td>
                <label>Attached Documents</label>
              </td>
              <td>
               <select class="form-control emailQuoteDoc">
                 <?php 
                    foreach ($modelDoc as $key => $doc) {
                      echo "<option value='".$doc->doc_id."'>".$doc->file_path_name."</option>";
                    }
                 ?>
               </select>

              </td>

            </tr>                                                                                                                                       
          </table>
        </div>
      </div>
      
    </div>
  </div>

<!--bootstrap modal for editing setting end here-->


<!--bootstrap modal for Editing Setting jss starts here-->
     <!-- Modal -->
  <div class="modal modal-child" data-backdrop-limit="1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-modal-parent="#myModal" id="editEmailSettingjss">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
         <!--  <button type="button"  class="pull-right">&times;</button>
          <button type="button" onclick="$('#myModalPeopleAdd').modal('hide')" class="pull-right">&times;</button> -->
           <button type="button" onclick="$('#editEmailSettingjss').modal('hide')" class="pull-right btn btn-primary">&times;</button>
           <input type="button" class="saveEmailSettingjss pull-right btn btn-primary" value="Save" >
          <h4 class="modal-title">JSS Email Settings</h4>
          <input type="hidden" id="user_idUpdateSystem">
          <input type="hidden" class="email_setting_id">
        </div>
        <div class="modal-body">
          <table class="table quoteEmailSettings">
            <tr>
              <td>
                <label>BCC to</label>
              </td>
              <td>
                <input type="text" class="form-control bcc_to_jss" name="bcc_to">
              </td>
            </tr>
            <tr>
              <td>
                <label>Email Subject</label>
              </td>
              <td>
                <input type="text" class="form-control email_subject_jss" name="email_subject" >
              </td>
            </tr> 
            <tr>
              <td>
                <label>Attached Documents</label>
              </td>
              <td>
               <select class="form-control emailQuoteDoc">
                 <?php 
                    foreach ($modelDoc as $key => $doc) {
                      echo "<option value='".$doc->doc_id."'>".$doc->file_path_name."</option>";
                    }
                 ?>
               </select>

              </td>

            </tr>                                                                                                                                       
          </table>
        </div>
      </div>
      
    </div>
  </div>

<!--bootstrap modal for editing setting jss end here-->



<!--bootstrap modal for Adding prep starts here-->
     <!-- Modal -->
  <div class="modal modal-child" data-backdrop-limit="1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-modal-parent="#myModal" id="createPrep">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
         <!--  <button type="button"  class="pull-right">&times;</button>
          <button type="button" onclick="$('#myModalPeopleAdd').modal('hide')" class="pull-right">&times;</button> -->
           <button type="button" onclick="$('#createPrep').modal('hide')" class="pull-right btn btn-primary">&times;</button>
           <input type="button" class="savePrepCreate pull-right btn btn-primary" value="Save" >
          <h4 class="modal-title">Create Prep</h4>
          <div class="deletePREP"></div>
          <input type="hidden" id="user_idUpdateSystem">
        </div>
        <div class="modal-body">
          <table class="table quoteEmailSettings">
            <tr>
              <td>
                <label>Prep. Level</label>
              </td>
              <td>
                <input type="text" class="form-control prep_level" name="prep_level">
              </td>
            </tr>
            <tr>
              <td>
                <label>% uplift to paint cost</label>
              </td>
              <td>
                <input type="text" class="form-control uplift_cost" name="uplift_cost" >
              </td>
            </tr> 
            <tr>
              <td>
                <label>% uplift to paint time</label>
              </td>
              <td>
                <input type="text" class="form-control uplift_time" name="uplift_time" >
              </td>
            </tr>
            <tr>
              <td>
                <label>Is_Default</label>
              </td>
              <td>
                <input type="checkbox" class="is_default" name="is_default" >
              </td>
            </tr>                                                                                                                                      
          </table>
        </div>
      </div>
      
    </div>
  </div>

<!--bootstrap modal for editing setting jss end here-->



<!--bootstrap modal for Adding prep starts here-->
     <!-- Modal -->
  <div class="modal modal-child" data-backdrop-limit="1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-modal-parent="#myModal" id="UpdatePrep">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
         <!--  <button type="button"  class="pull-right">&times;</button>
          <button type="button" onclick="$('#myModalPeopleAdd').modal('hide')" class="pull-right">&times;</button> -->
           <button type="button" onclick="$('#UpdatePrep').modal('hide')" class="pull-right btn btn-primary">&times;</button>
           <input type="button" class="savePrepUpdate pull-right btn btn-primary" value="Save" >
           <input type="hidden" class="idprep">
          <h4 class="modal-title">Create Prep</h4>
          <div class="deletePREP"></div>
          <input type="hidden" id="user_idUpdateSystem">
        </div>
        <div class="modal-body">
          <table class="table quoteEmailSettings">
            <tr>
              <td>
                <label>Prep. Level</label>
              </td>
              <td>
                <input type="text" class="form-control prep_levelupdate" name="prep_level">
              </td>
            </tr>
            <tr>
              <td>
                <label>% uplift to paint cost</label>
              </td>
              <td>
                <input type="text" class="form-control uplift_costupdate" name="uplift_cost" >
              </td>
            </tr> 
            <tr>
              <td>
                <label>% uplift to paint time</label>
              </td>
              <td>
                <input type="text" class="form-control uplift_timeupdate" name="uplift_time" >
              </td>
            </tr>
            <tr>
              <td>
                <label>Is_Default</label>
              </td>
              <td>
                <input type="checkbox" class="is_defaultupdate" name="is_default" >
              </td>
            </tr>                                                                                                                                      
          </table>
        </div>
      </div>
      
    </div>
  </div>

<!--bootstrap modal for editing setting jss end here-->


<!--bootstrap modal for previewing documents start here-->
<div class="modal modal-child" data-backdrop-limit="1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-modal-parent="#myModal" id="mydocModal">  
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
       <button type="button" onclick="$('#mydocModal').modal('hide')">&times;</button>
      <div id="pdf_name"></div>
      </div>
      <div class="modal-body">
        <div id="pdffile_load"></div>
      </div>
      <div class="modal-footer">
        <input type="hidden" name="hidden" id="hiddenId">
        <button type="button" id="myBtn" onclick="$('#mydocModal').modal('hide')">Close</button>
      </div>
    </div>
  </div>
</div>
<!--bootstrap modal for previewing documents end here-->

</body>
</html>
<script>
	$('.showLoader').show();
	$('.bodyMain').hide();
	var data ={"json":'{"company_id":"<?php echo $_GET['id'];?>"}'};
	//var da = '{"company_id":"<?php //echo $_GET['id'];?>"}';
	// console.log({"json":da})
	// console.log(data);
	var siteBaseUrl = "<?php echo Url::base(true);?>";
	//console.log(siteBaseUrl);

	$.ajax({
	url:  "http://enacteservices.com/paintpad/webservice/all-company-data",
	type: "POST",
	data: data, // Send the object.
	  success: function(response) {
	  	var parsed = JSON.parse(response);
      	console.log(parsed);
      	$('.upload').text('Upload');
      	$('.imageChange').text('Change Image')
      	$('.subscriberName').html("<label>"+parsed.data.compDetail.name+"</label>");
      	$('.subscriberAddress').html("<label>"+parsed.data.compDetail.address+"</label>");
      	$('.subscriberSuburb').html("<label>"+parsed.data.compDetail.suburb+"</label>");
      	$('.subscriberState').html("<label>"+parsed.data.compDetail.state+" "+parsed.data.compDetail.postcode+ "</label>");
      	$('.subscriberCountry').html("<label>"+parsed.data.compDetail.country+"</label>");
      	$('.subscriberPhone').html("<label>"+parsed.data.compDetail.phone+"</label>");
      	$('.subscriberEmail').html("<label>"+parsed.data.compDetail.email+"</label>");
      	$('.subscriberWebsite').html("<label><a>"+parsed.data.compDetail.website+"</a></label>");
      	console.log(parsed.data.compDetail.logo);
      	if(parsed.data.compDetail.logo=="")
      	{
      		parsed.data.compDetail.logo="<?php  echo Url::base(true); ?>/uploads/no_image.png";
      	}
        $('.subscriberLogo').html("<img src='"+parsed.data.compDetail.logo+"' width='100px' height='100px' id='blah'>");



        ///settings fill up starts here
        $('.account_name').text(parsed.data.companySetting.account_name);
        $('.application_cost').text(parsed.data.companySetting.application_cost);
        $('.profit_markup').text(parsed.data.companySetting.profit_markup);
        $('.gross_margin_percent').text(parsed.data.companySetting.gross_margin_percent); 
        $('.default_deposit_percent').text(parsed.data.companySetting.default_deposit_percent);
        $('.terms_condition').text(parsed.data.companySetting.terms_condition);
        $('.xero_consumer_key').text(parsed.data.companySetting.xero_consumer_key); 
        $('.xero_share_secret').text(parsed.data.companySetting.xero_share_secret);
        $('.xero_invoice_line_account_code').text(parsed.data.companySetting.xero_invoice_line_account_code); 
        $('.account_BSB').text(parsed.data.companySetting.account_BSB);
        $('.account_number').text(parsed.data.companySetting.account_number); 
        $('.payment_phone_number').text(parsed.data.companySetting.payment_phone_number);
        $('.colour_consultant_item').text(parsed.data.companySetting.colour_consultant_item);  


        $('.updateSettings').attr('account_name',parsed.data.companySetting.account_name) ;
        $('.updateSettings').attr('application_cost',parsed.data.companySetting.application_cost) ;
        $('.updateSettings').attr('profit_markup',parsed.data.companySetting.profit_markup) ;
        $('.updateSettings').attr('gross_margin_percent',parsed.data.companySetting.gross_margin_percent) ;
        $('.updateSettings').attr('default_deposit_percent',parsed.data.companySetting.default_deposit_percent) ;
        $('.updateSettings').attr('terms_condition',parsed.data.companySetting.terms_condition) ;
        $('.updateSettings').attr('xero_consumer_key',parsed.data.companySetting.xero_consumer_key) ;
        $('.updateSettings').attr('xero_share_secret',parsed.data.companySetting.xero_share_secret) ;
        $('.updateSettings').attr('xero_invoice_line_account_code',parsed.data.companySetting.xero_invoice_line_account_code) ;
        $('.updateSettings').attr('account_BSB',parsed.data.companySetting.account_BSB) ;
        $('.updateSettings').attr('account_number',parsed.data.companySetting.account_number) ;
        $('.updateSettings').attr('payment_phone_number',parsed.data.companySetting.payment_phone_number) ;
        $('.updateSettings').attr('colour_consultant_item',parsed.data.companySetting.colour_consultant_item) ;
        
        $('.empty').each(function(){
          if($(this).text()=='' || $(this).text()=="")
          {
            $(this).text('---')
          }
        })
        ///settings fill up ends here
        function isEmpty(obj) {
        for(var key in obj) {
           if(obj.hasOwnProperty(key))
            return false;
        }
        return true;
        }

        ///quote email setting fill up starts here
        $('.serverEmailAddress').text(parsed.data.quoteEmailSettingsDetail.server_email_address);
        $('.serverBccAddress').text(parsed.data.quoteEmailSettingsDetail.bcc_to);
        $('.quoteEmailSubject').text(parsed.data.quoteEmailSettingsDetail.email_subject);
        $('.editEmailSetting').attr('serverEmailAddress',parsed.data.quoteEmailSettingsDetail.server_email_address);
        $('.editEmailSetting').attr('serverBccAddress',parsed.data.quoteEmailSettingsDetail.bcc_to);
        $('.editEmailSetting').attr('quoteEmailSubject',parsed.data.quoteEmailSettingsDetail.email_subject);
        $('.editEmailSetting').attr('email_setting_id',parsed.data.quoteEmailSettingsDetail.email_setting_id); 
        //$('.editEmailSetting').attr('doc',parsed.data.quoteEmailSettingsDetail.email_subject);
        if(isEmpty(parsed.data.quoteEmailSettingsDetail))
        {
          
        }
        else
        {
          for(var i=0; i<parsed.data.quoteEmailSettingsDetail.doc.length; i++)
          {
            $('.quoteIncludeDocuments').append('<tr><td></td><td><input type="text" class ="attachmentquote" readonly value="'+parsed.data.quoteEmailSettingsDetail.doc[i].file_name+'" data-id ="'+parsed.data.quoteEmailSettingsDetail.doc[i].file_id+'"></td></tr>')
          }
        }
        //$('.quoteIncludeDocuments').text(parsed.data.quoteEmailSettingsDetail.quoteIncludeDocuments);
        ///quote email setting fill up ends here
        ///quote email setting fill up starts here
        $('.serverEmailAddressjss').text(parsed.data.jssEmailSettingsDetail.server_email_address);
        $('.serverBccAddressjss').text(parsed.data.jssEmailSettingsDetail.bcc_to);
        $('.quoteEmailSubjectjss').text(parsed.data.jssEmailSettingsDetail.email_subject);
        $('.editEmailSettingJss').attr('serverEmailAddress',parsed.data.jssEmailSettingsDetail.server_email_address);
        $('.editEmailSettingJss').attr('serverBccAddress',parsed.data.jssEmailSettingsDetail.bcc_to);
        $('.editEmailSettingJss').attr('quoteEmailSubject',parsed.data.jssEmailSettingsDetail.email_subject);
        $('.editEmailSettingJss').attr('email_setting_id',parsed.data.jssEmailSettingsDetail.email_setting_id);
        if(isEmpty(parsed.data.jssEmailSettingsDetail))
        {
          // for(var i=0; i<parsed.data.jssEmailSettingsDetail.doc.length; i++)
          // {
          //   $('.quoteIncludeDocumentsjss').append('<tr><td></td><td><input type="text" readonly value="'+parsed.data.jssEmailSettingsDetail.doc[i].file_name+'"></td></tr>')
          // } 
        }
        else
        {
          for(var i=0; i<parsed.data.jssEmailSettingsDetail.doc.length; i++)
          {
            $('.quoteIncludeDocumentsjss').append('<tr><td></td><td><input type="text" class="attachmentjss" readonly data-id ="'+parsed.data.jssEmailSettingsDetail.doc[i].file_id+'" value="'+parsed.data.jssEmailSettingsDetail.doc[i].file_name+'"></td></tr>')
          }
        }
        //$('.quoteIncludeDocuments').text(parsed.data.quoteEmailSettingsDetail.quoteIncludeDocuments);
        ///quote email setting fill up ends here

        ///prep listing
         var prep = parsed.data.prepLevel;
         for(var i=0; i<prep.length; i++)
         {
            $('.prepBody').append('<tr class="prepEdit" name="'+prep[i].name+'" id="'+prep[i].id+'" deletable="'+prep[i].deletable+'" is_default="'+prep[i].is_default+'" uplift_cost="'+prep[i].uplift_cost+'" uplift_time="'+prep[i].uplift_time+'"><td>'+prep[i].name+'</td><td>'+prep[i].uplift_cost+'</td><td>'+prep[i].uplift_time+'</td></tr>')
         }
        ///prep listing ends here

        var doclist = parsed.data.docList;
        console.log(doclist.length);
        for(var i=0; i<doclist.length; i++)
        {
							var d = new Date(0); // The 0 there is the key, which sets the date to the epoch
							d.setUTCSeconds(doclist[i].uploaded_date);
							//alert(d);
							var da = moment(d).format("DD/MM/YYYY");
							da = da.toUpperCase();
        	$('.documentListing').append("<tr id="+doclist[i].file_id+" class='docUpdateListing' name='"+doclist[i].file_name+"' desc='"+doclist[i].desc+"' actual_name='"+doclist[i].actual_name+"' always_link ='"+doclist[i].always_link+"' image_url ='"+doclist[i].url+"' uploaded_by ='"+doclist[i].uploaded_by+"'><td>"+doclist[i].file_id+"</td><td>"+doclist[i].file_name+"</td><td>"+doclist[i].desc+"</td><td>"+doclist[i].uploaded_by+"</td><td>"+da+"</td></tr>")
        }
        $('.bodyMain').show();  
  		  $('.showLoader').hide();

      }
	})
</script>
<script type="text/javascript">
	$(document).on('click', '.imageChange', function (e) {
		e.preventDefault();
		$('.changeImage').trigger('click');
		e.preventDefault();
	})
</script>
<script>
	$(document).on('click', '.upload', function (e) {
		e.preventDefault();
        var file_data = $('.changeImage').prop('files')[0];  
        var id_data = $('.idClicked').val(); 
       //  var form_data = new FormData();  
       // // console.log(form_data);

       //   form_data.append('image_0', file_data);
       //   form_data.append('company_id', id_data);
       //   console.log(form_data);

         var form_data = new FormData();
    //var data ={"json":'{"company_id":"<?php //echo $_GET['id'];?>"}'};
        var file_data = $('.changeImage').prop('files')[0]; ;  
        var myJSON = JSON.stringify({"company_id":id_data});

    //form_data.append('json', '{"file_name":'+file_name+',"file_doc_desc":'+file_doc_desc+',"doc_id":'+doc_id+',"always_link":'+always_link+',"company_id":'+company_id+',"subscriber_id":'+subscriber_id+'}');
    form_data.append('json',myJSON);
    form_data.append('image_0',file_data);


           
        $.ajax({
            url: "http://enacteservices.com/paintpad/webservice/update-company-logo",
            type: "POST",
            data: form_data,
            contentType: false,
            cache: false,
            processData:false,
            success: function(data){
                console.log(data);
            }
        });		
		
	})
</script>
<script>
$('.addDoc').click(function(){

	$('#myModalDocAdd').modal('show')
  $('#myModalDocAdd').find("input[type=text], textarea").val("");
  $('#myModalDocAdd').find("input[type=file]").val("");
  $('.viewimageDoc').attr('src','<?php  echo Url::base(true); ?>/uploads/no_image.png');
	if($('.modal-backdrop').length>1)
	{
		 $('.modal-backdrop:gt(0)').remove();
		//$('.modal-backdrop').remove();
	}

})
</script>
<script>
$('.closeModalDocAdd').click(function(){
	$('#myModalDocAdd').modal('hide');
	$('#myModalDocUpdate').modal('hide');
})
</script>
<script>
	$('.viewimageDoc').click(function(){
		$('.imageDoc').click();
    $('.imageDoc_create').click();
	})
	$('.viewimageDocupdate').click(function(){
		$('.imageDocupdate').click();
	})
</script>
<script type="text/javascript">
	$(document).on('click','.docUpdateListing',function(){
		var id = $(this).attr('id');
		var name = $(this).attr('name');
		var desc = $(this).attr('desc');
		var actual_name = $(this).attr('actual_name');
		var always_link = $(this).attr('always_link');
		var image_url = $(this).attr('image_url');
    	$('.imageName').text(actual_name);
    	$('.previewImage').attr('image',image_url);
    	$('.uploaded_byUpdate').val($(this).attr('uploaded_by'))
		//console.log("name--->"+name+"------desc---->"+desc+"-----actual_name----->"+actual_name+"-----always_link---->"+always_link+"---image_url---->"+image_url);

		$('.file_doc_name').val(name);
		$('.file_doc_desc').text(desc);
		$('.doc_id').val(id);
		$('.imageDocupdate').attr('id','imageDocupdate'+id);
		if(always_link==1)
		{
			$('.file_doc_alwayslinkon').attr('checked','checked');
		}
		else
		{
			$('.file_doc_alwayslinkoff').attr('checked','checked');
		}
		$('.imagedoc_list').attr('src','//'+image_url);
		$('#myModalDocUpdate').modal('show');
		if($('.modal-backdrop').length>1)
		{
		 $('.modal-backdrop:gt(0)').remove();
		}
	})
</script>
<script>
 $('#previewImage').click(function(){
    var image = $(this).attr('image');
    var Pdf = '<iframe id="frame" style="width:1100px;height:500px;" src="'+image+'" type="application/pdf"></iframe>';
    $('.modal-body #pdffile_load').html(Pdf);
    $('#mydocModal').modal('show');
    if($('.modal-backdrop').length>1)
    {
      $('.modal-backdrop:gt(0)').remove();
    }
 })
</script>
<script>


	$('.SaveDocUpdate').click(function(e){
		var file_name = $('.file_doc_name').val();
		var file_doc_desc = $('.file_doc_desc').val();
		var doc_id = $('.doc_id').val();
		var always_link = $("input[name=always_link]:checked").val()
		var company_id = $('.company_id').val();
		var subscriber_id = $('.subscriber_id').val();
    	var uploaded_by = $('.uploaded_byUpdate').val();
		var form_data = new FormData();
		//var data ={"json":'{"company_id":"<?php //echo $_GET['id'];?>"}'};
		var file_data = $('#imageDocupdate'+doc_id).prop('files')[0];  
		var myJSON = JSON.stringify({"file_name":file_name,"desc":file_doc_desc,"doc_id":doc_id,"always_link":always_link,"company_id":company_id,"subscriber_id":subscriber_id});

		//form_data.append('json', '{"file_name":'+file_name+',"file_doc_desc":'+file_doc_desc+',"doc_id":'+doc_id+',"always_link":'+always_link+',"company_id":'+company_id+',"subscriber_id":'+subscriber_id+'}');
		form_data.append('json',myJSON);
		form_data.append('image_0',file_data);
	    $.ajax({
            url: "http://enacteservices.com/paintpad/webservice/save-doc",
            //url:'save-doc',
            type: "POST",
            data: form_data,
            contentType: false,
            //cache: false,
            processData:false,
            success: function(data){
                 var parsed = JSON.parse(data);
                 console.log(parsed);
                 if(parsed.success == 1)
                 {
                  alert("data has been saved successfully");
                  // console.log(parsed.data.docList[0].file_name);
                  // $('#myModalDocAdd').modal('hide');
                    var d = new Date(0); // The 0 there is the key, which sets the date to the epoch
                    d.setUTCSeconds(parsed.data.docList[0].uploaded_date);
                    //alert(d);
                    var da = moment(d).format("DD/MM/YYYY");
                    da = da.toUpperCase();
                   $('#'+doc_id).attr('name',parsed.data.docList[0].file_name);
                   $('#'+doc_id).attr('desc',parsed.data.docList[0].desc);
                   $('#'+doc_id).attr('always_link',parsed.data.docList[0].always_link);
                   $('#'+doc_id).attr('image_url',parsed.data.docList[0].url);
                   $('#'+doc_id).html("<td>"+parsed.data.docList[0].file_id+"</td><td>"+parsed.data.docList[0].file_name+"</td><td>"+parsed.data.docList[0].desc+"</td><td>"+parsed.data.docList[0].uploaded_by+"</td><td>"+da+"</td>");
                    $('#myModalDocUpdate').modal('hide');


                 }
            }
        });	
	    e.preventDefault();
	})
</script>	
<script>
	$('.SaveDocAdd').click(function(e){
    var file_name = $('.file_name_doc_create').val();

    if(file_name=="")
    {
        alert("File Name Cannnot Be Blank");
        return false;
    }
    var file_name = $('.file_name_doc_create').val();
    var file_doc_desc = $('.file_desc_doc_create').val();
    var doc_id = $('.doc_id_create').val();
    var always_link = $("input[name=always_link]:checked").val()
    var company_id = $('.company_id_create').val();
    var subscriber_id = $('.subscriber_id_create').val();
    var form_data = new FormData();
    var file_data = $('.imageDoc_create').prop('files')[0];  
   /// var file_data = $('#imageDocupdate'+doc_id).prop('files')[0];  
    var myJSON = JSON.stringify({"file_name":file_name,"desc":file_doc_desc,"doc_id":doc_id,"always_link":always_link,"company_id":company_id,"subscriber_id":subscriber_id}); 
    form_data.append('json',myJSON);
    form_data.append('image_0',file_data);       
	    $.ajax({
            url: "http://enacteservices.com/paintpad/webservice/save-doc",
            //url:'save-doc',
            type: "POST",
            data: form_data,
            contentType: false,
            //cache: false,
            processData:false,
            success: function(data){
                var parsed = JSON.parse(data);
               // alert(parsed.success)
            if(parsed.success == 1)
            {
              alert('data successfully saved');
              var d = new Date(0); // The 0 there is the key, which sets the date to the epoch
              d.setUTCSeconds(parsed.data.docList[0].uploaded_date);
              //alert(d);
              var da = moment(d).format("DD/MM/YYYY");
              da = da.toUpperCase();
              $('.documentListing').append("<tr id="+parsed.data.docList[0].file_id+" class='docUpdateListing' name='"+parsed.data.docList[0].file_name+"' desc='"+parsed.data.docList[0].desc+"' actual_name='"+parsed.data.docList[0].actual_name+"' always_link ='"+parsed.data.docList[0].always_link+"' image_url ='"+parsed.data.docList[0].url+"' uploaded_by ='"+parsed.data.docList[0].uploaded_by+"'><td>"+parsed.data.docList[0].file_id+"</td><td>"+parsed.data.docList[0].file_name+"</td><td>"+parsed.data.docList[0].desc+"</td><td>"+parsed.data.docList[0].uploaded_by+"</td><td>"+da+"</td></tr>");
              $('#myModalDocAdd').modal('hide');
            }

            }
        });	
	    e.preventDefault();
	})
</script>	
<script>
  $('.updateSettings').click(function(){
    var account_name =   $(this).attr('account_name');
    var application_cost =   $(this).attr('application_cost');
    var profit_markup =   $(this).attr('profit_markup') ;
    var gross_margin_percent =   $(this).attr('gross_margin_percent');
    var default_deposit_percent =   $(this).attr('default_deposit_percent');
    var terms_condition =   $(this).attr('terms_condition');
    var xero_consumer_key =   $(this).attr('xero_consumer_key');
    var xero_share_secret =   $(this).attr('xero_share_secret');
    var xero_invoice_line_account_code =   $(this).attr('xero_invoice_line_account_code');
    var account_BSB =   $(this).attr('account_BSB');
    var account_number =   $(this).attr('account_number');
    var payment_phone_number =   $(this).attr('payment_phone_number');
    var colour_consultant_item =   $(this).attr('colour_consultant_item') ;
    //alert("rk");
  })
</script>
<script>
  $(document).on('click','.addsubscriberPeople',function(){
    //alert("rk");
    $('.empty').each(function(){
    	$(this).val("");
    })
    $('#myModalPeopleAdd').modal('show');
    //alert($('.modal-backdrop').length)
   if($('.modal-backdrop').length > 1)
    {
     $('.modal-backdrop:gt(0)').remove();
    }
    $('#myModalPeopleAdd').modal('show');
  })
</script>
<script>
$(document).on('click','.SavePeopleSubscriber',function(e){
	//e.preventDefault();
	$('.SavePeopleSubscriber').prop("disabled", true);
	var first_name = $('.first_name').val();
	var last_name =  $('.last_name').val();
	var user_name =  $('.user_name').val();
	var email =  $('.email').val();
	var phone =  $('.phone').val();
	var mobile =  $('.mobile').val();
	var user_role =  $('.user_role').val();
	var password = $('.newPassword').val();
	var confirm_password = $('.confirm_password').val()
	var company_id = "<?php echo $_GET['id'];?>";


	error = 0;
	if(user_name == ""){
		error = 1;
		alert('User Name is mandatory');
		$('.SavePeopleSubscriber').prop("disabled", false);
		return
	}
	if(email == ""){
		error = 1;
		alert('Email is mandatory');
		$('.SavePeopleSubscriber').prop("disabled", false);
		return
	}
	if(user_role == ""){
		error = 1;
		alert('User Role is mandatory');
		$('.SavePeopleSubscriber').prop("disabled", false);
		return
	}
	if(password == ""){
		error = 1;
		alert('Password is mandatory');
		$('.SavePeopleSubscriber').prop("disabled", false);
		return
	}
	if(confirm_password == ""){
		error = 1;
		alert('Confirm Password is mandatory');
		$('.SavePeopleSubscriber').prop("disabled", false);
		return
	}

	if(error == 1){
		$('.SavePeopleSubscriber').prop("disabled", false);
		return
	}


// {
// "company_id":"3",
// "first_name":"test2",
// "last_name":"name",
// "username":"testing",
// "email":"em@email.com",
// "user_role":"10",
// "password":"Lbim2201",
// "phone":"1234567890"
// }
	if($('.newPassword').val() == $('.confirm_password').val())
	{
		var form_data = new FormData();
		var myJSON = JSON.stringify({"first_name":first_name,"last_name":last_name,"username":user_name,"email":email,"company_id":company_id,"phone":phone,"user_role":user_role,"password":password}); 
		form_data.append('json',myJSON);
		$.ajax({
			url: "save-subscriber",
			//url:'save-doc',
			type: "POST",
			data: form_data,
			contentType: false,
			//cache: false,
			processData:false,
			dataType : 'JSON',
			success: function(parsed){
				console.log(parsed);
				if(parsed.success == 1){
					$('.SavePeopleSubscriber').prop("disabled", false);
					// alert('done');
					console.log(parsed);
					//var parsed = JSON.parse(data);
					console.log(parsed);
					//console.log(parsed.userdata);  
					// console.log(parsed.data.details[i])
					//   var d = new Date(0);  
					//   d.setUTCSeconds(parsed.userdata.logon);
					//   var da = moment(d).format("DD/MM/YYYY HH:mm A");
					//  da = da.toUpperCase();
					var da = "15/03/2019 18:11 PM";
					var la ="15/03/2019 18:11 PM";
					//  var da1 = new Date(0);
					// da1.setUTCSeconds(parsed.userdata.last_activity);
					//   var la = moment(da1).format("DD/MM/YYYY HH:mm A");
					//  la = la.toUpperCase();
					alert("data saved successfully");
					$('.subscriberPeople').append('<tr fname="'+first_name+'" id="'+parsed.data.userdata.user_id+'" lname="'+last_name+'" email="'+email+'" username="'+user_name+'" phone="'+phone+'" mobile="'+mobile+'"  class="SubscriberPeopleUpdate" user_id="'+parsed.data.userdata.user_id+'" id="id'+parsed.data.userdata.user_id+'"><td>'+first_name+" "+last_name+'</td><td>#</td><td class="loggedON'+da+'" data="'+da+'">'+da+'</td><td class="lastActivity'+la+'" data="'+la+'">'+la+'</td><tr>');
					$('#myModalPeopleAdd').modal('hide');
				}
				else{
					//alert(parsed.message);
					$('.SavePeopleSubscriber').prop("disabled", false);
				}
			},
			error: function(err){
				console.log(err)
			}
		})
	}
	else
	{
		alert('password does not matched');
		$('.SavePeopleSubscriber').prop("disabled", false);
	}

})
</script>
<script>
$(document).ready(function(){
  var data ={"json":'{"company_id":"<?php echo $_GET['id'];?>"}'};
  $.ajax({
  url:  "http://enacteservices.com/paintpad/webservice/company-people",
  type: "POST",
  data: data, // Send the object.
    success: function(response) {
      var parsed = JSON.parse(response);
      console.log(parsed)
      // console.log(parsed.data.details[0].logon);
      for(var i=0; i<parsed.data.details.length; i++)
      {
          console.log(parsed.data.details[i])
          var d = new Date(0);  
          d.setUTCSeconds(parsed.data.details[i].logon);
          var da = moment(d).format("DD/MM/YYYY HH:mm A");
          da = da.toUpperCase();
   
          var da1 = new Date(0);
          da1.setUTCSeconds(parsed.data.details[i].last_activity);
          var la = moment(da1).format("DD/MM/YYYY HH:mm A");
          la = la.toUpperCase();
          $('.subscriberPeople').append('<tr fname="'+parsed.data.details[i].first_name+'" lname="'+parsed.data.details[i].last_name+'" email="'+parsed.data.details[i].email+'" username="'+parsed.data.details[i].username+'" id="id'+parsed.data.details[i].user_id+'" phone="'+parsed.data.details[i].phone+'" mobile="'+parsed.data.details[i].mobile+'"  class="SubscriberPeopleUpdate" user_id="'+parsed.data.details[i].user_id+'" ><td>'+parsed.data.details[i].first_name+" "+parsed.data.details[i].last_name+'</td><td>#</td><td class="loggedON'+parsed.data.details[i].user_id+'" data="'+da+'">'+da+'</td><td class="lastActivity'+parsed.data.details[i].user_id+'" data="'+la+'">'+la+'</td><tr>');
      }


       // console.log(parsed.data.length);
      }
  })
})
</script>
<script>
  $(document).on('click','.SubscriberPeopleUpdate',function(){
    //alert($(this).attr('fname'));
    var fname = $(this).attr('fname');
    var lname = $(this).attr('lname');
    var email = $(this).attr('email');
    var username = $(this).attr('username');
    var phone = $(this).attr('phone');
    var mobile = $(this).attr('mobile');
    var user_id = $(this).attr('user_id');

    $('.fnameSystem').val(fname);
    $('.lnameSystem').val(lname);
    $('.emailSystem').val(email);
    $('.usernameSystem').val(username);
    $('.phoneSystem').val(phone);
    $('.mobileSystem').val(mobile);
    $('.user_role').val('10');
    $('#user_idUpdateSystem').val(user_id);

    $('#myModalPeopleUpdate').modal('show');
    if($('.modal-backdrop').length > 1)
    {
     $('.modal-backdrop:gt(0)').remove();
    }

  })
</script>
<script>
var user_role = $('.user_role').val();
if(user_role=="")
{

}
</script>
<script>
$(document).on('click','.SavePeopleSubscriberUpdate',function(){
		$('.SavePeopleSubscriberUpdate').prop("disabled", true);
		var first_name = $('.fnameSystem').val();
		var last_name =  $('.lnameSystem').val();
		var user_name =  $('.usernameSystem').val();
		var email =  $('.emailSystem').val();
		var phone =  $('.phoneSystem').val();
		var mobile =  $('.mobileSystem').val();
		var user_role =  $('.user_role').val();
		var password = $('#newPassword').val();
		var company_id = "<?php echo $_GET['id'];?>";
		var user_id = $('#user_idUpdateSystem').val();
		var loggedON = $('.loggedON'+user_id).attr('data');
		var lastActivity = $('.lastActivity'+user_id).attr('data');


		var confirm_password = $('.confirm_password').val()
		var company_id = "<?php echo $_GET['id'];?>";


		error = 0;
		if(email == ""){
			error = 1;
			alert('Email is mandatory');
			$('.SavePeopleSubscriberUpdate').prop("disabled", false);
			return
		}
		if(user_name == ""){
			error = 1;
			alert('User Name is mandatory');
			$('.SavePeopleSubscriberUpdate').prop("disabled", false);
			return
		}
		if(user_role == ""){
			error = 1;
			alert('User Role is mandatory');
			$('.SavePeopleSubscriberUpdate').prop("disabled", false);
			return
		}

		if(error == 1){
			$('.SavePeopleSubscriberUpdate').prop("disabled", false);
			return
		}


		if($('#newPassword').val() == $('#confirm_password').val())
		{
			var form_data = new FormData();
			var myJSON = JSON.stringify({'first_name':first_name,'last_name':last_name,'username':user_name,'email':email,'company_id':company_id,'phone':phone,'user_role':user_role,'password':password,'user_id':user_id,'mobile_number':mobile}); 
			console.log(myJSON);
			form_data.append('json',myJSON);
			$.ajax({
				url: "http://enacteservices.com/paintpad/webservice/update-company-people",
				//url:'save-doc',
				type: "POST",
				data: form_data,
				contentType: false,
				//cache: false,
				processData:false,
				dataType : 'JSON',
				success: function(data){
					if(data.success == 1){
						// alert('done');
						//  console.log(data);
						//  var parsed = JSON.parse(data);
						//  console.log(parsed);  
						//  var d = new Date(0);  
						//   d.setUTCSeconds(parsed.userdata.logon);
						//   var da = moment(d).format("DD/MM/YYYY HH:mm A");
						//  da = da.toUpperCase();

						//  var da1 = new Date(0);
						// da1.setUTCSeconds(parsed.userdata.last_activity);
						//   var la = moment(da1).format("DD/MM/YYYY HH:mm A");
						//  la = la.toUpperCase();
						$('#id'+user_id).empty();
						///<tr fname="'+first_name+'" lname="'+last_name+'" email="'+email+'" username="'+user_name+'" phone="'+phone+'" mobile="'+mobile+'"  class="SubscriberPeopleUpdate" user_id="'+user_id+'" ></tr>
						$('#id'+user_id).attr('fname',first_name);
						$('#id'+user_id).attr('lname',last_name);
						$('#id'+user_id).attr('email',email);
						$('#id'+user_id).attr('username',user_name);
						$('#id'+user_id).attr('phone',phone);
						$('#id'+user_id).attr('mobile',mobile);
						$('#id'+user_id).attr('class','SubscriberPeopleUpdate');
						$('#id'+user_id).attr('user_id',user_id);
						//$('#id'+user_id).attr('lname',last_name);

						$('#id'+user_id).append('<td>'+first_name+" "+last_name+'</td><td>#</td><td class="loggedON'+user_id+'" data="'+loggedON+'">'+loggedON+'</td><td class="loggedON'+user_id+'" data="'+lastActivity+'">'+lastActivity+'</td>');
						alert("data saved successfully");
						$('#myModalPeopleUpdate').modal('hide');
						$('.SavePeopleSubscriberUpdate').prop("disabled", false);
					}
					else{
						alert(data.message);
						$('.SavePeopleSubscriberUpdate').prop("disabled", false);
					}
				}
			});
		}
		else
		{
			alert('password does not matched');
			$('.SavePeopleSubscriberUpdate').prop("disabled", true);
		}
})
</script>
<script type="text/javascript">
  $('.updateSettings').click(function(){
    $('.application_cost_setting').val($(this).attr('application_cost'));
    $('.profit_markup_setting').val($(this).attr('profit_markup'));
    $('.gross_margin_percent_setting').val($(this).attr('gross_margin_percent'));
    $('.default_deposit_percent_setting').val($(this).attr('default_deposit_percent'));
    $('.terms_condition_setting').val($(this).attr('terms_condition'));
    $('.xero_consumer_key_setting').val($(this).attr('xero_consumer_key')); 
    $('.xero_share_secret_setting').val($(this).attr('xero_share_secret'));
    $('.xero_invoice_line_account_code_setting').val($(this).attr('xero_invoice_line_account_code'));
    $('.account_name_setting').val($(this).attr('account_name'));
    $('.account_BSB_setting').val($(this).attr('account_BSB'));
    $('.account_number_setting').val($(this).attr('account_number'));
    $('.payment_phone_number_setting').val($(this).attr('payment_phone_number'));
    $('.colour_consultant_item_setting').val($(this).attr('colour_consultant_item'));
    $('#myModalsettingsUpdate').modal('show');
    if($('.modal-backdrop').length > 1)
    {
     $('.modal-backdrop:gt(0)').remove();
    }
  })
</script>
<script>
$('.SaveSettingCompany').click(function(){
   //alert('rk');
   var application_cost = $('.application_cost_setting').val();
   //////////alert(application_cost);
   var profit_markup = $('.profit_markup_setting').val();
   var gross_margin_percent = $('.gross_margin_percent_setting').val();
   var default_deposit_percent = $('.default_deposit_percent_setting').val();
   var terms_condition = $('.terms_condition_setting').val();
   var xero_consumer_key = $('.xero_consumer_key_setting').val(); 
   var xero_share_secret = $('.xero_share_secret_setting').val();
   var xero_invoice_line_account_code = $('.xero_invoice_line_account_code_setting').val();
   var account_name = $('.account_name_setting').val();
   var account_BSB = $('.account_BSB_setting').val();
   var account_number = $('.account_number_setting').val();
   var payment_phone_number = $('.payment_phone_number_setting').val();
   var colour_consultant_item = $('.colour_consultant_item_setting').val();
   var company_id = "<?php echo $_GET['id'];?>"

   var form_data = new FormData();
   
   var myJSON = JSON.stringify({'application_cost':application_cost,'profit_markup':profit_markup,'gross_margin_percent':gross_margin_percent,'default_deposit_percent':default_deposit_percent,'xero_consumer_key':xero_consumer_key,'xero_share_secret':xero_share_secret,'xero_invoice_line_account_code':xero_invoice_line_account_code,'account_name':account_name,'account_BSB':account_BSB,'account_number':account_number,'payment_phone_number':payment_phone_number,'colour_consultant_item':colour_consultant_item,'company_id':company_id}); 
   console.log(myJSON);
   form_data.append('json',myJSON);
   $.ajax({
    url: "http://enacteservices.com/paintpad/webservice/save-company-setting",
     //url:'save-doc',
    type: "POST",
    data: form_data,
    contentType: false,
    //cache: false,
    processData:false,
    success: function(data){
        // alert('done');
        alert("data has been saved successfully");
        console.log(data);
        var parsed = JSON.parse(data);
          
        if(parsed.success==1)
        {

        $('.account_name').text(parsed.data.companySetting.account_name);
        $('.application_cost').text(parsed.data.companySetting.application_cost);
        $('.profit_markup').text(parsed.data.companySetting.profit_markup);
        $('.gross_margin_percent').text(parsed.data.companySetting.gross_margin_percent); 
        $('.default_deposit_percent').text(parsed.data.companySetting.default_deposit_percent);
        $('.terms_condition').text(parsed.data.companySetting.terms_condition);
        $('.xero_consumer_key').text(parsed.data.companySetting.xero_consumer_key); 
        $('.xero_share_secret').text(parsed.data.companySetting.xero_share_secret);
        $('.xero_invoice_line_account_code').text(parsed.data.companySetting.xero_invoice_line_account_code); 
        $('.account_BSB').text(parsed.data.companySetting.account_BSB);
        $('.account_number').text(parsed.data.companySetting.account_number); 
        $('.payment_phone_number').text(parsed.data.companySetting.payment_phone_number);
        $('.colour_consultant_item').text(parsed.data.companySetting.colour_consultant_item);  


        $('.updateSettings').attr('account_name',parsed.data.companySetting.account_name) ;
        $('.updateSettings').attr('application_cost',parsed.data.companySetting.application_cost) ;
        $('.updateSettings').attr('profit_markup',parsed.data.companySetting.profit_markup) ;
        $('.updateSettings').attr('gross_margin_percent',parsed.data.companySetting.gross_margin_percent) ;
        $('.updateSettings').attr('default_deposit_percent',parsed.data.companySetting.default_deposit_percent) ;
        $('.updateSettings').attr('terms_condition',parsed.data.companySetting.terms_condition) ;
        $('.updateSettings').attr('xero_consumer_key',parsed.data.companySetting.xero_consumer_key) ;
        $('.updateSettings').attr('xero_share_secret',parsed.data.companySetting.xero_share_secret) ;
        $('.updateSettings').attr('xero_invoice_line_account_code',parsed.data.companySetting.xero_invoice_line_account_code) ;
        $('.updateSettings').attr('account_BSB',parsed.data.companySetting.account_BSB) ;
        $('.updateSettings').attr('account_number',parsed.data.companySetting.account_number) ;
        $('.updateSettings').attr('payment_phone_number',parsed.data.companySetting.payment_phone_number) ;
        $('.updateSettings').attr('colour_consultant_item',parsed.data.companySetting.colour_consultant_item) ;
        	 $('#myModalsettingsUpdate').modal('hide');

        }
    } 
  })
})
</script>

<script>
  $('.editEmailSetting').click(function(){
    $('#editEmailSettingquote').modal('show');
    var name = $(this).attr('serveremailaddress');
    var bcc = $(this).attr('serverbccaddress');
    var subject = $(this).attr('quoteemailsubject');
    var email_setting_id = $(this).attr('email_setting_id');
    $('.attachmentquote').each(function(){
      var nameAttachment = $(this).val();
      var id = $(this).attr('data-id');
       $('.quoteEmailSettings').append("<tr class='"+id+"quote'><input type='hidden' class='docquoteid' value='"+id+"'><td></td><td><input type='text' class='docquote form-control' value='"+nameAttachment+"' readonly></td><td class='deletequotedoc' id='"+id+"'>X</td></tr>");
    })
    // $('.bcc_to_quote').val(name);
    // $('.email_subject_quote').val(subject);
    //$('').val(name);
    $('.bcc_to_quote').val(bcc);
    $('.email_subject_quote').val(subject);
    $('.email_setting_id').val(email_setting_id);
    //bcc_to_quote

    if($('.modal-backdrop').length > 1)
    {
     $('.modal-backdrop:gt(0)').remove();
    }
  })
</script>

<script type="text/javascript">
  $('.editEmailSettingJss').click(function(){
    $('#editEmailSettingjss').modal('show');
    var name = $(this).attr('serveremailaddress');
    var bcc = $(this).attr('serverbccAddress');
    var subject = $(this).attr('quoteemailsubject');
    var email_setting_id = $(this).attr('email_setting_id');

    $('.attachmentjss').each(function(){
      var nameAttachment = $(this).val();
      var id = $(this).attr('data-id');
       $('.quoteEmailSettings').append("<tr class='"+id+"quote'><input type='hidden' class='docquoteid' value='"+id+"'><td></td><td><input type='text' class='docquote form-control' value='"+nameAttachment+"' readonly></td><td class='deletequotedoc' id='"+id+"'>X</td></tr>");
    })
    //$('').val(name);
    $('.bcc_to_quote').val(bcc);
    $('.email_subject_quote').val(subject);
    $('.email_setting_id').val(email_setting_id);    
    //bcc_to_quote

    if($('.modal-backdrop').length > 1)
    {
     $('.modal-backdrop:gt(0)').remove();
    }
  })
</script>

<script>
  $('.emailQuoteDoc').change(function(){
    var name = $(this).find("option:selected").text();
    $('.quoteEmailSettings').append("<tr class='"+$(this).val()+"quote'><input type='hidden' class='docquoteid' value='"+$(this).val()+"'><td></td><td><input type='text' class='docquote form-control' value='"+name+"' readonly></td><td class='deletequotedoc' id='"+$(this).val()+"'>X</td></tr>");
  })
</script>

<script>
  $(document).on('click','.deletequotedoc',function(){
    var id = $(this).attr('id');
   $(this).closest("tr").remove();
    //$('.'+id+'quote').remove();
  })
</script>

<script>
  $('.saveEmailSettingquote').click(function(){
    var id = [];
    $('.docquoteid').each(function(){
        id.push($(this).val());
    })
    console.log(id);
    var unique = id.filter(function(itm, i, id) {
      return i == id.indexOf(itm);
    });
    console.log(unique);
    var id = unique.join(",");
    console.log(id);   
    var email_setting_id = $('.email_setting_id').val();
    var bccTo =  $('.bcc_to_quote').val();
    var subject =  $('.email_subject_quote').val();
    var form_data = new FormData();
    alert(email_setting_id);
    if(email_setting_id == "")
    {
      email_setting_id = '0';
    }   
    var myJSON = JSON.stringify({'doc_id':id,'company_id':<?php echo $_GET['id'];?>,'bcc_to':bccTo,'quote_email_subject':subject,'type_id':'1','email_setting_id':email_setting_id}); 
   console.log(myJSON);
   form_data.append('json',myJSON); 
   $.ajax({
          url: "http://enacteservices.com/paintpad/webservice/save-email-setting",
          //url:'save-doc',
          type: "POST",
          data: form_data,
          contentType: false,
          //cache: false,
          processData:false,
          success: function(data){
               //alert('done');
               console.log(data);
               var parsed = JSON.parse(data);
               console.log(parsed);  
               alert("data saved successfully");
                // $('.prepBody').append('<tr class="prepEdit " name="'+parsed.data.prepLevel[0].name+'" id="'+parsed.data.prepLevel[0].id+'" deletable="'+parsed.data.prepLevel[0].deletable+'" is_default="'+parsed.data.prepLevel[0].is_default+'" uplift_cost="'+parsed.data.prepLevel[0].uplift_cost+'" uplift_time="'+parsed.data.prepLevel[0].uplift_time+'"><td>'+parsed.data.prepLevel[0].name+'</td><td>'+parsed.data.prepLevel[0].uplift_cost+'</td><td>'+parsed.data.prepLevel[0].uplift_time+'</td></tr>')
                 //$('#createPrep').modal('hide');
                    function isEmpty(obj) {
                    for(var key in obj) {
                      if(obj.hasOwnProperty(key))
                         return false;
                    }
                     return true;
                    }

                    ///quote email setting fill up starts here
                    $('.serverEmailAddress').text(parsed.data.quoteEmailSettingsDetail.server_email_address);
                    $('.serverBccAddress').text(parsed.data.quoteEmailSettingsDetail.bcc_to);
                    $('.quoteEmailSubject').text(parsed.data.quoteEmailSettingsDetail.email_subject);
                    $('.editEmailSetting').attr('serverEmailAddress',parsed.data.quoteEmailSettingsDetail.server_email_address);
                    $('.editEmailSetting').attr('serverBccAddress',parsed.data.quoteEmailSettingsDetail.bcc_to);
                    $('.editEmailSetting').attr('quoteEmailSubject',parsed.data.quoteEmailSettingsDetail.email_subject);
                    $('.editEmailSetting').attr('email_setting_id',parsed.data.quoteEmailSettingsDetail.email_setting_id); 
                    //$('.editEmailSetting').attr('doc',parsed.data.quoteEmailSettingsDetail.email_subject);
                    if(isEmpty(parsed.data.quoteEmailSettingsDetail))
                    {

                    }
                    else
                    {
                      for(var i=0; i<parsed.data.quoteEmailSettingsDetail.doc.length; i++)
                    {
                    $('.quoteIncludeDocuments').append('<tr><td></td><td><input type="text" class ="attachmentquote" readonly value="'+parsed.data.quoteEmailSettingsDetail.doc[i].file_name+'" data-id ="'+parsed.data.quoteEmailSettingsDetail.doc[i].file_id+'"></td></tr>')
                    }
                    }

                   // $('#editEmailSettingjss').modal('hide');                 
                   $('#editEmailSettingquote').modal('hide');
         } 
    })   
  })
</script>

<script>
  $('.saveEmailSettingjss').click(function(){
    var id = [];
    $('.docquoteid').each(function(){
        id.push($(this).val());
    })
    console.log(id);
    var unique = id.filter(function(itm, i, id) {
      return i == id.indexOf(itm);
    });
    console.log(unique);
    var id = unique.join(",");
    console.log(id); 
    var email_setting_id = $('.email_setting_id').val();
    var bccTo =  $('.bcc_to_jss').val();
    var subject =  $('.email_subject_jss').val();
    var form_data = new FormData();
    if(email_setting_id=="")
    {
      email_setting_id = '0'
    }
   
    var myJSON = JSON.stringify({'doc_id':id,'company_id':<?php echo $_GET['id'];?>,'bcc_to':bccTo,'quote_email_subject':subject,'type_id':2,'email_setting_id':email_setting_id}); 
   console.log(myJSON);
   form_data.append('json',myJSON);    
   $.ajax({
          url: "http://enacteservices.com/paintpad/webservice/save-email-setting",
          //url:'save-doc',
          type: "POST",
          data: form_data,
          contentType: false,
          //cache: false,
          processData:false,
          success: function(data){
                    //alert('done');
                    console.log(data);
                    var parsed = JSON.parse(data);
                    console.log(parsed);  
                    alert("data saved successfully");

                    function isEmpty(obj) {
                    for(var key in obj) {
                      if(obj.hasOwnProperty(key))
                         return false;
                    }
                     return true;
                    }

                    ///quote email setting fill up starts here
                    $('.serverEmailAddressjss').text(parsed.data.jssEmailSettingsDetail.server_email_address);
                    $('.serverBccAddressjss').text(parsed.data.jssEmailSettingsDetail.bcc_to);
                    $('.quoteEmailSubjectjss').text(parsed.data.jssEmailSettingsDetail.email_subject);
                    $('.editEmailSettingJss').attr('serverEmailAddress',parsed.data.jssEmailSettingsDetail.server_email_address);
                    $('.editEmailSettingJss').attr('serverBccAddress',parsed.data.jssEmailSettingsDetail.bcc_to);
                    $('.editEmailSettingJss').attr('quoteEmailSubject',parsed.data.jssEmailSettingsDetail.email_subject);
                    $('.editEmailSettingJss').attr('email_setting_id',parsed.data.jssEmailSettingsDetail.email_setting_id); 
                    //$('.editEmailSetting').attr('doc',parsed.data.quoteEmailSettingsDetail.email_subject);
                    if(isEmpty(parsed.data.jssEmailSettingsDetail))
                    {

                    }
                    else
                    {
                      for(var i=0; i<parsed.data.jssEmailSettingsDetail.doc.length; i++)
                    {
                    $('.quoteIncludeDocuments').append('<tr><td></td><td><input type="text" class ="attachmentquote" readonly value="'+parsed.data.jssEmailSettingsDetail.doc[i].file_name+'" data-id ="'+parsed.data.jssEmailSettingsDetail.doc[i].file_id+'"></td></tr>')
                    }
                    }

                    $('#editEmailSettingjss').modal('hide');
         } 
    })     
  })
</script>

<script>
  $('.prepbutton').click(function(){
    $('#createPrep').modal('show');
    if($('.modal-backdrop').length > 1)
    {
     $('.modal-backdrop:gt(0)').remove();
    }
  })
</script>
<script type="text/javascript">
  $('.savePrepCreate').click(function(){
    var name = $('.prep_level').val();
    var uplift_cost = $('.uplift_cost').val();
    var uplift_time = $('.uplift_time').val();
    var prep_id = 0;
    if($('.is_default').prop('checked')==true)
    {
      var is_default = 1;
    }
    else
    {
      var is_default =0;
    }
     var company_id = "<?php echo $_GET['id'];?>";
     var form_data = new FormData();

     var myJSON = JSON.stringify({'name':name,'uplift_cost':uplift_cost,'uplift_time':uplift_time,'id':prep_id,'is_default':is_default,'company_id':company_id}); 
     console.log(myJSON);
    form_data.append('json',myJSON);
      $.ajax({
          url: "http://enacteservices.com/paintpad/webservice/save-prep-level",
          //url:'save-doc',
          type: "POST",
          data: form_data,
          contentType: false,
          //cache: false,
          processData:false,
          success: function(data){
               //alert('done');
               console.log(data);
               var parsed = JSON.parse(data);
               console.log(parsed);  
               alert("data saved successfully");
                $('.prepBody').append('<tr class="prepEdit " name="'+parsed.data.prepLevel[0].name+'" id="'+parsed.data.prepLevel[0].id+'" deletable="'+parsed.data.prepLevel[0].deletable+'" is_default="'+parsed.data.prepLevel[0].is_default+'" uplift_cost="'+parsed.data.prepLevel[0].uplift_cost+'" uplift_time="'+parsed.data.prepLevel[0].uplift_time+'"><td>'+parsed.data.prepLevel[0].name+'</td><td>'+parsed.data.prepLevel[0].uplift_cost+'</td><td>'+parsed.data.prepLevel[0].uplift_time+'</td></tr>')
                 $('#createPrep').modal('hide');
         } 
      })

  })
</script>
<script>
  $(document).on('click','.prepEdit',function(){
    var name = $(this).attr('name');
    var id = $(this).attr('id');
    var is_default = $(this).attr('is_default');
    var uplift_time = $(this).attr('uplift_time');
    var uplift_cost = $(this).attr('uplift_cost');
    var deletable = $(this).attr('deletable');
    if(deletable==1)
    {
      $('.deletePREP').html("<span class='deletePrep pull-right btn btn-danger'  id='"+id+"'>Delete</span>")
    }
    $('#UpdatePrep').modal('show');
    if($('.modal-backdrop').length > 1)
    {
     $('.modal-backdrop:gt(0)').remove();
    }
    $('.prep_levelupdate').val(name);
    $('.uplift_costupdate').val(uplift_cost);
    $('.uplift_timeupdate').val(uplift_time);
    $('.idprep').val(id);
    if(is_default==1)
    {
      $('.is_defaultupdate').attr('checked','checked');
    }

  })
</script>
<script type="text/javascript">
  $('.savePrepUpdate').click(function(){
    var name = $('.prep_levelupdate').val();
    var uplift_cost = $('.uplift_costupdate').val();
    var uplift_time = $('.uplift_timeupdate').val();
    var prep_id = $('.idprep').val();
    if($('.is_defaultupdate').prop('checked')==true)
    {
      var is_default = 1;
    }
    else
    {
      var is_default =0;
    }
     var company_id = "<?php echo $_GET['id'];?>";
     var form_data = new FormData();

     var myJSON = JSON.stringify({'name':name,'uplift_cost':uplift_cost,'uplift_time':uplift_time,'id':prep_id,'is_default':is_default,'company_id':company_id}); 
    console.log(myJSON);
    form_data.append('json',myJSON);
      $.ajax({
          url: "http://enacteservices.com/paintpad/webservice/save-prep-level",
          //url:'save-doc',
          type: "POST",
          data: form_data,
          contentType: false,
          //cache: false,
          processData:false,
          success: function(data){
              // alert('done');
               console.log(data);
               var parsed = JSON.parse(data);
               console.log(parsed);  
                alert("data saved successfully");
                $('#'+parsed.data.prepLevel[0].id).attr('class','prepEdit');
                $('#'+parsed.data.prepLevel[0].id).attr('name',parsed.data.prepLevel[0].name);
                $('#'+parsed.data.prepLevel[0].id).attr('id',parsed.data.prepLevel[0].id);
                $('#'+parsed.data.prepLevel[0].id).attr('deletable',parsed.data.prepLevel[0].deletable);
                $('#'+parsed.data.prepLevel[0].id).attr('is_default',parsed.data.prepLevel[0].is_default);
                $('#'+parsed.data.prepLevel[0].id).attr('uplift_cost',parsed.data.prepLevel[0].uplift_cost);
                $('#'+parsed.data.prepLevel[0].id).attr('uplift_time',parsed.data.prepLevel[0].uplift_time);
                // <tr class="prepEdit" name="'+parsed.data.prepLevel[0].name+'" id="'+parsed.data.prepLevel[0].id+'" deletable="'+parsed.data.prepLevel[0].deletable+'" is_default="'+parsed.data.prepLevel[0].is_default+'" uplift_cost="'+parsed.data.prepLevel[0].uplift_cost+'" uplift_time="'+parsed.data.prepLevel[0].uplift_time+'">
                $('#'+parsed.data.prepLevel[0].id).html('<td>'+parsed.data.prepLevel[0].name+'</td><td>'+parsed.data.prepLevel[0].uplift_cost+'</td><td>'+parsed.data.prepLevel[0].uplift_time+'</td>')
                 $('#UpdatePrep').modal('hide');
         } 
      })

  })  
</script>
<script>
$(document).on('click','.deletePrep',function(e){
  //alert($(this).attr('id'));
  e.preventDefault();
  var form_data = new FormData();
  var id = $(this).attr('id');
  var myJSON = JSON.stringify({'id':$(this).attr('id')}); 
  console.log(myJSON);
  form_data.append('json',myJSON);
    $.ajax({
      url: "http://enacteservices.com/paintpad/webservice/delete-prep-level",
      //url:'save-doc',
      type: "POST",
      data: form_data,
      contentType: false,
      //cache: false,
      processData:false,
      success: function(data){
          console.log(data);
          var parsed = JSON.parse(data);
          console.log(parsed);
          if(parsed.success==1)
          {
            alert('prep deleted');
             $('#'+id).remove();
             $('#UpdatePrep').modal('hide');
            // return false;
          }
     } 
  })
   e.stopImmediatePropagation();
   return false;  
})
</script>