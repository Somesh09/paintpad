<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TblBrandsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-company-search tbl-search-input-all">
    

    <?php $form2 = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
        'data-pjax' => 1

    ],
    ]); ?>
    <div style="width:23%; margin-right:0px; border:1px;">
       <?= $form2->field($searchModelQuotes, 'client_name')->textInput(['placeholder' => "Search Quote"])->label(false); ?>
    </div>
    <div style="" class="search-top-div">
        <?php
            echo $form2->field($searchModelQuotes, 'status')->dropDownList(
                        ['' => 'SHOW ALL', '0' => 'Soft-Deleted', '1' => 'Pending', '2' => 'In-progress', '3' => 'Completed', '4' => 'Approved', '5' => 'Declined', '6' => 'New', '7' => 'Submitted', '8' => 'Open'])->label(false); 
        ?>

    </div>

    <?php ActiveForm::end(); ?>

</div>