<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TblProducts */

//$this->title = 'Update Tbl Productss: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Tbl Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->product_id]];
$this->params['breadcrumbs'][] = 'Update';

//echo '<pre>'; print_r($modelsecond); exit;
?>

<input type="hidden" id="pdtname" value="<?php echo $model->name;?>">
<div class="tbl-products-update">

    <!--<h1><?= Html::encode($this->title) ?></h1> -->

    <?= $this->render('_formproductupdate', [
        'model' => $model,
        'id'=>$id,
        'modelsecond'=>$modelsecond,
        'modelstock'=>$modelstock,
        'modelstockData'=>$modelstockData,
    ]) ?>

</div>
