<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TblBrandsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-company-search tbl-search-input-all">
    

    <?php $form1 = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
        'data-pjax' => 1

    ],
    ]); ?>
    <div style="width:23%; margin-right:0px; border:1px;">
       <?= $form1->field($searchModelActivity, 'name')->textInput(['placeholder' => "Search User"])->label(false); ?>
    </div>
    <div style="" class="search-top-div">
        <?php
            echo $form1->field($searchModelActivity, 'group')->dropDownList(
                        ['' => 'SHOW ALL', '0' => 'Admin', '1' => 'Sales Rep'])->label(false); 
        ?>

    </div>

    <?php ActiveForm::end(); ?>

</div>