<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TblBrandsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-company-search tbl-search-input-all">
    

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
        'data-pjax' => 1

    ],
    ]); ?>
    <div style="width:23%; margin-right:0px; border:1px;">
       <?= $form->field($model, 'name')->textInput(['placeholder' => "Search Subscriber"])->label(false); ?>
    </div>
    <div style="" class="search-top-div">
        <?php
            echo $form->field($model, 'status')->dropDownList(
                        ['' => 'SHOW ALL', '1' => 'Active', '0' => 'Archived'])->label(false); 
        ?>

    </div>

    <?php ActiveForm::end(); ?>

</div>