<?php
use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\bootstrap\ButtonDropdown;
use dosamigos\grid\GridView;
use dosamigos\exportable\ExportableButton; 
use kartik\export\ExportMenu;
?>

<style>
table, th, td {
  border-bottom: 1px solid black;
  border-collapse: collapse;
}
th, td {
  padding: 5px;
  text-align: left;
}
tr{
  background: #f7f4f4;
  margin-bottom: 5px;
}
table{
    margin: 0;
    width: 100%;
    margin-top: 20px;
}
</style>

<div class="tbl-brand-index appointmentpage" style="width: 100%;margin: auto;">
  <span style="font-size: 22px;">Appointment Types</span>
  <span style="font-size: :20px; margin:0px 5px;" class="pull-right btn btn-large btn-primary appointmentTypebutton"><img src="/app/admin/uploads/add.png" width="17" height="17"></span>
  <span style="font-size: :20px" class="pull-right btn btn-large btn-primary appointmentTypeSortbutton">Sort Appointment</span>
  <!-- <h3>Appointment Types</h3> -->



<table>
  <thead>
    <tr>
      <th colspan="2">Name</th>
    </tr>
  </thead>
  <tbody class="AppointmentBody">
    
  </tbody> 
</table>

</div>

<!--bootstrap modal for creating room type-->
<div class="modal modal-child" data-backdrop-limit="1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-modal-parent="#myModal" id="myappointmentTypeModal">  
  <div class="modal-dialog">
    <div class="modal-content">
    <div class="modal-header">
      <button type="button" onclick="$('#myappointmentTypeModal').modal('hide')" class="pull-right btn btn-primary">&times;</button>
      <input type="button" class="saveAppointmentType pull-right btn btn-primary" value="Save" >
<!--       <tr style="display: none;" class="delAppointmentTypeRow">
            <td colspan="2"> -->
              <button  style="display: none;"class="delAppointmentTypeRow delAppointmenttype btn btn-primary" data-id="0">Delete</button>              
        <!--     </td>
          </tr> -->
      <input type="hidden" class="idprep">
      <h4 class="modal-title">Create Appointment Type</h4>
      <div class="deletePREP"></div>
      <input type="hidden" >
    </div>
    <div class="modal-body">
      <table class="table appointmentTYPE">
          <tr>
            <td>
              <label>Name</label>
            </td>
            <td>
              <input type="text" class="form-control appointmentTypeName" id="appointmentTypeName" name="name">
              <input type="hidden" class="form-control appointmentTypeId" value="0" name="id">
              <?php if (Yii::$app->user->identity->role == 20) { ?>
                 <input type="hidden" class="form-control company_id" value="0" name="company_id">
              <?php } else{?>
                 <input type="hidden" class="form-control company_id" value="<?php echo Yii::$app->user->identity->company_id ;?>" name="company_id">
              <?php }?>
              
            </td>
          </tr>      
          <!-- <tr style="display: none;" class="delAppointmentTypeRow">
            <td colspan="2">
              <button class="delAppointmenttype btn btn-primary" data-id="0">Delete</button>              
            </td>
          </tr> -->                                                                                                                                    
      </table>
    </div>
    <div class="modal-footer">
     <!-- <button type="button" id="myBtn" onclick="$('#myroomModal').modal('hide')">Close</button> -->
    </div>
  </div>
  </div>
</div>

<!--bootstrap modal for creating room type end here-->

<!--bootstrap modal for Sort Appointment Types-->
<div class="modal modal-child" data-backdrop-limit="1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-modal-parent="#myModal" id="myappointmentTypeSortModal">  
  <div class="modal-dialog">
    <div class="modal-content">
    <div class="modal-header">
      <button type="button" onclick="$('#myappointmentTypeSortModal').modal('hide')" class="pull-right btn btn-primary">&times;</button>
      <button type="button" id=sortBTN class="pull-right btn btn-primary">Save</button>
      <h4 class="sort-modal-title">Sort Appointment Types</h4>
      <div class="deletePREP"></div>
      <input type="hidden" >
    </div>
    <div class="modal-body">
      <table class="table appointmentTYPEsort">
        <!-- Appointment Types Data Here -->
      </table>
    </div>
  </div>
  </div>
</div>

<!--bootstrap modal for creating room type end here-->


<script>
  $(document).ready(function(){
    getAppointmentTypes('AppointmentBody');   
  });

</script>

<script>
  $('div').on('click','.AppointmentEdit',function(){
    $('.AppointmentEdit').unbind('click');
    //alert('createRoomType');
    var id = $(this).attr('id');
    var name = $($(this).find('td')).text();

    $('.appointmentTypeId').val(id);
    $('.appointmentTypeName').val(name);
    $('#myappointmentTypeModal .delAppointmenttype').data('id',id);
    $('.modal-title').text('Edit Appointment Type');
    $('.delAppointmentTypeRow').show();
     

    $('#myappointmentTypeModal').modal('show');
  })
</script>

<script>
  $('.saveAppointmentType').unbind('click').click(function(e){
      var type_name = $('.appointmentTypeName').val();
      var appointment_type_id = $('.appointmentTypeId').val();
      var company_id = $('.company_id').val();
      // var appointment_type_id = '0'
      var form_data = new FormData();
      var myJSON = JSON.stringify({'name':type_name,'id':appointment_type_id,'company_id':company_id});
      form_data.append('json',myJSON);
      $.ajax({
        url: "//<?php echo $_SERVER['HTTP_HOST'];?>/paintpad/webservice/save-appointment-type",
        type: "POST",
        data: form_data,
        contentType: false,
        //cache: false,
        processData:false,
        success: function(data){
           // console.log(data);
            var parsed = JSON.parse(data);
           // console.log(parsed);
            if(parsed.success==1)
            {
              // alert(parsed.message);
              $('#myappointmentTypeModal').modal('hide');
              // return false; var rowData = '';
              var rowData = '';
              $.each(parsed.data, function(i,v){
                 if (v.delete != 0 || <?php echo Yii::$app->user->identity->role?> == 20) {

                rowData += '<tr  id="'+v.id+'" data-orderid="'+(++i)+'" class="AppointmentEdit"><td>'+v.name+'</td></tr>';
                }
            else{
                 rowData += '<tr  id="'+v.id+'" data-orderid="'+(++i)+'" class="AppointmenteEdit"><td>'+v.name+'</td></tr>';
                }
              });
              $('.AppointmentBody').html(rowData);
            }
            else
            {
              alert(parsed.message)
            }
       } 
      })
      e.stopImmediatePropagation();
      return false;         
  })
</script>

<script>
  $(document).on('click','#sortBTN',function(){
    getAppointmentTypes('AppointmentBody'); 
    $('#myappointmentTypeSortModal').modal('hide')
  });

  $('div').on('click','.AppointmentEdit',function(){
    $('.AppointmentEdit').unbind('click');
    //alert('createRoomType');
    var id = $(this).attr('id');
    var name = $($(this).find('td')).text();

    $('.appointmentTypeId').val(id);
    $('.appointmentTypeName').val(name);
    $('#myappointmentTypeModal .delAppointmenttype').data('id',id);
    $('.modal-title').text('Edit Appointment Type');
    $('.delAppointmentTypeRow').show();
     

    $('#myappointmentTypeModal').modal('show');
    if($('.modal-backdrop').length > 1)
    {
      $('.modal-backdrop:gt(0)').remove();
    }
  })
</script>

<script>
  $('.delAppointmenttype').unbind('click').click(function(){
  // alert($(this).data('id'));
  // e.preventDefault();
  var form_data = new FormData();
  var id = $(this).data('id');
  var myJSON = JSON.stringify({'id':id}); 
 // console.log(myJSON);
  form_data.append('json',myJSON);
    $.ajax({
      url: "//<?php echo $_SERVER['HTTP_HOST'];?>/paintpad/webservice/delete-appointment-type",
      //url:'save-doc',
      type: "POST",
      data: form_data,
      contentType: false,
      //cache: false,
      processData:false,
      success: function(data){
         // console.log(data);
          var parsed = JSON.parse(data);
         // console.log(parsed);
          if(parsed.success==1)
          {
            // alert('Appointment type deleted');
             $('.AppointmentBody #'+id).remove();
             $('#myappointmentTypeModal').modal('hide');
            // return false;
          }
     } 
  })
})
</script>

<script>
  $('.appointmentTypebutton').unbind('click').click(function(){
    //alert('createRoomType');
    $('#appointmentTypeName').val('');
    $('.appointmentTypeId').val('0');
    $('.modal-title').text('Create Appointment Type');
    $('.delAppointmentTypeRow').hide();
    
    $('#myappointmentTypeModal').modal('show');
   
  })
</script>

<script>
  $('.appointmentTypeSortbutton').unbind('click').click(function(){
    getAppointmentTypes('appointmentTYPEsort');
    
   $( ".appointmentTYPEsort" ).sortable({
        delay: 150,
        stop: function() {
            var selectedData = new Array();
            $('.appointmentTYPEsort>tr').each(function() {
                // selectedData[$(this).attr("id")] = $(this).data("orderid");
                selectedData.push($(this).attr("id"));
            });
            updateOrder(selectedData);
        }
    });


    function updateOrder(data) {
        $.ajax({
            url:  "//<?php echo $_SERVER['HTTP_HOST'];?>/paintpad/webservice/updte-appointment-position",
            type:'post',
            data:{position:data},
            success:function(){
                // alert('your change successfully saved');
            }
        })
    }
    
    $('#myappointmentTypeSortModal').modal('show');
   
  })
</script>

<script>
  function getAppointmentTypes(appendToClass){
    $.ajax({
      url:  "//<?php echo $_SERVER['HTTP_HOST'];?>/paintpad/webservice/get-appointment-init?company_id=1&subscriber_id=1",
      type: "GET",
        success: function(res) {
          var pars = JSON.parse(res);
          console.log(pars);
          var rowData = '';
          $.each(pars.data.types, function(i,v){
            if (v.delete != 0 || <?php echo Yii::$app->user->identity->role?> == 20) {
              rowData += '<tr colspan="2" id="'+v.id+'" data-orderid="'+(++i)+'" class="AppointmentEdit"><td>'+v.name+'</td></tr>';
            }
            else{
              rowData += '<tr colspan="2" id="'+v.id+'" data-orderid="'+(++i)+'" class="AppointmentEsdit"><td>'+v.name+'</td></tr>';
            }
          });
          $('.'+appendToClass).html(rowData);
          $('.AppointmentBody').html(rowData);
        }
    });
  }
</script>


  
  
  

