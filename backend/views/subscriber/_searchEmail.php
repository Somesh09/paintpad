<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TblBrandsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-company-search tbl-search-input-all">
    

    <?php $form5 = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
        'data-pjax' => 1

    ],
    ]); ?>
    <div style="width:23%; margin-right:0px; border:1px;">
       <?= $form5->field($searchModelEmailTemp, 'temp_name')->textInput(['placeholder' => "Search Email"])->label(false); ?>
    </div>
    <div style="" class="search-top-div">
        <button class="Add_template" type="button">Add Template</button>
        <?php
            //echo $form5->field($searchModelEmailTemp, 'group')->dropDownList(
             //           ['' => 'SHOW ALL', '1' => 'Admin', '0' => 'Sales Rep'])->label(false); 
        ?>

    </div>

    <?php ActiveForm::end(); ?>

</div>