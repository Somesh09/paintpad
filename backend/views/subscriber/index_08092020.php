<?php
use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\bootstrap\ButtonDropdown;
use dosamigos\grid\GridView;
use dosamigos\exportable\ExportableButton; 
use kartik\export\ExportMenu;

// global 
// global $cRole = "";
?>
<style type="text/css">
  #modalSubscriber .modal-body {
    max-height: calc(100vh - 120px);
    overflow-y: auto;
  }
</style>

<?php if (Yii::$app->user->identity->role == 20) { ?>
  <div class="tbl-brand-index subscriberBoxclass">
  <?php //print_r(Yii::$app->user->identity->role);die; ?>
  <?php Pjax::begin(['timeout' => 5000,'id'=>'my-subscriber-pjax']);?>   

    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php  echo Html::button('<img src="'.Url::base().'/uploads/add.png"width="17" height="17"/>', ['value'=>Url::to( Url::base().'/subscriber/create'), 'class' => 'btn btn-primary pull-right','id'=>'modalButton']);?>
  <?php
    Modal::begin([
      'header'=>'',
      'id'=>'modalSubscriber',
      //'class'=>'main-popup-all',
      'size'=>'modal-lg',
      'clientOptions' => ['backdrop' => 'static'],
    ]);
    echo "<div id='modalContentSubscriber' class='clearfix'>
    <div style='text-align:center'>
     <img src='".Url::base()."/uploads/avatar2.gif' height='100' width='100'>
    </div></div>";
    Modal::end();
  ?>

  <?php
    Modal::begin([
      'header'=>'',
      'id'=>'modalSubscriberCreate',
      //'class'=>'main-popup-all',
      'size'=>'modal-lg',
      'clientOptions' => ['backdrop' => 'static'],
    ]);
    echo "<div id='modalContentSubscriberCreate' class='clearfix'>
    <div style='text-align:center'>
     <img src='".Url::base()."/uploads/avatar2.gif' height='100' width='100'>
    </div></div>";
    Modal::end();
  ?>

      
  <div id="SubscriberSearch" class="main-table-index-page">
  <?=  GridView::widget([
      'dataProvider' => $dataProvider,
      'rowOptions'   => function ($model, $key, $index, $grid) {
                        return  [
                        // "class"=>"modalButton44",
                          "class"=>($model->status != 1)?"modalButton44 disabledTr":"modalButton44",
                        'value'=>Url::base().'/subscriber/update?id='.$model->company_id
                        ];
      },
      'behaviors' => [
          [
          'class' => '\dosamigos\grid\behaviors\LoadingBehavior',
          'type' => 'spinner'
          ]
      ],
      'columns' => [                              
        
        ['attribute'=>'name',
         'label' => 'Name',
         'format' => 'raw',
         'value' =>
           function($model){
             if($model->status != 1){
                return '<strike>'.$model->name.'</strike>';
              }else{
                return $model->name;
              }
           },
         ],
         ['attribute'=>'address',
         'label' => 'Address',
         'format' => 'raw',
         'value' =>
           function($model){
             if($model->status != 1){
                return '<strike>'.$model->address.'</strike>';
              }else{
                return $model->address;
              }
           },
         ],
         ['attribute'=>'phone',
         'label' => 'Phone',
         'format' => 'raw',
         'value' =>
           function($model){
             if($model->status != 1){
                return '<strike>'.$model->phone.'</strike>';
              }else{
                return $model->phone;
              }
           },
         ],
         ['attribute'=>'email',
         'label' => 'Email',
         'format' => 'raw',
         'value' =>
           function($model){
             if($model->status != 1){
                return '<strike>'.$model->email.'</strike>';
              }else{
                return $model->email;
              }
           },
         ],
         ['attribute'=>'website',
         'label' => 'Website',
         'format' => 'raw',
         'value' =>
           function($model){
             if($model->status != 1){
                return '<strike>'.$model->website.'</strike>';
              }else{
                return $model->website;
              }
           },
         ],
        ['class' => 'yii\grid\ActionColumn',
          'header' => 'UPDATE',
          'headerOptions' => ['style' => 'color:#337ab7'],
          'template' => '{update}',
          'buttons' => [
              'update' => function ($url, $model,$id) {
                                  return Html::a('<span class=""><img src="'.Url::base().'/uploads/update.png" width="13" height="13"/></span>', $url, [
                                      "class"=>"modalButton4",'value'=>Url::base().'/subscriber/update?id='.$model->company_id
                                  ]);
                            },
          ],
          'urlCreator' => function ($action, $model, $key, $index) {
               Url::to(['update?id='.$model->company_id],['id'=>'modal'])   ;         
          }
        ],
      ],
  ]); 
  ?>
  </div>

  </div>

  <script type="text/javascript">
    
  $('#modalButton').click(function(){
    $('#modalContentSubscriberCreate')
    .load ($(this).attr('value'));
    console.log('vv')
    console.log($(this).attr('value'))
    $('#modalSubscriberCreate').modal('show')

  })


  $('.modalButton4').click(function(){
     $("#modalContentSubscriber").html(" <div style='text-align:center'><img src='<?php echo Url::base();?>/uploads/avatar2.gif' height='100' width='100'></div>");
    $('#modalContentSubscriber')
    .load ($(this).attr('value'));
    $('#modalSubscriber').modal('show')
  })


  $('.modalButton44').click(function(){
     $("#modalContentSubscriber").html(" <div style='text-align:center'><img src='<?php echo Url::base();?>/uploads/avatar2.gif' height='100' width='100'></div>");
    $('#modalContentSubscriber')
    .load ($(this).attr('value'));
    $('#modalSubscriber').modal('show')

  })

  $(".modal").on("hidden.bs.modal", function(){
      $("#modalContentSubscriber").html("");
      $('.modal-backdrop').remove();
      // $('#modalSubscriberCreate').html("")
  });


  $('#tblcompanysearch-status').bind('change', function() { 
    $('.modal-backdrop').remove();
      $('#tblcompanysearch-status').delay(200).submit();
  });

  $(document).ready(function(){
    $(".modal").modal({
      show:false,
      backdrop:'static'
    });
  });
  </script>
  <?php Pjax::end();?>
<?php } ?>

<div class="tbl-brand-index yserActivityBoxclass">

<?php Pjax::begin(['timeout' => 5000,'id'=>'my-Activity-pjax']);?>   
<?php echo $this->render('_searchActivity', ['searchModelActivity' => $searchModelActivity]); ?>


    
<div id="ActivitySearch" class="main-table-index-page">
<?=  GridView::widget([
    'dataProvider' => $dataProviderActivity,
    'behaviors' => [
        [
        'class' => '\dosamigos\grid\behaviors\LoadingBehavior',
        'type' => 'spinner'
        ]
    ],
    'columns' => [                              
      ['attribute'=>'online',
       'label' => '',
       'format' => 'raw',
       'value' =>
         function($model){
            if ($model->logIn == 1) {
              return '<span class="online"></span>';
            }
            else{
              return '';
            }
            
         },
       ],
      ['attribute'=>'name',
       'label' => 'User Name',
       'format' => 'raw',
       'value' =>
         function($model){
           
              return $model->name;
            
         },
       ],
       ['attribute'=>'group',
       'label' => 'Group',
       'format' => 'raw',
       'value' =>
         function($model){
            if ($model->group == 1) {
              return 'Sales Rep';
            }
            else{
              return 'Admin';
            }
            
         },
       ],
       ['attribute'=>'loggedon',
       'label' => 'Logged On',
       'format' => 'raw',
       'value' =>
         function($model){

            return date('d, M Y H:m A', $model->timestamp);
            
         },
       ],
       ['attribute'=>'lastactivity',
       'label' => 'Last Activity',
       'format' => 'raw',
       'value' =>
         function($model){

           return date('d, M Y H:m A', $model->timestamp);
            
         },
       ],
    ],
]); 
?>
</div>

</div>
<script type="text/javascript">
  $(document).ready(function(){
  // var url = window.location.href;
  // var n = url.search("TblCompanySearch%5Bname%5D=&TblCompanySearch%5Bstatus%5D=");
  // console.log(n)
  var v = $('#tblcompanysearch-name').val()
  // if (v != "") {
    $('#tblcompanysearch-name').focus().val("").val(v);
    
  // }
  var searchActivity = URLToArray(window.location.search)

  $('#tbluseractivityseach-name').focus().val("").val(searchActivity['TbluserActivitySeach[name]'])
  $('#tbluseractivityseach-group').val(searchActivity['TbluserActivitySeach[group]'])
  function URLToArray(url) {
    var request = {};
    var pairs = url.substring(url.indexOf('?') + 1).split('&');
    for (var i = 0; i < pairs.length; i++) {
        if(!pairs[i])
            continue;
        var pair = pairs[i].split('=');
        request[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1]);
     }
     return request;
}
  // console.log(searchActivity)
})

</script>
 <?php Pjax::end();?>
<div class="tbl-brand-index yserquotesBoxclass">

<?php Pjax::begin(['timeout' => 5000,'id'=>'my-quotes-pjax']);?>   
<?php echo $this->render('_searchQuotes', ['searchModelQuotes' => $searchModelQuotes]); ?>


    
<div id="QuoteSearch" class="main-table-index-page">
<?=  GridView::widget([
    'dataProvider' => $dataProviderQuotes,
    // 'pager' => [
    //     'firstPageLabel' => 'First',
    //     'lastPageLabel' => 'Last',
    // ],
    'behaviors' => [
        [
        'class' => '\dosamigos\grid\behaviors\LoadingBehavior',
        'type' => 'spinner'
        ]
    ],
    'columns' => [                              
    
      ['attribute'=>'agent',
       'label' => 'Agent',
       'format' => 'raw',
       'value' =>
         function($model){
           
              return $model->contact_name;
            
         },
       ],
       ['attribute'=>'project',
       'label' => 'Project',
       'format' => 'raw',
       'value' =>
         function($model){
           
              return 'Project '.$model->quote_id .' '.$model->client_name;
            
         },
       ],
       ['attribute'=>'client',
       'label' => 'Client',
       'format' => 'raw',
       'value' =>
         function($model){
           
              return $model->client_name;
            
         },
       ],
       ['attribute'=>'created',
       'label' => 'Created',
       'format' => 'raw',
       'value' =>
         function($model){
           
              return date('d/m/Y', $model->created_at);
            
         },
       ],
       ['attribute'=>'amount',
       'label' => 'Total Value',
       'format' => 'raw',
       'value' =>
         function($model){
           
              return '$'.$model->price;
            
         },
       ],
       ['attribute'=>'status',
       'label' => 'Status',
       'format' => 'raw',
       'value' =>
         function($model){
              if($model->status == 1){
                $statusStr = "Pending";
              }  
              if($model->status == 0){
                $statusStr = "Soft-Deleted";
              }    
              if($model->status == 2){
                $statusStr = "In-progress";
              }
              if($model->status == 3){
                $statusStr = "Completed";
              }
              if($model->status == 4){
                $statusStr = "Accepted";
              }
              if($model->status == 5){
                $statusStr = "Declined";
              }
              if($model->status == 6){
                $statusStr = "New";
              }
              if($model->status == 7){
                $statusStr = "Offered";
              }
              if($model->status == 8){
                $statusStr = "Open";
              }
              return  $statusStr ;
         },
       ],
    ],
]); 
?>
</div>

</div>
<script type="text/javascript">
  var searchActivity = URLToArray(window.location.search)

  $('#tblquotessearch-client_name').focus().val("").val(searchActivity['TblQuotessearch[client_name]'])
  $('#tblquotessearch-status').val(searchActivity['TblQuotessearch[status]'])
  function URLToArray(url) {
    var request = {};
    var pairs = url.substring(url.indexOf('?') + 1).split('&');
    for (var i = 0; i < pairs.length; i++) {
        if(!pairs[i])
            continue;
        var pair = pairs[i].split('=');
        request[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1]);
     }
     return request;
}
</script>
<script >
//


//$('#modalSubscriber .modal-header').remove();



$(document).on('keyup','#tblcompanysearch-name',function(){
  $('#my-subscriber-pjax').find('form').submit();
});
$(document).on('keyup','#tbluseractivityseach-name',function(){
  $('#my-Activity-pjax').find('form').submit();

});
$(document).on('change','#tbluseractivityseach-group',function(){
  // $('#w2').submit();
  $('#my-Activity-pjax').find('form').submit();
});
$(document).on('keyup','#tblquotessearch-client_name',function(){
  $('#my-quotes-pjax').find('form').submit();
});
$(document).on('change','#tblquotessearch-status',function(){
   $('#my-quotes-pjax').find('form').submit();
});
// // Change the selector if needed
// var $table = $('table.table'),
//     $bodyCells = $table.find('tbody tr:first').children(),
//     colWidth;

// // Adjust the width of thead cells when window resizes
// $(window).resize(function() {
//     // Get the tbody columns width array
//     colWidth = $bodyCells.map(function() {
//         return $(this).width();
//     }).get();
    
//     // Set the width of thead columns
//     $table.find('thead tr').children().each(function(i, v) {
//         $(v).width(colWidth[i]);
//     });    
// }).resize(); // Trigger resize handler
</script>

   <?php Pjax::end();?>

<div class="tbl-brand-index emailTempBoxclass">

<?php Pjax::begin(['timeout' => 5000,'id'=>'my-email-pjax']);?>   
<?php echo $this->render('_searchEmail', ['searchModelEmailTemp' => $searchModelEmailTemp]); ?>


    
<div id="ActivitySearch" class="main-table-index-page">
<?=  GridView::widget([
    'dataProvider' => $dataProviderEmail,
    'behaviors' => [
        [
        'class' => '\dosamigos\grid\behaviors\LoadingBehavior',
        'type' => 'spinner'
        ]
    ],
    'rowOptions'   => function ($model, $key, $index, $grid) {
                      return  [
                      // "class"=>"modalButton44",
                        "class"=>"moelss",
                         // "data-toggle"=>"modal" ,
                         // "data-target"=>"#addEmail",
                        "data-email"=>html_entity_decode($model->temp_body),
                        "data-name"=>$model->temp_name,
                        "data-desc"=>$model->description,
                        "data-home"=>$model->email_type,
                      // 'value'=>Url::base().'/subscriber/update?id='.$model->company_id
                      ];
    },

    'columns' => [                              
      ['attribute'=>'name',
       'label' => 'Name',
       'format' => 'raw',
       'contentOptions' => function($model){
        if ($model->delete != 0 || Yii::$app->user->identity->role == 20) {
           return ['class' => 'open_addmodel'];
        }
           else{
             return ['class' => 'open_addmodels'];
           }
         },
       
       'value' =>
         function($model){
            return $model->temp_name;
         },
       ],
                                
      ['attribute'=>'email_type',
       'label' => 'Email Type',
       'format' => 'raw',
       'value' =>
         function($model){
          if ($model->email_type == 0) {
            return 'Homeowner';
          }
          else if ($model->email_type == 1) {
            return 'Multi-Dwelling Building';
          }
          else if ($model->email_type == 2) {
            return 'Home Builder';
          }
            
         },
       ],
       ['attribute'=>'clone',
       'label' => 'Clone',
       'format' => 'raw',
       'value' =>
         function($model){
            return '<button class="clone_template" data-id="'.$model->temp_id.'">Clone</button>';
         },
       ],
       ['attribute'=>'delete',
       'label' => 'Delete',
       'format' => 'raw',
       'value' =>
         function($model){
          if ($model->delete != 0 || Yii::$app->user->identity->role == 20) {
           return '<button class="delete_template" data-id="'.$model->temp_id.'">Delete</button>';
          }
          else{
            return '-';
          } 
         },
       ],
      
    ],
]); 
?>
</div>

</div>
<div class="modal fade" id="addEmail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form id="update_form">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabels">Update Template</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="left-modal-div">
        <label>Template Name</label>
        <input type="text" name="template_name" id="template_name">
        <input type="hidden" name="temp_id" id="temp_iddd">
        <label>Description</label>
          <input type="text" name="description" class="descriptionU" required>
          <label>Type</label>
          <select name="typeHome" class="typeHomeU" required>
            <option value="0">Homeowner</option>
            <option value="1">Multi-Dwelling Building</option>
            <option value="2">Home Builder</option>
          </select>
          </div>
          <div class="admin-right-tags">
            <h1>Tags:</h1>
            <ul class="temp-tags">
              <li class="tagsInsert1" data-value="{:client_contact_name:}">Client Name </li>
              <li class="tagsInsert1" data-value="{:currentUser_name:}">Username</li>
              <li class="tagsInsert1" data-value="{:currentUser_title:}">Title </li>
              <li class="tagsInsert1" data-value="{:currentUser_mobile:}">Mobile </li>
              <li class="tagsInsert1" data-value="{:currentUser_phone:}">Phone </li>
              <li class="tagsInsert1" data-value="{:currentUser_email:}">Email </li>
              <li class="tagsInsert1" data-value="{:agent_website:}">Website </li>
              <li class="tagsInsert1" data-value="{:agent_logo_url:}">Logo </li>
              <li class="tagsInsert1" data-value="{:proj_id:}">Quote Id</li>
              <li class="tagsInsert1" data-value="{:site_address1:}">Address</li>
              <li class="tagsInsert1" data-value="{:site_suburb:}">Suburb</li>
              <li class="tagsInsert1" data-value="{:agent_name:}">Name</li>
            </ul>
          </div>
        
        <textarea id="full-featured-non-premium" name="tempLate"></textarea>
      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
    </div>
  </form>
  </div>
</div>
<div class="modal fade" id="update_email" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form id="add_form">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add Template</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="left-modal-div">
          <label>Template Name</label>
          <?php if (Yii::$app->user->identity->role != 20) { ?>
            <input type="hidden" name="company_id" id="company_id" value="<?php  echo Yii::$app->user->identity->company_id; ?>">
          <?php } else{?>
            <input type="hidden" name="company_id" id="company_id" value="0">
          <?php } ?>
          

          <input type="text" name="template_name" class="template_name" required>
          <label>Description</label>
          <input type="text" name="description" class="description" required>
          <label>Type</label>
          <select name="typeHome" class="typeHome" required>
            <option value="0">Homeowner</option>
            <option value="1">Multi-Dwelling Building</option>
            <option value="2">Home Builder</option>
          </select>
        </div>
        <div class="admin-right-tags">
            <h1>Tags:</h1>
          <ul class="temp-tags">
          <li class="tagsInsert" data-value="{:client_contact_name:}">Client Name </li>
          <li class="tagsInsert" data-value="{:currentUser_name:}">Username</li>
          <li class="tagsInsert" data-value="{:currentUser_title:}">Title </li>
          <li class="tagsInsert" data-value="{:currentUser_mobile:}">Mobile </li>
          <li class="tagsInsert" data-value="{:currentUser_phone:}">Phone </li>
          <li class="tagsInsert" data-value="{:currentUser_email:}">Email </li>
          <li class="tagsInsert" data-value="{:agent_website:}">Website </li>
          <li class="tagsInsert" data-value="{:agent_logo_url:}">Logo </li>
          <li class="tagsInsert" data-value="{:proj_id:}">Quote Id</li>
          <li class="tagsInsert" data-value="{:site_address1:}">Address</li>
          <li class="tagsInsert" data-value="{:site_suburb:}">Suburb</li>
          <li class="tagsInsert" data-value="{:agent_name:}">Name</li>
        </ul>
      </div>
          <input type="hidden" name="new" value="1">
          <textarea id="full-featured-non-premium2" name="tempLate"></textarea>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->
          <button type="submit" class="btn btn-primary">Save</button>
        </div>
      </div>
    </form>
  </div>
</div>
  <script src="https://cdn.tiny.cloud/1/4occtt7zoonycf968h3nbhn7f08kr3cqy3gnakngvoizooji/tinymce/5/tinymce.min.js" referrerpolicy="origin">
    </script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript">
      $(document).ready(function(){
        tinyMCE.remove()
        tinymce.init({
            selector: 'textarea#full-featured-non-premium',
            plugins: 'print preview paste importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help charmap quickbars emoticons',
            imagetools_cors_hosts: ['picsum.photos'],
            menubar: 'file edit view insert format tools table help',
            toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor backcolor removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | insertfile image media template link anchor codesample | ltr rtl',
            toolbar_sticky: true,
            autosave_ask_before_unload: true,
            autosave_interval: "30s",
            autosave_prefix: "{path}{query}-{id}-",
            autosave_restore_when_empty: false,
            autosave_retention: "2m",
            image_advtab: true,
            // content_css: '//www.tiny.cloud/css/codepen.min.css',
            // link_list: [
            //   { title: 'My page 1', value: 'http://www.tinymce.com' },
            //   { title: 'My page 2', value: 'http://www.moxiecode.com' }
            // ],
            image_list: [
              { title: 'Logo', value: 'https://app.paintpad.com/paintpad/common/mail/views/logo.png' },
              // { title: 'My page 2', value: 'http://www.moxiecode.com' }
            ],
            image_class_list: [
              { title: 'None', value: '' },
              { title: 'Some class', value: 'class-name' }
            ],
            importcss_append: true,
            file_picker_callback: function (callback, value, meta) {
            //   /* Provide file and text for the link dialog */
            //   if (meta.filetype === 'file') {
            //     callback('https://www.google.com/logos/google.jpg', { text: 'My text' });
            //   }

            //   /* Provide image and alt text for the image dialog */
              if (meta.filetype === 'image') {
                callback('https://app.paintpad.com/paintpad/common/mail/views/logo.png', { alt: 'logo' });
              }

            //   /* Provide alternative source and posted for the media dialog */
            //   if (meta.filetype === 'media') {
            //     callback('movie.mp4', { source2: 'alt.ogg', poster: 'https://www.google.com/logos/google.jpg' });
            //   }
            },
            // templates: [
            //       { title: 'New Table', description: 'creates a new table', content: '<div class="mceTmpl"><table width="98%%"  border="0" cellspacing="0" cellpadding="0"><tr><th scope="col"> </th><th scope="col"> </th></tr><tr><td> </td><td> </td></tr></table></div>' },
            //   { title: 'Starting my story', description: 'A cure for writers block', content: 'Once upon a time...' },
            //   { title: 'New list with dates', description: 'New List with dates', content: '<div class="mceTmpl"><span class="cdate">cdate</span><br /><span class="mdate">mdate</span><h2>My List</h2><ul><li></li><li></li></ul></div>' }
            // ],
            template_cdate_format: '[Date Created (CDATE): %m/%d/%Y : %H:%M:%S]',
            template_mdate_format: '[Date Modified (MDATE): %m/%d/%Y : %H:%M:%S]',
            height: 600,
            image_caption: true,
            quickbars_selection_toolbar: 'bold italic | quicklink h2 h3 blockquote quickimage quicktable',
            noneditable_noneditable_class: "mceNonEditable",
            toolbar_mode: 'sliding',
            contextmenu: "link image imagetools table",
           });
        tinymce.init({
            selector: 'textarea#full-featured-non-premium2',
            plugins: 'print preview paste importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help charmap quickbars emoticons',
            imagetools_cors_hosts: ['picsum.photos'],
            menubar: 'file edit view insert format tools table help',
            toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor backcolor removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | insertfile image media template link anchor codesample | ltr rtl',
            toolbar_sticky: true,
            autosave_ask_before_unload: true,
            autosave_interval: "30s",
            autosave_prefix: "{path}{query}-{id}-",
            autosave_restore_when_empty: false,
            autosave_retention: "2m",
            image_advtab: true,
            // content_css: '//www.tiny.cloud/css/codepen.min.css',
            // link_list: [
            //   { title: 'My page 1', value: 'http://www.tinymce.com' },
            //   { title: 'My page 2', value: 'http://www.moxiecode.com' }
            // ],
            image_list: [
              { title: 'Logo', value: 'https://app.paintpad.com/paintpad/common/mail/views/logo.png' },
              // { title: 'My page 2', value: 'http://www.moxiecode.com' }
            ],
            image_class_list: [
              { title: 'None', value: '' },
              { title: 'Some class', value: 'class-name' }
            ],
            importcss_append: true,
            file_picker_callback: function (callback, value, meta) {
            //   /* Provide file and text for the link dialog */
            //   if (meta.filetype === 'file') {
            //     callback('https://www.google.com/logos/google.jpg', { text: 'My text' });
            //   }

            //   /* Provide image and alt text for the image dialog */
              if (meta.filetype === 'image') {
                callback('https://app.paintpad.com/paintpad/common/mail/views/logo.png', { alt: 'logo' });
              }

            //   /* Provide alternative source and posted for the media dialog */
            //   if (meta.filetype === 'media') {
            //     callback('movie.mp4', { source2: 'alt.ogg', poster: 'https://www.google.com/logos/google.jpg' });
            //   }
            },
            // templates: [
            //       { title: 'New Table', description: 'creates a new table', content: '<div class="mceTmpl"><table width="98%%"  border="0" cellspacing="0" cellpadding="0"><tr><th scope="col"> </th><th scope="col"> </th></tr><tr><td> </td><td> </td></tr></table></div>' },
            //   { title: 'Starting my story', description: 'A cure for writers block', content: 'Once upon a time...' },
            //   { title: 'New list with dates', description: 'New List with dates', content: '<div class="mceTmpl"><span class="cdate">cdate</span><br /><span class="mdate">mdate</span><h2>My List</h2><ul><li></li><li></li></ul></div>' }
            // ],
            template_cdate_format: '[Date Created (CDATE): %m/%d/%Y : %H:%M:%S]',
            template_mdate_format: '[Date Modified (MDATE): %m/%d/%Y : %H:%M:%S]',
            height: 600,
            image_caption: true,
            quickbars_selection_toolbar: 'bold italic | quicklink h2 h3 blockquote quickimage quicktable',
            noneditable_noneditable_class: "mceNonEditable",
            toolbar_mode: 'sliding',
            contextmenu: "link image imagetools table",
           });
        $(document).on('focusin', function(e) {
          if ($(e.target).closest(".tox-dialog").length) {
              e.stopImmediatePropagation();
          }
      });
      })
    </script>
    <script type="text/javascript">
      $('.tagsInsert').on('click',function(){
        var tag = $(this).data('value')
        console.log(tag)
        tinymce.get('full-featured-non-premium2').execCommand('mceInsertContent', false, tag);
        // tinymce.get('full-featured-non-premium').execCommand('mceInsertContent', false, tag);
      })
      $('.tagsInsert1').on('click',function(){
        var tag = $(this).data('value')
        console.log(tag)
        // tinymce.get('full-featured-non-premium2').execCommand('mceInsertContent', false, tag);
        tinymce.get('full-featured-non-premium').execCommand('mceInsertContent', false, tag);
      })
    </script>
    <script type="text/javascript">
      $(document).on('keyup','#tblemailtempsarch-temp_name',function(){
        $('#my-email-pjax').find('form').first().submit();
      });
      $('.open_addmodel').on('click',function(){
        $('#addEmail').modal('show')
        var id = $(this).parent().data('key')
        $('#temp_iddd').val(id)
        var dd = $(this).parent().data('email')
        // console.log(dd)
        var dd1 = $(this).parent().data('name')
        $('#template_name').val(dd1)
        // $('#full-featured-non-premium').text(dd)
        $('#exampleModalLabels').empty().html('Update Template')
        tinymce.get('full-featured-non-premium').setContent(dd);
        $('.descriptionU').val($(this).parent().data('desc'))
        $('.typeHomeU').val($(this).parent().data('home'))
      })
      $('.clone_template').on('click',function(){
        var dd1 = $(this).parent().parent().data('name')
        $('#update_email').modal('show')
        $('.template_name').val(dd1)
        var dd = $(this).parent().parent().data('email')
        $('#exampleModalLabel').empty().html('Clone Template')
        tinymce.get('full-featured-non-premium2').setContent(dd);
        $('.description').val($(this).parent().parent().data('desc'))
        $('.typeHome').val($(this).parent().parent().data('home'))
      })
      $('.Add_template').on('click',function(){
        $('#exampleModalLabel').empty().html('Add Template')
        $('#update_email').modal('show')
         $('.template_name').val('')
        tinymce.get('full-featured-non-premium2').setContent('');
        $('.description').val('')
        $('.typeHome').val(0)
      })
    </script>
    <script type="text/javascript">
      $(document).on('submit','#add_form',function(event){
         event.preventDefault();
         $('.Loaderoverlay').show()
         $.ajax({
             url:'<?php echo Url::base(); ?>/add_template.php',
             method:"POST",
             data:new FormData(this),
             dataType:'JSON',
             contentType: false,
             cache: false,
             processData: false,
             success:function(data)
             {
                 console.log(data)
                 $('.Loaderoverlay').hide()
                 location.reload()
                 if (data.success == '1') {
                  // $('#myPayment').modal('hide')
                 }
                 
             },
             complete:function(data){
              $('.Loaderoverlay').hide()
              location.reload()
             }
         })
     });
      $(document).on('submit','#update_form',function(event){
         event.preventDefault();
         $('.Loaderoverlay').show()
         $.ajax({
             url:'<?php echo Url::base(); ?>/add_template.php',
             method:"POST",
             data:new FormData(this),
             dataType:'JSON',
             contentType: false,
             cache: false,
             processData: false,
             success:function(data)
             {
                 console.log(data)
                 $('.Loaderoverlay').hide()
                 location.reload()
                 if (data.success == '1') {
                  // $('#myPayment').modal('hide')
                 }
                 
             },
             complete:function(data){
              $('.Loaderoverlay').hide()
              location.reload()
             }
         })
     });
      $(document).on('click','.delete_template',function(event){
         event.preventDefault();
         swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this template",
            // icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
              $('.Loaderoverlay').show()
              $.ajax({
                 url:'<?php echo Url::base(); ?>/add_template.php',
                 method:"POST",
                 data:{'temp_id':$(this).data('id'),'del':1},
                 dataType:'JSON',
                 success:function(data)
                 {
                     console.log(data)
                     $('.Loaderoverlay').hide()
                     location.reload()
                     if (data.success == '1') {
                      // $('#myPayment').modal('hide')
                     }
                     
                 },
                 complete:function(data){
                  $('.Loaderoverlay').hide()
                  location.reload()
                 }
             })
            } else {
              // swal("Your imaginary file is safe!");
            }
          });
         
         
     });
    </script>
    <script type="text/javascript">
    var searchActivity = URLToArray(window.location.search)

    $('#tblemailtempsarch-temp_name').focus().val("").val(searchActivity['TblemailTempSarch[temp_name]'])
    // $('#tblquotessearch-status').val(searchActivity['TblemailTempSarch[temp_name]'])
     // $('#tbluseractivityseach-name').focus().val("").val(searchActivity['TbluserActivitySeach[name]'])
    // $('#tbluseractivityseach-group').val(searchActivity['TbluserActivitySeach[group]'])
    function URLToArray(url) {
      var request = {};
      var pairs = url.substring(url.indexOf('?') + 1).split('&');
      for (var i = 0; i < pairs.length; i++) {
          if(!pairs[i])
              continue;
          var pair = pairs[i].split('=');
          request[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1]);
       }
       return request;
  }
</script>