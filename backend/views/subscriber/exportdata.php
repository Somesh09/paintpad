<?php
use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\bootstrap\ButtonDropdown;
use dosamigos\grid\GridView;
use dosamigos\exportable\ExportableButton; 
use kartik\export\ExportMenu;
?>

<style>
table, th, td {
  border-bottom: 1px solid black;
  border-collapse: collapse;
}
th, td {
  padding: 5px;
  text-align: left;
}
tr{
  background: #f7f4f4;
  margin-bottom: 5px;
}
table{
    margin: 0;
    width: 100%;
    margin-top: 20px;
}
</style>
<div id="spinner-border-overaly" >
  <div class="loader"></div>
</div>
<div class="tbl-brand-index appointmentpage" style="width: 100%;margin: auto;">
  <span style="font-size: 22px;">Export Paint Data</span>
  <!-- <h3>Appointment Types</h3> -->
  <!-- <form method="POST" id="uploadcsv" action="exportdatacsv"> -->
    <table>
      <tbody class="PaintDataBody">
        <tr>
          <td>
            Select Company
          </td>
          <td>
           <?php if (Yii::$app->user->identity->role == 20) { ?>
            
            <select name="company_id" class="form-control">
              <option value="0">Master</option>
              <?php 
              foreach ($data as $key => $company){
                echo "<option value='".$company['company_id']."'>".$company['name']."</option>";
              }
              ?>
            </select>
         
          <?php } else{ ?>
            <select name="company_id" class="form-control">
              <option value="0">Master</option>
              <option value="<?php echo Yii::$app->user->identity->company_id ; ?>">Self</option>
            </select>
             <!-- <input type="hidden" name="company_id" value="<?php //echo Yii::$app->user->identity->company_id ; ?>"> -->
          <?php } ?>
           </td>
          <td colspan="2"><span style="font-size: :20px" class="btn btn-large btn-primary getcsv">Get CSV</span></td>
        </tr>
      </tbody> 
    </table>
  <!-- </form> -->
</div>


<div class="tbl-brand-index appointmentpage" style="width: 100%;margin: auto;">
  <span style="font-size: 22px;">Import Paint Data</span>
  <!-- <h3>Appointment Types</h3> -->
  <form method="POST" id="uploadcsv" enctype="multipart/form-data">
    <table>
      <tbody class="PaintDataBody">
        <tr>
          <td>
            Select Company
          </td>
          
            <?php if (Yii::$app->user->identity->role == 20) { ?>
              <td>
              <select name="company" class="form-control">
                <option value="0">Master</option>
                <?php 
                foreach ($data as $key => $company){
                  echo "<option value='".$company['company_id']."'>".$company['name']."</option>";
                }
                ?>
              </select> 
            </td>
            <?php } else{ ?>
              <input type="hidden" name="company" value="<?php echo Yii::$app->user->identity->company_id ; ?>">
                <!-- <option value="<?php //echo Yii::$app->user->identity->company_id ; ?>">Master</option> -->
                
            <?php } ?>
            
         
          <td colspan="2">
            <input type="file" accept=".csv" name="file" id="choosefile" style="display: none;">
            <span style="font-size: :20px" class="btn btn-large btn-primary uploadcsv" onclick="$('#choosefile').trigger('click')">Select File</span>
          <span style="font-size: :20px" class="btn btn-large btn-primary" id="uploadStart">Upload</span></td></tr>
      </tbody> 
    </table>
  </form>

</div>

<script>
  $('.getcsv').unbind('click').click(function(){
    window.location.href = "exportdatacsv?id="+$('[name="company_id"]').val();
  })

  $('#uploadStart').unbind('click').click(function(){
    // alert('hello');
    console.log($("#choosefile").val());
    if($("#choosefile").val() == '' || $("#choosefile").val() == 'undefined'){
      alert('Choose a .csv file');
    }else{
      $('#uploadcsv').submit();
    }
  })

$(function () {
  $('#uploadcsv').on('submit', function (e) {
    e.preventDefault();
    $.ajax({
      type: 'POST',
      url: 'uploadcsv',
      data: new FormData(this),
      dataType: 'json',
      contentType: false,
      cache: false,
      processData:false,
      beforeSend: function(){
        $('#spinner-border-overaly').show();
      },
      success: function(response){ 
        $('#spinner-border-overaly').hide();
        if(response == "No file" || response == "Invalid File"){
          alert('Something went wrong!!');
        }else{
          alert('Data Uploaded Successfully');
        }
        $("#choosefile").val('');
      },
      error:function(response){
        $('#spinner-border-overaly').hide();
        console.log(response.responseText);
        alert('Something went Wrong.!!!');
        $("#choosefile").val('');
      }
    });
  });
});

  // File type validation
$("#choosefile").change(function() {
    var file = this.files[0];
    var fileType = file.type;
    var match = ['application/vnd.ms-excel', 'text/csv'];
    if(!((fileType == match[0]) || (fileType == match[1]))){
        alert('Only CSV allowed to upload.');
        $("#choosefile").val('');
        return false;
    }
});
</script>

  
  
  

