<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\widgets\Pjax;
use dosamigos\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TblSpecialItemsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


?>
<div class="tbl-special-items-index">


  
   <?php Pjax::begin(['timeout' => 5000,'id'=>'specialpjax']);?>
<?php  echo  Html::button('<img src="uploads/add.png"width="17" height="17"/>', ['value'=>Url::to( Url::base().'/createspecial'),'class'=>'btn btn-primary pull-right' ,'id'=>'modalButtonspecial']);?>
    
       <?php

       Modal::begin([
          'header'=>'',
          'id'=>'modalspecial',
          'size'=>'modal-lg',
        ]);
          echo "<div id='modalContentspecial'><div style='text-align:center'><img src='uploads/avatar2.gif' height='100' width='100'></div></div>";
          Modal::end();



       


      

     ?>
   
  <?php  echo $this->render('_searchspecial', ['model' => $searchModelspecial]); ?>
  <div class="main-table-index-page">
    <?= GridView::widget([
        'dataProvider' => $dataProviderspecial,
        //'filterModel' => $searchModelspecial,
        'rowOptions'   => function ($model, $key, $index, $grid) {
              return  [
                           "class"=>"modalButtonspecial2",'value'=>Url::to(Url::base().'/updatespecial?id='.$model->item_id)
                          ];
                },
        'behaviors' => [
            [
                'class' => '\dosamigos\grid\behaviors\LoadingBehavior',
                'type' => 'spinner'
            ]
        ],
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            //'item_id',
            'name',
            [
              'attribute'=>'price',
              'value'=>'price',
              'filter'=>false,


            ],
            //'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn',
               'header' => 'UPDATE',
               'headerOptions' => ['style' => 'color:#337ab7'],
               'template' => '{update}',
               'buttons' => [
            

                'update' => function ($url, $model,$id) {
                            return Html::a('<span class=""><img src="uploads/update.png" width="13" height="13"/></span>', $url, [
                           "class"=>"modalButtonspecialupdate",'value'=>Url::to(Url::base().'/updatespecial?id='.$model->item_id)
                 ]);
             },
            

        ],
            'urlCreator' => function ($action, $model, $key, $index) {
            

             Url::to(['special?id='.$model->item_id],['id'=>'modal'])   ;         
                //$url ='update?id='.$model->brand_id;
              //return $url;
                 
                 //return Html::a($url, ['title' => 'view']);
                
                
            
            

               }




           ],




        ],
    ]); ?>
  </div>
    <div class="specialPageDisplay page-display-showing-foot"></div>
</div>

<script>
  $(function()
{
  //
  $('#modalButtonspecial').click(function(){
    $('#modalspecial').modal('show')
      .find('#modalContentspecial')
      .load ($(this).attr('value'), function(){
        $('#modalspecial .modal-header').remove();
        // $('#modalspecial .modal-header').empty();
      });
  });
});


  $(function()
{
  //
  $('.modalButtonspecialupdate').click(function(){
    $('#modalspecial').modal('show')
      .find('#modalContentspecial')
      .load ($(this).attr('value'));
  });
});

   $(function()
{
  //
  $('.modalButtonspecial2').click(function(){
    $('#modalspecial').modal('show')
      .find('#modalContentspecial')
      .load ($(this).attr('value'),function(){

        var spllitm = $('#splitmname').val();
        $('#modalspecial .modal-header').empty();
        

      });
  });
});
  
$(function() { //equivalent to document.ready
  $(".form-control").focus();
});


$('.form-control').focus(function(){
  var that = this;
  setTimeout(function(){ that.selectionStart = that.selectionEnd = 10000; }, 0);
});

$('#tblspecialitemssearch-name').bind('keyup', function() { 
    $('#tblspecialitemssearch-name').delay(200).submit();
});


  </script>
  <?php $this->registerJs('jQuery("#w7").on("keyup", "input", function(){
                jQuery(this).change();
                 $("#modalspecial").remove();
                //OR $.pjax.reload({container:\'#w7\'});
        });',
        yii\web\View::POS_READY);
        ?> 
<script>
  var name = $('.summary').html();
  $('.summary').remove();
  $('.specialPageDisplay').html(name);
  if($('.specialPageDisplay').text()=='undefined')
  {
     $('.specialPageDisplay').hide();
  }
</script>
<?php Pjax::end();?>


