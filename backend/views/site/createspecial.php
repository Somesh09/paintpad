<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TblSpecialItems */

//$this->title = 'Create Tbl Special Items';
$this->params['breadcrumbs'][] = ['label' => 'Tbl Special Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-special-items-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_formspecial', [
        'model' => $model,
    ]) ?>

</div>
