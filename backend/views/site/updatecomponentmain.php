<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TblComponents */

//$this->title = 'Update Tbl Components: ' . $model->name;
?>

<input type="hidden" id="typeename" value="<?php echo $model->name;?>">

<div class="tbl-components-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_formupdatecomponentmain', [
        'model' => $model,
        'model2'=>$model2,
        //'model3'=>$model3,
        'id'=>$id,
        'model3'=>$model3,'model4'=>$model4,'model5'=>$model5
        
    ]) ?>

</div>
