<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TblColorTags */

//$this->title = 'Create Tbl Color Tags';
$this->params['breadcrumbs'][] = ['label' => 'Tbl Color Tags', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-color-tags-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_formcolor', [
        'model' => $model,
    ]) ?>

</div>
