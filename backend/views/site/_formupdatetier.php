<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\models\TblBrands;

/* @var $this yii\web\View */
/* @var $model app\models\TblColorTags */
/* @var $form yii\widgets\ActiveForm */

?>
<style>

.right{
	float: right;
	width:620px;
}
.table-left-aside .left {
    float: left;
    border-style: solid;
    border-width: 1px;
    width: 200px;
    max-height: 700px;
    overflow-y: auto;
    overflow-x: hidden;
    height: 100%;
}
.add_inner_img {
    float: left;
    width: 100%;
    height: 100%;
    overflow: hidden;
    position: relative;
}
button#save-tier-image {
    margin-top: 10px;
}
.activeTier
{
  background-color: #66ccff;
  color: white;
}
#modaltier .modal-content {
    padding: 15px;
}


</style>

<div class="right" id="tier">
		<div class="tbl-color-tags-form form-inner-head">


		    <div id="maintier" style="width: 100%;height: 500px;">
				<?php $form = ActiveForm::begin(['validationUrl' => Url::toRoute(['updatetier?id='.$id]),'enableAjaxValidation' => true, 'id' => 'mytierid']); ?>
				<div class = "modal-body-div-add">
					<h2><?php echo $model->name;?></h2>
							<?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
					<button type="button" class="close glyphicon glyphicon-remove" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
					<?= $form->field($model, 'name')->textInput(['maxlength' => true,'label'=>false]) ?>


					<?= $form->field($model,'brand_id')->dropDownList(ArrayHelper::map(TblBrands::find()->all(),'brand_id','name'),['prompt'=>'Select brand']) ?>


					<?= $form->field($model, 'type_id')->radioList(array('1'=>'interior','2'=>'exterior','3'=>'both')); ?>
					<?= $form->field($model, 'enabled')->radioList(array('1'=>'YES','0'=>'NO')); ?>
				<?php ActiveForm::end(); ?>		
			</div>
		</div>
</div>





<div class="table-left-aside">

	

	<div class="left">
		<h5  style="text-align:center;width:200px;background-color:#e0e4e5;height:35px;"><b>TIER</b></h5>
		<table class="table">
			

			<?php
			// echo "<pre>";print_r($model1);die;
				$i=1;
				foreach($model1 as $model1)
				{
					?>
					<tr>
					<td>
					<?php
					echo "<a href='JavaScript:void(0);' color='blue' class='gettiercoat' tid='".$model->tier_id."' cid='".$model1->group_id."' id='compname".$model1->group_id."'>".$model1->name."</a></td></tr>";
				}
			?>
		</table>
			<?php $form = ActiveForm::begin(['action' => ['/updatetierimage?tier_id='.$model->tier_id],'id' => 'tierImageUpdate']); ?>
		      <div class="imgg_div">

				<span class="opnImg"><img id="blah21" src="uploads/edit.png" alt="your image" /></span>

				<?php

				//echo '<pre>'; print_r($model); exit;

				$imgg = $model->image;

				if( $model->image == 'null' || $model->image == null ){
				?>
					<img id="blah2" src="uploads/no_image.png" alt="your image" />
				<?php
				}else{
				?>
					<img id="blah2" src="uploads/<?php echo $model->image?>" alt="your image" />
				<?php
				}
				?>

				<?php
				/*if( ($model->image) != 'null' || ($model->image) != null)
				{
					echo 'line44';
				?>
					<img id="blah3" src="uploads/<?php echo $model->image?>" alt="your image" />
				<?php
				}else{
					echo 'line49';
				?>
					<img id="blah2" src="uploads/no_image.png" alt="your image" />
				<?php
				}*/
				?>        
				<?= $form->field($model,'image',['inputOptions'=>['name' => 'image','id'=>'imgInp']])->fileinput()->label(false);?>
			<p class="img-size"> Select Image max 2Mb</p>
			</div>
			<?= Html::submitButton('Save Image', ['class' => 'btn btn-success','id' => 'save-tier-image','style'=>'display:none;']) ?>
		<?php ActiveForm::end(); ?>
	</div>
</div>

  <!-- div style="display: none;">
	<div id="tiercoat" class="container">
		
		
		<div>
			<table class="table">
				<tr>
					<th>DEFAULT</th>
					<th>TOP COAT PRODUCT</th>
				</tr>
				<tr>
					<td></td>
				</tr>
			</table>

		</div>
	</div>
</div>

 -->
<script src="/app/admin/js/compressor.min.js"></script>

<script>

$('body').on('beforeSubmit', '#mytierid', function (e) {
  var pathname = window.location.href;  
     var form = $(this);
     
     $.ajax({
          url: form.attr('action'),
          type: 'post',
          data: form.serialize(),
          success: function (response) {
              

               console.log(response);              

               if(response == 1){
                 $("#modaltier").modal('hide');
                 $(document.body).removeClass('modal-open');
				 $('.modal-backdrop').remove();
                 $.pjax.reload('#tierpjax' , {timeout : false});
               }
          },
         
        }
     );
      e.stopImmediatePropagation();
     return false;
});
</script>
<script>
var CompImage = ''
$(document).on('click', '.gettiercoat', function(){  
	$('.left').find('.activeTier').removeClass('activeTier');
	$(this).closest('tr').addClass('activeTier');
	var pathname = window.location.href;    
	console.log(pathname);
	var base_url = pathname + 'data';
	console.log(base_url);

	var cid = $(this).attr('cid');
	var tid = $(this).attr('tid');
	console.log(cid);
	console.log(tid);

	var datastring = {cid:cid,tid:tid};
	console.log(datastring);

	$.ajax({
		url: "data",
		type: 'post',
		data: datastring,
		success: function (response) {
			// var data = $.parseJSON(response)
			// do something with response
			console.log(response);       

			$('#maintier').empty();
			$('#maintier').html(response); 
			//$('#maintier').html(data.profiledata);
		},

	});

});

$(document).on('submit', '#tierImageUpdate', function (e) {
		e.preventDefault();
		$('.tierLoaderoverlay').show();
        var formAction = $(this).attr("action");
        var pathname = window.location.href;
        var formData = new FormData($('#tierImageUpdate')[0]);
		formData.delete('image');
        formData.append('image', CompImage, CompImage.name);
        var form = $(this);
    
		$.ajax({
			url: form.attr('action'),
			type: 'POST',
			data: formData,
			processData: false,
			contentType: false,
			cache:false,
			success: function (response) {
				$('.tierLoaderoverlay').hide();
				if(response == 1){
					$("#save-tier-image").hide();
					alert("Image Updated!");

				}
				else{
					alert("Image not saved!");
				}
			},
			error: function (error) {
				$('.tierLoaderoverlay').hide();
				alert("Unable to upload Image!");
			}
		});
		e.stopImmediatePropagation();  
		return false;
});

function readURL(input) {

	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function(e) {

			$("img").show();
			$('#blah2').attr('src', e.target.result);
			//$("#blah3").hide();
			$("#blah").show();
			$("#save-tier-image").show();
		}
		reader.readAsDataURL(input.files[0]);
		const file = input.files[0];
		// alert(file.size);
		new Compressor(file, {
			quality: 0.6,
			success(result) {
			const formData = new FormData();
			// The third parameter is required for server
			// formData.append('file', result, result.name);
			CompImage = result
			// console.log(CompImage)
			},
			error(err) {
			console.log(err.message);
			},
		});
		//myy end
	}

}

$("#imgInp").change(function() {
	var maxSizeKB = 2000; //Size in KB
	var maxSize = maxSizeKB * 1024; //File size is returned in Bytes
	if (this.files[0].size > maxSize) {
		$(this).val("");
		alert("Please select Image of 2mb or less");
		return false;
	}
  readURL(this);
});

</script>
















