<?php
use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\bootstrap\ButtonDropdown;
use dosamigos\grid\GridView;
use dosamigos\exportable\ExportableButton; 
use kartik\export\ExportMenu;
?>

<style>
/*#modal {
    top: 50%;
    transform: translate(-50%, -50%);
    left: 50%;
    overflow: unset !important;
    height: 350px !important;
}*/


.modal.fade .modal-dialog {
	-webkit-transition: -webkit-transform .3s ease-out;
	-o-transition: -o-transform .3s ease-out;
	transition: transform .3s ease-out;
	-webkit-transform: translate(-50%, -50%) !important; 
	-ms-transform: translate(-50%, -50%) !important; 
	-o-transform: translate(-50%, -50%) !important; 
}
.modal-dialog.modal-sl, .modal-dialog.modal-lg {
	transform: translate(-50%, -50%) !important;
	position: absolute !important;
	top: 50% !important;
	left: 50% !important;
} 
#modalcomponentmain .modal-dialog {
	width: 900px;
	max-height: 100%;
	/*overflow-y: scroll;*/
}
/*****/



.fade.modal.in .modal-header, .fade.modal .modal-header {
	padding: 15px;
	border-bottom: 0px solid #e5e5e5;
}
.fade.modal.in .form-inner-head h2, .fade.modal .form-inner-head h2 {
    width: auto;
    float: left;
    margin: 0;
    font-size: 18px;
    color: #383838;
    line-height: 20px;
}
.fade.modal.in .form-inner-head .form-group, .fade.modal .form-inner-head .form-group {
    width: 100%;
    float: left;
    margin: 5px 0 5px 0;
    padding: 5px 15px;
}



.fade.modal.in .form-inner-head .form-control, .fade.modal .form-inner-head .form-control {
	margin: 0 !important;
}
.fade.modal.in .modal-header, .fade.modal .modal-header {
	padding: 15px 20px 0px 0 !important;
	border-bottom: 0px solid #e5e5e5 !important;
}
.modal-body {
	position: relative;
	padding: 0px 0px !important;
	float: left;
	width: 100%;
}
.modal-body-div-add {
    float: left;
    width: 100%;
    padding: 12px 40px 3px 15px;
    margin-bottom: 0px;
    border-bottom: 1px solid #ccc;
    position: relative;
}
button.close {
    -webkit-appearance: none;
    padding: 0px 0 0 0 !important;
    cursor: pointer;
    background: transparent;
    border: 0;
    font-size: 16px;
    position: relative;
    top: 0px;
    line-height: normal;
    left: 3px;
    font-weight: normal;
    opacity: 0.4;
}
/* #modalSort .modal-dialog.modal-sl {
	max-height: 80%;
} */
#modalSort .modal-dialog.modal-sl {
    max-height: 100%;
}
/* #modalSort .model-inner-part {
    width: 570px;
    overflow-y: auto;
    overflow-x: hidden;
    margin: 0 auto;
    max-height: 600px;
    padding: 12px 0;
} */
#modalSort .model-inner-part {
    width: 570px;
    overflow-y: auto;
    overflow-x: hidden;
    margin: 12px auto;
    max-height: 800px;
    padding: 12px 0 0 0;
}

.fade.modal.in .form-inner-head .btn.btn-success, .fade.modal .form-inner-head .btn.btn-success {
    float: right !important;
    width: auto;
    filter: unset !important;
    background: #00aef0;
    border-color: #00aef0;
    font-size: 15px !important;
    letter-spacing: 1px;
    position: relative !important;
    top: -5px !important;
    padding: 4px 10px !important;
}
.fade.modal.in .form-inner-head .stoke-main a.clicked.btn.btn-success.pull-right {
    top: auto !important;
}

#modal2.fade.modal.in .form-inner-head h2, #modal2.fade.modal .form-inner-head h2 {
	width: auto;
	float: left;
	margin: 0;
	font-size: 18px;
}

.stoke-main {
    float: left;
    padding: 7px 15px 7px;
    width: 100%;
    background: #f5f5f5;
}

.stoke-main h3 {
    padding: 0;
    text-align: left;
    margin: 0;
    width: auto;
    float: left;
    font-size: 18px;
    line-height: 30px;
}
	
.stoke-main .clicked.btn.btn-success.pull-right {
	margin: 0;
}
.stoke-main-table .table {
	margin-bottom: 0;
}
div#choosingdiv label.control-label {
	font-size: 18px;
	font-weight: normal;
	padding: 0px 15px 0 15px;
	text-align: left;
	margin: 0;
	width: 755px;
	float: left;
}
div#choosingdiv select {
    width: 234px;
    margin: 0px 15px 0 15px !important;
}
.table {
	width: 100%;
	max-width: 100%;
	margin-bottom: 0 !important;
}

.table > tbody > tr > td.deleteform {
	padding-right: 15px;
	font-size: 18px;
  vertical-align: middle;
	padding-left: 0;
}


</style>
<div class="tbl-brand-index">
  
  <?php Pjax::begin(['timeout' => 5000,'id'=>'my-grid-pjax']);?>   
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="jquery.js"></script>
  <?php  echo Html::button('<img src="uploads/add.png"width="17" height="17"/>', ['value'=>Url::to( Url::base().'/create'), 'class' => 'btn btn-primary pull-right','id'=>'modalButton']);?>
  <?php  echo Html::button('Sort Brand', ['value'=>Url::to( Url::base().'/sortbrand'), 'class' => 'btn btn-primary pull-right','id'=>'modalButtonsort']);?>
  <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
  <?php
    Modal::begin([
      'header'=>'',
      'id'=>'modal',
	  'class'=>'main-popup-all',
      'size'=>'modal-sl',
    ]);
    echo "<div id='modalContent2' class='clearfix'>
          <div style='text-align:center'>
          <img src='uploads/avatar2.gif' height='100' width='100'>
          </div></div>";
    Modal::end();
  ?>
  <?php
    Modal::begin([
     // 'header'=>'',
      'id'=>'modalSort',
	  
      'size'=>'modal-sl',
    ]);
    echo "<div id='modalContentSort' class='clearfix'>
          <div style='text-align:center'>
          <img src='uploads/avatar2.gif' height='100' width='100'>
          </div></div>";
    Modal::end();
  ?>
  <?php  //echo Html::button('export brand', ['value'=>Url::to( 'export.php'), 'class' => 'btn btn-primary pull-right','id'=>'exportbrand']);?>       
  <div id="brandsearch" class="main-table-index-page">
    <?=  GridView::widget([
            'dataProvider' => $dataProvider,
            //'filterModel' => $searchModel,
            'filterSelector' => 'select[name="per-page"]',
            //'tableOptions' => ['class' => 'brandsGridView'],
            'rowOptions'   => function ($model, $key, $index, $grid) {
              return  [
              
              "class"=>($model->enabled != 1)?"modalButton44 disabledTr":"modalButton44",
              'value'=>Url::to(Url::base().'/update?id='.$model->brand_id),
              "style"=>($model->enabled != 1)?"background-color:#f9f9f9;":""
              ];
            },
            'behaviors' => [
              [
              'class' => '\dosamigos\grid\behaviors\LoadingBehavior',
              'type' => 'spinner'
              ]
            ],
            'columns' => [                              
              [
                'attribute' => 'name',
                'label' => 'Name',
                'format' => 'raw',
                'value' => function ($model) {    
                  if($model->enabled != 1){
                    return '<strike>'.$model->name.'</strike>';
                  }else{
                    return $model->name;
                  }
                },
              ],
              ['class' => 'yii\grid\ActionColumn',
                'header' => 'UPDATE',
                'headerOptions' => ['style' => 'color:#00aef0'],
                'template' => '{update}',
                'buttons' => [
                  'update' => function ($url, $model,$id) {
                  return Html::a('<span class=""><img src="uploads/editt.png" width="13" height="13"/></span>', $url, [
                  "class"=>"modalButton4",'value'=>Url::to(Url::base().'/update?id='.$model->brand_id)
                  ]);
                  },
                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                   Url::to(['update?id='.$model->brand_id],['id'=>'modal'])   ;         
                }
              ],
            ],
    ]); 
    ?>
  </div>
  <div class="pageNoDisplay page-display-showing-foot"></div>
  <div class="list-div">
  <?php echo \nterms\pagesize\PageSize::widget(['defaultPageSize' =>50, 'sizes' => [ 10 => 10, 15 => 15, 20 => 20, 25 => 25,30 => 30,50 => 50], 'label' => 'List']); ?>
</div>
</div>

<script type="text/javascript">
  $(function()
{
  //
  $('#modalButton').click(function(){
   
    $('#modal').modal('show')
      .find('#modalContent2')
      .load ($(this).attr('value'), function(){
        // $('#modal .modal-header').remove();
      });
  });


  $('.modalButton4').click(function(){

    $('#modal').modal('show')
      .find('#modalContent2')
      .load ($(this).attr('value'));
  });


 $('.modalButton44').click(function(){

    $('#modal').modal('show')
      .find('#modalContent2')
      .load ($(this).attr('value'),function(){

        var brndd = $('#brdname').val();
        // $('#modal .modal-header').remove();

      });
  });

  $('#modalButtonsort').click(function(){
         $('#modalSort').modal('show')
         .find('#modalContentSort')
         .load ($(this).attr('value'),function(){
            // $('#modal .modal-header').remove();
          });
         });
});
//  $('.modal-header').remove();

$(function() { //equivalent to document.ready
  $(".form-control").focus();
});


$('.form-control').focus(function(e){
  var that = this;
  setTimeout(function(){ that.selectionStart = that.selectionEnd = 10000; }, 0);
   e.stopImmediatePropagation();
});

 </script>

<?php $this->registerJs('jQuery("#w0").on("keyup", "input", function(e){
                jQuery(this).change();
                $("#modal").remove();
                 e.stopImmediatePropagation();
                //OR $.pjax.reload({container:\'#w0\'});
        });',
        yii\web\View::POS_READY);
        ?>
               <script>

$('#tblbrandssearch-name').bind('keyup', function() { 
    $('#tblbrandssearch-name').delay(200).submit();
});
$('#tblbrandssearch-enabled').bind('change', function() { 
    $('#tblbrandssearch-enabled').delay(200).submit();
});

 $(function()
 {
            $('#exportbrand').click(function(){
           window.location.href = 'export.php'; 
       });

 });


</script>
<script>
  var name = $('.summary').html();
$('.summary').remove();
$('.pageNoDisplay').html(name);
if($('.pageNoDisplay').text()=='undefined')
  {
     $('.pageNoDisplay').hide();
  }
</script>
<?php Pjax::end();?>
   


  
  
  

