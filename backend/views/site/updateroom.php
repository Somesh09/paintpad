<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Tblroomtypes */

//$this->title = 'Update Tblroomtypes: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Tblroomtypes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->room_id]];
$this->params['breadcrumbs'][] = 'Update';
?>

<input type="hidden" id="rmmname" value="<?php echo $model->name;?>">
<div class="tblroomtypes-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_formroomupdate', [
        'model' => $model,
        'id'=>$id
    ]) ?>

</div>
