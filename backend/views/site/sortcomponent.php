<?php
use yii\widgets\Pjax;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
?>
<!DOCTYPE html>
<html>
<head>
  
   
    
   
</head>

<body>

<div class="tbl-brands-form form-inner-head">
    <div class="modal-body-div-add sortBrandInner">
    <div style="width: 790px;float: left; ">
    <h2 class="">Sort Component</h2>
    <span align="left" style="color:#000">Hold and drag rows to sort the Component</span>
    </div>
    <?= Html::submitButton('Save', ['class' => 'btn btn-success sortComponent','id'=>'blah22']) ?>
    <button type="button" class="close glyphicon glyphicon-remove" data-dismiss="modal" aria-hidden="true">×</button>
    </div>
    <div style="cursor: pointer;width: 870px;overflow-y: auto;overflow-x: hidden;margin: 12px auto;max-height: 800px;padding: 12px 0 0 0;" class="model-inner-part" >
        
        <table class="table table-hover table-condensed table-striped table-bordered">

            <tbody class="row_position2">
            <?php
                foreach($model as $model){
            ?>
                <tr  id="<?php echo $model->comp_id ?>">
                    <td><?php echo $model->name ?></td>
                </tr>
            <?php 
                } 
            ?>
            </tbody>

        </table>
    </div> <!-- container / end -->
    </div>

</body>


<script type="text/javascript">
    $( ".row_position2" ).sortable({
        delay: 150,
        stop: function() {
            var selectedData = new Array();
            $('.row_position2>tr').each(function() {
                selectedData.push($(this).attr("id"));
            });
            updateOrder(selectedData);
        }
    });


    function updateOrder(data) {
        $.ajax({
            url:"ajaxpro3.php",
            type:'post',
            data:{position:data},
            success:function(){
              $("body").removeClass('modal-open').removeAttr("style");
            }
        })
    }

    
$(document).on('click', '.sortComponent', function(){ 
              $("#modalcomponentmain").modal('hide');
              $(".modal-backdrop").remove();
              $.pjax.reload('#componentmainpjax' , {timeout : false});
                });
    

</script>
</html>
