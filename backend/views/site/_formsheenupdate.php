<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\TblSheen */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
h1{
  display:none;
}
</style>

<div class="form-inner-head">

    <?php $form = ActiveForm::begin(['validationUrl' => Url::toRoute(['validatesheenupdate?id='.$id]),'enableAjaxValidation' => true, 'id' => 'myid4']); ?>

     <div class = "modal-body-div-add">
     <h2><?= $model->name;?></h2>
         <?= Html::submitButton('Save', ['class' => 'btn btn-success','id'=>'closesheen']) ?>
      <button type="button" class="close glyphicon glyphicon-remove" data-dismiss="modal" aria-hidden="true">×</button>
    </div>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>


    
    <div class="form-group">
    
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script>
$('body').on('beforeSubmit', '#myid4', function (e) {
  var pathname = window.location.href;  
     var form = $(this);
     $.ajax({
          url: form.attr('action'),
          type: 'post',
          data: form.serialize(),
          success: function (response) {

               console.log(response);              

               if(response == 1){
                 $("#modalsheen").modal('hide');
                 $(document.body).removeClass('modal-open');
         $('.modal-backdrop').remove();
                 $.pjax.reload('#sheenpjax' , {timeout : false});

               }
          },
        
     });
      e.stopImmediatePropagation();
     return false;
});








</script>

