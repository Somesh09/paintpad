<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Tbl_rooms_typesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-special-items-search tbl-search-input-all">

    
     <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
        'data-pjax' => 1
        ],
    ]);


     ?>
  <div style="width:23%; margin-right:0px; border:1px;">
    <?= $form->field($model, 'name')->textInput(['placeholder' => "Search Special Item"])->label(false); ?>

   </div>


   

    <?php ActiveForm::end(); ?>

</div>
