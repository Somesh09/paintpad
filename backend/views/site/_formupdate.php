<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
?>

<div class="tbl-brands-form form-inner-head">

    <?php $form = ActiveForm::begin(['validationUrl' => Url::toRoute(['validatebrandupdate?id='.$id]),
    'enableAjaxValidation' => true, 
    'id' => 'myidbrand']); ?>
	<div class="modal-body-div-add">
	  <h2 > <?php echo $model->name;?></h2>
	  <?= Html::submitButton('Save', ['class' => 'btn btn-success','id'=>'closebrandupdate']) ?>
	  <button type="button" class="close glyphicon glyphicon-remove" data-dismiss="modal" aria-hidden="true"></button>
	  </div>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'enabled')->radioList(array('0'=>'YES','1'=>'NO')); ?>

    <?php ActiveForm::end(); ?>
</div>
<script>
$('body').on('beforeSubmit', '#myidbrand', function (e) {
  var pathname = window.location.href;
  var form = $(this);
  var request =$.ajax({
    url: form.attr('action'),
    type: 'post',
    data: form.serialize(),

    success: function (response) {
      if(response == 1){
        $("#modal").modal('hide');
        $(document.body).removeClass('modal-open');
         $('.modal-backdrop').remove();
        $.pjax.reload('#my-grid-pjax' , {timeout : false});
      }
    },
  });
  e.stopImmediatePropagation();
  return false;
});
</script>
