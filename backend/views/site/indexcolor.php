<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\widgets\Pjax;
use dosamigos\grid\GridView;



/* @var $this yii\web\View */
/* @var $searchModel app\models\TblColorTagsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


?>
<div class="tbl-color-tags-index">


    <?php Pjax::begin(['timeout' => 5000,'id'=>'colorpjax']

); ?>
<?php 
        echo  Html::button('<img src="uploads/add.png"width="17" height="17"/>', ['value'=>Url::to( Url::base().'/createcolor'),'class'=>'btn btn-primary pull-right' ,'id'=>'modalButton8']);
;?>

       <?php

       Modal::begin([
          'header'=>'',
          'id'=>'modalcolor',
          'size'=>'modal-lg',
        ]);
          echo "<div id='modalContentcolor'><div style='text-align:center'><img src='uploads/avatar2.gif' height='100' width='100'></div></div>";
          Modal::end();
     ?>
 
<?php  echo $this->render('_searchcolor', ['model' => $searchModelcolor]); ?>
<div class="main-table-index-page">
    <?= GridView::widget([
        'dataProvider' => $dataProvidercolor,
        //'filterModel' => $searchModelcolor,
        'rowOptions'   => function ($model, $key, $index, $grid) {
              return  [
                           "class"=>"modalcolor4",'value'=>Url::to(Url::base().'/updatecolor?id='.$model->tag_id)
                          ];
                },
        'behaviors' => [
            [
                'class' => '\dosamigos\grid\behaviors\LoadingBehavior',
                'type' => 'spinner'
            ]
        ],
        'columns' => [
           // ['class' => 'yii\grid\SerialColumn'],

            //'tag_id',
            'name',
           // 'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn',
               'header' => 'UPDATE',
               'headerOptions' => ['style' => 'color:#337ab7'],
               'template' => '{update}',
               'buttons' => [
            

                'update' => function ($url, $model,$id) {
                            return Html::a('<span class=""><img src="uploads/update.png" width="13" height="13"/></span>', $url, [
                           "class"=>"modalButton9",'value'=>Url::to(Url::base().'/updatecolor?id='.$model->tag_id)
                 ]);
             },            

        ],
            'urlCreator' => function ($action, $model, $key, $index) {
            

             Url::to(['update?id='.$model->tag_id],['id'=>'modal'])   ;         
                //$url ='update?id='.$model->brand_id;
              //return $url;
                 
                 //return Html::a($url, ['title' => 'view']);
                
                
               }

           ],

        ],
    ]); ?>
  </div>
    <div class="ColorPAGEDisplay"></div>
</div>

<script>

$(function()
{
  //
  $('#modalButton8').click(function(){
    $('#modalcolor').modal('show')
      .find('#modalContentcolor')
      .load ($(this).attr('value'), function(){
        // $('#modalcolor .modal-header').remove();
      });
  });
});


$(function()
{
  //
  $('.modalButton9').click(function(){
    $('#modalcolor').modal('show')
      .find('#modalContentcolor')
      .load ($(this).attr('value'));
  });
});


$(function()
{
  //
  $('.modalcolor4').click(function(){
    $('#modalcolor').modal('show')
      .find('#modalContentcolor')
      .load ($(this).attr('value'),function(){

        var clrr = $('#clrname').val();
        // $('#modalcolor .modal-header').remove();

      });
  });
});

$('#tblcolortagssearch-name').bind('keyup', function() { 
    $('#tblcolortagssearch-name').delay(200).submit();
});
  

$(function() { //equivalent to document.ready
  $(".form-control").focus();
});


$('.form-control').focus(function(){
  var that = this;
  setTimeout(function(){ that.selectionStart = that.selectionEnd = 10000; }, 0);
});



</script>
<?php $this->registerJs('jQuery("#w6").on("keyup", "input", function(){
              jQuery(this).change();
               $("#modalcolor").remove();
              //OR $.pjax.reload({container:\'#w6\'});
      });',
      yii\web\View::POS_READY);
      ?> 
<script>
var name = $('.summary').html();
$('.summary').remove();
$('.ColorPAGEDisplay').html(name);
if($('.ColorPAGEDisplay').text()=='undefined')
{
   $('.ColorPAGEDisplay').hide();
}
</script>
<?php Pjax::end();?>
