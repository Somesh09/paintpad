<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TblStrengths */

//$this->title = 'Update Tbl Strengths: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Tbl Strengths', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->strength_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<input type="hidden" id="strrname" value="<?php echo $model->name;?>">
<div class="tbl-strengths-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_formstrengthupdate', [
        'model' => $model,
        'id'=>$id,
    ]) ?>

</div>
