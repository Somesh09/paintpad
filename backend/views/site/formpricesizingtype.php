<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\TblComponentGroups;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
USE app\models\TblComponentCalculateMethod;
use app\models\TblComponentPricingMethods;
use wbraganca\dynamicform\DynamicFormWidget;


/* @var $this yii\web\View */
/* @var $model app\models\TblComponents */
/* @var $form yii\widgets\ActiveForm */
?>
    <?php $form = ActiveForm::begin(['id' => 'myid14main']); ?>

<div class="panel panel-default">
        <div class="panel-body">
             <?php DynamicFormWidget::begin([
                'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                'widgetBody' => '.container-items', // required: css class selector
                'widgetItem' => '.item', // required: css class
                'limit' => 4, // the maximum times, an element can be cloned (default 999)
                'min' => 1, // 0 or 1 (default 1)
                'insertButton' => '.add-item', // css class
                'deleteButton' => '.remove-item', // css class
                'model' => $model2[1],
                'formId' => 'myid14main',
                'formFields' => [
                    'name',
                    'work_rate',
                    'spread_ratio',
                    'is_default',
                    
                ],
            ]); ?>
                     <div class="pull-right">
                            <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                           
                        </div>
                         <div class="panel-heading">
                        <h3 class="panel-title pull-left">Price Sizing Method</h3>
                       
                        <div class="clearfix"></div>
                    </div>
            <div class="container-items"><!-- widgetContainer -->
            <?php foreach ($model2 as $i => $model2): ?>
                <div class="item panel panel-default"><!-- widgetBody -->
                    <div class="pull-right">
                            
                            <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                        </div>
                    <div class="panel-body">
                        <?php
                            // necessary for update action.
                            if (! $model2->isNewRecord) {
                                echo Html::activeHiddenInput($model2, "[{$i}]id");
                            }
                        ?>
                         <div class="row">
                         <div class="col-sm-3">
                        <?= $form->field($model2, "[{$i}]name")->textInput(['maxlength' => true]) ?></div>
                       
                            <div class="col-sm-3">
                                <?= $form->field($model2, "[{$i}]work_rate")->textInput(['maxlength' => true]) ?>
                            </div>
                            <div class="col-sm-3">
                                <?= $form->field($model2, "[{$i}]spread_ratio")->textInput(['maxlength' => true]) ?>
                            </div>
                        <!-- .row -->
                        
                            <div class="col-sm-3">
                              <?= $form->field($model2, 'is_default')->checkbox(array('label'=>''))
                                ->label('Is Default'); ?>
                           </div></div>
                            
                           
                     </div>
                    </div>
                
            <?php endforeach; ?>
            </div>
            <?php DynamicFormWidget::end(); ?>
        </div>


     </div>
     <?php ActiveForm::end(); ?>