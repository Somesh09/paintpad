<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\widgets\Pjax;
use dosamigos\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TblComponentGroupsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


?>
<style type="text/css">
.titlee
{
  font-size: 21px;
}
.svecompgrp {
  float: right;
}
.svecompgrp img {
  width: 21px;
  cursor: pointer;
}
/*#myid5 .savee {
  display: none;
}*/
</style>

<div class="tbl-component-groups-index">


    <?php Pjax::begin([ 'timeout' => 5000,'id'=>'componentpjax']);?>


  <?php   echo  Html::button('<img src="uploads/add.png"width="17" height="17"/>', ['value'=>Url::to( Url::base().'/createcomponent'),'class'=>'btn btn-primary pull-right' ,'id'=>'createcomponent']);;?>
       <?php

     Modal::begin([
        'header'=>'',
        'id'=>'modalcomponent11',
        'size'=>'modal-sl',
      ]);
        echo "<div id='modalContent6'><div style='text-align:center'><img src='uploads/avatar2.gif' height='100' width='100'></div></div>";
        Modal::end();

   ?>
   
  
<?php  echo $this->render('_searchcomponent', ['model' => $searchModelcomponent]); ?>
<div class="main-table-index-page">
    <?= GridView::widget([
        'dataProvider' => $dataProvidercomponent,
        //'filterModel' => $searchModelcomponent,
        'rowOptions'   => function ($model, $key, $index, $grid) {
              return  [
                           "class"=>($model->enabled != 1)?"modalButton000007 disabledTr":"modalButton000007",
                           'value'=>Url::to(Url::base().'/updatecomponent?id='.$model->group_id),
                           "style"=>($model->enabled != 1)?"background-color:#f9f9f9;":""
                          ];
                },
        'behaviors' => [
            [
                'class' => '\dosamigos\grid\behaviors\LoadingBehavior',
                'type' => 'spinner'
            ]
        ],
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            //'group_id',
            
            ['attribute' => 'name',
             'label' => 'Name',
             'format' => 'raw',
             'value' =>function ($model) {    
                  if($model->enabled != 1){
                    return '<strike>'.$model->name.'</strike>';
                  }else{
                    return $model->name;
                  }
                },
              ],
            'type.name',
            ['attribute'=>'Active or Archived',
             'value' =>
                       function($model){
                         if($model->enabled == '1'){
                            return 'Active';
                         }
                         else
                         {
                            return 'Archived';
                         }
                       },



             'filter' =>false],
            //'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn',
                          'header' => 'UPDATE',
                          'headerOptions' => ['style' => 'color:#337ab7'],
                          'template' => '{update}',
                          'buttons' => [
            

               'update' => function ($url, $model,$id) {
                            return Html::a('<span class=""><img src="uploads/update.png" width="13" height="13"/></span>', $url, [
                           "class"=>"updatecomponent",'value'=>Url::to(Url::base().'/updatecomponent?id='.$model->group_id)
                ]);
            },
            

          ],
           'urlCreator' => function ($action, $model, $key, $index) {
            

            Url::to(['update?id='.$model->group_id],['id'=>'modal'])   ;         
                //$url ='update?id='.$model->brand_id;
              //return $url;
                 
                 //return Html::a($url, ['title' => 'view']);
                
                
            
            

          },],



        ],
    ]); ?>
  </div>
    <div class="ComponentPageDisplay page-display-showing-foot"></div>
</div>

<script>
  $(function()
{
  //
  $('#createcomponent').click(function(){

   // alert('component is clicked');

    $('#modalcomponent11').modal('show')
      .find('#modalContent6')
      .load ($(this).attr('value'), function(){
        $('#modalcomponent11 .modal-header').remove();

        $('.svecompgrp').click(function(){
          console.log('save is clicked');
          $("#myid5").submit();
        })

      });
  });
});


$(function()
{
  //
  $('.updatecomponent').click(function(){
    $('#modalcomponent11').modal('show')
      .find('#modalContent6')
      .load ($(this).attr('value'));
  });
});
  $(function()
{
  //
  $('.modalButton000007').click(function(){
    $('#modalcomponent11').modal('show')
      .find('#modalContent6')
      .load ($(this).attr('value'),function(){

        var cmpp = $('#cmppname').val();
        $('#modalcomponent11 .modal-header').remove();

      });
  })
});

  //
  $(function() { //equivalent to document.ready
  $(".form-control").focus();
});


$('.form-control').focus(function(){
  var that = this;
  setTimeout(function(){ that.selectionStart = that.selectionEnd = 10000; }, 0);
});


$('#tblcomponentgroupssearch-name').bind('keyup', function() { 
    $('#tblcomponentgroupssearch-name').delay(200).submit();
});

$('#tblcomponentgroupssearch-enabled').bind('change', function() { 
    $('#tblcomponentgroupssearch-enabled').delay(200).submit();
});

  </script>
  <?php $this->registerJs('jQuery("#w4").on("keyup", "input", function(){
                jQuery(this).change();
                  $("#modalcomponent11").remove();
                //OR $.pjax.reload({container:\'#w4\'});
        });',
        yii\web\View::POS_READY);
  ?> 

<script>
  var name = $('.summary').html();
$('.summary').remove();
$('.ComponentPageDisplay').html(name);
if($('.ComponentPageDisplay').text()=='undefined')
{
   $('.ComponentPageDisplay').hide();
}
  $('#modalcomponent11 .modal-header').remove();
</script>
<?php Pjax::end();?>