<?php

use yii\helpers\Html;

use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\TblBrands;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\models\TblTiers */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
h1{
  display:none;
}
</style>

<div class="tbl-tiers-form form-inner-head">

    <?php $form = ActiveForm::begin(['validationUrl' => Url::toRoute(['validatetier']),'enableAjaxValidation' => true, 'id' => 'myidtier']); ?>
    <div class = "modal-body-div-add">
      <h2 >Create Tier</h2>
      <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
      <button type="button" class="close glyphicon glyphicon-remove" data-dismiss="modal" aria-hidden="true"></button>
    </div>
   <?= $form->field($model, 'name')->textInput(['maxlength' => true,'label'=>false]) ?>


    <?= $form->field($model,'brand_id')->dropDownList(ArrayHelper::map(TblBrands::find()->all(),'brand_id','name'),['prompt'=>'Select brand']) ?>

   
    <?= $form->field($model, 'type_id')->radioList(array('1'=>'Interior','2'=>'Exterior','3'=>'Both')); ?>
    <?= $form->field($model, 'enabled')->radioList(array('1'=>'YES','0'=>'NO')); ?>
   




    <?php ActiveForm::end(); ?>
    <?php 
     // echo "<div style=\"width: 280px; border: 2px dashed #000000; float: right; padding: 3px; margin: 5px\">"; 
      ?>
</div>
<script>
$('body').on('beforeSubmit', '#myidtier', function (e) {
  var pathname = window.location.href;  
     var form = $(this);
     
     $.ajax({
          url: form.attr('action'),
          type: 'post',
          data: form.serialize(),
          success: function (response) {
              

               console.log(response);              

               if(response == 1){
                 
               
                 $("#modaltier").modal('hide');
                 $(document.body).removeClass('modal-open');
         $('.modal-backdrop').remove();
                 $.pjax.reload('#tierpjax' , {timeout : false});
              

               }
          },
         
        }
     );
      e.stopImmediatePropagation();
     return false;
});


</script>
