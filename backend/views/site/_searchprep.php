<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\tblPrepLevelSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-prep-level-search tbl-search-input-all">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
        'data-pjax' => 1
        ],
    ]);


     ?>
   
<div style="width:23%; margin-right:0px; border:1px;">
    <?= $form->field($model, 'prep_level')->textInput(['placeholder' => "Search Prep Level"])->label(false); ?>

    </div>

   
    
    <?php ActiveForm::end(); ?>

</div>
