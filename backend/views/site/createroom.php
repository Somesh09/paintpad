<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Tblroomtypes */

//$this->title = 'Create Tblroomtypes';
$this->params['breadcrumbs'][] = ['label' => 'Tblroomtypes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tblroomtypes-create">
    
    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_formroom', [
        'model' => $model,
    ]) ?>

</div>
