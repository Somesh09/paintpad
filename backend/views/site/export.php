<?php
/*
* iTech Empires:  Export Data from MySQL to CSV Script
* Version: 1.0.0
* Page: Export
*/

// Database Connection
$host = "localhost"; // MySQL host name eg. localhost
$user = "root"; // MySQL user. eg. root ( if your on localserver)
$password = "root"; // MySQL user password  (if password is not set for your root user then keep it empty )
$database = "paintpad_dev2"; // MySQL Database name

// Connect to MySQL Database
$con = new mysqli($host, $user, $password, $database);




//get records from database
$query = $con->query("SELECT * FROM tbl_brands");

if($query->num_rows > 0){
    $delimiter = ",";
    $filename = "brands" . date('Y-m-d') . ".csv";
    
    //create a file pointer
    $f = fopen('php://memory', 'w');
    
    //set column headers
    $fields = array('brand_id','name','enabled');
    fputcsv($f, $fields, $delimiter);
    
    //output each row of the data, format line as csv and write to file pointer
    while($row = $query->fetch_assoc()){
     
        $lineData = array($row['brand_id'], $row['name'], $row['enabled']);
        fputcsv($f, $lineData, $delimiter);
    }
    
    //move back to beginning of file
    fseek($f, 0);
    
    //set headers to download file rather than displayed
    header('Content-Type: text/csv');
    header('Content-Disposition: attachment; filename="' . $filename . '";');
    
    //output all remaining data on a file pointer
    fpassthru($f);
}
exit;

?>