<?php
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use kartik\tabs\TabsX;
use yii\bootstrap\Tabs;
use yii\web\Controller;
use yii\filters\VerbFilter;

/* @var $this yii\web\View */

?>

<style type="text/css">

</style>


  <div  id="main">


  <?php
  
  
      $tab1=$this->render('brands',[
            'searchModel' => $searchModel,  
            'dataProvider' => $dataProvider,
            'model'=>$model,
       
           
         ]);




      $tab2=$this->render('productindex', [
            'searchModelproduct' => $searchModelproduct,
            'dataproviderproduct' => $dataproviderproduct,
            'model'=>$model2,

            
         ]);


      $tab3=$this->render('indexsheen',[
            'searchModelSheen'=>$searchModelSheen,
            'dataProviderSheen'=>$dataProviderSheen,
            'model' =>$model3,

      ]);


      $tab4=$this->render('indexprep',[
            'searchModelprep'=>$searchModelprep,
            'dataProviderprep'=>$dataProviderprep,
            'model' =>$model4,

      ]);



      $tab5=$this->render('indexcomponent',[
            'searchModelcomponent'=>$searchModelcomponent,
            'dataProvidercomponent'=>$dataProvidercomponent,
            'model5'=>$model5,  


      ]);



      $tab6=$this->render('indexroom',[
            'searchModelrooms'=>$searchModelrooms,
            'dataProviderrooms'=>$dataProviderrooms,
            'model6'=> $model6,           



      ]);



      $tab7=$this->render('indexcolor',[
            'searchModelcolor'=>$searchModelcolor,
            'dataProvidercolor'=>$dataProvidercolor,
            'model7'=>$model7,


      ]);




      $tab8=$this->render('indexspecial',[
            'searchModelspecial'=>$searchModelspecial,
            'dataProviderspecial'=>$dataProviderspecial,
            'model8'=>$model8,

      ]);




      $tab9=$this->render('indexstrength',[
            'searchModelstrength'=>$searchModelstrength,
            'dataProviderstrength'=>$dataProviderstrength,
            'model9'=>$model9,  


      ]);




      $tab10=$this->render('indexcomponentmain',[
             'searchModelcomponents'=> $searchModelcomponents,
             'dataProvidercomponents'=>$dataProvidercomponents,
             'model10'=>$model10,



      ]);




      $tab11=$this->render('indexcolorsmain',[
             'searchModelcolorsmain'=> $searchModelcolorsmain,
             'dataProvidercolorsmain'=>$dataProvidercolorsmain,
             'model11'=>$model11,



      ]);




       $tab12=$this->render('indexTier',[
            'searchModelTier' =>  $searchModelTier,
            'dataProviderTier' => $dataProviderTier,
            'model12'=>$model12



      ]);





  echo TabsX::widget([
    'options' => ['tab' => 'div'],
    'containerOptions' => ['id' => 'container-id'],
    'itemOptions' => ['tab' => 'div'],
    'pluginOptions' => ['enableCache'=>FALSE],
    'items' => [
                  [
                    'options' => ['id' => 'tab-c'],
                    'label' => 'BRANDS',
                    'content' => $tab1,
                    'linkOptions' => ['class'=>'brandTab']

                  ],
                  [
                    'options' => ['id' => 'tab-udc'],
                    'label' => 'PRODUCTS',
                    'content' => $tab2,
                    'linkOptions' => ['class'=>'productTab']
                  ],

                  [
                    'options' => ['id' => 'tab-adc'],
                    'label' => 'SHEEN',
                    'content' => $tab3,
                    'linkOptions' => ['class'=>'sheenTab']

                  ],

                  [
                    'options' => ['id' => 'tab-Hdc'],
                    'label' => 'COMPONENTS',
                    'content' => $tab10,
                    'linkOptions' => ['class'=>'componentTab']

                  ],

                  [
                    'options' => ['id' => 'tab-bdc'],
                    'label' => 'PREP.LEVEL',
                    'content' => $tab4,
                    'linkOptions' => ['class'=>'prepTab']

                  ],

                  [
                    'options' => ['id' => 'tab-cdc'],
                    'label' => 'COMPONENTS GROUP',
                    'content' => $tab5,
                    'linkOptions' => ['class'=>'groupTab']
                  ],

                  [
                    'options' => ['id' => 'tab-ddc'],
                    'label' => 'ROOMS TYPE',
                    'content' => $tab6,
                    'linkOptions' => ['class'=>'roomTab']

                  ],

                  [
                    'options' => ['id' => 'tab-edc'],
                    'label' => 'COLORS TAG',
                    'content' => $tab7,
                    'linkOptions' => ['class'=>'colorTagTab']

                  ],
                  [
                    'options' => ['id' => 'tab-jdc'],
                    'label' => 'TIERS',
                    'content' => $tab12,
                    'linkOptions' => ['class'=>'tierTab']
                  ],

                  [
                    'options' => ['id' => 'tab-idc'],
                    'label' => 'COLORS',
                    'content' => $tab11,
                    'linkOptions' => ['class'=>'colorsTab']

                  ],

                  [
                    'options' => ['id' => 'tab-fdc'],
                    'label' => 'SPECIAL ITEMS',
                    'content' => $tab8,
                    'linkOptions' => ['class'=>'specialTab']
                  ],

                  [
                    'options' => ['id' => 'tab-gdc'],
                    'label' => 'TINT STRENGTH',
                    'content' => $tab9,
                    'linkOptions' => ['class'=>'tintTab']

                  ],
        
    ],
]);
 

  ?>      
           


</div>



 
<script>
  $(document).ready(function(){
      $('#w1').change(function(){
        //alert("rk");
         if(document.location.href.indexOf("per-page") > -1) {
            console.log(location.href.replace(/&?per-page=([^&]$|[^&]*)/i, ""));
            history.pushState(location.href.replace(/&?per-page=([^&]$|[^&]*)/i, ""), 'Title of the page', location.href.replace(/&?per-page=([^&]$|[^&]*)/i, ""));            
         }
      })

      $('#w4').change (function(){
       // alert("rk");
         if(document.location.href.indexOf("per-page") > -1) {
            console.log(location.href.replace(/&?per-page=([^&]$|[^&]*)/i, ""));
            history.pushState(location.href.replace(/&?per-page=([^&]$|[^&]*)/i, ""), 'Title of the page', location.href.replace(/&?per-page=([^&]$|[^&]*)/i, ""));            
         }
      })

      // $('.sheenTab').click(function(){
      //   //alert("rk");
      //    if(document.location.href.indexOf("per-page") > -1) {
      //       console.log(location.href.replace(/&?per-page=([^&]$|[^&]*)/i, ""));
      //       history.pushState(location.href.replace(/&?per-page=([^&]$|[^&]*)/i, ""), 'Title of the page', location.href.replace(/&?per-page=([^&]$|[^&]*)/i, ""));            
      //    }
      // })
  })


  $(document).ready(function(){
    $('#w25').find('li').removeClass( "active" )
    // $('.brandTab').trigger()

    $(".modal").modal({
      show:false,
      backdrop:'static'
    });
  });
 
</script>
<script>
 
// $(document.body).removeClass('modal-open');
// $( document ).ready(function() {
//     $('#w0').trigger("reset");
//     // $('#tbl_rooms_typessearch-name').val('')
//     // $('#tblbrandssearch-name').val('')
//     // $('#tblproductssearch-name').val('')
//     // $('#tblsheensearch-name').val('')
//     // $('#tblcomponentssearch-name').val('')
//     // $('#tblpreplevelsearch-prep_level').val('')
//     // $('#tblcomponentgroupssearch-name').val('')
//     // $('#tbl_rooms_typessearch-name').val('')
//     // $('#tblcolortagssearch-name').val('')
//     // $('#tbltiers_search-name').val('')
//     // $('#tblcolorssearch-name').val('')
//     // $('#tblspecialitemssearch-name').val('')
//     // $('#tblstrengthssearch-name').val('')

//     //  $('#tbl_rooms_typessearch-name').keyup();
//     // $('#tblbrandssearch-name').keyup();
//     // $('#tblproductssearch-name').keyup();
//     // $('#tblsheensearch-name').keyup();
//     // $('#tblcomponentssearch-name').keyup();
//     // $('#tblpreplevelsearch-prep_level').keyup();
//     // $('#tblcomponentgroupssearch-name').keyup();
//     // $('#tbl_rooms_typessearch-name').keyup();
//     // $('#tblcolortagssearch-name').keyup();
//     // $('#tbltiers_search-name').keyup();
//     // $('#tblcolorssearch-name').keyup();
//     // $('#tblspecialitemssearch-name').keyup();
//     // $('#tblstrengthssearch-name').keyup();
    
// });
</script>