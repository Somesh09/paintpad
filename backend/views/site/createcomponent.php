<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TblComponentGroups */

//$this->title = 'Create Component Groups';

?>
<div class="tbl-component-groups-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_formcomponent', [
        'model' => $model,
    ]) ?>

</div>
