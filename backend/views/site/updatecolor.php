<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TblColorTags */

//$this->title = 'Update Tbl Color Tags: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Tbl Color Tags', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->tag_id]];
$this->params['breadcrumbs'][] = 'Update';
?>

<input type="hidden" id="clrname" value="<?php echo $model->name;?>">
<div class="tbl-color-tags-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_formcolorupdate', [
        'model' => $model,
        'id'=>$id,
    ]) ?>

</div>
