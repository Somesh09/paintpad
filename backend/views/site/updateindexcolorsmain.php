<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TblColors */

//$this->title = 'Update Tbl Colors: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Tbl Colors', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->color_id]];
$this->params['breadcrumbs'][] = 'Update';
?>

<input type="hidden" id="clrrname" value="<?php echo $model->name;?>">
<div class="tbl-colors-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_formupdatecolorsmain', [
        'model' => $model,

        'id'=>$id,
    ]) ?>

</div>
