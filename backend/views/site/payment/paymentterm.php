<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\widgets\Pjax;
use dosamigos\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TblSpecialItemsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


?>
<div class="tbl-special-items-index">

    <h1><?= Html::encode($this->title) ?></h1>
  
<?php Pjax::begin(['timeout' => 5000,'id'=>'PaymentTermPjax']);?>
<?php  echo  Html::button('<img src="'.Url::base().'/uploads/add.png"width="17" height="17"/>', ['value'=>Url::to( Url::base().'/payment/createpaymentterm'),'class'=>'btn btn-primary pull-right' ,'id'=>'modalTerm']);?>
    
 <?php

Modal::begin([
    'header'=>'',
    'id'=>'modelTermOptions',
    'size'=>'modal-lg',
]);
echo "<div id='modalPaymentOptions'><div style='text-align:center'><img src='".Url::base()."/uploads/avatar2.gif' height='100' width='100'></div></div>";

Modal::end();
?>
   
  <?php  //echo $this->render('_searchspecial', ['model' => $searchModelspecial]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProviderTerm,
        //'filterModel' => $searchModel,
        'rowOptions'   => function ($model2, $key, $index, $grid) {
          if ($model2->delete != 0 || Yii::$app->user->identity->role == 20 ) {
            return  [
                           "class"=>"modalButtonUpdate",'value'=>Url::base().'/payment/updatepaymentterm?id='.$model2->id
                      ];
          }
            else{
              return  [
                           "class"=>"modalButtonUpdatexe"
                      ];
            }
              
              },
        'behaviors' => [
            [
                'class' => '\dosamigos\grid\behaviors\LoadingBehavior',
                'type' => 'spinner'
            ]
        ],
        'columns' => [
           
            //'name',
            ['attribute'=>'Name','value' =>'name','filter' =>false],
            ['attribute'=>'Description','value' =>'description','filter' =>false],
            ['attribute'=>'Number Of Days','value' =>'no_of_days','filter' =>false],
            ['attribute'=>'Discount %','value' =>'discount','filter' =>false],
            ['attribute'=>'Discount Days','value' =>'discount_days','filter' =>false],
            ['attribute'=>'Default',
             'value' =>
                       function($model2){
                         if($model2->is_default == '1'){
                            return 'Default';
                         }
                         else
                         {
                            return '';
                         }
                       },



             'filter' =>false,],
             // ['attribute'=>'Enabled',
             // 'value' =>
             //           function($model2){
             //             if($model2->enabled == '1'){
             //                return 'Enabled';
             //             }
             //             else
             //             {
             //                return '';
             //             }
             //           },



             // 'filter' =>false,],

            ['class' => 'yii\grid\ActionColumn',
               'header' => 'UPDATE',
               'headerOptions' => ['style' => 'color:#337ab7'],
               'template' => '{update}',
               'buttons' => [
                    'update' => function ($url, $model2 ,$id) {
                      if ($model2->delete != 0 || Yii::$app->user->identity->role == 20 ) {
                        return Html::a('<span class=""><img src="'.Url::base().'/uploads/update.png" width="13" height="13"/></span>', $url, [
                                 "class"=>"modalButtonUpdate",'value'=>Url::base().'/payment/updatepaymentmethod?id='.$model2->id
                                  ]);
                      }
                      else{
                        return '-';
                      }
                                  
                                },
                    ],
              'urlCreator' => function ($action, $model2, $key, $index) {
               Url::to(['update?id='.$model2->id],['id'=>'modal'])   ;         
                //$url ='update?id='.$model->brand_id;
              //return $url;
                 
                 //return Html::a($url, ['title' => 'view'            

          		},      
           ],
        ],
    ]); ?>
</div>
<script>

$('#modalTerm').click(function(){
  $('#modelTermOptions').modal('show')
  .find('#modalPaymentOptions') .load ($(this).attr('value'))
  $('.modal-header').html('<h5>Create Payment Term</h5><button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>')
})

$('.modalButtonUpdate').click(function(){
	 $('#modelTermOptions').modal('show')
	 .find('#modalPaymentOptions') .load ($(this).attr('value'))
	 $('.modal-header').html('<h5>Edit Payment Term</h5><button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>')
})
</script>
<?php Pjax::end();?>


