<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\widgets\Pjax;
use dosamigos\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TblSpecialItemsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


?>
<div class="tbl-special-items-index">

    <h1><?= Html::encode($this->title) ?></h1>
  
<?php Pjax::begin(['timeout' => 5000,'id'=>'Paymentpjax']);?>
<?php  echo  Html::button('<img src="'.Url::base().'/uploads/add.png"width="17" height="17"/>', ['value'=>Url::to( Url::base().'/payment/createpaymentmethod'),'class'=>'btn btn-primary pull-right' ,'id'=>'modalPaymentMethodCreate']);?>
    
 <?php

Modal::begin([
    'header'=>'',
    'id'=>'modelPaymentMethodCreate',
    'size'=>'modal-lg',
]);
  echo "<div id='modalMethodPayment'><div style='text-align:center'><img src='".Url::base()."/uploads/avatar2.gif' height='100' width='100'></div></div>";

Modal::end();
?>
   
  <?php  //echo $this->render('_searchspecial', ['model' => $searchModelspecial]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'rowOptions'   => function ($model, $key, $index, $grid) {
          if ($model->delete != 0 || Yii::$app->user->identity->role == 20 ) {
            return  [
                         "class"=>"modalButtonspecial2",'value'=>Url::base().'/payment/updatepaymentmethod?id='.$model->id
                    ];
          }
          else{
             return  [
                        "class"=>"modalButtonspecials2"
                    ];
          }
              
              },
        'behaviors' => [
            [
                'class' => '\dosamigos\grid\behaviors\LoadingBehavior',
                'type' => 'spinner'
            ]
        ],
        'columns' => [
           
            //'name',
            ['attribute'=>'Name','value' =>'name','filter' =>false],
            ['attribute'=>'Default',
             'value' =>
                       function($model){
                         if($model->is_default == '1'){
                            return 'Default';
                         }
                         else
                         {
                            return '';
                         }
                       },



             'filter' =>false,],

            ['class' => 'yii\grid\ActionColumn',
               'header' => 'UPDATE',
               'headerOptions' => ['style' => 'color:#337ab7'],
               'template' => '{update}',
               'buttons' => [
                    'update' => function ($url, $model,$id) {
                      if ($model->delete != 0 || Yii::$app->user->identity->role == 20 ) {
                        return Html::a('<span class=""><img src="'.Url::base().'/uploads/update.png" width="13" height="13"/></span>', $url, [
                                 "class"=>"modalButtonspecial2",'value'=>Url::base().'/payment/updatepaymentmethod?id='.$model->id
                                  ]);
                      }
                      else{
                        return '-';
                      }
                                  
                                },
                    ],
              'urlCreator' => function ($action, $model, $key, $index) {
               Url::to(['updatepaymentmethod?id='.$model->id],['id'=>'modal'])   ;         
                //$url ='update?id='.$model->brand_id;
              //return $url;
                 
                 //return Html::a($url, ['title' => 'view'            

          },      
           ],
         ],
      ]); ?>
</div>
<script>
  $('#modalPaymentMethodCreate').click(function(){
     $('#modelPaymentMethodCreate').modal('show')
      .find('#modalMethodPayment') .load ($(this).attr('value'))
      $('.modal-header').html('<h5>Create Payment Method</h5><button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>');

  })

  $('.modalButtonspecial2').click(function(){
     $('#modelPaymentMethodCreate').modal('show')
      .find('#modalMethodPayment') .load ($(this).attr('value'))
      $('.modal-header').html('<h5>Edit Payment Method</h5><button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>');

  })
  
</script>
<?php Pjax::end();?>

