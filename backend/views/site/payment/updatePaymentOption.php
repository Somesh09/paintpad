<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\TblBrands */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-payment-form">
 <?php $form = ActiveForm::begin(['id' => 'myid4']); ?>
	 <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
   <?= $form->field($model, 'no_of_days')->textInput(['maxlength' => true]) ?>
   <?= $form->field($model, 'discount_days')->textInput(['maxlength' => true]) ?>
   <?= $form->field($model, 'discount', [
            'template' => '<strong>Discount %</strong><div class="input-group">{input}
            <span class="input-group-addon">%</span></div>{error}{hint}'
        ])->textInput(['maxlength' => true]) ?>
   <?= $form->field($model, 'is_default')->radioList(array('1'=>'YES','0'=>'NO')); ?>
   <?= $form->field($model, 'description')->textarea(['rows' => '6']) ?>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
 <?php ActiveForm::end(); ?>
</div>
<script>
$('body').on('beforeSubmit', '#myid4', function (e) {
     var pathname = window.location.href;
     //console.log(pathname);
     var form = $(this);
     //console.log("sdadas");
     // submit form
     $.ajax({
          url: form.attr('action'),
          type: 'post',
          data: form.serialize(),
          success: function (response) {
               // do something with response

                          

               if(response == 1){
                  $("#modelTermOptions").modal('hide')
                  $('.modal-backdrop').remove();
                  $.pjax.reload('#PaymentTermPjax' , {timeout : false});
                
      
               }
          },
          
     });
     e.stopImmediatePropagation();
     return false;
});





</script>