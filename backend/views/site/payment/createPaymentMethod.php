<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\TblBrands */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-payment-form">
 <?php $form = ActiveForm::begin(['id' => 'myid1']); ?>
	 <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
   <?= $form->field($model,'company_id')->textInput(['type'=>"hidden", 'value' => Yii::$app->user->identity->company_id])->label(false) ?>
    <?php $model->is_default = '0';?>
      <?= $form->field($model, 'is_default')->radioList(array('1'=>'YES','0'=>'NO')); ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success','id'=>'closepayment']) ?>
    </div>
 <?php ActiveForm::end(); ?>
</div>
<script>
$('body').on('beforeSubmit', '#myid1', function (e) {
     var pathname = window.location.href;
     //console.log(pathname);
     var form = $(this);
     //console.log("sdadas");
     // submit form
     $.ajax({
          url: form.attr('action'),
          type: 'post',
          data: form.serialize(),
          success: function (response) {
               // do something with response

                          

               if(response == 1){
                  $("#modelPaymentMethodCreate").modal('hide')
                  $('.modal-backdrop').remove();
                  // $.pjax.reload('#Paymentpjax' , {timeout : false});
                  $.pjax.reload('.printable' , {timeout : false});
                
      
               }
          },
          
     });
     e.stopImmediatePropagation();
     return false;
});





</script>