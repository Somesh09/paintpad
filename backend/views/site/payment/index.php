<?php
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use kartik\tabs\TabsX;
use yii\bootstrap\Tabs;
use yii\web\Controller;
use yii\filters\VerbFilter;

/* @var $this yii\web\View */

?>
<style type="text/css">
  .nav > li {
    font-size: 13px !important;
  }
</style>

<script>
  $(document).ready(function(){
    $('.paymentMethodTab').click(function(){
      $('#Paymentpjax').show();
      $('#tab-gdc').hide();
    })
    $('.paymentTermTab').click(function(){
      $('#Paymentpjax').hide();
      $('#tab-gdc').show();
    })
    $('#w2 li:first').removeClass('active')
  })
  
  
</script>



  <?php
  
  
      $tab1=$this->render('/site/payment/paymentindex',[
            'searchModel' => $searchModel,  
            'dataProvider' => $dataProvider,
            'model'=>$model,
       
           
     ]);




      $tab2=$this->render('/site/payment/paymentterm',[
            'searchModelTerm' => $searchModelTerm,
            'dataProviderTerm' => $dataProviderTerm,
            'model2'=>$model2,


     ]);


      





  echo TabsX::widget([
    'options' => ['tab' => 'div'],
    'containerOptions' => ['id' => 'container-id'],
    'itemOptions' => ['tab' => 'div'],
    'pluginOptions' => ['enableCache'=>FALSE],
    'items' => [
                  [
                    'options' => ['id' => 'tab-c'],
                    'label' => 'Payment Method',
                    'content' => $tab1,
                    'linkOptions' => ['class'=>'paymentMethodTab active']

                  ],
                  

                  [
                    'options' => ['id' => 'tab-gdc'],
                    'label' => 'Payment Term',
                    'content' => $tab2,
                    'linkOptions' => ['class'=>'paymentTermTab']

                  ],
        
    ],
]);
 

?>   





           






 
