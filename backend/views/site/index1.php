<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TblBrandsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tbl Brands';
// $this->params['breadcrumbs'][] = $this->title;
?>
    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        

    </p>
    <?php
    //modal for creating the brands and showing the create form using pop up
       Modal::begin([
          'header'=>'',
          'id'=>'modal',
          'size'=>'modal-lg',
       ]);
       echo "<div id='modalContent'></div>";
      Modal::end();
    ?>

     <?php
     ///modal for sorting the brands and viewing that in pop up
       Modal::begin([
          'header'=>'',
          'id'=>'modalSort',
          'size'=>'modal-lg',
       ]);
       echo "<div id='modalContent'></div>";
      Modal::end();


      Modal::begin([
          'header'=>'',
          'id'=>'modalSort',
          'size'=>'modal-lg',
       ]);
       echo "<div id='modalContent'></div>";
      Modal::end();
    ?>
    <?php
    //modal for updating the brands and showing the update form using pop up
       Modal::begin([
          'header'=>'',
          'id'=>'modalupdate',
          'size'=>'modal-lg',
       ]);
       echo "<div id='modalContent'></div>";
      Modal::end();
    ?>

<?= Html::button('Sort Brands', ['value'=>Url::to( Url::base().'/sort'), 'class' => 'btn btn-success','id'=>'modalButtons2']);
?>
  
    <?php 
      
    Pjax::begin(['timeout' => 5000],['id'=>'branchesGrid'] ); 
    echo  GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
         'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

        //     // 'brand_id',
            'name',
        //     // 'logo',
        //     // 'enabled',
        //     // 'created_at',
        //     //'updated_at',

          ['class' => 'yii\grid\ActionColumn',

           'header' => 'UPDATE',
          'headerOptions' => ['style' => 'color:#337ab7'],
          'template' => '{update}',
          'buttons' => [
            

            'update' => function ($url, $model,$id) {
                return Html::a('<span class="">UPDATE</span>', $url, [
                           "class"=>"modalButton3",'value'=>Url::to(Url::base().'/update?id='.$model->brand_id)
                ]);
            },
            

          ],
          'urlCreator' => function ($action, $model, $key, $index) {
            

              Url::to(['update?id='.$model->brand_id],['id'=>'modal'])   ;         
                //$url ='update?id='.$model->brand_id;
              //return $url;
                 
                 //return Html::a($url, ['title' => 'view']);
                
                
            
            

          }


        ],

         ],

    ]); 

    ?>
    <!--  -->
</div>
<?php pjax::end();?>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
 --><script>
  $(function()
{
  //
  $('#modalButton').click(function(){
    $('#modal').modal('show')
      .find('#modalContent')
      .load ($(this).attr('value'));
  });

  //
  $('#modalButtons2').click(function(){
    $('#modalSort').modal('show')
      .find('#modalContent')
      .load ($(this).attr('value'));
  });

  //
  $('.modalButton3').click(function(){
    $('#modalupdate').modal('show')
      .find('#modalContent')
      .load ($(this).attr('value'));
  });


});




  </script>

  
 

