<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\TblComponentGroups;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
USE app\models\TblComponentCalculateMethod;
use app\models\TblComponentPricingMethods;
//use wbraganca\dynamicform\DynamicFormWidget;
use app\models\TblComponentType;
?>
<style> 
.form-group.field-tblcomponenttype-0-is_default {margin: 0;}.form-control {margin-top: 10px;}
/* .modal-dialog {
    width: 700px;
    margin: 30px auto;
} */
.panel-default > .panel-heading {

    color: #333;
    background-color: #f5f5f5;
    border-color: #ddd;
    float: left;
    width: 100%;

}
.panel-heading h4 {

    float: left;

}
.panel-heading button {
    float: right;
    padding: 5px 10px;
    font-size: 14px;
    letter-spacing: .7px;
    margin-top: 5px;
}
.btn.btn-success.btn-xs i {
    margin: 0 0 0 5px;
}

.container-items {

    margin-top: 65px;

}
#main-ww .form-control {
    width: 78%;
    display: inline-block;
}
#main-ww label {
    display: inline-block;
    max-width: 100%;
    margin-bottom: 5px;
    font-weight: bold;
    width: 20%;
}

.form-group {
    margin-bottom: 0;
}
label {
    margin-bottom: 0;
}
.panel-body {

    padding: 0 15px;

}
.panel-default {

    border-color: transparent;
    box-shadow: none;
    margin: 0;

}
.panel-default > .panel-heading {

    color: #333;
    background-color: #f5f5f5;
    border-color: transparent;
    float: left;
    width: 100%;
    margin-bottom: 25px;

}

.tbl-components-form .form-group {
  float: left;
  width: 50%;
  padding: 0 10px;
 /* min-height: 85px;
  height: 85px;*/
}
#main-ww .form-control {

    margin-bottom: 0 !important;
}
 .ttClass {
    width: 100%;
    float: left;
}
.ttClass-one {
    /*width: 50%;*/
    float: left;
}
.ttClass-two {
    float: right;
    width: 50%;
}
.tbl-components-form .ttClass-two .form-group {
    width: 100%;
}
.tbl-components-form .ttClass-one .form-group {
    height: auto;
    min-height: auto;
    width: 100%;
}
.ttClass-one img {
    float: left;
    margin-bottom: 14px;
    border: 1px solid #ccc;
}
/* .newTable-col .col-sm-4, .newTable-col .col-sm-3 {
    width: 100%;
    padding-left: 0;
} */
h1{
  display:none;
}

/* #td-set-one {
	overflow-y: scroll;
	overflow-x: hidden;
	margin: 0 auto;
	max-height: 100px;
	float: left;
	width: 100%;
} */
#td-set-one .col-sm-3, #td-set-one .col-sm-2 {
	padding: 0 25px 0 10px;
	width: 25%;
	text-align: left;
}
div#here1 h3 ,div#hereSized h3, div#hereThickness h3 ,div#hereFixed h3,div#maindiv22 h3 {
    text-align: left;
    margin: 0;
    width: 724px;
    float: left;
    font-size: 20px;
    padding: 0px 15px 0 15px;
}
.td-set-2 .col-sm-2 {
	width: 16.66666667% !important;
}

.aabb-02 td {
    padding: 10px 15px 8px 15px !important;
    width: 16% !important;
    text-align: left !important;
    display: table-cell;
    border-top: 1px solid #ddd !important;
}
.two-clsss th {
    width: 17.2% !important;
    display: table-cell;
}
/* .aabb-02 input {
    width: 118px;
} */
.table.newTable-col.aabb-02 th.col-sm-3 {
    padding-top: 10px !important;
    padding-bottom: 10px !important;
}
.aabb-01.aabb-02 input {
    width: 171px;
}
.aabb-01.aabb-02 .col-sm-2 input {
    width: 52px;
    margin-right: 140px;
}
.aabb-03.aabb-02 input.empty.form-control {
    width: 158px;
}

</style>
<div class="loader" style="display:none;"></div>
<input type="hidden" class="pricingMethodType" value="<?php echo $model['price_method_id']?>">
<input type="hidden" value="<?php echo count($model2) - 1;?>" class="countMethod">
<input type="hidden" value="0" class="formValue">
<input type="hidden" value="0" class="formValueNew">
<div class="tbl-components-form form-inner-head">
<?php $group_type = json_encode(ArrayHelper::map(TblComponentGroups::find()->all(),'group_id','type_id')); ?>
<?php $GroupType = $model->group_id;?>

    <?php $form = ActiveForm::begin([ 'id' => 'dynamic']); ?>

    <div class = "modal-body-div-add">
    <h2><?php echo $model->name;?></h2>
       <input type="submit" class="btn btn-success saveComponentUpdate" value="Save" >
      <button type="button" class="close glyphicon glyphicon-remove" data-dismiss="modal" aria-hidden="true">×</button>
    </div>



<div class="div-scroll-product" style="padding-top: 0;"> 

<div class="main-div-upimgdiv">
  <div class="upImgDiv main-copo-01">
  <h2 style="
    font-size: 15px;
    padding-left: 15px;
    font-weight: bold;
    background: #eee;
    padding-top: 13px;
    padding-bottom: 13px;
    border-bottom: 1px solid #ddd;
    width: 100%;
"> Component </h2>
      <div class="">
     <!-- <div class="imgg_div">-->
	  <div class="">
		
    <?= $form->field($model, 'name')->textInput(['maxlength' => true])->label('Name') ?>
    <?= $form->field($model,'group_id')->dropDownList(ArrayHelper::map(TblComponentGroups::find()->all(),'group_id','name'),['prompt'=>'Select Component Group','onchange'=>'return showGroupId(this)']) ?>
    <!-- ,'onchange'=>'return showGroupId(this)' add this-->
    <?= $form->field($model,'calc_method_id')->dropDownList(ArrayHelper::map(TblComponentCalculateMethod::find()->all(),'calc_method_id','name'),['prompt'=>'Select Calculate Method Name']) ?>
    <?= $form->field($model,'price_method_id')->dropDownList(ArrayHelper::map(TblComponentPricingMethods::find()->all(),'price_method_id','name'),['prompt'=>'Select Price Method Name','onchange'=>'return showdiv(this)'])?>
      </div>
      </div>
    </div> <!-- .upImgDiv -->




  <div id="main-ww" class="bttmDiv main-copo-02">    
  <h2 style="
    font-size: 15px;
    padding-left: 15px;
    font-weight: bold;
    background: #eee;
    border-bottom: 1px solid #ddd;
    margin-top: 15px;
    padding-top: 13px;
    padding-bottom: 13px;
    width: 100%;
    border-top: 1px solid #ddd;
">Tool Tips</h2>
   <!--<?//= $form->field($model,'tt_image',['inputOptions'=>['name' => 'image','id'=>'imgInp']])->fileinput()->label(false);?>

    <?php
        //$imgg = $model->tt_image;

      //  if( $model->tt_image == 'null' || $model->tt_image == null ){
        ?>
          <img id="blah2" src="uploads/no_image.png" alt="your image" style="max-width: 100%" />
        <?php
        //}else{
        ?>
          <img id="blah2" src="uploads/<?php// echo $model->tt_image?>" alt="your image" />
        <?//php
       // }
        ?>  
         <span class="opnImg"><img id="blah21" src="uploads/edit.png" alt="your image" /></span>      
        <?//= $form->field($model,'tt_image',['inputOptions'=>['name' => 'image','id'=>'imgInp','class'=>'addImageProduct']])->fileinput()->label(false);?>-->
		
		
  <div class="imgg_div "> 
    <?php
      if( $model->tt_image == 'null' || $model->tt_image == null ){
          echo ' <img id="blah2" src="uploads/no_image.png"  style="max-width: 100%"/>';
      }
      else
      {
        echo '<img id="blah2" src="uploads/'.$model->tt_image.'"  />';
      }
    ?>
    
   
    <span class="opnImg"><img id="blah21" src="uploads/edit.png" alt="your image" />
    </span> <?= $form->field($model,'tt_image',['inputOptions'=>['name' => 'image','id'=>'imgInp']])->fileinput()->label(false);?>  
  </div>
   <!--<div class="newAreaField" style="display:hidden; width:45%;right: 150px;"> </div>-->
  




	
	<div class="ttClass">     
      <div class="">
          <?= $form->field($model, 'tt_url')->textInput(['maxlength' => true]) ?>
          <?= $form->field($model, 'tt_text')->textarea(['maxlength' => true]) ?>
      </div>
   </div> 
 </div>



</div>



    
<div id="maindiv22" class="main-dvv-121">
  <div id="here1">
    <div class="panel panel-default">           
      <div class="panel-heading" style="border-bottom: 0px solid #ddd;float: left;width: 100%;padding: 10px 0 0px 0; "><h3>Types</h3> 
        <button type="button" class="add-itemmain btn btn-success btn-xs" data="<?php  if($model->price_method_id==1){echo 'sizing';}else if($model->price_method_id==2){echo 'fixed';}else{echo 'thickness';}?>" style="top: -8px !important;">Add New Type<i class="glyphicon glyphicon-plus"></i></button>    
      </div>

                   
               <?php if($model->price_method_id==1){ ?>
               <div >
                  <table class="table sizingBody">
                    <tr>
                      <th>
                         Name
                      </th>
                      <th>
                         Work Rate m2/hour 
                      </th>
                      <th>
                         Spread Ratio
                      </th>
                      <th>
                       Default    
                      </th>
                      <th>
                        Delete
                      </th>
                    </tr>
                  
                </div>
             

                <?php } ?>

                   <?php if($model->price_method_id==3 ){ ?>

                     <table class="table sizingBody">
                    <tr>
                      <th>
                         Name
                      </th>
                      <th>
                         Work Rate m2/hour
                      </th>
                      <th>
                         Spread Ratio
                      </th>
                      <th>
                        Thickness mm
                      </th>
                      <th>
                        Default
                      </th>
                      <th>
                       Delete
                      </th>
                    </tr>
                 
             

                    <?php } ?>


                    <?php if( $model->price_method_id==2){ ?>
                        <table class="two-clsss sizingBody table">
                          <tr>
                            <th >
                               Name
                            </th>
                            <th>
                               Work Rate m2/hour
                            </th>
                            <th> 
                               Spread Ratio
                            </th>
                            <th>
                             Exclusion Area
                            </th>
                            <th>
                               Fixed Area
                            </th>
                            <th>
                              Default
                            </th>
                            <th> 
                            Delete
                            </th>
                          </tr>
                                
                    <?php } ?>
                  <?php foreach ($model2 as $i => $models): 
                if(isset($models->comp_type_id))
                    {
                      ?>
                      <input type="hidden" name="TblComponentType[<?php echo $i;?>][id]" value="<?php echo $models->comp_type_id?>">
                      
                          <tr>
                              <td>
                                 <?= $form->field($models, "[{$i}]name",['inputOptions' => ['class'=>'form-control']])->textInput(['maxlength' => true,'required'=>'required','name' => 'TblComponentType['.$i.'][name_update]'])->label(false); ?>
                              </td>
                            
                              <td>
                                 <?= $form->field($models, "[{$i}]work_rate")->textInput(['maxlength' => true,'required'=>'required','name' => 'TblComponentType['.$i.'][work_rate_update]'])->label(false); ?>
                              </td>
                              <td>
                                    <?= $form->field($models, "[{$i}]spread_ratio")->textInput(['maxlength' => true,'required'=>'required','name' => 'TblComponentType['.$i.'][spread_ratio_update]'])->label(false); ?>
                              </td>
                              <?php 
                                if($model->price_method_id==2)
                                {
                                  ?>
                                  <td>
                                   <?= $form->field($models, "[{$i}]excl_area")->textInput(['maxlength' => true,'required'=>'required','name' => 'TblComponentType['.$i.'][excl_area_update]'])->label(false); ?>
                                 </td>
                                 <td>
                                   <?= $form->field($models, "[{$i}]fixed_area")->textInput(['maxlength' => true,'required'=>'required','name' => 'TblComponentType['.$i.'][fixed_area]'])->label(false); ?>
                                 </td>
                              <?php
                                }
                              ?>
                              <?php 
                                if($model->price_method_id==3)
                                {
                                  ?>
                                  <td>
                                   <?= $form->field($models, "[{$i}]thickness")->textInput(['maxlength' => true,'required'=>'required','name' => 'TblComponentType['.$i.'][thickness_update]'])->label(false); ?>
                                 </td>
                                  <?php
                                }
                              ?>
                              
                                <td>
                                   <?= $form->field($models, "[{$i}]is_default")->checkbox(array('class'=>'nn','label'=>'','name' => 'TblComponentType['.$i.'][is_default_update]'))->label(false);?>
                                </td>
                                <td>
                                  <button type="button" class="close glyphicon glyphicon-remove deleteSaveComponentType" data-id="<?php echo $models->comp_type_id;?>"></button>
                                </td>
                             
                            </tr>
                        
                    
                         
                  <?php   } endforeach; ?>
                 </table>
                </div>
                  </div>
               
              </div>
       
          </div>
		  
		  
		  
		  
          </div>





     </div>
  </div>  
 <?php ActiveForm::end(); ?>


<div id="hereSized" style="display:none">  
  <div class="item panel panel-default">
  	<div class="panel-heading" style="border-bottom: 1px solid #ddd;float: left;width: 100%;padding: 10px 0 0px 0; ">
			<h3 class="panel-title pull-left">Types</h3>
			<button type="button" class="add-item btn btn-success btn-xs" data="sized" style="top: -8px !important;">Add New Type<i class="glyphicon glyphicon-plus"></i></button>
		</div>
    <div class="panel-body ">
      <div class="row">
        <div id="td-set-one">
          <table class="table  newTable-col aabb-01 aabb-02" id="sizingTable">
        <tr>
          <th>
             <label class="control-label" >Name</label>
          </th>
          <th>
             <label class="control-label" >Work Rate m2/hour</label>
          </th>
          <th >
             <label class="control-label" >spread_ratio</label>
          </th>
          <th>
             <label class="control-label" >Default</label>
          </th>
          <th>
              <label class="control-label" >Delete</label>
          </th>
        </tr>
      </table>

        </div>					  
      </div><!-- .row -->
    <div class="row">
    </div>
    </div>
  </div>
</div><!--sized area div end here-->
 
<div id="hereFixed" style="display:none">
  <div class="item panel panel-default">
    <div class="panel-heading" style="border-bottom: 0px solid #ddd;float: left;width: 100%;padding: 10px 0 0px 0; ">
      <h3 class="panel-title pull-left">Types</h3>
      <button type="button" class="add-item btn btn-success btn-xs" data="fixed" style="top: -8px !important;">Add New Type<i class="glyphicon glyphicon-plus"></i></button>
    </div>
    <div class="panel-body">
      <div class="row">
        <div id="td-set-one">
          <table class="table  newTable-col aabb-02" id="fixedTable">
            <tr>
            <th>
              <label class="control-label" >Name</label>
            </th>
            <th>
              <label class="control-label" >Work Rate m2/hour</label>
            </th>
            <th>
              <label class="control-label" >Exclusion Area</label>
            </th>
            <th>
              <label class="control-label" >Spread Ratio</label>
            </th>
            <th>
              <label class="control-label" >Fixed Area</label>
            </th>
            <th>
              <label class="control-label" >Is Default</label>
            </th>
            <th>
              <label class="control-label" >Delete</label>
            </th>
            </tr>
          </table>
        </div>
      </div><!-- .row -->
      <div class="row">
      </div>
    </div>
  </div>
</div><!--fixed area div end here-->




<div id="hereThickness" style="display:none">
  <div class="panel-heading" style="border-bottom: 0px solid #ddd;float: left;width: 100%;padding: 10px 0 0px 0; ">
    <h3 class="panel-title pull-left">Types</h3>
    <button type="button" class="add-item btn btn-success btn-xs" data="thickness" style="top: -8px !important;">Add New Type<i class="glyphicon glyphicon-plus"></i></button>
  </div>
  <div class="panel-body">
    <div class="row">
      <table class="table  newTable-col aabb-03 aabb-02" id="thicknessTable">
      <tr>
        <th>
          <label class="control-label" >Name</label>
        </th>
        <th>
          <label class="control-label" >Work Rate m2/hour</label>
        </th>
        <th>
          <label class="control-label" >Spread Ratio</label>
        </th>
        <th>
          <label class="control-label" >Thickness</label>
        </th>
        <th>
          <label class="control-label" >Is Default</label>
        </th>
        <th>
          <label class="control-label" >Delete</label>
        </th>
      </tr>
    </table>
    </div>
  </div><!-- .row -->
  <div class="row">
  </div>
</div>
<div class="loader" style="display:none;"></div>



<script>
  $('#blah21').click(function(){
    $('#imgInp').click();
  })
</script>

<script>
  //


    
//$("div").removeClass("form-group");
var pId= $('.pricingMethodType').val();
var fieldHtml =$("#here1").html();
//console.log(fieldHtml);

function showdiv(elem)
{
   if(elem.value == pId)
    {     
         $('#maindiv22').empty();
         $('#maindiv22').append(fieldHtml);
    }
    
    if(elem.value == 1 && elem.value!=pId)
    { 
         $('#maindiv22').empty();
         var daata = $('#hereSized').html();         
         $('#maindiv22').append(daata);
    }    

     if(elem.value == 2 && elem.value!=pId)
    {        
          $('#maindiv22').empty();    
          var data = $('#hereFixed').html();
          $('#maindiv22').append(data);
    }
    if(elem.value == 3 && elem.value!=pId)
    {
          $('#maindiv22').empty();
          var dataa = $('#hereThickness').html();    
          $('#maindiv22').append(dataa);
    }
    if(elem.value =="" && elem.value!=pId)
    {
        $('#maindiv22').empty();
    }
}





$(document).on('change', 'input[type="checkbox"]', function(){
   $(this).closest('#maindiv22').find('input[type="checkbox"]').prop('checked',false);
   $(this).prop('checked',true);
});



function readURL(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {

      $("img").show();
      $('#blah2').attr('src', e.target.result);
      //$("#blah3").hide();      
    }
    reader.readAsDataURL(input.files[0]);
  }

}

$("#imgInp").change(function() {
  readURL(this);
});

$(document).ready(function(){

    $("div").on("click",".add-itemmain", function(e){
      // $('.nn').prop('checked',false)
    var type = $('.add-itemmain').attr("data");
    var countMethod =$('.countMethod').val();
   // var countMethod =countMethod-1;
    // alert(countMethod);
    var formValue = $('.formValue').val();

    $('.countMethod').removeAttr('value');  
    $('.countMethod').val(parseInt(countMethod)+parseInt("1"));
    //alert(parseInt(countMethod)+parseInt("1"));
    $(".div-scroll-product").animate({ scrollTop: $(document).height() }, 1000);
    if(type=="sizing")
    {
       var fieldHtml='<tr><td><div class="form-group field-tblcomponenttype-0-name required"><input type="text" name="new['+formValue+'][name]" class="form-control empty" data="Name"><div class="help-block"></div></div></td><td><div class="form-group field-tblcomponenttype-0-work_rate required"><input type="text"  class="form-control empty number " name="new['+formValue+'][work_rate]" data="Work Rate"><div class="help-block"></div></div></td><td><div class="form-group field-tblcomponenttype-0-spread_ratio"><input type="text"  class="form-control empty number" name="new['+formValue+'][spread_ratio]" data="Spread Ratio"><div class="help-block"></div></div></td><td class="text-center" ><div class="form-group field-tblcomponenttype-0-is_default"><input type="checkbox" class="nn"  name="new['+formValue+'][checkbox]" ><div class="help-block"></div></div></td><td text-center"><button type="button" class="close glyphicon glyphicon-remove deleteComponentType" data-id="'+formValue+'"></button></td></tr>';
       $('.sizingBody').append(fieldHtml);    
    }
    if(type=="fixed")
    {
       var fieldHtml='<tr><td><div class="form-group field-tblcomponenttype-0-name required has-success"><input type="text" data="Name" name="new['+formValue+'][name]" class="form-control empty"><div class="help-block"></div></div></td><td><div class="form-group field-tblcomponenttype-0-work_rate required"><input type="text"  class="form-control empty number" name="new['+formValue+'][work_rate]" data="Work Rate"><div class="help-block"></div></div></td><td class=""><div class="form-group field-tblcomponenttype-0-spread_ratio"><input type="text"  class="form-control empty number" name="new['+formValue+'][spread_ratio]" data="spread ratio"><div class="help-block"></div></div></td><td><div class="form-group field-tblcomponenttype-0-excl_area"><input type="text" data="Excl Area" class="form-control empty number" name="new['+formValue+'][excl_area]"><div class="help-block"></div></div></td><td ><div class="form-group field-tblcomponenttype-0-fixed_area"><input type="text"  class="form-control empty number" name="new['+formValue+'][fixed_area]" data="fixed area"><div class="help-block"></div></div></td><td text-center" style="width: auto !important;padding-left: 10px !important;"><div class="form-group field-tblcomponenttype-0-is_default"><input style="width: 43px;" type="checkbox"   name="new['+formValue+'][checkbox]"></label><div class="help-block"></div></div></td><td class="text-center" style="width: auto !important;"><button class="glyphicon glyphicon-remove close deleteComponentType" data-id="'+formValue+'" type="button" ></span></td></tr>';
       $('.sizingBody').append(fieldHtml);    
    }
    if(type=="thickness")
    {
       var fieldHtml='<tr><td><div class="form-group field-tblcomponenttype-0-name required"><input type="text" data="Name" name="new['+formValue+'][name]" class="form-control empty "><div class="help-block"></div></div></td><td><div class="form-group field-tblcomponenttype-0-work_rate required"><input type="text"  class="form-control empty number" name="new['+formValue+'][work_rate]" data="Work Rate"><div class="help-block"></div></div></td><td><div class="form-group field-tblcomponenttype-0-spread_ratio"><input type="text"  class="form-control empty number" data="Spread Ratio" name="new['+formValue+'][spread_ratio]"><div class="help-block"></div></div></td><td><div class="form-group field-tblcomponenttype-0-thickness"><input type="text"  class="form-control empty number" data="Thickness" name="new['+formValue+'][thickness]"><div class="help-block"></div></div></td><td class="text-center"><div class="form-group field-tblcomponenttype-0-is_default"><input type="checkbox"  name="new['+formValue+'][checkbox]" ><div class="help-block"></div></div></td><td text-center"><button type="button" class="close glyphicon glyphicon-remove deleteComponentType" data-id="'+formValue+'"></button></td></tr>';
       $('.sizingBody').append(fieldHtml);    
    }
        var formValue = formValue++;
        console.log(formValue++);
        $('.formValue').val(formValue++);
        e.stopPropagation();
 })
   
})




</script>
<script>
  $(document).ready(function(){
    $("div").on("click",".add-item", function(e){
      var type = $('.add-item').attr('data');
      var formValue = $('.formValueNew').val();
     // alert(type);
      if(type=="sized")
      {
        var fieldHtml ='<tr><td class="col-sm-3"><div><input type="text" name="newType['+formValue+'][name]" autofocus="autofocus" class="empty form-control" data="Name"></div></td><td class="col-sm-3"><div><input type="text" name="newType['+formValue+'][work_rate]" data="Work Rate" class="empty number form-control"></div></td><td class="col-sm-3"><div><input data="Spread Ratio" type="text" name="newType['+formValue+'][spread_ratio]" class="empty number form-control"></div></td><td class="col-sm-2"><div><input type="checkbox" name="newType['+formValue+'][is_default]" ></div></td><td class="col-sm-2"><div><label class="control-label deleteNewComponentType" data-id="'+formValue+'">×</label></div></td></tr>'
         $('#sizingTable').append(fieldHtml);    
      }
     if(type=="fixed")
      {
        var fieldHtml ='<tr><td class="col-sm-3"><div><input type="text"  name="newType['+formValue+'][name]" autofocus="autofocus" data="Name" class="empty form-control"></div></td><td class="col-sm-3"><div><input data="Work Rate" class="empty number form-control" type="text"  name="newType['+formValue+'][work_rate]" ></div></td><td class="col-sm-3"><div><input data="Exl Area" type="text" class="empty number form-control" name="newType['+formValue+'][excl_area]" ></div></td><td class="col-sm-3"><div><input data="Spread Ratio" type="text" class="empty number form-control"  name="newType['+formValue+'][spread_ratio]"></div></td><td class="col-sm-3"><div><input data="fixed area" type="text" class="empty number form-control"  name="newType['+formValue+'][fixed_area]" ></div></td><td class="col-sm-2"><div><input type="checkbox" name="newType['+formValue+'][is_default]" ></div></td><td class="col-sm-2"><div><label class="control-label deleteNewComponentType" data-id="'+formValue+'">×</label></div></td></tr>'
         $('#fixedTable').append(fieldHtml);    
         
      }
    if(type=="thickness")
      {
        var fieldHtml ='<tr><td class="col-sm-3"><div><input data="Name"  type="text" name="newType['+formValue+'][name]" autofocus="autofocus " class="empty form-control"></div></td><td class="col-sm-3"><div><input data="Work Rate" type="text" class="empty number form-control" name="newType['+formValue+'][work_rate]" ></div></td><td class="col-sm-3"><div><input type="text" data="Spread Ratio" class="empty number form-control"  name="newType['+formValue+'][spread_ratio]" ></div></td><td class="col-sm-3"><div><input type="text" data="Thickness" class="empty number form-control"  name="newType['+formValue+'][thickness]" ></div></td><td class="col-sm-2"><div ><input type="checkbox" name="newType['+formValue+'][is_default]" ></div></td><td class="col-sm-2"><div><label class="control-label deleteNewComponentType" data-id="'+formValue+'">×</label></div></td></tr>'
         $('#thicknessTable').append(fieldHtml);    
         
      }
        //formValue = formValue++;
        console.log(formValue++);
        $('.formValueNew').val(formValue++);
        e.stopPropagation();
    })
  })
</script>

<script>
 $('.saveComponentUpdate').click(function(event){
   // alert("ravi");
   // if()
   //  $('#hereThickness').remove();
   //  $('#hereFixed').remove();
   //  $('#hereSized').remove();
   //alert(($(".empty").length));
   event.preventDefault();
    //var stop = [];
    var numbers = /^[0-9.,]+$/;
    var i=0;
   
       $("#modalcomponentmain .empty").each(function() {
        if($(this).val()=="")
        {
          name = $(this).attr('data');
          //var nameId = $(this).attr('id');
          $(this).attr("style","border:1px solid #a94442 ");
           $(this).parent().parent().find('.help-block').remove();
          $(this).parent().parent().append('<div class="help-block" ><font color="#a94442">'+name+' cannot be blank.</font></div>');
          //stop.push(i) ;
          i++;
          //return false;
        }
        else
        {
          if($(this).hasClass('number')){
            if(isNaN($(this).val()))
            {
              name = $(this).attr('data');
              //var nameId = $(this).attr('id');
              $(this).attr("style","border:1px solid #a94442");
              
              $(this).parent().parent().find('.help-block').remove();
              $(this).parent().parent().append('<div class="help-block" ><font color="#a94442">'+name+' Should be a Number.</font></div>');
              
              i++;
            }
            else
            {
              var name =$(this).attr('data');
              //var nameId = $(this).attr('id');
              $(this).attr("style","border:1px solid #0ecc31");
              $(this).parent().parent().find('.help-block').remove();
            }
          }
          else{
            var name =$(this).attr('data');
            //var nameId = $(this).attr('id');
            $(this).attr("style","border:1px solid #0ecc31");
            $(this).parent().parent().find('.help-block').remove();
          }
        }
      });
    
    // alert(name);
   if(i>0)
   {
       return false;
      //alert("kant");
   }
   else
   {
    if($('#tblcomponents-tt_text').val() == ''){
      exit();
    }
      var formData = new FormData($('#dynamic')[0]);
      var form = $('#dynamic').attr('action');
      $('.loader').show();
      $.ajax({
        url: form,
        type: 'post',
        data: formData,
        processData: false,
        contentType: false,
        cache:false,
        success: function (response) {
          $('.loader').hide();
            if(response == 1){
            $("#modalcomponentmain").modal('hide');
            $(document.body).removeClass('modal-open');
         $('.modal-backdrop').remove();
            $.pjax.reload('#componentmainpjax' , {timeout : false});
          }
        },
      });
   }

 
});
</script>
<script>
  $(document).ready(function(){
    var groupId = '<?= $group_type ?>';
    var GroupIds = JSON.parse(groupId);
    var Gid = '<?= $GroupType?>';
    //alert(Gid);
    console.log(groupId);
    console.log(GroupIds[Gid]);
    var FG_ID = GroupIds[Gid];
    if(FG_ID==1 || FG_ID==3)
    {
        if($('.newAreaField').empty())
        {
          $('.newAreaField').append('<input type="text" class="form-control">');
        }
    }
    else
    {
        $('.newAreaField').empty('');
    }
})
</script>
<script>
  function showGroupId(elem)
  {
    //var groupId = {1: 3, 2: 2, 3: 1, 4: 1, 5: 2, 6: 1, 7: 2, 8: 2, 9: 2, 10: 2, 11: 2, 12: 2, 13: 1, 14: 1, 15: 2, 16: 1, 17: 2, 18: 1, 19: 2, 20: 2, 21: 2, 22: 2, 23: 2, 24: 1, 25: 1, 26: 1, 27: 2, 38: 2}
    var groupId = '<?= $group_type ?>';
    var GroupIds = JSON.parse(groupId);
    var Gid = elem.value;
    //alert(Gid);
    console.log(groupId);
    console.log(GroupIds[Gid]);
    var FG_ID = GroupIds[Gid];
    if(FG_ID==1 || FG_ID==3)
    {
        if($('.newAreaField').empty())
        {
          $('.newAreaField').append('<input type="text" class="form-control">');
        }
    }
    else
    {
        $('.newAreaField').empty('');
    }
  }

//$("div").removeClass("form-group");

</script>
<script>
  $('.deleteSaveComponentType').click(function(e){
    var id=$(this).attr('data-id');
    //alert(id);
       // id=$(this).attr('data-id');
   
    var formdata = {'id':id};
    $(this).closest("tr").remove();
    $.ajax({
        url:'deletecomponenttype' ,
        type: 'post',
        data: formdata,
        async:false,
        // processData: false,
        // contentType: false,
        // cache:false,
        success: function (response) {
        //console.log(response);              
            if(response == 1){
             //alert('success');
              
              
           }
         },
    });
  e.stopImmediatePropagation();
  return false;
})
 

  
$(document).on('click','.deleteNewComponentType',function(){
  //var id= $(this).attr('data-id');
   $(this).closest("tr").remove();
  
})
$(document).on('click','.deleteComponentType',function(){
  //var id= $(this).attr('data-id');
   $(this).closest("tr").remove();
})

$( ".row_position2" ).sortable({ axis: 'y' });
$( ".row_position2" ).sortable({
    delay: 150,
    stop: function() {
        var selectedData = new Array();
        $('.row_position2>table').each(function() {
            //selectedData.push($(this).attr("id"));
        });
       //  updateOrder(selectedData);
    }
})



</script>