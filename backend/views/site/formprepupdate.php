<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\tblPrepLevel */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
h1{
  display:none;
}
</style>

<div class="tbl-prep-level-form form-inner-head">

    <?php $form = ActiveForm::begin([ 'validationUrl' => Url::toRoute(['validateprepupdate?id='.$id]),'enableAjaxValidation' => true, 'id' => 'myid22']); ?>
     <div class = "modal-body-div-add prepClassUpdate">
     <h2><?php echo $model->prep_level;?></h2>
         <?= Html::submitButton('Save', ['class' => 'btn btn-success pull-right','id'=>'closeprepupdate']) ?>
      <button type="button" class="close pull-right glyphicon glyphicon-remove" data-dismiss="modal" aria-hidden="true"></button>
    </div>

    <?= $form->field($model, 'prep_level')->textInput(['maxlength' => true]) ?>

   
    <?= $form->field($model, 'uplift_cost', [
            'template' => '<strong>% uplift to paint cost</strong><div class="input-group">{input}
            <span class="input-group-addon">%</span></div>{error}{hint}'
        ])->textInput(); ?>


    <?= $form->field($model, 'uplift_time', [
            'template' => '<strong>% uplift to paint time</strong><div class="input-group">{input}
            <span class="input-group-addon">%</span></div>{error}{hint}'
        ])->textInput(); ?>

    


    <?= $form->field($model, 'is_default')->checkbox(array('label'=>''))
            ->label('Is Default'); ?>



    <?php ActiveForm::end(); ?>

</div>

<script>
$('body').on('beforeSubmit', '#myid22', function (e) {
  var pathname = window.location.href;  
     var form = $(this);
     
     $.ajax({
          url: form.attr('action'),
          type: 'post',
          data: form.serialize(),
          success: function (response) {
              

               console.log(response);              

               if(response == 1){
                 
               
                 $("#modalprep").modal('hide');
                 $('.modal-backdrop').remove();
                 $.pjax.reload('#preppjax' , {timeout : false});
              

               }
          },
         
        }
     );
      e.stopImmediatePropagation();
     return false;
});


// $(document).ready(function()
// {
//     $("#closeprepupdate").on("click", function()
//     {
//         $("#modalprep").modal('hide');
//     }
//     );
// });


// $(document).ready(function()
// {
//     $("#closeprep").on("click", function()
//     {
//         $("#modalprep2").modal('hide');
//     }
//     );
// });





</script>

