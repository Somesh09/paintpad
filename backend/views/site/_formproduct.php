<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\TblBrands;
use app\models\TblSheen;
use yii\helpers\Url;
use wbraganca\dynamicform\DynamicFormWidget;
/* @var $this yii\web\View */
/* @var $model app\models\TblProducts */
/* @var $form yii\widgets\ActiveForm */
$modelsheen = TblSheen::find()->all();
?>
<style>
/*#modal2{
    width: 850px;
    height:700px;
    margin: 0px auto;
   overflow: scroll;
}*/

</style>
<div>
<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'], 'id' => 'myid2']); ?>
<div class="form-inner-head" >

    
    
   
    <div class = "modal-body-div-add">
    <h2>Create Product</h2>
      <?php echo Html::submitButton('Save', ['id'=>'closeproduct','class'=>'btn btn-success pull-right saveProduct']) ?>
      <button type="button" class="close glyphicon glyphicon-remove" data-dismiss="modal" aria-hidden="true"></button>
    </div>
  
  
  
  <div class="div-scroll-product"> 
  
  <div class="main-div-upimgdiv">
    <div class="upImgDiv">
  <div class="form-group">
      <div class="imgg_div">

    <!-- <div class="imggCnt">-->
    <!-- <img id="blah" src="#" alt="your image" width="150" height="120" /> -->
        <img id="blah" class="addImageProduct" src="uploads/no_image.png" alt="your image" />
        
    <span class="opnImg"><img id="blah21" src="uploads/edit.png" alt="your image" />
    
    </span>
        <?php echo $form->field($model,'image',['inputOptions'=>['name' => 'image','id'=>'imgInp','required'=>'true']])->fileinput()->label(false);?>      
    <!--</div>-->
    <p class="img-size"> Select Image max 2Mb</p>
      </div>
      
      </div>
    </div>
  
  
  
  
  

    <div class="bttmDiv">
      
      <?php echo $form->field($model, 'name')->textInput(['maxlength' => true,'label'=>false]) ?>

      <?php echo $form->field($model, 'spread_rate')->textInput() ?>

      <?php echo $form->field($model,'brand_id')->dropDownList(ArrayHelper::map(TblBrands::find()->all(),'brand_id','name'),['prompt'=>'Select brand']) ?>
     <?php $model->type_id = '3'; ?>
      <?php echo $form->field($model, 'type_id')->radioList(array('1'=>'Interior','2'=>'Exterior','3'=>'Both')); ?>

    </div>
  
    </div>


   <!--  <span id="addStock" class="btn btn-primary" >ADD STOCK </span>
     -->
   
   
   <div class="one-stoke">
   
  <div class="stoke-main">
  <!-- <h3 align="center">UPDATE STOCK</h3> -->
  <h3 align="center">Stock</h3>
   <a  href="javascript:void(0)" class='clicked btn btn-success pull-right' >Add Stock Item</a>
  </div>
    <table class="table" id="maindiv77">
      <tr>
        <th>Sheen</th>
        <th>Litre</th>
        <th>Code</th>
        <th>Name</th>
        <th>Price</th>
        <th>&nbsp;</th>
      </tr>
    </table>
    <div>
      </div>
      
  <div id="choosingdiv" style="width:23%; margin-right:0px; border:1px;padding-left: 15px;">
  <span ><?php echo $form->field($modelstock,'sheen_id')->dropDownList(ArrayHelper::map(TblSheen::find()->all(),'sheen_id','name'),['prompt'=>'--Add New Item--','id'=>'dropdown'])->label(false); ?></span>
</div>
  
  
    </div>
  
  </div>
  
  </div>
</div>
  
<div class="loader" style="display:none;"></div>


<script src="/app/admin/js/compressor.min.js"></script>

<script>

    
  $('.clicked').click(function(){
      showDiv(1);
      $("#dropdown").val('');
  })
 
 $('#dropdown').change(function(){
    var id = $(this).val();
    //alert(id);
    showDiv(id);
    $("#dropdown").val('');
 })


  function showDiv(id)
  {
          //$('#maindiv77').hide();

          var  l = $('#maindiv77').find('tr').length;
         // alert(l);
          //var id=elem.value;
         
          // alreadySheen=[];
          // $("#maindiv77").children('table').each(function () {
          // alreadySheen.push($(this).find('input').attr('id'));
          // });
          // lastArr = alreadySheen[alreadySheen.length - 1];        
          // for (var i = alreadySheen.length ; i > 0; i--) 
          // {
          // console.log(id);
          // $("#dropdown option[value='"+lastArr+"']").remove();
        
          // }
// <input id="'+id+'" type="text" name="stock_sheen['+id+']"  value="'+name+'" readonly style="width:45%;margin-right:0px; border:1px;"/>
          var name = $("#dropdown option:selected").text();
          //var nameSheen = ""
          
          var fieldHTML = '<tr class="table'+id+'" table_id="'+id+'"><td><select name="stock_sheen['+l+'][stock]" class="pages form-control" value="'+id+'" data-id="'+l+'"><?php foreach($modelstockData as $stock):?><option value="<?php echo $stock->sheen_id?>"><?php echo $stock->name;?></option> <?php endforeach?></select></td><td><input data-id="'+l+'"  type="text" data="litre" class="litre empty number form-control"  name="stock_sheen['+l+'][litre]" id="litre'+id+'"  ></td><td><input data-id="'+l+'" data="code" class="code  form-control empty" type="text" id="code'+id+'" name="stock_sheen['+l+'][code]"   ></td><td><input data-id='+l+' class="name empty form-control" data="name" type="text" name="stock_sheen['+l+'][name]"  id="name'+id+'"  ></td><td><input data-id='+l+' type="text" name="stock_sheen['+l+'][price]" data="price" class="price empty number form-control"  id="price'+id+'" ></td><td class="deleteform" data-id="'+l+'" readonly ><button type="button" class="glyphicon glyphicon-remove close" ></button></td></tr><tr class="tdError tdError'+l+'"><td>&nbsp;</td><td><span style="color:#a94442;" class="litre'+id+'message"></span></td><td><span style="color:#a94442;" class="code'+id+'message"></span></td><td><span style="color:#a94442;" class="name'+id+'message"></span></td><td><span style="color:#a94442;" class="price'+id+'message"></span></td><td>&nbsp;</td></tr>'; 
          $('#maindiv77').show();
         // var newOption = $('<option value="'+id+'">'+name+'</option>');

          //$('#pages').append(newOption);
         // $("#pages option:selected").text(name);
          console.log(id+""+name);
         

          if(id!="" && name!="Select brand")
          {
            $('#maindiv77').append(fieldHTML);
          }        
          if ($(".pages").length > 0) {
            $(".pages").last().val(id);
          }
         else
          {
           // alert("d");
            $(".pages").val(id);
          }
         $(".div-scroll-product").animate({ scrollTop: $(document).height() }, 1000);

          $('.pages').change(function(){
            var sid = $(this).val();
           var dataStock = $(this).parent().parent().parent().find('.litre').attr("data-id");
            $(this).removeAttr("name");
            $(this).removeAttr("value");
            $(this).attr("name",'stock_sheen['+dataStock+'][stock]')
            $(this).attr("value",sid);
            $(this).parent().parent().parent().find('.litre').removeAttr("name");
            var dataLitre = $(this).parent().parent().parent().find('.litre').attr("data-id");
            $(this).parent().parent().parent().find('.litre').attr("name","stock_sheen["+dataLitre+"][litre]");
            $(this).parent().parent().parent().find('.code').removeAttr("name");
            var dataCode = $(this).parent().parent().parent().find('.code').attr("data-id");
            $(this).parent().parent().parent().find('.code').attr("name","stock_sheen["+dataCode+"][code]");
            $(this).parent().parent().parent().find('.name').removeAttr("name");
            var dataName = $(this).parent().parent().parent().find('.name').attr("data-id");
            $(this).parent().parent().parent().find('.name').attr("name","stock_sheen["+dataName+"][name]");
            $(this).parent().parent().parent().find('.price').removeAttr("name");
            var dataPrice = $(this).parent().parent().parent().find('.price').attr("data-id");
            $(this).parent().parent().parent().find('.price').attr("name","stock_sheen["+dataPrice+"][price]");
             //alert(litre);
           // $(this)
          })
          //$('.modal-body').scrollTop($('.modal-body').prop("scrollHeight"));
  }

   $(document).on('click','.deleteform',function(){
    var id= $(this).parent().parent().parent().attr('table_id');
    //alreadySheen.push(id);
    var data = $(this).attr('data-id');
    $(this).closest('tr').remove();
    $('.tdError'+data).remove();
  });


</script>

<script>
var CompImage = ''
  //$("#blah").hide();
  function readURL(input) {
 
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      $("img").show();
      $('#blah').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
    //myy
    const file = input.files[0];
    // alert(file.size);
    new Compressor(file, {
        quality: 0.6,
        success(result) {
          const formData = new FormData();

          // The third parameter is required for server
          // formData.append('file', result, result.name);
          CompImage = result
          // console.log(CompImage)
        },
        error(err) {
          console.log(err.message);
        },
      });
    //myy end
  }
}

$("#imgInp").change(function() {
  var maxSizeKB = 2000; //Size in KB
  var maxSize = maxSizeKB * 1024; //File size is returned in Bytes
  if (this.files[0].size > maxSize) {
    $(this).val("");
    alert("Please select Image of 2mb or less");
    return false;
  }
  readURL(this);
});
</script>

<script>
 $('.saveProduct').click(function(event){
   //alert("ravi");
   // if()
   //  $('#hereThickness').remove();
   //  $('#hereFixed').remove();
   //  $('#hereSized').remove();
   //alert(($(".empty").length));
  event.preventDefault();
  //var stop=[];
  var numbers = /^[0-9.,]+$/;  
  var i=0;
  var inputLoop = 1;
  var inputLoopError = 0;
  if ($('#imgInp').val() == '') {
    alert('Image is Empty')
    return;
  }
  if ($('#tblproducts-name').val() == '') {
    alert('Name is Empty')
    return;
  }
  if ($('#tblproducts-spread_rate').val() == '') {
    alert('Spread Rate is Empty')
    return;
  }
  if ($('#tblproducts-brand_id').val() == '') {
    alert('Select Brand')
    return;
  }
  var stockVal = $('#maindiv77 tr[table_id]').length
  if(stockVal == 0){
    alert('Add product stock')
    return;
  }
  $("#myid2 .empty").each(function() {
    if($(this).val()=='')
    {
      name = $(this).attr('data');
      console.log(name)
      var nameId = $(this).attr('id');
      console.log(nameId)
      $(this).css('background-color', 'red');
      $(this).css('color', 'red');
      $(this).attr("style","border:1px solid #a94442 ");
      $('.'+nameId+'message').empty();
      $('.'+nameId+'message').html(name+' cannot be empty');
      //stop.push(i) ;
      i++;
      //return false;
      
      //$(this).parent().next().show();
      inputLoopError++;
    }
    else if($(this).val()==0 || $(this).val()=="0")
    {
      name = $(this).attr('data');
      var nameId = $(this).attr('id');
      $(this).attr("style","border:1px solid #a94442 ");
      $('.'+nameId+'message').empty();
      $('.'+nameId+'message').html(name+' cannot be 0 ');
      i++;
      inputLoopError++;
    }
    else
    {
      if($(this).hasClass('number')){
        if(isNaN($(this).val()))
        {
          name = $(this).attr('data');
          var nameId = $(this).attr('id');
          $(this).attr("style","border:1px solid #a94442");
          
          $('.'+nameId+'message').empty();
          $('.'+nameId+'message').text(name+' should be a number');
          //$(this).parent().next().show();
          inputLoopError++;
          i++;
        }
        else
        {
          var name =$(this).attr('data');
          var nameId = $(this).attr('id');
          $(this).attr("style","border:1px solid #0ecc31");
          $('.'+nameId+'message').empty();
        }
      }
      else{
        var name =$(this).attr('data');
        var nameId = $(this).attr('id');
        $(this).attr("style","border:1px solid #0ecc31");
        $('.'+nameId+'message').empty();
      }
    }
    

      inputLoop = 1;
      if(inputLoopError > 0){
        $(this).closest('tr').next('tr').show();
      }
      else{
          $(this).closest('tr').next('tr').hide();
      }
      inputLoopError = 0;

  });
    if(i>0)
  {
    //$(".tdError").show();
    return false;
    //alert("kant");
  }
  else{
    //return false;
        //$('#modal2').css('opacity','0.5');
        $('.loader').show();
        //$("#voltaic_holder").css("position", "relative");
        var pathname = window.location.href;
        var formData = new FormData($('#myid2')[0]);
          // console.log(CompImage)
          
          formData.delete('image');
          formData.append('image', CompImage, CompImage.name);
          // console.log(formData.get('image'))
        // return false;
        var form = $('#myid2').attr('action');
        $.ajax({
            url: form,
            type: 'post',
            data: formData,
            processData: false,
            contentType: false,
            cache:false,
            success: function (response) {
            //console.log(response);              
                if(response == 1){
                $('.loader').hide();
                $("#modal2").modal('hide');
                $(document.body).removeClass('modal-open');
                $('.modal-backdrop').remove();
                $.pjax.reload('#productajax' , {timeout : false});
               }
             },
             error: function (response) {
              $('.loader').hide();
             },
        });
  }
 });
 $('#imgInp').hide();
</script>


<script>
   $('.opnImg').click(function(){
    $("#imgInp").click()
  })
</script>
<?php ActiveForm::end(); ?> 
