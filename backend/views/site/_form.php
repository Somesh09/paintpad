<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
?>
<div class="tbl-brands-form form-inner-head">
  <?php $form = ActiveForm::begin(['validationUrl' => Url::toRoute(['validatebrand']),'enableAjaxValidation' => true, 'id' => 'myid1']); ?>
  <div class="modal-body-div-add">
  <h2>Add New Brand</h2>
  <?= Html::submitButton('Save', ['class' => 'btn btn-success','id'=>'closebrand']) ?>
  <button type="button" class="close glyphicon glyphicon-remove" data-dismiss="modal" aria-hidden="true"></button>
</div>
  <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
  <?php $model->enabled = '1';?>
  <?= $form->field($model, 'enabled')->radioList(array('0'=>'YES','1'=>'NO')); ?>

  <?php ActiveForm::end(); ?>
</div>


<script>
$('body').on('beforeSubmit', '#myid1', function (e) {
  var pathname = window.location.href;
  var form = $(this);
  $.ajax({
    url: form.attr('action'),
    type: 'post',
    data: form.serialize(),
    success: function (response) {
      if(response == 1){
        $("#modal").modal('hide')
        $.pjax.reload('#my-grid-pjax' , {timeout : false});
      }
    },
  });
  e.stopImmediatePropagation();
  return false;
});
</script>

