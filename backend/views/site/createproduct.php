<?php
// phpinfo();die;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TblProducts */

//$this->title = 'Create Tbl Products';
$this->params['breadcrumbs'][] = ['label' => 'Tbl Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-products-create">



    <?php echo $this->render('_formproduct', [
        'model' => $model,
         'modelstock'=>$modelstock,
         'modelstockData'=>$modelstockData,

    ]) ?>

</div>
