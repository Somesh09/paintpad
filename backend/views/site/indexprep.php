<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\widgets\Pjax;
use dosamigos\grid\GridView;


/* @var $this yii\web\View */
/* @var $searchModel app\models\tblPrepLevelSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


?>

<div class="tbl-prep-level-index">



<?php Pjax::begin(['timeout' => 5000,'id'=>'preppjax']



);?>

<?php echo  Html::button('<img src="uploads/add.png"width="17" height="17"/>', ['value'=>Url::to( Url::base().'/createprep'), 'class' => 'btn btn-primary pull-right','id'=>'modalPrep']); ?>
  
<?php 
  Modal::begin([
  'header'=>'',
  'id'=>'modalprep',
  'size'=>'modal-lg',
  ]);
  echo "<div id='modalContent3'><div style='text-align:center'><img src='uploads/avatar2.gif' height='100' width='100'></div></div>";
  Modal::end();
?>


<?php  echo $this->render('_searchprep', ['model' => $searchModelprep]); ?>



<div class="main-table-index-page">
    <?= GridView::widget([
        //'id' =>'no-js-grid2',
        'dataProvider' => $dataProviderprep,
        //'filterModel' => $searchModelprep,
         'rowOptions'   => function ($model, $key, $index, $grid) {
              return  [
                           "class"=>"modalButton00007",'value'=>Url::to(Url::base().'/updateprep?id='.$model->prep_id)
                          ];
                },
        'behaviors' => [
            [
                'class' => '\dosamigos\grid\behaviors\LoadingBehavior',
                'type' => 'spinner'
            ]
        ],
        //'Pjax'         =>true,

        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            //'prep_id',
            //'prep_level',
            ['attribute'=>'Prep Level',
            'value'=>function($model) {

                $level = '';

                if($model->is_default == 1){
                  $level = $model->prep_level.' (Default)';
                }else{
                  $level = $model->prep_level;
                }
                return $level;
            },
            'filter'=>false],



            ['attribute'=>'% Uplift to Paint Cost',
            'value'=>'uplift_cost',
            'filter'=>false],


            ['attribute'=>'% Uplift to Paint Time',
            'value'=>'uplift_time',
            'filter'=>false],
            //'is_default',
            //'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn',


                'header' => 'UPDATE',
                 'headerOptions' => ['style' => 'color:#337ab7'],
                 'template' => '{update}',
                 'buttons' => [
            

                 'update' => function ($url, $model,$id) {
                             return Html::a('<span class=""><img src="uploads/update.png" width="13" height="13"/></span>', $url, [
                             "class"=>"modalPrep2",'value'=>Url::to(Url::base().'/updateprep?id='.$model->prep_id)
                              ]);
                  },
            

               ],
                 'urlCreator' => function ($action, $model, $key, $index) {
            

                  Url::to(['updateprep?id='.$model->prep_id],['id'=>'modal'])   ;         
                //$url ='update?id='.$model->brand_id;
              //return $url;
                 
                 //return Html::a($url, ['title' => 'view']);
                
                
            
            

          }


        ],
        ],
    ]); 
    ?>
  </div>
    <div class="prepDisplayPage"></div>
</div>
 
 <script>
  $(function()
{
  //
  $('#modalPrep').click(function(){
    $('#modalprep').modal('show')
      .find('#modalContent3')
      .load ($(this).attr('value'), function(){
        // $('#modalprep .modal-header').remove();
      });
  })
});


$(function()
{
  //
  $('.modalPrep2').click(function(){
    $('#modalprep').modal('show')
      .find('#modalContent3')
      .load ($(this).attr('value'));


  })
});

$(function()
{
  //
  $('.modalButton00007').click(function(){
    $('#modalprep').modal('show')
      .find('#modalContent3')
      .load ($(this).attr('value'), function(){
        var prepp = $('#prepname').val();
        // $('#modalprep .modal-header').remove();
      });
  })
});



$(function() { //equivalent to document.ready
  $(".form-control").focus();
});


$('.form-control').focus(function(){
  var that = this;
  setTimeout(function(){ that.selectionStart = that.selectionEnd = 10000; }, 0);
});



$('#tblpreplevelsearch-prep_level').bind('keyup', function() { 
    $('#tblpreplevelsearch-prep_level').delay(200).submit();
});


</script>
<?php $this->registerJs('jQuery("#w3").on("keyup", "input", function(){
                jQuery(this).change();
                 $("#modalprep").remove();
                //OR $.pjax.reload({container:\'#w3\'});
        });',
        yii\web\View::POS_READY);
        ?> 
        <script>
var name = $('.summary').html();
$('.summary').remove();
$('.prepDisplayPage').html(name);
if($('.prepDisplayPage').text()=='undefined')
{
   $('.prepDisplayPage').hide();
}
</script>
<?php Pjax::end();?>


