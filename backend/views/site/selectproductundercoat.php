<?php

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use yii\widgets\Pjax;
use yii\helpers\Url;
//use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\TblBrands;
use dosamigos\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TblProductsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

// $this->title = 'Products';
// $this->params['breadcrumbs'][] = $this->title;
//echo "<pre>";
//print_r($model2);

?>
<style>
.pagePagination1
{
  background:#000; color:#FFF;
}
.activePagination{
  background:#000; color:#FFF;
}
</style>
<span class="totalPAGE" style="display:none "><?php echo $total;?> </span>
<!-- <input type="hidden" value="<?php// echo $id;?>" class="getId"> -->
<input type="text" class="form-control" id="searchProductName" style="width: 95%;margin: 10px auto !important;" placeholder="Search/filter">
<div style="width: 570px;overflow-y: auto;overflow-x: hidden;margin: 0 auto;max-height: 520px;">
<div class="loader" style="display:none"></div>
 <table class="table" id='example'>
    <tr>
<?php

foreach($model2 as $model)
{
  ?>
  
   <tr class="tosearch">
        <td>
          <p class="productDisplay" data-id="<?php echo $model['product_id'];?>"><?php echo $model['name'];?></p>
        </td>
    </tr>      
  <?php
}
?>
</tr>
    </table>
    <div class="paginationSelectProduct">
    <?php
      for($i=1;$i<=$total;$i++)
      {
        echo "<button type='button' class='changePage pagePagination".$i."' data-id='".$i."' >".$i."</button>";
      }
    ?>
  </div>



  </div>
<script>
// $('.productDisplay').click(function(){
//   alert($(this).attr('data-id'));
// })
</script>
<script>

  $(document).ready(function(){
    // alert('rk')
    $(document).on('click', '.productDisplay', function(e){  
      var id=$(this).attr('data-id');
      var name =$(this).text();
     // var pid = $('.getId').val();
       var fieldHTMLS = '<li id="undercoat'+id+'" class="'+id+'" data-under-coat="'+id+'"><div></div><div><tr><td><input  type="checkbox" name="under_coatEnabled['+id+']" class="my-checkbox sheenProductCheck underCoatCheck"></td><td><p class="sheenProductList">'+name+'</p><input data-under-coat="'+id+'" type="hidden" name="under_coat['+id+']"  value="'+name+'" readonly  /></td></tr><span class="abc">&times;</span></div></li>';

      //$('.alert_'+pid).append(fieldHTML);
     // alert('rk')
      $('ul.undercoat').append(fieldHTMLS);
      //$("#modaldiv").modal('hide');
      $('#myModal1').hide();
      e.stopImmediatePropagation();  
      return false;
    })
  })
</script>
<script>
// $(document).ready(function(){
//   $("#searchProductName").on("keyup", function(){
//     $('.noAppointment').remove();
//     var searchClient = $(this).val();
//     //console.log(searchClient);
//     searchClient = searchClient.toLowerCase();
//     if(searchClient != ""){
//       $('.tosearch').show();
//       var flag=0;
//       $(".productDisplay").each(function(){
//         var thisHtml = $(this).text();
//         thisHtml = thisHtml.toLowerCase();
//        // console.log(thisHtml);
//         if(thisHtml.indexOf(searchClient) < 0 ){
//           $(this).closest("tr").hide();
//         }
//         else
//         {
//           flag=1;
//         }
//       });
//       if(flag ==0)
//       {
//         var fieldHtml="<li class='alert alert-danger noAppointment'>NO Contacts</li>";
//         $('.contact_list').append(fieldHtml);
//       }
//     }
//     else
//     {
//       $('.tosearch').show();
//     }
//   })
// })
</script>
<script type="text/javascript">
  function selectproductUndercoat(id,searchProduct,ap){
    $('.loader').show();
    // var mid = $('.getId').val();
    var mid=$(this).attr('data-id');
    var array = "<?php echo $array;?>"
    var data = {'page':id,'sp':searchProduct};  
    $.ajax({
      url: 'selectproductundercoat?&array='+array,
      type: 'post',
      data: data,
      success: function (response) {
          

           console.log(response);              

           if(response){
            //$('.pagination').removeClass('pagePagination1')
              var response = response;
              var fieldHtml =  $(response).find('#example').html();
              var pagination = $(response).find('.paginationSelectProduct').html();
              $('#example').html(fieldHtml);
              $('.paginationSelectProduct').html(pagination);
              $('.loader').hide();
              $(response).find('.pagePagination1').removeClass('pagePagination1')
              $(response +'.pagePagination1').removeClass('pagePagination1');
              $('.pagePagination'+ap).addClass('activePagination');
              if($('.activePagination').length>0)
              {
                $('div').find('.pagePagination1').removeClass('pagePagination1');
              }

           }
      },
     
    });
  }

</script>
<script>
    $("#searchProductName").on("keyup", function(){
        var searchProduct = $(this).val();
        searchProduct = searchProduct.toLowerCase();
        selectproductUndercoat(1,searchProduct,1);
      })

</script>
<script>
$(document).on('click', '.changePage', function(e){  
  //$('.pagination').addClass('loader')
  var ap = $(this).text();
  //alert(ap);
  var searchProduct = $('#searchProductName').val();
  searchProduct = searchProduct.toLowerCase();
  $('.paginationSelectProduct').find('.pagePagination1').removeClass('pagePagination1');
  $('.paginationSelectProduct').find('.activePagination').removeClass('activePagination');
  $(this).addClass('activePagination');
  var id = $(this).attr('data-id');
  selectproductUndercoat(id,searchProduct,ap);

  e.stopImmediatePropagation();
  return false;  
})
</script> 

        