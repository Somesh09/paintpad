<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TblSheen */

//$this->title = 'Update Tbl Sheen: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Tbl Sheens', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->sheen_id]];
$this->params['breadcrumbs'][] = 'Update';
?>

<input type="hidden" id="shnname" value="<?php echo $model->name;?>">
<div class="tbl-sheen-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_formsheenupdate', [
        'model' => $model,
        'id'=>$id
    ]) ?>

</div>
