<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\widgets\Pjax;
use dosamigos\grid\GridView;


/* @var $this yii\web\View */
/* @var $searchModel app\models\Tbl_rooms_typesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


?>
<div class="tblroomtypes-index">


    <?php Pjax::begin(['timeout' => 5000,'id'=>'roompjax']



); ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="jquery.js"></script> 



   <?php  echo  Html::button('<img src="uploads/add.png"width="17" height="17"/>', ['value'=>Url::to( Url::base().'/createroom'), 'class' => 'btn btn-primary pull-right','id'=>'modalRoom']);?>


  <?php  echo Html::button('Sort Room Type', ['value'=>Url::to( Url::base().'/sortroom'), 'class' => 'btn btn-primary pull-right','id'=>'modalButtonsortroom']);?>


         <?php 
         Modal::begin([
          'header'=>'',
          'id'=>'modalroom',
          'size'=>'modal-sl',
          ]);
           echo "<div id='modalContent4'><div style='text-align:center'><img src='uploads/avatar2.gif' height='100' width='100'></div></div>";
           Modal::end();
     
     ?>
<div class="main-table-index-page">
<?php  echo $this->render('_searchroom', ['model' => $searchModelrooms]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProviderrooms,
        //'filterModel' => $searchModelrooms,
        'rowOptions'   => function ($model, $key, $index, $grid) {
              return  [
                           "class"=>"modalRoom3",'value'=>Url::to(Url::base().'/updateroom?id='.$model->room_id)
                          ];
                },
        'behaviors' => [
            [
                'class' => '\dosamigos\grid\behaviors\LoadingBehavior',
                'type' => 'spinner'
            ]
        ],
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            //'room_id',
            'name',
            //'type_id',
            //'multiples',
            //'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn',

                 'header' => 'UPDATE',
                 'headerOptions' => ['style' => 'color:#337ab7'],
                 'template' => '{update}',
                 'buttons' => [
            

                 'update' => function ($url, $model,$id) {
                             return Html::a('<span class=""><img src="uploads/update.png" width="13" height="13"/></span>', $url, [
                             "class"=>"modalRoom2",'value'=>Url::to(Url::base().'/updateroom?id='.$model->room_id)
                              ]);
                  },
            

               ],
                 'urlCreator' => function ($action, $model, $key, $index) {
            

                  Url::to(['updateroom?id='.$model->room_id],['id'=>'modal'])   ;         
                //$url ='update?id='.$model->brand_id;
              //return $url;
                 
                 //return Html::a($url, ['title' => 'view']);
                
                
            
            

          }

        ],
        ],
    ]); ?>
  </div>
    <div class="roompageDisplay"></div>
</div>

<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
 --><script>
  $(function()
{
  //
  $('#modalRoom').click(function(){
    $('#modalroom').modal('show')
      .find('#modalContent4')
      .load ($(this).attr('value'), function(){
        $('#modalroom .modal-header').remove();
      });
  })
});


$(function()
{
  //
  $('.modalRoom2').click(function(){
    $('#modalroom').modal('show')
      .find('#modalContent4')
      .load ($(this).attr('value'));
  })
});
$(function()
{
  //
  $('.modalRoom3').click(function(){
    $('#modalroom').modal('show')
      .find('#modalContent4')
      .load ($(this).attr('value'),function(){


        var rmm = $('#rmmname').val();
        $('#modalroom .modal-header').remove();
      });
  })
});

 $(function()
{
  //
  $('#modalButtonsortroom').click(function(){
    $('#modalroom').modal('show')
      .find('#modalContent4')
      .load ($(this).attr('value'), function(){
        $('#modalroom .modal-header').remove();
      });
  })
});



$(function() { //equivalent to document.ready
  $(".form-control").focus();
});


$('.form-control').focus(function(){
  var that = this;
  setTimeout(function(){ that.selectionStart = that.selectionEnd = 10000; }, 0);
});


$('#tbl_rooms_typessearch-name').bind('keyup', function() { 
    $('#tbl_rooms_typessearch-name').delay(200).submit();
});

</script>
  <?php $this->registerJs('jQuery("#w5").on("keyup", "input", function(){
                jQuery(this).change();
                 $("#modalroom").remove();
                //OR $.pjax.reload({container:\'#w5\'});
        });',
        yii\web\View::POS_READY);
        ?> 
        <script>
var name = $('.summary').html();
$('.summary').remove();
$('.roompageDisplay').html(name);
if($('.roompageDisplay').text()=='undefined')
{
   $('.roompageDisplay').hide();
}
</script>
<?php Pjax::end();?>

