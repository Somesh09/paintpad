<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\TblColorTags;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model app\models\TblColorsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-colors-search tbl-search-input-all">

   <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
        'data-pjax' => 1
        ],
    ]);


     ?>

   
<div style="width:23%; margin-right:0px; border:1px;">
    <?= $form->field($model, 'name')->textInput(['placeholder' => "Search Colors"])->label(false); ?>
</div>
  <div style="width:43%;  border:1px; margin-left:340px; margin-top: -49px">
    <?=
$form->field($model, 'tag_id')
     ->dropDownList(
            ArrayHelper::map(TblColorTags::find()->asArray()->all(), 'tag_id', 'name'),['prompt'=>'---Sort By Tag---']
            )->label(false)
?>
</div>
    

    

   
    <?php ActiveForm::end(); ?>

</div>
