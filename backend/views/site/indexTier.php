<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use dosamigos\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TblTiers_Search */
/* @var $dataProvider yii\data\ActiveDataProvider */



?>

 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
#modalContenttier{
    width: 850px;
    height:700px;
    margin: 0px auto;
     overflow: scroll;
}


/* .tbl-tiers-index .modal-dialog.modal-lg {
	width: 1500px;
} */
.tbl-tiers-index .modal-dialog.modal-lg {
	width: 85%;
	padding: 0;
}
.tbl-tiers-index .modal-dialog.modal-lg #modalContenttier {width: auto;height: 700px;margin: 0px auto;overflow: inherit;}
/* .tbl-tiers-index .modal-dialog.modal-lg #modalContenttier #tier.right {
	float: right;
	width: 1250px;
	height: 700px;
	overflow-y: scroll;
	overflow-x: hidden;
} */
.tbl-tiers-index .modal-dialog.modal-lg #modalContenttier #tier.right {
	display: inline-block;
	width: 100%;
	height: 700px;
	overflow-y: scroll;
	overflow-x: hidden;
	padding-left: 210px;
}

.tbl-tiers-index .modal-dialog.modal-lg #modalContenttier #tier .tiercoat.pull-left input {/*width: 95% !important;*/float: left;background: transparent;box-shadow: none;font-weight: bold;}
.tbl-tiers-index .modal-dialog.modal-lg #modalContenttier #tier .tiercoat.pull-left ul li input {
	width: auto !important;
	margin-right: 5px;
	font-weight: normal;
	float: none;
	display: inline-block;
	margin-top: 8px;
}

.tbl-tiers-index .modal-dialog.modal-lg #modalContenttier #tier .tiercoat.pull-left span {
	position: relative;
	top: 4px;
	font-size: 20px;
  color: #9a9a9a;
	font-weight: bolder;
}

.tbl-tiers-index .modal-dialog.modal-lg #modalContenttier #tier .tiercoat.pull-left ul li span {position: relative;top: 3px;font-size: 23px;font-weight: normal;right: -2px;float: right;font-weight: bolder !important;}
.tbl-tiers-index .modal-dialog.modal-lg #modalContenttier #tier .tiercoat.pull-left ul li:hover {background: #e8e8e8;}
.tbl-tiers-index .modal-dialog.modal-lg #modalContenttier #tier .tiercoat.pull-left ul li {border-bottom:1px solid #C8C8C8;}

.tbl-tiers-index .modal-dialog.modal-lg #modalContenttier #tier .tiercoat.pull-left {width: 50% !important; margin-right: 14px; min-height:150px;}
.tbl-tiers-index .modal-dialog.modal-lg #modalContenttier #tier .tiercoat.pull-left ul {
	width: 100%;
	padding: 0;
	text-align: right;
	border-top: 1px solid #ccc;
	margin-bottom: 0;
	margin-top: 0;
	float: left;
}
.tbl-tiers-index .modal-dialog.modal-lg #modalContenttier #tier .undercoats {width: 48% !important;}

.tbl-tiers-index .modal-dialog.modal-lg #modalContenttier #tier .undercoats h5 {width: auto;float: left;font-weight: bold;}
.tbl-tiers-index .modal-dialog.modal-lg #modalContenttier #tier .undercoats span.addproduct2 {position: relative;top: 0;font-size: 25px;font-weight: bolder;}

.tbl-tiers-index .modal-dialog.modal-lg #modalContenttier #tier .undercoats .undercoat {width: 100%;float: left;text-align: left;line-height: 22px;padding: 0;position: relative;}

.tbl-tiers-index .modal-dialog.modal-lg #modalContenttier #tier .undercoats .undercoat li > input:nth-child(1) {float: none;border: 0;position: relative;background: transparent;width: 350px;}
.tbl-tiers-index .modal-dialog.modal-lg #modalContenttier #tier .undercoats .undercoat input:nth-child(2) {float: left; margin-right: 10px; position: absolute; right: 10px;}
.tbl-tiers-index .modal-dialog.modal-lg #modalContenttier #tier .undercoats .undercoat span {float: right;position: relative;top: 0px;font-size: 23px;font-weight: normal;right: 0px;font-weight: bold; color: #9a9a9a}
.tbl-tiers-index .modal-dialog.modal-lg #modalContenttier #tier .undercoats .undercoat li {float: left;width: 100%;border-bottom: 1px solid #C8C8C8;padding: 5px;}
.tbl-tiers-index .modal-dialog.modal-lg #modalContenttier #tier .undercoats .undercoat li:hover {background: #e8e8e8;}
.tbl-tiers-index .modal-dialog.modal-lg #modalContenttier #tier .undercoats .undercoat li p{width: auto;float: left;margin: 0;}


/*********************************** 21-11-2018 **********************************************/
.form-control {color: #000 !important;}
.tiercoat.pull-left .table {margin-bottom: 0 !important;}

.tiercoat.pull-left .table th:nth-child(4) {padding: 0 !important;}
.tiercoat.pull-left .table th:nth-child(4) span {
	font-size: 25px !important;
	padding: 0 !important;
	/* float: right; */
}
.tiercoat.pull-left .table th {border-top: 0 !important;}
.tiercoat.pull-left .table tr {border-bottom: 1px solid #ccc !important;}
.tbl-tiers-index .tiercoat.pull-left .sheenCheck {
	width: auto !important;
	margin-right: 5px;
	margin-top: 10px;
}
.mainSheen {padding: 0 !important;height: 22px;}
.sheenP {
	margin-top: 8px;
	float: left;
	margin-right: 5px;
	/* font-weight: bold; */
}

.tiercoat.pull-left .cls ul li {
	float: left;
	width: 100%;
	text-align: right;
	display: inline;
	background: transparent !important;
}
.mainDiv i {
	padding-top: 10px;
	padding-right: 10px;
}
.sheenProductList {
	display: inline-block;
	margin-right: 0px;
	margin-bottom: 8px;
	/* font-weight: bold; */
}

.tiercoat.pull-left .cls ul li div:first-child {width: 5%;overflow: hidden;height: 15px;float: left;}
.table-left-aside {width: 200px;background: #fff;position: absolute;}
.close {display: block !important;}
.undercoats.tiercoat-one.pull-right .sheenProductCheck {width: auto !important;margin-right: 5px;font-weight: normal;float: left !important;display: inline-block;margin-top: 4px;	margin-left: 50px;}
.undercoats.tiercoat-one.pull-right .addproduct2 {font-weight: bold !important;}
.undercoats.tiercoat-one.pull-right th:last-child {padding: 0 5px 0 0 !important;text-align: right;}
.undercoats.tiercoat-one.pull-right table {margin-bottom: 0;border-bottom: 1px solid #ccc;}
.undercoats.tiercoat-one.pull-right li {background: #fff !important;}
.submitForm {width: 100%;display: inline-block;	background: linear-gradient(#dedede, #ededed);	border: 1px solid #dadada;	border-radius: 5px;	padding: 8px 15px;	margin-bottom: 15px;}
.submitForm button.btn.btn-success {background: #0077d0 !important;border-color: #0077d0 !important;padding: 5px 20px !important;}
.undercoats.tiercoat-one.pull-right th {border: 0;}
.table-left-aside .left h5 {text-align:center;width: 100%;display: inline-block;background: linear-gradient(#dedede, #ededed);border: 0px solid #dadada;border-radius: 0;padding: 8px 15px;margin-bottom: 0;margin-top: 0;}
.table-left-aside .left {border-color: #e0e0e0;border-radius: 5px;}
.loader {
    border: 9px solid #f3f3f3;
    border-radius: 50%;
    border-top: 10px solid #3498db;
    width: 70px;
    height: 70px;
    -webkit-animation: spin 2s linear infinite;
    animation: spin 2s linear infinite;
    position: absolute;
    left: 50%;
    top: 50%;
    transform: translate(-50%, -50%);
}


.tierLoaderoverlay {
    position: absolute;
    left: 0;
    top: -50px;
    background: rgba(0,0,0,0.4);
    width: 100%;
    height: calc(100% + 50px);
    z-index: 9999;
    text-align: center;
    display:none;

}

.tiercoat.pull-left {
	border-right: 0px solid #ccc;
	padding-right: 0px;
}





/* Safari */
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}

@media screen and (max-width: 1700px) {
.tiercoat.pull-left .cls ul li div:last-child {
	width: 90% !important;
	overflow: hidden;
	height: 32px;
	float: right;
	text-align: left;
}
}







/****************************************************************************/
#myModal1.modal.modal-child .modal-dialog {
}
#myModal1.modal.modal-child #searchProductName {
	margin: 13px 10px;
}
</style>
<div class="tbl-tiers-index">
<?php Pjax::begin(['timeout' => 5000,'id'=>'tierpjax']);?>

<?php
 Modal::begin([
          'header'=>'',
          'id'=>'modaltier',
          'size'=>'modal-lg',
       ]);
        echo "<div class='tierLoaderoverlay'><div class='loader'></div></div>";
       echo "<div id='modalContenttier'><div style='text-align:center'><img src='uploads/avatar2.gif' height='100' width='100'></div></div>";
      Modal::end();
      ?>
      <?php
        Modal::begin([
          'header'=>'',
          'id'=>'modaltierCreate',
          'size'=>'modal-sl',
       ]);
        //echo "<div class='tierLoaderoverlay'><div class='loader'></div></div>";
       echo "<div id='modalContenttierCreate'><div style='text-align:center'><img src='uploads/avatar2.gif' height='100' width='100'></div></div>";
      Modal::end();
      ?>
   <?php echo  Html::button('<img src="uploads/add.png" width="20" height="20"/>', ['value'=>Url::to( Url::base().'/createtier'),'class'=>'btn btn-primary pull-right' ,'id'=>'modalButtontier']); ?>
    <?php echo $this->render('_searchtier', ['model' => $searchModelTier]); ?>

   
    
   
<div class="main-table-index-page">
    <?= GridView::widget([
        'dataProvider' => $dataProviderTier,
        //'filterModel' => $searchModelTier,
        'rowOptions'   => function ($model, $key, $index, $grid) {
              return  [
                           "class"=>($model->enabled != 1)?"modalButtontier2 disabledTr":"modalButtontier2",
                           'value'=>Url::to(Url::base().'/updatetier?id='.$model->tier_id),
                           "style"=>($model->enabled != 1)?"background-color:#f9f9f9;":""
                          ];
                },
         'behaviors' => [
            [
                'class' => '\dosamigos\grid\behaviors\LoadingBehavior',
                'type' => 'spinner'
            ]
        ],  
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

          ['attribute'=>'name',
           'label' => 'Name',
           'format' => 'raw',
           'value' =>
             function($model){
               if($model->enabled != 1){
                  return '<strike>'.$model->name.'</strike>';
                }else{
                  return $model->name;
                }
             },
           ],
            // 'name',

            'brand.name',
            'type.name',
             // ['attribute'=>'enabled or disabled',
             // 'value' =>
             //           function($model){
             //             if($model->enabled == '1'){
             //                return 'enabled';
             //             }
             //             else
             //             {
             //                return 'disabled';
             //             }
             //           },



             // 'filter' =>false,],
            //'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn',
            'header' => 'UPDATE',
               'headerOptions' => ['style' => 'color:#337ab7'],
               'template' => '{update}',
               'buttons' => [
            

               'update' => function ($url, $model,$id) {
                            return Html::a('<span class=""><img src="uploads/update.png" width="13" height="13"/></span>', $url, [
                           "class"=>"modalButtontier3",'value'=>Url::to(Url::base().'/updatetier?id='.$model->tier_id)
                ]);
            },
            

          ],
           'urlCreator' => function ($action, $model, $key, $index) {
            

            Url::to(['update?id='.$model->tier_id],['id'=>'modal'])   ;         
                //$url ='update?id='.$model->brand_id;
              //return $url;
                 
                 //return Html::a($url, ['title' => 'view']);

            }
          ],
        ],
    ]); ?>
  </div>
    <div class="tierPageDisplay page-display-showing-foot"></div>
</div>
<script>

  $(function()
{
  //
  $('.modalButtontier3').click(function(){
    $('#modaltier').modal('show')
      .find('#modalContenttier')
      .load ($(this).attr('value'));
  });
});


  $(function()
{
  //
  $('#modalButtontier').click(function(){
    $('#modaltierCreate').modal('show')
      .find('#modalContenttierCreate')
      .load ($(this).attr('value'), function(){
        // $('#modaltierCreate .modal-header').remove();
      });
  });
});
  $(function()

{
  //$('.modalButtontier2').dblclick(function(){
  $('.modalButtontier2').click(function(){
    $('#modaltier').modal('show')
    .find('#modalContenttier')
    .load ($(this).attr('value'));
  });

});
   
$(function() { //equivalent to document.ready
  $("#tbltiers_search-name").focus();
});


$('#tbltiers_search-name').focus(function(){
  var that = this;
  setTimeout(function(){ that.selectionStart = that.selectionEnd = 10000; }, 0);
});

  

$('#tbltiers_search-name').bind('keyup', function() { 
    $('#tbltiers_search-name').delay(200).submit();
});

$('#tbltiers_search-enabled').bind('change', function() { 
    $('#tbltiers_search-enabled').delay(200).submit();
});

$('#tbltiers_search-brand_id').bind('change', function() { 
    $('#tbltiers_search-brand_id').delay(200).submit();
});


    </script>
<script>
  var name = $('.summary').html();
$('.summary').remove();
$('.tierPageDisplay').html(name);
if($('.tierPageDisplay').text()=='undefined')
{
   $('.tierPageDisplay').hide();
}
//  $('#modaltier .modal-header').remove();
  </script>    
<?php Pjax::end();?>
