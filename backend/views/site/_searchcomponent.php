<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TblComponentGroupsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-component-groups-search tbl-search-input-all">

   <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
        'data-pjax' => 1
        ],
    ]);


     ?>

<div style="width:23%; margin-right:0px; border:1px;">
    <?= $form->field($model, 'name')->textInput(['placeholder' => "Search Components group"])->label(false); ?>
</div>
<div style="width:43%;  border:1px; margin-left:340px; margin-top: -49px">
        <?php
            echo $form->field($model, 'enabled')->dropDownList(
                        ['' => 'SHOW ALL', '1' => 'Active', '0' => 'Archived'])->label(false); 
        ?>

    </div>

 

   

    <?php ActiveForm::end(); ?>

</div>
