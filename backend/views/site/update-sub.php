<?php
   use yii\helpers\Url;
   use app\models\TblUserRoles;
   use app\models\TblCompanyDocuments;
   use app\models\TblProductType;
   ?>
<!DOCTYPE html>
<html>
   <head>
      <style type="text/css">
         .modal-body {
         position: relative;
         padding: 15px;
         float: left;
         /*width: 93%;*/
         padding-bottom: 0;
         }		
         .modal-dialog{
         width: 750px;
         }
         #blah21 {
         position: relative;
         top: 0;
         left: 0;
         }
         .deletequotedoc
         {cursor: pointer;}
         .cr-slider-wrap
         {display: none;}
         img#pdfLogo {
         width: 250px;
         height: 70px;
         object-fit: contain;
         }
         /*    #popup {
         width: 320px;
         height: 300px;
         margin: 0 auto;
         box-shadow: 1px 1px 1px 1px black;
         display: none;
         }
         #popup iframe {
         width: 100%;
         height: 100%
         }*/ 
         /*    #mydocModal
         {
         max-height: calc(100vh - 210px);
         overflow-y: auto;
         }*/
         #myModalsettingsUpdate .subs-detail.upload-img .subscriberLogo-btn-divUpload {
    float: right;
    width: 100%;
    padding-top:12px;
}
#myModalsettingsUpdate .subs-detail.upload-img .subscriberLogo-btn-divUpload button.clickDocUpp.pull-right.btn.btn-primary {
    width: 100px;
    /* margin-top: 10px !important; */
    float: left;
}


div#table_idQuoets_filter label {
    font-size: 14px;
    float: left;
    width: 100%;
    line-height: 34px;
}
div#table_idQuoets_filter {
    float: right;
    width: 35%;
}
div#table_idQuoets_filter input[type="search"] {
    display: inline-block;
    width: 63%;
    padding: 0.375rem 0.75rem;
    font-size: 1rem;
    line-height: 1.5;
    color: #495057;
    background-color: #fff;
    background-clip: padding-box;
    border: 1px solid #ced4da;
    border-radius: 0.25rem;
    transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
    float: right;
    height: 33px;
}
div#table_idQuoets_length label select {
    -webkit-appearance: none;
    background-image: url(../uploads/dropdown.png) !important;
    background-repeat: no-repeat;
    background-position: 95% 14px;
    background-color: #fff;
    background-size: 13px 8px;
    padding-right: 30px !important;
    display: inline-block;
    width: 71px;
    padding: 0.375rem 0.75rem;
    font-size: 1rem;
    line-height: 1.5;
    color: #495057;
    background-color: #fff;
    background-clip: padding-box;
    border: 1px solid #ced4da;
    border-radius: 0.25rem;
    transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
    /* float: right; */
    height: 33px;
    margin: 0 4px;
}
#modalSubscriber .modal-dialog .modal-dialog .table td .radio-btns-div label {
    width: 23% !important;
    float: left;
}
#modalSubscriber .modal-dialog .modal-dialog .table td .subscriberLogo-btn-div button.clickDocRemove.pull-right.btn.btn-primary {
    display: block !important;
    background-position: center 7px;
    width: 24px;
    height: 24px;
    background-size: 10px;
}
#modalSubscriber .modal-dialog .modal-dialog .table td .subscriberLogo-btn-div {
    top: 34px;
    left: 98px;
}
@media only screen and (max-width:1439px){
   #modalSubscriber .modal-dialog .modal-dialog .table td .subscriberLogo-btn-div {
    top: 34px;
    left: 90px;
}
}
      </style>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment-with-locales.min.js"></script>
   </head>
   <body>
      <?php 
         $model = TblUserRoles::find() ->where(["status" => '1'])->all(); 
         $modelDoc = TblCompanyDocuments::find()->where(['company_id'=>$_GET['id']])->all();
         $serverName = $_SERVER['HTTP_HOST'];
         $modelProductType = TblProductType::find()->all();
         ?>
      <!-- <div class="showLoader text-center" style="display:none;"><p>loading....</p></div> -->
      <div class="showLoader text-center" style="display:none;" style='text-align:center;display:none;'><img src='<?php echo Url::base();?>/uploads/avatar2.gif' height='100' width='100'></div>
      <div style="display:none;" class="bodyMain col-sm-12 row">
         <div class="subscribe-update-div">
            <div role="tabpanel">
               <!-- Nav tabs -->
               <ul class="nav nav-tabs nav-justified nav-tabs-dropdown" role="tablist">
                  <li role="presentation" class="active"><a href="#Subscriber" aria-controls="Subscriber" role="tab" data-toggle="tab">Subscriber</a></li>
                  <li role="presentation"><a href="#Document" aria-controls="Document" role="tab" data-toggle="tab">Library Document</a></li>
                  <li role="presentation"><a href="#Settings" aria-controls="Settings" role="tab" data-toggle="tab">Settings</a></li>
                  <li role="presentation"><a href="#People" aria-controls="People" role="tab" data-toggle="tab">Subscriber's People</a></li>
                  <li role="presentation" class=""><a href="#q-email" aria-controls="q-email" role="tab" data-toggle="tab">Quote Email</a></li>
                  <li role="presentation"><a href="#jss-email" aria-controls="services" role="tab" data-a-controls="jss-email" role="tab" data-toggle="tab">JSS Email</a></li>
                  <li role="presentation"><a href="#Prep" aria-controls="Prep" role="tab" data-toggle="tab">Prep</a></li>
                  <li role="presentation"><a href="#room-type" aria-controls="room-type" role="tab" data-toggle="tab">Room Type</a></li>
                  <li role="presentation"><a href="#paint-store" aria-controls="services" role="tab" data-a-controls="paint-store" role="tab" data-toggle="tab">Paint Stores</a></li>
                  <li role="presentation"><a href="#paint-order" aria-controls="paint-order" role="tab" data-toggle="tab">Paint Orders</a></li>
                  <li role="presentation"><a href="#Customers" aria-controls="Customers" role="tab" data-toggle="tab">Customer Sources</a></li>
                  <li role="presentation"><a href="#specialItems" aria-controls="specialItems" role="tab" data-toggle="tab">Special Items</a></li>
                  <li role="presentation"><a href="#paymentNotes" aria-controls="paymentNotes" role="tab" data-toggle="tab">Payment Notes</a></li>
                  <li role="presentation"><a href="#paymentOrderNotes" aria-controls="paymentOrderNotes" role="tab" data-toggle="tab">Payment Order Notes</a></li>
                  <li role="presentation"><a href="#companyQuotes" aria-controls="companyQuotes" role="tab" data-toggle="tab">Quotes</a></li>
               </ul>
               <!-- Tab panes -->
               <div class="tab-content">
                  <div role="tabpanel" class="tab-pane active" id="Subscriber">
                     <div class="col-sm-12">
                        <div class="inner-box first-box">
                           <div class="subscribe-div subs-detail">
                              <span style="font-size: :20px" class="pull-right btn btn-large btn-primary editSub">Edit Subscriber</span>
                              <h2>Subscriber Details</h2>
                           </div>
                           <input type="file" class="changeImage pull-right" style="display:none;"  id="insert_image_logo" onchange="document.getElementById('blah').src = window.URL.createObjectURL(this.files[0])">
                           <input type="text" style="display:none;" value="<?php echo $_GET['id']?>" class="idClicked">
                           <div class="subs-img-main">
                              <div class="subs-detail upload-img">
                                 <div class="subscriberLogo pull-right"></div>
                                 <div class="subscriberLogo-btn-div">
                                    <!-- <button class="upload pull-right btn btn-primary" style="margin-top: 117px;"></button> -->
                                    <button class="imageChange pull-right btn btn-primary" style="margin-top: 117px;"></button>
                                 </div>
                                 <p>Recommended Size: 270 &times; 170 pixels. Max: 10MB.</p>
                                 <p>Acceptable file types are .jpg, .png.</p>
                              </div>
                              <div class="pdf-main-div">
                                 <div class="subs-detail">
                                    <div class="pdf-label">
                                       <label>PDF Logo</label>
                                    </div>
                                    <input type="file" class="changePdfImage pull-right" style="display:none;" id="insert_image" onchange="document.getElementById('pdfLogo').src = window.URL.createObjectURL(this.files[0])" alt="">
                                    <input type="text" style="display:none;" value="<?php echo $_GET['id']?>" class="idClicked">
                                    <div class="logoPdf-upr">
                                       <div class="logopdf-image-main">
                                          <div class="logoPdf pull-right"></div>
                                       </div>
                                       <button class="imageChangePdf pull-left btn btn-primary" style="margin-right: 3px;"></button>
                                       <p>Recommended Size: 260 &times; 90 pixels. Max: 10MB.</p>
                                       <p>Acceptable file types are .jgp, .png.</p>
                                       <!-- <button class="uploadPdfLogo pull-left btn btn-primary" style="margin-right: 3px;"></button> -->
                                    </div>
                                 </div>
                              </div>
                              <div class="pdf-main-div">
                                 <div class="subs-detail">
                                    <div class="pdf-label">
                                       <label>Certified Logo</label>
                                    </div>
                                    <input type="file" class="changeAccredateImage pull-right" style="display:none;" id="insert_accredate_image" onchange="document.getElementById('accredateLogo').src = window.URL.createObjectURL(this.files[0])">
                                    <input type="text" style="display:none;" value="<?php echo $_GET['id']?>" class="idClicked">
                                    <div class="logoPdf-upr">
                                       <div class="logopdf-image-main">
                                          <div class="logoAccredate pull-right"></div>
                                       </div>
                                       <button class="imageChangeAccredate pull-left btn btn-primary" style="margin-right: 3px;"></button>
                                       <p>Recommended Size: 270 × 170 pixels. Max: 10MB.</p>
                                       <p>Acceptable file types are .jpg, .png.</p>
                                       <!-- <button class="uploadPdfLogo pull-left btn btn-primary" style="margin-right: 3px;"></button> -->
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="subs-right-detail">
                              <div class="subscriberName subs-detail" data-name=""></div>
                              <div class="subscriberAddress subs-detail" data-address=""></div>
                              <div class="subscriberSuburb subs-detail" data-suburb=""></div>
                              <div class="subscriberState subs-detail" data-state="" data-postcode=""></div>
                              <div class="subscriberCountry subs-detail" data-country=""></div>
                              <div class="subscriberPhone subs-detail" data-phone=""></div>
                              <div class="subscriberEmail subs-detail" data-email=""></div>
                              <div class="subscriberAbn subs-detail" data-abn=""></div>
                              <div class="subscriberinsurance_comp_detail subs-detail" data-insurance_comp_detail=""></div>
                              <div class="subscriberworker_insurance_detail subs-detail" data-worker_insurance_detail=""></div>
                              <div class="subscriberlicense_number subs-detail" data-license_number=""></div>
                              <div class="subscriberpaint_brand_name subs-detail" data-paint_brand_name=""></div>
                              <div class="subscribertimezone subs-detail" data-timezone=""></div>
                              <div class="subscriberWebsite subs-detail" data-website=""></div>
                              <div class="subscriberstatus subs-detail" data-status=""></div>
                              <div class="subscriberpaint_GST subs-detail" data-GST=""></div>
                              <div class="subscriberpaint_brand subs-detail" data-paint_brand=""></div>
                           </div>
                           <!-- </div>
                              <div class="col-sm-6"> -->
                        </div>
                     </div>
                  </div>
                  <div role="tabpanel" class="tab-pane" id="Document">
                     <div class="col-sm-12">
                        <div class="inner-box sec-box">
                           <div class="add-doc-head">
                              <button class="btn btn-primary addDoc">Add New Library Document</button>
                              <!-- <span class="search-right">
                                 <input type="text" placeholder="Search/Filter" class="form-control">
                                 </span> -->
                           </div>
                           <table class="table doc-table popup-table">
                              <tr>
                                 <th>File ID</th>
                                 <th>File Name</th>
                                 <th>Description</th>
                                 <th>Uploaded By</th>
                                 <th>Uploaded Date</th>
                              </tr>
                              <tbody class="documentListing">
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
                  <div role="tabpanel" class="tab-pane" id="Settings">
                     <div class="col-sm-12">
                        <div class="inner-box third-box">
                           <span style="font-size: 22px;">Settings</span>
                           <span class="pull-right updateSettings btn btn-large btn-primary"  style="font-size: 20px;">Edit</span>
                           <div>
                              <table class="table popup-table setting-table">
                                 <tr>
                                    <td>Application Cost</td>
                                    <td class="application_cost empty"></td>
                                 </tr>
                                 <tr>
                                    <td>Profit Mark Up %</td>
                                    <td class="profit_markup empty"></td>
                                 </tr>
                                 <tr>
                                    <td>Gross Margin %</td>
                                    <td class="gross_margin_percent empty"></td>
                                 </tr>
                                 <tr>
                                    <td>Default Deposit Percent</td>
                                    <td class="default_deposit_percent empty"></td>
                                 </tr>
                                 <!-- <tr>
                                    <td>Terms & Conditions</td>
                                    <td class="terms_condition empty"></td>
                                 </tr> -->
                                 <tr>
                                    <td>Terms & Conditions Doc</td>
                                    <td class="Docddd"></td>
                                 </tr>
                                 <!-- <tr>
                                    <td>Xero Consumer Key</td>
                                    <td class="xero_consumer_key empty"></td>
                                    </tr> 
                                    <tr>
                                    <td>Xero Share Secret</td>
                                    <td class="xero_share_secret empty"></td>
                                    </tr>
                                    <tr>
                                    <td>Xero Invoice Line Account Code</td>
                                    <td class="xero_invoice_line_account_code empty"></td>
                                    </tr> -->
                                 <tr>
                                    <td>Account Name</td>
                                    <td class="account_name empty"></td>
                                 </tr>
                                 <tr>
                                    <td>Account BSB</td>
                                    <td class="account_BSB empty"></td>
                                 </tr>
                                 <tr>
                                    <td>Account Number</td>
                                    <td class="account_number empty"></td>
                                 </tr>
                                 <tr>
                                    <td>Payment Phone Number</td>
                                    <td class="payment_phone_number empty"></td>
                                 </tr>
                                 <tr>
                                    <td>Colour Consultant Item</td>
                                    <td class="colour_consultant_item empty"></td>
                                 </tr>
                                 <tr>
                                    <td>Quickbook Status</td>
                                    <td class="quickbook_status empty"></td>
                                 </tr>
                                 <tr>
                                    <td>Price Increase %</td>
                                    <td class="price_increase empty"></td>
                                 </tr>
                                 
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div role="tabpanel" class="tab-pane" id="People">
                     <div class="col-sm-12">
                        <div class="inner-box fourth-box">
                           <span style="font-size: 22px;">Subscriber's People</span>
                           <div class="select-filter-div pull-right">
                              <span style="font-size:20px" class="">
                                 <select id="sub_filter" name="status_filter">
                                    <option value="" selected>Show All</option>
                                    <option value="10">Active</option>
                                    <option value="20">Archived</option>
                                 </select>
                              </span>
                              <span style="font-size:20px" class="pull-right addsubscriberPeople btn btn-large btn-primary">Add People</span>
                           </div>
                           <table class="table popup-table subs-people">
                              <tr>
                                 <th>
                                    Person
                                 </th>
                                 <th>
                                    Active
                                 </th>
                                 <th>
                                    Role
                                 </th>
                                 <th>
                                    Logged On
                                 </th>
                                 <th>
                                    Last Activity
                                 </th>
                              </tr>
                              <tbody class="subscriberPeople">
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
                  <div role="tabpanel" class="tab-pane" id="q-email">
                     <div class="col-sm-12">
                        <div class="inner-box fifth-box">
                           <span style="font-size: 22px;">Quote Email Settings</span><span style="font-size: :20px" class="pull-right btn btn-large btn-primary editEmailSetting">Edit</span>
                           <table class="table popup-table quote-table">
                              <tr>
                                 <td>Server Email Address</td>
                                 <td class='serverEmailAddress'></td>
                              </tr>
                              <tr>
                                 <td>BCC to</td>
                                 <td class='serverBccAddress'></td>
                              </tr>
                              <tr>
                                 <td>Quote Email Subject</td>
                                 <td class='quoteEmailSubject'></td>
                              </tr>
                              <tr>
                                 <td>Include Documents</td>
                                 <td></td>
                              </tr>
                           </table>
                           <table class="table quoteIncludeDocuments pull-right"></table>
                        </div>
                     </div>
                  </div>
                  <div role="tabpanel" class="tab-pane" id="jss-email">
                     <div class="col-sm-12">
                        <div class="inner-box sixth-box">
                           <span style="font-size: 22px;">JSS Email Settings</span><span style="font-size: :20px" class="pull-right btn btn-large btn-primary editEmailSettingJss">Edit</span>
                           <table class="table popup-table jss-setting">
                              <tr>
                                 <td>Server Email Address</td>
                                 <td class='serverEmailAddressjss'></td>
                              </tr>
                              <tr>
                                 <td>BCC to</td>
                                 <td class='serverBccAddressjss'></td>
                              </tr>
                              <tr>
                                 <td>Quote Email Subject</td>
                                 <td class='quoteEmailSubjectjss'></td>
                              </tr>
                              <tr>
                                 <td>Include Documents</td>
                                 <td></td>
                              </tr>
                           </table>
                           <table class="table quoteIncludeDocumentsjss pull-right"></table>
                        </div>
                     </div>
                  </div>
                  <div role="tabpanel" class="tab-pane" id="Prep">
                     <div class="col-sm-12">
                        <div class="inner-box seventh-box">
                           <span style="font-size: 22px;">Prep</span><span style="font-size: :20px" class="pull-right btn btn-large btn-primary prepbutton">Create Prep</span>
                           <table class="table popup-table prep-table">
                              <tr>
                                 <th>Name</th>
                                 <th>% uplift to paint cost</th>
                                 <th>% uplift to paint time</th>
                              </tr>
                              <tbody class="prepBody"></tbody>
                           </table>
                        </div>
                     </div>
                  </div>
                  <div role="tabpanel" class="tab-pane" id="room-type">
                     <div class="col-sm-12">
                        <div class="inner-box eighth-box">
                           <span style="font-size: 22px;">Room Types</span><span style="font-size: :20px" class="pull-right btn btn-large btn-primary roombutton">Create Room Types</span>
                           <table class="table popup-table rooms-table">
                              <tr>
                                 <th>Name</th>
                                 <th>type</th>
                              </tr>
                              <tbody class="RoomBody"></tbody>
                           </table>
                        </div>
                     </div>
                  </div>
                  <div role="tabpanel" class="tab-pane" id="paint-store">
                     <div class="col-sm-12">
                        <div class="inner-box eighth-box">
                           <span style="font-size: 22px;">Paint Stores</span>
                           <span style="font-size: :20px" class="pull-right btn btn-large btn-primary paintStoreButton">Add Paint Stores</span>
                           <table class="table popup-table paint-store-table">
                              <tr>
                                 <th>Name</th>
                              </tr>
                              <tbody class="PaintStoreBody"></tbody>
                           </table>
                        </div>
                     </div>
                  </div>
                  <div role="tabpanel" class="tab-pane" id="paint-order">
                     <div class="col-sm-12">
                        <div class="inner-box eighth-box">
                           <span style="font-size: 22px;">Paint Orders</span>
                           <table class="table popup-table paint-order-table">
                              <tr>
                                 <th>Quote No.</th>
                                 <th>Client</th>
                                 <th>Notes</th>
                                 <th>Ordered By</th>
                                 <th>I/E</th>
                                 <th>Date</th>
                              </tr>
                              <tbody class="PaintOrderBody"></tbody>
                           </table>
                        </div>
                     </div>
                  </div>
                  <div role="tabpanel" class="tab-pane" id="Customers">
                     <div class="col-sm-12">
                        <div class="inner-box eighth-box">
                           <span style="font-size: 22px;">Customer Sources</span>
                           <span style="font-size: :20px" class="pull-right btn btn-large btn-primary customersourcebutton">Add Customers Sources</span>
                           <table class="table popup-table customer-source-table">
                              <tr>
                                 <th>Name</th>
                              </tr>
                              <tbody class="CustomerSourceBody"></tbody>
                           </table>
                        </div>
                     </div>
                  </div>
                  <div role="tabpanel" class="tab-pane" id="specialItems">
                     <div class="col-sm-12">
                        <div class="inner-box eighth-box">
                           <span style="font-size: 22px;">Special Items</span><span style="font-size: :20px" class="pull-right btn btn-large btn-primary specialItemsbutton ">Add Special Item</span>
                           <table class="table popup-table special-items-table">
                              <tr>
                                 <th>Name</th>
                                 <th>Price (Exc. GST)</th>
                              </tr>
                              <tbody class="SpecialItemBody"></tbody>
                           </table>
                        </div>
                     </div>
                  </div>
                  <div role="tabpanel" class="tab-pane" id="paymentNotes">
                     <div class="col-sm-12">
                        <div class="inner-box eighth-box">
                           <span style="font-size: 22px;">Payment Notes</span><span style="font-size: :20px" class="pull-right btn btn-large btn-primary paymentnotebutton">Add Payment Note</span>
                           <table class="table popup-table payment-notes-table">
                              <tr>
                                 <th>Name</th>
                              </tr>
                              <tbody class="PaymentNotesBody"></tbody>
                           </table>
                        </div>
                     </div>
                  </div>
                  <div role="tabpanel" class="tab-pane" id="paymentOrderNotes">
                     <div class="col-sm-12">
                        <div class="inner-box eighth-box">
                           <span style="font-size: 22px;">Payment Order Notes</span><span style="font-size: :20px" class="pull-right btn btn-large btn-primary paintordernotebutton">Add Payment Order Notes</span>
                           <table class="table popup-table payment-order-notes-table">
                              <tr>
                                 <th>Name</th>
                              </tr>
                              <tbody class="PaintOrderNotesBody"></tbody>
                           </table>
                        </div>
                     </div>
                  </div>
                  <div role="tabpanel" class="tab-pane" id="companyQuotes">
                     <div class="col-sm-12">
                        <div class="inner-box eighth-box">
                           <table id="table_idQuoets" class="display table popup-table quotes-table">
                               <thead>
                                   <tr>
                                       <th>Quote_id</th>
                                       <th>Contact_id</th>
                                       <th>Type</th>
                                       <th>Client_name</th>
                                       <th>Contact_email</th>
                                       <th>Contact_name</th>
                                       <th>Contact_number</th>
                                       <th>Contact_mobile</th>
                                       <th>Price</th>
                                       <th>Status</th>
                                   </tr>
                               </thead>
                               <tbody class="quotes-tableBody">
                                  
                               </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- <div class="col-sm-6">
            <span style="font-size: 22px;">Appointment Types</span>
            <span style="font-size: :20px" class="pull-right btn btn-large btn-primary appointmentTypebutton">Create Appointment Types</span>
            <table class="table">
             <tr>
               <th>Name</th>
             </tr>
             <tbody class="AppointmentBody"></tbody>
            </table>
            </div>   -->      
      </div>
      <!--- bootstrap Modal for adding doc starts here!-->
      <!-- Modal -->
      <div id="myModalDocAdd"  class="modal modal-child" data-backdrop-limit="1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-modal-parent="#myModal">
         <div class="modal-dialog">
            <!-- Modal content-->
            <form action="" method="post" enctype="multipart/form-data" id="formDoc">
               <input type="hidden" name="subscriber_id" class="subscriber_id_create" value="<?php echo Yii::$app->user->id;?>"> 
               <input type="hidden" name="company_id" value="<?php echo $_GET['id']?>" class="company_id_create">
               <input type="hidden" name="doc_id" value="0" class="doc_id_create">
               <div class="modal-content">
                  <div class="modal-header">
                     <button type="button" class="close-icon closeModalDocAdd pull-right btn btn-primary"  >&times;</button>
                     <input type="button" class="SaveDocAdd pull-right btn btn-primary" value="Save" name="saveDoc">
                     <h4 class="modal-title">Document Detail</h4>
                  </div>
                  <div class="modal-body DocAdd">
                     <table class="table popup-table">
                        <tr>
                           <td>
                              <label>File Name</label>
                           </td>
                           <td>
                              <input type="text" name="file_name" class="form-control file_name_doc_create " required="required">
                           </td>
                        </tr>
                        <tr>
                           <td>
                              <label>File Description</label>
                           </td>
                           <td>
                              <textarea rows="4" cols="50" name="desc" class="form-control file_desc_doc_create" style="margin: 0px 0px 0px 0px; width: 100%; height: 85px;"></textarea>
                           </td>
                        </tr>
                        <tr>
                           <td>
                              <label>Always Link</label>
                           </td>
                           <td>
                              <div class="radio-btns-div">
                                 <label><input type="radio" name="always_link" value="1" > Yes</label>
                                 <label><input type="radio" name="always_link"  value="0" checked> No</label>
                              </div>
                           </td>
                        </tr>
                        <tr>
                           <td>
                              <label>Upload New file</label>
                           </td>
                           <td>
                              <input type="file" name="image_0" class="imageDoc_create"  style="display:none; ">
                              <img style="border: 3px solid #ddd;" src="<?php  echo Url::base(true); ?>/uploads/no_image.png"  class="viewimageDoc" id="viewimageDoc" width="120px" height="120px" >
                              <span class="opnImg"><img id="blah21" src="<?php  echo Url::base(true); ?>/uploads/edit.png" alt="your image" width="15px" height="16px"/ onclick="$('.imageDoc_create').click();"></span>
                           </td>
                        </tr>
                     </table>
            </form>
            </div>
            <div class="modal-footer">
            </div>
            </div>
            </form>
         </div>
      </div>
      <!--- bootstrap Modal for adding doc ends here!-->
      <!--- bootstrap Modal for Updating doc starts here!-->
      <!-- Modal -->
      <div id="myModalDocUpdate"  class="modal modal-child" data-backdrop-limit="1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-modal-parent="#myModal">
         <div class="modal-dialog">
            <!-- Modal content-->
            <form action="" method="post" enctype="multipart/form-data" id="formDocUpdate">
               <input type="hidden" name="subscriber_id" class="subscriber_id" value="<?php echo Yii::$app->user->id;?>">
               <input type="hidden" name="company_id" class="company_id" value="<?php echo $_GET['id']?>">
               <input type="hidden" name="doc_id" class="doc_id" >
               <input type="hidden" class="uploaded_byUpdate">
               <div class="modal-content">
                  <div class="modal-header">
                     <button type="button" class="closeModalDocAdd pull-right btn btn-primary"  >&times;</button>
                     <input type="button" class="SaveDocUpdate pull-right btn btn-primary" value="Save" >
                     <h4 class="modal-title">Document Detail</h4>
                  </div>
                  <div class="modal-bodyDocAdd">
                     <table class="table">
                        <tr>
                           <td>
                              <label>File Name</label>
                           </td>
                           <td>
                              <input required type="text" name="file_name" class="form-control file_doc_name">
                           </td>
                        </tr>
                        <tr>
                           <td>
                              <label>File Description</label>
                           </td>
                           <td>
                              <textarea rows="4" cols="50" name="desc" class="form-control file_doc_desc" style="margin: 0px 313px 0px 0px; width: 451px; height: 85px;"></textarea>
                           </td>
                        </tr>
                        <tr>
                           <td>
                              <label>Always Link</label>
                           </td>
                           <td>
                              <p>
                                 <input type="radio" name="always_link" value="1" class="file_doc_alwayslinkon">
                                 <label>Yes</label>
                              </p>
                              <p>
                                 <input type="radio" name="always_link"  value="0" class="file_doc_alwayslinkoff">
                                 <label>No</label>
                              </p>
                           </td>
                        </tr>
                        <tr>
                           <td class="imageName">
                           </td>
                           <td class="previewImage" id="previewImage">
                              <span>Preview</span>
                           </td>
                        </tr>
                        <tr>
                           <td>
                              <label>Upload New file</label>
                           </td>
                           <td>
                              <input type="file" name="image_0" class="imageDocupdate" >
                              <!-- <img style="border: 3px solid #ddd;" src="<?php  //echo Url::base(true); ?>/uploads/no_image.png"  class="viewimageDoc imagedoc_list" id="viewimageDocupdate" width="120px" height="120px" >
                                 <span class="opnImg"><img id="blah21" src="<?php  //echo Url::base(true); ?>/uploads/edit.png" alt="your image" width="15px" height="16px"/ onclick="$('.imageDocupdate').click();"></span> -->
                           </td>
                        </tr>
                     </table>
            </form>
            </div>
            <div class="modal-footer">
            </div>
            </div>
            </form>
         </div>
      </div>
      <!--- bootstrap Modal for adding doc ends here!-->
      <!--bootstrap modal for adding people starts here-->
      <!-- Modal -->
      <div class="modal modal-child" data-backdrop-limit="1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-modal-parent="#myModal" id="myModalPeopleAdd">
         <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
               <div class="modal-header">
                  <!--  <button type="button"  class="pull-right">&times;</button>
                     <button type="button" onclick="$('#myModalPeopleAdd').modal('hide')" class="pull-right">&times;</button> -->
                  <button type="button" onclick="$('#myModalPeopleAdd').modal('hide')" class="close-icon pull-right btn btn-primary">&times;</button>
                  <input type="button" class="SavePeopleSubscriber pull-right btn btn-primary" value="Save" >
                  <h4 class="modal-title">Add New System User</h4>
               </div>
               <div class="modal-body">
                  <table class="table">
                     <tr>
                        <td>
                           <label>First Name</label>
                           <input type="text" class="form-control first_name empty" name="first_name">
               </div>
               </td>
               <td>
               <label>Last Name</label>
               <div class="input-box"><input type="text" class="form-control last_name empty" name="last_name"></div>
               </td>
               </tr>
               <tr>
               <td>
               <label>User Name</label>
               <div class="input-box"><input type="text" class="form-control user_name empty" name="user_name"></div>
               </td>
               <td>
               <label>Email</label>
               <div class="input-box"><input type="email" class="form-control email empty" name="email"></div>
               </td>
               </tr>
               <tr>
               <td>
               <label>Phone</label>
               <div class="input-box"><input type="number" class="form-control phone empty" name="phone"></div>
               </td>
               <td>
               <label>Mobile</label>
               <div class="input-box"><input type="number" class="form-control mobile empty" name="mobile"></div>
               </td>
               </tr> 
               <tr>
               <td>
               <label>User Role</label>
               <div class="input-box"><select  class="form-control user_role empty" name="user_role"> 
               <option value="">Select User Role</option>
               <?php
                  foreach($model as $models)
                  {
                    echo "<option value='".$models->role_id."'>".$models->role_name."</option>";
                  }
                  ?>
               </select></div>
               </td>
               <td>
               <label>New Password</label>
               <div class="input-box"><input type="password" class="form-control newPassword empty" name="newPassword"></div>
               </td>  
               </tr> 
               <tr>
               <td>
               <label>Confirm Password</label>
               <div class="input-box"><input type="password" class="form-control confirm_password empty" name="confirm_password"></div>
               </td>
               <td>
               <label>Status</label>
               <div class="input-box"><div id="tblbrands-enabled">
               <label><input type="radio" name="user_status" value="10" checked=""> Enable</label>
               <label><input type="radio" name="user_status" value="0" > Disable</label>
               </div></div>
               </td>
               </tr>                                                                                                
               </table>
            </div>
         </div>
      </div>
      </div>
      <!--bootstrap modal for adding people end here-->
      <!--bootstrap modal for Updating people starts here-->
      <!-- Modal -->
      <div class="modal modal-child" data-backdrop-limit="1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-modal-parent="#myModal" id="myModalPeopleUpdate">
         <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
               <div class="modal-header">
                  <!--  <button type="button"  class="pull-right">&times;</button>
                     <button type="button" onclick="$('#myModalPeopleAdd').modal('hide')" class="pull-right">&times;</button> -->
                  <button type="button" onclick="$('#myModalPeopleUpdate').modal('hide')" class="pull-right btn btn-primary">&times;</button>
                  <input type="button" class="SavePeopleSubscriberUpdate pull-right btn btn-primary" value="Save" >
                  <h4 class="modal-title">Update System User</h4>
                  <input type="hidden" id="user_idUpdateSystem">
               </div>
               <div class="modal-body">
                  <table class="table">
                     <tr>
                        <td>
                           <label>First Name</label>
                           <input type="text" class="form-control first_name fnameSystem" name="first_name" >
                        </td>
                        <td>
                           <label>Last Name</label>
                           <input type="text" class="form-control last_name lnameSystem" name="last_name" >
                        </td>
                     </tr>
                     <tr>
                        <td>
                           <label>User Name</label> 
                           <input type="text" class="form-control user_name usernameSystem" name="user_name">
                        </td>
                        <td>
                           <label>Email</label>
                           <input type="email" class="form-control email emailSystem" name="email">
                        </td>
                     </tr>
                     <tr>
                        <td>
                           <label>Phone</label>
                           <input type="number" class="form-control phone phoneSystem" name="phone">
                        </td>
                        <td>
                           <label>Mobile</label>
                           <input type="number" class="form-control mobile mobileSystem" name="mobile">
                        </td>
                     </tr>
                     <tr>
                        <td>
                           <label>User Role</label>
                           <select  class="form-control user_role" name="user_role">
                              <option value="">Select User Role</option>
                              <?php
                                 foreach($model as $models)
                                 {
                                   echo "<option value='".$models->role_id."'>".$models->role_name."</option>";
                                 }
                                 ?>
                           </select>
                        </td>
                        <td>
                           <label>New Password</label>
                           <input type="password" class="form-control newPassword" name="newPassword" id="newPassword">
                        </td>
                     </tr>
                     <tr>
                        <td>
                           <label>Confirm Password</label>
                           <input type="password" class="form-control confirm_password" name="confirm_password" id="confirm_password">
                        </td>
                        <td>
                           <label>Status</label>
                           <div id="tblbrands-enabled">
                              <label><input type="radio" name="user_status" value="10"> Enable</label>
                              <label><input type="radio" name="user_status" value="0"> Disable</label>
                           </div>
                        </td>
                     </tr>
                  </table>
               </div>
            </div>
         </div>
      </div>
      <!--bootstrap modal for Updating people end here-->
      <!--bootstrap modal for Editing Setting starts here-->
      <!-- Modal -->
      <div class="modal modal-child" data-backdrop-limit="1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-modal-parent="#myModal" id="myModalsettingsUpdate">
         <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
               <div class="modal-header">
                  <!--  <button type="button"  class="pull-right">&times;</button>
                     <button type="button" onclick="$('#myModalPeopleAdd').modal('hide')" class="pull-right">&times;</button> -->
                  <button type="button" onclick="$('#myModalsettingsUpdate').modal('hide')" class="close-icon pull-right btn btn-primary">&times;</button>
                  <input type="button" class="SaveSettingCompany pull-right btn btn-primary" value="Save" >
                  <h4 class="modal-title">Settings</h4>
                  <input type="hidden" id="user_idUpdateSystem">
               </div>
               <div class="modal-body">
                  <table class="table">
                     <tr>
                        <td>
                           <label>Application Cost</label>
                           <div class="input-box"><input  type="number" step="0.0000000001" class="form-control application_cost_setting" name="application_cost"></div>
                        </td>
                        <td>
                           <label>Profit Mark Up %</label>
                           <div class="input-box"><input  type="number" step="0.0000000001" class="form-control profit_markup_setting" name="profit_markup" ></div>
                        </td>
                     </tr>
                     <tr>
                        <td>
                           <label>Gross Margin %</label>
                           <div class="input-box"><input type="number"  step="0.0000000001" class="form-control gross_margin_percent_setting" name="gross_margin_percent"></div>
                        </td>
                        <td>
                           <label>Default Deposit Percent</label>
                           <div class="input-box"><input type="number" step="0.0000000001" class="form-control default_deposit_percent_setting" name="default_deposit_percent"></div>
                        </td>
                     </tr>
                     <tr>
                        <td>
                           <label>Terms & Conditions</label>
                           <div class="input-box" style="display: none;"><input type="text" class="form-control terms_condition_setting" name="terms_condition"></div>
                           <input type="file" class="form-control terms_condition_settingDoc" name="terms_condition_settingDoc" style="display: none;" accept=".doc, .docx,.txt,.pdf" >
                           <!-- <input type="file" class="form-control terms_condition_settingDoc" name="terms_condition_settingDoc" style="display: none;" accept="application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint,text/plain, application/pdf"> -->
                           <div class="subs-detail upload-img">
                              <div class="subscriberLogoa pull-right viewDoc"><img src="<?php echo Url::base(true) ?>/uploads/cross.png" width="100px" height="100px" id="blah"></div>

                              <div class="subscriberLogo-btn-div">
                                 <!-- <button class="upload pull-right btn btn-primary" style="margin-top: 117px;"></button> -->
                                 <!-- <button class="clickDocUp pull-right btn btn-primary" style="margin-top: 117px;">Upload</button> -->
                                 <button class="clickDocRemove pull-right btn btn-primary" style="margin-top: 117px;">Remove</button>
                              </div>
                              <div class="subscriberLogo-btn-divUpload">
                                 <!-- <button class="upload pull-right btn btn-primary" style="margin-top: 117px;"></button> -->
                                 <button class="clickDocUpp pull-right btn btn-primary" style="margin-top: 117px;">Upload</button>
                              </div>
                           </div>
                           <!-- <button class="button clickDocUp">Upload</button> -->
                           <!-- <button class="viewDoc" data-toggle="modal" data-target="#viewdModal" >View Doc</button> -->
                           <!-- <button class="viewDoc"  >View Doc</button> -->
                            
                        </td>
                        
                        <!-- <td>
                           <label>Xero Consumer Key</label>
                            <div class="input-box"> -->
                        <input type="hidden" class="form-control xero_consumer_key_setting" name="xero_consumer_key">
                        <!-- </div>
                           </td> -->
                        <!-- </tr> -->
                        <!-- <tr> -->
                        <!-- <td>
                           <label>Xero Share Secret</label>
                           <div class="input-box"> -->
                        <input type="hidden" class="form-control xero_share_secret_setting" name="xero_share_secret">
                        <!-- </div>
                           </td>
                           <td>
                             <label>Xero Invoice Line Account Code</label>
                             <div class="input-box"> -->
                        <input type="hidden" class="form-control xero_invoice_line_account_code_setting" name="xero_invoice_line_account_code">
                        <!-- </div>
                           </td> -->
                        <!-- </tr> -->
                        <td>
                           <label>Account Name</label>
                           <div class="input-box"><input type="text" class="form-control account_name_setting" name="account_name"></div>
                            <label>Account BSB</label>
                           <div class="input-box"><input type="text" class="form-control account_BSB_setting" name="account_BSB"></div>
                        </td>
                     <tr>
                       <!--  <td>
                          
                        </td> -->
                        <td>
                           <label>Account Number</label>
                           <div class="input-box"><input type="number" step="0.0000000001" class="form-control account_number_setting" name="account_number"></div>
                        </td>
                         <td>
                           <label>Payment Phone Number</label>
                           <div class="input-box"><input  type="number" step="0.0000000001" class="form-control payment_phone_number_setting" name="payment_phone_number"></div>
                        </td>
                     </tr>
                     <tr>
                       
                        <td>
                           <label>Colour Consultant Item</label>
                           <div class="input-box"><input type="text" class="form-control colour_consultant_item_setting" name="colour_consultant_item"></div>
                        </td>
                        <td>
                           <label>Price Increase %</label>
                           <div class="input-box"><input type="number" class="form-control price_increase_setting" name="price_increase" step="0.0000000001"></div>
                           <input type="hidden" class="form-control gst_sendd" name="gst_sendd">
                        </td>
                     </tr>

                     <!-- <tr> -->
                        
                     <!-- </tr> -->
                  </table>
               </div>
            </div>
         </div>
      </div>
      <!--bootstrap modal for editing setting end here-->
      <!--bootstrap modal for Editing Setting starts here-->
      <!-- Modal -->
      <div class="modal modal-child" data-backdrop-limit="1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-modal-parent="#myModal" id="editEmailSettingquote">
         <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
               <div class="modal-header">
                  <!--  <button type="button"  class="pull-right">&times;</button>
                     <button type="button" onclick="$('#myModalPeopleAdd').modal('hide')" class="pull-right">&times;</button> -->
                  <button type="button" onclick="$('#editEmailSettingquote').modal('hide')" class="close-icon pull-right btn btn-primary">&times;</button>
                  <input type="button" class="saveEmailSettingquote pull-right btn btn-primary" value="Save" >
                  <h4 class="modal-title">Quote Email Settings</h4>
                  <input type="hidden" id="user_idUpdateSystem">
                  <input type="hidden" class="email_setting_id">
               </div>
               <div class="modal-body">
                  <table class="table quoteEmailSettings">
                     <tr>
                        <td>
                           <label>BCC to</label>
                        </td>
                        <td>
                           <input type="email" class="form-control bcc_to_quote" name="bcc_to">
                        </td>
                        <td></td>
                     </tr>
                     <tr>
                        <td>
                           <label>Email Subject</label>
                        </td>
                        <td>
                           <input type="text" class="form-control email_subject_quote" name="email_subject" >
                        </td>
                        <td></td>
                     </tr>
                     <tr>
                        <td>
                           <label>Attached Documents</label>
                        </td>
                        <td>
                           <select class="form-control emailQuoteDocQuote">
                              <option value="" disabled="" selected="">--Select Document--</option>
                              <?php 
                                 foreach ($modelDoc as $key => $doc) {
                                   echo "<option value='".$doc->doc_id."'>".$doc->file_name."</option>";
                                 }
                                 ?>
                           </select>
                        </td>
                        <td></td>
                     </tr>
                  </table>
               </div>
            </div>
         </div>
      </div>
      <!--bootstrap modal for editing setting end here-->
      <!--bootstrap modal for Editing Setting jss starts here-->
      <!-- Modal -->
      <div class="modal modal-child" data-backdrop-limit="1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-modal-parent="#myModal" id="editEmailSettingjss">
         <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
               <div class="modal-header">
                  <!--  <button type="button"  class="pull-right">&times;</button>
                     <button type="button" onclick="$('#myModalPeopleAdd').modal('hide')" class="pull-right">&times;</button> -->
                  <button type="button" onclick="$('#editEmailSettingjss').modal('hide')" class="close-icon pull-right btn btn-primary">&times;</button>
                  <input type="button" class="saveEmailSettingjss pull-right btn btn-primary" value="Save" >
                  <h4 class="modal-title">JSS Email Settings</h4>
                  <input type="hidden" id="user_idUpdateSystem">
                  <input type="hidden" class="email_setting_id">
               </div>
               <div class="modal-body">
                  <table class="table quoteEmailSettings">
                     <tr>
                        <td>
                           <label>BCC to</label>
                        </td>
                        <td>
                           <input type="email" class="form-control bcc_to_jss" name="bcc_to">
                        </td>
                     </tr>
                     <tr>
                        <td>
                           <label>Email Subject</label>
                        </td>
                        <td>
                           <input type="text" class="form-control email_subject_jss" name="email_subject" >
                        </td>
                     </tr>
                     <tr>
                        <td>
                           <label>Attached Documents</label>
                        </td>
                        <td>
                           <select class="form-control emailQuoteDocJss">
                              <option value="" disabled="" selected="">--Select Document--</option>
                              <?php 
                                 foreach ($modelDoc as $key => $doc) {
                                   echo "<option value='".$doc->doc_id."'>".$doc->file_name."</option>";
                                 }
                                 ?>
                           </select>
                        </td>
                     </tr>
                  </table>
               </div>
            </div>
         </div>
      </div>
      <!--bootstrap modal for editing setting jss end here-->
      <!--bootstrap modal for Adding prep starts here-->
      <!-- Modal -->
      <div class="modal modal-child" data-backdrop-limit="1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-modal-parent="#myModal" id="createPrep">
         <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
               <div class="modal-header">
                  <!--  <button type="button"  class="pull-right">&times;</button>
                     <button type="button" onclick="$('#myModalPeopleAdd').modal('hide')" class="pull-right">&times;</button> -->
                  <button type="button" onclick="$('#createPrep').modal('hide')" class="close-icon pull-right btn btn-primary">&times;</button>
                  <input type="button" class="savePrepCreate pull-right btn btn-primary" value="Save" >
                  <h4 class="modal-title">Create Prep</h4>
                  <div class="deletePREP"></div>
                  <input type="hidden" id="user_idUpdateSystem">
               </div>
               <div class="modal-body">
                  <table class="table quoteEmailSettings">
                     <tr>
                        <td>
                           <label>Prep. Level</label>
                        </td>
                        <td>
                           <input type="text" class="form-control prep_level" name="prep_level">
                        </td>
                     </tr>
                     <tr>
                        <td>
                           <label>% uplift to paint cost</label>
                        </td>
                        <td>
                           <input type="number" step="0.0001" class="form-control uplift_cost" name="uplift_cost" >
                        </td>
                     </tr>
                     <tr>
                        <td>
                           <label>% uplift to paint time</label>
                        </td>
                        <td>
                           <input type="number" step="0.0001" class="form-control uplift_time" name="uplift_time" >
                        </td>
                     </tr>
                     <tr>
                        <td>
                           <label>Is_Default</label>
                        </td>
                        <td>
                           <input type="checkbox" class="is_default" name="is_default" >
                        </td>
                     </tr>
                  </table>
               </div>
            </div>
         </div>
      </div>
      <!--bootstrap modal for editing setting jss end here-->
      <!--bootstrap modal for Adding Paint  notes starts here-->
      <div class="modal modal-child" data-backdrop-limit="1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-modal-parent="#myModal" id="createPaymentnotebtn">
         <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
               <div class="modal-header">
                  <!--  <button type="button"  class="pull-right">&times;</button>
                     <button type="button" onclick="$('#myModalPeopleAdd').modal('hide')" class="pull-right">&times;</button> -->
                  <button type="button" onclick="$('#createPaymentnotebtn').modal('hide')" class="close-icon pull-right btn btn-primary">&times;</button>
                  <input type="button" style="margin-left: 20px;" class="savePaymentnoteCreate pull-right btn btn-primary" value="Save" >
                  <h4 class="modal-title">Create Payment Note</h4>
                  <div class="deletePaymentnotebtn"><span class='deletePaymentnote pull-right btn btn-danger'  id='"+id+"'>Delete</span></div>
               </div>
               <div class="modal-body">
                  <table class="table paymentnotes">
                     <tr>
                        <td>
                           <label>Name</label>
                        </td>
                        <td>
                           <input type="text" class="form-control name" name="name" value="">
                           <input type="hidden" class="form-control note_id" name="note_id" value="0">
                        </td>
                     </tr>
                  </table>
               </div>
            </div>
         </div>
      </div>
      <!--bootstrap modal for Paint  notes end here-->
      <!--bootstrap modal for Adding Paint order notes starts here-->
      <div class="modal modal-child" data-backdrop-limit="1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-modal-parent="#myModal" id="createPaintordernotebtn">
         <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
               <div class="modal-header">
                  <!--  <button type="button"  class="pull-right">&times;</button>
                     <button type="button" onclick="$('#myModalPeopleAdd').modal('hide')" class="pull-right">&times;</button> -->
                  <button type="button" onclick="$('#createPaintordernotebtn').modal('hide')" class="close-icon pull-right btn btn-primary">&times;</button>
                  <input type="button" style="margin-left: 20px;" class="savePaintordernoteCreate pull-right btn btn-primary" value="Save" >
                  <h4 class="modal-title">Create Payment Note</h4>
                  <div class="deletePaintordernotebtn"><span class='deletePaintordernote pull-right btn btn-danger'  id='"+id+"'>Delete</span></div>
               </div>
               <div class="modal-body">
                  <table class="table paintordernotes">
                     <tr>
                        <td>
                           <label>Name</label>
                        </td>
                        <td>
                           <input type="text" class="form-control name" name="name" value="">
                           <input type="hidden" class="form-control note_id" name="note_id" value="0">
                        </td>
                     </tr>
                  </table>
               </div>
            </div>
         </div>
      </div>
      <!--bootstrap modal for Paint order notes end here-->
      <!--bootstrap modal for Adding Special Items starts here-->
      <div class="modal modal-child" data-backdrop-limit="1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-modal-parent="#myModal" id="createSpecialItemsbtn">
         <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
               <div class="modal-header">
                  <!--  <button type="button"  class="pull-right">&times;</button>
                     <button type="button" onclick="$('#myModalPeopleAdd').modal('hide')" class="pull-right">&times;</button> -->
                  <button type="button" onclick="$('#createSpecialItemsbtn').modal('hide')" class="close-icon pull-right btn btn-primary">&times;</button>
                  <input type="button" style="margin-left: 20px;" class="saveSpecialItemsCreate pull-right btn btn-primary" value="Save" >
                  <h4 class="modal-title">Add Special Item</h4>
                  <div class="deleteSpecialItemsbtn"><span class='deleteSpecialItems pull-right btn btn-danger'  id='"+id+"'>Delete</span></div>
               </div>
               <div class="modal-body">
                  <table class="table SpecialItems">
                     <tr>
                        <td>
                           <label>Name</label>
                        </td>
                        <td>
                           <input type="text" class="form-control name" name="name" value="">
                           <input type="hidden" class="form-control id" name="id" value="0">
                        </td>
                     </tr>
                     <tr>
                        <td>
                           <label>Price</label>
                        </td>
                        <td>
                           <input type="text" class="form-control price" name="price" value="">
                        </td>
                     </tr>
                  </table>
               </div>
            </div>
         </div>
      </div>
      <!--bootstrap modal for Paint order notes end here-->
      <!--bootstrap modal for Adding Paint Store starts here-->
      <div class="modal modal-child" data-backdrop-limit="1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-modal-parent="#myModal" id="createpaintstorebtn">
         <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
               <div class="modal-header">
                  <!--  <button type="button"  class="pull-right">&times;</button>
                     <button type="button" onclick="$('#myModalPeopleAdd').modal('hide')" class="pull-right">&times;</button> -->
                  <button type="button" onclick="$('#createpaintstorebtn').modal('hide')" class="close-icon pull-right btn btn-primary">&times;</button>
                  <input type="button" style="margin-left: 20px;" class="savepaintstoreCreate pull-right btn btn-primary" value="Save" >
                  <h4 class="modal-title">Paint Store</h4>
                  <div class="deletepaintstorebtn"><span class='deletepaintstore pull-right btn btn-danger'  id=''>Delete</span></div>
               </div>
               <div class="modal-body">
                  <table class="table paintstore">
                     <tr>
                        <td>
                           <label>Address</label>
                        </td>
                        <td>
                           <input type="text" placeholder="Address" class="form-control address" name="address" value="">
                           <input type="text" placeholder="Suburb" class="form-control suburb" name="suburb" value="">
                           <input type="text" placeholder="Postal Code" class="form-control post_code" name="post_code" value="">
                           <input type="hidden" class="form-control store_id" name="store_id" value="0">
                           <input type="hidden" class="form-control subscriber_id" name="subscriber_id" value="0">
                        </td>
                     </tr>
                     <tr>
                        <td>
                           <label>Name</label>
                        </td>
                        <td>
                           <input type="text" class="form-control name" name="name" value="">
                        </td>
                     </tr>
                     <tr>
                        <td>
                           <label>Email</label>
                        </td>
                        <td>
                           <input type="email" class="form-control email" name="email" value="">
                        </td>
                     </tr>
                     <tr>
                        <td>
                           <label>Phone Number</label>
                        </td>
                        <td>
                           <input type="number" class="form-control phone" name="phone" value="">
                        </td>
                     </tr>
                     <tr>
                        <td>
                           <label>Mobile Number</label>
                        </td>
                        <td>
                           <input type="number" class="form-control mobile" name="mobile" value="">
                        </td>
                     </tr>
                  </table>
               </div>
            </div>
         </div>
      </div>
      <!--bootstrap modal for Paint Store end here-->
      <!--bootstrap modal for Adding Special Items starts here-->
      <div class="modal modal-child" data-backdrop-limit="1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-modal-parent="#myModal" id="createcustomersourcebtn">
         <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
               <div class="modal-header">
                  <!--  <button type="button"  class="pull-right">&times;</button>
                     <button type="button" onclick="$('#myModalPeopleAdd').modal('hide')" class="pull-right">&times;</button> -->
                  <button type="button" onclick="$('#createcustomersourcebtn').modal('hide')" class="close-icon pull-right btn btn-primary">&times;</button>
                  <input type="button" style="margin-left: 20px;" class="savecustomersourceCreate pull-right btn btn-primary" value="Save" >
                  <h4 class="modal-title">Customer Source</h4>
                  <div class="deletecustomersourcebtn"><span class='deletecustomersource pull-right btn btn-danger'  id='"+id+"'>Delete</span></div>
               </div>
               <div class="modal-body">
                  <table class="table customersource">
                     <tr>
                        <td>
                           <label>Name</label>
                        </td>
                        <td>
                           <input type="text" class="form-control cs_source" name="cs_source" value="">
                           <input type="hidden" class="form-control cs_id" name="cs_id" value="0">
                           <input type="hidden" class="form-control subscriber_id" name="subscriber_id" value="0">
                        </td>
                     </tr>
                  </table>
               </div>
            </div>
         </div>
      </div>
      <!--bootstrap modal for Paint order notes end here-->
      <!--bootstrap modal for Adding prep starts here-->
      <!-- Modal -->
      <div class="modal modal-child" data-backdrop-limit="1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-modal-parent="#myModal" id="editSubModal">
         <div class="modal-dialog" style="border: 1px solid black; overflow: scroll;">
            <!-- Modal content-->
            <div class="modal-content">
               <div class="modal-header">
                  <!--  <button type="button"  class="pull-right">&times;</button>
                     <button type="button" onclick="$('#myModalPeopleAdd').modal('hide')" class="pull-right">&times;</button> -->
                  <button type="button" onclick="$('#editSubModal').modal('hide')" class="close-icon pull-right btn btn-primary">&times;</button>
                  <input type="button" class="updateSub pull-right btn btn-primary" value="Save" >
                  <h4 class="modal-title">Edit Subscriber</h4>
                  <div class="deletePREP"></div>
                  <input type="hidden" id="user_idUpdateSub">
               </div>
               <div class="modal-body">
                  <table class="table quoteEmailSettings">
                     <tr>
                        <td>
                           <label>Name</label>
                           <input type="input" class="form-control sub_name" name="sub_name">
                        </td>
                        <td>
                           <label>Address</label>
                           <input type="input" class="form-control sub_address" name="sub_address" >
                        </td>
                     </tr>
                     <tr>
                        <td>
                           <label>Suburb</label>
                           <input type="input" class="form-control sub_suburb" name="sub_suburb" >
                        </td>
                        <td>
                           <label>State</label>
                           <input type="input" class="form-control sub_state" name="sub_state" >
                        </td>
                     </tr>
                     <tr>
                        <td>
                           <label>Postcode</label>
                           <input type="input" class="form-control sub_postcode" name="sub_postcode" >
                        </td>
                        <td>
                           <label>Country</label>
                           <input type="input" class="form-control sub_country" name="sub_country" >
                        </td>
                     </tr>
                     <tr>
                        <td>
                           <label>Phone</label>
                           <input type="input" class="form-control sub_phone" name="sub_phone" >
                        </td>
                        <td>
                           <label>Email</label>
                           <input type="input" class="form-control sub_email" name="sub_email" >
                        </td>
                     </tr>
                     <tr>
                        <td>
                           <label>ABN</label>
                           <input type="input" placeholder="12 123 456" class="form-control sub_abn" name="sub_abn" >
                        </td>
                        <td>
                           <label>Insurance</label>
                           <input type="input"  placeholder="Insurance Co Name & Policy Number" class="form-control sub_insurance_comp_detail" name="sub_insurance_comp_detail" >
                        </td>
                     </tr>
                     <tr>
                        <td>
                           <label>Workers Insurance</label>
                           <input type="input" placeholder="Insurance Co Name & Policy Number" class="form-control sub_worker_insurance_detail" name="sub_worker_insurance_detail" >
                        </td>
                        <td>
                           <label>Licence Number</label>
                           <input type="input" placeholder="eg BLD 12345" class="form-control sub_license_number" name="sub_license_number" >
                        </td>
                     </tr>
                     <tr>
                        <td>
                           <label>Paint Brand</label>
                           <select class="form-control sub_paint_brand" name="paint_brand"></select>
                           <!-- <input type="input" class="form-control paint_brand" name="paint_brand" > -->
                        </td>
                        <td>
                           <label>Time Zone</label>
                           <?php
                              $allZones = '{"timeZones": ["Africa/Abidjan","Africa/Accra","Africa/Addis_Ababa","Africa/Algiers","Africa/Asmara","Africa/Asmera","Africa/Bamako","Africa/Bangui","Africa/Banjul","Africa/Bissau","Africa/Blantyre","Africa/Brazzaville","Africa/Bujumbura","Africa/Cairo","Africa/Casablanca","Africa/Ceuta","Africa/Conakry","Africa/Dakar","Africa/Dar_es_Salaam","Africa/Djibouti","Africa/Douala","Africa/El_Aaiun","Africa/Freetown","Africa/Gaborone","Africa/Harare","Africa/Johannesburg","Africa/Juba","Africa/Kampala","Africa/Khartoum","Africa/Kigali","Africa/Kinshasa","Africa/Lagos","Africa/Libreville","Africa/Lome","Africa/Luanda","Africa/Lubumbashi","Africa/Lusaka","Africa/Malabo","Africa/Maputo","Africa/Maseru","Africa/Mbabane","Africa/Mogadishu","Africa/Monrovia","Africa/Nairobi","Africa/Ndjamena","Africa/Niamey","Africa/Nouakchott","Africa/Ouagadougou","Africa/Porto-Novo","Africa/Sao_Tome","Africa/Timbuktu","Africa/Tripoli","Africa/Tunis","Africa/Windhoek","America/Adak","America/Anchorage","America/Anguilla","America/Antigua","America/Araguaina","America/Argentina/Buenos_Aires","America/Argentina/Catamarca","America/Argentina/ComodRivadavia","America/Argentina/Cordoba","America/Argentina/Jujuy","America/Argentina/La_Rioja","America/Argentina/Mendoza","America/Argentina/Rio_Gallegos","America/Argentina/Salta","America/Argentina/San_Juan","America/Argentina/San_Luis","America/Argentina/Tucuman","America/Argentina/Ushuaia","America/Aruba","America/Asuncion","America/Atikokan","America/Atka","America/Bahia","America/Bahia_Banderas","America/Barbados","America/Belem","America/Belize","America/Blanc-Sablon","America/Boa_Vista","America/Bogota","America/Boise","America/Buenos_Aires","America/Cambridge_Bay","America/Campo_Grande","America/Cancun","America/Caracas","America/Catamarca","America/Cayenne","America/Cayman","America/Chicago","America/Chihuahua","America/Coral_Harbour","America/Cordoba","America/Costa_Rica","America/Creston","America/Cuiaba","America/Curacao","America/Danmarkshavn","America/Dawson","America/Dawson_Creek","America/Denver","America/Detroit","America/Dominica","America/Edmonton","America/Eirunepe","America/El_Salvador","America/Ensenada","America/Fort_Nelson","America/Fort_Wayne","America/Fortaleza","America/Glace_Bay","America/Godthab","America/Goose_Bay","America/Grand_Turk","America/Grenada","America/Guadeloupe","America/Guatemala","America/Guayaquil","America/Guyana","America/Halifax","America/Havana","America/Hermosillo","America/Indiana/Indianapolis","America/Indiana/Knox","America/Indiana/Marengo","America/Indiana/Petersburg","America/Indiana/Tell_City","America/Indiana/Vevay","America/Indiana/Vincennes","America/Indiana/Winamac","America/Indianapolis","America/Inuvik","America/Iqaluit","America/Jamaica","America/Jujuy","America/Juneau","America/Kentucky/Louisville","America/Kentucky/Monticello","America/Knox_IN","America/Kralendijk","America/La_Paz","America/Lima","America/Los_Angeles","America/Louisville","America/Lower_Princes","America/Maceio","America/Managua","America/Manaus","America/Marigot","America/Martinique","America/Matamoros","America/Mazatlan","America/Mendoza","America/Menominee","America/Merida","America/Metlakatla","America/Mexico_City","America/Miquelon","America/Moncton","America/Monterrey","America/Montevideo","America/Montreal","America/Montserrat","America/Nassau","America/New_York","America/Nipigon","America/Nome","America/Noronha","America/North_Dakota/Beulah","America/North_Dakota/Center","America/North_Dakota/New_Salem","America/Ojinaga","America/Panama","America/Pangnirtung","America/Paramaribo","America/Phoenix","America/Port-au-Prince","America/Port_of_Spain","America/Porto_Acre","America/Porto_Velho","America/Puerto_Rico","America/Punta_Arenas","America/Rainy_River","America/Rankin_Inlet","America/Recife","America/Regina","America/Resolute","America/Rio_Branco","America/Rosario","America/Santa_Isabel","America/Santarem","America/Santiago","America/Santo_Domingo","America/Sao_Paulo","America/Scoresbysund","America/Shiprock","America/Sitka","America/St_Barthelemy","America/St_Johns","America/St_Kitts","America/St_Lucia","America/St_Thomas","America/St_Vincent","America/Swift_Current","America/Tegucigalpa","America/Thule","America/Thunder_Bay","America/Tijuana","America/Toronto","America/Tortola","America/Vancouver","America/Virgin","America/Whitehorse","America/Winnipeg","America/Yakutat","America/Yellowknife","Antarctica/Casey","Antarctica/Davis","Antarctica/DumontDUrville","Antarctica/Macquarie","Antarctica/Mawson","Antarctica/McMurdo","Antarctica/Palmer","Antarctica/Rothera","Antarctica/South_Pole","Antarctica/Syowa","Antarctica/Troll","Antarctica/Vostok","Arctic/Longyearbyen","Asia/Aden","Asia/Almaty","Asia/Amman","Asia/Anadyr","Asia/Aqtau","Asia/Aqtobe","Asia/Ashgabat","Asia/Ashkhabad","Asia/Atyrau","Asia/Baghdad","Asia/Bahrain","Asia/Baku","Asia/Bangkok","Asia/Barnaul","Asia/Beirut","Asia/Bishkek","Asia/Brunei","Asia/Calcutta","Asia/Chita","Asia/Choibalsan","Asia/Chongqing","Asia/Chungking","Asia/Colombo","Asia/Dacca","Asia/Damascus","Asia/Dhaka","Asia/Dili","Asia/Dubai","Asia/Dushanbe","Asia/Famagusta","Asia/Gaza","Asia/Harbin","Asia/Hebron","Asia/Ho_Chi_Minh","Asia/Hong_Kong","Asia/Hovd","Asia/Irkutsk","Asia/Istanbul","Asia/Jakarta","Asia/Jayapura","Asia/Jerusalem","Asia/Kabul","Asia/Kamchatka","Asia/Karachi","Asia/Kashgar","Asia/Kathmandu","Asia/Katmandu","Asia/Khandyga","Asia/Kolkata","Asia/Krasnoyarsk","Asia/Kuala_Lumpur","Asia/Kuching","Asia/Kuwait","Asia/Macao","Asia/Macau","Asia/Magadan","Asia/Makassar","Asia/Manila","Asia/Muscat","Asia/Nicosia","Asia/Novokuznetsk","Asia/Novosibirsk","Asia/Omsk","Asia/Oral","Asia/Phnom_Penh","Asia/Pontianak","Asia/Pyongyang","Asia/Qatar","Asia/Qyzylorda","Asia/Rangoon","Asia/Riyadh","Asia/Saigon","Asia/Sakhalin","Asia/Samarkand","Asia/Seoul","Asia/Shanghai","Asia/Singapore","Asia/Srednekolymsk","Asia/Taipei","Asia/Tashkent","Asia/Tbilisi","Asia/Tehran","Asia/Tel_Aviv","Asia/Thimbu","Asia/Thimphu","Asia/Tokyo","Asia/Tomsk","Asia/Ujung_Pandang","Asia/Ulaanbaatar","Asia/Ulan_Bator","Asia/Urumqi","Asia/Ust-Nera","Asia/Vientiane","Asia/Vladivostok","Asia/Yakutsk","Asia/Yangon","Asia/Yekaterinburg","Asia/Yerevan","Atlantic/Azores","Atlantic/Bermuda","Atlantic/Canary","Atlantic/Cape_Verde","Atlantic/Faeroe","Atlantic/Faroe","Atlantic/Jan_Mayen","Atlantic/Madeira","Atlantic/Reykjavik","Atlantic/South_Georgia","Atlantic/St_Helena","Atlantic/Stanley","Australia/ACT","Australia/Adelaide","Australia/Brisbane","Australia/Broken_Hill","Australia/Canberra","Australia/Currie","Australia/Darwin","Australia/Eucla","Australia/Hobart","Australia/LHI","Australia/Lindeman","Australia/Lord_Howe","Australia/Melbourne","Australia/NSW","Australia/North","Australia/Perth","Australia/Queensland","Australia/South","Australia/Sydney","Australia/Tasmania","Australia/Victoria","Australia/West","Australia/Yancowinna","Brazil/Acre","Brazil/DeNoronha","Brazil/East","Brazil/West","CET","CST6CDT","Canada/Atlantic","Canada/Central","Canada/East-Saskatchewan","Canada/Eastern","Canada/Mountain","Canada/Newfoundland","Canada/Pacific","Canada/Saskatchewan","Canada/Yukon","Chile/Continental","Chile/EasterIsland","Cuba","EET","EST5EDT","Egypt","Eire","Etc/GMT","Etc/GMT+0","Etc/GMT+1","Etc/GMT+10","Etc/GMT+11","Etc/GMT+12","Etc/GMT+2","Etc/GMT+3","Etc/GMT+4","Etc/GMT+5","Etc/GMT+6","Etc/GMT+7","Etc/GMT+8","Etc/GMT+9","Etc/GMT-0","Etc/GMT-1","Etc/GMT-10","Etc/GMT-11","Etc/GMT-12","Etc/GMT-13","Etc/GMT-14","Etc/GMT-2","Etc/GMT-3","Etc/GMT-4","Etc/GMT-5","Etc/GMT-6","Etc/GMT-7","Etc/GMT-8","Etc/GMT-9","Etc/GMT0","Etc/Greenwich","Etc/UCT","Etc/UTC","Etc/Universal","Etc/Zulu","Europe/Amsterdam","Europe/Andorra","Europe/Astrakhan","Europe/Athens","Europe/Belfast","Europe/Belgrade","Europe/Berlin","Europe/Bratislava","Europe/Brussels","Europe/Bucharest","Europe/Budapest","Europe/Busingen","Europe/Chisinau","Europe/Copenhagen","Europe/Dublin","Europe/Gibraltar","Europe/Guernsey","Europe/Helsinki","Europe/Isle_of_Man","Europe/Istanbul","Europe/Jersey","Europe/Kaliningrad","Europe/Kiev","Europe/Kirov","Europe/Lisbon","Europe/Ljubljana","Europe/London","Europe/Luxembourg","Europe/Madrid","Europe/Malta","Europe/Mariehamn","Europe/Minsk","Europe/Monaco","Europe/Moscow","Europe/Nicosia","Europe/Oslo","Europe/Paris","Europe/Podgorica","Europe/Prague","Europe/Riga","Europe/Rome","Europe/Samara","Europe/San_Marino","Europe/Sarajevo","Europe/Saratov","Europe/Simferopol","Europe/Skopje","Europe/Sofia","Europe/Stockholm","Europe/Tallinn","Europe/Tirane","Europe/Tiraspol","Europe/Ulyanovsk","Europe/Uzhgorod","Europe/Vaduz","Europe/Vatican","Europe/Vienna","Europe/Vilnius","Europe/Volgograd","Europe/Warsaw","Europe/Zagreb","Europe/Zaporozhye","Europe/Zurich","GB","GB-Eire","GMT","GMT0","Greenwich","Hongkong","Iceland","Indian/Antananarivo","Indian/Chagos","Indian/Christmas","Indian/Cocos","Indian/Comoro","Indian/Kerguelen","Indian/Mahe","Indian/Maldives","Indian/Mauritius","Indian/Mayotte","Indian/Reunion","Iran","Israel","Jamaica","Japan","Kwajalein","Libya","MET","MST7MDT","Mexico/BajaNorte","Mexico/BajaSur","Mexico/General","NZ","NZ-CHAT","Navajo","PRC","PST8PDT","Pacific/Apia","Pacific/Auckland","Pacific/Bougainville","Pacific/Chatham","Pacific/Chuuk","Pacific/Easter","Pacific/Efate","Pacific/Enderbury","Pacific/Fakaofo","Pacific/Fiji","Pacific/Funafuti","Pacific/Galapagos","Pacific/Gambier","Pacific/Guadalcanal","Pacific/Guam","Pacific/Honolulu","Pacific/Johnston","Pacific/Kiritimati","Pacific/Kosrae","Pacific/Kwajalein","Pacific/Majuro","Pacific/Marquesas","Pacific/Midway","Pacific/Nauru","Pacific/Niue","Pacific/Norfolk","Pacific/Noumea","Pacific/Pago_Pago","Pacific/Palau","Pacific/Pitcairn","Pacific/Pohnpei","Pacific/Ponape","Pacific/Port_Moresby","Pacific/Rarotonga","Pacific/Saipan","Pacific/Samoa","Pacific/Tahiti","Pacific/Tarawa","Pacific/Tongatapu","Pacific/Truk","Pacific/Wake","Pacific/Wallis","Pacific/Yap","Poland","Portugal","ROK","Singapore","SystemV/AST4","SystemV/AST4ADT","SystemV/CST6","SystemV/CST6CDT","SystemV/EST5","SystemV/EST5EDT","SystemV/HST10","SystemV/MST7","SystemV/MST7MDT","SystemV/PST8","SystemV/PST8PDT","SystemV/YST9","SystemV/YST9YDT","Turkey","UCT","US/Alaska","US/Aleutian","US/Arizona","US/Central","US/East-Indiana","US/Eastern","US/Hawaii","US/Indiana-Starke","US/Michigan","US/Mountain","US/Pacific","US/Pacific-New","US/Samoa","UTC","Universal","W-SU","WET","Zulu"]}';
                              $allzones = json_decode($allZones);
                              ?>
                           <select class="form-control sub_timezone" name="timezone">
                           <?php
                              // print_r($allzones);die;
                                foreach ($allzones->timeZones as $zone) {
                                  echo "<option value='".$zone."'>".$zone."</option>";
                                }
                              ?>
                           </select>
                           <!-- <input type="input" class="form-control timezone" name="timezone" > -->
                        </td>
                     </tr>
                     <tr>
                        <td>
                           <label>Website</label>
                           <input type="input" class="form-control sub_website" name="sub_website" >
                        </td>
                        <td>
                           <label>Status</label>
                           <div  class="brands-tr-div">
                              <p><input type="radio" class="form-control sub_status-1" value="1" name="sub_status" >Enable</p>
                              <p><input type="radio" class="form-control sub_status-0" value="0" name="sub_status" >Disable</p>
                           </div>
                        </td>
                     </tr>
                     <tr>
                        <td>
                           <label>GST</label>
                           <input type="numbert" min="0" max="100" value="0" placeholder="Enter GST value" class="form-control sub_GST" name="sub_GST" >
                        </td>
                     </tr>
                  </table>
               </div>
            </div>
         </div>
      </div>
      <!--bootstrap modal for editing setting jss end here-->
      <!--bootstrap modal for Adding prep starts here-->
      <!-- Modal -->
      <div class="modal modal-child" data-backdrop-limit="1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-modal-parent="#myModal" id="UpdatePrep">
         <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
               <div class="modal-header">
                  <!--  <button type="button"  class="pull-right">&times;</button>
                     <button type="button" onclick="$('#myModalPeopleAdd').modal('hide')" class="pull-right">&times;</button> -->
                  <button type="button" onclick="$('#UpdatePrep').modal('hide')" class="close-icon pull-right btn btn-primary">&times;</button>
                  <input type="button" class="savePrepUpdate pull-right btn btn-primary" value="Save" >
                  <input type="hidden" class="idprep">
                  <h4 class="modal-title">Create Prep</h4>
                  <div class="deletePREP"></div>
                  <input type="hidden" id="user_idUpdateSystem">
               </div>
               <div class="modal-body">
                  <table class="table quoteEmailSettings">
                     <tr>
                        <td>
                           <label>Prep. Level</label>
                        </td>
                        <td>
                           <input type="text" class="form-control prep_levelupdate" name="prep_level">
                        </td>
                     </tr>
                     <tr>
                        <td>
                           <label>% uplift to paint cost</label>
                        </td>
                        <td>
                           <input type="number"  step="0.00001" class="form-control uplift_costupdate" name="uplift_cost" >
                        </td>
                     </tr>
                     <tr>
                        <td>
                           <label>% uplift to paint time</label>
                        </td>
                        <td>
                           <input type="number"step="0.00001" class="form-control uplift_timeupdate" name="uplift_time" >
                        </td>
                     </tr>
                     <tr>
                        <td>
                           <label>Is_Default</label>
                        </td>
                        <td>
                           <input type="checkbox" class="is_defaultupdate" name="is_default" >
                        </td>
                     </tr>
                  </table>
               </div>
            </div>
         </div>
      </div>
      <!--bootstrap modal for editing setting jss end here-->
      <!--bootstrap modal for previewing documents start here-->
      <div class="modal modal-child" data-backdrop-limit="1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-modal-parent="#myModal" id="mydocModal">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close-icon" onclick="$('#mydocModal').modal('hide')">&times;</button>
                  <div id="pdf_name"></div>
               </div>
               <div class="modal-body">
                  <div id="pdffile_load"></div>
               </div>
               <div class="modal-footer">
                  <input type="hidden" name="hidden" id="hiddenId">
                  <button type="button" id="myBtn" onclick="$('#mydocModal').modal('hide')">Close</button>
               </div>
            </div>
         </div>
      </div>
      <!--bootstrap modal for previewing documents end here-->
      <!--bootstrap modal for creating room type-->
      <div class="modal modal-child" data-backdrop-limit="1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-modal-parent="#myModal" id="myroomModal">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" onclick="$('#myroomModal').modal('hide')" class="close-icon pull-right btn btn-primary">&times;</button>
                  <input type="button" class="saveRoom pull-right btn btn-primary" value="Save" >
                  <input type="hidden" class="idprep">
                  <h4 class="modal-title">Create Room Type</h4>
                  <div class="deletePREP"></div>
                  <input type="hidden" >
               </div>
               <div class="modal-body">
                  <table class="table roomTYPE">
                     <tr>
                        <td>
                           <label>Name</label>
                        </td>
                        <td>
                           <input type="text" class="form-control room_name" name="name">
                        </td>
                     </tr>
                     <tr>
                        <td>
                           <label>Room Type</label>
                        </td>
                        <td>
                           <select class="room_type form-control">
                              <option value=''>Select Room Type</option>
                              <?php 
                                 // echo "<pre>";
                                    foreach($modelProductType as $key=>$value)
                                    {
                                      //print_r($value->name);
                                      echo "<option value='".$value->type_id."'>".$value->name."</option>";
                                    }
                                  ?>
                           </select>
                        </td>
                     </tr>
                     <tr>
                        <td>
                           <label>multiples</label>
                        </td>
                        <td>
                           <input type="text" class="form-control room_multiples" name="multiples">
                        </td>
                     </tr>
                  </table>
               </div>
               <div class="modal-footer">
                  <!-- <button type="button" id="myBtn" onclick="$('#myroomModal').modal('hide')">Close</button> -->
               </div>
            </div>
         </div>
      </div>
      <!--bootstrap modal for creating room type end here-->
      <!--bootstrap modal for creating room type-->
      <div class="modal modal-child" data-backdrop-limit="1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-modal-parent="#myModal" id="myappointmentTypeModal">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" onclick="$('#myappointmentTypeModal').modal('hide')" class="close-icon pull-right btn btn-primary">&times;</button>
                  <input type="button" class="saveAppointmentType pull-right btn btn-primary" value="Save" >
                  <input type="hidden" class="idprep">
                  <h4 class="modal-title">Create Appointment Type</h4>
                  <div class="deletePREP"></div>
                  <input type="hidden" >
               </div>
               <div class="modal-body">
                  <table class="table appointmentTYPE">
                     <tr>
                        <td>
                           <label>Name</label>
                        </td>
                        <td>
                           <input type="text" class="form-control appointmentTypeName" name="name">
                           <input type="hidden" class="form-control appointmentTypeId" value="0" name="id">
                        </td>
                     </tr>
                     <tr style="display: none;" class="delAppointmentTypeRow">
                        <td colspan="2">
                           <button class="delAppointmenttype" data-id="0">Delete</button>              
                        </td>
                     </tr>
                  </table>
               </div>
               <div class="modal-footer">
                  <!-- <button type="button" id="myBtn" onclick="$('#myroomModal').modal('hide')">Close</button> -->
               </div>
            </div>
         </div>
      </div>
      <!--bootstrap modal for creating room type end here-->
      <div class="modal modal-child" data-backdrop-limit="1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-modal-parent="#myModal" id="myroomModalUpdate">
         <div class="modal-dialog">
            <div class="modal-content">
               <!--     <div class="modal-header">
                  <button type="button" onclick="$('#myroomModal').modal('hide')">&times;</button>    </div>   -->
               <div class="modal-header">
                  <button type="button" onclick="$('#myroomModalUpdate').modal('hide')" class="close-icon pull-right btn btn-primary">&times;</button>
                  <input type="button" class="saveRoomUpdate pull-right btn btn-primary" value="Save" >
                  <input type="hidden" class="idprep">
                  <h4 class="modal-title">Update Room Type</h4>
                  <input type="hidden" >
               </div>
               <input type="hidden" class="room_id_update"
               <div class="modal-body">
                  <table class="table roomTYPE">
                     <tr>
                        <td>
                           <label>Name</label>
                        </td>
                        <td>
                           <input type="text" class="form-control room_name_update" name="name">
                        </td>
                     </tr>
                     <tr>
                        <td>
                           <label>Room Type</label>
                        </td>
                        <td>
                           <select class="room_type_update form-control">
                              <option value=''>Select Room Type</option>
                              <?php 
                                 // echo "<pre>";
                                    foreach($modelProductType as $key=>$value)
                                    {
                                      //print_r($value->name);
                                      echo "<option value='".$value->type_id."'>".$value->name."</option>";
                                    }
                                  ?>
                           </select>
                        </td>
                     </tr>
                     <tr>
                        <td>
                           <label>multiples</label>
                        </td>
                        <td>
                           <input type="text" class="form-control room_multiples_update" name="multiples">
                        </td>
                     </tr>
                  </table>
               </div>
               <div class="modal-footer">
                  <!-- <button type="button" id="myBtn" onclick="$('#myroomModal').modal('hide')">Close</button> -->
               </div>
            </div>
         </div>
      </div>
      <div id="insertimageModal" class="modal" role="dialog">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close-icon close imageDismiss" >&times;</button>
                  <button type="button" class="pull-right btn-success crop_image">&#10003;</button>
                  <h4 class="modal-title">Cropper</h4>
               </div>
               <div class="modal-body">
                  <div class="row">
                     <div class="col-md-12 text-center">
                        <div id="image_demo"></div>
                     </div>
                  </div>
               </div>
               <!--  <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div> -->
            </div>
         </div>
      </div>
      <!--bootstrap modal for creating room type end here-->
   </body>
</html>

<div class="modal fade" id="viewdModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">View Doc</h5>
        <button type="button" class="close this-modal-die"  aria-label="Close">
          <span aria-hidden="true" class="">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <iframe id="openDocfile" src=""></iframe>
      </div>
      <!-- <div class="modal-footer"> -->
        <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->
      <!-- </div> -->
    </div>
  </div>
</div>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.css">
  
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.js"></script>
<script>
   var pdfLink = '';
   $('.this-modal-die').on('click',function(){
      $('#viewdModal').modal('hide')
   })
   $(document).on('click','.viewDoc',function(){
      window.open(pdfLink, '_blank'); return false;
   })
   $('.showLoader').show();
   $('.bodyMain').hide();
    var company_id = "<?php echo $_GET['id'];?>";
    var subscriber_idd = $('.subscriber_id').val();
   var data ={"json":'{"company_id":"'+company_id+'"}'};
   //var da = '{"company_id":"<?php //echo $_GET['id'];?>"}';
   // console.log({"json":da})
   // console.log(data);
   var siteBaseUrl = "<?php echo Url::base(true);?>";
   //console.log(siteBaseUrl);
   
   $.ajax({
   url:  "//<?php echo $serverName;?>/paintpad/webservicecopy/all-company-data",
   type: "POST",
   data: data, // Send the object.
     success: function(response) {
     	var parsed = JSON.parse(response);
         console.log(parsed);
        	$('.upload').text('Upload');
          $('.uploadPdfLogo').text('Upload Logo');
        	$('.imageChange').text('Change Image')
          $('.imageChangePdf').text('Change PDF Image')
          $('.imageChangeAccredate').text('Change Certified Image')
          $('.subscriberName').html("<h3>Name: </h3><label>"+parsed.data.compDetail.name+"</label>");
          $('.subscriberpaint_GST').html("<h3>GST: </h3><label>"+parsed.data.companySetting.gst+"</label>");
          $('.subscriberpaint_GST').attr("data-GST",parsed.data.companySetting.gst);
          $('.gst_sendd').attr("value",parsed.data.companySetting.gst);
        	$('.subscriberName').data("name",parsed.data.compDetail.name);
          $('.subscriberName').attr("data-name",parsed.data.compDetail.name);
   
        	$('.subscriberAddress').html("<h3>Address: </h3><label>"+parsed.data.compDetail.address+"</label>");
          $('.subscriberAddress').attr("data-address",parsed.data.compDetail.address);
   
        	$('.subscriberSuburb').html("<h3>Suburb: </h3><label>"+parsed.data.compDetail.suburb+"</label>");
          $('.subscriberSuburb').attr("data-suburb",parsed.data.compDetail.suburb);
   
        	$('.subscriberState').html("<h3>State: </h3><label>"+parsed.data.compDetail.state+" "+parsed.data.compDetail.postcode+ "</label>");
          $('.subscriberState').attr("data-state",parsed.data.compDetail.state);
          $('.subscriberState').attr("data-postcode",parsed.data.compDetail.postcode);
   
        	$('.subscriberCountry').html("<h3>Country: </h3><label>"+parsed.data.compDetail.country+"</label>");
          $('.subscriberCountry').attr("data-country",parsed.data.compDetail.country);
   
        	$('.subscriberPhone').html("<h3>Phone: </h3><label>"+parsed.data.compDetail.phone+"</label>");
          $('.subscriberPhone').attr("data-phone",parsed.data.compDetail.phone);
   
        	$('.subscriberEmail').html("<h3>Email: </h3><label>"+parsed.data.compDetail.email+"</label>");
          $('.subscriberEmail').attr("data-email",parsed.data.compDetail.email);
   
          $('.subscriberAbn').html("<h3>ABN: </h3><label>"+parsed.data.compDetail.abn+"</label>");
          $('.subscriberAbn').attr("data-abn",parsed.data.compDetail.abn);
   
          $('.subscriberinsurance_comp_detail').html("<h3>Insurance: </h3><label>"+parsed.data.compDetail.insurance_comp_detail+"</label>");
          $('.subscriberinsurance_comp_detail').attr("data-insurance_comp_detail",parsed.data.compDetail.insurance_comp_detail);
   
          $('.subscriberworker_insurance_detail').html("<h3>Workers Insurance: </h3><label>"+parsed.data.compDetail.worker_insurance_detail+"</label>");
          $('.subscriberworker_insurance_detail').attr("data-worker_insurance_detail",parsed.data.compDetail.worker_insurance_detail);
   
          $('.subscriberlicense_number').html("<h3>Licence Number: </h3><label>"+parsed.data.compDetail.license_number+"</label>");
          $('.subscriberlicense_number').attr("data-license_number",parsed.data.compDetail.license_number);
   
          $('.subscriberpaint_brand_name').html("<h3>Paint Brand: </h3><label>"+parsed.data.compDetail.paint_brand_name+"</label>");
   
          $('.subscriberpaint_brand').attr("data-paint_brand",parsed.data.compDetail.paint_brand);
   
        	$('.subscribertimezone').html("<h3>Timezone: </h3><label>"+parsed.data.compDetail.timezone+"</label>");
          $('.subscribertimezone').attr("data-timezone",parsed.data.compDetail.timezone);
   
          $('.subscriberWebsite').html("<h3>Website: </h3><label><a>"+parsed.data.compDetail.website+"</a></label>");
          $('.subscriberWebsite').attr("data-website",parsed.data.compDetail.website);
   
          var sub_status = (parsed.data.compDetail.status == 1)?'Enable':'Disable';
          $('.subscriberstatus').html("<h3>Status: </h3><label>"+sub_status+"</label>");
          $('.subscriberstatus').attr("data-status",parsed.data.compDetail.status);
   
        //	console.log(parsed.data.compDetail.logo);
        	if(parsed.data.compDetail.logo=="")
        	{
        		parsed.data.compDetail.logo="<?php  echo Url::base(true); ?>/uploads/no_image.png";
        	}
          $('.subscriberLogo').html("<img src='"+parsed.data.compDetail.logo+"' width='100px' height='100px' id='blah'>");
          // console.log(parsed.data.compDetail);
          $('.logoPdf').html("<img src='"+parsed.data.compDetail.pdflogo+"' id='pdfLogo' alt=''>");
          $('.logoAccredate').html("<img src='"+parsed.data.compDetail.accredatelogo+"' id='accredateLogo'>");
   
   
   
          ///settings fill up starts here
          $('.account_name').text(parsed.data.companySetting.account_name);
          $('.application_cost').text(parsed.data.companySetting.application_cost);
          $('.profit_markup').text(parsed.data.companySetting.profit_markup);
          $('.gross_margin_percent').text(parsed.data.companySetting.gross_margin_percent); 

          $('#openDocfile').attr('src',parsed.data.companySetting.terms_conditionDoc); 

          $('.default_deposit_percent').text(parsed.data.companySetting.default_deposit_percent);
          $('.terms_condition').text(parsed.data.companySetting.terms_condition);
          $('.xero_consumer_key').text(parsed.data.companySetting.xero_consumer_key); 
          $('.xero_share_secret').text(parsed.data.companySetting.xero_share_secret);
          $('.xero_invoice_line_account_code').text(parsed.data.companySetting.xero_invoice_line_account_code); 
          $('.account_BSB').text(parsed.data.companySetting.account_BSB);
          $('.account_number').text(parsed.data.companySetting.account_number); 
          $('.payment_phone_number').text(parsed.data.companySetting.payment_phone_number);
          $('.colour_consultant_item').text(parsed.data.companySetting.colour_consultant_item);  
          if (parsed.data.companySetting.quickbook_access_token == '') {
            $('.quickbook_status').text('Not Connected'); 
          }
          else{
            $('.quickbook_status').text('Connected'); 
          }
          $('.price_increase').text(parsed.data.companySetting.price_increase);  

          if (parsed.data.companySetting.terms_conditionDoc == '' || parsed.data.companySetting.terms_conditionDoc == null || parsed.data.companySetting.terms_conditionDoc == undefined) {
            $(".subscriberLogoa").removeClass("viewDoc");
            $('.Docddd').text('---');  
            $('.viewDoc').hide()
            $('.clickDocRemove').hide()
            
            
          }
          else{
            $(".subscriberLogoa").addClass("viewDoc");
            $('.Docddd').html('<button class="viewDoc"  >View Doc</button>'); 
            $('.viewDoc').show() 
            $('.clickDocRemove').show()
            pdfLink = parsed.data.companySetting.terms_conditionDoc
            var val = parsed.data.companySetting.terms_conditionDoc;
            var myString = val.substr(val.indexOf(".") + 1)
            var baseUrl   = window.location.origin;
            if (myString == 'doc' || myString == 'docx') {
               $('.subscriberLogoa img').attr('src',baseUrl+'/paintpad/uploads/document.png')
            }
            else{
               $('.subscriberLogoa img').attr('src',baseUrl+'/paintpad/uploads/pdf-doc.png')
               
            }
            
             
          }
          
   
   
          $('.updateSettings').attr('account_name',parsed.data.companySetting.account_name) ;
          $('.updateSettings').attr('application_cost',parsed.data.companySetting.application_cost) ;
          $('.updateSettings').attr('profit_markup',parsed.data.companySetting.profit_markup) ;
          $('.updateSettings').attr('gross_margin_percent',parsed.data.companySetting.gross_margin_percent) ;
          $('.updateSettings').attr('default_deposit_percent',parsed.data.companySetting.default_deposit_percent) ;
          $('.updateSettings').attr('terms_condition',parsed.data.companySetting.terms_condition) ;
          $('.updateSettings').attr('xero_consumer_key',parsed.data.companySetting.xero_consumer_key) ;
          $('.updateSettings').attr('xero_share_secret',parsed.data.companySetting.xero_share_secret) ;
          $('.updateSettings').attr('xero_invoice_line_account_code',parsed.data.companySetting.xero_invoice_line_account_code) ;
          $('.updateSettings').attr('account_BSB',parsed.data.companySetting.account_BSB) ;
          $('.updateSettings').attr('account_number',parsed.data.companySetting.account_number) ;
          $('.updateSettings').attr('payment_phone_number',parsed.data.companySetting.payment_phone_number) ;
          $('.updateSettings').attr('colour_consultant_item',parsed.data.companySetting.colour_consultant_item) ;
          $('.updateSettings').attr('price_increase',parsed.data.companySetting.price_increase) ;
          
          $('.empty').each(function(){
            if($(this).text()=='' || $(this).text()=="")
            {
              $(this).text('---')
            }
          })
          ///settings fill up ends here
          function isEmpty(obj) {
          for(var key in obj) {
             if(obj.hasOwnProperty(key))
              return false;
          }
          return true;
          }
   
          ///quote email setting fill up starts here
          $('.serverEmailAddress').text(parsed.data.quoteEmailSettingsDetail.server_email_address);
          $('.serverBccAddress').text(parsed.data.quoteEmailSettingsDetail.bcc_to);
          $('.quoteEmailSubject').text(parsed.data.quoteEmailSettingsDetail.email_subject);
          $('.editEmailSetting').attr('serverEmailAddress',parsed.data.quoteEmailSettingsDetail.server_email_address);
          $('.editEmailSetting').attr('serverBccAddress',parsed.data.quoteEmailSettingsDetail.bcc_to);
          $('.editEmailSetting').attr('quoteEmailSubject',parsed.data.quoteEmailSettingsDetail.email_subject);
          $('.editEmailSetting').attr('email_setting_id',parsed.data.quoteEmailSettingsDetail.email_setting_id); 
          //$('.editEmailSetting').attr('doc',parsed.data.quoteEmailSettingsDetail.email_subject);
          if(isEmpty(parsed.data.quoteEmailSettingsDetail))
          {
            
          }
          else
          {
            var doc_html = '';
            for(var i=0; i<parsed.data.quoteEmailSettingsDetail.doc.length; i++)
            {
              doc_html += '<tr><td><input type="text" class ="attachmentquote" readonly value="'+parsed.data.quoteEmailSettingsDetail.doc[i].file_name+'" data-id ="'+parsed.data.quoteEmailSettingsDetail.doc[i].file_id+'"></td></tr>';
            }
              $('.quoteIncludeDocuments').html(doc_html)
          }
          //$('.quoteIncludeDocuments').text(parsed.data.quoteEmailSettingsDetail.quoteIncludeDocuments);
          ///quote email setting fill up ends here
          ///quote email setting fill up starts here
          $('.serverEmailAddressjss').text(parsed.data.jssEmailSettingsDetail.server_email_address);
          $('.serverBccAddressjss').text(parsed.data.jssEmailSettingsDetail.bcc_to);
          $('.quoteEmailSubjectjss').text(parsed.data.jssEmailSettingsDetail.email_subject);
          $('.editEmailSettingJss').attr('serverEmailAddress',parsed.data.jssEmailSettingsDetail.server_email_address);
          $('.editEmailSettingJss').attr('serverBccAddress',parsed.data.jssEmailSettingsDetail.bcc_to);
          $('.editEmailSettingJss').attr('quoteEmailSubject',parsed.data.jssEmailSettingsDetail.email_subject);
          $('.editEmailSettingJss').attr('email_setting_id',parsed.data.jssEmailSettingsDetail.email_setting_id);
          if(isEmpty(parsed.data.jssEmailSettingsDetail))
          {
            // for(var i=0; i<parsed.data.jssEmailSettingsDetail.doc.length; i++)
            // {
            //   $('.quoteIncludeDocumentsjss').append('<tr><td></td><td><input type="text" readonly value="'+parsed.data.jssEmailSettingsDetail.doc[i].file_name+'"></td></tr>')
            // } 
          }
          else
          {
            for(var i=0; i<parsed.data.jssEmailSettingsDetail.doc.length; i++)
            {
              $('.quoteIncludeDocumentsjss').append('<tr><td></td><td><input type="text" class="attachmentjss" readonly data-id ="'+parsed.data.jssEmailSettingsDetail.doc[i].file_id+'" value="'+parsed.data.jssEmailSettingsDetail.doc[i].file_name+'"></td></tr>')
            }
          }
          //$('.quoteIncludeDocuments').text(parsed.data.quoteEmailSettingsDetail.quoteIncludeDocuments);
          ///quote email setting fill up ends here
   
          ///prep listing
           var prep = parsed.data.prepLevel;
           for(var i=0; i<prep.length; i++)
           {
              $('.prepBody').append('<tr class="prepEdit" name="'+prep[i].name+'" id="'+prep[i].id+'" deletable="'+prep[i].deletable+'" is_default="'+prep[i].is_default+'" uplift_cost="'+prep[i].uplift_cost+'" uplift_time="'+prep[i].uplift_time+'"><td>'+prep[i].name+'</td><td>'+prep[i].uplift_cost+'</td><td>'+prep[i].uplift_time+'</td></tr>')
           }
          ///prep listing ends here
   
           var room = parsed.data.roomTypes;
           for(var i=0; i<room.length; i++)
           {
              $('.RoomBody').append('<tr class="roomEdit" name="'+room[i].name+'" id="'+room[i].id+'" type_id="'+room[i].type_id+'" type="'+room[i].type+'" company_id="'+room[i].company_id+'" multiples="'+room[i].multiples+'" blue_image="'+room[i].blue_image+'" white_image="'+room[i].white_image+'" room_order_id="'+room[i].room_order_id+'"><td>'+room[i].name+'</td><td>'+room[i].type+'</td></tr>')
           }         
           // Room types list end here
   
           var allsotres = parsed.data.allStores;
           for(var i=0; i<allsotres.length; i++)
           {
              $('.PaintStoreBody').append('<tr class="storeEdit" id="'+allsotres[i].deliver_id+'" name="'+allsotres[i].name+'" deliver_id="'+allsotres[i].deliver_id+'" address="'+allsotres[i].address+'" company="'+allsotres[i].company+'" email="'+allsotres[i].email+'" mobile="'+allsotres[i].mobile+'" phone="'+allsotres[i].phone+'" postcode="'+allsotres[i].postcode+'"  subscriber_id="'+allsotres[i].subscriber_id+'" suburb="'+allsotres[i].suburb+'"><td>'+allsotres[i].name+'</td></tr>')
           }         
           // Paint Store list end here
   
           var customerSources = parsed.data.customerSources;
           for(var i=0; i<customerSources.length; i++)
           {
              $('.CustomerSourceBody').append('<tr class="customerSourceEdit" cs_source="'+customerSources[i].cs_source+'" subscriber_id="'+customerSources[i].subscriber_id+'" id="'+customerSources[i].cs_id+'" cs_id="'+customerSources[i].cs_id+'"><td>'+customerSources[i].cs_source+'</td></tr>')
           }         
           // Special Items list end here
   
           var specialItems = parsed.data.specialItems;
           for(var i=0; i<specialItems.length; i++)
           {
              $('.SpecialItemBody').append('<tr class="specialItemEdit" name="'+specialItems[i].name+'" price="'+specialItems[i].price+'" id="'+specialItems[i].id+'"><td>'+specialItems[i].name+'</td><td>$'+parseFloat(specialItems[i].price).toFixed(2)+'</td></tr>')
           }         
           // Customer Sources list end here
   
           var paymentNotes = parsed.data.paymentNotes;
           for(var i=0; i<paymentNotes.length; i++)
           {
              $('.PaymentNotesBody').append('<tr class="paymentnoteEdit" name="'+paymentNotes[i].name+'" id="'+paymentNotes[i].id+'"><td>'+paymentNotes[i].name+'</td></tr>')
           }         
           // Payment Notes list end here
   
           var paint_order_notes = parsed.data.paint_order_notes;
           for(var i=0; i<paint_order_notes.length; i++)
           {
              $('.PaintOrderNotesBody').append('<tr class="paintordernoteEdit" name="'+paint_order_notes[i].name+'" id="'+paint_order_notes[i].id+'"><td>'+paint_order_notes[i].name+'</td></tr>')
           }         
           // Payment Order Notes list end here
   
   
          $.ajax({
            url:  "//<?php echo $serverName;?>/paintpad/webservice/get-appointment-init?company_id="+company_id+"&subscriber_id="+subscriber_idd+"",
            type: "GET",
              success: function(res) {
                var pars = JSON.parse(res);
                // console.log(pars.data.types);
                var rowData = '';
                $.each(pars.data.types, function(i,v){
                  rowData += '<tr  id="'+v.id+'" data-orderid="'+(++i)+'" class="AppointmentEdit"><td>'+v.name+'</td></tr>';
                });
                $('.AppointmentBody').html(rowData);
              }
          });
   
          var doclist = parsed.data.docList;
          //console.log(doclist.length);
          for(var i=0; i<doclist.length; i++)
          {
   						var d = new Date(0); // The 0 there is the key, which sets the date to the epoch
   						d.setUTCSeconds(doclist[i].uploaded_date);
   						//alert(d);
                var file_name_main = doclist[i].file_name ;
                var file_desc_main = doclist[i].desc ;
                if(doclist[i].file_name.length>25)
                {
                  file_name_main = doclist[i].file_name.slice(0,25);
                }
                if(doclist[i].desc.length>25)
                {
                  file_desc_main = doclist[i].desc.slice(0,25);
                }
   						var da = moment(d).format("DD/MM/YYYY");
   						da = da.toUpperCase();
          	$('.documentListing').append("<tr id="+doclist[i].file_id+" class='docUpdateListing' name='"+doclist[i].file_name+"' desc='"+doclist[i].desc+"' actual_name='"+doclist[i].actual_name+"' always_link ='"+doclist[i].always_link+"' image_url ='"+doclist[i].url+"' uploaded_by ='"+doclist[i].uploaded_by+"'><td>"+doclist[i].file_id+"</td><td>"+file_name_main+"</td><td>"+file_desc_main+"</td><td>"+doclist[i].uploaded_by+"</td><td>"+da+"</td></tr>")
          }
          $('.bodyMain').show();  
    		  $('.showLoader').hide();
   
        }
   })

</script>
<script type="text/javascript">
   var old_pic = ''
   var typeImm = ''
   	$('.imageChange').unbind('click').on('click', function (e) {
       e.stopPropagation();
   		e.preventDefault();
         var type = 'upload'
         typeImm  = 'upload'
         new_croppie(type)
         old_pic = $('.subscriberLogo').find('img').attr('src')
         $($('.changeImage').get(0)).trigger('click');
         console.log( $('.changeImage').length)
         e.stopPropagation();
   	})
   
   $('.imageDismiss').click(function(){
     $('#insertimageModal').modal('hide')
     if (typeImm == 'upload') {
       $('.subscriberLogo').find('img').attr('src',old_pic)
        $('#insert_image_logo').val('')
     }
     else if (typeImm == 'imageChangeAccredate') {
       $('.logoAccredate').find('img').attr('src',old_pic)
       $('#insert_accredate_image').val('')
       
     }
     else if (typeImm == 'pdflogochange') {
       $('.logoPdf').find('img').attr('src',old_pic)
       $('#insert_image').val('')
       
     }
     
   
   })
</script>
<script type="text/javascript">
   $("#imageChangePdf").unbind( "click" ); 
   	$('.imageChangePdf').unbind('click').on('click', function (e) {
     // $(document).on('click', '.imageChangePdf', function (e) {
       e.preventDefault();
       e.stopPropagation();
       var type = 'pdflogochange'
       typeImm  = 'pdflogochange'
       old_pic = $('.logoPdf').find('img').attr('src')
       new_croppie(type)
       
       $($('.changePdfImage').get(0)).trigger('click');
       e.stopPropagation();
     })
</script>
<script type="text/javascript">
   $("#imageChangeAccredate").unbind( "click" ); 
   	$('.imageChangeAccredate').unbind('click').on('click', function (e) {
     // $(document).on('click', '.imageChangeAccredate', function (e) {
       e.preventDefault();
        e.stopPropagation();
       var type = 'imageChangeAccredate'
       typeImm  = 'imageChangeAccredate'
       old_pic = $('.logoAccredate').find('img').attr('src')
       new_croppie(type)
       $($('.changeAccredateImage').get(0)).trigger('click');
       e.stopPropagation();
     })
</script>
<script>
   function dataURLtoFile(dataurl, filename) {
   
          var arr = dataurl.split(','),
              mime = arr[0].match(/:(.*?);/)[1],
              bstr = atob(arr[1]), 
              n = bstr.length, 
              u8arr = new Uint8Array(n);
              
          while(n--){
              u8arr[n] = bstr.charCodeAt(n);
          }
          
          return new File([u8arr], filename, {type:mime});
      }
    var up = '0'
   $(document).on('click', '.upload', function (e) {
   	e.preventDefault();
      if (up == '1') {
        return false;
      }
      else{
        up = '1'
          var file_data = $('.changeImage').prop('files')[0];  
          var id_data = $('.idClicked').val(); 
          //   form_data.append('image_0', file_data);
          //   form_data.append('company_id', id_data);
          //   console.log(form_data);
   
            var form_data = new FormData();
            var myJSON = JSON.stringify({"company_id":id_data});
        //var data ={"json":'{"company_id":"<?php //echo $_GET['id'];?>"}'};
            var file_data = $('.changeImage').prop('files')[0];
            console.log(file_data.name)
            $image_crop.croppie('result', {
              type: 'canvas',
              size: 'original',
              quality: 1
            }).then(function(response){
              // console.log(dataURLtoFile(response,file_data.name))
              // return
              var ne_file = dataURLtoFile(response,file_data.name)
              form_data.append('json',myJSON);
              form_data.append('image_0',ne_file);
              $.ajax({
                  url: "//<?php echo $serverName;?>/paintpad/webservice/update-company-logo",
                  type: "POST",
                  data: form_data,
                //   async: false,
                  contentType: false,
                  cache: false,
                  processData:false,
                  success: function(data){
                    console.log(data);
                    alert("profile pic updated successfully");
                    $('#insertimageModal').modal('hide');
                    var parsed = JSON.parse(data);
                    console.log(parsed.data.logo);
                    $('#blah').attr('src',parsed.data.logo);
                    $('.changeImage').val('')
                    up = '0'
                  }
              });	
                e.stopImmediatePropagation();
                  return false;
        });
      } 
        
          
   
      //form_data.append('json', '{"file_name":'+file_name+',"file_doc_desc":'+file_doc_desc+',"doc_id":'+doc_id+',"always_link":'+always_link+',"company_id":'+company_id+',"subscriber_id":'+subscriber_id+'}');
      	
   	
   })
</script>
<script>
//  $('.editEmailSetting').click(function(){
//    $.ajax({
//          url: "//<?php //echo $serverName;?>/app/admin/getDocsajax.php",
//          type:'POST',
//          data:{"id":<?php //echo $_GET['id']?>},
//          success:function(data){
//             console.log(data)
//       }
//    })
// });
   
</script>
<script>
var up = '0'
   $(document).on('click','.pdflogochange',function(event){
       event.preventDefault();
       if (up == '1') {
       return false;
     }
     else{
       up = '1'
       var company_id = $('.company_id_create').val();
         
        $image_crop.croppie('result', {
          type: 'canvas',
          size: 'original',
          quality: 1
        }).then(function(response){
          
          $.ajax({
            url: "//<?php echo $serverName;?>/paintpad/webservice/update-pdf-logo",
            type:'POST',
            data:{"image_0":response,"json":'{"company_id":'+company_id+'}'},
            success:function(data){
              $('#insertimageModal').modal('hide');
              var parsed = JSON.parse(data);
              console.log(parsed.data.pdflogo);
              $('#pdfLogo').attr('src',parsed.data.pdflogo);
              up = '0'
      
            }
          })
          event.stopImmediatePropagation();
                    return false;
        });
      }
         
     
   });
   
</script>
<script>
var up = '0'
   $(document).on('click','.accredatelogochange',function(event){
       event.preventDefault();
        if (up == '1') {
       return false;
     }
     else{
       up = '1'
       var company_id = $('.company_id_create').val();
         
     $image_crop.croppie('result', {
       type: 'canvas',
       size: 'original',
       quality: 1
     }).then(function(response){
       
       $.ajax({
         url: "//<?php echo $serverName;?>/paintpad/webservicecopy/update-accredate-admin",
         type:'POST',
         data:{"image_0":response,"json":'{"company_id":'+company_id+'}'},
         success:function(data){
           $('#insertimageModal').modal('hide');
           var parsed = JSON.parse(data);
           console.log(parsed.data.accredate);
           $('#accredateLogo').attr('src',parsed.data.accredate);
           $('.changeAccredateImage').val('')
           up = '0'
         }
       })
       event.stopImmediatePropagation();
                 return false;
     });
     }
     
     
   });
   
</script>
<script>
   $(document).on('click', '.uploadPdfLogo', function (e) {
     e.preventDefault();
         var file_data = $('.changePdfImage').prop('files')[0];  
         var id_data = $('.idClicked').val(); 
         var form_data = new FormData();
         var file_data = $('.changePdfImage').prop('files')[0]; ;  
         var myJSON = JSON.stringify({"company_id":id_data});
         form_data.append('json',myJSON);
         form_data.append('image_0',file_data);
   
         $.ajax({
             url: "//<?php echo $serverName;?>/paintpad/webservice/update-pdf-logo",
             type: "POST",
             data: form_data,
             contentType: false,
             cache: false,
             processData:false,
             success: function(data){
                // consdfole.log(data);
                alert("PDF logo updated successfully");
                $('.changePdfImage').val('')
             }
         });   
     
   })
</script>
<script>
   $('.addDoc').click(function(){
   
   	$('#myModalDocAdd').modal('show')
     $('#myModalDocAdd').find("input[type=text], textarea").val("");
     $('#myModalDocAdd').find("input[type=file]").val("");
     $('.viewimageDoc').attr('src','<?php  echo Url::base(true); ?>/uploads/no_image.png');
   	if($('.modal-backdrop').length>1)
   	{
   		 $('.modal-backdrop:gt(0)').remove();
   		//$('.modal-backdrop').remove();
   	}
   
   })
</script>
<script>
   $(document).on('click','.paymentnoteEdit',function(e){
       // e.preventDefault();
       var name = $(this).attr('name');
       var id = $(this).attr('id');
       $('.deletePaymentnote').attr('id',id);
       $('.paymentnotes input[name="name"]').val(name);
       $('.paymentnotes input[name="note_id"]').val(id);
       $('.deletePaymentnotebtn').show();
       $('#createPaymentnotebtn').modal('show');
     });
   
   $(document).on('click','.paymentnotebutton',function(e){
     // $(this).prop('disabled', true);
       e.preventDefault();
        
       // e.stopPropagation();
       $('.deletePaymentnotebtn').hide();
       $('.deletePaymentnote').attr('id','0');
       $('.paymentnotes input[name="name"]').val('');
       $('.paymentnotes input[name="note_id"]').val('0');
       $('#createPaymentnotebtn').modal('show');
       //  $(this).prop('disabled', false);
     });
   var ccL = '1'
     $(document).on('click','.savePaymentnoteCreate',function(e){
       e.preventDefault();
       if($('.paymentnotes input[name="name"]').val() == ''){
         alert('Name is mandatory!');
         return;
       }
       // $(this).attr('disabled','disabled');
       if (ccL == '2') {
         return ;
       }
       else{
         ccL = '2'
         var name = $('.paymentnotes input[name="name"]').val();
         var note_id = $('.paymentnotes input[name="note_id"]').val();
         var company_id = "<?php echo $_GET['id'];?>";
         var form_data = new FormData();
         var myJSON = JSON.stringify({'name':name,'note_id':note_id,'company_id':company_id}); 
         form_data.append('json',myJSON);
         $.ajax({
             url: "//<?php echo $serverName;?>/paintpad/webservice/save-payment-notes",
             //url:'save-doc',
             type: "POST",
             data: form_data,
             contentType: false,
             //cache: false,
             processData:false,
             success: function(data){
               //  console.log(data);
               var parsed = JSON.parse(data);
               alert(parsed.message);
               if($('.PaymentNotesBody #'+parsed.data.id).length == 1){
                 $('.PaymentNotesBody #'+parsed.data.id).replaceWith('<tr class="paymentnoteEdit" name="'+name+'" id="'+parsed.data.id+'"><td>'+name+'</td></tr>')
               }else{
                 $('.PaymentNotesBody').append('<tr class="paymentnoteEdit" name="'+name+'" id="'+parsed.data.id+'"><td>'+name+'</td></tr>')
               }
               $('#createPaymentnotebtn').modal('hide');
               ccL = '1'
             } 
         })
         // $(this).removeAttr('disabled');
       }
       
   
     });
   var clicked = '1' 
     $(document).on('click','.deletePaymentnote',function(e){
       
       e.preventDefault();
       if (clicked == '2') {
         return ;
       }
       else{
         clicked = '2'
         var note_id = $('.deletePaymentnotebtn .deletePaymentnote').attr('id');
         var company_id = "<?php echo $_GET['id'];?>";
         var form_data = new FormData();
         var myJSON = JSON.stringify({'note_id':note_id,'company_id':company_id}); 
         form_data.append('json',myJSON);
         //  e.preventDefault();
         $.ajax({
             url: "//<?php echo $serverName;?>/paintpad/webservice/delete-payment-note",
             //url:'save-doc',
             type: "POST",
             data: form_data,
             contentType: false,
             //cache: false,
             processData:false,
             success: function(data){
                 // console.log(data);
               var parsed = JSON.parse(data);
               alert(parsed.message);
                 $('.PaymentNotesBody #'+note_id).remove();
               $('#createPaymentnotebtn').modal('hide');
               clicked = '1'
             } 
         })
         
       }
       
     })
</script>
<script>
   $(document).on('click','.paintordernotebutton',function(){
       $('.deletePaintordernotebtn').hide();
       $('.deletePaintordernote').attr('id','0');
       $('.paintordernotes input[name="name"]').val('');
       $('.paintordernotes input[name="note_id"]').val('0');
       $('#createPaintordernotebtn').modal('show');
     });
   
   $(document).on('click','.paintordernoteEdit',function(){
       var name = $(this).attr('name');
       var id = $(this).attr('id');
       $('.deletePaintordernote').attr('id',id);
       $('.paintordernotes input[name="name"]').val(name);
       $('.paintordernotes input[name="note_id"]').val(id);
       $('.deletePaintordernotebtn').show();
       $('#createPaintordernotebtn').modal('show');
     });
   
   $('.savePaintordernoteCreate').unbind('click').click(function(){
      if($('.paintordernotes input[name="name"]').val() == ''){
         alert('Name is mandatory!');
         return;
       }
       var name = $('.paintordernotes input[name="name"]').val();
       var note_id = $('.paintordernotes input[name="note_id"]').val();
       var company_id = "<?php echo $_GET['id'];?>";
       var form_data = new FormData();
       var myJSON = JSON.stringify({'name':name,'note_id':note_id,'company_id':company_id}); 
       form_data.append('json',myJSON);
       $.ajax({
           url: "//<?php echo $serverName;?>/paintpad/webservice/save-paint-order-notes",
           //url:'save-doc',
           type: "POST",
           data: form_data,
           contentType: false,
           //cache: false,
           processData:false,
           success: function(data){
               // console.log(data);
             var parsed = JSON.parse(data);
             alert(parsed.message);
             // alert('PaintOrderNotesBody #'+parsed.data.id);
             if($('.PaintOrderNotesBody #'+parsed.data.id).length == 1){
               $('.PaintOrderNotesBody #'+parsed.data.id).replaceWith('<tr class="paintordernoteEdit" name="'+name+'" id="'+parsed.data.id+'"><td>'+name+'</td></tr>')
             }else{
               $('.PaintOrderNotesBody').append('<tr class="paintordernoteEdit" name="'+name+'" id="'+parsed.data.id+'"><td>'+name+'</td></tr>')
             }
             $('#createPaintordernotebtn').modal('hide');
           } 
       })
   
     });
     
     $('.deletePaintordernotebtn').unbind('click').click(function(){
       var note_id = $('.deletePaintordernotebtn .deletePaintordernote').attr('id');
       var company_id = "<?php echo $_GET['id'];?>";
       var form_data = new FormData();
       var myJSON = JSON.stringify({'note_id':note_id,'company_id':company_id}); 
       form_data.append('json',myJSON);
       $.ajax({
           url: "//<?php echo $serverName;?>/paintpad/webservice/delete-paint-order-note",
           //url:'save-doc',
           type: "POST",
           data: form_data,
           contentType: false,
           //cache: false,
           processData:false,
           success: function(data){
               // console.log(data);
             var parsed = JSON.parse(data);
             alert(parsed.message);
               $('.PaintOrderNotesBody #'+note_id).remove();
             $('#createPaintordernotebtn').modal('hide');
           } 
       })
   
     })
</script>
<script>
   $(document).on('click','.specialItemsbutton',function(){
       $('.deleteSpecialItemsbtn').hide();
       $('.deleteSpecialItems').attr('id','0');
       $('.SpecialItems input[name="name"]').val('');
       $('.SpecialItems input[name="price"]').val('');
       $('.SpecialItems input[name="note_id"]').val('0');
       $('#createSpecialItemsbtn').modal('show');
     });
   
   $(document).on('click','.specialItemEdit',function(){
       var name = $(this).attr('name');
       var price = $(this).attr('price');
       var id = $(this).attr('id');
   
       $('.deleteSpecialItems').attr('id',id);
       $('.SpecialItems input[name="name"]').val(name);
       $('.SpecialItems input[name="price"]').val(price);
       $('.SpecialItems input[name="id"]').val(id);
       $('.deleteSpecialItemsbtn').show();
       $('#createSpecialItemsbtn').modal('show');
     });
   
   $('.saveSpecialItemsCreate').unbind('click').click(function(){
       var name = $('.SpecialItems input[name="name"]').val();
       var price  = $('.SpecialItems input[name="price"]').val();
       var id = $('.SpecialItems input[name="id"]').val();
       var company_id = "<?php echo $_GET['id'];?>";
       var form_data = new FormData();
   
       var myJSON = JSON.stringify({"company_id" : company_id,"name" : name,"price" : price,"special_item_id" : id}); 
       form_data.append('json',myJSON);
       $.ajax({
           url: "//<?php echo $serverName;?>/paintpad/webservice/save-special-items",
           //url:'save-doc',
           type: "POST",
           data: form_data,
           contentType: false,
           //cache: false,
           processData:false,
           success: function(data){
               // console.log(data);
             var parsed = JSON.parse(data);
             alert(parsed.message);
             // alert('PaintOrderNotesBody #'+parsed.data.id);
             if (parsed.success == 0 ) {
                 return false;
             }
             else{
                 if($('.SpecialItemBody #'+parsed.data.special_item_id).length == 1){
                   $('.SpecialItemBody #'+parsed.data.special_item_id).replaceWith('<tr class="specialItemEdit" name="'+name+'" price="'+price+'" id="'+parsed.data.special_item_id+'"><td>'+name+'</td><td>$'+parseFloat(price).toFixed(2)+'</td></tr>')
               }else{
                   $('.SpecialItemBody').append('<tr class="specialItemEdit" price="'+price+'" name="'+name+'" id="'+parsed.data.special_item_id+'"><td>'+name+'</td><td>$'+parseFloat(price).toFixed(2)+'</td></tr>')
               }
               $('#createSpecialItemsbtn').modal('hide');
             }
             
             
           } 
       })
   
     });
     
     $('.deleteSpecialItemsbtn').unbind('click').click(function(){
       var special_item_id = $('.deleteSpecialItemsbtn .deleteSpecialItems').attr('id');
       var company_id = "<?php echo $_GET['id'];?>";
       var form_data = new FormData();
       var myJSON = JSON.stringify({'special_item_id':special_item_id,'company_id':company_id}); 
       form_data.append('json',myJSON);
       $.ajax({
           url: "//<?php echo $serverName;?>/paintpad/webservice/delete-special-items",
           //url:'save-doc',
           type: "POST",
           data: form_data,
           contentType: false,
           //cache: false,
           processData:false,
           success: function(data){
               // console.log(data);
             var parsed = JSON.parse(data);
             alert(parsed.message);
               $('.SpecialItemBody #'+special_item_id).remove();
             $('#createSpecialItemsbtn').modal('hide');
           } 
       })
   
     })
</script>
<script>
   $(document).on('click','.paintStoreButton',function(){
       $('.deletepaintstorebtn').hide();
       $('.deletepaintstore').attr('id','0');
       $('.paintstore input[name="name"]').val('');
       $('.paintstore input[name="address"]').val('');
       $('.paintstore input[name="post_code"]').val('');
       $('.paintstore input[name="suburb"]').val('');
       $('.paintstore input[name="mobile"]').val('');
       $('.paintstore input[name="phone"]').val('');
       $('.paintstore input[name="email"]').val('');
       $('.paintstore input[name="subscriber_id"]').val('0');
       $('.paintstore input[name="store_id"]').val('0');
       $('#createpaintstorebtn').modal('show');
     });
   
   $(document).on('click','.storeEdit',function(){
       var name = $(this).attr('name');
       var address = $(this).attr('address');
       var suburb = $(this).attr('suburb');
       var post_code = $(this).attr('postcode');
       var phone = $(this).attr('phone');
       var mobile = $(this).attr('mobile');
       var store_id = $(this).attr('deliver_id');
       var subscriber_id = $(this).attr('subscriber_id');
       var email = $(this).attr('email');
   
       $('.deletepaintstore').attr('id',store_id);
       $('.paintstore input[name="name"]').val(name);
       $('.paintstore input[name="address"]').val(address);
       $('.paintstore input[name="post_code"]').val(post_code);
       $('.paintstore input[name="suburb"]').val(suburb);
       $('.paintstore input[name="mobile"]').val(mobile);
       $('.paintstore input[name="phone"]').val(phone);
       $('.paintstore input[name="email"]').val(email);
       $('.paintstore input[name="subscriber_id"]').val(subscriber_id);
       $('.paintstore input[name="store_id"]').val(store_id);
       $('.deletepaintstorebtn').show();
       $('#createpaintstorebtn').modal('show');
     });
   
   $('.savepaintstoreCreate').unbind('click').click(function(){
       var mobile = $('.paintstore input[name="mobile"]').val();
       var address = $('.paintstore input[name="address"]').val();
       var suburb = $('.paintstore input[name="suburb"]').val();
       var contact = $('.paintstore input[name="phone"]').val();
       var postcode = $('.paintstore input[name="post_code"]').val();
       var subscriber_id = $('.paintstore input[name="subscriber_id"]').val();
       var name = $('.paintstore input[name="name"]').val();
       var email  = $('.paintstore input[name="email"]').val();
       var store_id = $('.paintstore input[name="store_id"]').val();
       var company_id = "<?php echo $_GET['id'];?>";
       
       if(address == '' || suburb == '' || postcode == '' || name == '' || email == ''){
         alert('Fields are mandatory');exit();
       }
   
       var form_data = new FormData();
       
       var myJSON = JSON.stringify({"mobile" : mobile,"address" : address,"suburb" : suburb,"company_id" : company_id,"contact" : contact,"postcode" : postcode,"subscriber_id" : subscriber_id,"email" : email,"store_id" : store_id,"name" : name,'store_id':store_id}); 
       // console.log(myJSON);exit();
       form_data.append('json',myJSON);
       $.ajax({
           url: "//<?php echo $serverName;?>/paintpad/webservice/add-paint-store",
           //url:'save-doc',
           type: "POST",
           data: form_data,
           contentType: false,
           //cache: false,
           processData:false,
           success: function(data){
             var parsed = JSON.parse(data);
             console.log(parsed);
             alert(parsed.message);
             if($('.PaintStoreBody #'+parsed.data.store_id).length == 1){
               $('.PaintStoreBody #'+parsed.data.store_id).replaceWith('<tr class="storeEdit" id="'+parsed.data.store_id+'" name="'+parsed.data.name+'" deliver_id="'+parsed.data.store_id+'" address="'+parsed.data.address+'" company="'+parsed.data.company_id+'"  email="'+parsed.data.email+'" mobile="'+parsed.data.mobile+'" phone="'+parsed.data.contact+'" postcode="'+parsed.data.postcode+'"  subscriber_id="'+parsed.data.subscriber_id+'" suburb="'+parsed.data.suburb+'"><td>'+parsed.data.name+'</td></tr>');
             }else{
               $('.PaintStoreBody').append('<tr class="storeEdit" id="'+parsed.data.store_id+'" name="'+parsed.data.name+'" deliver_id="'+parsed.data.store_id+'" address="'+parsed.data.address+'" company="'+parsed.data.company_id+'"  email="'+parsed.data.email+'" mobile="'+parsed.data.mobile+'" phone="'+parsed.data.contact+'" postcode="'+parsed.data.postcode+'"   subscriber_id="'+parsed.data.subscriber_id+'" suburb="'+parsed.data.suburb+'"><td>'+parsed.data.name+'</td></tr>');
             }
             $('#createpaintstorebtn').modal('hide');
   
           
           } 
       })
   
     });
     
     $('.deletepaintstorebtn').unbind('click').click(function(){
       var store_id = $('.deletepaintstorebtn .deletepaintstore').attr('id');
       var company_id = "<?php echo $_GET['id'];?>";
       var form_data = new FormData();
       var myJSON = JSON.stringify({'store_id':store_id,'company_id':company_id}); 
       form_data.append('json',myJSON);
       $.ajax({
           url: "//<?php echo $serverName;?>/paintpad/webservice/delete-paint-store",
           //url:'save-doc',
           type: "POST",
           data: form_data,
           contentType: false,
           //cache: false,
           processData:false,
           success: function(data){
               // console.log(data);
             var parsed = JSON.parse(data);
             alert(parsed.message);
               $('.PaintStoreBody #'+store_id).remove();
             $('#createpaintstorebtn').modal('hide');
           } 
       })
   
     })
</script>
<script>
   $(document).on('click','.paintorderEdit',function(){
       var pdf_url = $(this).attr('pdf_url');
       var Pdf = '<iframe id="frame" style="width:1100px;height:500px;" src="'+pdf_url+'" type="application/pdf"></iframe>';
       $('.modal-body #pdffile_load').html(Pdf);
       $('#mydocModal').modal('show');
       if($('.modal-backdrop').length>1)
       {
         $('.modal-backdrop:gt(0)').remove();
       }
    })
</script>
<script>
   $(document).on('click','.customersourcebutton',function(){
     $('.deletecustomersourcebtn').hide();
     $('.deletecustomersource').attr('id','0');
     $('.customersource input[name="cs_source"]').val('');
     $('.customersource input[name="suburb"]').val('');
     $('.customersource input[name="cs_id"]').val('');
     $('.customersource input[name="subscriber_id"]').val('0');
     $('#createcustomersourcebtn').modal('show');
   });
   
   $(document).on('click','.customerSourceEdit',function(){
     var cs_source = $(this).attr('cs_source');
     var id = $(this).attr('cs_id');
     var subscriber_id = $(this).attr('subscriber_id');
   
     $('.deletecustomersource').attr('id',id);
     $('.customersource input[name="cs_source"]').val(cs_source);
     $('.customersource input[name="cs_id"]').val(id);
     $('.customersource input[name="subscriber_id"]').val(subscriber_id);
     $('.deletecustomersourcebtn').show();
     $('#createcustomersourcebtn').modal('show');
   });
   
   $('.savecustomersourceCreate').unbind('click').click(function(){
     var form_data = new FormData();
     var cs_source = $('.customersource input[name="cs_source"]').val();
     var cs_id = $('.customersource input[name="cs_id"]').val();
     var subscriber_id = $('.customersource input[name="subscriber_id"]').val();
     var company_id = "<?php echo $_GET['id'];?>";
     if(cs_id == 0 || cs_id ==''){
       var url = "//<?php echo $serverName;?>/paintpad/webservice/add-customer-source";
       var myJSON = JSON.stringify({"company_id" : company_id,"cs_source" : cs_source,"subscriber_id" : 0});
     }else{
       var url = "//<?php echo $serverName;?>/paintpad/webservice/update-customer-source";
       var myJSON = JSON.stringify({"company_id" : company_id,"cs_source" : cs_source,"cs_id" : cs_id });
     }
      
     form_data.append('json',myJSON);
     // console.log(myJSON);
     $.ajax({
         url: url,
         //url:'save-doc',
         type: "POST",
         data: form_data,
         contentType: false,
         //cache: false,
         processData:false,
         success: function(data){
           var parsed = JSON.parse(data);
           alert(parsed.message);
           // console.log(parsed);
           // alert('.CustomerSourceBody #'+parsed.data.cs_id);
           // if(parsed.success == 1){
           if(cs_id != 0 || cs_id != ''){
             $('.CustomerSourceBody #'+cs_id).replaceWith('<tr class="customerSourceEdit" cs_source="'+cs_source+'" subscriber_id="'+subscriber_id+'" id="'+cs_id+'" cs_id="'+cs_id+'"><td>'+cs_source+'</td></tr>')
           }else{
             $('.CustomerSourceBody').append('<tr class="customerSourceEdit" cs_source="'+parsed.data.cs_source+'" subscriber_id="'+parsed.data.subscriber_id+'" id="'+parsed.data.cs_id+'" cs_id="'+parsed.data.cs_id+'"><td>'+parsed.data.cs_source+'</td></tr>')
           }
           
           $('#createcustomersourcebtn').modal('hide');
         } 
     })
   
   });
   
   $('.deletecustomersourcebtn').unbind('click').click(function(){
     var cs_id = $('.deletecustomersourcebtn .deletecustomersource').attr('id');
     var company_id = "<?php echo $_GET['id'];?>";
     var form_data = new FormData();
     var myJSON = JSON.stringify({'cs_id':cs_id,'company_id':company_id}); 
     form_data.append('json',myJSON);
     $.ajax({
         url: "//<?php echo $serverName;?>/paintpad/webservice/delete-customer-source",
         //url:'save-doc',
         type: "POST",
         data: form_data,
         contentType: false,
         //cache: false,
         processData:false,
         success: function(data){
             // console.log(data);
           var parsed = JSON.parse(data);
           alert(parsed.message);
             $('.CustomerSourceBody #'+cs_id).remove();
           $('#createcustomersourcebtn').modal('hide');
         } 
     })
   
   })
</script>
<script>
   $('.closeModalDocAdd').click(function(){
   	$('#myModalDocAdd').modal('hide');
   	$('#myModalDocUpdate').modal('hide');
   })
</script>
<script>
   $('.viewimageDoc').click(function(){
   	$('.imageDoc').click();
      $('.imageDoc_create').click();
   })
   $('.viewimageDocupdate').click(function(){
   	$('.imageDocupdate').click();
   })
</script>
<script>
   $(document).on('change','.imageDoc_create',function(){
   
     const size =  (this.files[0].size / 1024 / 1024).toFixed(2); 
     var sFileName = this.files[0].name;
     var sFileExtension = sFileName.split('.')[sFileName.split('.').length - 1].toLowerCase();
   
     if (!(sFileExtension === "pdf" || sFileExtension === "doc" || sFileExtension === "docx" || sFileExtension === "xls" || sFileExtension === "xlsx" || sFileExtension === "jpeg" || sFileExtension === "gif" || sFileExtension === "tif" || sFileExtension === "png") || size > 25){ 
         alert("Please make sure file format is correct (pdf,doc,docx,xls,xlsx,jpeg,gif,png) and size less than 25 MB."); 
         $('.imageDoc_create').val('');
     }else{ 
       document.getElementById('viewimageDoc').src = window.URL.createObjectURL(this.files[0]);
     } 
   
   });
</script>
<script type="text/javascript">
   $(document).on('click','.file_doc_alwayslinkon',function(){
       $('.file_doc_alwayslinkoff').prop("checked", false);
       $('.file_doc_alwayslinkon').prop("checked", true);
   })
   $(document).on('click','.file_doc_alwayslinkoff',function(){
       $('.file_doc_alwayslinkoff').prop("checked", true);
       $('.file_doc_alwayslinkon').prop("checked", false);
   })
      $('.imageDocupdate').on('change',function(){
           const size =  (this.files[0].size / 1024 / 1024).toFixed(2); 
           var sFileName = this.files[0].name;
           var sFileExtension = sFileName.split('.')[sFileName.split('.').length - 1].toLowerCase();
            if (!(sFileExtension === "pdf" || sFileExtension === "doc" || sFileExtension === "docx" || sFileExtension === "xls" || sFileExtension === "xlsx" || sFileExtension === "jpeg" || sFileExtension === "gif" || sFileExtension === "tif" || sFileExtension === "png") || size > 25){ 
               alert("Please make sure file format is correct (pdf,doc,docx,xls,xlsx,jpeg,gif,png) and size less than 25 MB."); 
               $('.imageDocupdate').val('');
          }

      })
   	$(document).on('click','.docUpdateListing',function(){
   		var id = $(this).attr('id');
   		var name = $(this).attr('name');
   		var desc = $(this).attr('desc');
   		var actual_name = $(this).attr('actual_name');
   		var always_link = $(this).attr('always_link');
       $('.imageDocupdate').val('')
           console.log(always_link)
   		var image_url = $(this).attr('image_url');
           if (actual_name != '') {
               $('.imageName').text(actual_name);
           }
       	else{
               $('.imageName').text('no image');
           }
       	$('.previewImage').attr('image',image_url);
       	$('.uploaded_byUpdate').val($(this).attr('uploaded_by'))
   		//console.log("name--->"+name+"------desc---->"+desc+"-----actual_name----->"+actual_name+"-----always_link---->"+always_link+"---image_url---->"+image_url);
   
   		$('.file_doc_name').val(name);
   		$('.file_doc_desc').text(desc);
   		$('.doc_id').val(id);
   		$('.imageDocupdate').attr('id','imageDocupdate'+id);
   		if(always_link==1)
   		{
   			$('.file_doc_alwayslinkon').prop("checked", true);
   		}
   		else
   		{
   			$('.file_doc_alwayslinkoff').prop("checked", true);
   		}
   		$('.imagedoc_list').attr('src','//'+image_url);
   		$('#myModalDocUpdate').modal('show');
   		if($('.modal-backdrop').length>1)
   		{
   		 $('.modal-backdrop:gt(0)').remove();
   		}
   	})
</script>
<script>
   $('#previewImage').click(function(){
      var image = $(this).attr('image');
      console.log(image)
      var isimg = checlImg(image)
      if (image != '') {
        if (isimg != true) {
          var Pdf = '<iframe id="frame" style="width:1100px;height:500px;" src="'+image+'" type="application/pdf"></iframe>';
        }
         else{
           var Pdf = '<img id="frameimg"  src="'+image+'" >';
         }
        //  
          $('.modal-body #pdffile_load').html(Pdf);
          $('#mydocModal').modal('show'); 
      }
      else(
          alert('No Image')
      )
      
      if($('.modal-backdrop').length>1)
      {
        $('.modal-backdrop:gt(0)').remove();
      }
   })
    function checlImg(uri) {
      //make sure we remove any nasty GET params 
      uri = uri.split('?')[0];
      //moving on, split the uri into parts that had dots before them
      var parts = uri.split('.');
      //get the last part ( should be the extension )
      var extension = parts[parts.length-1];
      //define some image types to test against
      var imageTypes = ['jpg','jpeg','tiff','png','gif','bmp','webp'];
      //check if the extension matches anything in the list.
      if(imageTypes.indexOf(extension) !== -1) {
          return true;   
      }
   }
</script>
<script>
   $('.SaveDocUpdate').click(function(e){
   	var file_name = $('.file_doc_name').val();
   	var file_doc_desc = $('.file_doc_desc').val();
   	var doc_id = $('.doc_id').val();
   	var always_link = $("input[name=always_link]:checked").val()
          if ($('.file_doc_alwayslinkon').prop("checked") == true) {
              always_link = 1
          }
          else{
              always_link = 0
          }
   	var company_id = $('.company_id').val();
   	var subscriber_id = $('.subscriber_id').val();
      	var uploaded_by = $('.uploaded_byUpdate').val();
   	var form_data = new FormData();
   	//var data ={"json":'{"company_id":"<?php //echo $_GET['id'];?>"}'};
   	var file_data = $('#imageDocupdate'+doc_id).prop('files')[0];  
   	var myJSON = JSON.stringify({"file_name":file_name,"desc":file_doc_desc,"doc_id":doc_id,"always_link":always_link,"company_id":company_id,"subscriber_id":subscriber_id});
   
   	//form_data.append('json', '{"file_name":'+file_name+',"file_doc_desc":'+file_doc_desc+',"doc_id":'+doc_id+',"always_link":'+always_link+',"company_id":'+company_id+',"subscriber_id":'+subscriber_id+'}');
   	form_data.append('json',myJSON);
   	form_data.append('image_0',file_data);
       $.ajax({
              url: "//<?php echo $serverName;?>/paintpad/webservice/save-doc",
              //url:'save-doc',
              type: "POST",
              data: form_data,
              contentType: false,
              //cache: false,
              processData:false,
              success: function(data){
                   var parsed = JSON.parse(data);
                  console.log(parsed);
                   if(parsed.success == 1)
                   {
                    alert(parsed.message);
                    // console.log(parsed.data.docList[0].file_name);
                    // $('#myModalDocAdd').modal('hide');
                      var d = new Date(0); // The 0 there is the key, which sets the date to the epoch
                      d.setUTCSeconds(parsed.data.docList[0].uploaded_date);
                      //alert(d);
                      var da = moment(d).format("DD/MM/YYYY");
                      da = da.toUpperCase();
                     $('#'+doc_id).attr('name',parsed.data.docList[0].file_name);
                     $('#'+doc_id).attr('desc',parsed.data.docList[0].desc);
                     $('#'+doc_id).attr('always_link',parsed.data.docList[0].always_link);
                     $('#'+doc_id).attr('image_url',parsed.data.docList[0].url);
                     if(parsed.data.docList[0].file_name.length>25)
                     {
                        parsed.data.docList[0].file_name = parsed.data.docList[0].file_name.slice(0,25)
                     }
                     if(parsed.data.docList[0].desc.length>25)
                     {
                        parsed.data.docList[0].desc = parsed.data.docList[0].desc.slice(0,25)
                     }
                     $('#'+doc_id).html("<td>"+parsed.data.docList[0].file_id+"</td><td>"+parsed.data.docList[0].file_name+"</td><td>"+parsed.data.docList[0].desc+"</td><td>"+parsed.data.docList[0].uploaded_by+"</td><td>"+da+"</td>");
                      $('#myModalDocUpdate').modal('hide');
   
   
                   }
              }
          });	
       e.preventDefault();
   })
</script>	
<script>
   $('.SaveDocAdd').click(function(e){
      var file_name = $('.file_name_doc_create').val();
   
      if(file_name=="")
      {
          alert("File Name Cannnot Be Blank");
          return false;
      }
      var file_name = $('.file_name_doc_create').val();
      var file_doc_desc = $('.file_desc_doc_create').val();
      var doc_id = $('.doc_id_create').val();
      var always_link = $("input[name=always_link]:checked").val()
      var company_id = $('.company_id_create').val();
      var subscriber_id = $('.subscriber_id_create').val();
      var form_data = new FormData();
      var file_data = $('.imageDoc_create').prop('files')[0];  
     /// var file_data = $('#imageDocupdate'+doc_id).prop('files')[0];  
      var myJSON = JSON.stringify({"file_name":file_name,"desc":file_doc_desc,"doc_id":doc_id,"always_link":always_link,"company_id":company_id,"subscriber_id":subscriber_id}); 
      form_data.append('json',myJSON);
      form_data.append('image_0',file_data);       
       $.ajax({
              url: "//<?php echo $serverName;?>/paintpad/webservice/save-doc",
              //url:'save-doc',
              type: "POST",
              data: form_data,
              contentType: false,
              //cache: false,
              processData:false,
              success: function(data){
                  var parsed = JSON.parse(data);
                 // alert(parsed.success)
              if(parsed.success == 1)
              {
                alert(parsed.message);
                var d = new Date(0); // The 0 there is the key, which sets the date to the epoch
                d.setUTCSeconds(parsed.data.docList[0].uploaded_date);
                //alert(d);
                var da = moment(d).format("DD/MM/YYYY");
                da = da.toUpperCase();
                $('.documentListing').append("<tr id="+parsed.data.docList[0].file_id+" class='docUpdateListing' name='"+parsed.data.docList[0].file_name+"' desc='"+parsed.data.docList[0].desc+"' actual_name='"+parsed.data.docList[0].actual_name+"' always_link ='"+parsed.data.docList[0].always_link+"' image_url ='"+parsed.data.docList[0].url+"' uploaded_by ='"+parsed.data.docList[0].uploaded_by+"'><td>"+parsed.data.docList[0].file_id+"</td><td>"+parsed.data.docList[0].file_name+"</td><td>"+parsed.data.docList[0].desc+"</td><td>"+parsed.data.docList[0].uploaded_by+"</td><td>"+da+"</td></tr>");
                $('#myModalDocAdd').modal('hide');
                
                var n = '<option value='+parsed.data.docList[0].file_id+'>'+parsed.data.docList[0].file_name+'</option>'
                $('.emailQuoteDocQuote').append(n)
                $('.emailQuoteDocJss').append(n)

              }
   
              }
          });	
       e.preventDefault();
   })
</script>	
<script>
   $('.updateSettings').click(function(){
     var account_name =   $(this).attr('account_name');
     var application_cost =   $(this).attr('application_cost');
     var profit_markup =   $(this).attr('profit_markup') ;
     var gross_margin_percent =   $(this).attr('gross_margin_percent');
     var default_deposit_percent =   $(this).attr('default_deposit_percent');
     var terms_condition =   $(this).attr('terms_condition');
     var xero_consumer_key =   $(this).attr('xero_consumer_key');
     var xero_share_secret =   $(this).attr('xero_share_secret');
     var xero_invoice_line_account_code =   $(this).attr('xero_invoice_line_account_code');
     var account_BSB =   $(this).attr('account_BSB');
     var account_number =   $(this).attr('account_number');
     var payment_phone_number =   $(this).attr('payment_phone_number');
     var colour_consultant_item =   $(this).attr('colour_consultant_item') ;
     var price_increase =   $(this).attr('price_increase') ;
     //alert("rk");
   })
</script>
<script>
   $(document).on('click','.addsubscriberPeople',function(){
     //alert("rk");
     $('.empty').each(function(){
     	$(this).val("");
     })
     $('#myModalPeopleAdd').modal('show');
     //alert($('.modal-backdrop').length)
    if($('.modal-backdrop').length > 1)
     {
      $('.modal-backdrop:gt(0)').remove();
     }
     $('#myModalPeopleAdd').modal('show');
   })
</script>
<script>
   // $(document).unbind().on('click','.SavePeopleSubscriber',function(e){
   
   $('.SavePeopleSubscriber').unbind('click').click(function(e){
   	//e.preventDefault();
   	$('.SavePeopleSubscriber').prop("disabled", true);
   	var first_name = $('.first_name').val();
   	var last_name =  $('.last_name').val();
   	var user_name =  $('.user_name').val();
   	var email =  $('.email').val();
   	var phone =  $('.phone').val();
   	var mobile =  $('.mobile').val();
   	var user_role =  $('#myModalPeopleAdd .user_role').val();
     var user_status =  $('#myModalPeopleAdd input[name="user_status"]:checked').val();
   	var password = $('.newPassword').val();
   	var confirm_password = $('.confirm_password').val()
   	// var company_id = "<?php echo $_GET['id'];?>";
     var company_id = $('.company_id_create').val();
   
   
   	error = 0;
   	if(user_name == ""){
   		error = 1;
   		alert('User Name is mandatory');
   		$('.SavePeopleSubscriber').prop("disabled", false);
   		return
   	}
      if(first_name == ""){
         alert('First Name is mandatory');
         $('.SavePeopleSubscriber').prop("disabled", false);
         return
      }
      if(last_name == ""){
         alert('Last Name is mandatory');
         $('.SavePeopleSubscriber').prop("disabled", false);
         return
      }
   	if(email == ""){
   		error = 1;
   		alert('Email is mandatory');
   		$('.SavePeopleSubscriber').prop("disabled", false);
   		return
   	}
      if(phone == ""){
         error = 1;
         alert('Phone is mandatory');
         $('.SavePeopleSubscriber').prop("disabled", false);
         return
      }
   	if(user_role == ""){
   		error = 1;
   		alert('User Role is mandatory');
   		$('.SavePeopleSubscriber').prop("disabled", false);
   		return
   	}
   	if(password == ""){
   		error = 1;
   		alert('Password is mandatory');
   		$('.SavePeopleSubscriber').prop("disabled", false);
   		return
   	}
   	if(confirm_password == ""){
   		error = 1;
   		alert('Confirm Password is mandatory');
   		$('.SavePeopleSubscriber').prop("disabled", false);
   		return
   	}
   
   	if(error == 1){
   		$('.SavePeopleSubscriber').prop("disabled", false);
   		return
   	}
   
   
   // {
   // "company_id":"3",
   // "first_name":"test2",
   // "last_name":"name",
   // "username":"testing",
   // "email":"em@email.com",
   // "user_role":"10",
   // "password":"Lbim2201",
   // "phone":"1234567890"
   // }
   	if($('.newPassword').val() == $('.confirm_password').val())
   	{
   		var form_data = new FormData();
   		var myJSON = JSON.stringify({"first_name":first_name,"last_name":last_name,"username":user_name,"email":email,"company_id":company_id,"phone":phone,"mobile":mobile,"user_role":user_role,'status':user_status,"password":password}); 
   		form_data.append('json',myJSON);
   		$.ajax({
   			url: "save-subscriber",
   			//url:'save-doc',
   			type: "POST",
   			data: form_data,
   			contentType: false,
   			//cache: false,
   			processData:false,
   			dataType : 'JSON',
   			success: function(parsed){
   				//console.log(parsed);
   				if(parsed.success == 1){
             getCompanyProple(company_id,'');
   					$('.SavePeopleSubscriber').prop("disabled", false);
   			// 		// alert('done');
   			// //		console.log(parsed);
   			// 		//var parsed = JSON.parse(data);
   			// 	//	console.log(parsed);
   			// 		//console.log(parsed.userdata);  
   			// 		// console.log(parsed.data.details[i])
   			// 		//   var d = new Date(0);  
   			// 		//   d.setUTCSeconds(parsed.userdata.logon);
   			// 		//   var da = moment(d).format("DD/MM/YYYY HH:mm A");
   			// 		//  da = da.toUpperCase();
   			// 		var da = "15/03/2019 18:11 PM";
   			// 		var la ="15/03/2019 18:11 PM";
   			// 		//  var da1 = new Date(0);
   			// 		// da1.setUTCSeconds(parsed.userdata.last_activity);
   			// 		//   var la = moment(da1).format("DD/MM/YYYY HH:mm A");
   			// 		//  la = la.toUpperCase();
   			// 		alert(parsed.message);
   			// 		$('.subscriberPeople').append('<tr fname="'+first_name+'" id="'+parsed.data.userdata.user_id+'" lname="'+last_name+'" email="'+email+'" username="'+user_name+'" phone="'+phone+'" mobile="'+mobile+'"  class="SubscriberPeopleUpdate" user_id="'+parsed.data.userdata.user_id+'" id="id'+parsed.data.userdata.user_id+'"><td>'+first_name+" "+last_name+'</td><td>#</td><td class="loggedON'+da+'" data="'+da+'">'+da+'</td><td class="lastActivity'+la+'" data="'+la+'">'+la+'</td><tr>');
   					$('#myModalPeopleAdd').modal('hide');
   				}
   				else{
   					alert(parsed.message);
   					$('.SavePeopleSubscriber').prop("disabled", false);
   				}
   			},
   			error: function(err){
   			//	console.log(err)
   			}
   		})
   	}
   	else
   	{
   		alert('password does not matched');
   		$('.SavePeopleSubscriber').prop("disabled", false);
   	}
       e.stopImmediatePropagation();
       e.preventDefault();
   })
</script>
<script>
   // $(document).on('change','#sub_filter',function(){
   $('#sub_filter').unbind('change').change(function(){
     var filter = $(this).val();
     getCompanyProple("<?php echo $_GET['id'];?>",filter);
   });
   
   $(document).ready(function(){
     getCompanyProple("<?php echo $_GET['id'];?>",'');
     getPaintOrdersAl();
     getPaintBrandsAll();
   })
   var tps= 1;
   function getCompanyProple(company_id, filter){
      var data ={"json":'{"company_id":"'+company_id+'","filter":"'+filter+'"}'};
      if (tps == 1) {
         $.ajax({
            url:  "//<?php echo $serverName;?>/app/admin/subscriber/quotes",
            type: "POST",
            data: data, // Send the object.
              success: function(response) {
               var parsed = JSON.parse(response);
                  // console.log(parsed);
                  var data_html = '';
                 for(var i=0; i<parsed.data.length; i++)
                 {
                   if(parsed.data[i].status == 1){
                       $statusStr = "Pending";
                       }
                   if(parsed.data[i].status == 2){
                       $statusStr = "In-progress";
                       }
                   if(parsed.data[i].status == 3){
                       $statusStr = "Completed";
                       }
                   if(parsed.data[i].status == 4){
                       $statusStr = "Accepted";
                       }
                   if(parsed.data[i].status == 5){
                       $statusStr = "Declined";
                       }
                   if(parsed.data[i].status == 6){
                       $statusStr = "New";
                       }
                   if(parsed.data[i].status == 7){
                       $statusStr = "Offered";
                       }
                   if(parsed.data[i].status == 8){
                       $statusStr = "Open";
                       }
               
                     data_html += '<tr role="row"><td>'+parsed.data[i].id +'</td><td>'+parsed.data[i].contact_id+'</td><td>'+parsed.data[i].type+'</td><td>'+parsed.data[i].client_name+'</td><td>'+parsed.data[i].contact_email+'</td><td>'+parsed.data[i].contact_name+'</td><td>'+parsed.data[i].contact_number+'</td><td>'+parsed.data[i].contact_mobile+'</td><td>'+parsed.data[i].price+'</td><td>'+$statusStr+'</td></tr>';
                 }
                  $('.quotes-tableBody').empty().append(data_html)
                  // setTimeout(function () {
                      $('#table_idQuoets').DataTable({
                         // paging: true,
                         // ordering:false,
                         "fnDrawCallback": function(oSettings) {
                            if ($('.quotes-tableBody tr').length < 10) {
                                $('.dataTables_paginate').hide();
                            }
                        }
                      });
                      tps++
                  //   }, 2500);
                  

                  
            }
         })
      }
      
       
       $.ajax({
       url:  "//<?php echo $serverName;?>/paintpad/webservice/company-people",
       type: "POST",
       data: data, // Send the object.
         success: function(response) {
           var parsed = JSON.parse(response);
          // console.log(parsed)
           // console.log(parsed.data.details[0].logon);
           var data_html = '';
           for(var i=0; i<parsed.data.details.length; i++)
           {
              // console.log(parsed.data.details[i])
               var d = new Date(0);  
               d.setUTCSeconds(parsed.data.details[i].logon);
               var da = moment(d).format("DD/MM/YYYY HH:mm A");
               da = da.toUpperCase();
        
               var da1 = new Date(0);
               da1.setUTCSeconds(parsed.data.details[i].last_activity);
               var la = moment(da1).format("DD/MM/YYYY HH:mm A");
               la = la.toUpperCase();
   
               if(parsed.data.details[i].user_role == '10'){
                 var role = 'Sales Rep';
               }else if(parsed.data.details[i].user_role == '30'){
                 var role = 'Admin';
               }else{
                 var role = '-';
               }
   
               if(parsed.data.details[i].status == '0'){
                 var style = "background:#f9f9f9";
                 data_html += '<tr style="'+style+'" fname="'+parsed.data.details[i].first_name+'" lname="'+parsed.data.details[i].last_name+'" email="'+parsed.data.details[i].email+'" username="'+parsed.data.details[i].username+'" id="id'+parsed.data.details[i].user_id+'" phone="'+parsed.data.details[i].phone+'" mobile="'+parsed.data.details[i].mobile_number+'"  class="SubscriberPeopleUpdate disabledTr" status="'+parsed.data.details[i].status+'" role="'+parsed.data.details[i].user_role+'" user_id="'+parsed.data.details[i].user_id+'" ><td><strike>'+parsed.data.details[i].first_name+" "+parsed.data.details[i].last_name+'</strike></td><td><strike>N</strike></td><td><strike>'+role+'</stike></td><td class="loggedON'+parsed.data.details[i].user_id+'" data="'+da+'"><strike>'+da+'</strike></td><td class="lastActivity'+parsed.data.details[i].user_id+'" data="'+la+'"><strike>'+la+'</strike></td><tr>';
               }else{
                 var style = "";
                 data_html += '<tr style="'+style+'" fname="'+parsed.data.details[i].first_name+'" lname="'+parsed.data.details[i].last_name+'" email="'+parsed.data.details[i].email+'" username="'+parsed.data.details[i].username+'" id="id'+parsed.data.details[i].user_id+'" phone="'+parsed.data.details[i].phone+'" mobile="'+parsed.data.details[i].mobile_number+'"  class="SubscriberPeopleUpdate" status="'+parsed.data.details[i].status+'" role="'+parsed.data.details[i].user_role+'" user_id="'+parsed.data.details[i].user_id+'" ><td>'+parsed.data.details[i].first_name+" "+parsed.data.details[i].last_name+'</td><td>Y</td><td>'+role+'</td><td class="loggedON'+parsed.data.details[i].user_id+'" data="'+da+'">'+da+'</td><td class="lastActivity'+parsed.data.details[i].user_id+'" data="'+la+'">'+la+'</td><tr>';
               }
               
               
   
           }
           $('.subscriberPeople').html(data_html);
            // console.log(parsed.data.length);
           }
       })

     }
   
   function getPaintOrdersAl(){
       var data ={"json":'{"company_id":"<?php echo $_GET['id'];?>"}'};
       $.ajax({
       url:  "//<?php echo $serverName;?>/paintpad/webservice/get-paint-orders-all",
       type: "POST",
       data: data, // Send the object.
         success: function(response) {
           var parsed = JSON.parse(response);
          // console.log(parsed);exit();
           // console.log(parsed.data);exit();
           var data_html = '';
           for(var i=0; i<parsed.data.length; i++)
           {
             // console.log(parsed.data[i]);exit();
              
             var style = "";
             data_html += '<tr class="paintorderEdit" deliver_detail="'+parsed.data[i].deliver_detail+'" notes="'+parsed.data[i].notes+'" paint_order_id="'+parsed.data[i].paint_order_id+'" painter_date_time="'+parsed.data[i].painter_date_time+'" painter_name="'+parsed.data[i].painter_name+'" painter_phone="'+parsed.data[i].painter_phone+'"  pdf_url="'+parsed.data[i].pdf_url+'"  quote_id="'+parsed.data[i].quote_id+'"  quote_type="'+parsed.data[i].quote_type+'">';
             data_html += '<td>'+parsed.data[i].quote_id+'</td>';
             data_html += '<td>'+parsed.data[i].painter_name+'</td>';
             data_html += '<td>'+parsed.data[i].notes+'</td>';
             data_html += '<td>'+parsed.data[i].quoted_by+'</td>';
   
             var q_type = (parsed.data[i].quote_type == '1')?"Interior":'Exterior';
   
             data_html += '<td>'+q_type+'</td>';
             data_html += '<td>'+parsed.data[i].painter_date_time+'</td>';
           }
           $('.PaintOrderBody').html(data_html);
           }
       })
     }
   function getPaintBrandsAll(){
       var data ={"company_id":"<?php echo $_GET['id'];?>"};
       $.ajax({
       url:  "//<?php echo $serverName;?>/paintpad/webservice/brands",
       type: "POST",
       data: data, // Send the object.
         success: function(response) {
           var parsed = JSON.parse(response);
          // console.log(parsed);exit();
           // console.log(parsed.data.brands);exit();
           var data_html = '';
           for(var i=0; i<parsed.data.brands.length; i++)
           {
             // console.log(parsed.data[i]);exit();
             data_html += '<option value="'+parsed.data.brands[i].brand_id+'">'+parsed.data.brands[i].name+'</option>';
           }
           $('select[name="paint_brand"]').html(data_html);
           }
       })
     }
</script>
<script>
   $(document).on('click','.SubscriberPeopleUpdate',function(){
     //alert($(this).attr('fname'));
     var fname = $(this).attr('fname');
     var lname = $(this).attr('lname');
     var email = $(this).attr('email');
     var username = $(this).attr('username');
     var phone = $(this).attr('phone');
     var mobile = $(this).attr('mobile');
     var status = $(this).attr('status');
     var role = $(this).attr('role');
     var user_id = $(this).attr('user_id');
   
     $('.fnameSystem').val(fname);
     $('.lnameSystem').val(lname);
     $('.emailSystem').val(email);
     $('.usernameSystem').val(username);
     $('.phoneSystem').val(phone);
     $('.mobileSystem').val(mobile);
     $('#myModalPeopleUpdate .user_role').val(role);
     // $("select[name='user_role'] option[value='"+role+"']").prop('selected', true);
     $("input[name='user_status'][value='"+status+"']").prop('checked', true);
     $('#user_idUpdateSystem').val(user_id);
   
     $('#myModalPeopleUpdate').modal('show');
     if($('.modal-backdrop').length > 1)
     {
      $('.modal-backdrop:gt(0)').remove();
     }
   
   })
</script>
<script>
   var user_role = $('.user_role').val();
   if(user_role=="")
   {
   
   }
</script>
<script>
   // $(document).unbind().on('click','.SavePeopleSubscriberUpdate',function(e){
   
   $('.SavePeopleSubscriberUpdate').unbind('click').click(function(e){
   		$('.SavePeopleSubscriberUpdate').prop("disabled", true);
   		var first_name = $('.fnameSystem').val();
   		var last_name =  $('.lnameSystem').val();
   		var user_name =  $('.usernameSystem').val();
   		var email =  $('.emailSystem').val();
   		var phone =  $('.phoneSystem').val();
   		var mobile =  $('.mobileSystem').val();
   		var user_role =  $('#myModalPeopleUpdate .user_role').val();
       var user_status =  $('#myModalPeopleUpdate input[name="user_status"]:checked').val();
   		var password = $('#newPassword').val();
   		var company_id = "<?php echo $_GET['id'];?>";
   		var user_id = $('#user_idUpdateSystem').val();
   		var loggedON = $('.loggedON'+user_id).attr('data');
   		var lastActivity = $('.lastActivity'+user_id).attr('data');
   
   
   		var confirm_password = $('.confirm_password').val()
   		var company_id = "<?php echo $_GET['id'];?>";
   
   
   		error = 0;
   		if(email == ""){
   			error = 1;
   			alert('Email is mandatory');
   			$('.SavePeopleSubscriberUpdate').prop("disabled", false);
   			return
   		}
   		if(user_name == ""){
   			error = 1;
   			alert('User Name is mandatory');
   			$('.SavePeopleSubscriberUpdate').prop("disabled", false);
   			return
   		}
   		if(user_role == ""){
   			error = 1;
   			alert('User Role is mandatory');
   			$('.SavePeopleSubscriberUpdate').prop("disabled", false);
   			return
   		}
   
   		if(error == 1){
   			$('.SavePeopleSubscriberUpdate').prop("disabled", false);
   			return
   		}
   
   
   		if($('#newPassword').val() == $('#confirm_password').val())
   		{
   			var form_data = new FormData();
   			var myJSON = JSON.stringify({'first_name':first_name,'last_name':last_name,'username':user_name,'email':email,'company_id':company_id,'phone':phone,'user_role':user_role,'status':user_status,'password':password,'user_id':user_id,'mobile_number':mobile}); 
   			console.log(myJSON);
   			form_data.append('json',myJSON);
   			$.ajax({
   				url: "//<?php echo $serverName;?>/paintpad/webservice/update-company-people",
   				//url:'save-doc',
   				type: "POST",
   				data: form_data,
   				contentType: false,
   				//cache: false,
   				processData:false,
   				dataType : 'JSON',
   				success: function(data){
   					if(data.success == 1){
               getCompanyProple(company_id,'');
   						// alert('done');
   						 // console.log(data);
   						//  var parsed = JSON.parse(data);
   						//  console.log(parsed);  
   						//  var d = new Date(0);  
   						//   d.setUTCSeconds(parsed.userdata.logon);
   						//   var da = moment(d).format("DD/MM/YYYY HH:mm A");
   						//  da = da.toUpperCase();
   
   						//  var da1 = new Date(0);
   						// da1.setUTCSeconds(parsed.userdata.last_activity);
   						//   var la = moment(da1).format("DD/MM/YYYY HH:mm A");
   						//  la = la.toUpperCase();
   						// $('#id'+user_id).empty();
   						// ///<tr fname="'+first_name+'" lname="'+last_name+'" email="'+email+'" username="'+user_name+'" phone="'+phone+'" mobile="'+mobile+'"  class="SubscriberPeopleUpdate" user_id="'+user_id+'" ></tr>
   						// $('#id'+user_id).attr('fname',first_name);
   						// $('#id'+user_id).attr('lname',last_name);
   						// $('#id'+user_id).attr('email',email);
   						// $('#id'+user_id).attr('username',user_name);
   						// $('#id'+user_id).attr('phone',phone);
   						// $('#id'+user_id).attr('mobile',mobile);
   						// $('#id'+user_id).attr('class','SubscriberPeopleUpdate');
   						// $('#id'+user_id).attr('user_id',user_id);
         //       $('#id'+user_id).attr('status',user_status);
         //       $('#id'+user_id).attr('role',user_role);
   
         //       if(user_status == '0'){
         //         $('#id'+user_id).attr('style',"background:#fdeded");
         //       }else{
         //         $('#id'+user_id).attr('style',"background:");
         //       }
   						// //$('#id'+user_id).attr('lname',last_name);
   
   						// $('#id'+user_id).append('<td>'+first_name+" "+last_name+'</td><td>#</td><td class="loggedON'+user_id+'" data="'+loggedON+'">'+loggedON+'</td><td class="loggedON'+user_id+'" data="'+lastActivity+'">'+lastActivity+'</td>');
               
   						alert(data.message);
   						$('#myModalPeopleUpdate').modal('hide');
   						$('.SavePeopleSubscriberUpdate').prop("disabled", false);
   					}
   					else{
   						alert(data.message);
   						$('.SavePeopleSubscriberUpdate').prop("disabled", false);
   					}
             e.stopImmediatePropagation();
             e.preventDefault();
   				}
   			});
   		}
   		else
   		{
   			alert('password does not matched');
   			$('.SavePeopleSubscriberUpdate').prop("disabled", true);
   		}
   
   })
</script>
<script type="text/javascript">
   $('.updateSettings').click(function(){
     $('.application_cost_setting').val($(this).attr('application_cost'));
     $('.profit_markup_setting').val($(this).attr('profit_markup'));
     $('.gross_margin_percent_setting').val($(this).attr('gross_margin_percent'));
     $('.default_deposit_percent_setting').val($(this).attr('default_deposit_percent'));
     $('.terms_condition_setting').val($(this).attr('terms_condition'));
     $('.xero_consumer_key_setting').val($(this).attr('xero_consumer_key')); 
     $('.xero_share_secret_setting').val($(this).attr('xero_share_secret'));
     $('.xero_invoice_line_account_code_setting').val($(this).attr('xero_invoice_line_account_code'));
     $('.account_name_setting').val($(this).attr('account_name'));
     $('.account_BSB_setting').val($(this).attr('account_BSB'));
     $('.account_number_setting').val($(this).attr('account_number'));
     $('.payment_phone_number_setting').val($(this).attr('payment_phone_number'));
     $('.colour_consultant_item_setting').val($(this).attr('colour_consultant_item'));
     $('.price_increase_setting').val($(this).attr('price_increase'));
     $('#myModalsettingsUpdate').modal('show');
     if($('.modal-backdrop').length > 1)
     {
      $('.modal-backdrop:gt(0)').remove();
     }
   })
</script>
<script>
   $('.SaveSettingCompany').click(function(){
      //alert('rk');
      var application_cost = $('.application_cost_setting').val();
      //////////alert(application_cost);
      var profit_markup = $('.profit_markup_setting').val();
      var gross_margin_percent = $('.gross_margin_percent_setting').val();
      var default_deposit_percent = $('.default_deposit_percent_setting').val();
      var terms_condition = $('.terms_condition_setting').val();
      var xero_consumer_key = $('.xero_consumer_key_setting').val(); 
      var xero_share_secret = $('.xero_share_secret_setting').val();
      var xero_invoice_line_account_code = $('.xero_invoice_line_account_code_setting').val();
      var account_name = $('.account_name_setting').val();
      var account_BSB = $('.account_BSB_setting').val();
      var account_number = $('.account_number_setting').val();
      var payment_phone_number = $('.payment_phone_number_setting').val();
      var colour_consultant_item = $('.colour_consultant_item_setting').val();
      var price_increase = $('.price_increase_setting').val();
      var vartncDoc =  $('.terms_condition_settingDoc')[0].files[0]
      var ggs = $('.gst_sendd').val();
      // openDocfile
      var company_id = "<?php echo $_GET['id'];?>"
      // console.log(vartncDoc)
      // return false;
   
      var form_data = new FormData();
      
      var myJSON = JSON.stringify({'application_cost':application_cost,'profit_markup':profit_markup,'gross_margin_percent':gross_margin_percent,'default_deposit_percent':default_deposit_percent,'xero_consumer_key':xero_consumer_key,'xero_share_secret':xero_share_secret,'xero_invoice_line_account_code':xero_invoice_line_account_code,'account_name':account_name,'account_BSB':account_BSB,'account_number':account_number,'payment_phone_number':payment_phone_number,'colour_consultant_item':colour_consultant_item,'price_increase':price_increase,'company_id':company_id,'terms_condition':terms_condition,'terms_conditionDoc':vartncDoc,gst:ggs}); 
      //console.log(myJSON);
      form_data.append('docFile',vartncDoc);
      form_data.append('json',myJSON);
      $.ajax({
       url: "//<?php echo $serverName;?>/paintpad/webservice/save-company-setting",
        //url:'save-doc',
       type: "POST",
       data: form_data,
       contentType: false,
       //cache: false,
       processData:false,
       success: function(data){
           // alert('done');
           var parsed = JSON.parse(data);
           alert(parsed.message);
          console.log(parsed);
         
             
           if(parsed.success==1)
           {
   
           $('.account_name').text(parsed.data.companySetting.account_name);
           $('.application_cost').text(parsed.data.companySetting.application_cost);
           $('.profit_markup').text(parsed.data.companySetting.profit_markup);
           $('.gross_margin_percent').text(parsed.data.companySetting.gross_margin_percent); 
           $('.default_deposit_percent').text(parsed.data.companySetting.default_deposit_percent);
           $('.terms_condition').text(parsed.data.companySetting.terms_condition);
           $('.xero_consumer_key').text(parsed.data.companySetting.xero_consumer_key); 
           $('.xero_share_secret').text(parsed.data.companySetting.xero_share_secret);
           $('.xero_invoice_line_account_code').text(parsed.data.companySetting.xero_invoice_line_account_code); 
           $('.account_BSB').text(parsed.data.companySetting.account_BSB);
           $('.account_number').text(parsed.data.companySetting.account_number); 
           $('.payment_phone_number').text(parsed.data.companySetting.payment_phone_number);
           $('.colour_consultant_item').text(parsed.data.companySetting.colour_consultant_item);  
           $('.price_increase').text(parsed.data.companySetting.price_increase);  
         
            if (parsed.data.companySetting.terms_conditionDoc == '' || parsed.data.companySetting.terms_conditionDoc == null || parsed.data.companySetting.terms_conditionDoc == undefined) {
               $('.Docddd').text('---');  
               $(".subscriberLogoa").removeClass("viewDoc");
               $('.viewDoc').hide() 
               $('.clickDocRemove').hide()
             }
             else{
               $('#openDocfile').attr('src',parsed.data.companySetting.terms_conditionDoc)
               // terms_conditionDoc
               $(".subscriberLogoa").addClass("viewDoc");
               $('.Docddd').html('<button class="viewDoc" >View Doc</button>');
               $('.viewDoc').show() 
               $('.clickDocRemove').show()
               pdfLink = parsed.data.companySetting.terms_conditionDoc
               var val = parsed.data.companySetting.terms_conditionDoc;
               var myString = val.substr(val.indexOf(".") + 1)
               var baseUrl   = window.location.origin;
               if (myString == 'doc' || myString == 'docx') {
                  $('.subscriberLogoa img').attr('src',baseUrl+'/paintpad/uploads/document.png')
               }
               else{
                  $('.subscriberLogoa img').attr('src',baseUrl+'/paintpad/uploads/pdf-doc.png')
                  
               }
             }
           // $('.price_increase').text(parsed.data.companySetting.price_increase);  
   
           $('.updateSettings').attr('account_name',parsed.data.companySetting.account_name) ;
           $('.updateSettings').attr('application_cost',parsed.data.companySetting.application_cost) ;
           $('.updateSettings').attr('profit_markup',parsed.data.companySetting.profit_markup) ;
           $('.updateSettings').attr('gross_margin_percent',parsed.data.companySetting.gross_margin_percent) ;
           $('.updateSettings').attr('default_deposit_percent',parsed.data.companySetting.default_deposit_percent) ;
           $('.updateSettings').attr('terms_condition',parsed.data.companySetting.terms_condition) ;
           $('.updateSettings').attr('xero_consumer_key',parsed.data.companySetting.xero_consumer_key) ;
           $('.updateSettings').attr('xero_share_secret',parsed.data.companySetting.xero_share_secret) ;
           $('.updateSettings').attr('xero_invoice_line_account_code',parsed.data.companySetting.xero_invoice_line_account_code) ;
           $('.updateSettings').attr('account_BSB',parsed.data.companySetting.account_BSB) ;
           $('.updateSettings').attr('account_number',parsed.data.companySetting.account_number) ;
           $('.updateSettings').attr('payment_phone_number',parsed.data.companySetting.payment_phone_number) ;
           $('.updateSettings').attr('colour_consultant_item',parsed.data.companySetting.colour_consultant_item) ;
           $('.updateSettings').attr('price_increase',parsed.data.companySetting.price_increase) ;
           	 $('#myModalsettingsUpdate').modal('hide');
   
           }
       } 
     })
   })
</script>
<script>
   $('.editEmailSetting').click(function(){
     $('#editEmailSettingquote').modal('show');

     var name = $(this).attr('serveremailaddress');
     var bcc = $(this).attr('serverbccaddress');
     var subject = $(this).attr('quoteemailsubject');
     var email_setting_id = $(this).attr('email_setting_id');
     
     // $($('.quoteEmailSettings input[name="bcc_to"]').parent('tr')).replaceWith('<tr><td><label>BCC to</label></td><td><input type="text" class="form-control bcc_to_quote" name="bcc_to1" value"'+bcc+'"></td></tr>')
   
     $('.attachmentquote').each(function(){
       var nameAttachment = $(this).val();
       var id = $(this).attr('data-id');
       $('.'+id+'quote').remove();
        $('#editEmailSettingquote .quoteEmailSettings').append("<tr class='"+id+"quote'><input type='hidden' class='docquoteid' value='"+id+"'><td></td><td><input type='text' class='docquote form-control' value='"+nameAttachment+"' readonly></td><td class='deletequotedoc' id='"+id+"'>&times;</td></tr>");
     })
     // $('.bcc_to_quote').val(name);
     // $('.email_subject_quote').val(subject);
     //$('').val(name);
   
     $('.bcc_to_quote').val(bcc);
     $('.email_subject_quote').val(subject);
     $('.email_setting_id').val(email_setting_id);
     //bcc_to_quote
   
     if($('.modal-backdrop').length > 1)
     {
      $('.modal-backdrop:gt(0)').remove();
     }
   })
</script>
<script type="text/javascript">
   $('.editEmailSettingJss').click(function(){
     $('#editEmailSettingjss').modal('show');
     var name = $(this).attr('serveremailaddress');
     var bcc = $(this).attr('serverbccaddress');
     var subject = $(this).attr('quoteemailsubject');
     var email_setting_id = $(this).attr('email_setting_id');
   
     $('.attachmentjss').each(function(){
       var nameAttachment = $(this).val();
       var id = $(this).attr('data-id');
       $('.'+id+'quote').remove();
        $('#editEmailSettingjss .quoteEmailSettings').append("<tr class='"+id+"quote'><input type='hidden' class='docquoteid' value='"+id+"'><td></td><td><input type='text' class='docquote form-control' value='"+nameAttachment+"' readonly></td><td class='deletequotedoc' id='"+id+"'>&times;</td></tr>");
     })
     //$('').val(name);
     $('#editEmailSettingjss .bcc_to_jss').val(bcc);
     $('#editEmailSettingjss .email_subject_jss').val(subject);
     $('#editEmailSettingjss .email_setting_id').val(email_setting_id);    
     //bcc_to_quote
   
     if($('.modal-backdrop').length > 1)
     {
      $('.modal-backdrop:gt(0)').remove();
     }
   })
</script>
<script>
   $('#editEmailSettingquote .emailQuoteDocQuote').change(function(){
     var name = $(this).find("option:selected").text();
     $('#editEmailSettingquote .quoteEmailSettings').append("<tr class='"+$(this).val()+"quote'><input type='hidden' class='docquoteid' value='"+$(this).val()+"'><td></td><td><input type='text' class='docquote form-control' value='"+name+"' readonly></td><td class='deletequotedoc' id='"+$(this).val()+"'>&times;</td></tr>");
   })
</script>
<script>
   $('#editEmailSettingjss .emailQuoteDocJss').change(function(){
     var name = $(this).find("option:selected").text();
     $('#editEmailSettingjss .quoteEmailSettings').append("<tr class='"+$(this).val()+"quote'><input type='hidden' class='docquoteid' value='"+$(this).val()+"'><td></td><td><input type='text' class='docquote form-control' value='"+name+"' readonly></td><td class='deletequotedoc' id='"+$(this).val()+"'>&times;</td></tr>");
   })
</script>
<script>
   $(document).on('click','.deletequotedoc',function(){
     var id = $(this).attr('id');
    $('.'+id+'quote').remove();
    $(this).closest("tr").remove();
    $('.emailQuoteDocQuote').prop('selectedIndex',0);
    $('.emailQuoteDocJss').prop('selectedIndex',0);

    //$(this).closest("input").remove();
     //$('.'+id+'quote').remove();
   })
</script>
<script>
   $('.saveEmailSettingquote').unbind('click').click(function(){
   
     var id = [];
     $('#editEmailSettingquote .docquoteid').each(function(){
         id.push($(this).val());
     })
     console.log(id);
     var unique = id.filter(function(itm, i, id) {
       return i == id.indexOf(itm);
     });
   //  console.log(unique);
     var id = unique.join(",");
     console.log(id);   
    // return false;
     var email_setting_id = $('.email_setting_id').val();
     var bccTo =  $('.bcc_to_quote').val();
     var subject =  $('.email_subject_quote').val();
     var form_data = new FormData();
     //alert(email_setting_id);
     if(email_setting_id == "")
     {
       email_setting_id = '0';
     }   
     var myJSON = JSON.stringify({'doc_id':id,'company_id':<?php echo $_GET['id'];?>,'bcc_to':bccTo,'quote_email_subject':subject,'type_id':'1','email_setting_id':email_setting_id}); 
    //console.log(myJSON);
    form_data.append('json',myJSON); 
    $.ajax({
           url: "//<?php echo $serverName;?>/paintpad/webservice/save-email-setting",
           //url:'save-doc',
           type: "POST",
           data: form_data,
           contentType: false,
           //cache: false,
           processData:false,
           success: function(data){
                //alert('done');
               // console.log(data);
                var parsed = JSON.parse(data);
               console.log(parsed);  
                alert(parsed.message);
                 // $('.prepBody').append('<tr class="prepEdit " name="'+parsed.data.prepLevel[0].name+'" id="'+parsed.data.prepLevel[0].id+'" deletable="'+parsed.data.prepLevel[0].deletable+'" is_default="'+parsed.data.prepLevel[0].is_default+'" uplift_cost="'+parsed.data.prepLevel[0].uplift_cost+'" uplift_time="'+parsed.data.prepLevel[0].uplift_time+'"><td>'+parsed.data.prepLevel[0].name+'</td><td>'+parsed.data.prepLevel[0].uplift_cost+'</td><td>'+parsed.data.prepLevel[0].uplift_time+'</td></tr>')
                  //$('#createPrep').modal('hide');
                     function isEmpty(obj) {
                     for(var key in obj) {
                       if(obj.hasOwnProperty(key))
                          return false;
                     }
                      return true;
                     }
   
                     ///quote email setting fill up starts here
                     $('.serverEmailAddress').text(parsed.data.quoteEmailSettingsDetail.server_email_address);
                     $('.serverBccAddress').text(parsed.data.quoteEmailSettingsDetail.bcc_to);
                     $('.quoteEmailSubject').text(parsed.data.quoteEmailSettingsDetail.email_subject);
                     $('.editEmailSetting').attr('serverEmailAddress',parsed.data.quoteEmailSettingsDetail.server_email_address);
                     $('.editEmailSetting').attr('serverBccAddress',parsed.data.quoteEmailSettingsDetail.bcc_to);
                     $('.editEmailSetting').attr('quoteEmailSubject',parsed.data.quoteEmailSettingsDetail.email_subject);
                     $('.editEmailSetting').attr('email_setting_id',parsed.data.quoteEmailSettingsDetail.email_setting_id); 
                     //$('.editEmailSetting').attr('doc',parsed.data.quoteEmailSettingsDetail.email_subject);
                     if(isEmpty(parsed.data.quoteEmailSettingsDetail))
                     {
   
                     }
                     else
                     {
                       $('.quoteIncludeDocuments').html('')
                       var doc_html = '';
                       for(var i=0; i<parsed.data.quoteEmailSettingsDetail.doc.length; i++)
                     {
                       doc_html += '<tr><td></td><td><input type="text" class ="attachmentquote" readonly value="'+parsed.data.quoteEmailSettingsDetail.doc[i].file_name+'" data-id ="'+parsed.data.quoteEmailSettingsDetail.doc[i].file_id+'"></td></tr>';
                     
                     }
                     $('.quoteIncludeDocuments').html(doc_html)
                     
                     }
   
                    // $('#editEmailSettingjss').modal('hide');                 
                    $('#editEmailSettingquote').modal('hide');
          } 
     })   
   })
</script>
<script>
   $('.saveEmailSettingjss').click(function(){
     var id = [];
     $('#editEmailSettingjss .docquoteid').each(function(){
         id.push($(this).val());
     })
   //  console.log(id);
     var unique = id.filter(function(itm, i, id) {
       return i == id.indexOf(itm);
     });
     //console.log(unique);
     var id = unique.join(",");
     console.log(id); 
     // return false;
     var email_setting_id = $('.email_setting_id').val();
     var bccTo =  $('.bcc_to_jss').val();
     var subject =  $('.email_subject_jss').val();
     var form_data = new FormData();
     if(email_setting_id=="")
     {
       email_setting_id = '0'
     }
    
     var myJSON = JSON.stringify({'doc_id':id,'company_id':<?php echo $_GET['id'];?>,'bcc_to':bccTo,'quote_email_subject':subject,'type_id':2,'email_setting_id':email_setting_id}); 
   // console.log(myJSON);
    form_data.append('json',myJSON);    
    $.ajax({
           url: "//<?php echo $serverName;?>/paintpad/webservice/save-email-setting",
           //url:'save-doc',
           type: "POST",
           data: form_data,
           contentType: false,
           //cache: false,
           processData:false,
           success: function(data){
                     //alert('done');
                    // console.log(data);
                     var parsed = JSON.parse(data);
                    // console.log(parsed);  
                     alert(parsed.message);
   
                     function isEmpty(obj) {
                     for(var key in obj) {
                       if(obj.hasOwnProperty(key))
                          return false;
                     }
                      return true;
                     }
   
                     ///quote email setting fill up starts here
                     $('.serverEmailAddressjss').text(parsed.data.jssEmailSettingsDetail.server_email_address);
                     $('.serverBccAddressjss').text(parsed.data.jssEmailSettingsDetail.bcc_to);
                     $('.quoteEmailSubjectjss').text(parsed.data.jssEmailSettingsDetail.email_subject);
                     $('.editEmailSettingJss').attr('serverEmailAddress',parsed.data.jssEmailSettingsDetail.server_email_address);
                     $('.editEmailSettingJss').attr('serverBccAddress',parsed.data.jssEmailSettingsDetail.bcc_to);
                     $('.editEmailSettingJss').attr('quoteEmailSubject',parsed.data.jssEmailSettingsDetail.email_subject);
                     $('.editEmailSettingJss').attr('email_setting_id',parsed.data.jssEmailSettingsDetail.email_setting_id); 
                     //$('.editEmailSetting').attr('doc',parsed.data.quoteEmailSettingsDetail.email_subject);
                     if(isEmpty(parsed.data.jssEmailSettingsDetail))
                     {
   
                     }
                     else
                     {
                       $('.quoteIncludeDocumentsjss').html('')
                       var doc_html = '';
                       for(var i=0; i<parsed.data.jssEmailSettingsDetail.doc.length; i++)
                     {
                       doc_html += '<tr><td></td><td><input type="text" class ="attachmentjss" readonly value="'+parsed.data.jssEmailSettingsDetail.doc[i].file_name+'" data-id ="'+parsed.data.jssEmailSettingsDetail.doc[i].file_id+'"></td></tr>';
                     }
                     $('.quoteIncludeDocumentsjss').html(doc_html)
                     }
   
                     $('#editEmailSettingjss').modal('hide');
          } 
     })     
   })
</script>
<!-- open edit model for subscriber detail -->
<script>
   $('.editSub').click(function(){
     $('.sub_name').val($('.subscriberName').data("name"));
     $('.sub_GST').val($('.subscriberpaint_GST').data("gst"));
     $('.sub_address').val($('.subscriberAddress').data("address"));
     $('.sub_suburb').val($('.subscriberSuburb').data("suburb"));
     $('.sub_state').val($('.subscriberState').data("state"));
     $('.sub_postcode').val($('.subscriberState').data("postcode"));
     $('.sub_country').val($('.subscriberCountry').data("country"));
     $('.sub_phone').val($('.subscriberPhone').data("phone"));
     $('.sub_email').val($('.subscriberEmail').data("email"));
     $('.sub_abn').val($('.subscriberAbn').data("abn"));
     $('.sub_insurance_comp_detail').val($('.subscriberinsurance_comp_detail').data("insurance_comp_detail"));
     $('.sub_worker_insurance_detail').val($('.subscriberworker_insurance_detail').data("worker_insurance_detail"));
     $('.sub_license_number').val($('.subscriberlicense_number').data("license_number"));
   
     $(".sub_paint_brand").val($('.subscriberpaint_brand').data("paint_brand")).change();
     $(".sub_timezone").val($('.subscribertimezone').data("timezone")).change();
     // $('.sub_paint_brand').val($('.subscriberpaint_brand').data("paint_brand"));
     $('.sub_website').val($('.subscriberWebsite').data("website"));
     // $('.sub_status').val($('.subscriberstatus').data("status"));
     if($('.subscriberstatus').data("status")==1)
     {
       $('.sub_status-1').attr('checked','checked');
     }
     else
     {
       $('.sub_status-0').attr('checked','checked');
     }
     $('#editSubModal').modal('show');
   })
</script>
<script type="text/javascript">
   $('.updateSub').click(function(){
     var name = $('.sub_name').val();
     var address = $('.sub_address').val();
     var suburb = $('.sub_suburb').val();
     var state = $('.sub_state').val();
     var postcode = $('.sub_postcode').val();
     var country = $('.sub_country').val();
     var phone = $('.sub_phone').val();
     var email = $('.sub_email').val();
     var abn = $('.sub_abn').val();
     var insurance_comp_detail = $('.sub_insurance_comp_detail').val();
     var worker_insurance_detail = $('.sub_worker_insurance_detail').val();
     var license_number = $('.sub_license_number').val();
     var paint_brand = $('.sub_paint_brand').val();
     var timezone = $('.sub_timezone').val();
     var website = $('.sub_website').val();
     var gst = $('.sub_GST').val();
     var status = $('input[name="sub_status"]:checked').val();
     var company_id = "<?php echo $_GET['id'];?>";
     
      var form_data = new FormData();
   
      var myJSON = JSON.stringify({
                    'name':name,
                    'address':address,
                    'suburb':suburb,
                    'state':state,
                    'postcode':postcode,
                    'country':country,
                    'phone':phone,
                    'email':email,
                    'website':website,
                    'abn':abn,
                    'insurance_comp_detail':insurance_comp_detail,
                    'worker_insurance_detail':worker_insurance_detail,
                    'license_number':license_number,
                    'paint_brand':paint_brand,
                    'timezone':timezone,
                    'status':status,
                    'company_id':company_id,
                    'gst':gst,
                    
                  }); 
     // console.log(myJSON);exit;
     form_data.append('json',myJSON);
       $.ajax({
           url: "//<?php echo $serverName;?>/paintpad/webservice/update-subscriber-detail",
           //url:'save-doc',
           type: "POST",
           data: form_data,
           contentType: false,
           //cache: false,
           processData:false,
           success: function(data){
             alert('Subscriber detail updated successfully.');
             location.reload();
               //  alert('done');
               // // console.log(data);exit();
               //  var parsed = JSON.parse(data);
               // // console.log(parsed);  
               //  alert(parsed.data.id);
               //   var company_id = parsed.data.id;
               //   // var subscriber_idd = $('.subscriber_id').val();
               //   var data ={"json":'{"company_id":"'+company_id+'"}'};
   
               //   $.ajax({
               //   url:  "//<?php echo $serverName;?>/paintpad/webservicecopy/all-company-data",
               //   type: "POST",
               //   data: data, // Send the object.
               //     success: function(response) {
               //       var parsed = JSON.parse(response);
               //        console.log(parsed);
   
               //         $('.subscriberName').html("<label>"+parsed.data.compDetail.name+"</label>");
               //         $('.subscriberName').data("name",parsed.data.compDetail.name);
               //         $('.subscriberName').attr("data-name",parsed.data.compDetail.name);
   
               //         $('.subscriberAddress').html("<label>"+parsed.data.compDetail.address+"</label>");
               //         $('.subscriberAddress').attr("data-address",parsed.data.compDetail.address);
   
               //         $('.subscriberSuburb').html("<label>"+parsed.data.compDetail.suburb+"</label>");
               //         $('.subscriberSuburb').attr("data-suburb",parsed.data.compDetail.suburb);
   
               //         $('.subscriberState').html("<label>"+parsed.data.compDetail.state+" "+parsed.data.compDetail.postcode+ "</label>");
               //         $('.subscriberState').attr("data-state",parsed.data.compDetail.state);
               //         $('.subscriberState').attr("data-postcode",parsed.data.compDetail.postcode);
   
               //         $('.subscriberCountry').html("<label>"+parsed.data.compDetail.country+"</label>");
               //         $('.subscriberCountry').attr("data-country",parsed.data.compDetail.country);
   
               //         $('.subscriberPhone').html("<label>"+parsed.data.compDetail.phone+"</label>");
               //         $('.subscriberPhone').attr("data-phone",parsed.data.compDetail.phone);
   
               //         $('.subscriberEmail').html("<label>"+parsed.data.compDetail.email+"</label>");
               //         $('.subscriberEmail').attr("data-email",parsed.data.compDetail.email);
   
               //         $('.subscriberWebsite').html("<label><a>"+parsed.data.compDetail.website+"</a></label>");
               //         $('.subscriberWebsite').attr("data-website",parsed.data.compDetail.website);
               // }});
          } 
       })
   
   })
</script>
<script>
   $('.prepbutton').click(function(){
     $('.deletePrep').remove();
     $('#createPrep').modal('show');
     if($('.modal-backdrop').length > 1)
     {
      $('.modal-backdrop:gt(0)').remove();
     }
   })
</script>
<script type="text/javascript">
   $('.savePrepCreate').click(function(){
     var name = $('.prep_level').val();
     var uplift_cost = $('.uplift_cost').val();
     var uplift_time = $('.uplift_time').val();
     var prep_id = 0;
     if($('.is_default').prop('checked')==true)
     {
       var is_default = 1;
     }
     else
     {
       var is_default =0;
     }
      var company_id = "<?php echo $_GET['id'];?>";
      var form_data = new FormData();
   
      var myJSON = JSON.stringify({'name':name,'uplift_cost':uplift_cost,'uplift_time':uplift_time,'id':prep_id,'is_default':is_default,'company_id':company_id}); 
     // console.log(myJSON);
     form_data.append('json',myJSON);
       $.ajax({
           url: "//<?php echo $serverName;?>/paintpad/webservice/save-prep-level",
           //url:'save-doc',
           type: "POST",
           data: form_data,
           contentType: false,
           //cache: false,
           processData:false,
           success: function(data){
                //alert('done');
              //  console.log(data);
                var parsed = JSON.parse(data);
              //  console.log(parsed);  
                alert(parsed.message);
                 $('.prepBody').append('<tr class="prepEdit " name="'+parsed.data.prepLevel[0].name+'" id="'+parsed.data.prepLevel[0].id+'" deletable="'+parsed.data.prepLevel[0].deletable+'" is_default="'+parsed.data.prepLevel[0].is_default+'" uplift_cost="'+parsed.data.prepLevel[0].uplift_cost+'" uplift_time="'+parsed.data.prepLevel[0].uplift_time+'"><td>'+parsed.data.prepLevel[0].name+'</td><td>'+parsed.data.prepLevel[0].uplift_cost+'</td><td>'+parsed.data.prepLevel[0].uplift_time+'</td></tr>')
                  $('#createPrep').modal('hide');
                  $('.prep_level').val('')
                  $('.uplift_cost').val('')
                  $('.uplift_time').val('')
                  $('.is_default').prop('checked',false)
          } 
       })
   
   })
</script>
<script>
   $(document).on('click','.prepEdit',function(){
     var name = $(this).attr('name');
     var id = $(this).attr('id');
     var is_default = $(this).attr('is_default');
     var uplift_time = $(this).attr('uplift_time');
     var uplift_cost = $(this).attr('uplift_cost');
     var deletable = $(this).attr('deletable');
     if(deletable==1)
     {
       $('.deletePREP').html("<span class='deletePrep pull-right btn btn-danger'  id='"+id+"'>Delete</span>")
     }
     $('#UpdatePrep').modal('show');
     if($('.modal-backdrop').length > 1)
     {
      $('.modal-backdrop:gt(0)').remove();
     }
     $('.prep_levelupdate').val(name);
     $('.uplift_costupdate').val(uplift_cost);
     $('.uplift_timeupdate').val(uplift_time);
     $('.idprep').val(id);
     if(is_default==1)
     {
       $('.is_defaultupdate').attr('checked','checked');
     }
   
   })
</script>
<script type="text/javascript">
   // $(document).on('change','.imageDocupdate',function(){
   //   var v = $(this).val()
   //   var vv = v.replace('/C:\fakepath\/', 'W3Schools')
     
   //   $('.imageName').text(v);
   // })
     $('.savePrepUpdate').click(function(){
       var name = $('.prep_levelupdate').val();
       var uplift_cost = $('.uplift_costupdate').val();
       var uplift_time = $('.uplift_timeupdate').val();
       var prep_id = $('.idprep').val();
       if($('.is_defaultupdate').prop('checked')==true)
       {
         var is_default = 1;
       }
       else
       {
         var is_default =0;
       }
        var company_id = "<?php echo $_GET['id'];?>";
        var form_data = new FormData();
   
        var myJSON = JSON.stringify({'name':name,'uplift_cost':uplift_cost,'uplift_time':uplift_time,'id':prep_id,'is_default':is_default,'company_id':company_id}); 
      // console.log(myJSON);
       form_data.append('json',myJSON);
         $.ajax({
             url: "//<?php echo $serverName;?>/paintpad/webservice/save-prep-level",
             //url:'save-doc',
             type: "POST",
             data: form_data,
             contentType: false,
             //cache: false,
             processData:false,
             success: function(data){
                 // alert('done');
                 // console.log(data);
                  var parsed = JSON.parse(data);
                 // console.log(parsed);  
                   alert(parsed.message);
                   $('#'+parsed.data.prepLevel[0].id).attr('class','prepEdit');
                   $('#'+parsed.data.prepLevel[0].id).attr('name',parsed.data.prepLevel[0].name);
                   $('#'+parsed.data.prepLevel[0].id).attr('id',parsed.data.prepLevel[0].id);
                   $('#'+parsed.data.prepLevel[0].id).attr('deletable',parsed.data.prepLevel[0].deletable);
                   $('#'+parsed.data.prepLevel[0].id).attr('is_default',parsed.data.prepLevel[0].is_default);
                   $('#'+parsed.data.prepLevel[0].id).attr('uplift_cost',parsed.data.prepLevel[0].uplift_cost);
                   $('#'+parsed.data.prepLevel[0].id).attr('uplift_time',parsed.data.prepLevel[0].uplift_time);
                   // <tr class="prepEdit" name="'+parsed.data.prepLevel[0].name+'" id="'+parsed.data.prepLevel[0].id+'" deletable="'+parsed.data.prepLevel[0].deletable+'" is_default="'+parsed.data.prepLevel[0].is_default+'" uplift_cost="'+parsed.data.prepLevel[0].uplift_cost+'" uplift_time="'+parsed.data.prepLevel[0].uplift_time+'">
                   $('#'+parsed.data.prepLevel[0].id).html('<td>'+parsed.data.prepLevel[0].name+'</td><td>'+parsed.data.prepLevel[0].uplift_cost+'</td><td>'+parsed.data.prepLevel[0].uplift_time+'</td>')
                    $('#UpdatePrep').modal('hide');
            } 
         })
   
     })  
</script>
<script>
   // $(document).on('click','.delAppointmenttype',function(e){
     $('.delAppointmenttype').unbind('click').click(function(){
     // alert($(this).data('id'));
     // e.preventDefault();
     var form_data = new FormData();
     var id = $(this).data('id');
     var myJSON = JSON.stringify({'id':id}); 
    // console.log(myJSON);
     form_data.append('json',myJSON);
       $.ajax({
         url: "//<?php echo $serverName;?>/paintpad/webservice/delete-appointment-type",
         //url:'save-doc',
         type: "POST",
         data: form_data,
         contentType: false,
         //cache: false,
         processData:false,
         success: function(data){
            // console.log(data);
             var parsed = JSON.parse(data);
            // console.log(parsed);
             if(parsed.success==1)
             {
               // alert('Appointment type deleted');
                $('.AppointmentBody #'+id).remove();
                $('#myappointmentTypeModal').modal('hide');
               // return false;
             }
        } 
     })
      // e.stopImmediatePropagation();
      // return false;  
   })
</script>
<script>
   $(document).on('click','.deletePrep',function(e){
     //alert($(this).attr('id'));
     e.preventDefault();
     var form_data = new FormData();
     var id = $(this).attr('id');
     var myJSON = JSON.stringify({'id':$(this).attr('id')}); 
    // console.log(myJSON);
     form_data.append('json',myJSON);
       $.ajax({
         url: "//<?php echo $serverName;?>/paintpad/webservice/delete-prep-level",
         //url:'save-doc',
         type: "POST",
         data: form_data,
         contentType: false,
         //cache: false,
         processData:false,
         success: function(data){
            // console.log(data);
             var parsed = JSON.parse(data);
            // console.log(parsed);
             if(parsed.success==1)
             {
               alert('prep deleted');
                $('#'+id).remove();
                $('#UpdatePrep').modal('hide');
               // return false;
             }
        } 
     })
      e.stopImmediatePropagation();
      return false;  
   })
</script>
<script>
   $('.roombutton').unbind('click').click(function(){
     //alert('createRoomType');
     $('#myroomModal').modal('show');
    if($('.modal-backdrop').length > 1)
     {
      $('.modal-backdrop:gt(0)').remove();
     }
   })
</script>
<script>
   $('.appointmentTypebutton').unbind('click').click(function(){
     //alert('createRoomType');
     $('.appointmentTypeName').val();
     $('.appointmentTypeId').val('0');
     
     $('#myappointmentTypeModal').modal('show');
    if($('.modal-backdrop').length > 1)
     {
      $('.modal-backdrop:gt(0)').remove();
     }
   })
</script>
<script>
   $('.saveRoom').unbind('click').click(function(e){
       var room_name = $('.room_name').val();
       var room_type = $('.room_type').val();
       var room_multiples = $('.room_multiples').val();
       var company_id = "<?php echo $_GET['id']?>";
       var room_id = '0'
       var form_data = new FormData();
       var myJSON = JSON.stringify({'name':room_name,'type_id':room_type,'multiples':room_multiples,'company_id':company_id,'room_id':room_id});
       form_data.append('json',myJSON);
       $.ajax({
         url: "//<?php echo $serverName;?>/paintpad/webservicecopy/save-room-type",
         type: "POST",
         data: form_data,
         contentType: false,
         //cache: false,
         processData:false,
         success: function(data){
            // console.log(data);
             var parsed = JSON.parse(data);
            // console.log(parsed);
             if(parsed.success==1)
             {
               alert(parsed.message);
               $('#myroomModal').modal('hide');
               // return false;
               $('.RoomBody').append('<tr class="roomEdit" name="'+parsed.data.prepLevel.roomTypes.name+'" id="'+parsed.data.prepLevel.roomTypes.id+'" type_id="'+parsed.data.prepLevel.roomTypes.type_id+'" type="'+parsed.data.prepLevel.roomTypes.type_id+'" company_id="'+parsed.data.prepLevel.roomTypes.company_id+'" multiples="'+parsed.data.prepLevel.roomTypes.multiples+'" blue_image="'+parsed.data.prepLevel.roomTypes.blue_image+'" white_image="'+parsed.data.prepLevel.roomTypes.white_image+'" room_order_id="'+parsed.data.prepLevel.roomTypes.room_order_id+'"><td>'+parsed.data.prepLevel.roomTypes.name+'</td><td>'+parsed.data.prepLevel.roomTypes.type+'</td></tr>')
             }
             else
             {
               alert(parsed.message)
             }
        } 
       })
       e.stopImmediatePropagation();
       return false;         
   })
</script>
<script>
   $('.saveAppointmentType').unbind('click').click(function(e){
       var type_name = $('.appointmentTypeName').val();
       var appointment_type_id = $('.appointmentTypeId').val();
       // var appointment_type_id = '0'
       var form_data = new FormData();
       var myJSON = JSON.stringify({'name':type_name,'id':appointment_type_id});
       form_data.append('json',myJSON);
       $.ajax({
         url: "//<?php echo $serverName;?>/paintpad/webservice/save-appointment-type",
         type: "POST",
         data: form_data,
         contentType: false,
         //cache: false,
         processData:false,
         success: function(data){
            // console.log(data);
             var parsed = JSON.parse(data);
            // console.log(parsed);
             if(parsed.success==1)
             {
               // alert(parsed.message);
               $('#myappointmentTypeModal').modal('hide');
               // return false; var rowData = '';
               var rowData = '';
               $.each(parsed.data, function(i,v){
                 rowData += '<tr  id="'+v.id+'" data-orderid="'+(++i)+'" class="AppointmentEdit"><td>'+v.name+'</td></tr>';
               });
               $('.AppointmentBody').html(rowData);
             }
             else
             {
               alert(parsed.message)
             }
        } 
       })
       e.stopImmediatePropagation();
       return false;         
   })
</script>
<script>
   $('div').on('click','.roomEdit',function(){
     //alert('createRoomType');
     var room_id = $(this).attr('id');
     var type_id = $(this).attr('type_id');
     var multiples = $(this).attr('multiples');
     var name = $(this).attr('name');
   
     $('.room_name_update').val(name);
     $('.room_type_update').val(type_id);
     $('.room_multiples_update').val(multiples);
     $('.room_id_update').val(room_id);
   
     $('#myroomModalUpdate').modal('show');
     if($('.modal-backdrop').length > 1)
     {
       $('.modal-backdrop:gt(0)').remove();
     }
   })
</script>
<script>
   $('div').on('click','.AppointmentEdit',function(){
     $('.AppointmentEdit').unbind('click');
     //alert('createRoomType');
     var id = $(this).attr('id');
     var name = $($(this).find('td')).text();
   
     $('.appointmentTypeId').val(id);
     $('.appointmentTypeName').val(name);
     $('#myappointmentTypeModal .delAppointmenttype').attr('data-id',id);
     $('.delAppointmentTypeRow').show();
      
   
     $('#myappointmentTypeModal').modal('show');
     if($('.modal-backdrop').length > 1)
     {
       $('.modal-backdrop:gt(0)').remove();
     }
   })
</script>
<script>
   $('input[type=email]').change(function() {
     var email = $(this).val()
     // console.log(email)
     if (validateEmail(email) != true) {
       $(this).val('')
     }
   })
    function validateEmail($email) {
     var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
     return emailReg.test( $email );
   }
</script>
<script>
   $image_crop = $('#image_demo').croppie({
     enableExif: true,
     viewport: {
       width:270,
       height:170,
       type:'square' //circle
     },
     boundary:{
       width:450,
       height:300
     },
     enforceBoundary:false
   });
   function new_croppie(type){
     $('#image_demo').croppie('destroy');
     if (type == 'upload') {
         $image_crop = $('#image_demo').croppie({
           enableExif: true,
           viewport: {
             width:270,
             height:170,
             type:'square' //circle
           },
           boundary:{
             width:450,
             height:300
           },
           enforceBoundary:false
         });
       }
       else if(type == 'pdflogochange'){
         $image_crop = $('#image_demo').croppie({
           enableExif: true,
           viewport: {
             width:260,
             height:90,
             type:'square' //circle
           },
           boundary:{
             width:450,
             height:300
           },
           enforceBoundary:false
         });
       }
       else{
         $image_crop = $('#image_demo').croppie({
           enableExif: true,
           viewport: {
             width:270,
             height:170,
             type:'square' //circle
           },
           boundary:{
             width:450,
             height:300
           },
           enforceBoundary:false
         });
       }
     }
     
     
   $('#insert_image').on('change', function(){
     // $(document).on('change', '#insert_image', function(){
     var reader = new FileReader();
     reader.onload = function (event) {
       $image_crop.croppie('bind', {
         url: event.target.result
       }).then(function(){
         // $('.cr-slider').attr({'min':0});
         console.log('jQuery bind complete');
       });
     }
     reader.readAsDataURL(this.files[0]);
     if($('.crop_image').hasClass('accredatelogochange') ||$('.crop_image').hasClass('pdflogochange') ){
       $('.crop_image').addClass('pdflogochange');
       $('.crop_image').removeClass('accredatelogochange');
       $('.crop_image').removeClass('upload');
     }else{
       $('.crop_image').addClass('pdflogochange');
       $('.crop_image').removeClass('accredatelogochange');
       $('.crop_image').removeClass('upload');
     }
     $('#insertimageModal').modal('show');
   });
   $('#insert_image_logo').on('change', function(){
     // $(document).on('change', '#insert_image_logo', function(){
     var reader = new FileReader();
     reader.onload = function (event) {
       $image_crop.croppie('bind', {
         url: event.target.result
       }).then(function(){
         // $('.cr-slider').attr({'min':0});
         console.log('jQuery bind complete');
       });
     }
     reader.readAsDataURL(this.files[0]);
     if($('.crop_image').hasClass('pdflogochange') || $('.crop_image').hasClass('accredatelogochange')){
       $('.crop_image').removeClass('accredatelogochange');
       $('.crop_image').removeClass('pdflogochange');
       $('.crop_image').addClass('upload');
     }else{
       $('.crop_image').addClass('upload');
       $('.crop_image').removeClass('accredatelogochange');
       $('.crop_image').removeClass('pdflogochange');
     }
     $('#insertimageModal').modal('show');
   });
   $('#insert_accredate_image').on('change', function(){
     // $(document).on('change', '#insert_accredate_image', function(){
     var reader = new FileReader();
     reader.onload = function (event) {
       $image_crop.croppie('bind', {
         url: event.target.result
       }).then(function(){
         // $('.cr-slider').attr({'min':0});
         console.log('jQuery bind complete');
       });
     }
     reader.readAsDataURL(this.files[0]);
     if($('.crop_image').hasClass('pdflogochange') || $('.crop_image').hasClass('upload')){
       $('.crop_image').addClass('accredatelogochange');
       $('.crop_image').removeClass('pdflogochange');
       $('.crop_image').removeClass('upload');
       
     }else{
       $('.crop_image').addClass('accredatelogochange');
       $('.crop_image').removeClass('pdflogochange');
       $('.crop_image').removeClass('upload');
     }
     $('#insertimageModal').modal('show');
   });
</script>
<script>
   $('.saveRoomUpdate').unbind('click').click(function(e){
       var room_name = $('.room_name_update').val();
       var room_type = $('.room_type_update').val();
       var room_multiples = $('.room_multiples_update').val();
       var company_id = "<?php echo $_GET['id']?>";
       var room_id = $('.room_id_update').val();
       var form_data = new FormData();
       var myJSON = JSON.stringify({'name':room_name,'type_id':room_type,'multiples':room_multiples,'company_id':company_id,'room_id':room_id});
       form_data.append('json',myJSON);
       $.ajax({
         url: "//<?php echo $serverName;?>/paintpad/webservicecopy/save-room-type",
         type: "POST",
         data: form_data,
         contentType: false,
         //cache: false,
         processData:false,
         success: function(data){
            // console.log(data);
             var parsed = JSON.parse(data);
            // console.log(parsed);
             if(parsed.success==1)
             {
               alert(parsed.message);
               $('#myroomModalUpdate').modal('hide');
               $('#'+room_id).attr('name',parsed.data.prepLevel.roomTypes.name)
               $('#'+room_id).attr('type_id',parsed.data.prepLevel.roomTypes.type_id)
               $('#'+room_id).attr('type',parsed.data.prepLevel.roomTypes.type)
               $('#'+room_id).attr('multiples',parsed.data.prepLevel.roomTypes.multiples)
               $('#'+room_id).html("<td>"+parsed.data.prepLevel.roomTypes.name+"</td><td>"+parsed.data.prepLevel.roomTypes.type+"</td>")
             }
             else
             {
               alert(parsed.message)
             }
        } 
       })
       e.stopImmediatePropagation();
       return false;         
   })
   
    $( ".AppointmentBody" ).sortable({
         delay: 150,
         stop: function() {
             var selectedData = new Array();
             $('.AppointmentBody>tr').each(function() {
                 // selectedData[$(this).attr("id")] = $(this).data("orderid");
                 selectedData.push($(this).attr("id"));
             });
             updateOrder(selectedData);
         }
     });
   
   
     function updateOrder(data) {
         $.ajax({
             url:  "//<?php echo $serverName;?>/paintpad/webservice/updte-appointment-position",
             type:'post',
             data:{position:data},
             success:function(){
                 // alert('your change successfully saved');
             }
         })
     }
</script>
<script>
   $('#myModalsettingsUpdate input[name="profit_markup"]').on('change, keyup', function(){
     var sub_total = 100;
     var pm = $(this).val();
     var price = sub_total+(pm*sub_total/100);
     var gross_margin = (price-sub_total)/price*100;
     gross_margin = parseFloat(gross_margin).toFixed(2);
     $('#myModalsettingsUpdate input[name="gross_margin_percent"]').val(gross_margin);
   });
   
   $('#myModalsettingsUpdate input[name="gross_margin_percent"]').on('change, keyup', function(){
     var sub_total = 100;
     var gm = $(this).val();
     var cost = sub_total - gm;
     var pm = (gm/cost)*100;
     pm = parseFloat(pm).toFixed(2);
     $('#myModalsettingsUpdate input[name="profit_markup"]').val(pm);
   });
   $(document).ready(function(){
      $('.nav-justified').find('li').removeClass( "active" )
      $('.nav-justified li a:first').addClass( "active show" )
      
   });
   
</script>
<script type="text/javascript">
   $('.clickDocUp').on('click',function(){
      $('.terms_condition_settingDoc').click()
   })
    $('.clickDocRemove').on('click',function(){
      $('.terms_condition_settingDoc').val('')
      var baseUrl   = window.location.origin;
      $('.subscriberLogoa img').attr('src',baseUrl+'/paintpad/uploads/cross.png')
      $('.clickDocRemove').hide()
   })

   
   $('.clickDocUpp').on('click',function(){
      $('.terms_condition_settingDoc').click()
   })
   $('.terms_condition_settingDoc').on('change',function(){
   		var file =  $('.terms_condition_settingDoc')[0].files[0]
      	var ext = file.name.split('.').pop();
      	// console.log(ext)
         var baseUrl   = window.location.origin;
         $('.clickDocRemove').show()
      	if (ext == 'doc' || ext == 'docx') {

  			$('.subscriberLogoa img').attr('src',baseUrl+'/paintpad/uploads/document.png')
        }
        else{

         
           $('.subscriberLogoa img').attr('src',baseUrl+'/paintpad/uploads/pdf-doc.png')
           
        }

   })
   
</script>