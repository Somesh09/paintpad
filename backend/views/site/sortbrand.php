<?php
use yii\widgets\Pjax;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
?>
<!DOCTYPE html>
<html>
<head>
   
</head>

<style type="text/css">
.sort {
    float: right;
}
.sort img {
    width: 21px;
    cursor: pointer;
}
</style>

<body>
<div class="tbl-brands-form form-inner-head">
    <!-- <button class="pull-right sort btn btn-success">SAVE</button> -->
    <!-- <h3 align="center" style="color:#00cbff">Sort Brand </h3> -->
	<div class="modal-body-div-add sortBrandInner">
	<div style="width: 480px;float: left;">
	<h2 class="">Sort Brands</h2>
    <span align="left" style="color:#000">Hold and drag rows to sort the brand</span>
	</div>
	<?= Html::submitButton('Save', ['class' => 'btn btn-success sortBrand','id'=>'blah22']) ?>
	<button type="button" class="close glyphicon glyphicon-remove" data-dismiss="modal" aria-hidden="true"></button>
	</div>
    <div style="cursor: pointer" class="model-inner-part">
        
        <table class="table table-hover table-condensed table-striped table-bordered">

            <tbody class="row_position">
            <?php
                foreach($model as $model){
            ?>
                <tr  id="<?php echo $model->brand_id ?>">
                    <td><?php echo $model->name ?></td>
                </tr>
            <?php 
                } 
            ?>
            </tbody>

        </table>
    </div> <!-- container / end -->
	</div>
	
	
</body>


<script type="text/javascript">
    $( ".row_position" ).sortable({
        delay: 150,
        stop: function() {
            var selectedData = new Array();
            $('.row_position>tr').each(function() {
                selectedData.push($(this).attr("id"));
            });
            updateOrder(selectedData);
        }
    });


    function updateOrder(data) {
        $.ajax({
            url:"ajaxpro.php",
            type:'post',
            data:{position:data},
            success:function(){
                $("body").removeClass('modal-open').removeAttr("style");

                // $("h1").removeAttr("style"); 
            }
        })
    }
 $(document).on('click', '.sortBrand', function(){ 
    $("#modalSort").modal('hide');
    $(".modal-backdrop").remove(); 
    $.pjax.reload('#my-grid-pjax' , {timeout : false});
});
    


</script>
</html>
