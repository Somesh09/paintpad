<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TblTiers */

//$this->title = 'Create Tbl Tiers';
$this->params['breadcrumbs'][] = ['label' => 'Tbl Tiers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-tiers-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_formtier', [
        'model' => $model,
    ]) ?>

</div>
