<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\bootstrap\Modal;
/* @var $this yii\web\View */
/* @var $searchModel app\models\eventSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Events';
$this->params['breadcrumbs'][] = $this->title;
?>
<div id="wrapper">
    <div class="header">
        <img src="demo/img/pignose-logo.png" alt=""/>
        <h1>PIGNOSE Calendar</h1>
        <a href="https://github.com/KennethanCeyer/pg-calendar" target="_blank" class="ui button black">View
            project on Github</a>
        <a href="https://github.com/KennethanCeyer/pg-calendar/wiki/Documentation" target="_blank"
           class="ui button blue">Documentation</a>
        <a href="https://github.com/KennethanCeyer/pg-calendar/archive/master.zip" target="_blank"
           class="ui button teal">Download</a>
        <h4 class="version">Latest version <strong></strong></h4>
    </div>
    <div id="basic" class="article">
        <div class="title">
            <h3><span>📅 Calendar</span></h3>
            <h4>PIGNOSE Calendar is beautiful and eidetic jQuery date picker plguin</h4>
        </div>
        <div class="calendar"></div>
        <div class="box"></div>
        <ul class="ui top pointing secondary menu">
            <a href="#" role="presentation" class="item active" data-tab="html-basic">HTML</a>
            <a href="#" role="presentation" class="item" data-tab="javascript-basic">Javascript</a>
        </ul>
        <div role="tabpanel" class="ui tab segment active" data-tab="html-basic">
            <pre><code class="language-markup">&lt;div class=&quot;calendar&quot;&gt;&lt;/div&gt;</code></pre>
        </div>
        <div role="tabpanel" class="ui tab segment" data-tab="javascript-basic">
                <pre><code class="language-js">$(function() {
    $('.calendar').pignoseCalendar();
});</code></pre>
        </div>
    </div>
</div>
<div class="event-index">
<?php Pjax::begin(['timeout' => 5000,'id'=>'eventpjax']); ?>
 <?php

       Modal::begin([
          'header'=>"",
          'id'=>'modalevent',
          'size'=>'modal-lg',
        ]);
          echo "<div id='modalEvent'></div>";
          Modal::end();
     ?>
    <h1><?= Html::encode($this->title) ?></h1>
   
    <?php //echo $this->render('/site/event/_searchevent', ['model' => $searchModel]); ?>

    <p>
       <?php 
        echo  Html::button('Create event', ['value'=>Url::to( Url::base().'/event/create'),'class'=>'btn btn-primary pull-right' ,'id'=>'modalEventButton']);?>
    </p>

    <?= \yii2fullcalendar\yii2fullcalendar::widget(array(
      'events'=> $event,
  ));?>
   
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
     $(function()
{
  //
  $('#modalEventButton').click(function(){
    $('#modalevent').modal('show')
      .find('#modalEvent')
      .load ($(this).attr('value'));
  });
});
    </script>
<?php Pjax::end();?>