<?php 
use yii\helpers\Url;
?>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1" />
 <link href="<?php echo Url::base(); ?>/css/styleBuy.css" type="text/css"  rel="stylesheet" >
<!-- Latest compiled and minified CSS -->
 

 <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600&display=swap" rel="stylesheet"> 
 
  
  <!-- <script src="https://js.stripe.com/v3/"></script> -->
</head>
<!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myPackage">
  Subscribe
</button> -->
<div class="container pakcage-unique">

<!-- <div class="modal" id="myPackage"> -->

     <div class="modal-content">
       <!-- Modal Header -->
       <div class="login-overlay">
         <div class="payment-main">
            <div class="container-fluid">
            
              <div class="Package-main">
                <h1>Choose The Best Package for Your Business</h1>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
              </div><!-- Package-main -->
              <div class="package-report">
                <div class="report-box">
                  <div class="report-top">
                    <div class="report-top-img"><img src="<?php echo Url::base(); ?>/assets/images/report-bg.png"></div>
                    <h2>Freelance</h2>
                    <div class="price"><span>$</span>59 <small>/month</small></div>
                  </div><!-- report-top -->
                  <div class="report-botm">
                    <h3>10 Reports</h3>
                    <p>You can by additional reports<br /> 1 Report/1$</p>
                    <ul class="report-list">
                      <li><span class="report-img"><img src="<?php echo Url::base(); ?>/assets/images/check.png"></span>Lorem ipsum dolor sit amet</li>
                      <li><span class="report-img"><img src="<?php echo Url::base(); ?>/assets/images/check.png"></span>Consectetur adipiscing elit</li>
                      <li><span class="report-img"><img src="<?php echo Url::base(); ?>/assets/images/cross.png"></span>Lorem ipsum dolor sit amet</li>
                      <li><span class="report-img"><img src="<?php echo Url::base(); ?>/assets/images/cross.png"></span>Consectetur adipiscing elit</li>
                    </ul><!-- report-list -->
                  </div>
                  <button type="button" class="btn select-package" value="plan_H0fMB2JgH4hAUH"  >Select Package</button>
                </div><!-- report-box -->
                <div class="report-box">
                  <div class="report-top">
                    <div class="report-top-img"><img src="<?php echo Url::base(); ?>/assets/images/report-bg.png"></div>
                    <h2>BUSINESS</h2>
                    <div class="price"><span>$</span>79 <small>/month</small></div>
                  </div><!-- report-top -->
                  <div class="report-botm">
                    <h3>25 Reports</h3>
                    <p>You can by additional reports<br />1 Report/1$</p>
                    <ul class="report-list">
                      <li><span class="report-img"><img src="<?php echo Url::base(); ?>/assets/images/check.png"></span>Lorem ipsum dolor sit amet</li>
                      <li><span class="report-img"><img src="<?php echo Url::base(); ?>/assets/images/check.png"></span>Consectetur adipiscing elit</li>
                      <li><span class="report-img"><img src="<?php echo Url::base(); ?>/assets/images/cross.png"></span>Lorem ipsum dolor sit amet</li>
                      <li><span class="report-img"><img src="<?php echo Url::base(); ?>/assets/images/cross.png"></span>Consectetur adipiscing elit</li>
                    </ul><!-- report-list -->
                  </div>
                  <button type="button" class="btn select-package" value="plan_H0fMSoCgjEBX31"  >Select Package</button>
                </div><!-- report-box -->
                <div class="report-box">
                  <div class="report-top">
                    <div class="report-top-img"><img src="<?php echo Url::base(); ?>/assets/images/report-bg.png"></div>
                    <h2>ENTERPRICE</h2>
                    <div class="price"><span>$</span>99 <small>/month</small></div>
                  </div><!-- report-top -->
                  <div class="report-botm">
                    <h3>50 Reports</h3>
                    <p>You can by additional reports<br />1 Report/1$</p>
                    <ul class="report-list">
                      <li><span class="report-img"><img src="<?php echo Url::base(); ?>/assets/images/check.png"></span>Lorem ipsum dolor sit amet</li>
                      <li><span class="report-img"><img src="<?php echo Url::base(); ?>/assets/images/check.png"></span>Consectetur adipiscing elit</li>
                      <li><span class="report-img"><img src="<?php echo Url::base(); ?>/assets/images/cross.png"></span>Lorem ipsum dolor sit amet</li>
                      <li><span class="report-img"><img src="<?php echo Url::base(); ?>/assets/images/cross.png"></span>Consectetur adipiscing elit</li>
                    </ul><!-- report-list -->
                  </div>
                  <button type="button" class="btn select-package" value="plan_H0fL6pbm1CsnK3" >Select Package</button>
                </div><!-- report-box -->
              </div><!-- package-report -->
              
            </div>
          </div><!-- payment-main -->
        
         <!-- login-footer -->
      </div>
 
     </div>

 <!-- </div> -->
</div>

 <!--- PAYEMENT POPUP -->
 <!-- The PAYEMENT POPUP Modal -->
<div class="modal" id="myPayment">
   <div class="modal-dialog">
     <div class="modal-content">
 
       <!-- Modal Header -->
       <form id="paymentForm">
          <div class="login-overlay">
            <div class="payment-outer-div">
            <div class="payment-main">
              <div class="container-fluid">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <div class="row">
                  <div class="col-md-4">
                    <div class="payment-method">
                      <h3 class="pay-heading">Payment Method</h3>
                      <div class="pay-form">
                        <div class="form-group images">
                          <span class="card-icon"><img src="<?php echo Url::base(); ?>/assets/images/card-icon.png"></span>
                          <!-- <p><img src="assets/images/paypal.png"></p> -->
                        </div>
                        <div class="form-group">
                          <label>CC Number</label>
                          <input type="number" class="input-control pay-input rreeqq" placeholder="4568 5658 6549 8569" name="card_number" id="card_number" required>
                        </div>
                        <div class="form-group">
                          <label>Expiration</label>
                          <input type="text" class="input-control pay-input" id="expiration"placeholder="12/22" name="expiration" required readonly="readonly" >
                          <input type="hidden" class="input-control pay-input" id="month"placeholder="12/22" name="month"  >
                          <input type="hidden" class="input-control pay-input" id="year"placeholder="12/22" name="year"  >

                        </div>
                        <div class="form-group">
                          <label>CVV Code</label>
                          <input type="number" class="input-control pay-input rreeqq" placeholder="***" name="cvv" id="cvv" required>
                        </div>
                      </div><!-- Pay-form -->
                    </div><!-- payment-method -->
                  </div>
                  <div class="col-md-4">
                    <div class="payment-method">
                      <h3 class="pay-heading">Billing Info</h3>
                      <div class="pay-form">
                        <div class="form-group">
                          <label>First name</label>
                          <input type="text" class="input-control pay-input rreeqq" placeholder="Vladislav" name="Fname" id="fname">
                        </div>
                        <input type="hidden" class="input-control pay-input rreeqq" placeholder="Vladislav" name="plan_id_p" id="plan_id_p">
                        <div class="form-group">
                          <label>Last Name</label>
                          <input type="text" class="input-control pay-input rreeqq" placeholder="Valerievich" name="Lname" id="lname" required>
                        </div>
                        <div class="form-group">
                          <label>Billing Address</label>
                          <input type="text" class="input-control pay-input rreeqq" placeholder="Wall Street 125, New York" name="address"id="address" required>
                        </div>
                        <div class="form-group">
                          <label>City</label>
                          <input type="text" class="input-control pay-input rreeqq" placeholder="New York" name="city" id="city" required>
                        </div>
                        <div class="form-group">
                          <label>ZIP Code</label>
                          <input type="number" class="input-control pay-input rreeqq" placeholder="100001" name="zipcode" id="zip" required>
                        </div>
                      </div><!-- Pay-form -->
                    </div><!-- payment-method -->
                  </div>
                  <div class="col-md-4">
                    <div class="payment-method transparent">
                      <h3 class="pay-heading">Security Info</h3>
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                      <button type="submit" value="Save" class="btn sve-btn"  >Save</button>
                    </div><!-- payment-method -->
                  </div>
                </div>
              </div>
            </div><!-- payment-main -->
            
            </div><!--payment-outer-div -->
          </div><!-- login-overlay -->
      </form>
     </div>
   </div>
 </div>
 <script>
   var plan_id = ''
   $( function() {
      $( "#expiration" ).datepicker({
        minDate: new Date(),
        dateFormat: 'mm/yy',
        onSelect: function(date) {

            date = date.split(/\//g);
            var month = date[0];
            var year = date[1];

            $('#month').val(month);
            $('#year').val(year);

        }  
      });
    } );
    $('.select-package').on('click',function(){
      plan_id = $(this).val()
      $('#myPackage').modal('hide')
      $('#myPayment').modal('show')
      $('#plan_id_p').val(plan_id)
    })
    $(document).on('submit','#paymentForm',function(event){
       event.preventDefault();
       $.ajax({
           url:'<?php echo Url::base(); ?>/paymentAjax.php',
           method:"POST",
           data:new FormData(this),
           dataType:'JSON',
           contentType: false,
           cache: false,
           processData: false,
           success:function(data)
           {
               console.log(data)
               alert(data.message)
               if (data.success == '1') {
                $('#myPayment').modal('hide')
               }
               
           }
       })
   });  
 </script>
