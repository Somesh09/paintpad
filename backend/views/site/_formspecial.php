<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\TblSpecialItems */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-special-items-form form-inner-head">

    <?php $form = ActiveForm::begin(['validationUrl' => Url::toRoute(['validatespecial']),'enableAjaxValidation' => true, 'id' => 'myid11']); ?>
    <div class = "modal-body-div-add">
     <h2>Create Special Items</h2>
          <?= Html::submitButton('Save', ['class' => 'btn btn-success','id'=>'closespecial']) ?>
      <button type="button" class="close glyphicon glyphicon-remove" data-dismiss="modal" aria-hidden="true">×</button>
    </div>
    <?= $form->field($model, 'name')->textarea(['rows' => '6']) ?>
    <!-- <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?> -->
    
    <?= $form->field($model, 'price', ['template' => '<strong>Price (Exc. GST)</strong><div class="input-group">{input}<span class="input-group-addon">$</span></div>{error}{hint}'])->textInput(); ?>

  

    <?php ActiveForm::end(); ?>

</div>
<script>
$('body').on('beforeSubmit', '#myid11', function (e) {
    var pathname = window.location.href;    
     var form = $(this);
    
     $.ajax({
          url: form.attr('action'),
          type: 'post',
          data: form.serialize(),
          success: function (response) {
              

               //console.log(response);              

               if(response == 1){
                
                        $("#modalspecial").modal('hide');
                        $(document.body).removeClass('modal-open');
         $('.modal-backdrop').remove();
                        $.pjax.reload('#specialpjax' , {timeout : false});


               }
          },
          
     });
       e.stopImmediatePropagation();
     return false;
});













</script>
