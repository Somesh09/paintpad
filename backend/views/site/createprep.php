<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\tblPrepLevel */

//$this->title = 'Create  Prep Level';

?>
<div class="tbl-prep-level-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_formprep', [
        'model' => $model,
    ]) ?>

</div>
