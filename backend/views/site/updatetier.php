<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TblTiers */

$this->title = 'Update Tbl Tiers: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Tbl Tiers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->tier_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tbl-tiers-update">

  

    <?= $this->render('_formupdatetier', [
        'model' => $model,
        'id'=>$id,
        'model1'=>$model1,
    ]) ?>

</div>
