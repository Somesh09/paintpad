<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TblColors */


?>
<div class="tbl-colors-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_formcolorsmain', [
        'model' => $model,
    ]) ?>

</div>
