<?php
use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\bootstrap\ButtonDropdown;
use dosamigos\grid\GridView;
use dosamigos\exportable\ExportableButton; 
use kartik\export\ExportMenu;
?>


<div class="tbl-brand-index">

<?php Pjax::begin(['timeout' => 5000,'id'=>'my-subscriber-pjax']);?>   


  <?php  echo Html::button('<img src="'.Url::base().'/uploads/add.png"width="17" height="17"/>', ['value'=>Url::to( Url::base().'/subscriber/create'), 'class' => 'btn btn-primary pull-right','id'=>'modalButton']);?>
<?php
  Modal::begin([
    'header'=>'',
    'id'=>'modalSubscriber',
    //'class'=>'main-popup-all',
    'size'=>'modal-lg',
  ]);
  echo "<div id='modalContentSubscriber' class='clearfix'>
  <div style='text-align:center'>
   <img src='".Url::base()."/uploads/avatar2.gif' height='100' width='100'>
  </div></div>";
  Modal::end();
?>

<?php
  Modal::begin([
    'header'=>'',
    'id'=>'modalSubscriberCreate',
    //'class'=>'main-popup-all',
    'size'=>'modal-lg',
  ]);
  echo "<div id='modalContentSubscriberCreate' class='clearfix'>
  <div style='text-align:center'>
   <img src='".Url::base()."/uploads/avatar2.gif' height='100' width='100'>
  </div></div>";
  Modal::end();
?>

    
<div id="SubscriberSearch" class="main-table-index-page">
<?=  GridView::widget([
    'dataProvider' => $dataProvider,
    'rowOptions'   => function ($model, $key, $index, $grid) {
                      return  [
                      "class"=>"modalButton44",'value'=>Url::base().'/subscriber/update?id='.$model->company_id
                      ];
    },
    'behaviors' => [
        [
        'class' => '\dosamigos\grid\behaviors\LoadingBehavior',
        'type' => 'spinner'
        ]
    ],
    'columns' => [                              
      
        'name',
        'address',
        'phone',
        'email',
        'website',
      ['class' => 'yii\grid\ActionColumn',
        'header' => 'UPDATE',
        'headerOptions' => ['style' => 'color:#337ab7'],
        'template' => '{update}',
        'buttons' => [
            'update' => function ($url, $model,$id) {
                                return Html::a('<span class=""><img src="'.Url::base().'/uploads/update.png" width="13" height="13"/></span>', $url, [
                                    "class"=>"modalButton4",'value'=>Url::base().'/subscriber/update?id='.$model->company_id
                                ]);
                          },
        ],
        'urlCreator' => function ($action, $model, $key, $index) {
             Url::to(['update?id='.$model->company_id],['id'=>'modal'])   ;         
        }
      ],
    ],
]); 
?>
</div>

</div>

<script >
//


//$('#modalSubscriber .modal-header').remove();



// $('#modalButton').click(function(){
//   .find('#modalContentSubscriberCreate')
//   .load ($(this).attr('value'));
//   $('#modalSubscriberCreate').modal('show')

// })


// $('.modalButton4').click(function(){
//   .find('#modalContentSubscriber')
//   .load ($(this).attr('value'));
//   $('#modalSubscriber').modal('show')
// })


// $('.modalButton44').click(function(){
//   .find('#modalContentSubscriber')
//   .load ($(this).attr('value'));
//   $('#modalSubscriber').modal('show')

// })

$('#modalButton').click(function(){
  alert('rk');
   $.ajax({
    url:  $(this).attr('value'),
    type: "POST",
    data: data, // Send the object.
      success: function(response) {
       // $('#modalContentSubscriberCreate').html(response)
      //  $('#modalSubscriberCreate').modal('show')

      }
    });
})


$(".modal").on("hidden.bs.modal", function(){
    $("#modalContentSubscriber").html("");
    $('#modalSubscriberCreate').html("")
});


$('.modalButton4').click(function(){
    $.ajax({
    url:  $(this).attr('value'),
    type: "POST",
    data: data, // Send the object.
      success: function(response) {
        //$('#modalContentSubscriber').html(response)
        //$('#modalSubscriber').modal('show')

      }
    });
})


$('.modalButton44').click(function(){
   $.ajax({
    url:  $(this).attr('value'),
    type: "POST",
    data: data, // Send the object.
      success: function(response) {
        //$('#modalContentSubscriber').html(response)
       // $('#modalSubscriber').modal('show')

      }
    });

})

</script>
<?php Pjax::end();?>
   


  
  
  

