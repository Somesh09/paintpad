
<div class="mocha isFocused" id="win1" style=" width: 1215px; height: 801px; display: block; opacity: 1; z-index: 121; top: 62px; left: 192px;">
  <div id="win1_overlay" class="mochaOverlay" style="position: absolute; top: 0px; left: 0px; width: 1536px; height: 801px;">
    <div id="win1_titleBar" class="mochaTitlebar" style="cursor: move; width: 1536px; height: 36px;">
      <h3 id="win1_title" class="mochaTitle">Subscriber: <span>Paint Professionals</span></h3>
    </div>
    <div id="win1_contentBorder" class="mochaContentBorder">
      <div id="win1_contentWrapper" class="mochaContentWrapper" style="width: 1536px; height: 740px; overflow: auto;">
        <div id="win1_content" class="mochaContent" style="padding: 10px 12px 0px; display: block;">
          <style>
/*! CSS Used from: http://enacteservices.com/paintpad_dev/admin/assets/3cbc91e4/css/bootstrap.css */
body{margin:0;}
canvas{display:inline-block;vertical-align:baseline;}
a{background-color:transparent;}
a:active,a:hover{outline:0;}
b,strong{font-weight:bold;}
img{border:0;}
input,select{margin:0;font:inherit;color:inherit;}
select{text-transform:none;}
html input[type="button"],input[type="submit"]{-webkit-appearance:button;cursor:pointer;}
input::-moz-focus-inner{padding:0;border:0;}
input{line-height:normal;}
table{border-spacing:0;border-collapse:collapse;}
td,th{padding:0;}
@media print{
*,*:before,*:after{color:#000!important;text-shadow:none!important;background:transparent!important;-webkit-box-shadow:none!important;box-shadow:none!important;}
a,a:visited{text-decoration:underline;}
a[href]:after{content:" (" attr(href) ")";}
a[href^="#"]:after{content:"";}
thead{display:table-header-group;}
tr,img{page-break-inside:avoid;}
img{max-width:100%!important;}
p,h3{orphans:3;widows:3;}
h3{page-break-after:avoid;}
.table{border-collapse:collapse!important;}
.table td,.table th{background-color:#fff!important;}
.table-bordered th,.table-bordered td{border:1px solid #ddd!important;}
}
*{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;}
*:before,*:after{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;}
body{font-family:"Helvetica Neue", Helvetica, Arial, sans-serif;font-size:14px;line-height:1.42857143;color:#333;background-color:#fff;}
input,select{font-family:inherit;font-size:inherit;line-height:inherit;}
a{color:#337ab7;text-decoration:none;}
a:hover,a:focus{color:#23527c;text-decoration:underline;}
a:focus{outline:5px auto -webkit-focus-ring-color;outline-offset:-2px;}
img{vertical-align:middle;}
h3{font-family:inherit;font-weight:500;line-height:1.1;color:inherit;}
h3{margin-top:20px;margin-bottom:10px;}
h3{font-size:24px;}
p{margin:0 0 10px;}
ul{margin-top:0;margin-bottom:10px;}
.col-md-6{position:relative;min-height:1px;padding-right:15px;padding-left:15px;}
@media (min-width: 992px){
.col-md-6{float:left;}
.col-md-6{width:50%;}
}
table{background-color:transparent;}
th{text-align:left;}
.table{width:100%;max-width:100%;margin-bottom:20px;}
.table > thead > tr > th,.table > tbody > tr > td{padding:8px;line-height:1.42857143;vertical-align:top;border-top:1px solid #ddd;}
.table > thead > tr > th{vertical-align:bottom;border-bottom:2px solid #ddd;}
.table > colgroup + thead > tr:first-child > th,.table > thead:first-child > tr:first-child > th{border-top:0;}
.table-condensed > thead > tr > th,.table-condensed > tbody > tr > td{padding:5px;}
.table-bordered{border:1px solid #ddd;}
.table-bordered > thead > tr > th,.table-bordered > tbody > tr > td{border:1px solid #ddd;}
.table-bordered > thead > tr > th{border-bottom-width:2px;}
.table-striped > tbody > tr:nth-of-type(odd){background-color:#f9f9f9;}
.table > tbody > tr > td.active{background-color:#f5f5f5;}
input[type="file"]{display:block;}
select[size]{height:auto;}
input[type="file"]:focus{outline:5px auto -webkit-focus-ring-color;outline-offset:-2px;}
.btn{display:inline-block;padding:6px 12px;margin-bottom:0;font-size:14px;font-weight:normal;line-height:1.42857143;text-align:center;white-space:nowrap;vertical-align:middle;-ms-touch-action:manipulation;touch-action:manipulation;cursor:pointer;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;background-image:none;border:1px solid transparent;border-radius:4px;}
.btn:focus,.btn:active:focus{outline:5px auto -webkit-focus-ring-color;outline-offset:-2px;}
.btn:hover,.btn:focus{color:#333;text-decoration:none;}
.btn:active{background-image:none;outline:0;-webkit-box-shadow:inset 0 3px 5px rgba(0, 0, 0, .125);box-shadow:inset 0 3px 5px rgba(0, 0, 0, .125);}
.btn-primary{color:#fff;background-color:#337ab7;border-color:#2e6da4;}
.btn-primary:focus{color:#fff;background-color:#286090;border-color:#122b40;}
.btn-primary:hover{color:#fff;background-color:#286090;border-color:#204d74;}
.btn-primary:active{color:#fff;background-color:#286090;border-color:#204d74;}
.btn-primary:active:hover,.btn-primary:active:focus{color:#fff;background-color:#204d74;border-color:#122b40;}
.btn-primary:active{background-image:none;}
.btn-success{color:#fff;background-color:#5cb85c;border-color:#4cae4c;}
.btn-success:focus{color:#fff;background-color:#449d44;border-color:#255625;}
.btn-success:hover{color:#fff;background-color:#449d44;border-color:#398439;}
.btn-success:active{color:#fff;background-color:#449d44;border-color:#398439;}
.btn-success:active:hover,.btn-success:active:focus{color:#fff;background-color:#398439;border-color:#255625;}
.btn-success:active{background-image:none;}
.btn-danger{color:#fff;background-color:#d9534f;border-color:#d43f3a;}
.btn-danger:focus{color:#fff;background-color:#c9302c;border-color:#761c19;}
.btn-danger:hover{color:#fff;background-color:#c9302c;border-color:#ac2925;}
.btn-danger:active{color:#fff;background-color:#c9302c;border-color:#ac2925;}
.btn-danger:active:hover,.btn-danger:active:focus{color:#fff;background-color:#ac2925;border-color:#761c19;}
.btn-danger:active{background-image:none;}
.btn-group{position:relative;display:inline-block;vertical-align:middle;}
.btn-group > .btn{position:relative;float:left;}
.btn-group > .btn:hover,.btn-group > .btn:focus,.btn-group > .btn:active{z-index:2;}
.btn-group .btn + .btn{margin-left:-1px;}
.btn-group > .btn:first-child{margin-left:0;}
.btn-group > .btn:first-child:not(:last-child):not(.dropdown-toggle){border-top-right-radius:0;border-bottom-right-radius:0;}
.btn-group > .btn:last-child:not(:first-child){border-top-left-radius:0;border-bottom-left-radius:0;}
.pagination{display:inline-block;padding-left:0;margin:20px 0;border-radius:4px;}
.close{float:right;font-size:21px;font-weight:bold;line-height:1;color:#000;text-shadow:0 1px 0 #fff;filter:alpha(opacity=20);opacity:.2;}
.close:hover,.close:focus{color:#000;text-decoration:none;cursor:pointer;filter:alpha(opacity=50);opacity:.5;}
.clearfix:before,.clearfix:after{display:table;content:" ";}
.clearfix:after{clear:both;}
.pull-right{float:right!important;}
.pull-left{float:left!important;}
/*! CSS Used from: Embedded */
[id^='agent_panel_wrapper_'].quote_window .scroll-container .heading-container:first-child{margin:0;width:100%;background:#ebebeb;box-shadow:none;border:none;border-radius:0;min-height:40px;padding:0px!important;flex-direction:row-reverse;flex-wrap:wrap;margin-bottom:0;}
[id^='agent_panel_wrapper_'] .scroll-container{width:unset!important;}
.quote_window .scroll-container .heading-container h3{margin:18px 33px;}
.scroll-container.header.clearfix>*:nth-child(even):not(.editing){padding-left:1em;box-sizing:border-box;}
[id^='agent_panel_wrapper_'] ul.no-list{font-size:18px;}
[id^='agent_panel_wrapper_'] li{line-height:28px;}
.agent_doc_area .windowInfo{display:block!important;margin-left:-25px!important;margin-bottom:10px;}
/*body{background:#fff url(file:///C:/Users/enAct-W007D/img/pat.jpg) 0 0 repeat-x;overflow:hidden;height:auto;}*/
*::selection{background-color:rgba(0,0,0,0.15)!important;}
@media (min-width: 768px) and (max-width: 980px){
body{padding:0;}
}
@media (max-width: 768px){
body{padding:0;}
}
h3 span{font-weight:normal;margin:0 5px;opacity:0.5;}
input,select{border:1px solid #bbb;}
a,.table tr{cursor:pointer;}
@media only screen and (-webkit-min-device-pixel-ratio: 1.3), 	only screen and (-o-min-device-pixel-ratio: 13/10), 	only screen and (min-resolution: 120dppx){
[class*=" icon-"]{background-image:url(file:///C:/Users/enAct-W007D/img/glyphicons-halflings@2x.png);background-size:469px;}
}
[id$='_table_display_area']{background:white;}
.tblwrapper{padding-top:0;overflow-y:scroll;overflow-y:auto;}
.tblwrapper{transform:translateZ(0);}
@media only screen and (max-height:599px){
.tblwrapper{min-height:130px;}
}
@media only screen and (max-height:768px) and (max-width:768px){
body{overflow:auto;}
}
@media screen and (orientation: portrait) and (max-width:576px){
.table-condensed thead{display:none!important;}
table.table-condensed,.table-condensed tbody{display:flex;flex-direction:column;width:100%;}
.table-condensed tr{display:flex!important;flex-direction:row;flex-wrap:wrap;width:100%;padding-bottom:2ex;box-shadow:inset 0 -18px 15px -15px rgba(0,0,0,0.5);margin-bottom:2ex;}
.table-condensed td+td{padding-left:3ex;}
.table-condensed td a{flex:0 0 auto;width:100%;}
.table.table-striped tr:hover>td,.table.table-striped tr:hover>td:hover{box-shadow:none!important;}
}
.windowInfo{background:rgb(244,244,244);background:linear-gradient(to bottom, rgba(244,244,244,1) 0%,rgba(232,232,232,1) 86%);padding:11px;border:1px solid #ccc;-webkit-border-radius:4px;-moz-border-radius:4px;border-radius:4px;margin:0 0 18px 0;box-shadow:0 1px 6px rgba(0,0,0,0.08), inset 0 1px 1px #fff;}
.windowInfo form{margin:0;}
@media only screen and (min-width: 1400px ){
.windowInfo div.pull-left{display:block;float:none;}
}
@media only screen and (max-width: 1399px ){
.windowInfo div.pull-left{display:inline;float:none;}
}
.input-medium{width:auto;max-width:150px;min-width:70px;}
.spinner{height:auto!important;right:0!important;left:auto!important;}
.windowInfo.clearfix:hover .spinner,.windowInfo.clearfix:focus .spinner{top:0px!important;position:relative!important;height:auto!important;z-index:99999;right:0!important;left:auto!important;}
.windowInfo.clearfix:hover .spinner-content,.windowInfo.clearfix:focus .spinner-content{right:0px;left:auto!important;bottom:2px!important;top:auto!important;}
.windowInfo.clearfix:not(:hover):not(:focus) .spinner-content{top:-4px!important;}
.spinner-content{background:white;top:0!important;padding:6px 11px;border:1px solid #ccc;-webkit-border-radius:4px;-moz-border-radius:4px;border-radius:4px;box-shadow:0 1px 6px rgba(0,0,0,0.08), inset 0 1px 1px #fff;}
.spinner-content p{float:right;margin:2px 0 0;}
.spinner-img{position:relative;height:24px;background:#FFFFFD url(file:///C:/Users/enAct-W007D/images/spinner.gif) center center no-repeat;padding:0;float:left;margin:-1px 8px -1px -3px;width:24px;-webkit-animation:spin-rotate 4s 0s infinite reverse linear;animation:spin-rotate 4s 0s infinite reverse linear;border-radius:50%;}
div[id*=table_wrapper] .windowInfo,.windowInfo.clearfix{padding:6px 11px 0px 11px;}
.form-inline .windowInfo input,.form-inline .windowInfo .btn,.windowInfo .form-inline input,.windowInfo .form-inline .btn,.windowInfo [class*='pull-'] .btn,.windowInfo.clearfix .btn,.windowInfo.clearfix .btn-group{margin-bottom:6px;}
.windowInfo.clearfix .btn-group .btn{margin-bottom:0;}
span[class*='pull-']{margin-bottom:0!important;}
.table{background:#fff;}
.floatThead-wrapper{overflow-x:hidden;}
.floatThead-wrapper .table{margin-bottom:-1px;}
.mocha .floatThead-wrapper .table{margin-bottom:0;}
.pagination a{min-width:inherit!important;align-self:center;flex:1 1 100%;min-height:100%;display:flex;align-items:center;justify-content:center;border:0;padding:0;}
.pagination{min-height:36px;text-align:center;margin:0 auto;width:calc(100% - 4px);height:initial!important;}
.pagination select#limit{margin-top:3px;margin-bottom:3px;}
.floatThead-wrapper + div.pagination{margin:0px auto 4px;max-width:1280px;padding:0 0px;}
div.pagination ul{margin:0px auto;display:flex;flex-flow:row wrap;box-shadow:none;padding-left:0;}
.pagination li{display:flex;justify-content:center;border:1px solid #ddd;flex:1;transition:all 0.5s ease 0s;min-width:40px;max-width:8em;}
.pagination li + li{border-left:none;}
.pagination li:not(.active):hover{transition:all 0.5s ease 0s;min-width:3em;}
.pagination a.limit{line-height:120%;align-self:center;padding:0 3px;}
.pagination li:first-child{min-width:64px!important;border-bottom-left-radius:4px;}
.pagination li:nth-last-child(4):not(.active){min-width:2em;flex:2;}
.pagination li:nth-last-child(3):not(.active),.pagination li:nth-child(2):not(.active){min-width:1.5em;flex:1.5;}
@media screen and (max-width: 1279px){
.pagination li.disabled:last-of-type{min-width:8em;border-radius:0 4px 4px 0;}
}
@media screen and (min-width: 1280px){
.pagination li.disabled:last-of-type{min-width:10em;border-radius:0 4px 4px 0;}
}
.limit select{width:56px;}
th img{cursor:pointer;}
.mocha{-webkit-border-radius:7px;-moz-border-radius:7px;min-height:46px;}
.mocha{-webkit-box-shadow:0 3px 15px rgba(0, 0, 0, 0.3);-moz-box-shadow:0 3px 15px rgba(0, 0, 0, 0.3);box-shadow:0 3px 15px rgba(0, 0, 0, 0.3);}
.mocha:not(.mysys_design){-webkit-transform:translate3d(0,0,0);-moz-transform:translate3d(0,0,0);-ms-transform:translate3d(0,0,0);-o-transform:translate3d(0,0,0);transform:translate3d(0,0,0);}
@media screen and (min--moz-device-pixel-ratio:0){
.mocha:not(.mysys_design){transform:none!important;}
}
.isFocused{-webkit-box-shadow:0 5px 25px rgba(0, 0, 0, 0.4);-moz-box-shadow:0 4px 25px rgba(0, 0, 0, 0.4);box-shadow:0 4px 25px rgba(0, 0, 0, 0.4);}
.mochaCanvasControls{display:none;}
.mochaOverlay{position:fixed;overflow:hidden;background-color:#222;-webkit-border-radius:6px;-moz-border-radius:6px;border-radius:6px;min-height:57px;border-top:1px solid #777;}
.mochaTitlebar{min-height:54px;max-height:54px;transition:max-height 0.3s ease 0.1s;height:auto!important;overflow:hidden;position:absolute;}
.mochaTitlebar{padding:10px;background-color:#222;box-sizing:border-box;border-radius:7px 7px 0 0;}
.mochaContentBorder{margin-top:54px;}
@media screen and (max-height:599px){
.mochaContentBorder{margin-top:29px;}
}
.mochaTitlebar:hover{top:0px;z-index:3;border-radius:7px 7px 0 0;}
.mochaTitlebar h3{line-height:34px;color:#fff;margin-left:5px;transition:white-space 0 0.3s;text-overflow:ellipsis;overflow:hidden;white-space:nowrap;width:calc(100% - 143px);min-height:25px;margin-top:0;}
.mochaTitlebar:hover h3{white-space:normal;}
.mochaContentBorder{background:#fff;border-radius:1px 1px 2px 2px;overflow-y:auto;overflow-x:hidden;}
.mocha .handle{position:absolute;background:#0f0;width:12px;height:12px;z-index:2;overflow:hidden;font-size:1px;opacity:0;-moz-opacity:0;-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";filter:progid:DXImageTransform.Microsoft.Alpha(Opacity=0);}
.mocha .corner{width:17px;height:17px;background:#f00;z-index:3;}
.mocha .cornerSE{bottom:-5px!important;right:-5px!important;height:24px;}
.mocha .cornerSW{bottom:-5px!important;left:-5px!important;}
.mocha .cornerNE{top:-5px!important;right:-5px!important;}
.mocha .cornerNW{top:-5px!important;left:-5px!important;}
.mocha .handleN{top:0px!important;height:8px;}
.mocha .handleS{bottom:-6px!important;width:99%!important;}
.mocha .handleE{right:-6px!important;}
.mocha .handleW{left:-6px!important;}
.mochaControls{position:absolute;top:15px!important;right:15px!important;height:18px;z-index:4;}
.mochaMinimizeButton,.mochaMaximizeButton,.mochaCloseButton{float:right;width:18px;height:18px;font-size:1px;cursor:pointer;z-index:4;display:block;}
.mochaMinimizeButton:hover,.mochaMaximizeButton:hover,.mochaCloseButton:hover{opacity:1;}
@media screen and (max-height:599px){
.mochaTitlebar{padding:2px;max-height:29px;min-height:29px;}
.mochaControls{top:0px!important;height:29px;}
.mochaControls > *{height:100%!important;box-sizing:border-box;background-repeat:no-repeat;margin-top:0!important;}
.mochaWindowButton{height:100%;}
.mochaTitlebar h3{overflow:hidden;}
.mochaTitlebar h3 span{white-space:nowrap;font-size:16px!important;min-height:25px;display:flex;flex-direction:row;justify-content:space-between;align-items:center;}
.mochaTitlebar h3:only-child{min-height:initial!important;line-height:25px!important;font-size:16px;}
.mocha .mochaContentBorder{height:calc(100% - 29px - 7px);}
.mocha .mochaContentWrapper{height:100%!important;}
.mochaOverlay,.mocha{min-height:30px;}
div[id*=table_wrapper] .windowInfo,.windowInfo.clearfix{padding:7px 7px 2px 7px;}
}
@media only screen and (-webkit-min-device-pixel-ratio: 1.3), 			only screen and (-o-min-device-pixel-ratio: 13/10), 			only screen and (min-resolution: 120dppx){
.mocha>.mochaControls > *{height:100%!important;display:inline-block;float:right;margin-top:0!important;}
@media only screen and (max-height:599px){
.mocha>.mochaControls{height:14.5px;}
}
}
.mocha .mochaMinimizeButton,.mocha .mochaMaximizeButton,.mocha .mochaCloseButton{background-image:none;background-color:rgba(0,0,0,0.1);opacity:0.6;transition:opacity 100ms, background-color 100ms;width:18px;height:18px;line-height:14px;margin-left:0;width:50px;text-align:center;}
.mocha .mochaCloseButton{padding-right:6px;}
.mocha .mochaMinimizeButton:hover,.mocha .mochaMaximizeButton:hover,.mocha .mochaCloseButton:hover{opacity:0.8;background-color:rgba(0,0,0,0.22);transition:opacity 150ms, background-color 150ms;}
.mocha .mochaMaximizeButton{margin:auto 1px;}
.mocha .mochaControls{zoom:1!important;top:7px!important;right:0px!important;width:128px!important;width:273px!important;top:0!important;height:54px;}
.mocha .mochaControls > *{height:100%!important;float:right;margin-top:0!important;box-sizing:border-box;}
.mocha .mochaMinimizeButton::after,.mocha .mochaMaximizeButton::after,.mocha .mochaCloseButton::after{font-family:'Material Icons';background:none!important;font-size:18px;color:white;display:inline-block;line-height:54px;}
@media screen and (max-height:599px){
.mocha .mochaMinimizeButton,.mocha .mochaMaximizeButton,.mocha .mochaCloseButton{width:36px;}
.mocha .mochaMinimizeButton::after,.mocha .mochaMaximizeButton::after,.mocha .mochaCloseButton::after{line-height:30px;font-size:22px!important;}
.mocha .mochaControls{height:30px;width:203px!important;}
.mocha .mochaContentBorder{margin-top:29px;height:calc(100% - 29px - 7px)!important;}
}
.mocha .mochaMinimizeButton::after{content:'expand';font-size:12px;}
.mocha .mochaMaximizeButton::after{content:'aspect';font-size:12px;}
.mocha .mochaCloseButton::after{content:'close';font-size:12px;margin-left:0.4ex;}
.mochaSpinner{display:none;position:absolute;width:24px;height:24px;left:0px!important;bottom:6px!important;background:#FFFFFD url(file:///C:/Users/enAct-W007D/images/spinner.gif) center center no-repeat;padding:5px;-webkit-animation:spin-rotate 4s 0s infinite reverse linear;animation:spin-rotate 4s 0s infinite reverse linear;border-radius:50%;}
.mocha .pad{padding:10px;box-sizing:border-box;}
.windowInfo form{overflow:initial;}
.mochaContent .floatThead-wrapper{height:100%;box-sizing:border-box;}
.mochaContent .tblwrapper{max-height:100%;min-height:4em;overflow:auto;}
.mochaContent .windowInfo + div{overflow:auto;transform:translate3d(0,0,0);}
.mocha{transition:margin-top 0.35s cubic-bezier(0.86, 0, 0.07, 1);-webkit-transition:margin-top 0.35s cubic-bezier(0.86, 0, 0.07, 1);}
.mochaOverlay,.mochaTitlebar,.mochaContentWrapper{width:100%!important;}
.mochaContentWrapper{overflow:hidden!important;}
.mochaContent{overflow:auto;}
.mochaContent form{margin-bottom:0;padding-bottom:0;}
.heading-container:empty{display:none;}
@media screen and (max-height:820px){
.btn-large{padding:8px 13px;}
}
@media screen and (max-height:599px){
.btn-large{padding:5px 10px;font-size:14px;}
}
@media screen and (max-height:449px){
.tblwrapper{max-height:initial!important;}
.mocha{max-width:100vw!important;max-height:100vh!important;}
.mocha:not(.maximised),.mocha:not(.maximised)>.mochaOverlay,.mocha:not(.maximised)>iframe{max-height:calc(100vh - 33px)!important;}
.mocha .mochaContentBorder{overflow-y:hidden;}
.mochaContentWrapper{overflow:auto!important;}
.mochaContentWrapper  .pad{height:initial!important;}
}
.heading-container{min-height:20px;padding:10px 14px;margin-bottom:10px;background:linear-gradient(to bottom, rgba(249,249,249,1) 0%,rgba(241,241,241,1) 100%);border:1px solid #d5d5d5;border-bottom:1px solid rgba(0, 0, 0, 0.10);border-radius:4px;box-shadow:inset 0 1px 3px rgba(0, 0, 0, 0.05);flex:none;}
.mochaContent[style*='padding: 10px 12px'] :not(.boss-container):not(td)>.heading-container{margin:-10px -12px 10px;border:none;border-radius:0;}
.heading-container h3{overflow:hidden;text-overflow:ellipsis;margin-right:5px;}
h3{font-weight:normal;}
.heading-container h3:only-child{margin-right:0;}
.scroll-container{overflow:auto;box-sizing:border-box;padding-right:9px;min-height:6em;}
.scroll-container.header{height:calc(100% - 71px);}
.mochaContent[style*='padding: 10px 12px'] .scroll-container{margin-left:-12px;padding-left:12px;width:calc(100% + 24px - 6px);}
.mochaContent[style*='padding: 10px 12px'] .scroll-container .heading-container{width:calc(100% + 24px - 3px);box-sizing:border-box;}
.flex-column>.scroll-container{flex:1;}
ul.no-list{margin-left:0.5em;}
ul.no-list li>span{float:left;text-align:left;max-width:calc(50% - 1em);}
ul.no-list li>span,ul.no-list li a{word-wrap:break-word;word-break:break-word;}
ul.no-list li>strong{float:left;margin-right:0.5em;min-width:50%;}
@media only screen and (max-width: 1050px ){
ul.no-list li>strong{text-align:left;min-width:0;}
ul.no-list li>span{float:right;text-align:right;margin-right:0.5em;max-width:100%;}
}
.sws_form{margin-bottom:0;}
#admin_document_library_table_1549518550 tr th{font-size:13px;}
.no-list{list-style:none;margin:0;}
.btn-group > .btn:first-child{margin-left:0;-webkit-border-top-left-radius:4px;-moz-border-radius-topleft:4px;border-top-left-radius:4px;-webkit-border-bottom-left-radius:4px;-moz-border-radius-bottomleft:4px;border-bottom-left-radius:4px;}
.btn{background-image:none!important;}
.pull-right.pad{box-sizing:content-box;}
.table tbody:first-child tr:first-child td{border-top:none;}
[class*=" icon-"]:last-child{margin-right:0;}
.btn.pull-right{margin-left:0.5em;}
.btn.pull-left{margin-right:0.5em;}
.table > tbody > tr > td.active{background-color:transparent!important;}
.table.table-striped tr>td{transition:box-shadow 0.1s ease;}
.table.table-striped tr:hover>td,.table.table-striped tr:hover>td:hover{box-shadow:inset 0 17px 17px -17px rgba(64, 64, 64, 0.2),inset 0 -17px 17px -18px rgba(64, 64, 64, 0.25);}
.clearfix{*zoom:1;}
.clearfix:before,.clearfix:after{display:table;content:"";line-height:0;}
.clearfix:after{clear:both;}
.btn-file{overflow:hidden;position:relative;vertical-align:middle;}
.btn-file>input{position:absolute;top:0;right:0;margin:0;opacity:0;filter:alpha(opacity=0);transform:translate(-300px, 0) scale(4);font-size:23px;direction:ltr;cursor:pointer;}
.fileupload{margin-bottom:9px;}
.fileupload .btn{vertical-align:middle;}
.fileupload-new .fileupload-exists{display:none;}
.fileupload .fileupload-preview{text-overflow:ellipsis;white-space:nowrap;overflow:hidden;width:100px;display:inline-block;font-size:10.5px;}
.form-inline input,.form-horizontal input{white-space:normal;}
.table-condensed:not(.table-notoggles) td,.table-condensed:not(.table-notoggles) td a:not(.btn){overflow:hidden;text-overflow:ellipsis;white-space:normal;}
.table-condensed tr:hover td:hover{white-space:normal!important;white-space:-moz-pre-wrap!important;box-shadow:none;z-index:2;}
.table thead th{vertical-align:bottom;position:relative;border-radius:0px!important;}
img[alt=order]{position:absolute;right:2px;top:2px;opacity:0.3;transition-duration:0.15s;}
th:hover img[alt=order]{opacity:0.8;transition-duration:0.05s;}
img[src*="sort_dsc.png"][alt=order]{opacity:0.8;}
.mochaContent{height:100%;box-sizing:border-box;}
body{transition:background 0.3s;}
body .mochaOverlay,body iframe{transition:filter 0.3s, -webkit-filter 0.3s;}
body .mochaContentBorder{transition:filter 0.3s, -webkit-filter 0.3s;}
.pull-right{float:right;}
.pull-left{float:left;}
.quote_window{font-family:'Roboto', sans-serif;}
.quote_window .windowInfo .btn{border-color:white;border-width:0 0 0 1px;margin:0;box-sizing:border-box;}
.quote_window .windowInfo .btn.btn-primary{background:#c2e5ff;}
.quote_window .windowInfo h3{padding-left:20px;display:flex!important;flex-direction:row;max-height:unset;align-items:center!important;align-content:center;white-space:normal;}
.quote_window .scroll-container{height:auto;max-height:100%!important;flex:1;padding:20px 25px;}
.quote_window .scroll-container.header{height:calc(100% - 71px);}
.quote_window .scroll-container.header{padding-top:20px;}
.quote_window .scroll-container.nopad{padding-left:0;padding-right:0;}
.quote_window input[type="text"],.quote_window select{background-color:#fbfbfb;border-radius:0;border-color:#d4d4d4;box-shadow:none;height:46px;line-height:44px;font-size:18px;padding-left:0.5em;box-sizing:border-box;}
.quote_window .tblwrapper th:last-child:not(.centered),.quote_window .tblwrapper td:last-child:not(.centered){padding-right:20px;}
.quote_window .tblwrapper td{padding-top:1.5ex;padding-bottom:1.5ex;}
.quote_window .table th{color:#0a4570;}
.quote_window .table td{font-size:16px;padding-left:1ex;padding-right:1ex;}
.quote_window .btn-large{padding:18px 25px;font-size:20px;border-width:2px;position:relative;}
.quote_window .btn{border-radius:0!important;background-image:none;box-shadow:none;text-shadow:none!important;}
.quote_window .btn:hover{border-radius:1px!important;}
.quote_window .btn-primary{background-color:rgba(39,166,255,0.2);background-color:#d3edff;background-color:#c2e5ff;color:#333;box-shadow:none;border-color:rgba(39,166,255,0.2);border-color:#a8dbff;}
.quote_window .btn-primary:hover,.quote_window .btn-primary:active{background:rgba(39,166,255,0.4);background:#a8dbff;color:#2e2e2e;}
.quote_window .btn-success{background:rgba(136,215,51,0.2);background:#e7f7d6;background:#4ae559;background:#87fc70;background-image:linear-gradient(#a4f986, #67b634)!important;background:-moz-linear-gradient(top, #a4f986 0%, #67b634 100%)!important;background:-webkit-gradient(linear, left top, left bottom, color-stop(0%,#a4f986), color-stop(100%,#67b634))!important;background:-webkit-linear-gradient(top, #a4f986 0%,#67b634 100%)!important;background:-o-linear-gradient(top, #a4f986 0%,#67b634 100%)!important;background:-ms-linear-gradient(top, #a4f986 0%,#67b634 100%)!important;background:linear-gradient(to bottom, #a4f986 0%,#67b634 100%)!important;filter:progid:DXImageTransform.Microsoft.gradient( startColorstr='#a4f986', endColorstr='#67b634',GradientType=0 );border-color:rgba(136,215,51,0.2);border-color:#cfefad;color:black;color:white;text-shadow:0px 1px 2px rgba(0,0,0,0.3)!important;opacity:1;border-color:#a4f986 transparent #67b634 transparent;}
.quote_window .btn-success:hover,.quote_window .btn-success:active{background-color:rgba(136,215,51,0.4);background-color:#cfefad;background:#67b634!important;color:#2e2e2e;color:white;opacity:1;border-color:#67b634;transition:none;}
.quote_window .windowInfo:not(:only-child) .btn-large{padding-right:55px;}
.quote_window .btn-large::after{right:15px;}
.quote_window .btn-large::before{left:15px;}
.quote_window .windowInfo .btn-large::after,.quote_window .windowInfo .btn-large::before{font-family:'Material Icons';font-size:30px;position:absolute;top:0;bottom:0;display:flex;flex-direction:row;align-items:center;}
.quote_window .btn-large.btn-success::after,.quote_window .btn-large.btn-success::before{color:#74c819;color:white;filter:drop-shadow(0px 1px 1px rgba(0,0,0,0.1));}
.quote_window .btn-large.btn-primary::after,.quote_window .btn-large.btn-primary::before{color:#27a6ff;}
.scroll-container>:not(.tblwrapper):not(.span6){max-width:1660px;margin:0 auto;}
/*! CSS Used keyframes */
@-webkit-keyframes spin-rotate{0%{-webkit-transform:rotate(0deg);transform:rotate(0deg);}50%{-webkit-transform:rotate(180deg);transform:rotate(180deg);}100%{-webkit-transform:rotate(360deg);transform:rotate(360deg);}}
@keyframes spin-rotate{0%{-webkit-transform:rotate(0deg);transform:rotate(0deg);}50%{-webkit-transform:rotate(180deg);transform:rotate(180deg);}100%{-webkit-transform:rotate(360deg);transform:rotate(360deg);}}
/*! CSS Used fontfaces */
@font-face{font-family:'Material Icons';font-style:normal;font-weight:400;src:url(file:///C:/Users/enAct-W007D/img/2fcrYFNaTjcS6g4U3t-Y5ZjZjT5FdEJ140U2DJYC3mY.woff2) format('woff2');}	
</style>
          <div class="row-fluid agent_panel_wrapper contact_relation_receiver form-left flex-column  quote_window contact_receiver" id="agent_panel_wrapper_1549518548">
            <div class="windowInfo heading-container clearfix" style="display:block;padding:0;">
              <div class="btn-group pull-right">
                <input type="button" class="btn btn-large btn-primary" name="invoice_list_btn" value="Invoices">
                <input type="button" class="btn btn-large btn-primary" name="subscribe_list_btn" value="Subscription Fees">
              </div>
              <h3 class="pull-left" style="padding-top:0;margin-bottom:0;color: inherit;line-height:normal;">Subscriber Details</h3>
            </div>
            <div class="scroll-container header clearfix nopad" style="max-height: calc(100% - 65px);">
              <div class="col-md-6" style="padding:0;">
                <div class="subscriber_details clearfix pad">
                  <div class="pull-left pad ">
                    <ul class="no-list">
                      <li><a class="contact_detail_btn name_area"><b>Paint Professionals</b></a></li>
                      <li class="contact_address_area">Unit 13 / 22 Waddikee Road</li>
                      <li class="contact_suburb_area">Lonsdale</li>
                      <li class="contact_state_postcode_area">SA 5160</li>
                      <li class="contact_country_area">Australia</li>
                      <li>&nbsp;</li>
                      <li class="contact_phone_area">1800724683</li>
                      <li class="contact_email_area">martin@paintprofessionals.com.au</li>
                      <li><a class="contact_website_area">www.paintprofessionals.com.au</a></li>
                    </ul>
                  </div>
                  <div class="pull-right pad">
                    <div style="height:124px;text-align:center;"> <img class="agent_logo_img" src="http://paintpad.mysys.com.au/administrator/components/com_cweb/document/contact/contact_148/9e4c34fb62bc4930a86aea2243dd3e7c.jpg" style="width:125px;"> </div>
                    <form action="index.php" method="post" enctype="multipart/form-data" class="agent_logo_form">
                      <div class="fileupload fileupload-new" data-provides="fileupload" style="margin:0;"> <span class="btn btn-file pull-left"><span class="fileupload-new">Select Logo</span><span class="fileupload-exists">Change</span>
                        <input name="logo" type="file" accept="image/*">
                        </span> <span class="fileupload-preview pull-left"></span> <a class="close fileupload-exists icon-remove remove_button" data-dismiss="fileupload" style="float: none"></a> </div>
                      <input type="submit" class="btn btn-primary pull-right" value="Upload">
                      <input type="hidden" name="option" value="com_cweb">
                      <input type="hidden" name="controller" value="agent">
                      <input type="hidden" name="task" value="upload_agent_img">
                      <input type="hidden" name="format" value="agent">
                      <input type="hidden" name="agent_id" value="4">
                    </form>
                  </div>
                </div>
                <div class="clearfix">
                  <div class="heading-container clearfix">
                    <div class="pull-right agent_people_edit_section" style="display: block;">
                      <input type="button" class="add_people_btn btn btn-large btn-primary" value="Add People">
                    </div>
                    <div class="pull-left">
                      <h3>Subscriber's People</h3>
                    </div>
                  </div>
                  <div class="agent_people_area" style="padding:15px 0 0 25px;">
                    <div id="agent_people_readonly_wrapper_1549518549">
                      <table class="table table-condensed table-striped">
                        <thead>
                          <tr>
                            <th>Person</th>
                            <th>Active</th>
                            <th>Logged On</th>
                            <th>Last Activity</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr contact_id="149">
                            <td class="open_contact_person_btn"><span align="right" style="padding-right:5px;"><img src="http://paintpad.mysys.com.au/components/com_cweb/images/green_dot.gif" alt=""></span> <a class="edit_person_account_btn">Martin  Penney   Bld Lic 252014</a></td>
                            <td class="active"><img src="http://paintpad.mysys.com.au/components/com_cweb/images/tick.png"></td>
                            <td> 07/02/2019 03:51 pm </td>
                            <td> 07/02/2019 04:09 pm </td>
                          </tr>
                          <tr contact_id="186">
                            <td class="open_contact_person_btn"><span align="right" style="padding-right:5px;"><img src="http://paintpad.mysys.com.au/components/com_cweb/images/dot.gif" alt=""></span> <a class="edit_person_account_btn">Jenny Woodward</a></td>
                            <td class="active"><img src="http://paintpad.mysys.com.au/components/com_cweb/images/tick.png"></td>
                            <td> 19/12/2017 12:50 pm </td>
                            <td> 19/12/2017 12:50 pm </td>
                          </tr>
                          <tr contact_id="187">
                            <td class="open_contact_person_btn"><span align="right" style="padding-right:5px;"><img src="http://paintpad.mysys.com.au/components/com_cweb/images/dot.gif" alt=""></span> <a class="edit_person_account_btn">David Esson</a></td>
                            <td class="active"><img src="http://paintpad.mysys.com.au/components/com_cweb/images/tick.png"></td>
                            <td> 20/03/2018 12:43 pm </td>
                            <td> 20/03/2018 12:44 pm </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <div class="clearfix">
                  <div class="agent_doc_area" style="padding-left:25px;">
                    <div id="admin_document_library_wrapper_1549518550">
                      <form id="admin_document_library_table_form_1549518550" method="post" action="index.php" class="form-inline lib_doc_form_receiver">
                        <div class="windowInfo clearfix">
                          <div class="pull-right"> <a class="btn btn-primary button form_edit_file" href="#" library_doc_id="0">Add New Library Document</a> </div>
                          <div class="pull-left">
                            <input type="text" value="" name="search" class="change_button input-medium" placeholder="Search/filter" autofocus>
                          </div>
                        </div>
                        <input type="hidden" name="option" value="com_cweb" style="max-height: calc(100% - 65px);">
                        <input type="hidden" name="view" value="admin_document">
                        <input type="hidden" name="layout" value="admin_document_library_table">
                        <input type="hidden" name="format" value="admin_document">
                        <input type="hidden" name="limitstart" value="0">
                        <input type="hidden" name="limit" value="20">
                        <input type="hidden" name="order_by" value="library_doc_id">
                        <input type="hidden" name="direction" value="desc">
                        <input type="hidden" name="stamp" value="1549518550">
                        <input type="hidden" name="agent_id" value="4">
                      </form>
                      <div class="spinner" id="spinner-jru7bntj" style="display: none; width: 709px; height: 60px; position: absolute; left: 25px; top: 521px; opacity: 1;">
                        <div class="spinner-content" style="position: absolute; left: 305px; top: 0px;">
                          <p class="spinner-msg">Loading</p>
                          <div class="spinner-img"></div>
                        </div>
                      </div>
                      <div class="admin_document_library_table_display_area_1549518550" id="admin_document_library_table_display_area_1549518550" style="opacity: 1;">
                        <form action="index.php" method="post" id="library_list_form_1549518550">
                          <div class="floatThead-wrapper clearfix" style="position: relative; clear: both; max-height: calc(100% - 58px);">
                            
                            <div id="admin_document_library_table_wrapper_1549518550" class="tblwrapper">
                              <table class="tablesorter table table-bordered table-striped table-condensed" id="admin_document_library_table_1549518550" style="table-layout: fixed; min-width: 708px;">
                                <colgroup>
                                <col style="width: 56.3333px;">
                                <col style="width: 345.717px;">
                                <col style="width: 91.5833px;">
                                <col style="width: 99.7667px;">
                                <col style="width: 114.767px;">
                                </colgroup>
                                <thead>
                                  <tr>
                                    <th class="order_button library_doc_id" onclick="order_button_clicked_admin_document_library_table_form_1549518550(this)" order_by="library_doc_id" direction="asc"> <img src="http://paintpad.mysys.com.au/components/com_cweb/images/sort_dsc.png" alt="order" border="0">File ID </th>
                                    <th class="order_button library_doc_name" onclick="order_button_clicked_admin_document_library_table_form_1549518550(this)" order_by="library_doc_name" direction="desc"> <img src="http://paintpad.mysys.com.au/components/com_cweb/images/arrow_no_sort.png" alt="order" border="0">File Name </th>
                                    <th class="order_button library_doc_desc" onclick="order_button_clicked_admin_document_library_table_form_1549518550(this)" order_by="library_doc_desc" direction="desc"> <img src="http://paintpad.mysys.com.au/components/com_cweb/images/arrow_no_sort.png" alt="order" border="0">Description </th>
                                    <th class="order_button username" onclick="order_button_clicked_admin_document_library_table_form_1549518550(this)" order_by="u.username" direction="desc"> <img src="http://paintpad.mysys.com.au/components/com_cweb/images/arrow_no_sort.png" alt="order" border="0">Uploaded By </th>
                                    <th class="order_button uploaded_date" onclick="order_button_clicked_admin_document_library_table_form_1549518550(this)" order_by="uploaded_date" direction="desc"> <img src="http://paintpad.mysys.com.au/components/com_cweb/images/arrow_no_sort.png" alt="order" border="0">Uploaded Date </th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr library_doc_id="267" class="edit_file">
                                    <td> 267 </td>
                                    <td> Eco Sports Shield Data Sheet </td>
                                    <td></td>
                                    <td> martin </td>
                                    <td> 29/10/2018 </td>
                                  </tr>
                                  <tr library_doc_id="266" class="edit_file">
                                    <td> 266 </td>
                                    <td> Eco Sports Shield Data Sheet </td>
                                    <td></td>
                                    <td> martin </td>
                                    <td> 29/10/2018 </td>
                                  </tr>
                                  <tr library_doc_id="262" class="edit_file">
                                    <td> 262 </td>
                                    <td> Dulux Acratex Elastomeric Membrane </td>
                                    <td></td>
                                    <td> martin </td>
                                    <td> 31/08/2018 </td>
                                  </tr>
                                  <tr library_doc_id="261" class="edit_file">
                                    <td> 261 </td>
                                    <td> Dulux Acratex AcraSkin Membrane </td>
                                    <td></td>
                                    <td> martin </td>
                                    <td> 31/08/2018 </td>
                                  </tr>
                                  <tr library_doc_id="252" class="edit_file">
                                    <td> 252 </td>
                                    <td> Acrashield Advance Membrane </td>
                                    <td></td>
                                    <td> martin </td>
                                    <td> 13/08/2018 </td>
                                  </tr>
                                  <tr library_doc_id="251" class="edit_file">
                                    <td> 251 </td>
                                    <td> Acrashield Advance Data Sheet </td>
                                    <td></td>
                                    <td> martin </td>
                                    <td> 07/08/2018 </td>
                                  </tr>
                                  <tr library_doc_id="243" class="edit_file">
                                    <td> 243 </td>
                                    <td> Maintenance Aust Builders License </td>
                                    <td></td>
                                    <td> martin </td>
                                    <td> 17/07/2018 </td>
                                  </tr>
                                  <tr library_doc_id="221" class="edit_file">
                                    <td> 221 </td>
                                    <td> Caring for Your Paint Job </td>
                                    <td></td>
                                    <td> martin </td>
                                    <td> 13/06/2018 </td>
                                  </tr>
                                  <tr library_doc_id="215" class="edit_file">
                                    <td> 215 </td>
                                    <td> Murabond Mineral Silicate Paint </td>
                                    <td></td>
                                    <td> martin </td>
                                    <td> 08/06/2018 </td>
                                  </tr>
                                  <tr library_doc_id="214" class="edit_file">
                                    <td> 214 </td>
                                    <td> Your Benefits </td>
                                    <td></td>
                                    <td> martin </td>
                                    <td> 07/06/2018 </td>
                                  </tr>
                                  <tr library_doc_id="212" class="edit_file">
                                    <td> 212 </td>
                                    <td> Norglass Marine Coatings </td>
                                    <td></td>
                                    <td> martin </td>
                                    <td> 05/06/2018 </td>
                                  </tr>
                                  <tr library_doc_id="181" class="edit_file">
                                    <td> 181 </td>
                                    <td> Luxapool Fibreglass Application Guide </td>
                                    <td></td>
                                    <td> paintpad </td>
                                    <td> 26/02/2018 </td>
                                  </tr>
                                  <tr library_doc_id="180" class="edit_file">
                                    <td> 180 </td>
                                    <td> Luxapool Epoxy Pool Care Guide </td>
                                    <td></td>
                                    <td></td>
                                    <td> 01/01/1970 </td>
                                  </tr>
                                  <tr library_doc_id="179" class="edit_file">
                                    <td> 179 </td>
                                    <td> Porters Stucco Wax </td>
                                    <td></td>
                                    <td> paintpad </td>
                                    <td> 26/02/2018 </td>
                                  </tr>
                                  <tr library_doc_id="178" class="edit_file">
                                    <td> 178 </td>
                                    <td> Porters Fresco </td>
                                    <td></td>
                                    <td> paintpad </td>
                                    <td> 26/02/2018 </td>
                                  </tr>
                                  <tr library_doc_id="177" class="edit_file">
                                    <td> 177 </td>
                                    <td> Interior Painting Procedures </td>
                                    <td></td>
                                    <td> paintpad </td>
                                    <td> 26/02/2018 </td>
                                  </tr>
                                  <tr library_doc_id="60" class="edit_file">
                                    <td> 60 </td>
                                    <td> Dulux Wash &amp; Wear Plus </td>
                                    <td></td>
                                    <td> martin </td>
                                    <td> 31/07/2017 </td>
                                  </tr>
                                  <tr library_doc_id="59" class="edit_file">
                                    <td> 59 </td>
                                    <td> Heritage Projects Completed </td>
                                    <td></td>
                                    <td> martin </td>
                                    <td> 31/07/2017 </td>
                                  </tr>
                                  <tr library_doc_id="57" class="edit_file">
                                    <td> 57 </td>
                                    <td> Domestic Contract VO Form </td>
                                    <td></td>
                                    <td> martin </td>
                                    <td> 31/07/2017 </td>
                                  </tr>
                                  <tr library_doc_id="53" class="edit_file">
                                    <td> 53 </td>
                                    <td> Porters Mineral Silicate </td>
                                    <td></td>
                                    <td> martin </td>
                                    <td> 25/07/2017 </td>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                          </div>
                          <div class="pagination">
                            <ul>
                              <li class="disabled"><a class="limit" title="Display this many rows">
                                <select id="limit" name="limit" class="inputbox" size="1" onchange="$('admin_document_library_table_form_1549518550').getElement('input[name=limit]').value=this.value;$('admin_document_library_table_form_1549518550').getElement('input[name=limitstart]').value=0; form_hash.get('admin_document_library_table_form_1549518550').send();">
                                  <option value="5">5</option>
                                  <option value="10">10</option>
                                  <option value="15">15</option>
                                  <option value="20" selected="selected">20</option>
                                  <option value="25">25</option>
                                  <option value="30">30</option>
                                  <option value="50">50</option>
                                  <option value="100">100</option>
                                </select>
                                </a></li>
                              <li class="active"><a>Start</a></li>
                              <li class="active"><a>Prev</a></li>
                              <li class="active"><a>1</a></li>
                              <li><a href="#" title="2" onclick="$('admin_document_library_table_form_1549518550').getElement('input[name=limitstart]').value=20; form_hash.get('admin_document_library_table_form_1549518550').send(); return false; ">2</a></li>
                              <li><a href="#" title="3" onclick="$('admin_document_library_table_form_1549518550').getElement('input[name=limitstart]').value=40; form_hash.get('admin_document_library_table_form_1549518550').send(); return false; ">3</a></li>
                              <li><a href="#" title="Next" onclick="$('admin_document_library_table_form_1549518550').getElement('input[name=limitstart]').value=20; form_hash.get('admin_document_library_table_form_1549518550').send(); return false; ">Next</a></li>
                              <li><a href="#" title="End" onclick="$('admin_document_library_table_form_1549518550').getElement('input[name=limitstart]').value=40; form_hash.get('admin_document_library_table_form_1549518550').send(); return false; ">End</a></li>
                              <li class="disabled"><a class="limit" style="">Page 1 of 3 <br>
                                (41 Items)</a></li>
                              <input type="hidden" name="limitstart" value="0">
                            </ul>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6" style="padding:0;">
                <form action="index.php" method="post" class="form-horizontal form-elos setting_form">
                  <div class="heading-container clearfix">
                    <div class="pull-right setting_edit_section" style="display: block;">
                      <input type="button" class="btn btn-large btn-primary" name="edit_setting_btn" value="Edit">
                    </div>
                    <div style="display:none;" class="pull-right setting_submit_section">
                      <input type="button" class="btn btn-large btn-danger" name="setting_cancel_btn" value="Cancel">
                      <input type="submit" class="btn btn-large btn-success" value="Update">
                    </div>
                    <div class="pull-left">
                      <h3>Settings</h3>
                    </div>
                  </div>
                  <div class="setting_area forty scroll-container header">
                    <div class="form-elos form-horizontal">
                      <ul class="no-list">
                        <li class="clearfix"><strong>Application Cost</strong><span>55/H</span></li>
                        <li class="clearfix"><strong>Profit Mark Up %</strong><span>42.86%</span></li>
                        <li class="clearfix"><strong>Gross Margin %</strong><span>30.00%</span></li>
                        <li class="clearfix"><strong>Default Deposit Percent</strong><span>30%</span></li>
                        <li class="clearfix"><strong>Terms &amp; Conditions Doc</strong><span>PP Terms of Trade</span></li>
                        <li class="clearfix"><strong>Xero Consumer Key</strong><span>●●●●●</span></li>
                        <li class="clearfix"><strong>Xero Share Secret </strong><span>●●●●●</span></li>
                        <li class="clearfix"><strong>Xero Invoice Line Account Code </strong><span>200</span></li>
                        <li class="clearfix"><strong>Account Name </strong><span>Maintenance Australia Pty Ltd</span></li>
                        <li class="clearfix"><strong>Account BSB </strong><span>015220</span></li>
                        <li class="clearfix"><strong>Account Number </strong><span>213539683</span></li>
                        <li class="clearfix"><strong>Payment Phone Number </strong><span>1800686525</span></li>
                        <li class="clearfix"><strong>Colour Consultant Item</strong><span>A Colour Consultant  - Professional On-site Colour Consultation - 1hr - You will receive an individual consultation to assist you select the right colours for you.  Selections will be added to our Job Specification Sheet</span></li>
                      </ul>
                    </div>
                  </div>
                  <input type="hidden" name="option" value="com_cweb">
                  <input type="hidden" name="controller" value="agent">
                  <input type="hidden" name="task" value="update_setting">
                  <input type="hidden" name="format" value="agent">
                  <input type="hidden" name="agent_id" value="4">
                </form>
                <form action="index.php" method="post" class="form-horizontal form-elos quote_email_form">
                  <div class="heading-container clearfix">
                    <div class="pull-right quote_email_edit_section" style="display: block;">
                      <input type="button" class="btn btn-large btn-primary" name="edit_quote_email_btn" value="Edit">
                    </div>
                    <div style="display:none;" class="pull-right quote_email_submit_section">
                      <input type="button" class="btn btn-large btn-danger" name="quote_email_cancel_btn" value="Cancel">
                      <input type="submit" class="btn btn-large btn-success" value="Update">
                    </div>
                    <div class="pull-left">
                      <h3>Quote Email Settings</h3>
                    </div>
                  </div>
                  <div class="quote_email_area forty scroll-container header">
                    <div class="form-elos form-horizontal">
                      <ul class="no-list">
                        <li class="clearfix"><strong>Server Email Address</strong><span>paintprofessionals<wbr>
                          @paintpad<wbr>
                          .mysys<wbr>
                          .com<wbr>
                          .au</span></li>
                        <li class="clearfix"><strong>BCC to</strong><span>Martin<wbr>
                          @paintprofessionals<wbr>
                          .com<wbr>
                          .au<br>
                          admin<wbr>
                          @paintprofessionals<wbr>
                          .com<wbr>
                          .au</span></li>
                        <li class="clearfix"><strong>Quote Email Subject</strong><span>Professional Painting Quote</span></li>
                        <li><strong> Include Documents</strong><span>
                          <table class="table table-condensed table-bordered">
                            <tbody>
                              <tr>
                                <td>Caring for Your Paint Job</td>
                              </tr>
                              <tr>
                                <td>Client Reference - Barossa Council</td>
                              </tr>
                              <tr>
                                <td>Client Reference - Crowne Plaza</td>
                              </tr>
                              <tr>
                                <td>Client Reference - Defense</td>
                              </tr>
                              <tr>
                                <td>Dulux Accredited Painter</td>
                              </tr>
                              <tr>
                                <td>Insurance - Public Liability $20m</td>
                              </tr>
                              <tr>
                                <td>Maintenance Aust Builders License</td>
                              </tr>
                              <tr>
                                <td>Our Story - Paint Professionals</td>
                              </tr>
                              <tr>
                                <td>PP Terms of Trade</td>
                              </tr>
                              <tr>
                                <td>Responsible Officer - Martin Penney</td>
                              </tr>
                              <tr>
                                <td>Sleeping on the Job</td>
                              </tr>
                              <tr>
                                <td>Workcover - Return to Work Reg 2019</td>
                              </tr>
                              <tr>
                                <td>Your Benefits</td>
                              </tr>
                            </tbody>
                          </table>
                          </span></li>
                      </ul>
                    </div>
                  </div>
                  <input type="hidden" name="option" value="com_cweb">
                  <input type="hidden" name="controller" value="agent">
                  <input type="hidden" name="task" value="update_quote_email">
                  <input type="hidden" name="format" value="agent">
                  <input type="hidden" name="agent_id" value="4">
                </form>
                <form action="index.php" method="post" class="form-horizontal form-elos jss_email_form">
                  <div class="heading-container clearfix">
                    <div class="pull-right jss_email_edit_section" style="display: block;">
                      <input type="button" class="btn btn-large btn-primary" name="edit_jss_email_btn" value="Edit">
                    </div>
                    <div style="display:none;" class="pull-right jss_email_submit_section">
                      <input type="button" class="btn btn-large btn-danger" name="jss_email_cancel_btn" value="Cancel">
                      <input type="submit" class="btn btn-large btn-success" value="Update">
                    </div>
                    <div class="pull-left">
                      <h3>JSS Email Settings</h3>
                    </div>
                  </div>
                  <div class="jss_email_area forty scroll-container header">
                    <div class="form-elos form-horizontal">
                      <ul class="no-list">
                        <li class="clearfix"><strong>Server Email Address</strong><span>paintprofessionals<wbr>
                          @paintpad<wbr>
                          .mysys<wbr>
                          .com<wbr>
                          .au</span></li>
                        <li class="clearfix"><strong>BCC to</strong><span>Admin<wbr>
                          @paintprofessionals<wbr>
                          .com<wbr>
                          .au<br>
                          martin<wbr>
                          @paintprofessionals<wbr>
                          .com<wbr>
                          .au</span></li>
                        <li class="clearfix"><strong>JSS Email Subject</strong><span>Job Specification Sheet</span></li>
                        <li><strong> Include Documents</strong><span>
                          <table class="table table-condensed table-bordered">
                            <tbody>
                              <tr>
                                <td>Interior Painting Procedures</td>
                              </tr>
                              <tr>
                                <td>Prep Levels - Exterior</td>
                              </tr>
                              <tr>
                                <td>Prep Levels - Interior</td>
                              </tr>
                            </tbody>
                          </table>
                          </span></li>
                      </ul>
                    </div>
                  </div>
                  <input type="hidden" name="option" value="com_cweb">
                  <input type="hidden" name="controller" value="agent">
                  <input type="hidden" name="task" value="update_jss_email">
                  <input type="hidden" name="format" value="agent">
                  <input type="hidden" name="agent_id" value="4">
                </form>
                <form action="index.php" method="post" class="form-horizontal form-elos subscribe_form">
                  <div class="heading-container clearfix">
                    <div class="pull-right subscribe_edit_section" style="display: block;">
                      <input type="button" class="btn btn-large btn-primary" name="edit_subscribe_btn" value="Edit">
                    </div>
                    <div style="display:none;" class="pull-right subscribe_submit_section">
                      <input type="button" class="btn btn-large btn-danger" name="subscribe_cancel_btn" value="Cancel">
                      <input type="submit" class="btn btn-large btn-success" value="Update">
                    </div>
                    <div class="pull-left">
                      <h3>Subscription Settings</h3>
                    </div>
                  </div>
                  <div class="subscribe_area forty scroll-container header">
                    <div class="form-elos form-horizontal">
                      <ul class="no-list">
                        <li class="clearfix"><strong>Subscribe Month</strong><span>12 Month</span></li>
                        <li class="clearfix"><strong>Is Start Subscribe</strong><span>Yes</span></li>
                        <li class="clearfix"><strong>Subscribe Start Date</strong><span>11/07/2017</span></li>
                        <li class="clearfix"><strong>Dulux Accredited</strong><span>Yes</span></li>
                        <!--<li class="clearfix"><strong>Apply Initial Discount</strong><span>$0.00</span></li>
		<li class="clearfix"><strong>Bonus Quotes per Month</strong><span>0</span></li>-->
                      </ul>
                    </div>
                  </div>
                  <input type="hidden" name="option" value="com_cweb">
                  <input type="hidden" name="controller" value="agent">
                  <input type="hidden" name="task" value="update_subscribe">
                  <input type="hidden" name="format" value="agent">
                  <input type="hidden" name="agent_id" value="4">
                </form>
                <div class="agent_prep_level_area">
                  <div id="prep_level_painting_prep_level_table_wrapper_1549518550">
                    <div class="windowInfo clearfix" style="display:block;">
                      <form id="prep_level_painting_prep_level_table_form_1549518550" method="post" action="index.php" class="sws_form form-inline prep_level_painting_prep_level_form_receiver painting_prep_level_table_form_refresh">
                        <div class="pull-right">
                          <input type="button" class="btn btn-primary add_painting_prep_level_btn" value="Add New Prep. Level">
                        </div>
                        <div class="pull-left">
                          <input type="text" value="" name="search" class="change_button" placeholder="Search/filter">
                        </div>
                        <input type="hidden" name="option" value="com_cweb">
                        <input type="hidden" name="view" value="painting_product">
                        <input type="hidden" name="layout" value="painting_prep_level_list">
                        <input type="hidden" name="format" value="painting_product">
                        <input type="hidden" name="limitstart" value="0">
                        <input type="hidden" name="limit" value="100">
                        <input type="hidden" name="order_by" value="prep_level.name">
                        <input type="hidden" name="direction" value="asc">
                        <input type="hidden" name="stamp" value="1549518550">
                        <input type="hidden" name="agent_id" value="4">
                      </form>
                      <div class="spinner" id="spinner-jru7bntg" style="display: none; width: 697px; height: 52px; position: absolute; left: 797px; top: 1805px; opacity: 1;">
                        <div class="spinner-content" style="position: absolute; left: 299px; top: 0px;">
                          <p class="spinner-msg">Loading</p>
                          <div class="spinner-img"></div>
                        </div>
                      </div>
                    </div>
                    <div class="prep_level_painting_prep_level_table_display_area" id="prep_level_painting_prep_level_table_display_area" style="max-height: calc(100% - 65px); overflow: auto; opacity: 1;">
                      <div id="painting_prep_level_list_table_1549518550">
                        <table class="table table-bordered table-striped table-condensed" id="proposal_manage_list_table_1549518550">
                          <thead>
                            <tr>
                              <th>Name</th>
                              <th>% uplift to paint cost</th>
                              <th>% uplift to paint time</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr class="open_prep_level_btn" prep_level_id="5">
                              <td>1 - Standard Prep</td>
                              <td>3%</td>
                              <td>4%</td>
                            </tr>
                            <tr class="open_prep_level_btn" prep_level_id="6">
                              <td>2 - Professional Prep (Default)</td>
                              <td>5%</td>
                              <td>7%</td>
                            </tr>
                            <tr class="open_prep_level_btn" prep_level_id="7">
                              <td>3 - Premium Prep</td>
                              <td>8%</td>
                              <td>12%</td>
                            </tr>
                            <tr class="open_prep_level_btn" prep_level_id="15">
                              <td>F1 - Epoxy Floor Prep</td>
                              <td>15%</td>
                              <td>150%</td>
                            </tr>
                            <tr class="open_prep_level_btn" prep_level_id="183">
                              <td>F2 - Acid Etch Floor Prep</td>
                              <td>20%</td>
                              <td>30%</td>
                            </tr>
                            <tr class="open_prep_level_btn" prep_level_id="89">
                              <td>MG - Marine Grade Steel Prep</td>
                              <td>15%</td>
                              <td>50%</td>
                            </tr>
                            <tr class="open_prep_level_btn" prep_level_id="17">
                              <td>PC - Steelwork Protective Coatings Prep</td>
                              <td>15%</td>
                              <td>150%</td>
                            </tr>
                            <tr class="open_prep_level_btn" prep_level_id="185">
                              <td>R1 - Roof Tile Prep</td>
                              <td>15%</td>
                              <td>75%</td>
                            </tr>
                            <tr class="open_prep_level_btn" prep_level_id="8">
                              <td>S1 - Swimming Pool Prep 1</td>
                              <td>15%</td>
                              <td>150%</td>
                            </tr>
                            <tr class="open_prep_level_btn" prep_level_id="190">
                              <td>S2 - Swimming Pool Prep 2</td>
                              <td>10%</td>
                              <td>75%</td>
                            </tr>
                          </tbody>
                          <tfoot>
                          </tfoot>
                        </table>
                      </div>
                      <div class="pagination">
                        <ul>
                          <li class="disabled"><a class="limit" title="Display this many rows">
                            <select id="limit" name="limit" class="inputbox" size="1" onchange="$('painting_prep_level_table_form_1549518550').getElement('input[name=limit]').value=this.value;$('painting_prep_level_table_form_1549518550').getElement('input[name=limitstart]').value=0; form_hash.get('painting_prep_level_table_form_1549518550').send();">
                              <option value="5">5</option>
                              <option value="10">10</option>
                              <option value="15">15</option>
                              <option value="20">20</option>
                              <option value="25">25</option>
                              <option value="30">30</option>
                              <option value="50">50</option>
                              <option value="100" selected="selected">100</option>
                            </select>
                            </a></li>
                          <li class="disabled"><a class="limit" style="">(10 Items)</a></li>
                          <input type="hidden" name="limitstart" value="0">
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="win1_resizeHandle_nw" class="handle corner cornerNW" style="top: -1px; left: -1px; cursor: nw-resize;"></div>
  <div id="win1_resizeHandle_w" class="handle handleW" style="top: 9px; left: -1px; cursor: w-resize; height: 783px;"></div>
  <div id="win1_resizeHandle_sw" class="handle corner cornerSW" style="bottom: -1px; left: -1px; cursor: sw-resize;"></div>
  <div id="win1_resizeHandle_s" class="handle handleS" style="bottom: -1px; left: 9px; cursor: s-resize; width: 1508px;"></div>
  <div id="win1_resizeHandle_se" class="handle corner cornerSE" style="bottom: -1px; right: -1px; cursor: se-resize;"></div>
  <div id="win1_resizeHandle_e" class="handle handleE" style="top: 9px; right: -1px; cursor: e-resize; height: 773px;"></div>
  <div id="win1_resizeHandle_ne" class="handle corner cornerNE" style="top: -1px; right: -1px; cursor: ne-resize;"></div>
  <div id="win1_resizeHandle_n" class="handle handleN" style="top: -1px; left: 9px; cursor: n-resize; width: 1518px;"></div>
  <div id="win1_controls" class="mochaControls" style="width: 106px; right: 6px; top: 6px;">
    <div id="win1_closeButton" class="mochaCloseButton mochaWindowButton replaced" title="Close"></div>
    <div id="win1_maximizeButton" class="mochaMaximizeButton mochaWindowButton replaced" title="Maximize"></div>
    <div id="win1_minimizeButton" class="mochaMinimizeButton mochaWindowButton replaced" title="Minimize"></div>
  </div>
  <canvas id="win1_canvasControls" class="mochaCanvasControls" width="151" height="14" style="right: 6px; top: 6px;"></canvas>
  <div id="win1_spinner" class="mochaSpinner" width="16" height="16" style="display: none; left: 3px; bottom: 4px;"></div>
  <iframe src="javascript:false;" style="position: absolute; z-index: -1; width: 100%; height: 100%; border-width: 0px; top: 0px; left: 0px;"></iframe>
</div>
