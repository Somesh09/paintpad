<html>
<body>
	<form action="create" method="post" id="subscriberSave" enctype="multipart/form-data">
		<input type="submit" class="submit btn btn-primary" name="submit" >
	<table class="table">
		<tr>
			<td><label>Name</label></td>
			<td><input type="text" name="name" class="form-control" placeholder="Please Enter Your Name" required="required" ></td>
		</tr>
		<tr>
			<td><label>Address</label></td>
			<td><input type="text" name="address" placeholder="Please Enter Your Address" class="form-control" required="required"></td>
		</tr>
		<tr>
			<td><label>Suburb</label></td>
			<td><input type="text" name="suburb" class="form-control" placeholder="Please Enter Your Suburb" required="required"></td>
		</tr>
		<tr>
			<td><label>State</label></td>
			<td>
				<select name="state" class="pull-left form-control span pass_field" required="required">
					<option value="">Please Select Your State</option>
					<option value="ACT">ACT</option>
					<option value="NSW">NSW</option>
					<option value="NT">NT</option>
					<option value="QLD">QLD</option>
					<option value="SA">SA</option>
					<option value="TAS">TAS</option>
					<option value="VIC">VIC</option>
					<option value="WA">WA</option>
				</select>
			</td>
		</tr>
		<tr>
			<td><label>Postcode</label></td>
			<td><input type="text" name="postcode" class="form-control" placeholder="Please Enter Your Postcode" required="required"></td>
		</tr>
		<tr>
			<td><label>Country</label></td>
			<td>
				<select name="contact_country" class="form-control " required="required">
					<option value="">Please Select Your Country</option>
					<option value="Australia">Australia</option>
					<option value="New Zealand">New Zealand</option>
				</select>
			</td>
		</tr>
		<tr>
			<td><label>Postal Address</label></td>
			<td><input type="text" name="pAddress" class="form-control" placeholder="Please Enter Your Postal Address"></td>
		</tr>
		<tr>
			<td><label>Postal Suburb</label></td>
			<td><input type="text" name="pSuburb" class="form-control" placeholder="Please Enter Your Postal Suburb"></td>
		</tr>
		<tr>
			<td><label>Postal State</label></td>
			<td>
				<select name="pState" class="pull-left form-control">
					<option value="">Please Select Your Postal State</option>
					<option value="ACT">ACT</option>
					<option value="NSW">NSW</option>
					<option value="NT">NT</option>
					<option value="QLD">QLD</option>
					<option value="SA">SA</option>
					<option value="TAS">TAS</option>
					<option value="VIC">VIC</option>
					<option value="WA">WA</option>
				</select>
			</td>
		</tr>
		<tr>
			<td><label>Postal Postcode</label></td>
			<td><input type="text" name="pPostcode" class="form-control" placeholder="Please Enter Your Postal Postcode"></td>
		</tr>	
		<tr>
			<td><label>Postal Postcode</label></td>
			<td><input type="text" name="pPostcode" class="form-control" placeholder="Please Enter Your Postal Postcode"></td>
		</tr>	
		<tr>
			<td><label>Postal Country</label></td>
			<td>
				<select name="pContact_country" class="form-control country_select">
					<option value="">Please Select Your Postal Country</option>
					<option value="Australia">Australia</option>
					<option value="New Zealand">New Zealand</option>
				</select>
			</td>
		</tr>
		<tr>
			<td><label>Phone</label></td>
			<td><input type="text" name="phone" class="form-control" placeholder="Please Enter Your Phone Number" required="required"></td>
		</tr>
		<tr>
			<td><label>Email</label></td>
			<td><input type="text" name="email" class="form-control" placeholder="Please Enter Your Email" required="required"></td>
		</tr>
		<tr>
			<td><label>Website</label></td>
			<td><input type="text" name="website" class="form-control" placeholder="Please Enter Your Website Name" required="required"></td>
		</tr>
		<tr>
			<td><label>ABN</label></td>
			<td><input type="text" name="abn" class="form-control" placeholder="Please Enter Your ABN" required="required"></td>
		</tr>	
	</table>		
	</form>																	
</body>
</html>
<script>
	$('#subscriberSave').submit(function(e){
		var datastring = $(this).serialize();
		console.log(datastring);
        $.ajax({
          url: $(this).attr('action'),
          type: 'post',
          data: $(this).serialize(),
          success: function (response) {
               console.log(response);              
               if(response == 1){
									$("#modalSubscriber").modal('hide');
									$.pjax.reload('#my-subscriber-pjax' , {timeout : false});
               }
          }, 
     	});
        e.preventDefault();
	})
</script>