<?php
use yii\widgets\Pjax;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
?>

<!DOCTYPE html>
<html>
<head>
    <style type="text/css">
.sort {
    float: right;
}
.sort img {
    width: 21px;
    cursor: pointer;
}
.modal-dialog.modal-sl {
    max-height: 80%;
}

div#roompjax .sortRoomInner {
    padding-bottom: 5px !important;
}
div#roompjax .modal-body-div-add.sortRoomInner h2 {
    width: 490px;
    float: left;
    margin: 0;
    font-size: 20px;
}
.sortRoomInner button {
    margin-top: 10px !important;
}

</style>
</head>
<body>


<div class="tbl-brands-form form-inner-head">

    <div class="modal-body-div-add sortRoomInner">
    <button type="button" class="close sort-roomclose" data-dismiss="modal" aria-hidden="true">×</button>
    <div style="width: 490px;float: left;">
    <h2 class="">Sort Rooms</h2>
    <span align="left" style="color:#000">Hold and drag rows to sort the room</span>
    </div>
    <?= Html::submitButton('Save', ['class' => 'btn btn-success sortRoom','id'=>'blah22']) ?>
    <!-- <button type="button" class="close glyphicon glyphicon-remove" data-dismiss="modal" aria-hidden="true">x</button> -->
    </div>
    <div style="cursor: pointer;width: 570px;overflow-y: auto;overflow-x: hidden;margin: 12px auto;max-height: 600px;padding: 12px 0 0 0;" class="model-inner-part" >
        
        <table class="table table-hover table-condensed table-striped table-bordered">

            <tbody class="row_position2">
            <?php
                foreach($model as $model){
            ?>
                <tr  id="<?php echo $model->room_id ?>">
                    <td><?php echo $model->name ?></td>
                </tr>
            <?php 
                } 
            ?>
            </tbody>

        </table>
    </div> <!-- container / end -->
    </div>


</body>
<script type="text/javascript">
    $( ".row_position2" ).sortable({
        delay: 150,
        stop: function() {
            var selectedData = new Array();
            $('.row_position2>tr').each(function() {
                selectedData.push($(this).attr("id"));
            });
            updateOrder(selectedData);
        }
    });


    function updateOrder(data) {
        $.ajax({
            url:"ajaxpro4.php",
            type:'post',
            data:{position:data},
            success:function(){
              $("body").removeClass('modal-open').removeAttr("style");
            }
        })
    }
    $(document).on('click', '.sortRoom', function(){ 
                     $("#modalroom").modal('hide');
                     $(".modal-backdrop").remove();
               $.pjax.reload('#roompjax' , {timeout : false});
                });
</script>
</html>
