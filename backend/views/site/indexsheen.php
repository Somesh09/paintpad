<?php
   use yii\helpers\Html;
   //use yii\grid\GridView;
   use yii\helpers\Url;
   use yii\bootstrap\Modal;
   use yii\widgets\Pjax;
   use dosamigos\grid\GridView;
   
   /* @var $this yii\web\View */
   /* @var $searchModel app\models\TblSheenSearch */
   /* @var $dataProvider yii\data\ActiveDataProvider */
   
   ?>
<style>
   /* .tab-content {
   padding: 0px 0 !important;
   } */
   .tbl-sheen-search.tbl-search-input-all .form-group {
   margin-bottom: 10px;
   }
   .tbl-sheen-index
   {
   position: relative;
   /*bottom: 10px;*/
   }
</style>
<div class="tbl-sheen-index">
   <?php Pjax::begin(['timeout' => 5000,'id'=>'sheenpjax']);?>
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
   <script src="jquery.js"></script> 
   <?php 
      Modal::begin([
       'header'=>'<div class="titleSheen"></div>',
       'id'=>'modalsheen',
       'size'=>'modal-sl',
       ]);
        echo "<div id='modalContent5'><div style='text-align:center'><img src='uploads/avatar2.gif' height='100' width='100'></div></div>";
        Modal::end();
      
       
      
      echo  Html::button('<img src="uploads/add.png"width="17" height="17"/>', ['value'=>Url::to( Url::base().'/createsheen'), 'class' => 'btn btn-primary pull-right','id'=>'modalSheen']);
      ?>
   <?php  echo Html::button('Sort Sheen', ['value'=>Url::to( Url::base().'/sortsheen'), 'class' => 'btn btn-primary pull-right ','id'=>'modalButtonsortsheen']);?>
   <?php  echo $this->render('_searchsheen', ['model' => $searchModelSheen]); ?>
   <div class="main-table-index-page">
      <?= GridView::widget([
         'dataProvider' => $dataProviderSheen,
         //'filterModel' => $searchModelSheen,
         
         'rowOptions'   => function ($model, $key, $index, $grid) {
            return  [
                         "class"=>"modalButton0007",'value'=>Url::to(Url::base().'/updatesheen?id='.$model->sheen_id)
                        ];
              },
          'behaviors' => [
          [
              'class' => '\dosamigos\grid\behaviors\LoadingBehavior',
              'type' => 'spinner'
          ]
         ],
         'columns' => [
          //['class' => 'yii\grid\SerialColumn'],
         
          //'sheen_id',
          'name',
          ///'created_at',
          //'updated_at',
         
         
         
         
         
          ['class' => 'yii\grid\ActionColumn',
         
         
         
               'header' => 'UPDATE',
               'headerOptions' => ['style' => 'color:#337ab7'],
               'template' => '{update}',
               'buttons' => [
          
         
               'update' => function ($url, $model,$id) {
                           return Html::a('<span class=""><img src="uploads/update.png" width="13" height="13"/></span>', $url, [
                           "class"=>"modalSheen2",'value'=>Url::to(Url::base().'/updatesheen?id='.$model->sheen_id)
                            ]);
                },
          
         
             ],
               'urlCreator' => function ($action, $model, $key, $index) {
          
         
                Url::to(['updatesheen?id='.$model->sheen_id],['id'=>'modal'])   ;         
              //$url ='update?id='.$model->brand_id;
            //return $url;
               
               //return Html::a($url, ['title' => 'view']);
              
              
          
          
         
         }
         
         
         ],
         ],
         ]); ?>
   </div>
   <div class="SheenNoDisplay page-display-showing-foot"></div>
</div>

<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
   --><script>
   $(function()
   {
   //
   $('#modalSheen').click(function(){
     $('#modalsheen').modal('show')
       .find('#modalContent5')
       .load ($(this).attr('value'), function(){
         $('#modalsheen .modal-header').remove();
       });
   })
   });
   
   
   $(function()
   {
   //
   $('.modalSheen2').click(function(){
     $('#modalsheen').modal('show')
       .find('#modalContent5')
       .load ($(this).attr('value'));
   })
   });
   
   $(function()
   {
   //
   $('.modalButton0007').click(function(){
       $('#modalsheen').modal('show')
       .find('#modalContent5')
       .load ($(this).attr('value'), function(){
   
         var shnn = $('#shnname').val();
         $('#modalsheen .modal-header').remove();
       });
   })
   
       $('#modalButtonsortsheen').click(function(){
          $('#modalsheen .modal-header').empty();
          $('#modalsheen .modal-header').remove()
          $('#modalsheen').modal('show')
          .find('#modalContent5')
          .load ($(this).attr('value'));
       });
   });
   
   $(function() { //equivalent to document.ready
   $(".form-control").focus();
   });
   
   
   $('.form-control').focus(function(){
   var that = this;
   setTimeout(function(){ that.selectionStart = that.selectionEnd = 10000; }, 0);
   });
   
   $('#tblsheensearch-name').bind('keyup', function() { 
     $('#tblsheensearch-name').delay(200).submit();
   });
   
   
   
   
   
</script>
<script>
   var name = $('.summary').html();
   $('.summary').remove();
   $('.SheenNoDisplay').html(name);
   if($('.SheenNoDisplay').text()=='undefined')
     {
        $('.SheenNoDisplay').hide();
     }
</script>
<?php $this->registerJs('jQuery("#w2").on("keyup", "input", function(){
   jQuery(this).change();
   $("#modalsheen").remove();
   //OR $.pjax.reload({container:\'#w2\'});
   });',
   yii\web\View::POS_READY);
   
   ?> 
<?php Pjax::end();?>