<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TblSpecialItems */

//$this->title = 'Update Tbl Special Items: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Tbl Special Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->item_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<input type="hidden" id="splitmname" value="<?php echo $model->name;?>">
<div class="tbl-special-items-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_formspecialupdate', [
        'model' => $model,
        'id' =>$id,
    ]) ?>

</div>
