<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\tblPrepLevel */

//$this->title = 'Update Tbl Prep Level: ' . $model->prep_id;
// $this->params['breadcrumbs'][] = ['label' => 'Tbl Prep Levels', 'url' => ['index']];
// $this->params['breadcrumbs'][] = ['label' => $model->prep_id, 'url' => ['view', 'id' => $model->prep_id]];
// $this->params['breadcrumbs'][] = 'Update';
?>

<input type="hidden" id="prepname" value="<?php echo $model->prep_level;?>">
<div class="tbl-prep-level-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('formprepupdate', [
        'model' => $model,
        'id'=>$id,
    ]) ?>

</div>
