<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Tblroomtypes */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
h1{
  display:none;
}
</style>
<div class="tblroomtypes-form form-inner-head">

    <?php $form = ActiveForm::begin(['validationUrl' => Url::toRoute(['validateroomupdate?id='.$id]),'enableAjaxValidation' => true, 'id' => 'myid8']); ?>
     <div class = "modal-body-div-add roomUpdateClass">
     <h2><?php echo $model->name;?></h2>
         <?= Html::submitButton('Save', ['class' => 'btn btn-success','id'=>'closeroom']) ?>
      <button type="button" class="close pull-right glyphicon glyphicon-remove" data-dismiss="modal" aria-hidden="true">×</button>
    </div>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>


        <?= $form->field($model, 'type_id')->radioList(array('1'=>'interior','2'=>'exterior','3'=>'both')); ?>





     <?= $form->field($model, 'multiples')->radioList(array('0'=>'Onlyone','1'=>'Number')); ?>


    <?php ActiveForm::end(); ?>

</div>
<script>
$('body').on('beforeSubmit', '#myid8', function (e) {
     var pathname = window.location.href;  

     var form = $(this);
     $.ajax({
          url: form.attr('action'),
          type: 'post',
          data: form.serialize(),
          success: function (response) {

               console.log(response);              

               if(response == 1){
                
                $("#modalroom").modal('hide');
                $(document.body).removeClass('modal-open');
         $('.modal-backdrop').remove();
                 $.pjax.reload('#roompjax' , {timeout : false});
               }
          },
         
     });
      e.stopImmediatePropagation();
     return false;
});






</script>
