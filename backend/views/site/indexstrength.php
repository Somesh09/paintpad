<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\widgets\Pjax;
use dosamigos\grid\GridView;


/* @var $this yii\web\View */
/* @var $searchModel app\models\TblStrengthsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


?>

<div class="tbl-strengths-index">

    
<?php Pjax::begin(['timeout' => 5000,'id'=>'strengthpjax']



);?>
<?php
  echo  Html::button('<img src="uploads/add.png"width="17" height="17"/>', ['value'=>Url::to( Url::base().'/createstrength'),'class'=>'btn btn-primary pull-right' ,'id'=>'modalButtonstrength']);
?>
<?php

  Modal::begin([
    'header'=>'',
    'id'=>'modalstrength',
    'size'=>'modal-sl',
  ]);
    echo "<div id='modalContentstrength'><div style='text-align:center'><img src='uploads/avatar2.gif' height='100' width='100'></div></div>";
    Modal::end();
?>


  <?php  echo $this->render('_searchstrength', ['model' => $searchModelstrength]); ?>
 <div class="main-table-index-page">
    <?= GridView::widget([
        //'id'=> 'no-js-grid',
        
        'dataProvider' => $dataProviderstrength,
          'rowOptions'   => function ($model, $key, $index, $grid) {
              return  [
                           "class"=>"modalButtonstrengthupdate2",'value'=>Url::to(Url::base().'/updatestrength?id='.$model->strength_id)
                          ];
                },
        //'filterModel' => $searchModelstrength,
        'behaviors' => [
            [
                'class' => '\dosamigos\grid\behaviors\LoadingBehavior',
                'type' => 'spinner'
            ]
        ],
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            //'strength_id',
            'name',
            //'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn',
                         'header' => 'UPDATE',
               'headerOptions' => ['style' => 'color:#337ab7'],
               'template' => '{update}',
               'buttons' => [
            

                'update' => function ($url, $model,$id) {
                            return Html::a('<span class=""><img src="uploads/update.png" width="13" height="13"/></span>', $url, [
                           "class"=>"modalButtonstrengthupdate",'value'=>Url::to(Url::base().'/updatestrength?id='.$model->strength_id)
                 ]);
             },
            

         ],
            'urlCreator' => function ($action, $model, $key, $index) {
            

             Url::to(['updatestrength?id='.$model->strength_id],['id'=>'modal'])   ;         
                //$url ='update?id='.$model->brand_id;
              //return $url;
                 
                 //return Html::a($url, ['title' => 'view']);
                

               }

           ],

        ],
    ]); 
   
    ?>
  </div>
    <div class="tintPageDisplay page-display-showing-foot"></div>
</div>

<script>
 
$(function()
{
  //
  $('#modalButtonstrength').click(function(){
    $('#modalstrength').modal('show')
      .find('#modalContentstrength')
      .load ($(this).attr('value'), function(){
        // $('#modalstrength .modal-header').remove();
      });
  });
});


$(function()
{
  //
  $('.modalButtonstrengthupdate').click(function(){
    $('#modalstrength').modal('show')
      .find('#modalContentstrength')
      .load ($(this).attr('value'));
  });
});


$(function()
{
  //
  $('.modalButtonstrengthupdate2').click(function(){
    $('#modalstrength').modal('show')
      .find('#modalContentstrength')
      .load ($(this).attr('value'), function(){

        var strr = $('#strrname').val();
        // $('#modalstrength .modal-header').remove();

      });
  });
});


$(function() { //equivalent to document.ready
  $(".form-control").focus();
});


$('.form-control').focus(function(){
  var that = this;
  setTimeout(function(){ that.selectionStart = that.selectionEnd = 10000; }, 0);
});

  

$('#tblstrengthssearch-name').bind('keyup', function() { 
    $('#tblstrengthssearch-name').delay(200).submit();
});


</script>

<?php $this->registerJs('jQuery("#w8").on("keyup", "input", function(){
        jQuery(this).change();
         $("#modalstrength").remove();
        //OR $.pjax.reload({container:\'#w8\'});
});',
yii\web\View::POS_READY);
?> 
<script>
var name = $('.summary').html();
$('.summary').remove();
$('.tintPageDisplay').html(name);
if($('.tintPageDisplay').text()=='undefined')
{
 $('.tintPageDisplay').hide();
}
</script>
<?php Pjax::end();?>


