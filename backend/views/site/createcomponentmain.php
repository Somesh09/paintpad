<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TblComponents */

//$this->title = 'Create Tbl Components';

?>
<div class="tbl-components-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_formcomponentmain', [
        'model' => $model,
        'model2'=>$model2,
        'model3'=>$model3,
        'model4'=>$model4,
    ]) ?>

</div>
