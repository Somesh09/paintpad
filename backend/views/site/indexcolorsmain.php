<?php

use yii\helpers\Html;
use dosamigos\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel app\models\TblColorsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


?>
<style>
  .sp-container
  {
    display:none;
  }
  </style>
 <?php Pjax::begin(['timeout' => 5000,'id'=>'colormainpjax']);?>
    <div class="tbl-colors-index">

     <?php

       Modal::begin([
          'header'=>'',
          'id'=>'modalcolorsmain',
          'size'=>'modal-sl',
       ]);
       echo "<div id='modalcolorsmain3'><div style='text-align:center'><img src='uploads/avatar2.gif' height='100' width='100'></div></div>";
      Modal::end();
     ?>


      <?php   echo  Html::button('<img src="uploads/add.png"width="17" height="17"/>', ['value'=>Url::to( Url::base().'/createcolorsmain'),'class'=>'btn btn-primary pull-right' ,'id'=>'modalButtoncolorsmainmain1']);?>
    
<?php  echo $this->render('_searchcolorsmain', ['model' => $searchModelcolorsmain]); ?>
<div class="main-table-index-page">
    <?= GridView::widget([
        'dataProvider' => $dataProvidercolorsmain,
        //'filterModel' => $searchModelcolorsmain,
         'rowOptions'   => function ($model, $key, $index, $grid) {
              return  [
                           "class"=>"modalButtoncolorsmain3",'value'=>Url::to(Url::base().'/updatecolorsmain?id='.$model->color_id)
                          ];
                },
         'behaviors' => [
            [
                'class' => '\dosamigos\grid\behaviors\LoadingBehavior',
                'type' => 'spinner'
            ]
        ],
        'columns' => [
         
            'name',
            [
                'attribute' => 'hex',
                'label' => 'Color',
                'format' => 'raw',
               
                'value' => function ($model) {     
                    return "<div style='height:50px;width:50px;background-color:$model->hex;'></div>"  ;
                },
            ],
            //'tag.name',
            [
                'attribute' => 'Tag',
                'label' => 'Tag',
                'format' => 'raw', 
                //'value' => 'tag.name',               
                'value' => function($model){
                              if($model->tag_id=='1'){
                                return '-';
                              } else{
                                //echo "<pre>"; print_r($model['tag']);                    
                                return $model['tag']['name'];
                              }
                          }                
            ],

            //'number',
            //'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn',


                                 'header' => 'UPDATE',
                   'headerOptions' => ['style' => 'color:#337ab7'],
                   'template' => '{update}',
                   'buttons' => [
                

                   'update' => function ($url, $model,$id) {
                                return Html::a('<span class=""><img src="uploads/update.png" width="13" height="13"/></span>', $url, [
                               "class"=>"modalButtoncolorsmain2",'value'=>Url::to(Url::base().'/updatecolorsmain?id='.$model->color_id)
                    ]);
                },
                

              ],
               'urlCreator' => function ($action, $model, $key, $index) {
                

                Url::to(['update?id='.$model->color_id],['id'=>'modal'])   ;         
                    //$url ='update?id='.$model->brand_id;
                  //return $url;
                     
                     //return Html::a($url, ['title' => 'view']);
                    
                    }
                
                

             




            ],
        ],
    ]); ?>
  </div>
    <div class="colorMainPage page-display-showing-foot"></div>
</div>
<script>
    $(function() { //equivalent to document.ready
  $("#tblcolorssearch-name").focus();
});



$('#tblcolorssearch-name').focus(function(){
  var that = this;
  setTimeout(function(){ that.selectionStart = that.selectionEnd = 10000; }, 0);
});

$('#tblcolorssearch-hex').focus(function(){
  var that = this;
  setTimeout(function(){ that.selectionStart = that.selectionEnd = 10000; }, 0);
});

$('#tblcolorssearch-name').bind('keyup', function() { 
    $('#tblcolorssearch-name').delay(200).submit();
});
$('#tblcolorssearch-tag_id').bind('change', function() { 
    $('#tblcolorssearch-tag_id').delay(200).submit();
});



 $(function()
{
  //
  $('#modalButtoncolorsmainmain1').click(function(){
    $('#modalcolorsmain').modal('show')
      .find('#modalcolorsmain3')
      .load ($(this).attr('value'), function(){
        $('#modalcolorsmain .modal-header').remove();
      });
  });
});


  $(function()
{
  //
  $('.modalButtoncolorsmain2').click(function(){
    $('#modalcolorsmain').modal('show')
      .find('#modalcolorsmain3')
      .load ($(this).attr('value'));
  });
});

  $(function()
{
  //
  $('.modalButtoncolorsmain3').click(function(){
    $('#modalcolorsmain').modal('show')
      .find('#modalcolorsmain3')
      .load ($(this).attr('value'),function(){

        var clrr = $('#clrrname').val();
        $('#modalcolorsmain .modal-header').remove();

      });
  });
});
 </script>
<script>
var name = $('.summary').html();
$('.summary').remove();
$('.colorMainPage').html(name);
if($('.colorMainPage').text()=='undefined')
{
   $('.colorMainPage').hide();
}
</script>
<?php Pjax::end();?>
