<?php
// echo $tid;
// echo "<br>".$cid;
// echo "<pre>";
// print_r($model);
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\models\TblSheen;
use wbraganca\dynamicform\DynamicFormWidget;
use app\models\TblProducts;
?>

<div class="person-form">

    <?php $form = ActiveForm::begin(['id' => 'dynamic-form2']); ?>


    <div class="padding-v-md">
        <div class="line line-dashed"></div>
    </div>

    <?php DynamicFormWidget::begin([
        'widgetContainer' => 'dynamicform_wrapper',
        'widgetBody' => '.container-items1',
        'widgetItem' => '.house-item1',
        'limit' => 400,
        'min' => 1,
        'insertButton' => '.add-house2',
        'deleteButton' => '.remove-house2',
        'model' => $model1[0],
        'formId' => 'dynamic-form2',
        'formFields' => [
            'sheen_id',
        ],
    ]); ?>
    <table class="table table-bordered table-striped">
        <thead>
            <tr>
                
                <th style="width: 180px;">Sheen_Id</th>
                <th style="width: 90px;">Products</th>
                <th class="text-center" style="width: 10px;">
                    <button type="button" class="add-house2 btn btn-success btn-xs"><span class="fa fa-plus"></span></button>
                </th>
            </tr>
        </thead>
        <tbody class="container-items1">
        <?php foreach ($model1 as $indexHouse => $model): ?>
            <tr class="house-item1">
                <td class="vcenter">
                    <?php
                        // necessary for update action.
                        if (! $model->isNewRecord) {
                            echo Html::activeHiddenInput($model, "[{$indexHouse}]id");
                        }
                    ?>
                   
          <?= $form->field($model, "[{$indexHouse}]sheen_id")->label(false)->dropDownList(ArrayHelper::map(TblSheen::find()->all(),'sheen_id','name'),['prompt'=>'Select Sheen']) ?>
                
                </td>
                <td>
                    <?= $form->field($model, "[{$indexHouse}]product_id")->label(false)->dropDownList(ArrayHelper::map(TblProducts::find()->all(),'product_id','name'),['prompt'=>'Select Products']) ?>
                  
                </td>
                <td class="text-center vcenter" style="width: 190px; verti">
                    <button type="button" class="remove-house2 btn btn-danger btn-xs"><span class="fa fa-minus"></span></button>
                </td>
            </tr>
         <?php endforeach; ?>
        </tbody>
    </table>
    <?php DynamicFormWidget::end(); ?>
    
    <div class="form-group">
        <?= Html::submitButton($modelPerson->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>