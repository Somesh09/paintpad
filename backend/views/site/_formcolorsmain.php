<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\color\ColorInput;
use app\models\TblColorTags;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\models\TblColors */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
h1{
  display:none;
}
</style>
<div class="tbl-colors-form form-inner-head">

   <?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data'],'validationUrl' => Url::toRoute(['validatecolorsmain']),'enableAjaxValidation' => true, 'id' => 'myid23',]); ?>
   <div class = "modal-body-div-add">
     <h2>Create Color</h2>
           <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
      <button type="button" class="close glyphicon glyphicon-remove" data-dismiss="modal" aria-hidden="true">×</button>
    </div>

  

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'hex')->widget(ColorInput::classname(), [
    'options' => ['placeholder' => 'Select color ...'],
]); ?>

   <?= $form->field($model,'tag_id')->dropDownList(ArrayHelper::map(TblColorTags::find()->all(),'tag_id','name'),['prompt'=>'Select Component Group']) ?>

    

  

    <?php ActiveForm::end(); ?>

</div>
<script>
$('body').on('beforeSubmit', '#myid23', function (e) {
        var pathname = window.location.href;

     var form = $(this);
    
     $.ajax({
          url: form.attr('action'),
          type: 'post',
          data: form.serialize(),
          success: function (response) {
              if(response == 1){
                
                  $("#modalcolorsmain").modal('hide');
                  $(document.body).removeClass('modal-open');
         $('.modal-backdrop').remove();
                   $.pjax.reload('#colormainpjax' , {timeout : false});
               }
          },
         
     });
      e.stopImmediatePropagation();
      return false;
});
</script>
