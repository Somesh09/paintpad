<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\TblComponentGroups */
/* @var $form yii\widgets\ActiveForm */
?>
<style type="text/css">
  .form-group.field-tblcomponentgroups-enabled {
    float: right;
    width: 89%;
}
h1{
  display:none;
}
.imageClass
{
  display: none;
}
.main-copo-01 div#tblcomponentgroups-type_id label {
    margin-top: 0;
}
.main-copo-01 div#tblcomponentgroups-type_id label input[type="radio"] {
    margin-bottom: 0;
}
</style>
<div class="tbl-component-groups-form form-inner-head">

    <?php $form = ActiveForm::begin(['validationUrl' => Url::toRoute(['validatecomponentupdate?id='.$id]),'enableAjaxValidation' => true, 'id' => 'myid6']); ?>
    <div class = "modal-body-div-add">
      <h2><?php echo $model->name;?></h2>
        <?= Html::submitButton('Save', ['class' => 'btn btn-success','id'=>'closecomponent']) ?>
      <button type="button" class="close glyphicon glyphicon-remove" data-dismiss="modal" aria-hidden="true">×</button>
    </div>




    <div class="main-div-upimgdiv" style="padding-top: 0;">
    
	
	<div class="upImgDiv main-copo-01">
	<h2 style="
    font-size: 15px;
    padding-left: 15px;
    font-weight: bold;
    background: #eee;
    padding-top: 13px;
    padding-bottom: 13px;
    border-bottom: 1px solid #ddd;
    width: 100%;
"> Component </h2>
	
	
	<?= $form->field($model, 'name')->textInput(['maxlength' => true])->label('Name') ?>
    <?= $form->field($model, 'type_id')->radioList(array('1'=>'Interior','2'=>'Exterior','3'=>'Both')); ?>
	<?= $form->field($model, 'enabled')->radioList(array('1'=>'YES','0'=>'NO')); ?>
	
	
	</div>
 
 
 
   <div  class="bttmDiv main-copo-02">  
   
	<h2 style="
    font-size: 15px;
    padding-left: 15px;
    font-weight: bold;
    background: #eee;
    border-bottom: 1px solid #ddd;
    margin-top: 8px;
    padding-top: 13px;
    padding-bottom: 13px;
    width: 100%;
    border-top: 1px solid #ddd;
">Tool Tips</h2>
   
    <div class="imgg_div">
		
	 <?php
      if( $model->tt_image == 'null' || $model->tt_image == null ){
          echo ' <img id="blah2" src="uploads/no_image.png"  style="max-width: 100%"/>';
      }
      else
      {
        echo '<img id="blah2" src="uploads/'.$model->tt_image.'"  />';
      }
    ?>
	  <!-- <img id="blah2" src="uploads/no_image.png" alt="your image" style="max-width: 100%"/> -->
      <span class="opnImg"><img id="blah21" src="uploads/edit.png" alt="your image" /></span>  
     <?= $form->field($model,'tt_image',['inputOptions'=>['name' => 'image','id'=>'imgInp']])->fileinput()->label(false);?>  
      <!--<?//= $form->field($model,'tt_image',['inputOptions'=>['name' => 'image','id'=>'imgInp']])->fileinput()->label(false);?> 
        <?php
       // $imgg = $model->tt_image;

      //  if( $model->tt_image == 'null' || $model->tt_image == null ){
        ?>
          <img id="blah2" src="uploads/no_image.png" alt="your image" style="max-width: 100%" />
        <?php
       // }else{
        ?>
          <img id="blah2" src="uploads/<?php// echo $model->tt_image?>" alt="your image" />
        <?php
       // }
        ?>  <?//= $form->field($model,'tt_image',['inputOptions'=>['name' => 'image','id'=>'imgInp','class'=>'imageClass']])->fileinput()->label(false);?>   
          <span class="opnImg"><img id="blah21" src="uploads/edit.png" alt="your image" /></span>-->  
	</div>
       
		<div class="ttClass">   
			<?= $form->field($model, 'tt_url')->textInput(['maxlength' => true]) ?>
			<?= $form->field($model, 'tt_text')->textarea(['maxlength' => true]) ?>
		</div>
   
   </div>
   
   
  </div>
  
  
  
  
  
  
  
    <?php ActiveForm::end(); ?>

</div>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
<script>
$('body').on('submit', '#myid6', function (e) {
    e.preventDefault();
        var formAction = $(this).attr("action");
        var pathname = window.location.href;
        var formData = new FormData($('#myid6')[0]);
        var form = $(this);
     $.ajax({
          url: form.attr('action'),
          type: 'post',
          data: formData,
          processData: false,
          contentType: false,
          cache:false,
          success: function (response) {

               console.log(response);              

               if(response == 1){
                  //$('#myid')[0].reset();
                 //console.log('here11');
                  $("#modalcomponent11").modal('hide');
                  $(document.body).removeClass('modal-open');
         $('.modal-backdrop').remove();
                  $.pjax.reload('#componentpjax' , {timeout : false});
               }
          },
         
     });
      e.stopImmediatePropagation();
     return false;
});

function readURL(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {

      $("img").show();
      $('#blah2').attr('src', e.target.result);
      //$("#blah3").hide();
      
    }
    reader.readAsDataURL(input.files[0]);
  }

}

$("#imgInp").change(function() {
  readURL(this);
});
</script>
<script>
$('.opnImg').click(function(e){
// e.preventdefault();
$('#imgInp').click()
})
</script>
