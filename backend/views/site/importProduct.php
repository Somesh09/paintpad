<?php
namespace backend\controllers;

use Yii;
use yii\web\Response;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
//use common\models\LoginForm;
// use app\models\TblCompany;
use yii\helpers\Url;
use app\models\TblCompanySearch;
//use common\models\LoginForm;
use common\models\SignupForm;
use common\models\User;
use app\models\TblUsers;
use app\models\TblUsersDetails;
use app\models\TblContacts;
use app\models\TblAddress;
use app\models\TblQuotes;
use app\models\TblCommunications;
use app\models\TblAppointments;
use app\models\TblAppointmentType;
use app\models\TblMembers;
use app\models\TblInvoices;
use app\models\TblBrands;
use app\models\TblTiers;
use app\models\TblStrengths;
use app\models\TblSheen;
use app\models\TblColorTags;
use app\models\TblColors;
use app\models\TblComponents;
use app\models\TblComponentGroups;
use app\models\TblComponentType;
use app\models\TblSpecialItems;
use app\models\TblRoomTypes;
use app\models\TblNotes;
use app\models\TblBrandPref;
use app\models\TblPaintDefaults;
use app\models\TblCompanyStock;

use app\models\TblRooms;
use app\models\TblRoomPref;
use app\models\TblRoomCompPref;
use app\models\TblRoomDimensions;
use app\models\TblRoomSpecialItems;
use app\models\TblRoomNotes;


use app\models\TblProductStock;

use app\models\TblPaymentSchedule;
use app\models\TblSummary;
use app\models\TblQuoteSpecialItems;

use app\models\TblUsersSettings;
use app\models\TblPrepLevel;

use app\models\TblPaymentMethod;
use app\models\TblPaymentOptions;

use app\models\TblRoomBrandPref;
use app\models\TblRoomPaintDefaults;

use app\models\TblQuoteStatus;
use app\models\TblExtQuoteComponent;

use app\models\TblAreaCompPref;

use app\models\TblCompany;

use app\models\TblUserRoles;

use app\models\TblCompanyEmailSettings;
use app\models\TblEmailSettingsDoc;
use app\models\TblCompanyDocuments;

use app\models\TblCompanyPrepLevel;


            $company_id = $_POST['company'];

            $csvMimes = array('application/x-csv', 'text/x-csv', 'text/csv', 'application/csv');
        
        // Validate whether selected file is a CSV file
        // if(!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'], $csvMimes)){
            
            // If the file is uploaded
            if(is_uploaded_file($_FILES['file']['tmp_name'])){
                
                // Open uploaded CSV file with read-only mode
                $csvFile = fopen($_FILES['file']['tmp_name'], 'r');
                
                // Skip the first line
                print_r(fgetcsv($csvFile));die;
                while(($line = fgetcsv($csvFile)) !== FALSE){
                
                    // Get row data
                    if($company_id != '0'){
                        

                        $priceData = TblCompanyStock::find()->where(['company_id' => $company_id, 'stock_id'=>$line[4]])->one();
                        if(count($priceData) > 0){
                            // print_r($priceData);die;
                            $priceData->company_stock_id = $priceData->company_stock_id;
                            $priceData->price = $line[9];
                        }else{
                            $priceData = new TblCompanyStock();
                            $priceData->stock_id = $line[4];
                            $priceData->price = $line[9];
                            $priceData->company_id = $company_id;
                        }
                        // echo "<pre>";print_r($priceData);
                        $priceData->save();
                        
                    }else{
                        if(!empty($line[4]) || $line[4] != '0'){
                            TblProductStock::updateAll(['price' => $line[9]], 'product_stock_id ="'. $line[4] .'"' );
                        }

                    }
                    
                    
                }
                // Close opened CSV file
                fclose($csvFile);
                // die;
                return true;
                
            }else{

                return json_encode('Invalid File');
            }

    