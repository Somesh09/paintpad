<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TblStrengths */

//$this->title = 'Create Tbl Strengths';
$this->params['breadcrumbs'][] = ['label' => 'Tbl Strengths', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-strengths-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_formstrength', [
        'model' => $model,
    ]) ?>

</div>
