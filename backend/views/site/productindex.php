<?php
   use backend\assets\AppAsset;
   use yii\helpers\Html;
   use yii\bootstrap\Nav;
   use yii\bootstrap\NavBar;
   use yii\widgets\Breadcrumbs;
   use common\widgets\Alert;
   use yii\widgets\Pjax;
   use yii\helpers\Url;
   //use yii\grid\GridView;
   use yii\bootstrap\Modal;
   use yii\helpers\ArrayHelper;
   use yii\widgets\ActiveForm;
   use app\models\TblBrands;
   use dosamigos\grid\GridView;
   use app\models\TblCompany;
   /* @var $this yii\web\View */
   /* @var $searchModel app\models\TblProductsSearch */
   /* @var $dataProvider yii\data\ActiveDataProvider */
   
   // $this->title = 'Products';
   // $this->params['breadcrumbs'][] = $this->title;
   $companyss = TblBrands::find('brand_id,name')->all();
   
   ?>
<style type="text/css">
   .titlee
   {
   font-size: 21px;
   }
   .imgg_div img {
   width: 100%;
   position: absolute;
   left: 50%;
   top: 50%;
   transform: translate(-50%, -50%);
   }
   .imgg_div {
   float: left;
   width: 170px;
   height: 190px;
   position: relative;
   border: 1px solid #c2c2c2;
   border-radius: 4px;
   box-shadow: 0 0 2px #e3e3e3;
   -webkit-box-shadow: 0 0 2px #e3e3e3;
   margin-top: 7px;
   }
   #myid222 .form-group {
   float: left;
   width: 100%;
   }
   .imgg_div #imgInp {
   height: 100%;
   width: 100%;
   position: absolute;
   top: 0;
   opacity: 0;
   z-index: 9;
   }
   .opnImg {
   position: absolute;
   right: -12px;
   top: -9px;
   border: 1px solid #ccc;
   width: 26px;
   height: 26px;
   text-align: center;
   font-size: 20px;
   border-radius: 30px;
   color: #ccc;
   background: #fff;
   z-index: 999;
   line-height: 20px;
   }
   .opnImg img {
   width: 16px;
   position: initial;
   transform: initial;
   }
   .savve img {
   width: 21px;
   cursor: pointer;
   }
   .savve {
   float: right;
   }
   #blah22{
   filter: sepia(100%) hue-rotate(190deg) saturate(500%);
   }
</style>
<div class="tbl-products-index">
   <?php Pjax::begin(['timeout' => 5000,'id'=>'productajax']);?>
   <?php echo  Html::button('<img src="uploads/add.png" width="20" height="20"/>', ['value'=>Url::to( Url::base().'/createproduct'),'class'=>'btn btn-primary pull-right' ,'id'=>'modalButton7']); ?>
   <?php echo  Html::button('<img src="uploads/add.png" width="20" height="20"/> Import', ['class'=>'btn btn-primary pull-right' ,'id'=>'modalButtonImport']); ?>
   <?php 
      Modal::begin([
          'header' => '<h2>Import</h2><button type="button" class="close ssqq" data-dismiss="modal" aria-hidden="true">×</button>',
          'id'=>'modalimport',
          'size'=>'modal-lg',
      ]);
      echo '<div id="modalContent1"><div style="text-align:center" id="removeeeee"><img src="uploads/avatar2.gif" height="100" width="100"></div><form id="uploadcsv" method="POST" enctype="multipart/form-data"><select name="company_id" class="form-control">';
        foreach ($companyss as $key => $company){
          echo "<option value='".$company['brand_id']."'>".$company['name']."</option>";
        }
      echo '</select> <input type="file" accept=".csv" name="file" id="choosefile"><span style="font-size: :20px" class="btn btn-large btn-primary" id="uploadStart">Upload</span><button class="download-csv" type="button" download> Download Sample</button> </form></div>';
      
      // echo "";
      
      Modal::end();
      ?>
   <?php  echo $this->render('_searchproduct', ['model' => $searchModelproduct]); ?>
   <?php
      Modal::begin([
         'header'=>'',
         'id'=>'modal2',
         'size'=>'modal-lg',
      ]);
      echo "<div id='modalContent1'><div style='text-align:center'><img src='uploads/avatar2.gif' height='100' width='100'></div></div>";
      Modal::end();
      
      
      $model2 = new TblBrands();
      
      ?>
   <div class="main-table-index-page ">
      <?= GridView::widget([
         'dataProvider' => $dataproviderproduct,
         //'filterModel' => $searchModelproduct,
         //'Pjax'         =>true,
         //'filterSelector' => 'select[name="per-page"]',
         'rowOptions'   => function ($model, $key, $index, $grid) {
               return  [
                            "class"=>"modalButton007",'value'=>Url::to(Url::base().'/updateproduct?id='.$model->product_id)
                       ];
                 },
          'behaviors' => [
                             [
                                 'class' => '\dosamigos\grid\behaviors\LoadingBehavior',
                                 'type' => 'spinner'
                             ]
                         ],
         
         'columns' => [
                               'name',
                           [   'attribute' => 'brand_id',
                               'value' => 'brand.name',
                               //'filter' => ArrayHelper::map($model2::find()->asArray()->all(), 'brand_id', 'name')
         
         
                            ],
         
         
                               'type.name',
                               //'created_at',
                               //'updated_at',
         
         
                             ['class' => 'yii\grid\ActionColumn',
                                'header' => 'UPDATE',
                                'headerOptions' => ['style' => 'color:#337ab7'],
                                'template' => '{update}',
                                'buttons' => [
                             
         
                                                  'update' => function ($url, $model,$id) {
                                                   return Html::a('<span class=""><img src="uploads/update.png" width="13" height="13"/></span>', $url, [
                                                  "class"=>"modalButton3",'value'=>Url::to(Url::base().'/updateproduct?id='.$model->product_id)
                                                       ]);
                                                   },
                                              ],
                              'urlCreator' => function ($action, $model, $key, $index) {
                             
         
                               Url::to(['update?id='.$model->brand_id],['id'=>'modal'])   ;         
                                 //$url ='update?id='.$model->brand_id;
                                 //return $url;
                                  
                                 //return Html::a($url, ['title' => 'view']);
                           
                                      }
         
                             ],
                     ],
         
          ]); ?>
   </div>
   <div class="productPageDisplay"></div>
</div>
<script>
   $(function()
   {
   //
   $('.modalButton3').click(function(){
     $('#modal2').modal('show')
     //$('#modal2').draggable()
       .find('#modalContent1')
       .load ($(this).attr('value'));
   });
   });
   
   
   $(function()
   {
   //
   $('#modalButton7').click(function(){
     $('#modal2').modal('show')
    // $('#modal2').draggable()
       .find('#modalContent1')
       .load ($(this).attr('value'), function(){
         // $('#modal2 .modal-header').remove();
       });
   });
   });
   $(function()
   {
   //
   $('#modalButtonImport').click(function(){
     $('#modalimport').modal('show')
    // $('#modal2').draggable()
       .find('#removeeeee').hide()
   
       // .load ($(this).attr('value'), function(){
       //   $('#modalimport .modal-header').remove();
       // });
   });
   });
   $(function()
   {
   $('.modalButton007').click(function(){
   // <span class="savve"><img id="blah22" src="uploads/tick.png" alt="your image" /></span>///////// this is a save tick button for update product.
     $('#modal2').modal('show')
       .find('#modalContent1')
       .load ($(this).attr('value'), function(){
         console.log("I am fetched-->");
         var pdctt = $('#pdtname').val();
         // $('#modal2 .modal-header').remove();
   
         $('.savve').click(function(){
           console.log('save is clicked');
           $("#myid222").submit();
         })
   
   
       });
   });
   
   })
   
   //
   $(function() { //equivalent to document.ready
   $("#tblproductssearch-name").focus();
   });
   
   
   $('#tblproductssearch-name').focus(function(){
   var that = this;
   setTimeout(function(){ that.selectionStart = that.selectionEnd = 10000; }, 0);
   });
   
   
   
   $('#tblproductssearch-name').bind('keyup', function() { 
     $('#tblproductssearch-name').delay(200).submit();
   });
   
   $('#tblproductssearch-brand_id').bind('change', function() { 
     $('#tblproductssearch-brand_id').delay(200).submit();
   });
   
   
   
   
   
</script>
<?php $this->registerJs('jQuery("#w1").on("keyup", "input", function(){
   jQuery(this).change();
    $("#modal2").remove();
   //OR $.pjax.reload({container:\'#w1\'});
   });',
   yii\web\View::POS_READY);
   ?> 
<script type="text/javascript">
   var name = $('.summary').html();
   $('.summary').remove();
   $('.productPageDisplay').html(name);
   if($('.productPageDisplay').text()=='undefined')
   {
      $('.productPageDisplay').hide();
   }
</script>
<script>
   $('#uploadStart').unbind('click').click(function(){
     // alert('hello');
     console.log($("#choosefile").val());
     if($("#choosefile").val() == '' || $("#choosefile").val() == 'undefined'){
       alert('Choose a .csv file');
     }else{
       $('#uploadcsv').submit();
     }
   })
   $('.download-csv').on('click', function (e) {
     e.preventDefault();
    
     $.ajax({
       type: 'POST',
       url: 'downloadcsv',
       data: {data:1},
       dataType: 'json',
       success: function(response){ 
         // console.log(response)
         window.location = 'download.php';
         
       },
       complete:function () {
         window.location = 'download.php';
        
       }
     });
   });
   
   $(function () {
   // $('.download-csv').on('click',function(){
   //   $(this).attr('href','/sample.csv')
   // })
   $('#uploadcsv').on('submit', function (e) {
     e.preventDefault();
      $('#removeeeee').show()
      $('#uploadcsv').hide()
     
     $.ajax({
       type: 'POST',
       url: 'uploadcsv',
       data: new FormData(this),
       dataType: 'json',
       contentType: false,
       cache: false,
       processData:false,
       beforeSend: function(){
         $('#spinner-border-overaly').show();
         $('#removeeeee').show()
         $('#uploadcsv').hide()
       },
       success: function(response){ 
         $('#spinner-border-overaly').hide();
         $('#removeeeee').hide()
         $('#uploadcsv').show()
         if(response == "No file" || response == "Invalid File"){
           alert('Something went wrong!!');
         }
         else if(response.name != null || response.name != undefined){
           alert(response.name[0]+'Please check CSV');
         }
         else{
           alert('Data Uploaded Successfully');
           $('#modalimport').modal('hide')
           $.pjax.reload('#productajax' , {timeout : false});
         }
         $("#choosefile").val('');
       },
       error:function(response){
         $('#spinner-border-overaly').hide();
         $('#removeeeee').hide()
         $('#uploadcsv').show()
         console.log(response.responseText);
         alert('Something went Wrong.!!!');
         $("#choosefile").val('');
       }
     });
   });
   });
   
   // File type validation
   $("#choosefile").change(function() {
     var file = this.files[0];
     var fileType = file.type;
     var match = ['application/vnd.ms-excel', 'text/csv'];
     if(!((fileType == match[0]) || (fileType == match[1]))){
         alert('Only CSV allowed to upload.');
         $("#choosefile").val('');
         return false;
     }
   });
</script>
<?php Pjax::end();?>