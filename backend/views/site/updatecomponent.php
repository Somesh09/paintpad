<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TblComponentGroups */

//$this->title = 'Update Tbl Component Groups: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Tbl Component Groups', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->group_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<input type="hidden" id="cmppname" value="<?php echo $model->name;?>">
<div class="tbl-component-groups-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_formcomponentupdate', [
        'model' => $model,
        'id'=>$id
    ]) ?>

</div>
