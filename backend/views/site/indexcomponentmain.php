<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\widgets\Pjax;
use dosamigos\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TblComponentsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


?>
<div class="tbl-components-index">


  <?php Pjax::begin(['timeout' => 5000,'id'=>'componentmainpjax']);?>

   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="jquery.js"></script>   

      <?php

       Modal::begin([
          'header'=>'',
          'id'=>'modalcomponentmain',
          'size'=>'modal-sl',
       ]);
       echo "<div id='modalcomponent'><div style='text-align:center'><img src='uploads/avatar2.gif' height='100' width='100'></div></div>";
      Modal::end();
    
     ?>


<?php   echo  Html::button('<img src="uploads/add.png"width="17" height="17"/>', ['value'=>Url::to( Url::base().'/createcomponentmain'),'class'=>'btn btn-primary pull-right' ,'id'=>'modalButtoncomponentmain1']);?>

<?php  echo Html::button('Sort Component', ['value'=>Url::to( Url::base().'/sortcomponent'), 'class' => 'btn btn-primary pull-right','id'=>'modalButtonsortcomponent']);?>
<div class="main-table-index-page">
<?php  echo $this->render('_searchcomponentmain', ['model' => $searchModelcomponents]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvidercomponents,
        //'filterModel' => $searchModelcomponents,
        'rowOptions'   => function ($model, $key, $index, $grid) {
              return  [
                           "class"=>"modalButtoncomponentmain3",'value'=>Url::to(Url::base().'/updatecomponentmain?id='.$model->comp_id)
                          ];
                },
        'behaviors' => [
            [
                'class' => '\dosamigos\grid\behaviors\LoadingBehavior',
                'type' => 'spinner'
            ]
        ],
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            //'comp_id',
            'name',
            'group.name',
            //'price_method_id',
            //'calc_method_id',
            //'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn',


                                 'header' => 'UPDATE',
                   'headerOptions' => ['style' => 'color:#337ab7'],
                   'template' => '{update}',
                   'buttons' => [
                

                   'update' => function ($url, $model,$id) {
                                return Html::a('<span class=""><img src="uploads/update.png" width="13" height="13"/></span>', $url, [
                               "class"=>"modalButtoncomponentmain2",'value'=>Url::to(Url::base().'/updatecomponentmain?id='.$model->comp_id)
                    ]);
                },
                

              ],
               'urlCreator' => function ($action, $model, $key, $index) {
                

                Url::to(['update?id='.$model->comp_id],['id'=>'modal'])   ;         
                    //$url ='update?id='.$model->brand_id;
                  //return $url;
                     
                     //return Html::a($url, ['title' => 'view']);
                    
                    }
                
                
            ],
        ],
    ]); ?>
  </div>
    <div class="componentPAgeDisplay"></div>
</div> 
<script>
  $(function()
{
  //
  $('#modalButtoncomponentmain1').click(function(){
    $('#modalcomponentmain').modal('show')
      .find('#modalcomponent')
      .load ($(this).attr('value'), function(){
        // $('#modalcomponentmain .modal-header').remove();
      });
  });
});


  $(function()
{
  //
  $('.modalButtoncomponentmain2').click(function(){
    $('#modalcomponentmain').modal('show')
      .find('#modalcomponent')
      .load ($(this).attr('value'));
  });
});

  $(function()
{
  //
  $('.modalButtoncomponentmain3').click(function(){
    $('#modalcomponentmain').modal('show')
      .find('#modalcomponent')
      .load ($(this).attr('value'), function() {
        console.log("I am fetched-->");
        var typee = $('#typeename').val();
        $('#modalcomponentmain .modal-header').remove();
      });
  });
});

  $(function()
{
  //
  $('#modalButtonsortcomponent').click(function(){
    $('#modalcomponentmain').modal('show')
      .find('#modalcomponent')
      .load ($(this).attr('value'));
  });
});




  
$(function() { //equivalent to document.ready
  $(".form-control").focus();
});


$('.form-control').focus(function(){
  var that = this;
  setTimeout(function(){ that.selectionStart = that.selectionEnd = 10000; }, 0);
});

$('#tblcomponentssearch-name').bind('keyup', function() { 
    $('#tblcomponentssearch-name').delay(200).submit();
});


  </script>
  <?php $this->registerJs('jQuery("#w9").on("keyup", "input", function(){
                jQuery(this).change();
                $("#modalcomponentmain").remove();
                //OR $.pjax.reload({container:\'#w9\'});
        });',
        yii\web\View::POS_READY);
        ?> 
<script>
var name = $('.summary').html();
$('.summary').remove();
$('.componentPAgeDisplay').html(name);
if($('.componentPAgeDisplay').text()=='undefined')
{
 $('.componentPAgeDisplay').hide();
}
 $('#modalcomponentmain .modal-header').remove();
</script>
        
<?php Pjax::end();?>

