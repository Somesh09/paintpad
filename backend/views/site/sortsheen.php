<?php
use yii\widgets\Pjax;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
?>
<!DOCTYPE html>
<html>
<head>
    <style type="text/css">
.sort {
    float: right;
}
.sort img {
    width: 21px;
    cursor: pointer;
}
</style>

</head>
<body>
<div class="tbl-brands-form form-inner-head">
    <div class="modal-body-div-add sortSheenInner">
    <div style="width: 480px;float: left;">
    <h2 class="">Sort Sheen</h2>
    <span align="left" style="color:#000">Hold and drag rows to sort the Sheen</span>
    </div>
    <?= Html::submitButton('Save', ['class' => 'btn btn-success sortSheen','id'=>'blah22']) ?>
    <button type="button" class="close glyphicon glyphicon-remove" data-dismiss="modal" aria-hidden="true">×</button>
    </div>
    <div style="cursor: pointer" class="model-inner-part">
        
        <table class="table table-hover table-condensed table-striped table-bordered">

            <tbody class="row_position2">
            <?php
                foreach($model as $model){
            ?>
                <tr  id="<?php echo $model->sheen_id ?>">
                    <td><?php echo $model->name ?></td>
                </tr>
            <?php 
                } 
            ?>
            </tbody>

        </table>
    </div> <!-- container / end -->
    </div>
</body>
<script type="text/javascript">
    $( ".row_position2" ).sortable({
        delay: 150,
        stop: function() {
            var selectedData = new Array();
            $('.row_position2>tr').each(function() {
                selectedData.push($(this).attr("id"));
            });
            updateOrder(selectedData);
        }
    });


    function updateOrder(data) {
        $.ajax({
            url:"ajaxpro2.php",
            type:'post',
            data:{position:data},
            success:function(){
              $("body").removeClass('modal-open').removeAttr("style");
            }
        })
    }
     $(document).on('click', '.sortSheen', function(){ 
                   $("#modalsheen").modal('hide');
                   $(".modal-backdrop").remove();
                $.pjax.reload('#sheenpjax' , {timeout : false});
                });
</script>
</html>
