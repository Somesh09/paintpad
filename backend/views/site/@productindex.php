<?php

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use yii\widgets\Pjax;
use yii\helpers\Url;
//use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\TblBrands;
use dosamigos\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TblProductsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

// $this->title = 'Products';
// $this->params['breadcrumbs'][] = $this->title;

?>

<style type="text/css">
.titlee
{
  font-size: 21px;
}
.imgg_div img {

    width: 100%;
    position: absolute;
    left: 50%;
    top: 50%;
    transform: translate(-50%, -50%);

}
.imgg_div {
    float: left;
    width: 170px;
    height: 190px;
    position: relative;
    border: 1px solid #c2c2c2;
    border-radius: 4px;
    box-shadow: 0 0 2px #e3e3e3;
    -webkit-box-shadow: 0 0 2px #e3e3e3;
    margin-top: 7px;
}

#myid222 .form-group {

    float: left;
    width: 100%;

}
.imgg_div #imgInp {

    height: 100%;
    width: 100%;
    position: absolute;
    top: 0;
    opacity: 0;
    z-index: 9;

}
.opnImg {

    position: absolute;
    right: -12px;
    top: -9px;
    border: 1px solid #ccc;
    width: 26px;
    height: 26px;
    text-align: center;
    font-size: 20px;
    border-radius: 30px;
    color: #ccc;
    background: #fff;
    z-index: 999;
    line-height: 20px;

}
.opnImg img {

    width: 16px;
    position: initial;
    transform: initial;

}
.savve img {

    width: 21px;
    cursor: pointer;

}
.savve {

    float: right;

}

#blah22{
    filter: sepia(100%) hue-rotate(190deg) saturate(500%);
}

</style>

<div class="tbl-products-index">

    <h1><?= Html::encode($this->title) ?></h1>

    



<?php Pjax::begin(['timeout' => 5000,'id'=>'productajax']);?>

 <?php echo  Html::button('<img src="uploads/add.png" width="20" height="20"/>', ['value'=>Url::to( Url::base().'/createproduct'),'class'=>'btn btn-primary pull-right' ,'id'=>'modalButton7']); ?>


<?php  echo $this->render('_searchproduct', ['model' => $searchModelproduct]); ?>
    <?php

       Modal::begin([
          'header'=>'',
          'id'=>'modal2',
          'size'=>'modal-lg',
       ]);
       echo "<div id='modalContent1'><div style='text-align:center'><img src='uploads/avatar2.gif' height='100' width='100'></div></div>";
       Modal::end();


      $model2 = new TblBrands();

     ?>
     
   




    <?= GridView::widget([
        'dataProvider' => $dataproviderproduct,
        //'filterModel' => $searchModelproduct,
        //'Pjax'         =>true,
        //'filterSelector' => 'select[name="per-page"]',
        'rowOptions'   => function ($model, $key, $index, $grid) {
              return  [
                           "class"=>"modalButton007",'value'=>Url::to(Url::base().'/updateproduct?id='.$model->product_id)
                      ];
                },
         'behaviors' => [
                            [
                                'class' => '\dosamigos\grid\behaviors\LoadingBehavior',
                                'type' => 'spinner'
                            ]
                        ],
        
        'columns' => [
                              'name',
                          [   'attribute' => 'brand_id',
                              'value' => 'brand.name',
                              //'filter' => ArrayHelper::map($model2::find()->asArray()->all(), 'brand_id', 'name')


                           ],
        

                              'type.name',
                              //'created_at',
                              //'updated_at',


                            ['class' => 'yii\grid\ActionColumn',
                               'header' => 'UPDATE',
                               'headerOptions' => ['style' => 'color:#337ab7'],
                               'template' => '{update}',
                               'buttons' => [
                            

                                                 'update' => function ($url, $model,$id) {
                                                  return Html::a('<span class=""><img src="uploads/update.png" width="13" height="13"/></span>', $url, [
                                                 "class"=>"modalButton3",'value'=>Url::to(Url::base().'/updateproduct?id='.$model->product_id)
                                                      ]);
                                                  },
                                             ],
                             'urlCreator' => function ($action, $model, $key, $index) {
                            

                              Url::to(['update?id='.$model->brand_id],['id'=>'modal'])   ;         
                                //$url ='update?id='.$model->brand_id;
                                //return $url;
                                 
                                //return Html::a($url, ['title' => 'view']);
                          
                                     }

                            ],
                    ],

         ]); ?>
    
</div>

<script>
  $(function()
{
  //
  $('.modalButton3').click(function(){
    $('#modal2').modal('show')
      .find('#modalContent1')
      .load ($(this).attr('value'));
  });
});


  $(function()
{
  //
  $('#modalButton7').click(function(){
    $('#modal2').modal('show')
      .find('#modalContent1')
      .load ($(this).attr('value'), function(){
        $('#modal2 .modal-header').html('<b class="titlee">Add New Product</b><button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>');
      });
  });
});
$(function()
{
 $('.modalButton007').click(function(){

    $('#modal2').modal('show')
      .find('#modalContent1')
      .load ($(this).attr('value'), function(){
        console.log("I am fetched-->");
        var pdctt = $('#pdtname').val();
        $('#modal2 .modal-header').html('<b class="titlee">Update '+pdctt+'</b><span class="savve"><img id="blah22" src="uploads/tick.png" alt="your image" /></span>');

        $('.savve').click(function(){
          console.log('save is clicked');
          $("#myid222").submit();
        })


      });
  });

})

//
$(function() { //equivalent to document.ready
  $("#tblproductssearch-name").focus();
});


$('#tblproductssearch-name').focus(function(){
  var that = this;
  setTimeout(function(){ that.selectionStart = that.selectionEnd = 10000; }, 0);
});

  

$('#tblproductssearch-name').bind('keyup', function() { 
    $('#tblproductssearch-name').delay(200).submit();
});

$('#tblproductssearch-brand_id').bind('change', function() { 
    $('#tblproductssearch-brand_id').delay(200).submit();
});





</script>
 <?php $this->registerJs('jQuery("#w1").on("keyup", "input", function(){
                jQuery(this).change();
                 $("#modal2").remove();
                //OR $.pjax.reload({container:\'#w1\'});
        });',
        yii\web\View::POS_READY);
        ?> 
 <?php Pjax::end();?>
        