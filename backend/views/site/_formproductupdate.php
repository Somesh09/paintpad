<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\TblBrands;
use yii\helpers\Url;
use app\models\TblSheen;

/* @var $this yii\web\View */
/* @var $model app\models\TblProducts */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
select {
    -moz-appearance: none;
    -webkit-appearance: none;
    appearance: none;
    text-indent: 0.1;
    text-overflow: '';
}
/*#closeproductupdate {
    display: none;
}*/
</style>

<div  class=" form-inner-head">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'],'validationUrl' => Url::toRoute(['validateproductupdate?id='.$id]), 'id' => 'myid222','enableAjaxValidation' => true]); ?>

     <div class = "modal-body-div-add">
	 <h2><?php echo $model->name;?></h2>
      <?= Html::submitButton('Save', ['class' => 'btn btn-success pull-right saveProductUpdate','id'=>'closeproductupdate']) ?>
	  <button type="button" class="close glyphicon glyphicon-remove" data-dismiss="modal" aria-hidden="true"></button>
	  </div>

	<div class="div-scroll-product">
	<div class="main-div-upimgdiv">
    <div class="upImgDiv">
      <div class="form-group">
      <div class="imgg_div">

       

        <?php

        //echo '<pre>'; print_r($model); exit;

        $imgg = $model->image; 
        //echo $imgg;

        if( $model->image == 'null' || $model->image == null ){
        ?>

          <img id="blah2" src="uploads/no_image.png" alt="your image" />

        <?php
          }else{
        ?>
          <img id="blah2" src="uploads/<?php echo $model->image?>" alt="your image" />
        
        <?php
          }
        ?>

        <?php
        /*if( ($model->image) != 'null' || ($model->image) != null)
        {
          echo 'line44';
        ?>
        <img id="blah3" src="uploads/<?php echo $model->image?>" alt="your image" />
        <?php
        }else{
          echo 'line49';
        ?>
        <img id="blah2" src="uploads/no_image.png" alt="your image" />
        <?php
        }*/
        ?>  
         <span class="opnImg"><img id="blah21" src="uploads/edit.png" alt="your image" /></span>      
        <?= $form->field($model,'image',['inputOptions'=>['name' => 'image','id'=>'imgInp','class'=>'addImageProduct']])->fileinput()->label(false);?>
      </div>
      </div>
    </div> <!-- .upImgDiv -->

    <div class="bttmDiv">
    
      <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

      <?= $form->field($model, 'spread_rate')->textInput() ?> 

      <?= $form->field($model,'brand_id')->dropDownList(ArrayHelper::map(TblBrands::find()->all(),'brand_id','name'),['prompt'=>'Select brand']) ?>

      <?= $form->field($model, 'type_id')->radioList(array('1'=>'Interior','2'=>'Exterior','3'=>'Both')); ?>
     
    </div> <!-- .bttmDiv -->
	
	</div>

	
	
	
	
	
	
	
	
	<div class="one-stoke">
	
				<div class="stoke-main">
				<!-- <h3 align="center">UPDATE STOCK</h3> -->
				<h3 align="center">Stock</h3>
				<a  href="javascript:void(0)" class='clicked btn btn-success pull-right'>Add Stock Item</a>
				</div>

				<div class="stoke-main-table" >
				<table class="table" id="maindiv77">
				  <tr>
					<th>Sheen</th>
					<th>Litre</th>
					<th>Code</th>
					<th>Name</th>
					<th>Price</th>
          <th>&nbsp;</th>
				  </tr>
              <?php
              $i=0;
              foreach($modelsecond as $model2)
              {
                ?>
                 
                  <tr>
                    <td>
                     <?= $form->field($model2,'sheen_id')->dropDownList(ArrayHelper::map(TblSheen::find()->all(),'sheen_id','name'),['class'=>'sheen form-control','name' => 'stock_sheen_update['.$i.'][stock]'])->label(false); ?>
                  <!--  <div class="input-group">
                      <span class="input-group-addon"><img src="http://www.enacteservices.com/paintpad_dev/admin/uploads/dropdown.png" height="8px" width="8px"></span></div> -->
                    </td>
                    <td>
                    <?php //$form->field($model2, '['.$model2->sheen_id.']litres')->textInput(['maxlength' => true,'class'=>'litre'])->label(false); ?>
                    <?= $form->field($model2, 'litres')->textInput(['maxlength' => true,'class'=>'litre form-control','name' => 'stock_sheen_update['.$i.'][litre]']   )->label(false); ?>
                    </td>
                     <td>
                    <?= $form->field($model2, 'code')->textInput(['maxlength' => true,'class'=>'code form-control','name' => 'stock_sheen_update['.$i.'][code]'])->label(false); ?>
                    </td>
                    <td>
                    <?= $form->field($model2, 'name')->textInput(['maxlength' => true,'class'=>'name form-control','name' => 'stock_sheen_update['.$i.'][name]'])->label(false); ?>
                    </td>
                    <td>
                    <?= $form->field($model2, 'price')->textInput(['maxlength' => true,'class'=>'price form-control','name' => 'stock_sheen_update['.$i.'][price]'])->label(false); ?>
                    </td>
                    <td>
                    <button type="button" class="close deleteSavedProduct glyphicon glyphicon-remove" data-id="<?php echo $model2->product_stock_id;?>"></button>
                  
                    </td>
                     
                  </tr>
                 
                <?php
                $i++;
              }
            ?>
				</table>
				   </div>
   
   
				  <div >
				
				  </div>  

				  
		 </div>   
				  
				  
				  

    <?php ActiveForm::end(); ?>
<!-- <img src='uploads/avatar2.gif' height='100' width='100' id="loading" style="display:none"> -->

<div class="loader" style="display:none;"></div>



<div id="choosingdiv" style="width:23%; margin-right:0px; border:1px; " class="form-group field-tblproducts-brand_id required has-success">
 <?= $form->field($modelstock,'sheen_id')->dropDownList(ArrayHelper::map(TblSheen::find()->all(),'sheen_id','name'),['prompt'=>'--Add New Item--','id'=>'dropdown','class'=>'form-control'])->label(false); ?>
</div>
</div>
</div>
<script>


$('.clicked').click(function(){
    showDiv(1);
    $('#dropdown').val('');
})

$('#dropdown').change(function(){
  var id = $(this).val();
  //alert(id);
  showDiv(id);
  $('#dropdown').val('');
})





  function showDiv(id)
  {
    ///console.log(elem);
          //$('#maindiv77').hide();

         var  l = $('#maindiv77').find('tr').length;

          //var id=elem.value;
         
          // alreadySheen=[];
          // $("#maindiv77").children('table').each(function () {
          // alreadySheen.push($(this).find('input').attr('id'));
          // });
          // lastArr = alreadySheen[alreadySheen.length - 1];        
          // for (var i = alreadySheen.length ; i > 0; i--) 
          // {
          // console.log(id);
          // $("#dropdown option[value='"+lastArr+"']").remove();
        
          // }
// <input id="'+id+'" type="text" name="stock_sheen['+id+']"  value="'+name+'" readonly style="width:45%;margin-right:0px; border:1px;"/>
          var name = $("#dropdown option:selected").text();
          //var nameSheen = ""
          

           var fieldHTML = '<tr class="table'+id+'" table_id="'+id+'"><td><select name="stock_sheen['+l+'][stock]" class="pages form-control" value="'+id+'" data-id="'+l+'"><?php foreach($modelstockData as $stock):?><option value="<?php echo $stock->sheen_id?>"><?php echo $stock->name;?></option> <?php endforeach ?></select></td><td><input data-id="'+l+'"  type="text" data="litre" class="litre empty number form-control"  name="stock_sheen['+l+'][litre]" id="litre'+id+'"  ></td><td><input data-id="'+l+'" data="code" class="code  form-control empty" type="text" id="code'+id+'" name="stock_sheen['+l+'][code]"   ></td><td><input data-id='+l+' class="name empty form-control" data="name" type="text" name="stock_sheen['+l+'][name]"  id="name'+id+'"  ></td><td><input data-id='+l+' type="text" name="stock_sheen['+l+'][price]" data="price" class="form-control price empty number"  id="price'+id+'" ></td><td class="deleteform" data-id="'+l+'" readonly ><button type="button" class="close glyphicon glyphicon-remove"></button></td></tr><tr style="display:none;" class="errorTd tdError'+l+'""><td></td><td><span style="color:#a94442;" class="litre'+id+'message"></span></td><td><span style="color:#a94442;" class="code'+id+'message"></span></td><td><span style="color:#a94442;" class="name'+id+'message"></span></td><td><span style="color:#a94442;" class="price'+id+'message"></span></td><td>&nbsp;</td></tr>'; 
          $('#maindiv77').show();
         // var newOption = $('<option value="'+id+'">'+name+'</option>');

          //$('#pages').append(newOption);
          $(".div-scroll-product").animate({ scrollTop: $(document).height() }, 1000);
          console.log(id+""+name);
         

          if(id!="" && name!="Select brand")
          {
            $('#maindiv77').append(fieldHTML);
          }        
          if ($(".pages").length > 0) {
            $(".pages").last().val(id);
          }
         else
          {
           // alert("d");
            $(".pages").val(id);
          }
          

          $('.pages').change(function(){
            var sid = $(this).val();
           var dataStock = $(this).parent().parent().parent().find('.litre').attr("data-id");
            $(this).removeAttr("name");
            $(this).removeAttr("value");
            $(this).attr("name",'stock_sheen['+dataStock+'][stock]')
            $(this).attr("value",sid);
            $(this).parent().parent().parent().find('.litre').removeAttr("name");
            var dataLitre = $(this).parent().parent().parent().find('.litre').attr("data-id");
            $(this).parent().parent().parent().find('.litre').attr("name","stock_sheen["+dataLitre+"][litre]");
            $(this).parent().parent().parent().find('.code').removeAttr("name");
            var dataCode = $(this).parent().parent().parent().find('.code').attr("data-id");
            $(this).parent().parent().parent().find('.code').attr("name","stock_sheen["+dataCode+"][code]");
            $(this).parent().parent().parent().find('.name').removeAttr("name");
            var dataName = $(this).parent().parent().parent().find('.name').attr("data-id");
            $(this).parent().parent().parent().find('.name').attr("name","stock_sheen["+dataName+"][name]");
            $(this).parent().parent().parent().find('.price').removeAttr("name");
            var dataPrice = $(this).parent().parent().parent().find('.price').attr("data-id");
            $(this).parent().parent().parent().find('.price').attr("name","stock_sheen["+dataPrice+"][price]");
             //alert(litre);
           // $(this)
          })
          //$('.modal-body').scrollTop($('.modal-body').prop("scrollHeight"));
  }



 $(document).on('click','.deleteform',function(){
    var id= $(this).parent().parent().parent().attr('table_id');
    //alreadySheen.push(id);
     $(this).closest('tr').remove();
      var data = $(this).attr('data-id');
      $(this).closest('tr').remove();
     $('.tdError'+data).remove();
  });




function readURL(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
     
      $("img").show();
      $('#blah2').attr('src', e.target.result);
      //$("#blah3").hide();
      $("#blah").show();
    }

    reader.readAsDataURL(input.files[0]);
  }

}

$("#imgInp").change(function() {
  readURL(this);
});


// var base_url = window.location.origin + '/' + window.location.pathname.split ('/') [1] + '/'; 
// $('body').on('beforeSubmit', '#myid222', function (e) {
//         var formAction = $(this).attr("action");
//         var pathname = window.location.href;
//         var formData = new FormData($('#myid222')[0]);
//         console.log(formData);
//         var baseUrl = base_url + 'admin/updateproduct?id=<?php //echo $model->product_id;?>';
//         var form = $(this);

    
//      $.ajax({
//           url: form.attr('action'),
//           type: 'POST',
//           data: formData,
//           processData: false,
//           contentType: false,
//           cache:false,
//           success: function (response) {
//             //console.log(response);              
//             if(response == 1){
//               $("#modal2").modal('hide');
//               $.pjax.reload('#productajax' , {timeout : false});
//             }
//           },
         
//      });
//      e.stopImmediatePropagation();  
//      return false;
// });
</script>
<!-- <script>
   function checklitre(elem)
  {       
         var id=elem.id;    
         name=document.getElementById(id).value;
          var numbers = /^[0-9]+$/;
         if(name=="")
         {
            //console.log("name is empty");
            document.getElementById(id).style.border="1px solid #a94442";
             
            document.getElementById(id+"message").innerHTML="Litre field Cannot Be Blank.";
            return false;
         }
         else if(!elem.value.match(numbers))
         {
              document.getElementById(id).style.border="1px solid #a94442";
              document.getElementById(id).focus();
              document.getElementById(id+"message").innerHTML="Litre must be a number..";
              return false;
         }
         else
         {
             document.getElementById(id).style.border="1px solid #0ecc31";
             document.getElementById(id+"message").innerHTML="";
         }
  }
  </script>
  <script>
   function checkcode(elem)
  {       
         var id=elem.id;    
         name=document.getElementById(id).value;
         if(name=="")
         {
            //console.log("name is empty");
            document.getElementById(id).style.border="1px solid #a94442";
            
            document.getElementById(id+"message").innerHTML="Code field Cannot Be Blank.";
            return false;
         }
         else
         {
             document.getElementById(id).style.border="1px solid #0ecc31";
             document.getElementById(id+"message").innerHTML="";
         }
  }
  </script>
  <script>
   function checkname(elem)
  {       
         var id=elem.id;    
         name=document.getElementById(id).value;
         if(name=="")
         {
            //console.log("name is empty");
            document.getElementById(id).style.border="1px solid #a94442";
            
            document.getElementById(id+"message").innerHTML="Name field Cannot Be Blank.";
            return false;
         }
         else
         {
             document.getElementById(id).style.border="1px solid #0ecc31";
             document.getElementById(id+"message").innerHTML="";
         }
  }
  </script>
  <script>
   function checkprice(elem)
  {       
          var id=elem.id;    
          name=document.getElementById(id).value;
          var numbers = /^[0-9]+$/;
         if(name=="")
         {
            //console.log("name is empty");
            document.getElementById(id).style.border="1px solid #a94442";
            
            document.getElementById(id+"message").innerHTML="Price field Cannot Be Blank.";
            return false;
         }
          else if(!elem.value.match(numbers))
         {
              document.getElementById(id).style.border="1px solid #a94442";
               document.getElementById(id).focus(); 
              document.getElementById(id+"message").innerHTML="Price must be a number..";
              return false;
         }
         else
         {
             document.getElementById(id).style.border="1px solid #0ecc31";
             document.getElementById(id+"message").innerHTML="";
         }
  }
  </script> -->
<script>
 $('.saveProductUpdate').click(function(event){
   //alert("ravi");
   // if()
   //  $('#hereThickness').remove();
   //  $('#hereFixed').remove();
   //  $('#hereSized').remove();
   //alert(($(".empty").length));
  event.preventDefault();
  //var stop=[];
  var numbers = /^[0-9.,]+$/;  
  var i=0;
  var inputLoop = 1;
  var inputLoopError = 0;
  $("#myid222 .empty").each(function() {
    if($(this).val()=='')
    {
      name = $(this).attr('data');
      var nameId = $(this).attr('id');
      $(this).attr("style","border:1px solid #a94442 ");
      $('.'+nameId+'message').empty();
      $('.'+nameId+'message').html(name+' cannot be empty ');
      i++;
	    inputLoopError++;
    }
    else if($(this).val()==0 || $(this).val()=="0")
    {
      name = $(this).attr('data');
      var nameId = $(this).attr('id');
      $(this).attr("style","border:1px solid #a94442 ");
      $('.'+nameId+'message').empty();
      $('.'+nameId+'message').html(name+' cannot be 0 ');
      i++;
      inputLoopError++;
    }
    else
    {
      if($(this).hasClass('number')){
        if(isNaN($(this).val()))
        {
          name = $(this).attr('data');
          var nameId = $(this).attr('id');
          $(this).attr("style","border:1px solid #a94442");
          
          $('.'+nameId+'message').empty();
          $('.'+nameId+'message').text(name+' should be a number');
          
          i++;
		inputLoopError++;
        }
        else
        {
          var name =$(this).attr('data');
          var nameId = $(this).attr('id');
          $(this).attr("style","border:1px solid #0ecc31");
          $('.'+nameId+'message').empty();
        }
      }
      else{
        var name =$(this).attr('data');
        var nameId = $(this).attr('id');
        $(this).attr("style","border:1px solid #0ecc31");
        $('.'+nameId+'message').empty();
      }
    }

			if(inputLoopError > 0){
       // alert("rk");
				$(this).closest('tr').next('tr').show();
			}
			else{
       // alert("kr");
				$(this).closest('tr').next('tr').hide();
			}
			inputLoopError = 0;

  });
  if(i>0)
  {
    // console.log(i);
    // alert('hello');
	 // $(".errorTd").show();	
	return false;
  }
  // else{
        $('.loader').show();
        var pathname = window.location.href;
        var formData = new FormData($('#myid222')[0]);
        var form = $('#myid222').attr('action');
        console.log(pathname);
        $.ajax({
            url: form,
            type: 'post',
            data: formData,
            processData: false,
            contentType: false,
            cache:false,
            success: function (response) {
            //console.log(response);              
                if(response == 1){
                 $('.loader').hide();
                 $("#modal2").modal('hide');
                 $(document.body).removeClass('modal-open');
         $('.modal-backdrop').remove();
                 $.pjax.reload('#productajax' , {timeout : false});
               }
             },
        });
  // }
 });
</script>
<script>
  $('.opnImg').click(function(){
    $("#imgInp").click()
  })
</script>
<script>
  $('.deleteSavedProduct').click(function(e){
    id=$(this).attr('data-id');
   
    var formdata = {'id':id};
    $(this).closest("tr").remove();
    $.ajax({
        url:'deleteproductstock' ,
        type: 'post',
        data: formdata,
        async:false,
        // processData: false,
        // contentType: false,
        // cache:false,
        success: function (response) {
        //console.log(response);              
            if(response == 1){
             //alert('success');
              
              
           }
         },
    });
  e.stopImmediatePropagation();
  return false;
  });


  // $($('input[name="TblProducts[type_id]"]').before('')).remove();
  // $('input[name="TblProducts[type_id]"][type="hidden"]').remove();


</script>