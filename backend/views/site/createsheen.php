<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TblSheen */

//$this->title = 'Create Tbl Sheen';
$this->params['breadcrumbs'][] = ['label' => 'Tbl Sheens', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-sheen-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_formsheen', [
        'model' => $model,
    ]) ?>

</div>
