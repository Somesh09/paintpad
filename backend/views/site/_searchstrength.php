<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;


/* @var $this yii\web\View */
/* @var $model app\models\TblStrengthsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-strengths-search tbl-search-input-all">
  <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
        'data-pjax' => 1
        ],
    ]);


     ?>

    <div style="width:23%; margin-right:0px; border:1px;">
    <?= $form->field($model, 'name')->textInput(['placeholder' => "Search Tint Strength"])->label(false); ?>
</div>
   
    
    <?php ActiveForm::end(); ?>


</div>
