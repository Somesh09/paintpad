<?php

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use yii\widgets\Pjax;
use yii\helpers\Url;
//use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\TblBrands;
use dosamigos\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TblProductsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

// $this->title = 'Products';
// $this->params['breadcrumbs'][] = $this->title;
//echo "<pre>";
//print_r($model2);

?>
<style>
.pagePagination1
{
  background:#000; color:#FFF;
}
.activePagination{
  background:#000; color:#FFF;
}
</style>



<span class="totalPAGE" style="display:none "><?php echo $total;?> </span>
<input type="hidden" value="<?php echo $id;?>" class="getId">
<input type="text" class="form-control" id="searchProductName" style="width: 100%;margin: 10px auto !important;" placeholder="Search/filter">
<div style="width: 570px;overflow-y: scroll;overflow-x: hidden;margin: 0 auto;max-height: 490px;">

<div class="loader" style="display:none"></div>
 <table class="table table-striped table-bordered" id='example' >
    <tr>

<?php

foreach($model2 as $model)
{
  ?>
  
   <tr class="tosearch">
        <td>
          <p class="productDisplaytop" data-id="<?php echo $model['product_id'];?>"><?php echo $model['name'];?></p>
        </td>
    </tr>      
  <?php
}
?>
</tr>
    </table>
    <div class="paginationSelectProduct">
    <?php
      for($i=1;$i<=$total;$i++)
      {
        echo "<button type='button' class='changePage pagePagination".$i."' data-id='".$i."' >".$i."</button>";
      }
    ?>
  </div>



  </div>

<script>
// $(document).ready(function(){
//   $("#searchProductName").on("keyup", function(){
//     $('.noAppointment').remove();
//     var searchClient = $(this).val();
//     //console.log(searchClient);
//     searchClient = searchClient.toLowerCase();
//     if(searchClient != ""){
//       $('.tosearch').show();
//       var flag=0;
//       $(".productDisplaytop").each(function(){
//         var thisHtml = $(this).text();
//         thisHtml = thisHtml.toLowerCase();
//        // console.log(thisHtml);
//         if(thisHtml.indexOf(searchClient) < 0 ){
//           $(this).closest("tr").hide();
//         }
//         else
//         {
//           flag=1;
//         }
//       });
//       if(flag ==0)
//       {
//         var fieldHtml="<li class='alert alert-danger noAppointment'>NO Contacts</li>";
//         $('.contact_list').append(fieldHtml);
//       }
//     }
//     else
//     {
//       $('.tosearch').show();
//     }
//   })
// })
</script>
<script>
  $(document).ready(function(){
    $(document).on('click', '.productDisplaytop', function(e){  
      var id=$(this).attr('data-id');
      var name =$(this).text();
      var pid = $('.getId').val();
      var fieldHTML = '<li data-parent="'+id+'" data-id="'+id+'"><div></div><div><input type="checkbox" data-id="'+id+'" name="topcoatEnabled['+pid+']['+id+']" class="productCheck"><p class="sheenProductList">'+name+' </p><input type="hidden" name="field_name['+pid+']['+id+']"  value="'+name+'" data-id="'+id+'" readonly/><span class="abcx" >&times;</span></div></li>';

      $('.alert_'+pid).find('.selector').remove();
      $('.alert_'+pid).append(fieldHTML);
      $('#myModal1').hide();
      e.stopImmediatePropagation();  
      return false;
    })
  })
</script>

<script type="text/javascript">
  function selectproductTopcoat(id,searchProduct,ap){
    $('.loader').show();
    var mid = $('.getId').val();
    var array = "<?php echo $array;?>"
    var data = {'page':id,'sp':searchProduct};  
    $.ajax({
      url: 'selectproduct?id='+mid+'&array='+array,
      type: 'post',
      data: data,
      success: function (response) {
          

           console.log(response);              

           if(response){
            //$('.pagination').removeClass('pagePagination1')
              var response = response;
              var fieldHtml =  $(response).find('#example').html();
              var pagination = $(response).find('.paginationSelectProduct').html();
              $('#example').html(fieldHtml);
              $('.paginationSelectProduct').html(pagination);
              $('.loader').hide();
              $(response).find('.pagePagination1').removeClass('pagePagination1')
              $(response +'.pagePagination1').removeClass('pagePagination1');
              $('.pagePagination'+ap).addClass('activePagination');
              if($('.activePagination').length>0)
              {
                $('div').find('.pagePagination1').removeClass('pagePagination1');
              }

           }
      },
     
    });
  }

</script>
<script>
    $("#searchProductName").on("keyup", function(){
        var searchProduct = $(this).val();
        searchProduct = searchProduct.toLowerCase();
        selectproductTopcoat(1,searchProduct,1);
      })

</script>
<script>
$(document).on('click', '.changePage', function(e){  
  //$('.pagination').addClass('loader')
  var ap = $(this).text();
  //alert(ap);
  var searchProduct = $('#searchProductName').val();
  searchProduct = searchProduct.toLowerCase();
  $('.paginationSelectProduct').find('.pagePagination1').removeClass('pagePagination1');
  $('.paginationSelectProduct').find('.activePagination').removeClass('activePagination');
  $(this).addClass('activePagination');
  var id = $(this).attr('data-id');
  selectproductTopcoat(id,searchProduct,ap);

  e.stopImmediatePropagation();
  return false;  
})
</script>   

