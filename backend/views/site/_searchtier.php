<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\TblBrands;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model app\models\TblTiers_Search */
/* @var $form yii\widgets\ActiveForm */
$model2 = new TblBrands();
?>

<div class="tbl-tiers-search tbl-search-input-all">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
        'data-pjax' => 1
        ],
    ]);


     ?>
   
    <div style="width:23%; margin-right:0px; border:1px;">
        <?= $form->field($model, 'name')->textInput(['placeholder' => "Search Tier"])->label(false); ?>
    </div>
    <div style="width:43%;  border:1px; margin-left:340px; margin-top: -49px">
        <?=
            $form->field($model, 'brand_id')->dropDownList(ArrayHelper::map(TblBrands::find()->asArray()->all(), 'brand_id', 'name'),['prompt'=>'---Filter By Brand---'])->label(false)
        ?>

    </div>
    <div style="width:43%;  border:1px; margin-left:340px; margin-top: -49px">
        <?php
            echo $form->field($model, 'enabled')->dropDownList(['' => 'SHOW ALL', '1' => 'Active', '0' => 'Archived'])->label(false); 
        ?>

    </div>

    <?php ActiveForm::end(); ?>

</div>
