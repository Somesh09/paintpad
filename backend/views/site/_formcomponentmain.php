 <?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\TblComponentGroups;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
USE app\models\TblComponentCalculateMethod;
use app\models\TblComponentPricingMethods;
use wbraganca\dynamicform\DynamicFormWidget;
use app\models\TblComponentType;

//$model2 =new TblComponentType;

/* @var $this yii\web\View */
/* @var $model app\models\TblComponents */
/* @var $form yii\widgets\ActiveForm */
// $js = '
// jQuery(".dynamicform_wrapper").on("afterInsert", function(e, item) {
//     jQuery(".dynamicform_wrapper .panel-title-address").each(function(index) {
//         jQuery(this).html("Address: " + (index + 1))
//     });
// });

// jQuery(".dynamicform_wrapper").on("afterDelete", function(e) {
//     jQuery(".dynamicform_wrapper .panel-title-address").each(function(index) {
//         jQuery(this).html("Address: " + (index + 1))
//     });
// });
// ';

// $this->registerJs($js);

?>
<style>
.dynamicform_wrapper label {
    display: inline-block;
    max-width: 100%;
    margin-bottom: 5px;
    font-weight: bold;
    font-size: 12px;
}

.tbl-components-form .form-group {
  /* float: left;
  width: 50%;
  padding: 0 10px;
  min-height: 75px;
  height: 75px; */
}
 .ttClass {
    width: 100%;
    float: left;
}
.ttClass-one {
    width: 50%;
    float: left;
}
.ttClass-two {
    float: right;
    width: 50%;
}
.tbl-components-form .ttClass-two .form-group {
    width: 100%;
}
.tbl-components-form .ttClass-one .form-group {
    height: auto;
    min-height: auto;
    width: 100%;
}
.ttClass-one img {
    float: left;
    margin-bottom: 14px;
    border: 1px solid #ccc;
}
h1{
  display: none;
}
</style>

<div class="tbl-components-form form-inner-head">
    <?php $form = ActiveForm::begin(['validationUrl' => Url::toRoute(['validatecomponentmain']),'enableAjaxValidation' => false, 'id' => 'myid14',]); ?>
    <div class = "modal-body-div-add">
    <h2 style="position: relative;top: -2px;">Create Component</h2>
      <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
      <button type="button" class="close glyphicon glyphicon-remove" data-dismiss="modal" aria-hidden="true">×</button>
    </div>


	<div class="main-div-upimgdiv" style="border-bottom: 0px solid #ccc;">
		<div class="main-div-upimgdiv" style="border-bottom: 0px solid #ccc;padding-top: 0px;">
		  
		  
		  
		  <div class="upImgDiv main-copo-01">
			  <h2 style="
		font-size: 15px;
		padding-left: 15px;
		font-weight: bold;
		background: #eee;
		padding-top: 13px;
		padding-bottom: 13px;
		border-bottom: 1px solid #ddd;
		width: 100%;
	"> Component </h2>

			<?= $form->field($model, 'name')->textInput(['maxlength' => true])->label('Name') ?>
			<?= $form->field($model,'group_id')->dropDownList(ArrayHelper::map(TblComponentGroups::find()->all(),'group_id','name'),['prompt'=>'Select Component Group']) ?> 
			<?= $form->field($model,'price_method_id')->dropDownList(ArrayHelper::map(TblComponentPricingMethods::find()->all(),'price_method_id','name'),['prompt'=>'Select Price Method Name']) ?>
		  
				
		   </div>
		   
		   
		<div class="bttmDiv main-copo-02"> 
				  <h2 style="
					font-size: 15px;
					padding-left: 15px;
					font-weight: bold;
					background: #eee;
					border-bottom: 1px solid #ddd;
					margin-top: 15px;
					padding-top: 13px;
					padding-bottom: 13px;
					width: 100%;
					border-top: 1px solid #ddd;
				">Tool Tips</h2>

			<div class="imgg_div">
			 <img id="blah2" src="uploads/no_image.png" alt="your image" style="max-width: 100%"/>
			 <span class="opnImg"><img id="blah21" src="uploads/edit.png" alt="your image" /></span>
				 <?= $form->field($model,'tt_image',['inputOptions'=>['name' => 'image','id'=>'imgInp']])->fileinput()->label(false);?>
			<p class="img-size"> Select Image max 2Mb</p>
      </div>
			
			<div class="ttClass"> 	
			<?= $form->field($model, 'tt_url')->textInput(['maxlength' => true]) ?>
			<?= $form->field($model, 'tt_text')->textarea(['maxlength' => true]) ?>
			 </div>
			 
			</div>
		  </div>
	</div>



           




  </div>


<?php ActiveForm::end(); ?>
<script src="/app/admin/js/compressor.min.js"></script>
<script>
  $('#blah21').click(function(){
    $('#blah2').click();
  })
</script>

<script>
var CompImage = ''
$('body').on('submit', '#myid14', function (e) {
   e.preventDefault();
        var formAction = $(this).attr("action");
        var pathname = window.location.href;
        var formData = new FormData($('#myid14')[0]);
        formData.delete('image');
        if($('#tblcomponents-group_id').val() == ''){
          alert('Please select Component Group.');
          return false;
        }
        if($('#imgInp').val() == ''){
          alert('Please select image.');
          return false;
        }

        formData.append('image', CompImage, CompImage.name);
        var form = $(this);
    
     $.ajax({
         url: form.attr('action'),
          type: 'post',
          data: formData,
          processData: false,
          contentType: false,
          cache:false,
          success: function (response) {
          if(response == 1){
                
                    $("#modalcomponentmain").modal('hide');
                    $(document.body).removeClass('modal-open');
         $('.modal-backdrop').remove();
                    $.pjax.reload('#componentmainpjax' , {timeout : false});

               }
          },
       
     });
     e.stopImmediatePropagation();

     return false;
});


// function showDiv(elem)
// {

    
//     if(elem.value == 1)
//     { 
//              var datta = $('#here').html();

//              $('#maindiv').empty('');
//              $('#maindiv').append(datta);
//     }    

//     if(elem.value == 2)
//     {
            
//           var data = $('#hidden_div2').html();
//           $('#maindiv').empty('');
//           $('#maindiv').append(data);
//     }
//     if(elem.value == 3)
//     {
//           var data = $('#hidden_div3').html();
//           $('#maindiv').empty('');
//           $('#maindiv').append(data);
//     }
// }



// $(document).on('change', 'input[type="checkbox"]', function(){
//    $(this).closest('#maindiv').find('input[type="checkbox"]').prop('checked',false);
//    $(this).prop('checked',true);
// });

function readURL(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {

      $("img").show();
      $('#blah2').attr('src', e.target.result);
      //$("#blah3").hide();
      
    }
    reader.readAsDataURL(input.files[0]);
    const file = input.files[0];
    // alert(file.size);
    new Compressor(file, {
        quality: 0.6,
        success(result) {
          const formData = new FormData();
          // The third parameter is required for server
          // formData.append('file', result, result.name);
          CompImage = result
          // console.log(CompImage)
        },
        error(err) {
          console.log(err.message);
        },
      });
    //myy end
  }

}

$("#imgInp").change(function() {
  var maxSizeKB = 2000; //Size in KB
  var maxSize = maxSizeKB * 1024; //File size is returned in Bytes
  if (this.files[0].size > maxSize) {
    $(this).val("");
    alert("Please select Image of 2mb or less");
    return false;
  }
  readURL(this);
});
  $('#blah21').click(function(){
    $('#imgInp').click();
  })
</script>
<script type="text/javascript">
 
</script>