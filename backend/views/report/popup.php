<div id="win2_content" class="mochaContent" style="padding: 10px 12px 0px; display: block;"><div class="form-horizontal form-elos" id="total_column_set_form_1609822366">
	<div class="heading-container clearfix">		
		<div class="pull-right">			
			<input type="button" value="Submit" class="btn btn-primary" name="submit_btn"> 			
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label">Field to Summarise </label>
		<div class="controls">
			<span class="control-text summary_field_area">Client Name</span>
		</div>
	</div>	
	<div class="control-group">
		<label class="control-label">Total Type </label>
		<div class="controls">
			<select name="total_type" class="pass_field">
				<option value="total">Total</option>
				<option value="average">Average</option>
				<option value="count">Count</option>				
			</select>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label">Report Column Title</label>
		<div class="controls">
			<input type="text" value="" name="column_title" class="pass_field">
		</div>
	</div>
	<div class="control-group">
		<label class="control-label">Align</label>
		<div class="controls">
			<select name="align" class="pass_field">
				<option value="left">Left</option>
				<option value="right">Right</option>
				<option value="center">Center</option>
			</select>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label">Decimal Points</label>
		<div class="controls">
			<select name="decimal_points" class="pass_field">
				<option>0</option>
				<option>1</option>
				<option>2</option>				
				<option>3</option>
			</select>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label">Is Dollar</label>
		<div class="controls">
			<label class="radio inline"><input type="radio" value="1" name="is_dollar"> Yes</label>
			<label class="radio inline"><input type="radio" value="0" name="is_dollar" checked="checked"> No</label>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label">Is Percent</label>
		<div class="controls">
			<label class="radio inline"><input type="radio" value="1" name="is_percent"> Yes</label>
			<label class="radio inline"><input type="radio" value="0" name="is_percent" checked="checked"> No</label>
		</div>
	</div>
</div>
</div>



********************************************************************************************************************




<div id="cweb_sub_window_1609822192592_contentBorder" class="mochaContentBorder"><div id="cweb_sub_window_1609822192592_contentWrapper" class="mochaContentWrapper" style="width: 750px; height: 524.45px; overflow: auto;"><div id="cweb_sub_window_1609822192592_content" class="mochaContent" style="padding: 10px 12px 0px; display: block;"><div id="report_sys_field_select_1609822192">
	<div class="windowInfo clearfix">
		<select name="select_type">
			<option value="">--- All Type ---</option>
						<option value="client">Client</option>
						<option value="contact_person">Contact</option>
						<option value="project">Project</option>
					</select>
	</div>
	<table class="table">
		<tbody class="sys_field_container"><tr class=".type_client"><td style="font-weight: bold;">Client</td></tr><tr class=".type_client"><td style="padding-left: 20px;">Client Name</td></tr><tr class=".type_client"><td style="padding-left: 20px;">Phone</td></tr><tr class=".type_client"><td style="padding-left: 20px;">Mobile</td></tr><tr class=".type_client"><td style="padding-left: 20px;">Location</td></tr><tr class=".type_client"><td style="padding-left: 20px;">Postal Address</td></tr><tr class=".type_client"><td style="padding-left: 20px;">Email</td></tr><tr class=".type_client"><td style="padding-left: 20px;">Contact Group</td></tr><tr class=".type_contact_person"><td style="font-weight: bold;">Contact</td></tr><tr class=".type_contact_person"><td style="padding-left: 20px;">Client Name</td></tr><tr class=".type_contact_person"><td style="padding-left: 20px;">Phone</td></tr><tr class=".type_contact_person"><td style="padding-left: 20px;">Mobile</td></tr><tr class=".type_contact_person"><td style="padding-left: 20px;">Location</td></tr><tr class=".type_contact_person"><td style="padding-left: 20px;">Postal Address</td></tr><tr class=".type_contact_person"><td style="padding-left: 20px;">Email</td></tr><tr class=".type_contact_person"><td style="padding-left: 20px;">Contact Group</td></tr><tr class=".type_project"><td style="font-weight: bold;">Project</td></tr><tr class=".type_project"><td style="padding-left: 20px;">Project ID</td></tr><tr class=".type_project"><td style="padding-left: 20px;">Name</td></tr><tr class=".type_project"><td style="padding-left: 20px;">Site Council</td></tr><tr class=".type_project"><td style="padding-left: 20px;">Site CT Volume</td></tr><tr class=".type_project"><td style="padding-left: 20px;">Site CT Folio</td></tr><tr class=".type_project"><td style="padding-left: 20px;">Site Note</td></tr><tr class=".type_project"><td style="padding-left: 20px;">Owner</td></tr><tr class=".type_project"><td style="padding-left: 20px;">Creator</td></tr><tr class=".type_project"><td style="padding-left: 20px;">Site Address</td></tr><tr class=".type_project"><td style="padding-left: 20px;">Create Date</td></tr><tr class=".type_project"><td style="padding-left: 20px;">Project Status</td></tr><tr class=".type_project"><td style="padding-left: 20px;">Project Sales Type</td></tr><tr class=".type_project"><td style="padding-left: 20px;">Site Contact</td></tr><tr class=".type_project"><td style="padding-left: 20px;">Total Ejv</td></tr><tr class=".type_project"><td style="padding-left: 20px;">Last Interaction</td></tr><tr class=".type_project"><td style="padding-left: 20px;">Last Interaction Message</td></tr><tr class=".type_project"><td style="padding-left: 20px;">Total Value</td></tr></tbody>
	</table>
</div>
</div></div></div>