<?php
use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\bootstrap\ButtonDropdown;
use dosamigos\grid\GridView;
use dosamigos\exportable\ExportableButton; 
use kartik\export\ExportMenu;

// global 
// global $cRole = "";
?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-gb" lang="en-gb" dir="ltr" slick-uniqueid="3">

<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="robots" content="noindex,follow">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=5, minimum-scale= 1.0">
    <meta name="generator" content="MySys - www.mysys.com.au">
    <title>PaintPad -
        Projects

        - Report</title>

<!--
   <link rel="stylesheet" href="http://paintpad.mysys.com.au/components/com_cweb/js/jquery-ui-1.11.3.custom/jquery-ui.css" type="text/css">
   
 
    <link rel="stylesheet" href="http://paintpad.mysys.com.au/components/com_cweb/css/boost/bootstrap.css" type="text/css">
    <link rel="stylesheet" href="http://paintpad.mysys.com.au/components/com_cweb/css/boost/elos.css?nocache=1605787243" type="text/css">
-->


    

</head>

<body class="contentpane desktop physicalKeyboard">

    <div id="system-message-container">
    </div>
    <div id="desktop_top_wrapper" menu_open_delay="270">

        <div class="elos-nav" id="cweb_nav_bar">
            <div class="clearfix container">

                <div class="menu-left">
                    <ul id="js_multi_view_selector" class="">



                        <li class="active"><a menu_id="7" class="root_menu_btn js_menu_btn reports_view" dashboard="reports_view" url="index.php?option=com_cweb&amp;view=dash_board&amp;layout=dash_board&amp;section=reports_view&amp;format=dash_board" href="#"><i><i class="icon icon-list-alt icon-white"></i></i><span>Reports</span></a>
                            <ul>
                                <li class=""><a menu_id="128" class="js_menu_btn " href="#" url="index.php?option=com_cweb&amp;view=predefined_reports&amp;layout=predefined_report_table_wrapper&amp;format=default">
                                        <span class="pull-left">Predefined Reports</span>
                                    </a>
                                </li>
                                <li class="active"><a menu_id="125" class="js_menu_btn " href="#" url="index.php?option=com_cweb&amp;view=report&amp;layout=entity_report_config&amp;format=cweb_report&amp;report_type=project">
                                        <span class="pull-left">Projects</span>
                                        <i class="icon icon-folder-open icon-white pull-right"></i>
                                    </a>
                                </li>
                            </ul>
                        </li>



                    </ul>
                </div>
            </div>
        </div>

    </div>
    <div id="autocomplete_results">
        <div class="autocomplete-suggestions" style="position: absolute; display: none; width: 350px; max-height: 300px; z-index: 9999;"></div>
    </div>
    <div id="desktop" class="smallfonts reports_view">
        <div class="wrapper">
            <div class="row-fluid" id="main_page_sub_menu_section">
                <div class="span12">
                    <div class="clearfix">
                        <div class="span2" id="dash_board_section_side">
                            <span>
                                <a href="#" class="btn btn-large btn-icon" id="show_sidebar" title="Show/hide sidebar info"><i class="icon-list-alt "></i></a>
                                <a href="#" class="btn btn-primary btn-small " id="return_to_dashboard_btn">Dash<wbr>board</a>
                            </span>
                        </div>
                        <div class="btn-group span10" style="display: block;" id="dash_board_section_buttons">
                            <div class="pull-right nav_logo hide" style="height:37px;">
                                <img src="http://paintpad.mysys.com.au/templates/login/img/logo.png" alt="Logo" class="logo">
                            </div>
                            <a href="index" class="btn btn-large dashbord_btns reports_view " menu_id="128" dashboard="reports_view" style="display: inline;">Predefined Reports</a>
                            <a href="projects" class="btn btn-large dashbord_btns reports_view active" menu_id="125" dashboard="reports_view" style="display: inline;">Projects</a>

                        </div>
                        <div class="btn-group pull-right span6 hide" id="dash_board_widget_tool_btn">
                            <a href="#" class="btn btn-small add_remove_widget_btn pull-right">Widgets</a>
                        </div>
                    </div>
                    <hr class="hide">
                </div>
            </div>
            <div class="row-fluid">
                <div class="span10 desktop_dash_board_right pull-right" id="cweb_working_area">
                    <div id="cweb_report_config_wrapper_1605790365" class="flex-column">
                        <div class="windowInfo  clearfix">


                            <?php $form = ActiveForm::begin(); ?>

                            <span class="domHTml">
                                <input type="hidden" name="order" class="hidden_order" value="">
                            </span>


                            <div class="btn-group pull-right" style="margin-left: 2em;">
                                <?= Html::submitButton('Save', ['class' => 'btn btn-primary', 'name' => 'save_btn']) ?>

                            </div>

                            <div class="btn-group pull-right" style="margin-left: 2em;">
                                <input type="button" class="btn btn-primary" value="Preview HTML" name="preview_btn">
                                <button class="btn btn-primary" value="PDF" name="view_pdf_btn"><i class="icon icon_pdf pull-left" style="margin-right:0.5ex;"></i> PDF</button>
                            </div>

                            <div class="btn-group pull-right" style="margin-left: 2em;">
                                <button class="btn btn-primary" name="view_csv_btn"><i class="fa fa-download"></i> Download CSV</button>
                            </div>


                            <div class="input-prepend pull-left">
                                <span class="add-on pull-left">Report Name: </span>
                                <input type="text" value="" name="report_name" required>
                            </div>
                        </div>
                        <div class="scroll-container header footer pad form-inline form-horizontal form-elos form-left">
                            <div class="row-fluid">
                                <div class="span5">

                                    <div class="side-nav entry_wrapper">
                                        <div class="side-nav-header clearfix">
                                            <div class="pull-left">
                                                <h3>Date Settings</h3>
                                            </div>
                                        </div>
                                        <div class="side-nav-content">
                                            <div class="control-group">
                                                <label class="control-label">Date Field</label>
                                                <div class="controls">
                                                    <select name="date_field">
                                                        <optgroup label="System Date Field">
                                                            <option value="create_date">Create Date</option>
                                                        </optgroup>

                                                    </select>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Date Range</label>
                                                <div class="controls">
                                                    <input id="date_range" type="hidden" value="" name="date_range" class="input-small frm">

                                                    <select name="date_range" class="date_setting_select" id="filter_mee" onchange="change_mee()">
                                                        <optgroup label="Date Range">
                                                            <option value="0">Custom</option>
                                                            <option value="1">Tomorrow</option>
                                                            <option value="2">2 Days</option>
                                                            <option value="3">3 Days</option>
                                                            <option value="4">4 Days</option>
                                                            <option value="5">5 Days</option>
                                                            <option value="6">6 Days</option>
                                                            <option value="7">7 Days</option>
                                                            <option value="8">Yesterday</option>
                                                            <option value="9">Past Week</option>
                                                            <option value="10">Past Month</option>
                                                        </optgroup>
                                                        <optgroup label="For one day only">
                                                            <option value="01">Tomorrow</option>
                                                            <option value="02">in 2 Days</option>
                                                            <option value="03">in 3 Days</option>
                                                            <option value="04">in 4 Days</option>
                                                            <option value="05">in 5 Days</option>
                                                            <option value="06">in 6 Days</option>
                                                            <option value="07">in 7 Days</option>
                                                        </optgroup>
                                                    </select>
                                                </div>
                                            </div>
                                            
                                            <div class="control-group date_config_area zx">
                                                <div class="control ">
                                                    <div class="input-prepend">
                                                        <span class="add-on" style="width:30px">From</span>
                                                        <span class="from_date_input_wrapper" style="display: inline;">

                                                            <input id="date_from" type="text" value="" name="date_from" class="input-small frm">
                                                            <input id="date_from" type="hidden" value="" name="from" class="input-small frm">

                                                        </span>
                                                        <select name="from" class="date_setting_select input-large" id="filter_myfrm" onchange="change_frm()">
                                                            <option value="0">Fixed Date</option>
                                                            <option value="1">Start of the month</option>
                                                            <option value="3">Start of the Week</option>
                                                            <option value="4">End of the Week</option>
                                                            <option value="5">Start of previous month</option>
                                                            <option value="6">Start of the year</option>
                                                            <option value="7">Start of financial year</option>
                                                            <option value="8">Start of previous financial year</option>
                                                            <option value="9">Start of current quarter</option>
                                                            <option value="10">Start of previous quarter</option>
                                                            <option value="11">Yesterday</option>
                                                            <option value="12">Today</option>
                                                            <option value="13">End of The Month</option>
                                                            <option value="14">One Month Time</option>
                                                            <option value="15">End of previous month</option>
                                                            <option value="16">End of financial year</option>
                                                            <option value="17">End of previous financial year</option>
                                                            <option value="18">End of previous quarter</option>


                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="control">
                                                    <div class="input-prepend" style="display:inline-block;">
                                                        <span class="add-on" style="width:30px">To</span><span class="to_date_input_wrapper" style="display: inline;">
                                                            <input id="date_to" type="text" value="" name="date_to" class="input-small to">
                                                            <input id="date_to" type="hidden" value="" name="to" class="input-small to">
                                                        </span>
                                                        <select name="to" class="date_setting_select input-large" id="filter_myto" onchange="change_to()">
                                                            <option value="0">Fixed Date</option>
                                                            <option value="1">Start of the month</option>
                                                            <option value="3">Start of the Week</option>
                                                            <option value="4">End of the Week</option>
                                                            <option value="5">Start of previous month</option>
                                                            <option value="6">Start of the year</option>
                                                            <option value="7">Start of financial year</option>
                                                            <option value="8">Start of previous financial year</option>
                                                            <option value="9">Start of current quarter</option>
                                                            <option value="10">Start of previous quarter</option>
                                                            <option value="11">Yesterday</option>
                                                            <option value="12">Today</option>
                                                            <option value="13">End of The Month</option>
                                                            <option value="14">One Month Time</option>
                                                            <option value="15">End of previous month</option>
                                                            <option value="16">End of financial year</option>
                                                            <option value="17">End of previous financial year</option>
                                                            <option value="18">End of previous quarter</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>



                                    <div class="side-nav entry_wrapper">
                                        <div class="side-nav-header clearfix">
                                            <!--
                                            <div class="pull-right">
                                                <input type="button" class="btn btn-primary" value="Add Group" name="add_group_btn" onclick="click_me()" id="btn">
                                            </div>
-->
                                            <div class="pull-left">
                                                <h3>Grouping</h3>
                                            </div>
                                        </div>
                                        <div class="side-nav-content">
                                            <table class="table">
                                                <tbody class="group_by_wrapper">
                                                    <!--
                                                    <tr class="group_by_tmpl" style="display:none;">
                                                        <td class="group_by_heading_area" style="font-weight:bold;width:100px;">Group By</td>
                                                        <td class="group_by_select_wrapper">
                                                            <select name="group_by">

                                                            </select>
                                                        </td>
                                                        <td class="remove_icon_wrapper"></td>
                                                    </tr>
-->
                                                    <tr class="group_by_tmpl group_by_select" style="display: table-row;">
                                                        <td class="group_by_heading_area" style="font-weight:bold;width:100px;">Group By</td>
                                                        <td class="group_by_select_wrapper">



                                                            <select name="group_by" onchange="change_meee()" id="filter_meee">
                                                                <option value="0">Select Group Filter</option>


                                                                <option value="1">Client</option>
                                                                <option value="2">Custom Field</option>
                                                                <option value="3">Date</option>
                                                                <option value="4">Lead Source</option>
                                                                <option value="5">Lead Class</option>
                                                                <option value="6">Sales Office</option>
                                                                <option value="7">Sales Type</option>
                                                                <option value="8">Status</option>
                                                                <option value="9">Responsibility</option>
                                                                <option value="10">Site Suburb</option>
                                                                <option value="11">Agent</option>
                                                                <option value="12">Owner</option>
                                                                <option value="13">Creator</option>
                                                                <option value="14">Site State</option>
                                                                <option value="15">Site Postcode</option>
                                                                <option value="16">Country</option>
                                                            </select>
                                                            <select name="group_user_role_select" class="group_by_second_select dd" style="display:none;">
                                                                <option value="41">Accounts</option>
                                                                <option value="47">Accounts Receivable</option>
                                                                <option value="42">Administration</option>
                                                                <option value="64">Despatch &amp; Logistics </option>
                                                                <option value="56">Invoicing</option>
                                                                <option value="59">Marketing </option>
                                                                <option value="57">Marty</option>
                                                                <option value="60">Production </option>
                                                                <option value="65">Purchasing</option>
                                                                <option value="13">Senior Manager</option>
                                                                <option value="44">Server Configuration</option>
                                                                <option value="58">Subscriber</option>
                                                                <option value="63">Telemarketing</option>
                                                                <option value="62">Tenderer </option>
                                                                <option value="61">Tendering Manager </option>
                                                            </select>
                                                            <select name="group_by_range" class="group_by_second_select dz" style="display:none;">
                                                                <option value=" 0">No Selected</option>
                                                                <option value="1">Daily</option>
                                                                <option value="2">Weekly</option>
                                                                <option value="3">Monthly</option>
                                                                <option value="4">Quarterly</option>
                                                                <option value="5">Yearly</option>
                                                            </select>
                                                            <select name="group_field_select" class="group_by_second_select dy" style="display: none;">
                                                            </select>
                                                        </td>
                                                        <td class="remove_icon_wrapper"></td>
                                                    </tr>
                                                    <tr class="group_by_tmpl group_by_select mysys_focus_row gx" style="display: none;">
                                                        <td class="group_by_heading_area" style="font-weight:bold;width:100px;">Then Group By</td>
                                                        <!--
                                                        <td class="group_by_select_wrapper">
                                                            <select name="group_by">
                                                                <option value="client">Client</option>
                                                                <option value="custom_field">Custom Field</option>
                                                                <option value="date">Date</option>
                                                                <option value="lead_source">Lead Source</option>
                                                                <option value="lead_class">Lead Class</option>
                                                                <option value="sales_office">Sales Office</option>
                                                                <option value="sales_type">Sales Type</option>
                                                                <option value="status">Status</option>
                                                                <option value="user_role">Responsibility</option>
                                                                <option value="site_suburb">Site Suburb</option>
                                                                <option value="agent">Agent</option>
                                                                <option value="owner">Owner</option>
                                                                <option value="creator">Creator</option>
                                                                <option value="site_state">Site State</option>
                                                                <option value="site_postcode">Site Postcode</option>
                                                                <option value="site_country">Country</option>
                                                            </select>
                                                        </td>
-->
                                                        <td class="remove_icon_wrapper"><i class="icon-remove"></i></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <select name="group_field_select" class="group_by_second_select" style="display:none;">
                                        </select>

                                        <select name="group_user_role_select" class="group_by_second_select" style="display:none;">
                                            <option value="41">Accounts</option>
                                            <option value="47">Accounts Receivable</option>
                                            <option value="42">Administration</option>
                                            <option value="64">Despatch &amp; Logistics </option>
                                            <option value="56">Invoicing</option>
                                            <option value="59">Marketing </option>
                                            <option value="57">Marty</option>
                                            <option value="60">Production </option>
                                            <option value="65">Purchasing</option>
                                            <option value="13">Senior Manager</option>
                                            <option value="44">Server Configuration</option>
                                            <option value="58">Subscriber</option>
                                            <option value="63">Telemarketing</option>
                                            <option value="62">Tenderer </option>
                                            <option value="61">Tendering Manager </option>
                                        </select>

                                        <select name="group_date_select" class="group_by_second_select" style="display:none;">
                                            <option value="daily">Daily</option>
                                            <option value="weekly">Weekly</option>
                                            <option value="monthly">Monthly</option>
                                            <option value="quarterly">Quarterly</option>
                                            <option value="yearly">Yearly</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="span7">
                                    <div class="side-nav entry_wrapper">
                                        <div class="side-nav-header clearfix">
                                            <div class="pull-right">


                                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal"> Add System Field Total</button>

                                                <!-- Button trigger modal -->


                                                <!-- Modal -->
                                                <div class="modal fade" id="myModal" role="dialog">
                                                    <div class="modal-dialog">

                                                        <!-- Modal content-->
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                <h4 class="modal-title">Choose System Fields</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="windowInfo clearfix">
                                                                    <select name="select_type" id="filter_group" onchange="groupfilter()">
                                                                        <option value="1">--- All Type ---</option>
                                                                        <option value="2">Client</option>
                                                                        <option value="3">Contact</option>
                                                                        <option value="4">Project</option>
                                                                    </select>
                                                                </div>
                                                                <table class="table">
                                                                    <tbody class="sys_field_container">
                                                                        <tr class="type_client common">
                                                                            <td style="font-weight: bold;"> Client</td>
                                                                        </tr>
                                                                        <tr class="type_client common">
                                                                            <td style="padding-left: 20px;"><span onclick="show_data('Client Name')">Client</span></td>
                                                                        </tr>
                                                                        <tr class="type_client common">
                                                                            <td style="padding-left: 20px;">
                                                                                <span onclick="show_data('Phone')">Phone</span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="type_client common">
                                                                            <td style="padding-left: 20px;">
                                                                                <span onclick="show_data('Mobile')">Mobile</span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="type_client common">
                                                                            <td style="padding-left: 20px;"> <span onclick="show_data('Location')">Location</span></td>
                                                                        </tr>
                                                                        <tr class="type_client common">
                                                                            <td style="padding-left: 20px;"><span onclick="show_data('Postal Address')">Postal Address</span></td>
                                                                        </tr>
                                                                        <tr class="type_client common">
                                                                            <td style="padding-left: 20px;">
                                                                                <span onclick="show_data('Email')">Email</span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="type_client common">
                                                                            <td style="padding-left: 20px;">
                                                                                <span onclick="show_data('Contact Group')">Contact Group</span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="type_contact_person common">
                                                                            <td style="font-weight: bold;">Contact</td>
                                                                        </tr>
                                                                        <tr class="type_contact_person common">
                                                                            <td style="padding-left: 20px;">
                                                                                <span onclick="show_data('Client name')">Client</span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="type_contact_person common">
                                                                            <td style="padding-left: 20px;">
                                                                                <span onclick="show_data('Phone')">Phone</span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="type_contact_person common">
                                                                            <td style="padding-left: 20px;">
                                                                                <span onclick="show_data('Mobile')">Mobile</span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="type_contact_person common">
                                                                            <td style="padding-left: 20px;">
                                                                                <span onclick="show_data('Location')">Location</span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="type_contact_person common">
                                                                            <td style="padding-left: 20px;">
                                                                                <span onclick="show_data('Postal Address')">Postal Address</span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="type_contact_person common">
                                                                            <td style="padding-left: 20px;">
                                                                                <span onclick="show_data('Email')">Email</span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="type_contact_person common">
                                                                            <td style="padding-left: 20px;">
                                                                                <span onclick="show_data('Contact Group')">Contact Group</span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="type_project common">
                                                                            <td style="font-weight: bold;">Project</td>
                                                                        </tr>
                                                                        <tr class="type_project common">
                                                                            <td style="padding-left: 20px;">
                                                                                <span onclick="show_data('Project ID')">Project ID</span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="type_project  common">
                                                                            <td style="padding-left: 20px;">
                                                                                <span onclick="show_data('Name')">Name</span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="type_project common">
                                                                            <td style="padding-left: 20px;">
                                                                                <span onclick="show_data('Site Council')">Site Council</span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="type_project common">
                                                                            <td style="padding-left: 20px;">
                                                                                <span onclick="show_data('Site CT Volume')">Site CT Volume</span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="type_project common">
                                                                            <td style="padding-left: 20px;">
                                                                                <span onclick="show_data('Site CT Folio')">Site CT Folio</span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="type_project common">
                                                                            <td style="padding-left: 20px;">
                                                                                <span onclick="show_data('Site Note')">Site Note</span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="type_project common">
                                                                            <td style="padding-left: 20px;">
                                                                                <span onclick="show_data('Owner')">Owner</span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="type_project common">
                                                                            <td style="padding-left: 20px;">
                                                                                <span onclick="show_data('Creator')">Creator</span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="type_project common">
                                                                            <td style="padding-left: 20px;">
                                                                                <span onclick="show_data('Site Address')">Site Address</span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="type_project common">
                                                                            <td style="padding-left: 20px;">
                                                                                <span onclick="show_data('Create Date')">Create Date</span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="type_project common">
                                                                            <td style="padding-left: 20px;">
                                                                                <span onclick="show_data('Project Status')">Project Status</span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="type_project common">
                                                                            <td style="padding-left: 20px;">
                                                                                <span onclick="show_data('Project Sales Type')">Project Sales Type</span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="type_project common">
                                                                            <td style="padding-left: 20px;">
                                                                                <span onclick="show_data('Site Contact')">Site Contact</span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="type_project common">
                                                                            <td style="padding-left: 20px;">
                                                                                <span onclick="show_data('Total Ejv')">Total Ejv</span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="type_project common">
                                                                            <td style="padding-left: 20px;">
                                                                                <span onclick="show_data('Last Interaction')">Last Interaction</span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="type_project common">
                                                                            <td style="padding-left: 20px;">
                                                                                <span onclick="show_data('Last Interaction Message')">Last Interaction Message</span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="type_project common">
                                                                            <td style="padding-left: 20px;">
                                                                                <span onclick="show_data('Total Value')">Total Value</span>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>

                                                            </div>

                                                        </div>

                                                    </div>
                                                </div>


                                                <!-- Modal -->
                                                <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">

                                                                <h4 class="modal-title" id="myModalLabel">Group Total Setting</h4>
                                                            </div>

                                                            <div class="modal-body">
                                                                <div class="heading-container clearfix">
                                                                    <div class="pull-right">

                                                                        <input type="button" value="Submit" class="btn btn-primary" name="submit_btn" onclick="smtbtn()">
                                                                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <label class="control-label">Field to Summarise </label>
                                                                    <div class="controls group_total">

                                                                        <span class="control-text summary_field_area" id="myClientName"></span>

                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <label class="control-label">Total Type </label>
                                                                    <div class="controls">
                                                                        <select name="type" class="pass_field" id="mytype">
                                                                            <option value="Total">Total</option>
                                                                            <option value="Average">Average</option>
                                                                            <option value="Count">Count</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <label class="control-label">Report Column Title</label>
                                                                    <div class="controls">
                                                                        <input type="text" name="title" class="pass_field" id="mytitle">
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <label class="control-label">Align</label>
                                                                    <div class="controls">
                                                                        <select name="align" class="pass_field" id="myalign">
                                                                            <option value="Left">Left</option>
                                                                            <option value="Right">Right</option>
                                                                            <option value="Center">Center</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <label class="control-label">Decimal Points</label>
                                                                    <div class="controls">
                                                                        <select name="decimal_pt" class="pass_field" id="mydecimal">
                                                                            <option value=" 0">0</option>
                                                                            <option value="1">1</option>
                                                                            <option value="2">2</option>
                                                                            <option value="3">3</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <label class="control-label">Is Dollar</label>
                                                                    <div class="controls">
                                                                        <label class="radio inline"><input type="radio" value="Yes" name="is_dollar"> Yes</label>
                                                                        <label class="radio inline"><input type="radio" value="No" name="is_dollar" checked="checked"> No</label>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <label class="control-label">Is Percent</label>
                                                                    <div class="controls">
                                                                        <label class="radio inline"><input type="radio" value="Yes" name="is_percent"> Yes</label>
                                                                        <label class="radio inline"><input type="radio" value="No" name="is_percent" checked="checked"> No</label>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                        </div>
                                                    </div>
                                                </div>

                                                <input type="button" class="btn btn-primary" name="add_custom_field_total" value="Add Custom Field Total">

                                            </div>
                                            <div>
                                                <h3>Group Totals</h3>
                                            </div>
                                            <div class="pull-left">
                                                <i>Click and drag rows to re-order the fields</i>

                                            </div>
                                        </div>
                                        <div class="side-nav-content">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>Order <i class="icon icon-chevron-up group_order_dir_btn" order="asc"></i></th>
                                                        <th>Field</th>
                                                        <th>Report Col Title</th>
                                                        <th>Total Type</th>
                                                        <th>Align</th>
                                                        <th>DPS</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody class="nm">
                                                    <tr>
                                                        <td>
                                                            <input type="radio" class="order_radio" name="order" order="asc" value="group_name">
                                                        </td>
                                                        <td colspan="6">Group Name</td>

                                                    </tr>


                                                </tbody>
                                                <tbody class="total_column_wrapper"></tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="side-nav entry_wrapper">
                                        <div class="side-nav-header clearfix">
                                            <div class="pull-right">
                                                <select name="filter_type" id="filter_me">
                                                    <option value="">--- Select Filter ---</option>
                                                    <option value="1" filterType="1">Add Client Filter</option>
                                                    <option value="2" filterType="2">Add Lead Source Filter</option>
                                                    <option value="3" filterType="3">Add Sales Type Filter</option>
                                                    <option value="4" filterType="4">Add Sales Office Filter</option>
                                                    <option value="5" filterType="5">Add Status Filter</option>
                                                    <option value="6" filterType="20">Add Owner Filter</option>
                                                    <option value="7" filterType="10">Add Agent Filter</option>
                                                </select>
                                            </div>
                                            <div class="pull-left">
                                                <h3>Filtering</h3>
                                            </div>
                                        </div>



                                        <div id="textboxes">
                                            <div class="side-nav-content">
                                                <div class="">
                                                    <div class="status_block general_select_block filter_block alert clearfix" filter_field="status_id" id="divClient" style="display: none; padding: 8px; background: transparent; border-color: rgb(213, 213, 213);">
                                                        <div class="control-group filter_type1">
                                                            <label class="control-label">Filter Client</label>
                                                            <div class="controls">
                                                                <span class="icon-button search_client_btn">
                                                                    <i class="icon-search" data-toggle="modal" data-target="#myModal3"></i></span>
                                                                <div class="pull-right" onclick="close_client_me()"><i class="icon-remove remove_btn"></i></div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- Modal -->
                                                    <div class="modal fade" id="myModal3" role="dialog">
                                                        <div class="modal-dialog">

                                                            <!-- Modal content-->
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                    <h4 class="modal-title">Modal Header</h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <p>Some text in the modal.</p>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                </div>
                                                            </div>

                                                        </div>

                                                    </div>
                                                    <!-- Modal end -->

                                                    <div class="item_area" style="padding-left:30px;"></div>


                                                    <div class="status_block general_select_block filter_block alert clearfix" filter_field="status_id" id="divLead" style="display: none; padding: 8px; background: transparent; border-color: rgb(213, 213, 213);">
                                                        <div class="control-group filter_type1">
                                                            <label class="control-label">Lead Source (s)</label>


                                                            <div class="controls">
                                                                <div class="pull-right" onclick="close_lead_me()"><i class="icon-remove remove_btn"></i></div>
                                                                <select name="filter_entity_select" class="span11" id="lorem10">

                                                                    <option value="">--- Select ---</option>


                                                                    <option value="1" onclick="functions('Internet/Website')" id="1opt">Internet/Website </option>


                                                                    <option value="2" onclick="functions('Yellowpages')">Yellowpages </option>
                                                                    <option value="3" onclick="functions('Mail Outs')">Mail Outs</option>
                                                                    <option value="4" onclick="functions('Brochure Drop')">Brochure Drop</option>
                                                                    <option value="5" onclick="functions('Repeat Business')">Repeat Business </option>
                                                                    <option value="6" onclick="functions('Email / Mail Chimp')">Email / Mail Chimp</option>
                                                                    <option value="7" onclick="functions('Tendering')">Tendering</option>
                                                                    <option value="8" onclick="functions('Cold Call List')">Cold Call List</option>
                                                                    <option value="9" onclick="functions('Events / Shows')">Events / Shows</option>
                                                                    <option value="10" onclick="functions('Lead Note')">Lead Note</option>

                                                                </select>
                                                            </div><br>
                                                            <span id="my_lorem1"></span>
                                                        </div>
                                                    </div>


                                                    <div class="status_block general_select_block filter_block alert clearfix" filter_field="status_id" id="divSales" style="display: none; padding: 8px; background: transparent; border-color: rgb(213, 213, 213);">
                                                        <div class="control-group filter_type1">
                                                            <label class="control-label"> Sales Type (s)</label>
                                                            <div class="controls">
                                                                <div class="pull-right" onclick="close_sales_me()"><i class="icon-remove remove_btn"></i></div>
                                                                <select name="filter_entity_select" class="span11">
                                                                    <option value="">--- Select ---</option>
                                                                    <option value="1" onclick="functions2('Home Builders')">Home Builders </option>
                                                                    <option value="2" onclick="functions2('Home Owners')"> Home Owners</option>
                                                                    <option value="3" onclick="functions2('Multi-Dwelling Builders')">Multi-Dwelling Builders</option>
                                                                </select>

                                                            </div><br>
                                                            <span id="my_lorem2"></span>
                                                        </div>
                                                    </div>
                                                    <div class="item_area" style="padding-left:30px;"></div>

                                                    <div class="status_block general_select_block filter_block alert clearfix" filter_field="status_id" id="divOffice" style="display: none; padding: 8px; background: transparent; border-color: rgb(213, 213, 213);">
                                                        <div class="control-group filter_type1">
                                                            <label class="control-label">Sales Office (s)</label>
                                                            <div class="controls">
                                                                <div class="pull-right" onclick="close_sales_off()"><i class="icon-remove remove_btn"></i></div>
                                                                <select name="filter_entity_select" class="span11">
                                                                    <option value="">--- Select ---</option>
                                                                    <option value="1" onclick="functions3('Paint Professionals')">Paint Professionals</option>
                                                                </select>
                                                            </div><br>
                                                            <span id="my_lorem3"></span>
                                                        </div>
                                                    </div>

                                                    <div class="item_area" style="padding-left:30px;"></div>


                                                    <div class="status_block general_select_block filter_block alert clearfix" filter_field="status_id" id="divStatus" style="display: none; padding: 8px; background: transparent; border-color: rgb(213, 213, 213);">
                                                        <div class="control-group filter_type1">
                                                            <label class="control-label">Status (s)</label>
                                                            <div class="controls">
                                                                <div class="pull-right" onclick="close_status()"><i class="icon-remove remove_btn"></i></div>
                                                                <select name="filter_entity_select" class="span11">
                                                                    <option value="">--- Select ---</option>
                                                                    <option value="1" onclick="functions4('Accepted')">Accepted</option>
                                                                    <option value="2" onclick="functions4('Declined')">Declined</option>
                                                                    <option value="3" onclick="functions4('New')">New</option>
                                                                    <option value="4" onclick="functions4('Offered')">Offered </option>
                                                                    <option value="5" onclick="functions4('Open')">Open</option>
                                                                </select>
                                                            </div><br>
                                                            <span id="my_lorem4"></span>
                                                        </div>
                                                    </div>

                                                    <div class="item_area" style="padding-left:30px;"></div>

                                                    <div class="status_block general_select_block filter_block alert clearfix" filter_field="status_id" id="divOwner" style="display: none; padding: 8px; background: transparent; border-color: rgb(213, 213, 213);">
                                                        <div class="control-group filter_type1">
                                                            <label class="control-label">Owner (s)</label>
                                                            <div class="controls">
                                                                <div class="pull-right" onclick="close_owner()"><i class="icon-remove remove_btn"></i></div>
                                                                <select name="field3" id="drpdownOwner" class=" drpdownOwner span11">
                                                                    <option value="">--- Select ---</option>

                                                                </select>

                                                            </div><br>
                                                            <span id="my_lorem5"></span>
                                                        </div>
                                                    </div>

                                                    <div class="item_area" style="padding-left:30px;"></div>
                                                    <div class="status_block general_select_block filter_block alert clearfix" filter_field="status_id" id="divAgent" style="display: none; padding: 8px; background: transparent; border-color: rgb(213, 213, 213);">

                                                        <div class="control-group filter_type1">
                                                            <label class="control-label">Agent (s)</label>
                                                            <div class="controls">
                                                                <div class="pull-right" onclick="close_agent()"><i class="icon-remove remove_btn"></i></div>
                                                                <select name="field2" id="drpdownAgent" class=" span11 drpdownAgent">
                                                                    <option value="">--- Select ---</option>
                                                                </select>

                                                            </div><br>
                                                            <span id="my_lorem6"></span>
                                                        </div>
                                                    </div>
                                                    <div class="item_area" style="padding-left:30px;"></div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="side-nav entry_wrapper">
                                        <div class="side-nav-header clearfix">
                                            <div class="pull-left">
                                                <h3>Printing and Export Settings</h3>
                                            </div>
                                        </div>
                                        <div class="side-nav-content forty">
                                            <div class="control-group task_name">
                                                <label class="control-label">Page Size</label>
                                                <div class="controls">
                                                    <select name="page_size" required>
                                                        <option value="A4">A4</option>
                                                        <option value="A3">A3</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="control-group task_name">
                                                <label class="control-label">Orientation</label>
                                                <div class="controls">
                                                    <select name="page_orien" required>
                                                        <option value="L">Landscape</option>
                                                        <option value="P">Portrait</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="control-group task_name">
                                                <label class="control-label">Page Break by Group</label>
                                                <div class="controls">
                                                    <label class="radio inline"><input type="radio" name="page_break" value="YES"> Yes</label>
                                                    <label class="radio inline"><input type="radio" name="page_break" value="NO" checked> No</label>

                                                </div>
                                            </div>
                                            <div class="control-group task_name">
                                                <label class="control-label">Hide Group Summaries on PDF/CSV</label>
                                                <div class="controls">
                                                    <label class="radio inline"><input type="radio" name="hide_group" value="YES"> Yes</label>
                                                    <label class="radio inline"><input type="radio" name="hide_group" value="NO" checked> No</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="heading-container clearfix" style="max-height: calc(100% - 60px); overflow: auto;">
                            <div class="clearfix">
                                <div class="pull-left"><b>Report Fields</b></div>
                                <div class="pull-right">
                                    <input type="button" class="btn btn-inverse" value="Select System Fields" name="sys_field_select">
                                    <input type="button" class="btn btn-inverse" value="Select Custom Fields" name="custom_field_select">
                                </div>
                            </div>
                            <div>
                                <div class="no_field_select alert alert-info" style="display: block;">No Field Selected</div>
                                <div class="sys_field_wrapper">
                                    <div class="sys_field_tmpl" style="display: none;">
                                        <div class="btn-group" style="min-width: 200px;margin-bottom: 5px;">
                                            <button class="btn btn-mini btn-danger remove_btn" title="Remove Field"><i class="icon-white icon-remove"></i></button>
                                            <button class="btn btn-mini fieldname" style="width:110px"></button>
                                            <button class="btn btn-mini move_left_btn" title="Move field to the left"><i class="icon-chevron-left"></i></button>
                                            <button class="btn btn-mini dropdown-toggle sort_btn" data-toggle="dropdown" title="Order by this field"><i class="icon-chevron-down icon_btn"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div style="text-align:center;"><i>Resize report table column widths by clicking and dragging the column separators below.</i></div>
                            <br>
                            <div class="resize_table_wrapper">
                                <div class="rc-handle-container" style="width: 1083px;"></div>
                                <table class="table table-bordered" id="resize_table_1605790366913" data-resizable-columns-id="resize_table_1605790366913" style="margin-bottom: 0px;">
                                    <thead>
                                        <tr></tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="span2 desktop_dash_board_left pull-left ps-container ps-theme-default" id="cweb_sidebar" data-ps-id="d4ad0803-cf9d-4b4e-6d99-966db4f5ea8d" style="position: relative; overflow: hidden;">
                    <div class="site_info">
                        <img src="http://paintpad.mysys.com.au/templates/login/img/logo.png" width="240px" height="90px" alt="Logo" class="logo">
                        <h4 class="date">
                            <?php $d = new DateTime('now');
                            echo $d->format('d F,Y') ; ?>
                        </h4>
                        <div>
                            <div class="user_profile_detail_area">
                                <div id="cweb_user_panel_detail">
                                    <div class="" style="margin-bottom:20px;">
                                        <ul class="no-list list-left">
                                            <li><strong>User:</strong> <span>Martin Penney</span></li>
                                            <li><strong>Title:</strong> <span>Licensed Building Supervisor</span></li>
                                            <li><strong>Phone:</strong> <span>1800 686 525</span></li>
                                            <li><strong>Mobile:</strong> <span>0412 578 277</span></li>
                                            <li><strong>Email:</strong> <span>martin@paintprofessionals.com.au<br>---</span></li>
                                        </ul>
                                        <br>
                                        <ul class="no-list list-left">
                                            <li><strong>Role:</strong> <span>Marty</span></li>
                                        </ul>

                                        <hr style="margin: 14px 0;">

                                        <div class="clearfix">
                                            <a href="#" style="display:inline-block; float:left; margin: 0 1em 5px 0;" class="btn btn-inverse btn-mini view_response_btn">Responsibilities</a>
                                            <a href="#" style="display:inline-block; float:left;" class="btn btn-inverse btn-mini view_permission_btn">Permissions</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="score_board_area"></div>
                            <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px;">
                                <div class="ps-scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div>
                            </div>
                            <div class="ps-scrollbar-y-rail" style="top: 0px; right: 0px;">
                                <div class="ps-scrollbar-y" tabindex="0" style="top: 0px; height: 0px;"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="windowUnderlay" style="height: 440px; opacity: 0.01; display: none; z-index: 109;"></div>
            </div>

            <div class="activeWindows" id="dockWrapper" style="display: none;">
                <div class="container dock">
                    <ul class="clearfix" id="dock"></ul>
                    <div class="ui_toggling_container switch_min_container">
                        <span id="switch_min_panel_btn" class="" data-desc="Minimize / Restore 
 all open windows"><i id="switch_min_panel_icon" class="icon-arrow-down icon-white"></i></span>
                    </div>
                </div>
            </div>


            <div id="time_adv_log_console" style="display: none; top: 50px; z-index: 9999999;">
                <table class="table table-condensed table-transparent">
                    <tbody>
                        <tr class="timer_log_tmpl" style="display:none;" state="ready_to_go">
                            <td class="timer_log_info">
                                <div class="entity_name_area control-text"></div>
                                <div class="entity_desc_area small well-small"></div>
                            </td>
                            <td><span class="total_area control-text"></span></td>
                            <td class="timer_start_pause">
                                <input type="button" name="start" value="Start" class="btn btn-success control_button">
                                <input type="button" name="pause" value="Pause" class="btn btn-warning control_button" style="display:none;">
                            </td>
                            <td class="timer_stop_review">
                                <input type="button" name="stop" value="Stop &amp; Review" class="btn btn-primary control_button" style="display:none;">
                            </td>
                            <td class="timer_remove">
                                <span class="remove icon-button icon-button-danger" title="Remove / Cancel"><i class="icon-remove icon-white"></i></span>
                            </td>
                        </tr>
                        <tr class="hide">
                            <td colspan="2"></td>
                            <td class="breakdown" colspan="3">
                                <table class="table table-condensed table-transparent">
                                    <tbody>
                                        <tr class="timer_history_tmpl" link_type="" link_id="" style="display:none;">
                                            <td class=""></td>
                                            <td class="history_duration_area"></td>
                                            <td class="history_area small"></td>
                                            <td class="" colspan="2">
                                                <i class="finish_icon small pull-right" style="display:none;">Lodged <img src="http://paintpad.mysys.com.au/components/com_cweb/images/ok.png" class="finish_icon"></i>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>

                <?php ActiveForm::end(); ?>
            </div>


            <script>
                //script for close filtering 
                // by Somesh Vashist

                function close_client_me() {
                    var lead_menu = document.getElementById("divClient").style.display = "none";
                }

                function close_lead_me() {
                    var lead_menu = document.getElementById("divLead").style.display = "none";
                }

                function close_sales_me() {
                    var lead_menu = document.getElementById("divSales").style.display = "none";
                }

                function close_sales_off() {
                    var lead_menu = document.getElementById("divOffice").style.display = "none";
                }

                function close_status() {
                    var lead_menu = document.getElementById("divStatus").style.display = "none";
                }

                function close_owner() {
                    var lead_menu = document.getElementById("divOwner").style.display = "none";
                }

                function close_agent() {
                    var lead_menu = document.getElementById("divAgent").style.display = "none";
                }


                //script to set date range .
                //by Somesh Vashist

                function change_mee() {
                    var f = document.getElementById("filter_mee").value;

                    if (!(f == "0")) {
                        $(".zx").hide();
                    } else {
                        $(".zx").show();
                    }

                }


                //from date settings
                //by Somesh Vashist
                function change_frm() {
                    var r = document.getElementById("filter_myfrm").value;

                    if (!(r == "0")) {
                        $(".frm").hide();
                    } else {
                        $(".frm").show();
                    }

                }
                //to date settings
                //by Somesh Vashist
                function change_to() {
                    var t = document.getElementById("filter_myto").value;

                    if (!(t == "0")) {
                        $(".to").hide();
                    } else {
                        $(".to").show();
                    }

                }

                //script for choose system type filter dropdown .
                //by Somesh Vashist

                function groupfilter() {
                    var group = document.getElementById("filter_group").value;

                    if (group == "2") {
                        $(".type_client").show();
                        $(".type_contact_person").hide();
                        $(".type_project").hide();

                    } else if (group == "3") {
                        $(".type_client").hide();
                        $(".type_contact_person").show();
                        $(".type_project").hide();

                    } else if (group == "4") {
                        $(".type_client").hide();
                        $(".type_contact_person").hide();
                        $(".type_project").show();
                    } else {
                        $(".common").show();
                    }

                }

                //function change_meee for group by 
                // by Somesh Vashist

                function change_meee() {
                    var f = document.getElementById("filter_meee").value;
                    //alert(f);

                    if (f == "3") {
                        $(".dz").show();
                        $(".dy").hide();
                        $(".dd").hide();
                    } else if (f == "2") {
                        $(".dy").show();
                        $(".dz").hide();
                        $(".dd").hide();
                    } else if (f == "9") {
                        $(".dd").show();
                        $(".dy").hide();
                        $(".dz").hide();
                    } else {
                        $(".dz , .dy , .dd").hide();
                    }
                }

                function click_me() {
                    document.getElementById("btn").innerHTML = ".gx";

                    //alert('jj');

                }




                $(function() { // script to get current date 
                    $("#date_from").datepicker({
                        dateFormat: 'yy-mm-dd'
                    });
                });
                $(function() {
                    $("#date_to").datepicker({
                        dateFormat: 'yy-mm-dd'
                    });
                });

            </script>


            <script>
                //script for group totals
                function show_data(my_name) { //to get the value of client
                    $("#myClientName").html(my_name);
                    $('#myModal').modal('hide');
                    $('#myModal2').modal('show');
                }

                function smtbtn() //to submit the form after popup
                {
                    var user_name = $("#myClientName").html();
                    var type = $("#mytype").val();
                    var title = $("#mytitle").val();
                    var align = $("#myalign").val();
                    var decimal = $("#mydecimal").val();
                    var dollar = $("input[name='is_dollar']:checked").val();
                    var percent = $("input[name='is_percent']:checked").val();
                    //$(".client_client").html(user_name);
                    // $('.xyz').val(user_name);
                    $('.domHTml').append('<input type="hidden" name="field[]" value="' + user_name + '"><input type="hidden" name="title_data[]" value="' + title + '"><input type="hidden" name="type_data[]" value="' + type + '"><input type="hidden" name="align_data[]" value="' + align + '"><input type="hidden" name="decimal_data[]" value="' + decimal + '"><input type="hidden" name="dollar_data[]" value="' + dollar + '"><input type="hidden" name="percent_data[]" value="' + percent + '">');


                    $('.nm').append(' <tr class="closegroups" id="' + user_name + '"><td><input type="radio" class="order_radio" name="order" value="' + user_name + '"></td><td><span class="client_client">' + user_name + '</span></td><td><span class="title_title">' + title + '</span></td><td><span class="type_type">' + type + '</span></td><td><span class="align_align">' + align + '</span></td> <td><span class="decimal_decimal">' + decimal + '</span></td><td><span data-name="' + user_name + '"><button type="button" class="closegroup">&times;</button></span></td><input type="hidden" name="is_dollar"><input type="hidden" name="is_percent"></tr>');

                    $('#myModal2').modal('hide');



                }

                $('body').on('change', '.order_radio', function() //script for radio button
                    {
                        jQuery(".order_radio").prop("checked", false);
                        jQuery(this).prop("checked", true);
                        jQuery(".hidden_order").val(jQuery(this).val());

                        console.log(jQuery(this).val());
                    });


                //script to close grouptotals
                $('body').on('click', '.closegroup', function() {
                    $(this).closest("tr").remove();

                });

            </script>



            <!--script for filtering subitems
        By Somesh Vashist-->
            <script>
                var abc = 1;

                function functions(my_val) {
                    var my_id = "my_lorem" + abc;
                    $("#my_lorem1").append('<span class="btn btn-mini disabled pull-left" id="my_id">' + my_val + '<button type="button" class="closespan">&times;</button> </span>');

                }

                function functions2(my_val) {
                    var my_id1 = "my_lorem0" + abc;
                    $("#my_lorem2").append('<span class="btn btn-mini disabled pull-left" id="my_id1">' + my_val + '<button type="button" class="closespan">&times;</button></span>');

                }


                function functions3(my_val) {
                    var my_id2 = "my_lorem1" + abc;
                    $("#my_lorem3").append('<span class="btn btn-mini disabled pull-left" id="my_id2">' + my_val + ' <button type="button" class="closespan">&times;</button></span>');

                }

                function functions4(my_val) {
                    var my_id3 = "my_lorem2" + abc;
                    $("#my_lorem4").append('<span class="btn btn-mini disabled pull-left" id="my_id3">' + my_val + '<button type="button" class="closespan">&times;</button></span>');

                }

                function functions5(my_val) {
                    var my_id4 = "my_lorem3" + abc;
                    $("#my_lorem5").append('<span class="btn btn-mini disabled pull-left" id="my_id4">' + my_val + '<button type="button" class="closespan">&times;</button></span>');

                }

                function functions6(my_val) {
                    var my_id5 = "my_lorem4" + abc;
                    $("#my_lorem6").append('<span class="btn btn-mini disabled pull-left" id="my_id5">' + my_val + '<button type="button" class="closespan">&times;</button></span>');

                }

                //script to close subdropdown on filtering
                $('body').on('click', '.closespan', function() {
                    $(this).closest("span").remove();

                });

            </script>


            <!--
            script for filtering 
            by Somesh Vashist
-->
            <script>
                $(document).ready(function() {
                    $('body').on('change', '#filter_me', function() {
                        var role = $('option:selected', this).attr('filterType');
                        //alert(role);
                        if ((role == 20) || (role == 10)) {
                            sendFirstCategory(role);
                        } else if (role == 1) {
                            $('#divClient').show();

                        } else if (role == 2) {
                            //$('#lead_menu').show
                            $('#divLead').show();

                        } else if (role == 3) {
                            $('#divSales').show();

                        } else if (role == 4) {
                            $('#divOffice').show();

                        } else if (role == 5) {
                            $('#divStatus').show();

                        }
                    });


                });

                function sendFirstCategory(role) { //function for owner & agent 

                    $.ajax({
                        type: "POST",
                        url: "<?php echo Yii::$app->getUrlManager()->createUrl('report/role')  ; ?>",
                        data: {
                            role: role
                        },
                        success: function(result) {
                            if (role == 10) {
                                console.log(role);

                                $('#divAgent').show();
                                $('#drpdownAgent').html(result);
                            } else if (role == 20) {
                                $('#divOwner').show();
                                $('#drpdownOwner').html(result);
                            }
                            console.log(role);
                        },
                        error: function(exception) {
                            alert(exception);
                        }
                    });
                }

            </script>
