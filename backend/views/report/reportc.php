<?php

namespace backend\controllers;

use Yii;
use app\models\TblProjects;
use app\models\TblReportGroups;
use app\models\TblGroupTotals;
use app\models\TblReportFilter;
use app\models\TblGroupFields;
use app\models\TblUsers;
use app\models\Child;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\TblBrands;
use app\models\TblSheen;
use yii\helpers\Url;
use yii\jui\DatePicker;
use wbraganca\dynamicform\DynamicFormWidget;


class ReportController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    
     /**
     * 
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
     /**
     * 
     * @return mixed
     */
    

    /**  
     * function actionProjects() to insert data 
     * by Somesh Vashist
     */   

    public function actionProjects()   
    {   
        
        
//         $username = TblUsers::find('user_id,username')->all();
//        return $this->render('projects',['owner'=>$username ,'agent'=>$username]);
       
        if(!empty(Yii::$app->request->post())){
            
//print_r(Yii::$app->request->post());
//           die();
//            
           //tblprojects
            $report_name  = !empty(Yii::$app->request->post('report_name'))?Yii::$app->request->post('report_name'):''; 
            $date_range  = !empty(Yii::$app->request->post('date_range'))?Yii::$app->request->post('date_range'):'0';
            $from  = !empty(Yii::$app->request->post('from'))?Yii::$app->request->post('from'):'';
            $date_from  = !empty(Yii::$app->request->post('date_from'))?Yii::$app->request->post('date_from'):'';
            $to  = !empty(Yii::$app->request->post('to'))?Yii::$app->request->post('to'):'';
            $date_to  = !empty(Yii::$app->request->post('date_to'))?Yii::$app->request->post('date_to'):''; 
            
            
            
            if(isset($_POST['date_from']) and $_POST['date_from'] != "")
            {
                $from = 0;
            }
            
            $page_size  = !empty(Yii::$app->request->post('page_size'))?Yii::$app->request->post('page_size'):''; 
            $page_orien  = !empty(Yii::$app->request->post('page_orien'))?Yii::$app->request->post('page_orien'):'';
            $page_break  = !empty(Yii::$app->request->post('page_break'))?Yii::$app->request->post('page_break'):''; 
            $hide_group  = !empty(Yii::$app->request->post('hide_group'))?Yii::$app->request->post('hide_group'):'';
            
            
            //tblreportgroups
            $report_id = !empty(Yii::$app->request->post('id'))?Yii::$app->request->post('id'):'';
            $group_by = !empty(Yii::$app->request->post('group_by'))?Yii::$app->request->post('group_by'):''; 
            $group_by_range  = !empty(Yii::$app->request->post('group_by_range'))?Yii::$app->request->post('group_by_range'):'';
            
            
            //tblgrouptotals
            $report_id  = !empty(Yii::$app->request->post('report_id'))?Yii::$app->request->post('report_id'):''; 
            $field  = !empty(Yii::$app->request->post('field'))?Yii::$app->request->post('field'):'';
            $type  = !empty(Yii::$app->request->post('type'))?Yii::$app->request->post('type'):'';
            $title  = !empty(Yii::$app->request->post('title'))?Yii::$app->request->post('title'):'';
            $align  = !empty(Yii::$app->request->post('align'))?Yii::$app->request->post('align'):'';
            $decimal_pt  = !empty(Yii::$app->request->post('decimal_pt'))?Yii::$app->request->post('decimal_pt'):''; 
            $is_dollar  = !empty(Yii::$app->request->post('is_dollar'))?Yii::$app->request->post('is_dollar'):''; 
            $is_percent  = !empty(Yii::$app->request->post('is_percent'))?Yii::$app->request->post('is_percent'):'';
            
            $order  = !empty(Yii::$app->request->post('order'))?Yii::$app->request->post('order'):'';
            
          //TblReportFilter
             $report_id  = !empty(Yii::$app->request->post('report_id'))?Yii::$app->request->post('report_id'):''; 
            $filter_type  = !empty(Yii::$app->request->post('filter_type'))?Yii::$app->request->post('filter_type'):'';



           // print_r(Yii::$app->request->post('page_size'));
            
            
         //tblprojects  
        $TblProjects = new TblProjects(); 

        $TblProjects->report_name          = $report_name;
        $TblProjects->date_range           = $date_range;
        $TblProjects->from                 = $from;
        $TblProjects->date_from            = $date_from;
        $TblProjects->to                   = $to;
        $TblProjects->date_to              = $date_to;
        $TblProjects->page_size            = $page_size;
        $TblProjects->page_orien           = $page_orien;
        $TblProjects->page_break           = $page_break;
        $TblProjects->hide_group           = $hide_group;
        $TblProjects->date_created = date('y-m-d h:i:s', time());// current date
            
        
            
            
             $TblProjects->save();
//echo "<pre>";print_r($TblProjects);die;   
        if($TblProjects->save(false)){  //save data to db

            $Id = $TblProjects->id;
            
             //tblreportgroups   
        $TblReportGroups = new TblReportGroups(); 
            
        $TblReportGroups->report_id           = $Id;
        $TblReportGroups->group_by            = $group_by;
        $TblReportGroups->group_by_range      = $group_by_range;
            
              //tblreportgroups
                $TblReportGroups->save();
            
             //TblReportFilter
             $TblReportFilter = new TblReportFilter(); 
            
                $TblReportFilter->report_id           = $Id;
                $TblReportFilter->filter_type         = $filter_type;
            
                $TblReportFilter->save();

          foreach($field as $ord) {  
//              print_r($ord);
//              die();
         //tblgrouptotals                
        $TblGroupTotals = new TblGroupTotals(); 
            
        $TblGroupTotals->report_id             = $Id;
        $TblGroupTotals->field                 = $ord;
        $TblGroupTotals->type                  = $type;
        $TblGroupTotals->title                 = $title;
        $TblGroupTotals->align                 = $align;
        $TblGroupTotals->decimal_pt            = $decimal_pt;
        $TblGroupTotals->is_dollar             = $is_dollar;
        $TblGroupTotals->is_percent            = $is_percent;
              
              if($ord == $order) 
              {
                  $TblGroupTotals->order            = 1;

              } else 
              {
                  $TblGroupTotals->order            = 0;

              }
              
             //tblgrouptotals
                           
//print_r();
//              die();
                $TblGroupTotals->save();
          }
            
           
            
        
            //tblprojects
                $arr['id']                  = $Id;
                $arr['report_name']         = $report_name;
                $arr['date_range']          = $date_range;
                $arr['from']                = $from;
                $arr['date_from']           = $date_from;
                $arr['to']                  = $to;
                $arr['date_to']             = $date_to;
                $arr['page_size']           = $page_size;
                $arr['page_orien']          = $page_orien;
                $arr['page_break']          = $page_break;
                $arr['hide_group']          = $hide_group;
            
             
            
              //reportgroups
            
                $arr['id']                  = $Id;
                $arr['report_id']           = $report_id;
                $arr['group_by']            = $group_by;
                $arr['group_by_range']      = $group_by_range;
            
            
            
               
            //tblgrouptotals
            
                $arr['id']                  = $Id;
                $arr['report_id']           = $report_id;
                $arr['field']               = $field;
                $arr['type']                = $type;
                $arr['title']               = $title;
                $arr['align']               = $align;
                $arr['decimal_pt']          = $decimal_pt;
                $arr['is_dollar']           = $is_dollar;
                $arr['is_percent']          = $is_percent;
            
            if($ord == $order) 
            {
                  $arr['order']               = 1;

            }
            else
            {
                  $arr['order']               = 0;

            }
            
            //TblReportFilter
                $arr['id']                  = $Id;
                $arr['report_id']           = $report_id;
                $arr['filter_type']         = $filter_type;
            
               
            
            
            return $this->render('projects', [
          
            'value' => $arr // 'grps'=>$groups
            
        ]);
        }
      
        

    // else {
    //     echo "Please enter valid data";
    //     $arr = $TblProjects->errors;
    // }
        


        }
        else{
            return $this->render('projects');

        }
        
        
       
  
       
    }
    
    //function for owner & agent for filtering section.
    //by Somesh Vashist
    
     public function actionRole() {
    $data = Yii::$app->request->post('role');
    if (isset($data)) {
    $users = TblUsers::find()->where(['role'=> $data])->all();
    $option = '<option value="">--- Select ---</option>';
       
    foreach($users as $user):
        $username = $user['username'];
        $user_name ="'$username'";
   $option .= '<option value='.$username.' onclick=functions5('.$user_name.')>'.$username.'</option>';
    endforeach;
    return \yii\helpers\Json::encode($option);
    } else {
    $role = "Ajax failed";
    }
    return \yii\helpers\Json::encode($role);
    }

   

}

?>