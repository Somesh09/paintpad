<?php
use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\bootstrap\ButtonDropdown;
use dosamigos\grid\GridView;
use dosamigos\exportable\ExportableButton; 
use kartik\export\ExportMenu;
$reports = (new \yii\db\Query())
    ->select(['id', 'report_name','date_range','from', 'date_from','to', 'date_to', 'page_size', 'page_orien', 'page_break', 'hide_group','date_created'])
    ->from('tbl_projects')
    ->all();

    $reports1=[];
    foreach ($reports as $ke => $valu)
     {
      $reports1[$valu['id']]['id'] = $valu['id'];
      $reports1[$valu['id']]['report_name'] = $valu['report_name'];
         $reports1[$valu['id']]['date_range'] = $valu['date_range'];
        
        
        
//                    echo date("Y-m-d") . "<br>";
//        $d= strtotime("+". $valu['date_range'] ." Days")
////$d=strtotime("+2 Days");
//echo date("Y-m-d", $d) . "<br>";
        $reports1[$valu['id']]['from'] = $valu['from'];
        
        
      $reports1[$valu['id']]['date_from'] = $valu['date_from'];
//        $reports1[$valu['id']]['date_from'] = date('y-m-d');
//         echo date('y-m-d');
//        if($valu['report_name']=='jhjh') {
//        echo $valu['date_range'];
//             print_r(date('Y-m-d', strtotime("+". $valu['date_range'] ." Days")));
//        die();
            
            

}
        $reports1[$valu['id']]['to'] = $valu['to'];
            //date('Y-m-d', strtotime("+". $valu['date_range'] ." Days"));
        
//        print_r(date('Y-m-d', strtotime("+". $valu['date_range'] ." Days")));
//        die();
        
      $reports1[$valu['id']]['date_to'] = $valu['date_to'];
      $reports1[$valu['id']]['page_size'] = $valu['page_size'];
      $reports1[$valu['id']]['page_orien'] = $valu['page_orien'];
      $reports1[$valu['id']]['page_break'] = $valu['page_break'];
      $reports1[$valu['id']]['hide_group'] = $valu['hide_group'];
      $reports1[$valu['id']]['date_created'] = $valu['date_created'];
     
// print_r($reports);
// die();
    
    
  ?>

<table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>ID</th>
                <th>Report Name</th>
                 <th>Date Range</th>
                <th>From</th>
                <th>From Date</th>
               
                <th>To</th>
                <th>To Date</th>
                <th>Page Size</th>
                <th>Orientation</th>
                <th>Page Break</th>
                <th>Hide Group</th>
                <th>Created Date</th>
<!--
                <th>Created By</th>
                <th>Send Email</th>
                <th>Frequency</th>
                <th>Initial Date</th>
                <th>Last Time Sent</th>
-->
                <th>Actions</th>
                
            </tr>
        </thead>
        <tbody>
        <?php foreach($reports as $key => $value ){ ?>
            <tr>
                <td><?php echo $value['id']; ?></td>
                <td><?php echo $value['report_name']; ?></td>
                 <td><?php echo $value['date_range']; ?></td>
                <td><?php echo $value['from']; ?></td>
                 <td><?php echo $value['date_from']; ?></td>
                 <td><?php echo $value['to']; ?></td>
                <td><?php echo $value['date_to']; ?></td>
                <td><?php echo $value['page_size']; ?></td>
                <td><?php echo $value['page_orien']; ?></td>
                <td><?php echo $value['page_break']; ?></td>
                <td><?php echo $value['hide_group']; ?></td>
                <td><?php echo $value['date_created']; ?></td>
               
                <td><button> Preview</button></td>
            </tr>

          <?php  } ?>
            
            
           
        </tbody>
        <tfoot>
            <tr>
               <th>ID</th>
                <th>Report Name</th>
                 <th>Date Range</th>
                <th>From</th>
                <th>From Date</th>
                <th>To</th>
                <th>To Date</th>
                <th>Page Size</th>
                <th>Orientation</th>
                <th>Page Break</th>
                <th>Hide Group</th>
                <th>Created Date</th>
<!--
                <th>Created By</th>
                <th>Send Email</th>
                <th>Frequency</th>
                <th>Initial Date</th>
                <th>Last Time Sent</th>
-->
                <th>Actions</th>
            </tr>
        </tfoot>
    </table>


