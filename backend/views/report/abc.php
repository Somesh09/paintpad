<?php
use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\widgets\ActiveForm;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\bootstrap\ButtonDropdown;
use dosamigos\grid\GridView;
use dosamigos\exportable\ExportableButton; 
use kartik\export\ExportMenu;

// global 
// global $cRole = "";
?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-gb" lang="en-gb" dir="ltr" slick-uniqueid="3"><head>
	  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  <meta name="robots" content="noindex,follow">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=5, minimum-scale= 1.0">
  <meta name="generator" content="MySys - www.mysys.com.au">
  <title>PaintPad - 
									Projects
																									
																 - Report</title>
  <link href="/templates/mylogin/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon">
  <link rel="stylesheet" href="http://paintpad.mysys.com.au/components/com_cweb/js/lightface/style/LightFace.css" type="text/css">
  <link rel="stylesheet" href="http://paintpad.mysys.com.au/components/com_cweb/css/user.css" type="text/css">
  <link rel="stylesheet" href="http://paintpad.mysys.com.au/components/com_cweb/css/datepicker_vista.css" type="text/css">
  <link rel="stylesheet" href="http://paintpad.mysys.com.au/components/com_cweb/js/mooRainbow/mooRainbow.css" type="text/css">
  <link rel="stylesheet" href="http://paintpad.mysys.com.au/components/com_cweb/js/MooEditable/assess/MooEditable.css" type="text/css">
  <link rel="stylesheet" href="http://paintpad.mysys.com.au/components/com_cweb/js/mocha-ui/plugins/tree/css/style.css" type="text/css">
  <link rel="stylesheet" href="http://paintpad.mysys.com.au/components/com_cweb/css/mootree.css" type="text/css">
  <link rel="stylesheet" href="http://paintpad.mysys.com.au/components/com_cweb/js/formcheck/theme/classic/formcheck.css" type="text/css">
  <link rel="stylesheet" href="http://paintpad.mysys.com.au/components/com_cweb/css/selectoMoo.css" type="text/css">
  <link rel="stylesheet" href="http://paintpad.mysys.com.au/components/com_cweb/js/uploader_master/fineuploader.css" type="text/css">
  <link rel="stylesheet" href="http://paintpad.mysys.com.au/components/com_cweb/js/fullcalendar-2.1.1/fullcalendar.css" type="text/css">
  <link rel="stylesheet" href="http://paintpad.mysys.com.au/components/com_cweb/js/jquery-ui-1.11.3.custom/jquery-ui.css" type="text/css">
  <link rel="stylesheet" href="http://paintpad.mysys.com.au/components/com_cweb/js/scroll/perfect-scrollbar.min.css" type="text/css">
  <link rel="stylesheet" href="http://paintpad.mysys.com.au/components/com_cweb/css/boost/bootstrap.css" type="text/css">
  <link rel="stylesheet" href="http://paintpad.mysys.com.au/components/com_cweb/css/boost/elos.css?nocache=1605787243" type="text/css">
  <link rel="stylesheet" href="http://paintpad.mysys.com.au/components/com_cweb/css/boost/paint.css?nocache=1605787243" type="text/css">
  <link rel="stylesheet" href="http://paintpad.mysys.com.au/components/com_cweb/js/jqdatetimepicker/jquery.datetimepicker.min.css" type="text/css">
  <link rel="stylesheet" href="http://paintpad.mysys.com.au/components/com_cweb/css/boost/d3.css?nocache=1605787243" type="text/css">
  <script async="" src="//www.google-analytics.com/analytics.js"></script><script src="http://paintpad.mysys.com.au/components/com_cweb/js/mootools-nocompat-1.4.5.js" type="text/javascript"></script>
  <script src="http://paintpad.mysys.com.au/components/com_cweb/js/mootools-more-1.4.0.1.js" type="text/javascript"></script>
  <script src="http://paintpad.mysys.com.au/components/com_cweb/js/fullcalendar/jquery/jquery-1.8.1.min.js" type="text/javascript"></script>
  <script src="http://paintpad.mysys.com.au/components/com_cweb/js/fullcalendar/jquery/jquery-noconflick.js" type="text/javascript"></script>
  <script src="http://paintpad.mysys.com.au/components/com_cweb/js/moment.min.js" type="text/javascript"></script>
  <script src="http://paintpad.mysys.com.au/components/com_cweb/js/scroll/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
  <script src="http://paintpad.mysys.com.au/components/com_cweb/js/jquery.floatThead.min.js" type="text/javascript"></script>
  <script src="http://paintpad.mysys.com.au/components/com_cweb/js/jquery.autosize.min.js" type="text/javascript"></script>
  <script src="http://paintpad.mysys.com.au/components/com_cweb/js/fullcalendar-2.1.1/fullcalendar.js" type="text/javascript"></script>
  <script src="http://paintpad.mysys.com.au/components/com_cweb/js/zeroclipboard/ZeroClipboard.js" type="text/javascript"></script>
  <script src="http://paintpad.mysys.com.au/components/com_cweb/js/jquery-ui-1.11.3.custom/jquery-ui.js" type="text/javascript"></script>
  <script src="http://paintpad.mysys.com.au/components/com_cweb/js/Stickman.MultiUpload.js" type="text/javascript"></script>
  <script src="http://paintpad.mysys.com.au/components/com_cweb/js/mocha-ui/scripts_new/Core/Core.js?1605787243" type="text/javascript"></script>
  <script src="http://paintpad.mysys.com.au/components/com_cweb/js/mocha-ui/scripts_new/Layout/Layout.js?nocache=1605787243" type="text/javascript"></script>
  <script src="http://paintpad.mysys.com.au/components/com_cweb/js/mocha-ui/scripts_new/Layout/Dock.js?1605787243" type="text/javascript"></script>
  <script src="http://paintpad.mysys.com.au/components/com_cweb/js/mocha-ui/scripts_new/Window/Window.js?nocache=1605787243" type="text/javascript"></script>
  <script src="http://paintpad.mysys.com.au/components/com_cweb/js/mocha-ui/scripts_new/Window/Modal.js?1605787243" type="text/javascript"></script>
  <script src="http://paintpad.mysys.com.au/components/com_cweb/js/mocha-ui/scripts_new/Utilities/Themes.js" type="text/javascript"></script>
  <script src="http://paintpad.mysys.com.au/components/com_cweb/js/jqdatetimepicker/jquery.datetimepicker.full.min.js" type="text/javascript"></script>
  <script src="http://paintpad.mysys.com.au/components/com_cweb/js/mocha-ui/plugins/tree/scripts/tree.js" type="text/javascript"></script>
  <script src="http://paintpad.mysys.com.au/components/com_cweb/js/table_sorter_mool1_2.js" type="text/javascript"></script>
  <script src="http://paintpad.mysys.com.au/components/com_cweb/js/tablesorter_paging_mool1_2.js" type="text/javascript"></script>
  <script src="http://paintpad.mysys.com.au/components/com_cweb/js/si.files.js" type="text/javascript"></script>
  <script src="http://paintpad.mysys.com.au/components/com_cweb/js/TabSet.js" type="text/javascript"></script>
  <script src="http://paintpad.mysys.com.au/components/com_cweb/js/MooTooltips3.js" type="text/javascript"></script>
  <script src="http://paintpad.mysys.com.au/components/com_cweb/js/context_menu.js" type="text/javascript"></script>
  <script src="http://paintpad.mysys.com.au/components/com_cweb/js/MooEditable/MooEditable.js" type="text/javascript"></script>
  <script src="http://paintpad.mysys.com.au/components/com_cweb/js/datepicker_new.js?no_cache=1605787243" type="text/javascript"></script>
  <script src="http://paintpad.mysys.com.au/components/com_cweb/js/Roar.js" type="text/javascript"></script>
  <script src="http://paintpad.mysys.com.au/components/com_cweb/js/signature_pad.js" type="text/javascript"></script>
  <script src="http://paintpad.mysys.com.au/components/com_cweb/js/formcheck/formcheck.js" type="text/javascript"></script>
  <script src="http://paintpad.mysys.com.au/components/com_cweb/js/formcheck/lang/en.js" type="text/javascript"></script>
  <script src="http://paintpad.mysys.com.au/components/com_cweb/js/mooRainbow/mooRainbow.js" type="text/javascript"></script>
  <script src="http://paintpad.mysys.com.au/components/com_cweb/js/jquery.resizableColumns.js" type="text/javascript"></script>
  <script src="http://paintpad.mysys.com.au/components/com_cweb/js/store.js" type="text/javascript"></script>
  <script src="http://paintpad.mysys.com.au/components/com_cweb/js/uploader_master/util.js" type="text/javascript"></script>
  <script src="http://paintpad.mysys.com.au/components/com_cweb/js/uploader_master/button.js" type="text/javascript"></script>
  <script src="http://paintpad.mysys.com.au/components/com_cweb/js/uploader_master/ajax.requester.js" type="text/javascript"></script>
  <script src="http://paintpad.mysys.com.au/components/com_cweb/js/uploader_master/deletefile.ajax.requester.js" type="text/javascript"></script>
  <script src="http://paintpad.mysys.com.au/components/com_cweb/js/uploader_master/handler.base.js" type="text/javascript"></script>
  <script src="http://paintpad.mysys.com.au/components/com_cweb/js/uploader_master/window.receive.message.js" type="text/javascript"></script>
  <script src="http://paintpad.mysys.com.au/components/com_cweb/js/uploader_master/handler.form.js" type="text/javascript"></script>
  <script src="http://paintpad.mysys.com.au/components/com_cweb/js/uploader_master/handler.xhr.js" type="text/javascript"></script>
  <script src="http://paintpad.mysys.com.au/components/com_cweb/js/uploader_master/uploader.basic.js" type="text/javascript"></script>
  <script src="http://paintpad.mysys.com.au/components/com_cweb/js/uploader_master/dnd.js" type="text/javascript"></script>
  <script src="http://paintpad.mysys.com.au/components/com_cweb/js/uploader_master/uploader.js" type="text/javascript"></script>
  <script src="http://paintpad.mysys.com.au/components/com_cweb/js/knockoutjs/knockout-min.js" type="text/javascript"></script>
  <script src="http://paintpad.mysys.com.au/components/com_cweb/js/knockoutjs/knockout.mapping.js" type="text/javascript"></script>
  <script src="http://paintpad.mysys.com.au/components/com_cweb/js/knockoutjs/knockout-alltraders.js" type="text/javascript"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDm2jMv5iC-HyzR4zwMXJ4Q8uE24WwMyuE&amp;libraries=places,geometry" type="text/javascript"></script>
  <script src="http://paintpad.mysys.com.au/components/com_cweb/js/googlemap/maplabel-compiled.js" type="text/javascript"></script>
  <script src="http://paintpad.mysys.com.au/components/com_cweb/js/mash_tree/tree.js" type="text/javascript"></script>
  <script src="http://paintpad.mysys.com.au/components/com_cweb/js/cweb.js?nocache=1605787243" type="text/javascript"></script>
  <script src="http://paintpad.mysys.com.au/components/com_cweb/cweb_painting/js/painting.js?nocache=1605787243" type="text/javascript"></script>
  <script src="http://paintpad.mysys.com.au/components/com_cweb/cweb_painting/js/signature_pad.js" type="text/javascript"></script>
  <script src="https://secure.ewaypayments.com/scripts/eCrypt.min.js" type="text/javascript"></script>
  <script src="http://paintpad.mysys.com.au/plugins/editors/jce/tiny_mce/tiny_mce.js?version=1575" type="text/javascript"></script>
  <script src="http://paintpad.mysys.com.au/plugins/editors/jce/libraries/js/editor.js?version=1575" type="text/javascript"></script>
  <script src="http://paintpad.mysys.com.au/components/com_cweb/js/tinymce_4.3.1/plugins/advcode/js/toggle.js" type="text/javascript"></script>
  <script src="http://paintpad.mysys.com.au/components/com_cweb/js/d3/d3.v3.min.js" type="text/javascript"></script>
  <script src="http://paintpad.mysys.com.au/components/com_cweb/js/analytics.js" type="text/javascript"></script>
  <script src="http://paintpad.mysys.com.au/components/com_cweb/js/jquery.autocomplete.min.js" type="text/javascript"></script>
  <script type="text/javascript">
var global_maximized_windows = 0;var global_smart_subwindow_position = 0;var global_maximize_below_nav = 0;var global_browser_warning = 0;var global_darktheme = 0;
var CWEB_SYSTEM = 'paint'
  </script>

	<link rel="stylesheet" href="/templates/mylogin/css/template.css" type="text/css">

<script type="text/javascript" charset="UTF-8" src="https://maps.googleapis.com/maps-api-v3/api/js/43/0/common.js"></script><script type="text/javascript" charset="UTF-8" src="https://maps.googleapis.com/maps-api-v3/api/js/43/0/util.js"></script></head>
<body class="contentpane desktop physicalKeyboard">
	
<div id="system-message-container">
</div>
	<div id="desktop_top_wrapper" menu_open_delay="270">
	<noscript>
		<style type="text/css">
			#desktop_top_wrapper li:not(#logout), #desktop {display:none;}
		</style>
		<div class="browser_warning" id="system-message-container" style="top:50px;">
			<div class="alert alert-warning">
				<span class="icon-button hide"><span class="icon icon-warning-sign"></span></span>
				<h2>Your browser appears to have Javascript disabled. </h2>
				<p class="reason alert alert-danger"><p>To use MySys, Javascript needs to be enabled.</p><p>Please <a href="http://www.enable-javascript.com/" target="_blank" title="You can follow the instructions here">adjust your browser settings to enable Javascript</a>, and then refresh this tab.</p><p>If you encounter further issues, please contact <a href="http://www.mysysonline.com.au/support.html" target="_blank">Alltraders</a>.</p>
				<img class="logo pull-left" alt="Logo" src="/templates/login/img/logo.png">
				<a class="btn btn-info pull-right btn_close" href="http://www.enable-javascript.com/" target="_blank">How to enable Javascript</a>
			</div>
		</div>
		
	</noscript>
	<div class="elos-nav" id="cweb_nav_bar">
		<div class="clearfix container">
			<div class="pull-right menu-right">
				<span class="nav_menu_toggle hide"><i class="icon icon-cog"></i></span>
				<ul id="nav_menu">
					<li class="text">
																				<span id="proxy_btn" title="Click to proxy as someone else.">Welcome <br>Martin Penney</span>
																		</li>
										<li title="" class="new" id="main_quick_create_btn_group">
						<a><i class="icon icon-star icon-white"></i><span> New</span></a>
						<ul>
																																				
																
																
							<li class="new_paint_project_btn" int_ext="int"><a class="form_name_area">Interior Project</a></li>
<li class="new_paint_project_btn" int_ext="ext"><a class="form_name_area">Exterior Project</a></li>

<script type="text/javascript">
	function paint_new_top_menu()
	{
		$$('.new_paint_project_btn').addEvent('click' , function()
		{
			var url = 'index.php?option=com_cweb&view=quote_tool&layout=new_project_panel&format=quote_tool&int_ext=' + $(this).get('int_ext');
			new MochaUI.Window(
			{			
				title: 'Create New Project',
				loadMethod: 'xhr',
				contentURL: url , 
				width: 1000,
				height: 700,
				addClass : 'quote_window',
				restrict: false				
			});
		});
	}
	
	paint_new_top_menu();
</script>															<li class="new_contact_btn" contact_type="person"><a>New Person</a></li>
								<li class="new_contact_btn" contact_type="company"><a>New Company</a></li>
																						
														<li class="new_task_btn"><a>New Task</a></li>
							<li class="new_rec_task_btn"><a>New Recurring Task</a></li>
														
							<li class="new_workflow_btn"><a>New Workflow</a></li>
														
														<li class="new_comm_btn"><a>New Communication</a></li>
							<li class="new_doc_btn"><a>New Document</a></li>
														
													</ul>
					</li>
										<li id="time_adv_log_menu" class="time_adv_log_menu_active" title="Click to show or hide" style="display:none;">
						<span id="time_adv_log_menu_current" class="small"></span>
						<a href="#"><i class="icon icon-time"></i></a>
					</li>
																				<li id="instant_search_area">
						<a class="search_wrapper">
							<i class="search_btn icon icon-search icon-white"></i>
							<span class="search_input_wrapper"><input id="search_input" type="text" class="input-large search-query" placeholder="Search..." autocomplete="off"></span>
							<input type="hidden" id="searchID" name="searchID" value="">
							<span class="icon-button search_button hide" id="search_input_open" title="Search in names and descriptions for Projects, Jobs, Tasks, Contacts, Timesheets, Purchase Orders, Customer Invoices, and Supplier Invoices."><i class="icon icon-search"></i></span>
							<div class="spinner-img hide"></div>
						</a>
					</li>
																<li id="user_profile_open_btn" title="User Configuration">
							<a href="#"><i class="icon icon-cog icon-white"></i></a>
						</li>
										<li id="logout"><a href="index.php?option=com_cweb&amp;task=logout&amp;format=raw" title="Logout"><i class="icon icon-off icon-white"></i></a></li>
				</ul>
			</div>
			<div class="menu-left">
				<ul id="js_multi_view_selector" class="">
										<li class=""><a menu_id="1" class="root_menu_btn js_menu_btn home" dashboard="home" url="index.php?option=com_cweb&amp;view=dash_board&amp;layout=dash_board&amp;section=home&amp;format=dash_board" href="#"><i><i class="icon icon-home icon-white"></i></i><span></span></a>
												<ul>
														<li><a menu_id="" class="js_menu_btn " href="#" url="widget_btn">
									<span class="pull-left">Widget</span>
																								</a>
															</li>
													</ul>
											</li>
										<li><a menu_id="2" class="root_menu_btn js_menu_btn project_view" dashboard="project_view" url="index.php?option=com_cweb&amp;view=dash_board&amp;layout=dash_board&amp;section=project_view&amp;format=dash_board" href="#"><i><i class="icon icon-folder-open icon-white"></i></i><span>Project</span></a>
												<ul>
														<li><a menu_id="3" class="js_menu_btn " href="#" url="index.php?option=com_cweb&amp;view=project&amp;layout=project_list_wrapper&amp;format=project&amp;my_project=1">
									<span class="pull-left">Projects</span>
																									<i class="icon icon-folder-open icon-white pull-right"></i>
																</a>
															</li>
														<li><a menu_id="162" class="js_menu_btn calendar" dashboard="calendar" href="#" url="index.php?option=com_cweb&amp;view=calender&amp;layout=calender_wrapper&amp;format=calender">
									<span class="pull-left">Calendar</span>
																								</a>
															</li>
													</ul>
											</li>
										<li><a menu_id="8" class="root_menu_btn js_menu_btn crm_view" dashboard="crm_view" url="index.php?option=com_cweb&amp;view=dash_board&amp;layout=dash_board&amp;section=crm_view&amp;format=dash_board" href="#"><i><i class="icon icon-user icon-white"></i></i><span>CRM</span></a>
												<ul>
														<li><a menu_id="9" class="js_menu_btn " href="#" url="index.php?option=com_cweb&amp;view=communication_logs&amp;layout=communication_list_wrapper&amp;format=communications">
									<span class="pull-left">Communication Log</span>
																									<i class="icon icon-comment icon-white pull-right"></i>
																</a>
															</li>
														<li><a menu_id="11" class="js_menu_btn " href="#" url="index.php?option=com_cweb&amp;view=contacts&amp;layout=contact_list_wrapper&amp;format=contacts">
									<span class="pull-left">Contacts</span>
																									<i class="icon icon-user icon-white pull-right"></i>
																</a>
															</li>
														<li><a menu_id="" class="js_menu_btn " href="#" url="index.php?option=com_cweb&amp;view=xero&amp;layout=xero_test&amp;format=xero">
									<span class="pull-left">Xero Test</span>
																								</a>
															</li>
													</ul>
											</li>
										<li><a menu_id="18" class="root_menu_btn js_menu_btn stock_view" dashboard="stock_view" url="index.php?option=com_cweb&amp;view=dash_board&amp;layout=dash_board&amp;section=stock_view&amp;format=dash_board" href="#"><i><i class="icon icon-gift icon-white"></i></i><span>Product</span></a>
												<ul>
														<li><a menu_id="19" class="js_menu_btn " href="#" url="index.php?option=com_cweb&amp;view=painting_brand&amp;layout=painting_brand_list_wrapper&amp;format=painting_brand">
									<span class="pull-left">Brand</span>
																									<i class="icon icon-gift icon-white pull-right"></i>
																</a>
															</li>
														<li><a menu_id="20" class="js_menu_btn " href="#" url="index.php?option=com_cweb&amp;view=painting_product&amp;layout=painting_product_list_wrapper&amp;format=painting_product">
									<span class="pull-left">Product</span>
																								</a>
															</li>
														<li><a menu_id="21" class="js_menu_btn " href="#" url="index.php?option=com_cweb&amp;view=painting_product&amp;layout=painting_sheen_list_wrapper&amp;format=painting_product">
									<span class="pull-left">Sheen</span>
																									<i class="icon icon-align-left rotate icon-white pull-right"></i>
																</a>
															</li>
														<li><a menu_id="22" class="js_menu_btn " href="#" url="index.php?option=com_cweb&amp;view=painting_product&amp;layout=painting_comp_list_wrapper&amp;format=painting_product">
									<span class="pull-left">Component</span>
																								</a>
															</li>
														<li><a menu_id="23" class="js_menu_btn " href="#" url="index.php?option=com_cweb&amp;view=painting_product&amp;layout=painting_prep_level_list_wrapper&amp;format=painting_product">
									<span class="pull-left">Prep. Level</span>
																								</a>
															</li>
														<li><a menu_id="24" class="js_menu_btn " href="#" url="index.php?option=com_cweb&amp;view=painting_product&amp;layout=painting_comp_group_list_wrapper&amp;format=painting_product">
									<span class="pull-left">Component Group</span>
																								</a>
															</li>
														<li><a menu_id="102" class="js_menu_btn " href="#" url="index.php?option=com_cweb&amp;view=painting_product&amp;layout=painting_room_type_list_wrapper&amp;format=painting_product">
									<span class="pull-left">Room Type</span>
																								</a>
															</li>
														<li><a menu_id="104" class="js_menu_btn " href="#" url="index.php?option=com_cweb&amp;view=production_order&amp;layout=production_order_list_wrapper&amp;format=production_order">
									<span class="pull-left">Production Order</span>
																								</a>
															</li>
														<li><a menu_id="202" class="js_menu_btn " href="#" url="index.php?option=com_cweb&amp;view=painting_product&amp;layout=painting_color_tag_list_wrapper&amp;format=painting_product">
									<span class="pull-left">Colour Tag</span>
																								</a>
															</li>
														<li><a menu_id="203" class="js_menu_btn " href="#" url="index.php?option=com_cweb&amp;view=painting_product&amp;layout=painting_tire_list_wrapper&amp;format=painting_product">
									<span class="pull-left">Tier</span>
																								</a>
															</li>
														<li><a menu_id="204" class="js_menu_btn " href="#" url="index.php?option=com_cweb&amp;view=painting_product&amp;layout=painting_color_list_wrapper&amp;format=painting_product">
									<span class="pull-left">Colour</span>
																								</a>
															</li>
														<li><a menu_id="207" class="js_menu_btn " href="#" url="index.php?option=com_cweb&amp;view=painting_product&amp;layout=painting_special_item_list_wrapper&amp;format=painting_product">
									<span class="pull-left">Special Item</span>
																								</a>
															</li>
														<li><a menu_id="208" class="js_menu_btn " href="#" url="index.php?option=com_cweb&amp;view=painting_product&amp;layout=painting_tint_strength_list_wrapper&amp;format=painting_product">
									<span class="pull-left">Tint Strength</span>
																								</a>
															</li>
													</ul>
											</li>
										<li class="active"><a menu_id="7" class="root_menu_btn js_menu_btn reports_view" dashboard="reports_view" url="index.php?option=com_cweb&amp;view=dash_board&amp;layout=dash_board&amp;section=reports_view&amp;format=dash_board" href="#"><i><i class="icon icon-list-alt icon-white"></i></i><span>Reports</span></a>
												<ul>
														<li class=""><a menu_id="128" class="js_menu_btn " href="#" url="index.php?option=com_cweb&amp;view=predefined_reports&amp;layout=predefined_report_table_wrapper&amp;format=default">
									<span class="pull-left">Predefined Reports</span>
																								</a>
															</li>
														<li class="active"><a menu_id="125" class="js_menu_btn " href="#" url="index.php?option=com_cweb&amp;view=report&amp;layout=entity_report_config&amp;format=cweb_report&amp;report_type=project">
									<span class="pull-left">Projects</span>
																									<i class="icon icon-folder-open icon-white pull-right"></i>
																</a>
															</li>
													</ul>
											</li>
										<li><a menu_id="31" class="root_menu_btn js_menu_btn admin_view" dashboard="admin_view" url="index.php?option=com_cweb&amp;view=dash_board&amp;layout=dash_board&amp;section=admin_view&amp;format=dash_board" href="#"><i><i class="icon icon-lock icon-white"></i></i><span>Admin</span></a>
												<ul>
														<li><a menu_id="32" class="js_menu_btn admin_generial_view" dashboard="admin_generial_view" href="#" url="index.php?option=com_cweb&amp;view=dash_board&amp;layout=dash_board&amp;section=admin_generial_view&amp;format=dash_board">
									<span class="pull-left">General Settings</span>
																	<i class="icon-chevron-right icon-white icon-faint pull-right"></i>
																								</a>
																
								<ul>
																<li><a menu_id="43" class="js_menu_btn" href="#" url="index.php?option=com_cweb&amp;view=admin_user&amp;layout=admin_user_login_wrapper&amp;format=admin_user">
									<span class="pull-left">User Activity</span>
																	<i class="icon icon-align-left rotate icon-white pull-right"></i>
																</a></li>
																<li><a menu_id="206" class="js_menu_btn" href="#" url="index.php?option=com_cweb&amp;view=calender&amp;format=calender&amp;layout=event_type_list_wrapper">
									<span class="pull-left">Calendar Event Type</span>
																</a></li>
																</ul>
															</li>
														<li><a menu_id="45" class="js_menu_btn admin_crm_view" dashboard="admin_crm_view" href="#" url="index.php?option=com_cweb&amp;view=dash_board&amp;layout=dash_board&amp;section=admin_crm_view&amp;format=dash_board">
									<span class="pull-left">CRM</span>
																	<i class="icon-chevron-right icon-white icon-faint pull-right"></i>
																									<i class="icon icon-user icon-white pull-right"></i>
																</a>
																
								<ul>
																<li><a menu_id="157" class="js_menu_btn" href="#" url="index.php?option=com_cweb&amp;view=agent&amp;format=agent&amp;layout=agent_list_wrapper">
									<span class="pull-left">Subscriber</span>
																</a></li>
																</ul>
															</li>
														<li><a menu_id="57" class="js_menu_btn admin_customer_invoice_view" dashboard="admin_customer_invoice_view" href="#" url="index.php?option=com_cweb&amp;view=dash_board&amp;layout=dash_board&amp;section=admin_customer_invoice_view&amp;format=dash_board">
									<span class="pull-left">Accounting</span>
																	<i class="icon-chevron-right icon-white icon-faint pull-right"></i>
																									<i class="icon icon-book icon-white pull-right"></i>
																</a>
																
								<ul>
																<li><a menu_id="155" class="js_menu_btn" href="#" url="index.php?option=com_cweb&amp;view=payment_term&amp;layout=payment_term_list_wrapper&amp;format=payment_term">
									<span class="pull-left">Payment Term</span>
																</a></li>
																<li><a menu_id="205" class="js_menu_btn" href="#" url="index.php?option=com_cweb&amp;view=payment_term&amp;layout=payment_method_list_wrapper&amp;format=payment_term">
									<span class="pull-left">Payment Method</span>
																</a></li>
																</ul>
															</li>
														<li><a menu_id="60" class="js_menu_btn admin_doc_view" dashboard="admin_doc_view" href="#" url="index.php?option=com_cweb&amp;view=dash_board&amp;layout=dash_board&amp;section=admin_doc_view&amp;format=dash_board">
									<span class="pull-left">Documents</span>
																	<i class="icon-chevron-right icon-white icon-faint pull-right"></i>
																								</a>
																
								<ul>
																<li><a menu_id="86" class="js_menu_btn" href="#" url="index.php?option=com_cweb&amp;view=admin_document&amp;layout=admin_document_document_wrapper&amp;format=admin_document">
									<span class="pull-left">Documents</span>
																</a></li>
																<li><a menu_id="87" class="js_menu_btn" href="#" url="index.php?option=com_cweb&amp;view=admin_document&amp;layout=admin_document_type_wrapper&amp;format=admin_document">
									<span class="pull-left">Document Types</span>
																</a></li>
																<li><a menu_id="88" class="js_menu_btn" href="#" url="index.php?option=com_cweb&amp;view=admin_document&amp;layout=admin_document_library_wrapper&amp;format=admin_document">
									<span class="pull-left">Library Documents</span>
																</a></li>
																<li><a menu_id="90" class="js_menu_btn" href="#" url="index.php?option=com_cweb&amp;view=email&amp;layout=email_template_list_wrapper&amp;format=email_template">
									<span class="pull-left">Email Templates</span>
																</a></li>
																<li><a menu_id="177" class="js_menu_btn" href="#" url="index.php?option=com_cweb&amp;view=email&amp;layout=email_type_list_wrapper&amp;format=email_template">
									<span class="pull-left">Email Types</span>
																</a></li>
																</ul>
															</li>
														<li><a menu_id="166" class="js_menu_btn plugin_view" dashboard="plugin_view" href="#" url="#">
									<span class="pull-left">Plugins</span>
																	<i class="icon-chevron-right icon-white icon-faint pull-right"></i>
																								</a>
																
								<ul>
																<li><a menu_id="painting_config" class="js_menu_btn" href="#" url="index.php?option=com_cweb&amp;view=painting_config&amp;format=painting_config&amp;layout=global_config">
									<span class="pull-left">Paint Global Config</span>
																</a></li>
																<li><a menu_id="agent_config" class="js_menu_btn" href="#" url="index.php?option=com_cweb&amp;view=agent&amp;layout=agent_config&amp;format=agent">
									<span class="pull-left">Agent Config</span>
																</a></li>
																<li><a menu_id="xero_sys_config" class="js_menu_btn" href="#" url="index.php?option=com_cweb&amp;view=cweb_xero&amp;layout=xero_config&amp;format=xero">
									<span class="pull-left">Xero Config</span>
																</a></li>
																</ul>
															</li>
													</ul>
											</li>
										
										
									</ul>
			</div>
		</div>	
	</div>	

</div>
<div id="autocomplete_results"><div class="autocomplete-suggestions" style="position: absolute; display: none; width: 350px; max-height: 300px; z-index: 9999;"></div></div>
<div id="desktop" class="smallfonts reports_view" style="height: 402px;">
	<div class="wrapper">		
		<div class="row-fluid" id="main_page_sub_menu_section">
			<div class="span12">				
				<div class="clearfix">										
					<div class="span2" id="dash_board_section_side">
						<span>
							<a href="#" class="btn btn-large btn-icon" id="show_sidebar" title="Show/hide sidebar info"><i class="icon-list-alt "></i></a>
							<a href="#" class="btn btn-primary btn-small " id="return_to_dashboard_btn">Dash<wbr>board</a>
						</span>
																																			</div>
					<div class="btn-group span10" style="display: block;" id="dash_board_section_buttons">
						<div class="pull-right nav_logo hide" style="height:37px;">
														<img src="http://paintpad.mysys.com.au/templates/login/img/logo.png" alt="Logo" class="logo">
													</div>
																																	
															
															
																	<a href="#" class="btn btn-large dashbord_btns home" menu_id="" dashboard="home" style="display: none;">Widget</a>
																						
															
																																								
															
															
																	<a href="#" class="btn btn-large dashbord_btns project_view" menu_id="3" dashboard="project_view" style="display: none;">Projects</a>
																						
															
																	<a href="#" class="btn btn-large dashbord_btns project_view" menu_id="162" dashboard="project_view" style="display: none;">Calendar</a>
																						
															
																																								
															
															
																	<a href="#" class="btn btn-large dashbord_btns crm_view" menu_id="9" dashboard="crm_view" style="display: none;">Communication Log</a>
																						
															
																	<a href="#" class="btn btn-large dashbord_btns crm_view" menu_id="11" dashboard="crm_view" style="display: none;">Contacts</a>
																						
															
																	<a href="#" class="btn btn-large dashbord_btns crm_view" menu_id="" dashboard="crm_view" style="display: none;">Xero Test</a>
																						
															
																																								
															
															
																	<a href="#" class="btn btn-large dashbord_btns stock_view" menu_id="19" dashboard="stock_view" style="display: none;">Brand</a>
																						
															
																	<a href="#" class="btn btn-large dashbord_btns stock_view" menu_id="20" dashboard="stock_view" style="display: none;">Product</a>
																						
															
																	<a href="#" class="btn btn-large dashbord_btns stock_view" menu_id="21" dashboard="stock_view" style="display: none;">Sheen</a>
																						
															
																	<a href="#" class="btn btn-large dashbord_btns stock_view" menu_id="22" dashboard="stock_view" style="display: none;">Component</a>
																						
															
																	<a href="#" class="btn btn-large dashbord_btns stock_view" menu_id="23" dashboard="stock_view" style="display: none;">Prep. Level</a>
																						
															
																	<a href="#" class="btn btn-large dashbord_btns stock_view" menu_id="24" dashboard="stock_view" style="display: none;">Component Group</a>
																						
															
																	<a href="#" class="btn btn-large dashbord_btns stock_view" menu_id="102" dashboard="stock_view" style="display: none;">Room Type</a>
																						
															
																	<a href="#" class="btn btn-large dashbord_btns stock_view" menu_id="104" dashboard="stock_view" style="display: none;">Production Order</a>
																						
															
																	<a href="#" class="btn btn-large dashbord_btns stock_view" menu_id="202" dashboard="stock_view" style="display: none;">Colour Tag</a>
																						
															
																	<a href="#" class="btn btn-large dashbord_btns stock_view" menu_id="203" dashboard="stock_view" style="display: none;">Tier</a>
																						
															
																	<a href="#" class="btn btn-large dashbord_btns stock_view" menu_id="204" dashboard="stock_view" style="display: none;">Colour</a>
																						
															
																	<a href="#" class="btn btn-large dashbord_btns stock_view" menu_id="207" dashboard="stock_view" style="display: none;">Special Item</a>
																						
															
																	<a href="#" class="btn btn-large dashbord_btns stock_view" menu_id="208" dashboard="stock_view" style="display: none;">Tint Strength</a>
																						
															
																																								
															
															
																	<a href="index" class="btn btn-large dashbord_btns reports_view " menu_id="128" dashboard="reports_view" style="display: inline;">Predefined Reports</a>
																						
															
																	<a href="#" class="btn btn-large dashbord_btns reports_view active" menu_id="125" dashboard="reports_view" style="display: inline;">Projects</a>
																						
															
																																								
															
															
																										<a href="#" dashboard="admin_generial_view" menu_id="43" class="btn btn-large dashbord_btns admin_generial_view" style="display: none;">User Activity</a>
																		<a href="#" dashboard="admin_generial_view" menu_id="206" class="btn btn-large dashbord_btns admin_generial_view" style="display: none;">Calendar Event Type</a>
																															
															
																										<a href="#" dashboard="admin_crm_view" menu_id="157" class="btn btn-large dashbord_btns admin_crm_view" style="display: none;">Subscriber</a>
																															
															
																										<a href="#" dashboard="admin_customer_invoice_view" menu_id="155" class="btn btn-large dashbord_btns admin_customer_invoice_view" style="display: none;">Payment Term</a>
																		<a href="#" dashboard="admin_customer_invoice_view" menu_id="205" class="btn btn-large dashbord_btns admin_customer_invoice_view" style="display: none;">Payment Method</a>
																															
															
																										<a href="#" dashboard="admin_doc_view" menu_id="86" class="btn btn-large dashbord_btns admin_doc_view" style="display: none;">Documents</a>
																		<a href="#" dashboard="admin_doc_view" menu_id="87" class="btn btn-large dashbord_btns admin_doc_view" style="display: none;">Document Types</a>
																		<a href="#" dashboard="admin_doc_view" menu_id="88" class="btn btn-large dashbord_btns admin_doc_view" style="display: none;">Library Documents</a>
																		<a href="#" dashboard="admin_doc_view" menu_id="90" class="btn btn-large dashbord_btns admin_doc_view" style="display: none;">Email Templates</a>
																		<a href="#" dashboard="admin_doc_view" menu_id="177" class="btn btn-large dashbord_btns admin_doc_view" style="display: none;">Email Types</a>
																															
															
																										<a href="#" dashboard="plugin_view" menu_id="painting_config" class="btn btn-large dashbord_btns plugin_view" style="display: none;">Paint Global Config</a>
																		<a href="#" dashboard="plugin_view" menu_id="agent_config" class="btn btn-large dashbord_btns plugin_view" style="display: none;">Agent Config</a>
																		<a href="#" dashboard="plugin_view" menu_id="xero_sys_config" class="btn btn-large dashbord_btns plugin_view" style="display: none;">Xero Config</a>
																															
															
													</div>
					<div class="btn-group pull-right span6 hide" id="dash_board_widget_tool_btn">
												<a href="#" class="btn btn-small add_remove_widget_btn pull-right">Widgets</a>
											</div>
				</div>
				<hr class="hide">
			</div>
		</div>
		<div class="row-fluid">
			<div class="span10 desktop_dash_board_right pull-right" id="cweb_working_area"><div id="cweb_report_config_wrapper_1605790365" class="flex-column">
<div class="windowInfo  clearfix">


 <?php $form = ActiveForm::begin(); ?>


	<div class="btn-group pull-right" style="margin-left: 2em;">
	<?= Html::submitButton('Save', ['class' => 'btn btn-primary', 'name' => 'save_btn']) ?>
				<!--<input type="button" class="btn btn-success" value="Save/Schedule" name="save_btn"> -->
	</div>
	
	<div class="btn-group pull-right" style="margin-left: 2em;">
		<input type="button" class="btn btn-primary" value="Preview HTML" name="preview_btn">
		<button class="btn btn-primary" value="PDF" name="view_pdf_btn"><i class="icon icon_pdf pull-left" style="margin-right:0.5ex;"></i> PDF</button>
	</div>

	<div class="btn-group pull-right" style="margin-left: 2em;">
		<button class="btn btn-primary" name="view_csv_btn"><i class="fa fa-download"></i> Download CSV</button>
	</div>
	
		
	<div class="input-prepend pull-left">
		<span class="add-on pull-left">Report Name: </span>
		<input type="text" value="" name="report_name" required>
	</div>
</div>
<div class="scroll-container header footer pad form-inline form-horizontal form-elos form-left">
	<div class="row-fluid">
		<div class="span5">

			<div class="side-nav entry_wrapper">
				<div class="side-nav-header clearfix">
					<div class="pull-left">
						<h3>Date Settings</h3>
					</div>
				</div>
				<div class="side-nav-content">
					<div class="control-group">
						<label class="control-label">Date Field</label>
						<div class="controls">
							<select name="date_field">
								<optgroup label="System Date Field">
								<option value="create_date">Create Date</option>
								</optgroup>
						</select>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Date Range</label>
						<div class="controls">
						<!-- < ?php if (Yii::$app->user->identity->role == 20) { ?>
            
            <select name="company_id" class="form-control">
              <option value="0">Select Date</option>
              < ?php 
              foreach ($data as $key => $date){
                echo "<option value='".$date['id']."'>".$date['date']."</option>";
              }
              ?> -->
            </select>
<!--          
          < ?php } else{ ?>
            <select name="id" class="form-control">
              <option value="0">Select Date</option>
              <option value="< ?php echo Yii::$app->user->identity->id ; ?>">Self</option>
            </select>
             <!-- <input type="hidden" name="company_id" value="< ?php //echo Yii::$app->user->identity->company_id ; ?>"> -->
          <!-- < ?php } ?> -->
			 
	

         	
						</div>
					</div>
					<div class="control-group date_config_area" style="display: block;">
						<div class="control">
							<div class="input-prepend" style="display:inline-block;">
								<span class="add-on" style="width:30px">From</span>
								<span class="from_date_input_wrapper" style="display: inline;">
								
								<input type="date" value="" name="date_from" class="input-small">
								</span><select name="start_period" class="date_setting_select input-large">
																		<option value="fix">Fixed Date</option>
																		
<!-- 
																		< ?php 
              foreach ($dad as $key => $da){
                echo "<option value='".$da['id']."'>".$da['from_date']."</option>";
              }
              ?> -->

																	</select>
							</div>
						</div>
						<div class="control">
							<div class="input-prepend" style="display:inline-block;">
								<span class="add-on" style="width:30px">To</span><span class="to_date_input_wrapper" style="display: inline;">
								<input type="date" value="" name="date_to" class="input-small">
								</span>
								<select name="end_period" class="date_setting_select input-large">
																		<option value="fix">Fixed Date</option>
																		
<!-- 
																		< ?php 
              foreach ($dad as $key => $da){
                echo "<option value='".$da['id']."'>".$da['from_date']."</option>";
              }
              ?> -->

														  				
																	</select>
							</div>
						</div>
					</div>
				</div>
			</div>
		
			<div class="side-nav entry_wrapper">
				<div class="side-nav-header clearfix">
					<div class="pull-right">
						<input type="button" class="btn btn-primary" value="Add Group" name="add_group_btn">
					</div>
					<div class="pull-left">
						<h3>Grouping</h3>
					</div>
				</div>
				<div class="side-nav-content">
					<table class="table">
						<tbody class="group_by_wrapper">
							<tr class="group_by_tmpl" style="display:none;">
								<td class="group_by_heading_area" style="font-weight:bold;width:100px;">Group By</td>
								<td class="group_by_select_wrapper">
									<select name="group_by">
									
																		

			
																				
									</select>
								</td>
								<td class="remove_icon_wrapper"></td>
							</tr>
						<tr class="group_by_tmpl group_by_select" style="display: table-row;">
								<td class="group_by_heading_area" style="font-weight:bold;width:100px;">Group By</td>
								<td class="group_by_select_wrapper">
									
									
									
									<select name="group_by">
									<option value="0">Select Group Filter</option>
									<!-- < ?php 
								foreach ($data as $key => $gps){
									echo "<option value='".$gps['id']."'>".$gps['groups']."</option>";
								}

							
            					  ?> -->
									</select>


								



								</td>
								<td class="remove_icon_wrapper"></td>
							</tr></tbody>
					</table>
				</div>
				<select name="group_field_select" class="group_by_second_select" style="display:none;">
									</select>

				<select name="group_user_role_select" class="group_by_second_select" style="display:none;">
										<option value="41">Accounts</option>
										<option value="47">Accounts Receivable</option>
										<option value="42">Administration</option>
										<option value="64">Despatch &amp; Logistics </option>
										<option value="56">Invoicing</option>
										<option value="59">Marketing </option>
										<option value="57">Marty</option>
										<option value="60">Production </option>
										<option value="65">Purchasing</option>
										<option value="13">Senior Manager</option>
										<option value="44">Server Configuration</option>
										<option value="58">Subscriber</option>
										<option value="63">Telemarketing</option>
										<option value="62">Tenderer </option>
										<option value="61">Tendering Manager </option>
									</select>

				<select name="group_date_select" class="group_by_second_select" style="display:none;">
					<option value="daily">Daily</option>
					<option value="weekly">Weekly</option>
					<option value="monthly">Monthly</option>
					<option value="quarterly">Quarterly</option>
					<option value="yearly">Yearly</option>
				</select>
			</div>
		</div>

		<div class="span7">
			<div class="side-nav entry_wrapper">
				<div class="side-nav-header clearfix">
					<div class="pull-right">
						<input type="button" class="btn btn-primary" name="add_sys_field_total" value="Add System Field Total">
						<input type="button" class="btn btn-primary" name="add_custom_field_total" value="Add Custom Field Total">
					</div>
					<div><h3>Group Totals</h3></div>
					<div class="pull-left">
						<i>Click and drag rows to re-order the fields</i>
					</div>
				</div>
				<div class="side-nav-content">
					<table class="table">
						<thead>
							<tr>
								<th>Order <i class="icon icon-chevron-up group_order_dir_btn" order="asc"></i></th>
								<th>Field</th>
								<th>Report Col Title</th>
								<th>Total Type</th>
								<th>Align</th>
								<th>DPS</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td><input type="radio" name="group_order_btn" order="asc" value="group_name"></td>
								<td colspan="6">Group Name</td>
							</tr>
						</tbody>
						<tbody class="total_column_wrapper"></tbody>
					</table>
				</div>
			</div>
			<div class="side-nav entry_wrapper">
				<div class="side-nav-header clearfix">
					<div class="pull-right">
						<select name="select_filter">
							<option value="">--- Select Filter ---</option>
														<option value="client">Add Client Filter</option>
														<option value="lead_source">Add Lead Source Filter</option>
														<option value="sales_type">Add Sales Type Filter</option>
														<option value="sales_office">Add Sales Office Filter</option>
														<option value="status">Add Status Filter</option>
														<option value="owner">Add Owner Filter</option>
														<option value="agent">Add Agent Filter</option>
													</select>
					</div>
					<div class="pull-left">
						<h3>Filtering</h3>
					</div>
				</div>
                <div id="textboxes" style="display: none">	
				<div class="side-nav-content">
					<div class="">
																																	<div class="lead_source_block general_select_block filter_block alert clearfix" filter_field="lead_source_id" style="display:none;padding: 8px;background: transparent;border-color: #d5d5d5;">
							<div class="control-group">
								<label class="control-label">Lead Source (s)</label>
								<div class="controls">
									<div class="pull-right"><i class="icon-remove remove_btn"></i></div>
									<select name="filter_entity_select" class="span11">
										<option value="">--- Select ---</option>
																				
																			</select>
								</div>
							</div>

							<div class="item_area" style="padding-left:30px;"></div>
						</div>
																														<div class="sales_type_block general_select_block filter_block alert clearfix" filter_field="sales_type_id" st
                                                                                                                             <div class="control-group">
								<label class="control-label">Sales Type (s)</label>
								<div class="controls">
									<div class="pull-right"><i class="icon-remove remove_btn"></i></div>
									<select name="filter_entity_select" class="span11">
										<option value="">--- Select ---</option>
																				<option value="2">Home Builders </option>
																				<option value="3">Home Owners </option>
																				<option value="1">Multi-Dwelling Builders</option>
																			</select>
								</div>
							</div>

							<div class="item_area" style="padding-left:30px;"></div>
						</div>
																														<div class="sales_office_block general_select_block filter_block alert clearfix" filter_field="sales_office_id" style="display:none;padding: 8px;background: transparent;border-color: #d5d5d5;">
							<div class="control-group">
								<label class="control-label">Sales Office (s)</label>
								<div class="controls">
									<div class="pull-right"><i class="icon-remove remove_btn"></i></div>
									<select name="filter_entity_select" class="span11">
										<option value="">--- Select ---</option>
																				<option value="1">Paint Professionals</option>
																			</select>
								</div>
							</div>

							<div class="item_area" style="padding-left:30px;"></div>
						</div>
																														<div class="status_block general_select_block filter_block alert clearfix" filter_field="status_id" style="display:none;padding: 8px;background: transparent;border-color: #d5d5d5;">
							<div class="control-group">
								<label class="control-label">Status (s)</label>
								<div class="controls">
									<div class="pull-right"><i class="icon-remove remove_btn"></i></div>
									<select name="filter_entity_select" class="span11">
										<option value="">--- Select ---</option>
																				<option value="2">Accepted</option>
																				<option value="4">Declined</option>
																				<option value="1">New</option>
																				<option value="25">Offered </option>
																				<option value="19">Open</option>
																			</select>
								</div>
							</div>

							<div class="item_area" style="padding-left:30px;"></div>
						</div>
																														<div class="owner_block general_select_block filter_block alert clearfix" filter_field="owner_id" style="display:none;padding: 8px;background: transparent;border-color: #d5d5d5;">
							<div class="control-group">
								<label class="control-label">Owner (s)</label>
								<div class="controls">
									<div class="pull-right"><i class="icon-remove remove_btn"></i></div>
									<select name="filter_entity_select" class="span11">
										<option value="">--- Select ---</option>
																				
																			</select>
								</div>
							</div>

							<div class="item_area" style="padding-left:30px;"></div>
						</div>
																														<div class="agent_block general_select_block filter_block alert clearfix" filter_field="agent_id" style="display:none;padding: 8px;background: transparent;border-color: #d5d5d5;">
							<div class="control-group">
								<label class="control-label">Agent (s)</label>
								<div class="controls">
									<div class="pull-right"><i class="icon-remove remove_btn"></i></div>
									<select name="filter_entity_select" class="span11">
										<option value="">--- Select ---</option>
																				
																			</select>
								</div>
							</div>

							<div class="item_area" style="padding-left:30px;"></div>
						</div>
												
						<div class="client_block filter_block alert clearfix" filter_field="client" style="display:none;padding: 8px;background: transparent;border-color: #d5d5d5;">
							<div class="control-group">
								<label class="control-label">Filter Client</label>
								<div class="controls">
				  					<span class="icon-button search_client_btn"><i class="icon-search"></i></span>
									<i class="icon-remove remove_btn pull-right"></i>
								</div>
							</div>

							<div class="item_area" style="padding-left:30px;"></div>
						</div>
					</div>
				</div>
			</div>
			
			<script>              //onclick
        $(function() {
    $('input[name="select_filter"]').on('click', function() {
        if ($(this).val() == 'client') {
            $('#textboxes').show();
            //alert('done');
        }
        else {
            $('#textboxes').hide();
        }
        // if ($(this).val() == 'One Day') {
        //     $('#textbox').show();
        // }
        // else {
        //     $('#textbox').hide();
        // }
        //  if ($(this).val() == 'More than one day') {
        //     $('#textboxx').show();
        // }
        // else {
        //     $('#textboxx').hide();
        // }
    });
});
    </script>






			<div class="side-nav entry_wrapper">
				<div class="side-nav-header clearfix">
					<div class="pull-left">
						<h3>Printing and Export Settings</h3>
					</div>
				</div>
				<div class="side-nav-content forty">
					<div class="control-group task_name">
						<label class="control-label">Page Size</label>
						<div class="controls">
							<select name="page_size" required>
								<option value="A4">A4</option>
								<option value="A3">A3</option>
							</select>
								<!-- < ?php 
								foreach ($siz as $key => $size){
									echo "<option value='".$size['id']."'>".$size['page_size']."</option>";
								}
								?> -->
							</select>
						</div>
					 </div>
					<div class="control-group task_name">
						<label class="control-label">Orientation</label>
						<div class="controls">
							<select name="page_orien" required> 
								<option value="L">Landscape</option>
								<option value="P">Portrait</option>
									<!-- < ?php 
									foreach ($orii as $key => $ori){
										echo "<option value='".$ori['id']."'>".$ori['orientation']."</option>";
									}
									?> -->
							</select>
						</div>
					 </div>
					<div class="control-group task_name">
						<label class="control-label">Page Break by Group</label>
						<div class="controls">
							<label class="radio inline"><input type="radio" name="page_break" value="YES"> Yes</label>
							<label class="radio inline"><input type="radio" name="page_break" value="NO"> No</label>
						
						</div>
					</div>
					<div class="control-group task_name">
						<label class="control-label">Hide Group Summaries on PDF/CSV</label>
						<div class="controls">
							<label class="radio inline"><input type="radio" name="hide_group" value="YES"> Yes</label>
							<label class="radio inline"><input type="radio" name="hide_group" value="NO"> No</label>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="heading-container clearfix" style="max-height: calc(100% - 60px); overflow: auto;">
	<div class="clearfix">
		<div class="pull-left"><b>Report Fields</b></div>
		<div class="pull-right">
			<input type="button" class="btn btn-inverse" value="Select System Fields" name="sys_field_select">
			<input type="button" class="btn btn-inverse" value="Select Custom Fields" name="custom_field_select">
					</div>
	</div>
	<div>
		<div class="no_field_select alert alert-info" style="display: block;">No Field Selected</div>
		<div class="sys_field_wrapper">
			<div class="sys_field_tmpl" style="display: none;">
				<div class="btn-group" style="min-width: 200px;margin-bottom: 5px;">
				  <button class="btn btn-mini btn-danger remove_btn" title="Remove Field"><i class="icon-white icon-remove"></i></button>
				  <button class="btn btn-mini fieldname" style="width:110px"></button>
				  <button class="btn btn-mini move_left_btn" title="Move field to the left"><i class="icon-chevron-left"></i></button>
				  <button class="btn btn-mini dropdown-toggle sort_btn" data-toggle="dropdown" title="Order by this field"><i class="icon-chevron-down icon_btn"></i></button>
				</div>
			</div>
		</div>
	</div>

	<div style="text-align:center;"><i>Resize report table column widths by clicking and dragging the column separators below.</i></div>
	<br><div class="resize_table_wrapper"><div class="rc-handle-container" style="width: 1083px;"></div><table class="table table-bordered" id="resize_table_1605790366913" data-resizable-columns-id="resize_table_1605790366913" style="margin-bottom: 0px;"><thead><tr></tr></thead></table></div>
</div>
</div>
</div>
			<div class="span2 desktop_dash_board_left pull-left ps-container ps-theme-default" id="cweb_sidebar" data-ps-id="d4ad0803-cf9d-4b4e-6d99-966db4f5ea8d" style="position: relative; overflow: hidden;">
				<div class="site_info">
					<img src="http://paintpad.mysys.com.au/templates/login/img/logo.png" alt="Logo" class="logo">
					<h4 class="date">November 19, 2020</h4>
				</div>
				<div class="user_profile_detail_area">
<div id="cweb_user_panel_detail">
	<div class="" style="margin-bottom:20px;">
		<ul class="no-list list-left">
			<li><strong>User:</strong> <span>Martin Penney</span></li>			
			<li><strong>Title:</strong> <span>Licensed Building Supervisor</span></li>
						<li><strong>Phone:</strong> <span>1800 686 525</span></li>
									<li><strong>Mobile:</strong> <span>0412 578 277</span></li>
								<li><strong>Email:</strong> <span>martin@paintprofessionals.com.au<br>---</span></li>	
		</ul>
		<br>
		<ul class="no-list list-left">
			<li><strong>Role:</strong> <span>Marty</span></li>
					</ul>

		<hr style="margin: 14px 0;">

				<div class="clearfix">
			<a href="#" style="display:inline-block; float:left; margin: 0 1em 5px 0;" class="btn btn-inverse btn-mini view_response_btn">Responsibilities</a>
			<a href="#" style="display:inline-block; float:left;" class="btn btn-inverse btn-mini view_permission_btn">Permissions</a>	
		</div>
			</div>
</div>
</div>
				<div class="score_board_area"></div>
			<div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px;"><div class="ps-scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps-scrollbar-y-rail" style="top: 0px; right: 0px;"><div class="ps-scrollbar-y" tabindex="0" style="top: 0px; height: 0px;"></div></div></div>
		</div>
	</div>
<div id="windowUnderlay" style="height: 440px; opacity: 0.01; display: none; z-index: 109;"></div></div>

<div class="activeWindows" id="dockWrapper" style="display: none;">
	<div class="container dock">
		<ul class="clearfix" id="dock"></ul>
		<div class="ui_toggling_container switch_min_container">
			<span id="switch_min_panel_btn" class="" data-desc="Minimize / Restore 
 all open windows"><i id="switch_min_panel_icon" class="icon-arrow-down icon-white"></i></span>
		</div>
	</div>
</div>

<div class="shrinkcells_container smallfonts_container ui_toggling_container">
	<span class="pull-right shrinkcells" data-title="Row Height" data-desc="Row Height
 Switch between single or multi-line rows"><i class="icon-white icon-th-list rotate90"></i></span>
	<span class="pull-right cellwidth" data-title="Column Width" data-desc="Column Width
 Adjust between full column widths (horizontal scroll) or 
 force smaller width columns (wrap to window)"><i class="icon-white icon-th-list"></i></span>
	<span class="pull-right smallfonts" data-title="Font Size" data-desc="Font Size
 Switch font size between small and regular"><i class="icon-font icon-white icon-plus"></i></span>
</div>

<div id="time_adv_log_console" style="display: none; top: 50px; z-index: 9999999;">	
	<table class="table table-condensed table-transparent">
		<tbody>
			<tr class="timer_log_tmpl" style="display:none;" state="ready_to_go">
				<td class="timer_log_info">
					<div class="entity_name_area control-text"></div>
					<div class="entity_desc_area small well-small"></div>
				</td>
				<td><span class="total_area control-text"></span></td>				
				<td class="timer_start_pause">
					<input type="button" name="start" value="Start" class="btn btn-success control_button">
					<input type="button" name="pause" value="Pause" class="btn btn-warning control_button" style="display:none;">
				</td>
				<td class="timer_stop_review">
					<input type="button" name="stop" value="Stop &amp; Review" class="btn btn-primary control_button" style="display:none;">
				</td>
				<td class="timer_remove">
					<span class="remove icon-button icon-button-danger" title="Remove / Cancel"><i class="icon-remove icon-white"></i></span>
				</td>
			</tr>
			<tr class="hide">
				<td colspan="2"></td>
				<td class="breakdown" colspan="3">
					<table class="table table-condensed table-transparent">
			<tbody><tr class="timer_history_tmpl" link_type="" link_id="" style="display:none;">
							<td class=""></td>
				<td class="history_duration_area"></td>
							<td class="history_area small"></td>
							<td class="" colspan="2">
								<i class="finish_icon small pull-right" style="display:none;">Lodged <img src="http://paintpad.mysys.com.au/components/com_cweb/images/ok.png" class="finish_icon"></i>
							</td>
			</tr>
					</tbody></table>
				</td>
			</tr>
		</tbody>
	</table>
	<?php ActiveForm::end(); ?> 
</div>

