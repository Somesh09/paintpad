<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'https://cdn.jsdelivr.net/npm/croppie@2.6.4/croppie.css'
        
        

    ];
    public $js = [
            'js/jquery-ui.min.js',
            'https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.4/croppie.js'

         ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}

